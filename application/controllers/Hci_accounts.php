<?php

class Hci_accounts extends CI_controller {

function hci_accounts() {
	parent::__construct();
	$this->load->model('hci_accounts_model');
	$this->load->model('hci_feestructure_model');
}

function index() 
{
	// $data['comp_info'] = $this->company_model->get_comp_info();
	// $data['grp_info']  = $this->company_model->get_grp_info();
	// $data['fy_info']  = $this->company_model->get_fy_info();
	// $data['ay_info']  = $this->company_model->get_ay_info();

	// $data['main_content'] = 'company_view';
	// $data['title'] = 'COMPANY';
	// $this->load->view('includes/template',$data);
}

function invoice_view() 
{
	$data['invs']  = $this->hci_accounts_model->get_invoices();
	$data['main_content'] = 'hci_invoice_view';
	$data['title'] = 'Invoice';
	$this->load->view('includes/template',$data);
}

function adjusment_view()
{
	$this->load->model('hci_studentreg_model');
	$this->load->model('hci_grade_model');
	
	$data['reg_stu'] = $this->hci_studentreg_model->registeed_Stu();
	$data['adjs'] = $this->hci_accounts_model->load_adjusment();
	$data['fss'] = $this->hci_feestructure_model->load_active_feestructures();
	$data['fees']  = $this->hci_feestructure_model->get_feecats();
	$data['grd_info']  = $this->hci_grade_model->get_grd_info();
	$data['main_content'] = 'hci_feestructureadjusments_view';
	$data['title'] = 'FEE STRUCTURE ADJUSMENTS';
	$this->load->view('includes/template',$data);
}

function save_adjusment()
{
	$adj_save = $this->hci_accounts_model->save_adjusment();

	if($adj_save)
	{
		$this->session->set_flashdata('flashSuccess', 'Data saved successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to save Data. Retry.');
	}

	redirect('hci_accounts/adjusment_view');
}

function edit_pay_plan()
{
	echo json_encode($this->hci_feestructure_model->edit_pay_plan());
}

function load_invoicefees()
{
	$inv = $this->input->post('id');
	echo json_encode($this->hci_accounts_model->load_invoicefees($inv));
}

function config_adjusments()
{
	$conf_save = $this->hci_accounts_model->config_adjusments();

	if($conf_save)
	{
		$this->session->set_flashdata('flashSuccess', 'Configured successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to Configure. Retry.');
	}

	redirect('hci_accounts/adjusment_view');
}

function print_invoice()
{
   $invlist = $this->input->post('print_docs');

	$data['invlist'] = array();

	foreach ($invlist as $inv) 
	{
		$data['invlist'][] = $this->hci_accounts_model->load_invoicefees($inv);
	}
        
    $html = $this->load->view( 'hci_invoice_printview' ,$data,true);
   	echo base64_encode($html);
}

function process_invoice()
{
	echo json_encode($this->hci_accounts_model->erp_integration());
}

function invoice_edit_view()
{
	// $data['reg_stu'] = $this->hci_studentreg_model->registeed_Stu();
	// $data['adjs'] = $this->hci_accounts_model->load_adjusment();
	// $data['fss'] = $this->hci_feestructure_model->load_active_feestructures();
	// $data['fees']  = $this->hci_feestructure_model->get_feecats();
	// $data['grd_info']  = $this->hci_grade_model->get_grd_info();

	$data['main_content'] = 'hci_invoiceedit_view';
	$data['title'] = 'EDIT INVOICE';
	$this->load->view('includes/template',$data);
}

function regenerate_invoice()
{
	echo json_encode($this->hci_accounts_model->regenerate_invoice());
}

function monthend_view()
{
	$data['monthenddates'] = $this->hci_accounts_model->get_monthend_dates();	
	$data['main_content'] = 'hci_monthend_view';
	$data['title'] = 'MONTHEND';
	$this->load->view('includes/template',$data);
}

function run_monthend()
{
	$monthendrun = $this->hci_accounts_model->run_monthend();

	if($monthendrun)
	{
		$this->session->set_flashdata('flashSuccess', 'Month-end process completed successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to complete month-end process. Retry.');
	}

	redirect('hci_accounts/monthend_view');
}

// function creditnote_view()
// {
//     $data['main_content'] = 'hci_creditnote_view';
// 	$data['title'] = 'EDIT INVOICE';
// 	$this->load->view('includes/template',$data);
// }

function creditnote_view() 
{
	$brlist = $this->auth->get_accessbranch();
	foreach ($brlist as $branch) 
	{
		$brcode = $this->auth->get_brcode($branch);
		$data['cnoteindex'][$branch] = $this->sequence->getindexval('CRN'.$brcode.date('y'),5);
	}
	$data['main_content'] = 'hci_creditnote_view';
	$data['title'] = 'CREDIT NOTE';
	$this->load->view('includes/template',$data);
}

function debitnote_view() 
{
	$brlist = $this->auth->get_accessbranch();
	foreach ($brlist as $branch) 
	{
		$brcode = $this->auth->get_brcode($branch);
		$data['dnoteindex'][$branch] = $this->sequence->getindexval('DBN'.$brcode.date('y'),5);
	}
	$data['main_content'] = 'hci_debitnote_view';
	$data['title'] = 'DEBIT NOTE';
	$this->load->view('includes/template',$data);
}

function save_creditnote()
{
	$cnote_save = $this->hci_accounts_model->save_creditnote();

	if($cnote_save)
	{
		$this->session->set_flashdata('flashSuccess', 'Credit Note created successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to Create Credit Note. Retry.');
	}

	redirect('hci_accounts/creditnote_view');
}

function save_debitnote()
{
	$dnote_save = $this->hci_accounts_model->save_debitnote();

	if($dnote_save)
	{
		$this->session->set_flashdata('flashSuccess', 'Debit Note created successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to Create Debit Note. Retry.');
	}

	redirect('hci_accounts/debitnote_view');
}

function load_cnotelist()
{
	echo json_encode($this->hci_accounts_model->load_cnotelist());
}

function load_dnotelist()
{
	echo json_encode($this->hci_accounts_model->load_dnotelist());
}

function print_cnote()
{
	$cnotelist = $this->input->post('print_docs');

	$data['cnotelist'] = array();

	foreach ($cnotelist as $cnote) 
	{
		$data['cnotelist'][] = $this->hci_accounts_model->load_cnote_details($cnote);
	}
        
    $html = $this->load->view( 'hci_cnote_printview' ,$data,true);
   	echo base64_encode($html);
}

function print_dnote()
{
	$dnotelist = $this->input->post('print_docs');

	$data['dnotelist'] = array();

	foreach ($dnotelist as $dnote) 
	{
		$data['dnotelist'][] = $this->hci_accounts_model->load_dnote_details($dnote);
	}
        
    $html = $this->load->view( 'hci_dnote_printview' ,$data,true);
   	echo base64_encode($html);
}

function chequemanagment_view() 
{
	$data['main_content'] = 'hci_chequemanagement_view';
	$data['title'] = 'CHEQUE MANAGEMENT';
	$this->load->view('includes/template',$data);
}

function load_chequelist()
{
	echo json_encode($this->hci_accounts_model->load_chequelist());
}

function bouns_cheque()
{
	echo json_encode($this->hci_accounts_model->bouns_cheque());
}


/*start cnote receipt*/
function load_cnote_receipt()
{
	$cnote = $this->input->post('id');
	echo json_encode($this->hci_accounts_model->load_cnote_details($cnote));
}
/*end cnote receipt*/
/*start dnote receipt*/
function load_dnote_receipt()
{
	$dnote = $this->input->post('id');
	echo json_encode($this->hci_accounts_model->load_dnote_details($dnote));
}
/*end dnote receipt*/








}