<?php

class User extends CI_controller {

function user() {
	parent::__construct();
	$this->load->model('user_model');
	$this->load->model('company_model');
}

function index() 
{
	$data['groups'] = $this->user_model->get_user_groups();
	$data['users']  = $this->user_model->get_users_list();

	$data['main_content'] = 'user_view';
	$data['title'] = 'USER';
	$this->load->view('includes/template',$data);
}

function usergroup_view() 
{
	$data['groups']  = $this->user_model->get_user_groups();
	$data['company'] = $this->company_model->get_grp_info();

	$data['main_content'] = 'hgc_usergroup_view';
	$data['title'] = 'USER GROUP';
	$this->load->view('includes/template',$data);
}

function save_usergroup()
{
	$save_ugrp = $this->user_model->save_usergroup();

	if($save_ugrp)
	{
		$this->session->set_flashdata('flashSuccess', 'User group saved successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to save User group. Retry.');
	}

	redirect('user/usergroup_view');
}

function reset_user()
{
	echo json_encode($this->user_model->reset_user());
}

function update_status()
{
	echo json_encode($this->user_model->update_status());	
}

function delete_user()
{
	echo json_encode($this->user_model->delete_user());	
}
}