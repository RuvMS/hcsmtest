<?php
class Hci_admission extends CI_controller
{

    function hci_admission()
    {
        parent::__construct();
        $this->load->model('hci_admission_model');
    }

    function inquary_view()
    {
        $id = $this->uri->segment(3);
        if(!empty($id))
        {
            $branchlist = $this->auth->get_accessbranch();

            $this->db->where("es_enquiryid",$id);
            $this->db->where_in("eq_branch",$branchlist);
            $stu = $this->db->get('hci_enquiry')->row_array();
            if(!empty($stu))
            {
                $this->hci_admission_model->obj = $this->hci_admission_model->get_row('hci_enquiry',array('es_enquiryid'=>$id));
            }
        }
        $data['pop_method'] =$this->hci_admission_model->get_popularmethod();
        $data['reg_stu'] =$this->hci_admission_model->get_registeed_Stu();
        $data['stu_info'] = $this->hci_admission_model->get_student_info();
        $data['main_content'] = 'hci_admission_view';
        $data['title'] = 'ADMISSION';
        $this->load->view('includes/template', $data);
    }

    public function new_admission($action = "insert", $id = "") {
        $post = $this->input->post("post");

        if(empty($post['es_enquiryid']))
        {
            $action="insert";
        }
        else
        {
            $action="update";
        }
        if($action=="insert"){
            $today= date("Y-m-d");
            $new_method = $this->input->post("new_method");

            if(!empty($new_method))
            {
                $this->db->insert('hci_popularmethod',array('pm_method' => $new_method));
                $method = $this->db->insert_id();
                $post['knowabt'] = $method;
            }

            if( $this->input->post("post"))
            {
                $this->db->where('br_id',$post['eq_branch']);
                $brinfo = $this->db->get('hgc_branch')->row_array();

                $post['eq_code'] = $this->sequence->generate_sequence('INQ'.$brinfo['br_code'].date('y'),3);;
                $post['eq_createdon'] = $today;
                $sql = $this->hci_admission_model->insert('hci_enquiry',$post);
        if($sql)
        {
            $this->msg->set('admission', "success_Saved successfully");
        }
             //   $this->hci_admission_model->insert('hci_preadmission',$today);


            }
        }

        if($action=="update")
        {
            $post = $this->input->post("post");

            $new_method = $this->input->post("new_method");

            if(!empty($new_method))
            {
                $this->db->insert('hci_popularmethod',array('pm_method' => $new_method));
                $method = $this->db->insert_id();
                $post['knowabt'] = $method;
            }

            if( $this->input->post("post"))
            {
                $sql = $this->hci_admission_model->update('hci_enquiry',$post,array('es_enquiryid' => $post['es_enquiryid']));

                if($sql)
                {
                    $this->msg->set('admission', "success_Update successfully");
                }
            }

        }

        redirect('hci_admission/inquary_view');
    }

    function get_student_info()
    {
        echo json_encode($this->hci_admission_model->get_student_details());
    }

    function load_methodof_knowing()
    {
        echo json_encode($this->hci_admission_model->load_methodof_knowing());
    }

    function remove_admission()
    {
        echo json_encode($this->hci_admission_model->remove_admission());
    }

}