<?php

class Company extends CI_controller {

function company() {
	parent::__construct();
	$this->load->model('company_model');
}

function index() 
{
	$data['comp_info'] = $this->company_model->get_comp_info();
	$data['grp_info']  = $this->company_model->get_grp_info();
	$data['fy_info']  = $this->company_model->get_fy_info();
	$data['ay_info']  = $this->company_model->get_ay_info();

	$data['main_content'] = 'company_view';
	$data['title'] = 'COMPANY';
	$this->load->view('includes/template',$data);
}

function update_comp_info()
{
	$save_comp = $this->company_model->update_comp_info();

	if($save_comp)
	{
		$this->session->set_flashdata('flashSuccess', 'Changes saved successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to save changes. Retry.');
	}

	redirect('company?tab_id=company');

}

function save_group()
{
	$grp_save = $this->company_model->save_group();

	if($grp_save)
	{
		$this->session->set_flashdata('flashSuccess', 'Group saved successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to save group. Retry.');
	}

	redirect('company?tab_id=group');
}

function save_branch()
{
	$br_save = $this->company_model->save_branch();

	if($br_save)
	{
		$this->session->set_flashdata('flashSuccess', 'Branch saved successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to save branch. Retry.');
	}

	redirect('company?tab_id=branch');
}

function load_branches()
{
	echo json_encode($this->company_model->load_branches());
}

function edit_branch_load()
{
	echo json_encode($this->company_model->edit_branch_load());
}

function save_fyear()
{
	$fy_save = $this->company_model->save_fyear();

	if($fy_save)
	{
		$this->session->set_flashdata('flashSuccess', 'Financial Year saved successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to save Financial Year. Retry.');
	}

	redirect('company?tab_id=fyear');
}

function save_ayear()
{
	$ay_save = $this->company_model->save_ayear();

	if($ay_save)
	{
		$this->session->set_flashdata('flashSuccess', 'Academic Year saved successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to save Academic Year. Retry.');
	}

	redirect('company?tab_id=ayear');
}

function load_terms()
{
	echo json_encode($this->company_model->load_terms());
}

function save_termperiod()
{
	$tp_save = $this->company_model->save_termperiod();

	if($tp_save)
	{
		$this->session->set_flashdata('flashSuccess', 'Term Period saved successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to save Term Period. Retry.');
	}

	redirect('company?tab_id=tperiod');
}

function load_termsperiods()
{
	echo json_encode($this->company_model->load_termsperiods());
}

function temp_view1()
{
	$data['main_content'] = 'temp_view';
	$data['title'] = 'ENROLLMENTS  - Kandy Branch';
	$this->load->view('includes/template',$data);
}

function temp_view2()
{
	$data['main_content'] = 'temp_view2';
	$data['title'] = 'AVERAGE PERFORMANCE';
	$this->load->view('includes/template',$data);
}

function temp_view3()
{
	$data['main_content'] = 'temp_view3';
	$data['title'] = 'ATTENDANCE';
	$this->load->view('includes/template',$data);
}

function temp_view4()
{
	$data['main_content'] = 'temp_view4';
	$data['title'] = 'RESULT SHEET';
	$this->load->view('includes/template',$data);
}
}