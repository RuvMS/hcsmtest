<?php

class Hci_staff extends CI_controller{

function hci_staff() 
{
	parent::__construct();
	$this->load->model('hci_config_model');
	$this->load->model('hci_grade_model');
	$this->load->model('hci_edustructure_model');
	$this->load->model('Hci_staff_model');
}

function index(){

	$data['deptinfo']  = $this->hci_config_model->get_deptinfo();
	$data['desiginfo']  = $this->hci_config_model->get_desiginfo();
	$data['grd_info']  = $this->hci_grade_model->get_grd_info();
	$data['sections']  = $this->hci_edustructure_model->get_sections();
	$data['main_content'] = 'hci_staff_view.php';
    $data['title'] = 'Staff';
    $this->load->view('includes/template', $data);
}

function staff_save()
{
	$save_staf = $this->Hci_staff_model->staff_save();

	if($save_staf)
	{
		$this->session->set_flashdata('flashSuccess', 'Staff member saved successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to save staff member. Retry.');
	}

	redirect('Hci_staff');
}

}

