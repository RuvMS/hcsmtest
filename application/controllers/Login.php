<?php

class Login extends CI_controller {

function login() {
	parent::__construct();
	$this->load->model('login_model');
}

function index() {
	$data['main_content'] = 'login_view.php';
	$data['title'] = 'LOGIN';
	$this->load->view('login_view',$data);
}

function loginSubmit()
{
	$result = $this->login_model->authenticateLogin();

	if(!empty($result))
	{			
		redirect('dashboard');
	}
	else
	{
		redirect('login?login=invalid');
	}
}

function logout()
{
	$this->session->userdata = array();
	$this->session->sess_destroy();
	redirect('login?login=logout');
}

}