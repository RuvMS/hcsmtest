<?php
class Hci_attendance extends CI_controller
{
function hci_attendance()
{
    parent::__construct();
    $this->load->model('hci_attendance_model');
    $this->load->model('company_model');
}

function studentclass_attendance()
{
	$data['main_content'] = 'hci_studentattendance_view';
    $data['ay_info']  = $this->company_model->get_ay_info();
    // $data['grd_info']  = $this->hci_grade_model->get_grd_info();
    $data['title'] = 'STUDENT SCHOOL ATTENDANCE';
    $this->load->view('includes/template', $data);
}

function load_classlist()
{
	echo json_encode($this->hci_attendance_model->load_classlist());
}

function search_attendance()
{
	echo json_encode($this->hci_attendance_model->search_attendance());
}

function save_attendance()
{
	echo json_encode($this->hci_attendance_model->save_attendance());
}
}