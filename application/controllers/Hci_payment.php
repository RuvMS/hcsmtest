<?php

class Hci_payment extends CI_controller {

function hci_payment() {
	parent::__construct();
	$this->load->model('hci_payment_model');
	$this->load->model('hci_studentreg_model');
	$this->load->model('hci_accounts_model');
	$this->load->model('hci_student_model');
}

function index() 
{
	$brlist = $this->auth->get_accessbranch();
	$data['students'] = $this->hci_studentreg_model->registeed_Stu();
	$data['banks'] = $this->hci_payment_model->load_banklist();

	foreach ($brlist as $branch) 
	{
		$brcode = $this->auth->get_brcode($branch);
		$data['payindex'][$branch] = $this->sequence->getindexval('REC'.$brcode.date('y'),5);
	}

	$data['main_content'] = 'hci_payment_view';
	$data['title'] = 'RECEIPT';
	$this->load->view('includes/template',$data);
}

function load_outstanding()
{
	echo json_encode($this->hci_accounts_model->load_outstanding());
}

function save_payment()
{
	$save_pay = $this->hci_payment_model->save_payment();

	if($save_pay)
	{
		$this->session->set_flashdata('flashSuccess', 'Payment saved successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to save payment. Retry.');
	}

	redirect('hci_payment');
}

function load_customers()
{
	echo json_encode($this->hci_payment_model->load_customers());
}

function payments_view()
{
	// $data['students'] = $this->hci_studentreg_model->registeed_Stu();

	$data['main_content'] = 'hci_paymentlist_view';
	$data['title'] = 'PAYMENT';
	$this->load->view('includes/template',$data);
}

function load_payments()
{
	echo json_encode($this->hci_payment_model->load_payments());
}

function print_receipt()
{
	$paylist = $this->input->post('print_docs');

	$data['paylist'] = array();

	foreach ($paylist as $pay) 
	{
		$data['paylist'][] = $this->hci_payment_model->load_payment_details($pay);
	}
        
    $html = $this->load->view( 'hci_receipt_printview' ,$data,true);
   	echo base64_encode($html);
}

//start tested fuction.....

function load_paymentrecipt()
{
	$pay = $this->input->post('id');
	echo json_encode($this->hci_payment_model->load_payment_details($pay));
}


//End tested fuction......


}