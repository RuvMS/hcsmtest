<?php

class Hci_subject extends CI_controller {

function hci_subject() {
	parent::__construct();
	$this->load->model('hci_subject_model');
	$this->load->model('hci_edustructure_model');
}

function subjectgroup_view() 
{
	$data['subgrplist']  = $this->hci_subject_model->get_subjectgrps();
	$data['schemes']  = $this->hci_edustructure_model->get_schemes();

	$data['main_content'] = 'hci_subjectgroup_view';
	$data['title'] = 'SUBJECT GROUP';
	$this->load->view('includes/template',$data);
}

function save_subjectgroup()
{
	$save_subgrp = $this->hci_subject_model->save_subjectgroup();

	if($save_subgrp)
	{
		$this->session->set_flashdata('flashSuccess', 'Subject group saved successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to save subject group. Retry.');
	}

	redirect('hci_subject/subjectgroup_view');
}

function change_subgrpstatus()
{
	echo json_encode($this->hci_subject_model->change_subgrpstatus());
}

function subject_view() 
{
	$data['subjects']  = $this->hci_subject_model->get_subjects();
	$data['sections']  = $this->hci_edustructure_model->get_sections();
	$data['curriculum']  = $this->hci_subject_model->get_curriculum();

	$data['main_content'] = 'hci_subject_view';
	$data['title'] = 'SUBJECT';
	$this->load->view('includes/template',$data);
}

function load_schemelist()
{
	echo json_encode($this->hci_subject_model->load_schemelist());
}

function save_subject()
{
	$save_sub = $this->hci_subject_model->save_subject();

	if($save_sub)
	{
		$this->session->set_flashdata('flashSuccess', 'Subject saved successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to save subject. Retry.');
	}

	redirect('hci_subject/subject_view');
}

function change_substatus()
{
	echo json_encode($this->hci_subject_model->change_substatus());
}

function edit_sub_load()
{
	echo json_encode($this->hci_subject_model->edit_sub_load());
}
}