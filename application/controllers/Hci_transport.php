<?php

class Hci_transport extends CI_controller {

function hci_transport() 
{
	parent::__construct();
	$this->load->model('hci_transport_model');
	$this->load->model('hci_studentreg_model');
	$this->load->model('hci_transport_model');
}

function manage_routes_view()
{
	$data['routeindex'] = $this->sequence->getindexval('ROU',3);
	$data['routes'] = $this->hci_transport_model->get_routes();
	$data['main_content'] = 'hci_manageroutes_view';
	$data['title'] = 'TRANSPORT ROUTE';
	$this->load->view('includes/template',$data);
}

function save_route()
{
	$save_route = $this->hci_transport_model->save_route();

	if($save_route)
	{
		$this->session->set_flashdata('flashSuccess', 'Saved successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to save. Retry.');
	}

	redirect('hci_transport/manage_routes_view');
}

function manage_pickingpoints()
{
	$data['pointindex'] = $this->sequence->getindexval('PCP',3);
	$data['picks'] = $this->hci_transport_model->get_picking_points();
	$data['routes'] = $this->hci_transport_model->get_routes();
	$data['main_content'] = 'hci_pickingpoints_view';
	$data['title'] = 'TRANSPORT ROUTE';
	$this->load->view('includes/template',$data);
}

function save_pickpoint()
{
	$save_pick = $this->hci_transport_model->save_pickpoint();

	if($save_pick)
	{
		$this->session->set_flashdata('flashSuccess', 'Saved successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to save. Retry.');
	}

	redirect('hci_transport/manage_pickingpoints');
}

function transport_feestructure()
{
	$data['fsindex'] = $this->sequence->getindexval('TFS',3);
	$data['feestrucs'] = $this->hci_transport_model->get_feestructure();
	$data['main_content'] = 'hci_transfeestruc_view';
	$data['title'] = 'TRANSPORT FEE STRUCTURE';
	$this->load->view('includes/template',$data);
}

function save_feestructure()
{
	$save_fs = $this->hci_transport_model->save_feestructure();

	if($save_fs)
	{
		$this->session->set_flashdata('flashSuccess', 'Saved successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to save. Retry.');
	}

	redirect('hci_transport/transport_feestructure');
}

function load_transfeestructures()
{
	echo json_encode($this->hci_transport_model->load_transfeestructures());
}

function transport_registration()
{
	$data['regindex'] = $this->sequence->getindexval('TRG',3);
	$data['stus'] = $this->hci_studentreg_model->registeed_Stu();
	$data['fss'] = $this->hci_transport_model->get_feestructure();
	$data['routes'] = $this->hci_transport_model->get_routes();
	$data['registrations'] = $this->hci_transport_model->get_registrations();
	$data['main_content'] = 'hci_transregi_view';
	$data['title'] = 'TRANSPORT REGISTRATION';
	$this->load->view('includes/template',$data);
}

function registration_trans()
{
	$regi = $this->hci_transport_model->save_registration();

	$today = date('Y-m-d');

	$this->db->where('term_sdate >=',$today);
    $this->db->where('term_sdate <=',$today);
    $term = $this->db->get('hci_term')->row_array(); 

	//$invoice = $this->hci_accounts_model->generate_invoice('STUDENT',$this->input->post('student'),'TRANSPORT-REG',$term,$today);

	if($regi)
	{
		$this->session->set_flashdata('flashSuccess', 'Saved successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to save. Retry.');
	}

	redirect('hci_transport/transport_registration');
}

function load_pickingpoints()
{
	echo json_encode($this->hci_transport_model->load_pickingpoints());
}
}