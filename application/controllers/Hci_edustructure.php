<?php

class Hci_edustructure extends CI_controller {

function hci_edustructure() {
	parent::__construct();
	$this->load->model('hci_edustructure_model');
}

function educationalsection() 
{
	$data['sections']  = $this->hci_edustructure_model->get_sections();

	$data['main_content'] = 'hci_edusection_view';
	$data['title'] = 'EDUCATIONAL SECTION';
	$this->load->view('includes/template',$data);
}

function save_edusection()
{
	$save_subgrp = $this->hci_edustructure_model->save_edusection();

	if($save_subgrp)
	{
		$this->session->set_flashdata('flashSuccess', 'Educational sector saved successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to save educational sector. Retry.');
	}

	redirect('hci_edustructure/educationalsection');
}

function change_sectionstatus()
{
	echo json_encode($this->hci_edustructure_model->change_sectionstatus());
}

function educationalscheme() 
{
	$data['sections']  = $this->hci_edustructure_model->get_sections();
	$data['schemes']  = $this->hci_edustructure_model->get_schemes();

	$data['main_content'] = 'hci_scheme_view';
	$data['title'] = 'EDUCATIONAL SCHEME';
	$this->load->view('includes/template',$data);
}

function save_eduscheme()
{
	$save_scheme = $this->hci_edustructure_model->save_eduscheme();

	if($save_scheme)
	{
		$this->session->set_flashdata('flashSuccess', 'Educational scheme saved successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to save educational scheme. Retry.');
	}

	redirect('hci_edustructure/educationalscheme');
}

function change_schemestatus()
{
	echo json_encode($this->hci_edustructure_model->change_schemestatus());
}
}