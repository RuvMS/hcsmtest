<?php
class Hci_student extends CI_controller
{
function hci_student()
{
    parent::__construct();
    $this->load->model('hci_student_model');
    $this->load->model('hci_studentreg_model');
    $this->load->model('hci_accounts_model');
    $this->load->model('hci_feestructure_model');
    $this->load->model('hci_grade_model');
    $this->load->model('company_model');
}

function student_lookup()
{
	$data['main_content'] = 'hci_student_lookup';
	$data['title'] = 'Student Lookup';
	$this->load->view('includes/template',$data);
}

function load_student()
{
	echo json_encode($this->hci_student_model->load_student());
}

function load_stuprofview()
{
	if(isset($_GET['id']))
    {
        $data['stu'] = $_GET['id'];
        $data['type'] = $_GET['rt'];
    }
    else
    {
        $data['stu'] = null;
        $data['type'] = null;
    }
    $data['stu_info'] = $this->hci_student_model->stu_profile_data();

    if(!empty($data['stu_info']))
    {
        // $data['stu_info']['grd_info']  = $this->hci_grade_model->get_grd_info();
        $data['main_content'] = 'hci_studentprof_view';
        $data['title'] = 'STUDENT PROFILE';
        $this->load->view('includes/template',$data);
    }
    else
    {
        redirect('hci_student/student_lookup');
    }
}

function set_studentdatasession()
{
    $this->session->set_userdata('sessionstu',$_POST['id']);
    $this->session->set_userdata('sessionstutype',$_POST['rt']);

    echo true;
}

function load_stueditview()
{
    if(isset($_SESSION['sessionstu']))
    {
        $data['stu'] = $_SESSION['sessionstu'];
        $data['type'] = $_SESSION['sessionstutype'];
        $data['stu_data'] = $this->hci_student_model->load_student_data();
        $data['fees']  = $this->hci_studentreg_model->get_feecat_pplans();
        $data['acc_years'] = $this->company_model->get_ay_info();
        $data['main_content'] = 'hci_studentedit_view';
        $data['title'] = 'Student Edit';
        $this->load->view('includes/template', $data);
    }
    else
    {
        $this->session->set_flashdata('flashError', 'Session Expired. Login to continue');
        redirect('hci_student/student_lookup');
    }
}

function load_student_data()
{
    echo json_encode($this->hci_student_model->load_student_data());
}

function update_student_info()
{
    $update_stu = $this->hci_student_model->update_student_info();

    if($update_stu)
    {
        $this->session->set_flashdata('flashSuccess', 'Student Informations Updated successfully.');
    }
    else
    {
        $this->session->set_flashdata('flashError', 'Failed to Update Student Informations. Retry.');
    }

    redirect('hci_student/student_lookup');
}

function registerpendingstu()
{
    $reg_stu = $this->hci_student_model->registerpendingstu();

    if($reg_stu)
    {

        $id = $this->input->post('id');

        $this->db->where('st_tempreid',$id);
        $newstu = $this->db->get('st_details')->row_array();

        $this->db->where('term_id',$this->input->post('st_term'));
        $terms_date = $this->db->get('hci_term')->row_array();

        $this->db->where('br_id',$this->input->post('st_branch'));
        $branch = $this->db->get('hgc_branch')->row_array();

        $today= date("Y-m-d");

        $gen_invoice = $this->hci_accounts_model->generate_invoice('STUDENT',$newstu['id'],'REGISTRATION',$terms_date,$today,$branch,1);

        if($gen_invoice)
        {
            $this->db->where('id',$id);
            $this->db->update('st_details_temp',array('st_status' => 'R'));

            $this->db->where('promo_accyear',$this->input->post('st_accyear'));
            $this->db->where('promo_grade',$this->input->post('st_grade'));
            $this->db->where('promo_branch',$this->input->post('st_branch'));
            $isPromotedYear = $this->db->get('hci_gradepromotion')->row_array();

            $promoteSave['gp_ayear']        = $this->input->post('st_accyear');
            $promoteSave['gp_student']      = $newstu['id'];
            $promoteSave['gp_currgrade']    = $this->input->post('st_grade');
            $promoteSave['gp_promotedfrom'] = 0;
            $promoteSave['gp_branch']       = $this->input->post('st_branch');
            $promoteSave['gp_isleft']       = 0;

            if(!empty($isPromotedYear))
            {
                $promoteSave['gp_refpromo'] = $isPromotedYear['promo_id'];
            }

            $this->db->insert('hci_stuacademicdetails',$promoteSave);

            $this->session->set_flashdata('flashSuccess', 'Student Registred successfully.');
        }
        else
        {
            $this->session->set_flashdata('flashError', 'Failed to Register the Student. Retry.');
        }
    }
    else
    {
        $this->session->set_flashdata('flashError', 'Failed to Register the Student. Retry.');
    }

    redirect('hci_student/student_lookup');
}

function annualgradepromotion_view()
{
    //$data['reg_stu'] = $this->hci_studentreg_model->registeed_Stu();
    $data['main_content'] = 'hci_gradepromotion_view';
    $data['ay_info']  = $this->company_model->get_ay_info();
    $data['grd_info']  = $this->hci_grade_model->get_grd_info();
    $data['title'] = 'GRADE PROMOTION';
    $this->load->view('includes/template', $data);
}

function load_stubygrade()
{
	echo json_encode($this->hci_student_model->load_stubygrade());
}

function promote_students()
{
	$promote_stu = $this->hci_student_model->promote_students();

    if($promote_stu)
    {
        $this->session->set_flashdata('flashSuccess', 'Students promoted successfully.');
    }
    else
    {
        $this->session->set_flashdata('flashError', 'Failed to promote students. Retry.');
    }

    redirect('hci_student/annualgradepromotion_view');
}

function term_invoice_view()
{
    //$data['reg_stu'] = $this->hci_studentreg_model->registeed_Stu();
    $data['main_content'] = 'hci_terminvoice_view';
    $data['ay_info']  = $this->company_model->get_ay_info();
    $data['grd_info']  = $this->hci_grade_model->get_grd_info();
    $data['title'] = 'GRADE PROMOTION';
    $this->load->view('includes/template', $data);
}

function student_sufflingview()
{
    $data['main_content'] = 'hci_studentshuffle_view';
    $data['ay_info']  = $this->company_model->get_ay_info();
    $data['grd_info']  = $this->hci_grade_model->get_grd_info();
    $data['title'] = 'SHUFFLE STUDENTS';
    $this->load->view('includes/template', $data);
}

function load_class_stulist()
{
    echo json_encode($this->hci_student_model->load_class_stulist());
}

function load_shuffledata()
{
    echo json_encode($this->hci_student_model->load_shuffledata());
}

function save_suffling()
{
    echo json_encode($this->hci_student_model->save_suffling());
}

function load_student_discounts()
{
    echo json_encode($this->hci_student_model->load_student_discounts());
}

function studentWithdrawalSchool_view()
{
    $data['fees']  = $this->hci_studentreg_model->get_feecat_pplans();
    $data['main_content'] = 'hci_studentwithdrawal_view';
    $data['title'] = 'WITHDDRAW STUDENT';
    $this->load->view('includes/template', $data);
}

function load_sibling_data()
{
    $withdrawalPreData = $this->hci_student_model->load_sibling_data();
    $withdrawalPreData['feeTemps'] = $this->hci_feestructure_model->load_fee_templates();
    echo json_encode($withdrawalPreData);
}
}