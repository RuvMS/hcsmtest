<?php
class Hci_studentreg extends CI_controller
{

function hci_studentreg()
{
    parent::__construct();
    $this->load->model('hci_studentreg_model');
    $this->load->model('hci_feestructure_model');
    $this->load->model('company_model');
    $this->load->model('hci_transport_model');
    $this->load->model('hci_accounts_model');
}

function stureg_view()
{
    $id = $this->uri->segment(3);

    $branchlist = $this->auth->get_accessbranch();

    $this->db->where("es_enquiryid",$id);
    $this->db->where_in("eq_branch",$branchlist);
    $stu = $this->db->get('hci_enquiry')->row_array();
    if(!empty($stu))
    {
        $this->hci_studentreg_model->obj = $this->hci_studentreg_model->get_row('hci_enquiry',array('es_enquiryid'=>$id));
    }
    $data['siblingdiscs'] = $this->hci_studentreg_model->get_sibling_discounts();
    $data['fees']  = $this->hci_studentreg_model->get_feecat_pplans();
    // $data['fss'] = $this->hci_feestructure_model->load_active_feestructures();
    // $data['fts'] = $this->hci_feestructure_model->load_fee_templates();
    $data['stu_info'] = $this->hci_studentreg_model->get_stu_info();
    //$data['reg_stu'] = $this->hci_studentreg_model->registeed_Stu();
    $data['acc_years'] = $this->company_model->get_ay_info();
    // $data['fsstrans'] = $this->hci_transport_model->get_feestructure();
    // $data['routes'] = $this->hci_transport_model->get_routes();
    // $data['registrations'] = $this->hci_transport_model->get_registrations();
    $data['main_content'] = 'hci_studentreg_view';
    $data['title'] = 'STU_REG';
    $this->load->view('includes/template', $data);
}

public function new_studentreg() 
{
        $this->db->trans_begin();

        $today= date("Y-m-d");
        $post = $this->input->post("post");

        $this->db->where('fee_tftemp',$post['st_termtemplate']);
        $this->db->where('fee_aftemp',$post['st_admintemplate']);
        $fsexist = $this->db->get('hci_feemaster')->row_array();

        if(empty($fsexist))
        {
            $brcode = $this->auth->get_brcode($this->input->post('st_branch'));

            $fs_save['fee_description'] = 'SPECIAL_'.$post['st_termtemplate'].'_'.$post['st_admintemplate'];
            $fs_save['fee_status']      = "A";
            $fs_save['fee_effdate']     = $today;
            $fs_save['fee_aftemp']      = $post['st_admintemplate'];
            $fs_save['fee_tftemp']      = $post['st_termtemplate'];
            $fs_save['fee_branch']      = $this->input->post('st_branch');
            $fs_save['fee_iscurrent']   = 0;
            $fs_save['fee_code'] = $this->sequence->generate_sequence('FS/'.$brcode.'/',3);
            $result = $this->db->insert('hci_feemaster',$fs_save);

            $fsid = $this->db->insert_id();
        }
        else
        {
            $fsid = $fsexist['es_feemasterid'];
        }

        // $this->db->where('term_sdate >=',date('Y-m-d'));
        $this->db->where('term_id',$post['st_term']);
        $terms_date = $this->db->get('hci_term')->row_array();

        $this->db->where('br_id',$post['st_branch']);
        $branch = $this->db->get('hgc_branch')->row_array();

        if(isset($_POST['btn_type']))
        {
            $process_name = 'register';
        }
        else
        {
            $process_name = 'save';
        }

        if($this->input->post("post"))
        {
            $today= date("Y-m-d");

            $this->db->where('st_accyear',$post['st_accyear']);
            $this->db->where('st_grade',$post['st_grade']);
            $int_grd_count = $this->db->count_all_results('st_details');

            if($process_name=="register")
            {
                $post['st_feestructure'] = $fsid;
                $post['entered_date'] = $today;
                $post['st_status'] = 'R';
                $post['st_lastpromoted'] = $post['st_accyear'];

                $sql = $this->hci_studentreg_model->insert('st_details',$post);
                $new_stu=$this->hci_studentreg_model->last_insert;

                //---------------------------------- stu id

                $this->db->where('es_ac_year_id',$post['st_accyear']);
                $acc_year = $this->db->get('hgc_academicyears')->row_array();

                $this->db->where('int_id',$post['st_intake']);
                $intake = $this->db->get('hci_intake')->row_array();

                $this->db->where('grd_id',$post['st_grade']);
                $grd_data = $this->db->get('hci_grade')->row_array();

                $this->db->select('st_id');
                $this->db->where('id !=',$new_stu);
                $this->db->order_by('id','DESC');
                $this->db->limit(1);
                $tot_stu_count = $this->db->get('st_details')->row_array();

                if(empty($tot_stu_count))
                {
                    $tot_count = 1;
                }
                else
                {
                    $temp_idary = explode('/', $tot_stu_count['st_id']);
                    $tot_count = $temp_idary[4]+1;
                }
                
                $curr_year = date('y', strtotime($acc_year['ac_startdate']));
                $curr_intake = date('m', strtotime($intake['int_start']));
                $grd_code = $grd_data['grd_code'];

                $stu_index = $branch['br_code'].'/'.$curr_year.'/'.$curr_intake.'/'.$grd_code.'-'.($int_grd_count+1).'/'.($tot_count);

                $this->db->where('id',$new_stu);
                $this->db->update('st_details',array('st_id'=>$stu_index));

                $this->db->where('ug_level',5);
                $usergrp = $this->db->get('hgc_usergroup')->row_array();

                $saveuser['user_name'] = $stu_index;
                $saveuser['user_password'] = md5('123');
                $saveuser['user_ugroup'] = $usergrp['ug_id'];
                $saveuser['user_employee'] = $new_stu;
                $saveuser['user_group'] = $branch['br_group'];
                $saveuser['user_branch'] = $branch['br_id'];
                $saveuser['user_status'] = 'A';

                $this->db->insert('hgc_user',$saveuser);
            }
            else
            {
                $post['st_id'] = $this->sequence->generate_sequence('TEMPSTU'.$branch['br_code'],4);
                $post['entered_date'] = $today;
                $post['st_status'] = 'P';

                $sql = $this->hci_studentreg_model->insert('st_details_temp',$post);
                $new_stu=$this->hci_studentreg_model->last_insert;
            }

            //-----------------------------siblings----------------

            $sibilins=$this->input->post("es_sibiling");

            if(!empty($sibilins))
            {
                $this->db->where('es_reftable','st_details');
                $this->db->where_in('es_admissionid',$sibilins);
                $family = $this->db->get('hci_sibling')->result_array();

                $tot_families = $this->db->count_all('hci_stfamily');

                if(empty($family))
                {
                    $this->db->insert('hci_stfamily',array("sf_num"=>($tot_families+1)));
                    $family_id = $this->db->insert_id();
                }
                else
                {
                    $family_id = $family[0]['es_family'];
                }

                //array_push($sibilins,$new_stu);

                sort($sibilins);

                foreach ($sibilins as $key => $ns) 
                {
                    $this->db->where('es_reftable','st_details');
                    $this->db->where('es_admissionid',$ns);
                    $sibls = $this->db->get('hci_sibling')->row_array();

                    $siblins_save['es_family'] = $family_id;
                    $siblins_save['es_admissionid'] = $ns;
                    $siblins_save['es_seqnumber'] = $key+1;
                    $siblins_save['es_reftable'] = 'st_details';

                    if(empty($sibls))
                    {
                        $this->db->insert('hci_sibling',$siblins_save);
                    }
                }

                $add_tosib['es_family'] = $family_id;
                $add_tosib['es_admissionid'] = $new_stu;
                $add_tosib['es_seqnumber'] = count($sibilins)+1;

                if($process_name=="register")
                {
                    $add_tosib['es_reftable'] = 'st_details';
                }
                else
                {
                    $add_tosib['es_reftable'] = 'st_details_temp';
                }

                $this->db->insert('hci_sibling',$add_tosib);
            }

            //-------------------------payment scheme---------------------

            $pplans = $this->input->post('pplan');

            if($process_name=="register")
            {
                $savespp['spp_reftable'] = 'st_details';
            }
            else
            {
                $savespp['spp_reftable'] = 'st_details_temp';
            }

            $this->db->set('spp_status','D');
            $this->db->where('spp_reftable',$savespp['spp_reftable']);
            $this->db->where('spp_customer',$new_stu);
            $this->db->update('hci_cuspaymentplan');

            foreach ($pplans as $plan) 
            {
                if(!empty($plan))
                {
                    $this->db->where('spp_customer',$new_stu);
                    $this->db->where('spp_reftable',$savespp['spp_reftable']);
                    $this->db->where('spp_paymentplan',$plan);
                    $isexist = $this->db->get('hci_cuspaymentplan')->row_array();

                    $savespp['spp_status'] = 'A';

                    if(empty($isexist))
                    {
                        $savespp['spp_customer'] = $new_stu;
                        $savespp['spp_paymentplan'] = $plan;

                        $this->db->insert('hci_cuspaymentplan',$savespp);
                    }
                    else
                    {
                        $this->db->where('spp_id',$isexist['spp_id']);
                        $this->db->update('hci_cuspaymentplan',$savespp);
                    }
                }
            }

            //------------------Discounts-----------------

            $discounts = array();

            // if(isset($_POST['discountschk']))
            // {
            //     $discounts = $this->input->post('discountschk');
            // }

            $new_discount = $this->input->post('iId');
            
            foreach ($new_discount as $dis) 
            {
                if($dis>0)
                {
                    $pref = "_".$dis;
                }
                else
                {
                    $pref = '';
                }

                $adj_type = $this->input->post('adj_type'.$pref);
                $adj_name = $this->input->post('studisc'.$pref);
                $adj_fee = $this->input->post('adj_fee'.$pref);
                $adj_calamttype = $this->input->post('adj_calamttype'.$pref);
                $adj_amt = $this->input->post('amount'.$pref);

                if(!empty($adj_name))
                {
                    $save_adj['adj_description']   = $adj_name;
                    $save_adj['adj_type']          = $adj_type;
                    $save_adj['adj_feecat']        = $adj_fee;
                    $save_adj['adj_applyfor']      = 7;
                    $save_adj['adj_amttype']       = $adj_calamttype;
                    $save_adj['adj_amount']        = $adj_amt;
                    $save_adj['adj_status']        = 'A';
                    $save_adj['adj_isspec']        = 1;
                    // $save_adj['adj_glcode']        = $;
                    // $save_adj['adj_gldescription'] = $;
                    $this->db->insert('hci_adjusment',$save_adj);

                    $newdiscid = $this->db->insert_id();

                    array_push($discounts,$newdiscid);
                }
            }

            if(!empty($discounts))
            {
                foreach ($discounts  as $disc) 
                {
                    if($process_name=="register")
                    {
                        $disc_save['ats_reftable'] = 'st_details';
                    }
                    else
                    {
                        $disc_save['ats_reftable'] = 'st_details_temp';
                    }

                    $disc_save['ats_status'] = 'A';
                    $disc_save['ats_adjstemp'] = $disc;
                    $disc_save['ats_student'] = $new_stu;

                    $this->db->insert('hci_adjsstudent',$disc_save);
                }
            }

            //------------------------ Invoice ------------------

            if($process_name=="register")
            {

                $this->hci_accounts_model->generate_invoice('STUDENT',$new_stu,'REGISTRATION',$terms_date,$today,$branch);

                $this->db->where('promo_accyear',$post['st_accyear']);
                $this->db->where('promo_grade',$post['st_grade']);
                $this->db->where('promo_branch',$post['st_branch']);
                $isPromotedYear = $this->db->get('hci_gradepromotion')->row_array();

                $promoteSave['gp_ayear']        = $post['st_accyear'];
                $promoteSave['gp_student']      = $new_stu;
                $promoteSave['gp_currgrade']    = $post['st_grade'];
                $promoteSave['gp_promotedfrom'] = 0;
                $promoteSave['gp_branch']       = $post['st_branch'];
                $promoteSave['gp_isleft']       = 0;

                if(!empty($isPromotedYear))
                {
                    $promoteSave['gp_refpromo'] = $isPromotedYear['promo_id'];
                }

                $this->db->insert('hci_stuacademicdetails',$promoteSave);
            }

            // if($process_name=="register")
            // {
            //     if(empty($terms_date))
            //     {
            //         $this->hci_accounts_model->invoicefeesbreakdown($inv_id,date('Y-m-d'));
            //     }
            //     else
            //     {
            //         $this->hci_accounts_model->invoicefeesbreakdown($inv_id,$terms_date['term_sdate']);
            //     }     
            // }
            //discounts - end

            // if($process_name=="register")
            // {
            //     $this->load->model('hci_accounts_model');
            //     $this->hci_accounts_model->erp_integration($inv_id,$save_inv['inv_description']);
            // }

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                $this->msg->set('admission', "Failed to save data. retry");
            }
            else
            {
                $this->db->trans_commit(); 
                $this->msg->set('admission', "success_Saved successfully");
            }
        }

    redirect('hci_studentreg/stureg_view');
}

function load_academic_data()
{
    echo json_encode($this->company_model->load_terms());
}

function load_branch_student()
{
    echo json_encode($this->hci_studentreg_model->load_branch_student());
}

function get_staff_details()
{
    echo json_encode($this->hci_studentreg_model->get_staff_details());
}

function display_prefmethod_view()
{
    echo json_encode($this->hci_studentreg_model->display_prefmethod_view());
}

function load_feestructures()
{
    echo json_encode($this->hci_feestructure_model->load_fee_templates());
}

function confirm_regi()
{
    echo json_encode($this->hci_feestructure_model->confirm_regi());
}

function add_feestructures()
{
    $this->db->distinct('admin_template');
    $this->db->select('admin_template');
    $old_admin = $this->db->get('st_details')->result_array();

    foreach ($old_admin as $admin) 
    {
        switch ($admin['admin_template']) 
        {
        case "SPECIAL#5":
            $adminid = 31;
            break;
        case "FIXED FEES":
            $adminid = 1;
            break;
        case "SPECIAL#2":
            $adminid = 25;
            break;
        case "FS#2":
            $adminid = 3;
            break;
        case "FS#6":
            $adminid = 9;
            break;
        // case "SPECIAL#6":
        //     $adminid = 3;
        //     break;
        case "FS#4":
            $adminid = 5;
            break;
        case "SPECIAL#3":
            $adminid = 27;
            break;
        case "FS#5":
            $adminid = 7;
            break;
        case "SPECIAL#4":
            $adminid = 29;
            break;
        case "Same_Family_#_2-_2012_Sept":
            $adminid = 34;
            break;
        case "FS#7":
            $adminid = 11;
            break;
        case "Same_Family_#_1-_2012_Sept":
            $adminid = 33;
            break;
        case "FS#9":
            $adminid = 17;
            break;
        case "Oxford1":
            $adminid = 21;
            break;
        // case "INT_TRANS_NUG":
        //     $adminid = 3;
        //     break;
        case "FS#8_2014_Sept":
            $adminid = 13;
            break;
        case "FSF#10":
            $adminid = 15;
            break;
        case "FSF#9":
            $adminid = 17;
            break;
        case "Same Family #1":
            $adminid = 33;
            break;
        case "FS# 10":
            $adminid = 19;
            break;
        case "FS#10":
            $adminid = 19;
            break;
        case "SPECIAL#1":
            $adminid = 23;
            break;
        // default:
        //     $adminid = null;
        }

        $this->db->distinct('bill_template');
        $this->db->select('bill_template');
        $this->db->where('admin_template',$admin['admin_template']);
        $old_bill = $this->db->get('st_details')->result_array();

        foreach ($old_bill as $bill) 
        {
            switch ($bill['bill_template']) {
            case "Fixed Fees":
                $billid = 2;
                break;
            case "SPECIAL#2":
                $billid = 26;
                break;
            case "FS#2":
                $billid = 4;
                break;
            case "SPECIAL#5":
                $billid = 32;
                break;
            case "SPECIAL#1":
                $billid = 24;
                break;
            case "FS#4":
                $billid = 6;
                break;
            case "SPECIAL#3":
                $billid = 28;
                break;
            case "FS#5":
                $billid = 8;
                break;
            case "SPECIAL#4":
                $billid = 30;
                break;
            case "FS#6":
                $billid = 10;
                break;
            case "FS#7":
                $billid = 12;
                break;
            case "Oxford T1":
                $billid = 22;
                break;
            case "FS# 8":
                $billid = 20;
                break;
            // default:
            //     $billid = null;
            }

            $this->db->select('es_feemasterid');
            $this->db->where('fee_aftemp',$adminid);
            $this->db->where('fee_tftemp',$billid);
            $fs_id = $this->db->get('hci_feemaster')->row_array();

            if(empty($fs_id))
            {
                $fs_save['fee_description'] = 'OLD STUDENT SPECIAL [ '.$admin['admin_template'].' , '.$bill['bill_template'].' ]';
                $fs_save['fee_status']      = "A";
                $fs_save['fee_effdate']     = date('Y-m-d');
                $fs_save['fee_aftemp']      = $adminid;
                $fs_save['fee_tftemp']      = $billid;
                $fs_save['fee_iscurrent'] = 0;
                $this->db->insert('hci_feemaster',$fs_save);

                $stufs = $this->db->insert_id();
            }
            else
            {
                $stufs = $fs_id['es_feemasterid'];
            }

            $this->db->where('admin_template',$admin['admin_template']);
            $this->db->where('bill_template',$bill['bill_template']);
            $this->db->update('st_details',array('st_feestructure' => $stufs));
        }
    }

}

function add_old_student()
{
    // $this->db->distinct('intake');
    // $this->db->select('intake');
    // // $this->db->where('id >',2061);
    // $this->db->order_by('id');
    // $old_stu = $this->db->get('st_details')->result_array();

    // echo "<pre>";
    // var_dump($old_stu);
    // echo "</pre>";

    // $shiftin = null;
    // $shiftout= null;

    // $punch = null;
    // $prevpunch = null;

    // $savedata['punch'] = $punch;

    // if($prevpunch == 'in')
    // {
    //     if($shiftin>$punch>($shiftin-2))
    //     {
    //         $savedata['type'] = 'in';
    //     }
    //     else
    //     {
    //         $savedata['type'] = 'out';
    //     }
    // }
    // elseif($prevpunch=='out')
    // {
    //     $savedata['type'] = 'in';
    // }
    // else
    // {
    //     if($shiftin>$punch>($shiftin-2))
    //     {
    //         $savedata['type'] = 'in';
    //     }
    //     else
    //     {
    //         $savedata['type'] = 'out';
    //     }
    // }
}

function accy()
{
    // $years = $this->db->get('hgc_academicyears')->result_array();

    //     foreach ($years as $yr) 
    //     {
    //         $this->db->set('st_accyear',$yr['es_ac_year_id']);
    //         $this->db->where('entered_date >=',$yr['ac_startdate']);
    //         $this->db->where('entered_date <=',$yr['ac_enddate']);
    //         $this->db->update('st_details');

    //         // echo "<pre>";
    //         // var_dump("Started date".$yr['ac_startdate']);
    //         // var_dump($stu);
    //         // var_dump("End date".$yr['ac_enddate']);
    //         // echo "</pre>";
    //     }
}

function grade_id_insert()
{
    // ----------------------------Current Grade

    // $this->db->distinct('cur_grade');
    // $this->db->select('cur_grade');
    // $grade = $this->db->get('st_details')->result_array();

    // foreach ($grade as $value) 
    // {
    //     $this->db->set('st_grade',$this->get_grd_id($value['cur_grade']));
    //     $this->db->set('st_curgrade',$this->get_grd_id($value['cur_grade']));
    //     $this->db->where('cur_grade',$value['cur_grade']);
    //     $this->db->update('st_details');
    // }

    // ----------------------------Pre Grade

    // $this->db->distinct('pref_grade');
    // $this->db->select('pref_grade');
    // $grade = $this->db->get('st_details')->result_array();

    // foreach ($grade as $value) 
    // {
    //     $this->db->set('st_prefgrade',$this->get_grd_id($value['pref_grade']));
    //     $this->db->where('pref_grade',$value['pref_grade']);
    //     $this->db->update('st_details');
    // }

    // $this->db->distinct('pref_grade');
    // $this->db->select('pref_grade,st_prefgrade');
    // $newgrd = $this->db->get('st_details')->result_array();

    // print_r($newgrd);
}

function rel_id_insert()
{
    // $this->db->distinct('religion');
    // $this->db->select('religion');
    // $rels = $this->db->get('st_details')->result_array();

    // foreach ($rels as $rel) 
    // {
    //     switch ($rel['religion']) {
    //     case "Christianity":
    //         $relid = 2;
    //         break;
    //     case "Buddhism":
    //         $relid = 1;
    //         break;
    //     case "Roman ":
    //         $relid = 6;
    //         break;
    //     case "Islam":
    //         $relid = 4;
    //         break;
    //     case "Hinduism":
    //         $relid = 5;
    //         break;
    //     case "Roman Catholism":
    //         $relid = 3;
    //         break;
    //     case "Roman Catholicism":
    //         $relid = 3;
    //         break;
    //     default:
    //         $relid = null;
    //     }

    //     $this->db->set('st_religion',$relid);
    //     $this->db->where('religion',$rel['religion']);
    //     $this->db->update('st_details');
    // }
}


function payment_plans()
{
    // $this->db->where('st_status','R');
    // $stus = $this->db->get('st_details')->result_array();

    // foreach ($stus as $s) 
    // {
    //     $pps['spp_customer'] = $s['id'];
    //     $pps['spp_paymentplan'] = 1;
    //     $pps['spp_status'] = 'A';
    //     $pps['spp_reftable'] = 'st_details';

    //     $this->db->insert('hci_cuspaymentplan',$pps);

    //     $pps2['spp_customer'] = $s['id'];
    //     $pps2['spp_paymentplan'] = 3;
    //     $pps2['spp_status'] = 'A';
    //     $pps2['spp_reftable'] = 'st_details';

    //     $this->db->insert('hci_cuspaymentplan',$pps2);

    //     $pps3['spp_customer'] = $s['id'];
    //     $pps3['spp_paymentplan'] = 5;
    //     $pps3['spp_status'] = 'A';
    //     $pps3['spp_reftable'] = 'st_details';

    //     $this->db->insert('hci_cuspaymentplan',$pps3);
    // }
}

function sibs()
{

//     // $this->db->distinct('first_child');
//     $this->db->select('*');
//     // $this->db->where('id >',2061);
//     $old_stu = $this->db->get('st_details')->result_array();

//     $i =1;
//     foreach ($old_stu as $value) 
//     {  
//         if($value['first_child']!='' && $value['first_child']!=null)
//         {
//                 $this->db->select('id');
//                 $this->db->where('st_id',$value['first_child']);
//                 $firchild = $this->db->get('st_details')->row_array();

//                 $this->db->select('id');
//                 $this->db->where('first_child',$value['first_child']);
//                 $this->db->order_by('id');
//                 $siblings_old = $this->db->get('st_details')->result_array();

//                 $sibling_ary = Array($firchild['id']);

//                 foreach ($siblings_old as $sib) 
//                 {
//                     array_push($sibling_ary,$sib["id"]);
//                 }

//                 sort($sibling_ary);

//                 $tot_families = $this->db->count_all('hci_stfamily');

//                 $this->db->where_in('es_admissionid',$sibling_ary);
//                 $family = $this->db->get('hci_sibling')->result_array();

//                 if(empty($family))
//                 {
//                     $this->db->insert('hci_stfamily',array("sf_num"=>($tot_families+1)));
//                     $family_id = $this->db->insert_id();
//                 }
//                 else
//                 {
//                     $family_id = $family[0]['es_family'];
//                 }
                
//                 foreach ($sibling_ary as $key => $ns) 
//                 {
//                     $this->db->where('es_admissionid',$ns);
//                     $sibls = $this->db->get('hci_sibling')->row_array();

//                     $siblins_save['es_family'] = $family_id;
//                     $siblins_save['es_admissionid'] = $ns;
//                     $siblins_save['es_seqnumber'] = $key+1;

//                     if(empty($sibls))
//                     {
//                         $this->db->insert('hci_sibling',$siblins_save);
//                     }
//                 }
            

//             // if(count($sibling_ary)>=2)
//             // {
//             //     echo "<pre>";
//             // var_dump($sibling_ary);
//             // var_dump($i);
//             // echo "</pre>";
//             // $i++;
//             // }
            
//         }
//     }
//     // echo "<pre>";
//     // var_dump($old_stu);
//     // echo "</pre>";
}

function load_sibling_data()
{
    $sibls = $this->input->post('sibs');
    $type = $this->input->post('type');
    $stu = $this->input->post('stu');

    if(!empty($sibls))
    {
        $this->db->select('es_family');
        $this->db->where('es_reftable','st_details');
        $this->db->where('es_admissionid',$sibls[0]);
        $stufamily = $this->db->get('hci_sibling')->row_array();

        if(empty($stufamily))
        {
            $firstsib = $sibls[0];
            $siblings = array($sibls[0]);
            $famseq = 2;
        }
        else
        {
            $this->db->select('*');
            $this->db->where('es_reftable','st_details');
            $this->db->where('es_family',$stufamily['es_family']);
            if(isset($_POST['stu']))
            {
                $this->db->where('es_admissionid !=',$_POST['stu']);
            }
            $sibs = $this->db->get('hci_sibling')->result_array();

            $firstsib = null;
            $famseq = count($sibs)+1;
            $siblings = array();

            foreach ($sibs as $sib) 
            {
                if($sib['es_seqnumber']==1)
                {
                    $firstsib = $sib['es_admissionid'];
                }
                array_push($siblings, $sib['es_admissionid']);
            }
        }

        $this->db->select('*');
        $this->db->where('id',$firstsib);
        $fsdata = $this->db->get('st_details')->row_array();
    }
    else
    {
        $siblings = null;
        $fsdata = null;
    }

    $this->db->select('st_details.st_id,st_details.family_name,st_details.other_names,astemp.ft_name as admintemp,tftemp.ft_name as termtemp');
    $this->db->join('hci_feetemplate as astemp','astemp.ft_id = st_details.st_admintemplate');
    $this->db->join('hci_feetemplate as tftemp','tftemp.ft_id = st_details.st_termtemplate');
    $this->db->where_in('st_details.id',$siblings);
    $sibsfsdata = $this->db->get('st_details')->result_array();

    $all = array(
        'siblings' => $siblings,
        'fsdata' => $fsdata,
        'sibsfsdata' => $sibsfsdata
        );
    
    echo json_encode($all);
}

function get_grd_id($grd)
{

switch ($grd) {
    case "LK":
        $grade = 17;
        break;
    case "NR":
        $grade = 2;
        break;
    case "UK":
        $grade = 18;
        break;
    case "G5":
        $grade = 8;
        break;
    case "PG":
        $grade = 1;
        break;
    case "G1":
        $grade = 4;
        break;
    case "G4":
        $grade = 7;
        break;
    case "G3":
        $grade = 6;
        break;
    case "G2":
        $grade = 5;
        break;
    case "G6":
        $grade = 9;
        break;
    case "G7":
        $grade = 10;
        break;
    case "G8":
        $grade = 11;
        break;
    case "LKG":
        $grade = 17;
        break;
    case "Nursurey":
        $grade = 2;
        break;
    case "Nursery":
        $grade = 2;
        break;
    case "grade 4":
        $grade = 7;
        break;
    case "PLAY GROUP":
        $grade = 1;
        break;
    case "GRADE 1":
        $grade = 4;
        break;
    case "PLAYGROUP":
        $grade = 1;
        break;
    case "GRADE 7":
        $grade = 10;
        break;
    case "7":
        $grade = 10;
        break;
    case "GRADE 5":
        $grade = 8;
        break;
    case "LOWER KINDERGARTEN":
        $grade = 17;
        break;
    case "UKG":
        $grade = 18;
        break;
    case "GRADE 3":
        $grade = 6;
        break;
    case "GRADE 6":
        $grade = 9;
        break;
    case "GRADE 8":
        $grade = 11;
        break;
    case "GRADE 2":
        $grade = 5;
        break;
    case "GRADE1":
        $grade = 4;
        break;
    case "GRADE5":
        $grade = 8;
        break;
    case "GR 2":
        $grade = 5;
        break;
    case "P":
        $grade = 1;
        break;
    case "Upper Kindergarten":
        $grade = 18;
        break;
    case "YEAR 4":
        $grade = 7;
        break;
    case "Y4":
        $grade = 7;
        break;
    case "YEAR 1":
        $grade = 4;
        break;
    case "Y1":
        $grade = 4;
        break;
    case "GRADE7":
        $grade = 10;
        break;
    case "YEAR 3":
        $grade = 6;
        break;
    case "Y3":
        $grade = 6;
        break;
    case "YEAR 5":
        $grade = 8;
        break;
    case "Y5":
        $grade = 8;
        break;
    case "KINDERGARTEN":
        $grade = 3;
        break;
    case "YEAR 7":
        $grade = 10;
        break;
    case "Y7":
        $grade = 10;
        break;
    case "YEAR 9":
        $grade = 12;
        break;
    case "Y9":
        $grade = 12;
        break;
    case "YEAR 10":
        $grade = 13;
        break;
    case "Y10":
        $grade = 13;
        break;
    case "YEAR 8":
        $grade = 11;
        break;
    case "Y8":
        $grade = 11;
        break;
    case "YEAR 6":
        $grade = 9;
        break;
    case "Y6":
        $grade = 9;
        break;
    case "YEAR 2":
        $grade = 5;
        break;
    case "Y2":
        $grade = 5;
        break;
    case "NUR":
        $grade = 2;
        break;
    case "YEAR 12":
        $grade = 15;
        break;
    case "Y12":
        $grade = 15;
        break;
    case "YEAR 11":
        $grade = 14;
        break;
    case "Y11":
        $grade = 14;
        break;
    case "KG":
        $grade = 3;
        break;
    case "KIDERGARTEN":
        $grade = 3;
        break;
    case "NURSARY":
        $grade = 2;
        break;
    case "YEAR 06":
        $grade = 9;
        break;
    case "YEAAR 12":
        $grade = 15;
        break;
    case "YEAR12":
        $grade = 15;
        break;
    case "YEAR 03":
        $grade = 6;
        break;
    case "YEAR 01":
        $grade = 4;
        break;
    case "YEAR 02":
        $grade = 5;
        break;
    case "YEAR 04":
        $grade = 7;
        break;
    case "YEAR 05":
        $grade = 8;
        break;
    case "YEAR 08":
        $grade = 11;
        break;
    case "YEAR04":
        $grade = 7;
        break;
    case "YEAR01":
        $grade = 4;
        break;
    case "YEAR05":
        $grade = 8;
        break;
    case "YEAR2":
        $grade = 5;
        break;
    case "YEAR02":
        $grade = 5;
        break;
    case "YEAR03":
        $grade = 6;
        break;
    case "YEAR07":
        $grade = 10;
        break;
    case "YEAR08":
        $grade = 11;
        break;
    case "Y13":
        $grade = 16;
        break;
    default:
        $grade = null;
}

    return $grade;
}

function promote_for_startyear()
{
    // $this->db->where('grd_status','A');
    // $grades = $this->db->get('hci_grade')->result_array();

    // $this->db->where('br_group',1);
    // $branches = $this->db->get('hgc_branch')->result_array();

    // $this->db->where('ac_iscurryear',1);
    // $acc = $this->db->get('hgc_academicyears')->row_array();

    // foreach ($branches as $branch) 
    // {
    //     foreach ($grades as $grd) 
    //     {
    //         $promo['promo_fromaccyear'] = 0;
    //         $promo['promo_accyear']     = $acc['es_ac_year_id'];
    //         $promo['promo_grade']       = $grd['grd_id'];
    //         $promo['promo_branch']      = $branch['br_id'];

    //         $this->db->insert('hci_gradepromotion',$promo);

    //         $promoid = $this->db->insert_id();

    //         $this->db->where('st_branch',$branch['br_id']);
    //         $this->db->where('st_grade',$grd['grd_id']);
    //         $this->db->where('st_status','R');
    //         $stulist = $this->db->get('st_details')->result_array();

    //         foreach ($stulist as $stu) 
    //         {
    //             $this->db->where('id',$stu['id']);
    //             $this->db->update('st_details',array('st_lastpromoted' => $acc['es_ac_year_id']));

    //             $promote['gp_refpromo']     = $promoid;
    //             $promote['gp_ayear']        = $acc['es_ac_year_id'];
    //             $promote['gp_student']      = $stu['id'];
    //             $promote['gp_currgrade']    = $grd['grd_id'];
    //             $promote['gp_promotedfrom'] = 0;
    //             $promote['gp_branch']       = $branch['br_id'];
    //             $promote['gp_isleft']       = 0;

    //             $this->db->insert('hci_stuacademicdetails',$promote);
    //         }
    //     }
    // }
}
}