<?php

class Hci_grade extends CI_controller {

function hci_grade() {
	parent::__construct();
	$this->load->model('hci_grade_model');
	$this->load->model('company_model');
}

function index() 
{
	$data['grd_info']  = $this->hci_grade_model->get_grd_info();

	$data['main_content'] = 'hci_grade_view';
	$data['title'] = 'GRADE';
	$this->load->view('includes/template',$data);
}

function save_grade()
{
	$save_grade = $this->hci_grade_model->save_grade();

	if($save_grade)
	{
		$this->session->set_flashdata('flashSuccess', 'Grade saved successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to save grade. Retry.');
	}

	redirect('hci_grade');
}

function remove_grade()
{
	echo json_encode($this->hci_grade_model->remove_grade());
}

function change_status()
{
	echo json_encode($this->hci_grade_model->change_status());
}

function classmanage_view()
{
	$data['acc_years'] = $this->company_model->get_ay_info();
	$data['grd_info']  = $this->hci_grade_model->get_grd_info();
	$data['cls_info']  = $this->hci_grade_model->get_classes_list('createview');
	$data['assignInfo']= $this->hci_grade_model->get_assigned_list();

	$data['main_content'] = 'hci_classmanage_view';
	$data['title'] = 'CLASS';
	$this->load->view('includes/template',$data);
}

function save_class()
{
	$save_class = $this->hci_grade_model->save_class();

	if($save_class)
	{
		$this->session->set_flashdata('flashSuccess', 'Class saved successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to save class. Retry.');
	}

	redirect('hci_grade/classmanage_view?tab_id=create_tab');
}

function changeclass_status()
{
	echo json_encode($this->hci_grade_model->changeclass_status());
}

function load_year_classlist()
{
	echo json_encode($this->hci_grade_model->load_year_classlist());
}

function assign_class()
{
	$assign_class = $this->hci_grade_model->assign_class();

	if($assign_class)
	{
		$this->session->set_flashdata('flashSuccess', 'Classes assigned successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to assign classes. Retry.');
	}

	redirect('hci_grade/classmanage_view?tab_id=assign_tab');
}

function load_class_list()
{
	echo json_encode($this->hci_grade_model->load_class_list());
}

}