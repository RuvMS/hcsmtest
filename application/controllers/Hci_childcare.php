<?php

class Hci_childcare extends CI_controller {

function hci_childcare() {
	parent::__construct();
	$this->load->model('hci_childcare_model');
	//$this->load->model('hci_studentreg_model');
}

function index() 
{
	// $data['comp_info'] = $this->company_model->get_comp_info();
	// $data['grp_info']  = $this->company_model->get_grp_info();
	// $data['fy_info']  = $this->company_model->get_fy_info();
	// $data['ay_info']  = $this->company_model->get_ay_info();

	// $data['main_content'] = 'company_view';
	// $data['title'] = 'COMPANY';
	// $this->load->view('includes/template',$data);
}

function manage_routes_view()
{
	$data['routes'] = $this->hci_transport_model->get_routes();
	$data['main_content'] = 'hci_manageroutes_view';
	$data['title'] = 'TRANSPORT ROUTE';
	$this->load->view('includes/template',$data);
}

function save_route()
{
	$save_route = $this->hci_transport_model->save_route();

	if($save_route)
	{
		$this->session->set_flashdata('flashSuccess', 'Saved successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to save. Retry.');
	}

	redirect('hci_transport/manage_routes_view');
}

function manage_pickingpoints()
{
	$data['picks'] = $this->hci_transport_model->get_picking_points();
	$data['routes'] = $this->hci_transport_model->get_routes();
	$data['main_content'] = 'hci_pickingpoints_view';
	$data['title'] = 'TRANSPORT ROUTE';
	$this->load->view('includes/template',$data);
}

function save_pickpoint()
{
	$save_pick = $this->hci_transport_model->save_pickpoint();

	if($save_pick)
	{
		$this->session->set_flashdata('flashSuccess', 'Saved successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to save. Retry.');
	}

	redirect('hci_transport/manage_pickingpoints');
}

function cc_fee_structure()
{
	// $data['picks'] = $this->hci_transport_model->get_picking_points();
	// $data['routes'] = $this->hci_transport_model->get_routes();
	$data['main_content'] = 'hci_ccfeestruc_view';
	$data['title'] = 'TRANSPORT FEE STRUCTURE';
	$this->load->view('includes/template',$data);
}

function save_feestructure()
{
	$save_fs = $this->hci_childcare_model->save_feestructure();

	if($save_fs)
	{
		$this->session->set_flashdata('flashSuccess', 'Saved successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to save. Retry.');
	}

	redirect('hci_childcare/cc_fee_structure');
}

function transport_registration()
{
	$data['stus'] = $this->hci_studentreg_model->registeed_Stu();
	$data['fss'] = $this->hci_transport_model->get_feestructure();
	$data['main_content'] = 'hci_transregi_view';
	$data['title'] = 'TRANSPORT FEE STRUCTUREREGISTRATION';
	$this->load->view('includes/template',$data);
}

function registration_trans()
{
	$regi = $this->hci_transport_model->save_registration();

	if($regi)
	{
		$this->session->set_flashdata('flashSuccess', 'Saved successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to save. Retry.');
	}

	redirect('hci_transport/transport_feestructure');
}
}