<div class="row">
    <div class="col-md-12">
        <h3 class="page-header"><i class="fa fa-users"></i> INQUIRY FORM</h3>
        <ol class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
            <li><i class="fa fa-cog"></i>Settings</li>
            <li><i class="fa fa-users"></i>Inquiry</li>
        </ol>
    </div>
</div>

<div>
<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#group_tab" aria-controls="group_tab" role="tab" data-toggle="tab">Inquiry Lookup</a></li>
    <li role="presentation"><a href="#user_tab" aria-controls="user_tab" role="tab" data-toggle="tab" id="info_tab">Student Information </a></li>
</ul>
<!-- Tab panes -->
<div class="tab-content">
    <div role="tabpanel" class="tab-pane" id="user_tab">
        <br>
        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" role="form" method="post"  id="comp_form" action="<?php echo base_url('hci_admission/new_admission')?>" autocomplete="off" novalidate>
                <section class="panel">
                    <header class="panel-heading">
                       Student Information
                    </header>
                    <div class="panel-body">
                        <?php //echo $this->msg->show('admission') ?>
                        <input type="hidden" id="es_enquiryid" name="post[es_enquiryid]" value="<?php echo $this->hci_admission_model->show_val('es_enquiryid'); ?>">
                        <div class="form-group">
                            <label for="eq_branch" class="col-md-3 control-label">Branch</label>
                            <div class="col-md-6">
                                <?php 
                                    global $branchdrop;
                            
                                    if($this->hci_admission_model->show_val('es_enquiryid'))
                                    {
                                        $selectedbr = $this->hci_admission_model->show_val('eq_branch');
                                    }
                                    else
                                    {
                                        global $selectedbr;
                                    }

                                    $extraattrs = 'id="eq_branch" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" ';
                                    echo form_dropdown('post[eq_branch]',$branchdrop,$selectedbr, $extraattrs); 
                                ?>
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-md-3 control-label">First Name Of Child</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="fname" name="post[eq_name]" placeholder="" value="<?php echo $this->hci_admission_model->show_val('eq_name')?>">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="brnum" class="col-md-3 control-label">Last Name Of Child</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="lname" name="post[eq_lastname]" placeholder="" value="<?php echo $this->hci_admission_model->show_val('eq_lastname')?>">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <?php
                        $ss = $this->hci_admission_model->show_val('eq_sex');
                        $selected_gender1 =  'unchecked';
                        $selected_gender2 =  'unchecked';
                        if($ss == 'M')
                        {
                        $selected_gender1 =  'checked="checked"';
                        }elseif($ss == 'F')
                        {
                        $selected_gender2 =  'checked="checked"';
                        }
                        ?>
                        <div class="form-group">
                            <label for="comcode" class="col-md-3 control-label">Gender Of Child</label>
                                <div class="col-md-9">
                                    <label class="col-md-2 control-label">Male</label>
                                        <input type="radio" name="post[eq_sex]" data-validation="required" class="col-md-1" id="gender" value="M" <?php echo  $selected_gender1 ?>>

                                    <label class="col-md-2 control-label">Female</label>
                                        <input type="radio" name="post[eq_sex]" id="gender" data-validation="required" class="col-md-1" value="F" <?php echo   $selected_gender2 ?>>
                                </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Date Of Birth</label>
                            <div class="col-md-6">
                                <div id="as_date_div" class="input-group date">
                                    <input class="form-control" data-validation="required" data-validation-error-msg-required="Field is empty" type="text" name="post[eq_dob]" id="birthday" value="<?php echo $this->hci_admission_model->show_val('eq_dob')?>"  data-format="YYYY-MM-DD">
                                    <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="telephone" class="col-md-3 control-label">Current Grade/Level</label>
                            <div class="col-md-6">
                                <select class="form-control" name="post[eq_class]"  style="width: 100%;">
                                <option value="">---Not Applicable---</option>
                                    <?php
                                    $this->db->where('grd_status','A');
                                    $grades = $this->db->get('hci_grade')->result_array();
                                    foreach($grades as $row):
                                        ?>
                                        <option value="<?php echo $row['grd_id'] ?>">
                                            <?php echo $row['grd_name'];?>
                                        </option>
                                        <?php
                                    endforeach;
                                    ?>

                                </select>                                        <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fax" class="col-md-3 control-label">Current College / School </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="post[eq_prev_university]" value="<?php echo $this->hci_admission_model->show_val('eq_prev_university')?>">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fax" class="col-md-3 control-label">Expected Grade</label>
                            <div class="col-md-6">
                                <select class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" name="post[pre_class]"  style="width: 100%;">
                                <option value="">---Select Expected Grade----</option>
                                    <?php
                                    $this->db->where('grd_status','A');
                                    $students = $this->db->get('hci_grade')->result_array();
                                    foreach($students as $row):
                                        ?>
                                        <option value="<?php echo $row['grd_id'] ?>">
                                            <?php echo $row['grd_name'];?>
                                        </option>
                                        <?php
                                    endforeach;
                                    ?>

                                </select>
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <label for="eq_intake" class="col-md-3 control-label">Expected Intake</label>
                            <div class="col-md-4">
                                <select class="form-control"  name="post[eq_intake]"  style="width: 100%;">
                                    <option value=""></option>
                                    <?php
                                    $sel_intake = $this->hci_admission_model->show_val('eq_intake');
                                    $this->db->select('hci_intake.*');
                                    $this->db->join('hgc_academicyears','hgc_academicyears.es_ac_year_id=hci_intake.int_acyear');
                                    $this->db->where('hgc_academicyears.ac_iscurryear',1);
                                    $intakes = $this->db->get('hci_intake')->result_array();
                                    foreach($intakes as $row):
                                         $sel_text = '';
                                        if($sel_intake==$row['int_id'])
                                        {
                                            $sel_text = 'selected';
                                        }
                                        ?>
                                        <option value="<?php echo $row['int_id'] ?>" <?php echo $sel_text;?>>
                                            <?php echo $row['int_name'];?>
                                        </option>
                                        <?php
                                    endforeach;
                                    ?>

                                </select>
                            </div>
                        </div> -->
                        <div class="form-group">
                            <label for="eq_intake" class="col-md-3 control-label">Expected Intake</label>
                            <div class="col-md-4">
                                <select class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" name="post[eq_intake]" style="width: 100%;">
                                    <option value=""></option>
                                    <option value="1">September</option>
                                    <option value="2">January</option>
                                    <!-- <option value="3">May</option> -->
                                </select>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="panel">
                    <header class="panel-heading">
                       Details of Parent(Father)/Guardian
                    </header>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="fax" class="col-md-3 control-label">Full Name</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="fathername" name="post[eq_fathername]" value="<?php echo $this->hci_admission_model->show_val('eq_fathername')?>">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fax" class="col-md-3 control-label">Permanent Address</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="faddress1" onkeyup="$('#maddress1').val(this.value)" name="post[eq_address]" placeholder="Address Line 1" value="<?php echo $this->hci_admission_model->show_val('eq_address')?>">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fax" class="col-md-3 control-label"></label>
                            <div class="col-md-8">
                                <input type="text" onkeyup="$('#maddress2').val(this.value)" class="form-control" id="faddress2" name="post[eq_fatheraddress2]" placeholder="Address Line 2" value="<?php echo $this->hci_admission_model->show_val('eq_fatheraddress2')?>">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fax" class="col-md-3 control-label"></label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="faddress3" onkeyup="$('#maddress3').val(this.value)" name="post[eq_city]" placeholder="City"   value="<?php echo $this->hci_admission_model->show_val('eq_city')?>">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fax" class="col-md-3 control-label">Occupation</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="fwork" name="post[eq_fathersoccupation]" value="<?php echo $this->hci_admission_model->show_val('eq_fathersoccupation')?>">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fax" class="col-md-3 control-label">Place Of Work</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="fwork" name="post[eq_fathersplaceofwork]" value="<?php echo $this->hci_admission_model->show_val('eq_fathersplaceofwork')?>">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fax" class="col-md-3 control-label">Email Address</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" data-validation="email" data-validation-error-msg-email="Give Valid Email Address"  data-validation-optional="true" id="email" name="post[eq_emailid]" value="<?php echo $this->hci_admission_model->show_val('eq_emailid')?>">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fax" class="col-md-3 control-label">Home Number</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" data-validation="required number length" data-validation-length="10-10" data-validation-error-msg-number="Invalid. Please Try Again. ex: 0111234567" data-validation-error-msg-length="Must be 10 characters long" data-validation-error-msg-required="Field can not be empty" id="fmobile" name="post[eq_phno]" value="<?php echo $this->hci_admission_model->show_val('eq_phno')?>">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fax" class="col-md-3 control-label">Mobile Number</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" data-validation="required number length" data-validation-length="10-10" data-validation-error-msg-number="Invalid. Please Try Again. ex: 0775555555" data-validation-error-msg-length="Must be 10 characters long" data-validation-error-msg-required="Field can not be empty" id="fmobile" name="post[eq_mobile]" value="<?php echo $this->hci_admission_model->show_val('eq_mobile')?>">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fax" class="col-md-3 control-label">Alt. Number</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" data-validation="number length" data-validation-length="10-10" data-validation-error-msg-number="Invalid. Please Try Again. ex: 0111234567" data-validation-error-msg-length="Must be 10 characters long"  data-validation-optional="true" id="fmobile" name="post[eq_altno]" value="<?php echo $this->hci_admission_model->show_val('eq_altno')?>">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fax" class="col-md-3 control-label">Foreign Number</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="fmobile" name="post[eq_foreignnum1]" value="<?php echo $this->hci_admission_model->show_val('eq_foreignnum1')?>">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                    </div>
                </section>
                <section class="panel">
                    <header class="panel-heading">
                       Details of Parent(Mother)/Guardian
                    </header>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="fax" class="col-md-3 control-label">Full Name</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="mname" name="post[eq_mothername]" value="<?php echo $this->hci_admission_model->show_val('eq_mothername')?>">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fax" class="col-md-3 control-label">Permanent Address</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="maddress1" name="post[eq_motheraddress1]" placeholder="Address Line 1" value="<?php echo $this->hci_admission_model->show_val('eq_motheraddress1')?>">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fax" class="col-md-3 control-label"></label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="maddress2" name="post[eq_motheraddress2]" placeholder="Address Line 2" value="<?php echo $this->hci_admission_model->show_val('eq_motheraddress2')?>">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fax" class="col-md-3 control-label"></label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="maddress3" name="post[eq_mothercity]" placeholder="City" data-validation="required" value="<?php echo $this->hci_admission_model->show_val('eq_mothercity')?>">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fax" class="col-md-3 control-label">Occupation</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="mwork" name="post[eq_motheroccupation]" value="<?php echo $this->hci_admission_model->show_val('eq_motheroccupation')?>">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fax" class="col-md-3 control-label">Place Of Work</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="mwork" name="post[eq_motherplaceofwork]" value="<?php echo $this->hci_admission_model->show_val('eq_motherplaceofwork')?>">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="eq_emailid_2" class="col-md-3 control-label">Email Address</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" data-validation="email" data-validation-error-msg-email="Give Valid Email Address"  data-validation-optional="true" id="email" name="post[eq_emailid_2]" value="<?php echo $this->hci_admission_model->show_val('eq_emailid_2')?>">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <label for="fax" class="col-md-3 control-label">Contact Number</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" data-validation="required" 
                                data pattern="/^[0]-[1-9]{2}-[0-9]{3}-[0-9]{4}$/" 
                                data-validation-error-msg-required="Field can not be empty" id="mmobile" name="post[eq_mothermobile]"  value="<?php echo $this->hci_admission_model->show_val('eq_mothermobile')?>">
                                <p class="help-block">Example block-level help text here.</p>
                            </div>
                        </div> -->
                        <div class="form-group">
                            <label for="fax" class="col-md-3 control-label">Home Number</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" data-validation="required number length" data-validation-length="10-10" data-validation-error-msg-number="Invalid. Please Try Again. ex: 0111234567" data-validation-error-msg-length="Must be 10 characters long" data-validation-error-msg-required="Field can not be empty" id="fmobile" name="post[eq_phno2]" value="<?php echo $this->hci_admission_model->show_val('eq_phno2')?>">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fax" class="col-md-3 control-label">Mobile Number</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" data-validation="required number length" data-validation-length="10-10" data-validation-error-msg-number="Invalid. Please Try Again. ex: 0111234567" data-validation-error-msg-length="Must be 10 characters long" data-validation-error-msg-required="Field can not be empty" id="fmobile" name="post[eq_mothermobile]" value="<?php echo $this->hci_admission_model->show_val('eq_mothermobile')?>">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fax" class="col-md-3 control-label">Alt. Number</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" data-validation="number length" data-validation-length="10-10" data-validation-error-msg-number="Invalid. Please Try Again. ex: 0111234567" data-validation-error-msg-length="Must be 10 characters long"  data-validation-optional="true" id="fmobile" name="post[eq_motheraltno]" value="<?php echo $this->hci_admission_model->show_val('eq_motheraltno')?>">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fax" class="col-md-3 control-label">Foreign Number</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="fmobile" name="post[eq_foreignnum2]" value="<?php echo $this->hci_admission_model->show_val('eq_foreignnum2')?>">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                    </div>
                </section>
                <section class="panel">
                    <header class="panel-heading">
                       If parents are divorced or separated, details of the person who has legal custody of the child
                    </header>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="fax" class="col-md-3 control-label">Full Name</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="pname" name="post[eq_personname]" value="<?php echo $this->hci_admission_model->show_val('eq_personname')?>">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fax" class="col-md-3 control-label">Permanent Address</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="paddress1" name="post[eq_personaddress1]" placeholder="Address Line 1" value="<?php echo $this->hci_admission_model->show_val('eq_personaddress1')?>">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fax" class="col-md-3 control-label"></label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="paddress2" name="post[eq_personaddress2]" placeholder="Address Line 2" value="<?php echo $this->hci_admission_model->show_val('eq_personaddress2')?>">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fax" class="col-md-3 control-label"></label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="paddress3" name="post[eq_personcity]" placeholder="City" value="<?php echo $this->hci_admission_model->show_val('eq_personcity')?>">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fax" class="col-md-3 control-label">Occupation</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="pwork" name="post[eq_personoccupation]" value="<?php echo $this->hci_admission_model->show_val('eq_personoccupation')?>">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fax" class="col-md-3 control-label">Place Of Work</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="pwork" name="post[eq_personplaceofwork]" value="<?php echo $this->hci_admission_model->show_val('eq_personplaceofwork')?>">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="eq_emailid_3" class="col-md-3 control-label">Email Address</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="email" name="post[eq_emailid_3]" value="<?php echo $this->hci_admission_model->show_val('eq_emailid_3')?>">
                                <!-- <input type="text" class="form-control" id="email" name="post[eq_emailid_3]" required pattern="[a-z][@].[com]" value="<?php echo $this->hci_admission_model->show_val('eq_emailid_3')?>"  data-validation="email" data-validation-error-msg-email="Give Valid Email Address"  data-validation-optional="true"> -->
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fax" class="col-md-3 control-label">Contact Number</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="pmobile" name="post[eq_personmobile]" value="<?php echo $this->hci_admission_model->show_val('eq_personmobile')?>">
                                <!-- <input type="text" data-validation="number length" data-validation-length="10-10" data-validation-error-msg-number="Invalid. Please Try Again. ex: 0111234567" data-validation-error-msg-length="Must be 10 characters long" data-validation-optional="true" class="form-control" id="pmobile" name="post[eq_personmobile]" value="<?php echo $this->hci_admission_model->show_val('eq_personmobile')?>"> -->
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                    </div>
                </section>
                <section class="panel">
                    <header class="panel-heading">
                       Medical Background of the Child
                    </header>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="fax" class="col-md-4 control-label"><h>Has you child diagnosed for any learning disabilities ? If yes please describe.</h></label>
                            <div class="col-md-7">
                                <textarea  class="form-control" id="diagnosedreason" name="post[eq_physical_details1]"><?php echo $this->hci_admission_model->show_val('eq_physical_details1')?></textarea>
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="physicaldisabilities" class="col-md-4 control-label">Does the child have any physical disabilities ? If yes please describe.</label>
                            <div class="col-md-7">
                                <textarea  class="form-control" id="diagnosedreason1" name="post[physicaldisabilities]"><?php echo $this->hci_admission_model->show_val('physicaldisabilities')?></textarea>
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="allergicinfo" class="col-md-4 control-label">Is your child allergic to any medicine, food or beverage etc. ? If yes please provide details.</label>
                            <div class="col-md-7">
                                <textarea  class="form-control" id="diagnosedreason1" name="post[allergicinfo]"><?php echo $this->hci_admission_model->show_val('allergicinfo')?></textarea>
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="underdoctorscare" class="col-md-4 control-label">Is your child currently under a doctor's care ? If yes please provide reason.</label>
                            <div class="col-md-7">
                                <textarea  class="form-control" id="diagnosedreason1" name="post[underdoctorscare]" ><?php echo $this->hci_admission_model->show_val('underdoctorscare')?></textarea>
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                    </div>
                </section>
                <section class="panel">
                    <div class="panel-body">
                        <br>
                        <div class="form-group">
                            <label for="knowabt" class="col-md-3 control-label">How did you know about us</label>
                            <div class="col-md-9 typeahead-wrapper" id="knowabtlbl">
                                <select id="knowabt" name="knowabt" class="form-control">
                                    <option value=""></option>
                                    <?php
                                        $selectedmethod = $this->hci_admission_model->show_val('knowabt');

                                        foreach ($pop_method as $method) 
                                        {
                                            if($selectedmethod==$method['pm_id'])
                                            {
                                                $sel_txt = 'selected';
                                            }
                                            else
                                            {
                                                $sel_txt = '';
                                            }

                                            echo "<option value='".$method['pm_id']."' ".$sel_txt.">".$method['pm_method']."</option>";
                                        }
                                    ?>
                                </select>
                                <!-- <input type="text" class="typeahead form-control" id="knowabt" name="post[knowabt]" 
                                    value="<?php echo $this->hci_admission_model->show_val('knowabt')?>"> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="new_method" class="col-md-3 control-label">If other mention the method</label>
                            <div class="col-md-9 typeahead-wrapper">
                                <input type="text" class="typeahead form-control" id="new_method" name="new_method">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inqremarks" class="col-md-3 control-label">Remarks</label>
                            <div class="col-md-7">
                                <textarea  class="form-control" id="inqremarks" name="post[inqremarks]" ><?php echo $this->hci_admission_model->show_val('inqremarks')?></textarea>
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-11">
                                <button type="submit" class="btn btn-info">Save</button>
                                <a href="<?php echo  site_url("hci_admission/new_admission") ?>" class=" btn btn-default "> Reset </a>
                            </div>
                        </div>
                    </div>
                </section>
                </form>
            </div>
        </div>
    </div>
    <div role="tabpanel" class="tab-pane active" id="group_tab">
        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading">
                        Inquiry List
                    </header>
                    <table id='inqlist' class="table table-striped table-advance table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Full Name</th>
                                <th>Parents Name</th>
                                <th>Contact No. </th>
                                <th>City</th>
                                <th>Create Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $i = 0;
                                foreach ( $stu_info as $new_row )
                                {
                                    $i++;
                            ?>
                                <tr>
                                    <td><?php echo $i;?></td>
                                    <td><?php echo $new_row['eq_name'].' '.$new_row['eq_lastname']  ?></td>
                                    <td><?php echo $new_row['eq_fathername']?></td>
                                    <td><?php echo $new_row['eq_phno'].' '.$new_row['eq_mobile']?></td>
                                    <td><?php echo $new_row['eq_city']?></td>
                                    <td><?php echo $new_row['eq_createdon']?></td>
                                    <td>
                                        <button type='button' class='btn btn-info btn-xs' data-toggle='modal' data-target='#myModal' onclick='event.preventDefault();view_stuinfo("<?php echo $new_row['es_enquiryid']?>")'>view</button> | 
                                        <?php 
                                            $rslt =  $new_row['st_enqid'];
                                            if($rslt == NULL) 
                                            {
                                                ?>
                                                <a href="<?php echo  site_url("hci_admission/inquary_view/" . $new_row['es_enquiryid']) ?>"
                                           class=" btn btn-success btn-xs "> <span class='glyphicon glyphicon-pencil' aria-hidden='true'></span> </a> |
                                           <?php
                                            echo "<a class='btn btn-danger btn-xs' onclick='event.preventDefault();delete_admition
                                         (".$new_row['es_enquiryid'].")'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></a> | ";
                                        ?>
                                            <a href="<?php echo  site_url("hci_studentreg/stureg_view/" . $new_row['es_enquiryid']) ?>"
                                               class=" btn btn-warning btn-xs"> Register </a>
                                        <?php
                                            }
                                        ?>
                                    </td>
                                </tr>
                            <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </section>
            </div>
        </div>
    </div>
</div>
</div>
<!-- Modal Invoice-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Student : <span id="stuname_span"></span></h4>
                <h4 align="right"> Created Date : <span id="stu_create"></span></h4>
            </div>
            <div class="modal-body">
                <center>
                    <table border="1">
                        <tr><font color="green">Student Info.</font></tr>
                        <tr>
                            <td width="120" bgcolor="#DED9E8">Student Name : </td><td width="250"><span id="student_fname"></span> <span id="student_lname"></span></td>
                        </tr>
                        <tr>
                            <td width="120" bgcolor="#DED9E8">Birthday : </td><td width="250"><span id="student_dob"></span></td>
                        </tr>
                        <tr>
                            <td width="120" bgcolor="#DED9E8">Gender : </td><td width="250"><span id="stu_sex"></span></td>
                        </tr>
                        <tr>
                            <td width="120" bgcolor="#DED9E8">Student Address : </td><td width="250"><span id="student_address"></span></td>
                        </tr>
                        <tr>
                            <td width="120" bgcolor="#DED9E8">Privious School : </td><td width="250"><span id="stu_uni"></span></td>
                        </tr>
                        <tr>
                            <td width="120" bgcolor="#DED9E8">Telephone No : </td><td width="250"><span id="stu_home"></span></td>
                        </tr>
                    </table>
                    <table><tr><td height="20"></td></tr></table>
                    <table border="1">
                        <tr><font color="green">Father or Guardian Info.</font></tr>
                        <tr>
                            <td width="120" bgcolor="#DED9E8">Name : </td><td width="250"><span id="fa_name"></span></td>
                        </tr>
                        <tr>
                            <td width="120" bgcolor="#DED9E8">Address : </td><td width="250"><span id="fa_address"></span></td>
                        </tr>
                        <tr>
                            <td width="120" bgcolor="#DED9E8">City : </td><td width="250"><span id="stu_city"></span></td> 
                        </tr>
                        <tr>
                            <td width="120" bgcolor="#DED9E8">Mobile No : </td><td width="250"><span id="stu_mobi"></span></td>
                        </tr>
                        <tr>
                            <td width="120" bgcolor="#DED9E8">Occupation : </td><td width="250"><span id="fa_occu"></span></td>
                        </tr> 
                        <tr>
                            <td width="120" bgcolor="#DED9E8">Email Address : </td><td width="250"><span id="stu_email"></span></td>
                        </tr>
                        <tr>
                            <td width="120" bgcolor="#DED9E8">Foreign Number : </td><td width="250"><span id="fa_fore"></span></td>
                        </tr>
                    </table>
                    <table><tr><td height="20"></td></tr></table>
                    <table border="1">
                        <tr><font color="green">Mother or Guardian Info.</font></tr>
                        <tr>
                            <td width="120" bgcolor="#DED9E8">Name : </td><td width="250"><span id="stu_mom"></span></td>
                        </tr>
                        <tr>
                            <td width="120" bgcolor="#DED9E8">Address : </td><td width="250"><span id="stu_momadd1"></span> <span id="stu_momadd2"></span></td>
                        </tr>
                        <tr>
                            <td width="120" bgcolor="#DED9E8">City : </td><td width="250"><span id="stu_mom_city"></span></td>
                        </tr>
                        <tr>
                            <td width="120" bgcolor="#DED9E8">Occupation : </td><td width="250"><span id="mom_occu"></span></td>
                        </tr>
                        <tr>
                            <td width="120" bgcolor="#DED9E8">Mobile_No : </td><td width="250"><span id="mom_occu"></span></td>
                        </tr>
                        <tr>
                            <td width="120" bgcolor="#DED9E8">Email Address : </td><td width="250"><span id="mom_email"></span></td>
                        </tr>
                        <tr>
                            <td width="120" bgcolor="#DED9E8">Foreign Number : </td><td width="250"><span id="mom_fore"></span></td>
                        </tr>
                    </table>
                    <table><tr><td height="20"></td></tr></table>
                    <table border="1">
                        <tr><font color="green">Emergency contact Person Details</font></tr>
                        <tr>
                            <td width="120" bgcolor="#DED9E8">Person Name : </td><td width="250"><span id="person_name"></span></td>
                        </tr>
                        <tr>
                            <td width="120" bgcolor="#DED9E8">Address : </td><td width="250"><span id="person_add1"></span> <span id="person_add2"></span></td>
                        </tr>
                        <tr>
                            <td width="120" bgcolor="#DED9E8">City : </td><td width="250"><span id="person_city"></span></td>
                        </tr>
                        <tr>
                            <td width="120" bgcolor="#DED9E8">Mobile No : </td><td width="250"><span id="person_mobi"></span></td>
                        </tr>
                        <tr>
                            <td width="120" bgcolor="#DED9E8">Occupation : </td><td width="250"><span id="person_occu"></span></td>
                        </tr>
                        <tr>
                            <td width="120" bgcolor="#DED9E8">Email Address : </td><td width="250"><span id="person_email"></span></td>
                        </tr>
                    </table>
                    <table><tr><td height="20"></td></tr></table>
                    <table border="1">
                        <tr><font color="green">Remarks</font></tr>
                        <tr>
                            <td width="120" bgcolor="#DED9E8"></td><td width="250"><span id="rmks"></span></td>
                        </tr>
                    </table>
                </center>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
            </div>
    </div>
        </div>
</div>
<script type="text/javascript" src="<?php echo base_url('js/moment.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/bootstrap-datetimepicker.min.js'); ?>"></script>
<script type="text/javascript">
     $('#inqlist').DataTable();
     $('#as_date_div').datetimepicker({ defaultDate: "<?php echo date('Y-m-d');?>",  pickTime: false});

    $.validate({
        form : '#comp_form'
    });

    $(document).ready(function() 
    {
        page_type = '<?php echo $this->uri->segment(3)?>';

        if(page_type!='')
        {
            $( "#info_tab" ).trigger( "click" );
        }
    });

    function view_stuinfo(id)
    {
        $.post("<?php echo base_url('hci_admission/get_student_info')?>",{'id':id},
        function(data)
        { 
            if(data == 'denied')
            {
                funcres = {status:"denied", message:"You have no right to proceed the action"};
                result_notification(funcres);
            }
            else
            {
                $('#stuname_span').empty();
                $('#stuname_span').append(data['eq_name']);
                $('#stu_create').empty();
                $('#stu_create').append(data['eq_createdon']);
                $('#student_fname').empty();
                $('#student_fname').append(data['eq_name']);
                $('#student_lname').empty();
                $('#student_lname').append(data['eq_lastname']);
                $('#student_address').empty();
                $('#student_address').append(data['eq_address']);
                $('#student_dob').empty();
                $('#student_dob').append(data['eq_dob']);
                $('#fa_address').empty();
                $('#fa_address').append(data['eq_fatheraddress2']);
                $('#stu_city').empty();
                $('#stu_city').append(data['eq_city']);
                $('#stu_uni').empty();
                $('#stu_uni').append(data['eq_prev_university']);
                $('#stu_home').empty();
                $('#stu_home').append(data['eq_phno']);
                $('#stu_mobi').empty();
                $('#stu_mobi').append(data['eq_mobile']);
                $('#stu_email').empty();
                $('#stu_email').append(data['eq_emailid']);
                $('#stu_mom').empty();
                $('#stu_mom').append(data['eq_mothername']);
                $('#stu_momadd1').empty();
                $('#stu_momadd1').append(data['eq_motheraddress1']);
                $('#stu_momadd2').empty();
                $('#stu_momadd2').append(data['eq_motheraddress2']);
                $('#stu_mom_city').empty();
                $('#stu_mom_city').append(data['eq_mothercity']);
                $('#fa_name').empty();
                $('#fa_name').append(data['eq_fathername']);
                $('#fa_occu').empty();
                $('#fa_occu').append(data['eq_fathersoccupation']);
                $('#fa_fore').empty();
                $('#fa_fore').append(data['eq_foreignnum1']);
                $('#mom_occu').empty();
                $('#mom_occu').append(data['eq_motheroccupation']);
                $('#mom_mobi').empty();
                $('#mom_mobi').append(data['eq_mothermobile']);
                $('#mom_email').empty();
                $('#mom_email').append(data['eq_emailid_2']);
                $('#mom_fore').empty();
                $('#mom_fore').append(data['eq_foreignnum2']);
                $('#person_name').empty();
                $('#person_name').append(data['eq_personname']);
                $('#person_add1').empty();
                $('#person_add1').append(data['eq_personaddress1']);
                $('#person_add2').empty();
                $('#person_add2').append(data['eq_personaddress2']);
                $('#person_city').empty();
                $('#person_city').append(data['eq_personcity']);
                $('#person_mobi').empty();
                $('#person_mobi').append(data['eq_personmobile']);
                $('#person_occu').empty();
                $('#person_occu').append(data['eq_personoccupation']);
                $('#person_email').empty();
                $('#person_email').append(data['eq_emailid_3']);
                $('#rmks').empty();
                $('#rmks').append(data['inqremarks']);

                if(data['eq_sex']=='1')
                {
                    $('#stu_sex').empty();
                    $('#stu_sex').append(data['eq_sex'].value = 'Male');
                }
                else if(data['eq_sex']=='2')
                {
                    $('#stu_sex').empty();
                    $('#stu_sex').append(data['eq_sex'].value = 'Female');
                }
            }
        },  
        "json"
        );
    }

    // var knowingmethod = new Bloodhound({
    //     datumTokenizer: Bloodhound.tokenizers.obj.whitespace('knowabt'),
    //     queryTokenizer: Bloodhound.tokenizers.whitespace,
    //     remote: {
    //         url: '<?php echo base_url("hci_admission/load_methodof_knowing");?>',
    //         wildcard: '%QUERY'
    //     }
    // });

    // $('#remote .typeahead').typeahead(null, {
    //   name: 'knowabt',
    //   display: 'knowabt',
    //   source: knowingmethod
    // });

    function delete_admition(id)
{   
    $.post("<?php echo base_url('hci_admission/remove_admission')?>",{"es_enquiryid":id},
    function(data)
    {   
        if(data == 'denied')
        {
            funcres = {status:"denied", message:"You have no right to proceed the action"};
            result_notification(funcres);
        }
        else
        {
            location.reload();
        }
    },  
    "json"
    );
}


</script>
<script type="text/javascript">
</script>