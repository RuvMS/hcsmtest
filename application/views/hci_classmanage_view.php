<div class="row">
	<div class="col-md-12">
		<h3 class="page-header"><i class="fa fa-graduation-cap"></i>CLASS</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
			<li><i class="fa fa-cog"></i>Settings</li>
			<li><i class="fa fa-graduation-cap"></i>Class</li>
		</ol>
	</div>
</div>
<div>
	<!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a id="create_tab" href="#createpanel" aria-controls="createpanel" role="tab" data-toggle="tab">Create Class</a></li>
        <li role="presentation"><a id="assign_tab" href="#assignpanel" aria-controls="assignpanel" role="tab" data-toggle="tab">Academic Year Class</a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="createpanel">
            <div class="panel">
                <header class="panel-heading">
                    Create/Edit Class
                </header>
                <form class="form-horizontal" role="form" method="post" action="<?php echo base_url('hci_grade/save_class')?>" id="class_form" autocomplete="off" novalidate>
                <div class="panel-body">
                	<div class="row">
						<div class="col-md-6 right-line">
							<br>
							<div class="form-group">
								<input type="hidden" id="cls_id" name="cls_id">
			                  	<label for="cls_name" class="col-md-3 control-label">Class</label>
			                  	<div class="col-md-6">
			                      	<input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="cls_name" name="cls_name" placeholder="">
			                  	</div>
			              	</div>
			              	<div class="form-group">
			                  	<label for="cls_code" class="col-md-3 control-label">Code</label>
			                  	<div class="col-md-6">
			                      	<input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="cls_code" name="cls_code" placeholder="">
			                  	</div>
			              	</div>
						</div>
						<div class="col-md-6">
							<table class="table table-bordered">
			          		<thead>
			          			<tr>
			          				<th>Class</th>
			          				<th>Code</th>
			          				<th>Actions</th>
			          			</tr>
			          		</thead>
			          		<tbody id="classestbl">
			          			<?php 
			          				if(!empty($cls_info))
			          				{
				          				foreach ($cls_info as $cls) 
				          				{
				          					if($cls["cls_status"]=="A")
									    	{
									    		$statusbtn =  "<button onclick='event.preventDefault();change_status(".$cls["cls_id"].",\"D\")' class='btn btn-warning btn-xs'><span class='glyphicon glyphicon-ban-circle' aria-hidden='true'></span></button>";
									    	}
									    	else
									    	{
									    		$statusbtn =  "<button onclick='event.preventDefault();change_status(".$cls["cls_id"].",\"A\")' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-ok-circle' aria-hidden='true'></span></button>";
									    	}

									    	$edit_btn = '<a class="btn btn-info btn-xs" onclick="event.preventDefault();edit_class_load('.$cls['cls_id'].',\''.$cls['cls_name'].'\',\''.$cls['cls_code'].'\')"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>';

				          					echo "<tr>";
				          					echo "<td>".$cls['cls_name']."</td>";
				          					echo "<td>".$cls['cls_code']."</td>";
				          					echo "<td align='center'>".$edit_btn.' | '.$statusbtn."</td>";
				          					echo "</tr>";
				          				}
			          				}
			          				else
			          				{
			          					echo "<tr class='danger'>";
				          				echo "<td colspan='3'>No Record Found</td>";
				          				echo "</tr>";
			          				}
			          			?>
			          		</tbody>
			          	</table>
					    </div>
					</div>	
                </div>
                <div class="panel-footer">
		            <button onclick="event.preventDefault();$('#class_form').trigger('submit');" class="btn btn-info">Save</button>
		            <button onclick="event.preventDefault();$('#class_form').trigger('reset');" class="btn btn-default">Reset</button>
		        </div>
		        </form>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="assignpanel">
            <div class="panel">
                <header class="panel-heading">
                    Assign Class
                </header>
                <form class="form-horizontal" role="form" method="post" action="<?php echo base_url('hci_grade/assign_class')?>" id="assign_form" autocomplete="off" novalidate>
                <div class="panel-body">
                	<div class="row">
						<div class="col-md-6 right-line">
							<input type="hidden" id="yrcls_id" name="yrcls_id">
			          		<div class="form-group">
			                  	<label for="cls_branch" class="col-md-3 control-label">Branch</label>
			                  	<div class="col-md-8">
			                  		<?php 
			                  			global $branchdrop;
			                  			global $selectedbr;
			                  			$extraattrs = 'id="yrcls_branch" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" onchange="load_year_classlist()"';
			                  			echo form_dropdown('yrcls_branch',$branchdrop,$selectedbr, $extraattrs); 
			                  		?>
			                  </div>
			              	</div>
			          		<div class="form-group">
			                    <label for="yrcls_accyear" class="col-md-3 control-label">Academic Year</label>
			                    <div class="col-md-9">
			                        <select class="form-control select2" data-validation="required" onchange="load_year_classlist()" data-validation-error-msg-required="Field cannot be empty" id="yrcls_accyear" name="yrcls_accyear"  style="width: 100%;">
			                        <option value="all">---Select academic year---</option>
			                            <?php
			                            foreach($acc_years as $row):
			                                if($row['ac_iscurryear']==1)
			                                {
			                                    $selected = 'selected';
			                                }
			                                else
			                                {
			                                    $selected = '';
			                                }
			                                ?>
			                                <option value="<?php echo $row['es_ac_year_id'];?>" <?php echo $selected;?>>
			                                    <?php echo $row['ac_startdate']." - ".$row['ac_enddate'];?>
			                                </option>
			                                <?php
			                            endforeach;
			                            ?>
			                        </select>
			                    </div>
			                </div>
			              	<div class="form-group">
			                  	<label for="yrcls_grade" class="col-md-3 control-label">Grade</label>
			                  	<div class="col-md-6">
			                      	<select class="form-control" id="yrcls_grade" name="yrcls_grade" data-validation="required" data-validation-error-msg-required="Field cannot be empty" onchange="load_year_classlist()">
			                  			<option value="all">---Select Grade---</option>
			                  			<?php
	                                    $grd = $this->db->get('hci_grade')->result_array();
	                                    foreach($grd as $row):
	                                        $selected="";
	                                        // if($row['grd_id']==$this->hci_studentreg_model->show_val('pre_class')){
	                                        //     $selected="selected";
	                                        // }
	                                        ?>
	                                        <option value="<?php echo $row['grd_id'];?>"  <?php echo $selected?>>
	                                           <?php echo $row['grd_name'];?>
	                                        </option>
	                                        
	                                        <?php
	                                    endforeach;
	                                ?>
			                      	</select>
			                  </div>
			              	</div>
			              	<div id="classesListDiv">
			              		<div class="row">
									<div class="col-md-3"></div>
									<div class="col-md-4">Class</div>
									<div class="col-md-4">Max Students</div>
								</div>
			              	</div>
			              	<!-- <div class="form-group">
			                  	<label for="yrcls_maxstudents" class="col-md-3 control-label">Max.Students</label>
			                  	<div class="col-md-6">
			                      	<input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="yrcls_maxstudents" name="yrcls_maxstudents" placeholder="" value="25">
			                  </div>
			              	</div> -->
						</div>
						<div class="col-md-6">
					    </div>
					</div>
                </div>
                <div class="panel-footer">
		            <button onclick="event.preventDefault();$('#assign_form').trigger('submit');" class="btn btn-info">Save</button>
		            <button onclick="event.preventDefault();$('#assign_form').trigger('reset');" class="btn btn-default">Reset</button>
		        </div>
                </form>
            </div>
        </div>
    </div>
	<br>
</div>
<script type="text/javascript">

$(document).ready(function() 
{
    tab_id = '<?php echo $_GET['tab_id']?>';

    if(tab_id=='assign_tab')
    {
    	$( "#assign_tab" ).trigger( "click" );
    }
    else
    {
    	$( "#create_tab" ).trigger( "click" );
    }
});

function change_status(id,new_s)
{	
	$.post("<?php echo base_url('hci_grade/changeclass_status')?>",{"cls_id":id,"new_s":new_s},
	function(data)
	{	
		if(data == 'denied')
		{
    		funcres = {status:"denied", message:"You have no right to proceed the action"};
    		result_notification(funcres);
		}
		else
		{
			location.reload();
		}
	},	
	"json"
	);
}

function edit_class_load(id,name,code)
{
	$('#cls_id').val(id);
	$('#cls_name').val(name);
	$('#cls_code').val(code);
}

function load_year_classlist()
{
	$('#classesListDiv').empty();
	$('#classesListDiv').append('<div class="row">'+
								'<div class="col-md-3"></div>'+
								'<div class="col-md-4">Class</div>'+
								'<div class="col-md-4">Max Students</div>'+
								'</div><br>');
	yrcls_branch  = $('#yrcls_branch').val();
	yrcls_accyear = $('#yrcls_accyear').val();
	yrcls_grade   = $('#yrcls_grade').val();

	$.post("<?php echo base_url('hci_grade/load_year_classlist')?>",{"yrcls_branch":yrcls_branch,"yrcls_accyear":yrcls_accyear,"yrcls_grade":yrcls_grade},
	function(data)
	{	
		if(data == 'denied')
		{
    		funcres = {status:"denied", message:"You have no right to proceed the action"};
    		result_notification(funcres);
		}
		else
		{
			if(data.length > 0)
			{
				for (i = 0; i<data.length; i++)
				{
					is_assigned = '';
					maxstudents = 25;

					if(data[i]['is_assigned'] == 1)
					{
						is_assigned = 'checked';
						maxstudents = data[i]['assiged_data']['yrcls_maxstudents'];
					}

					$('#classesListDiv').append('<div class="row">'+
												'<div class="col-md-3"></div>'+
												'<div class="col-md-4">'+
												'<div class="checkbox"><label><input type="checkbox" name="accyearClass[]" value="'+data[i]['cls_id']+'" '+is_assigned+'>'+data[i]['cls_code']+'</label></div>'+
												'</div>'+
												'<div class="col-md-4">'+
												'<input type="text" class="form-control" value="'+maxstudents+'" name="maxstu_'+data[i]['cls_id']+'">'+
												'</div>'+
												'</div><br>');
				}
			}
		}
	},	
	"json"
	);
}

$(document).ready(function() 
{
  // load_class_list();
});

$.validate({
   	form : '#cls_form'
});

// function load_class_list()
// {
// 	acayear = $('#cls_academicyear').val();
// 	grade = $('#cls_grade').val();
// 	branch = $('#cls_branch').val();

// 	$('#classestbl').empty();

// 	$.post("<?php echo base_url('hci_grade/load_class_list')?>",{"acayear":acayear,"grade":grade,"branch":branch},
// 	function(data)
// 	{	
// 		if(data == 'denied')
// 		{
//     		funcres = {status:"denied", message:"You have no right to proceed the action"};
//     		result_notification(funcres);
// 		}
// 		else
// 		{
// 			if(data['acayears'].length>0)
// 			{
// 				for (i = 0; i<data['acayears'].length; i++) 
// 				{			
// 					$('#classestbl').append('<tr style="background-color:#D0D0D0"><td colspan="5">'+data['acayears'][i]['ac_startdate']+' - '+data['acayears'][i]['ac_enddate']+'</td></tr>');

// 					if(data['classes'].length>0)
// 					{
// 						if(data['acayears'][i]['branches'].length>0)
// 						{
// 							for (b = 0; b<data['acayears'][i]['branches'].length; b++)
// 							{
// 								$('#classestbl').append('<tr style="background-color:#D0D0D0"><td colspan="5">'+data['acayears'][i]['branches'][b]['br_name']+'</td></tr>');

// 								if(data['acayears'][i]['grades'].length>0)
// 								{
// 									for (x = 0; x<data['acayears'][i]['grades'].length; x++) 
// 									{			
// 										$('#classestbl').append('<tr style="background-color:#E0E0E0 "><td colspan="5">['+data['acayears'][i]['grades'][x]['grd_code']+'] - '+data['acayears'][i]['grades'][x]['grd_name']+'</td></tr>');
										
// 										for (y = 0; y<data['classes'].length; y++) 
// 										{	
// 											if(data['classes'][y]['cls_academicyear']==data['acayears'][i]['es_ac_year_id'] && data['classes'][y]['cls_grade']==data['acayears'][i]['grades'][x]['grd_id'] && data['classes'][y]['cls_branch']==data['acayears'][i]['branches'][b]['br_id'])	
// 											{
// 												if(data['classes'][y]["cls_status"]=="A")
// 										    	{
// 										    		statusbtn =  "<button onclick='event.preventDefault();change_status("+data['classes'][y]["cls_id"]+",\"D\")' class='btn btn-warning btn-xs'><span class='glyphicon glyphicon-ban-circle' aria-hidden='true'></span></button>";
// 										    	}
// 										    	else
// 										    	{
// 										    		statusbtn =  "<button onclick='event.preventDefault();change_status("+data['classes'][y]["cls_id"]+",\"A\")' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-ok-circle' aria-hidden='true'></span></button>";
// 										    	}

// 										    	// echo "<a class='btn btn-danger btn-xs' onclick='event.preventDefault();delete_group_load
// 				          	// 					     (".$grd['grd_id'].")'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></a>| ";
										    	
// 												$('#classestbl').append('<tr><td>'+data['classes'][y]['cls_name']+'</td><td>'+data['classes'][y]['cls_code']+'</td><td>'+data['classes'][y]['cls_maxstudents']+'</td><td>'+data['classes'][y]['cls_numofstudents']+'</td><td><a class="btn btn-info btn-xs" onclick="event.preventDefault();edit_class_load('+data['classes'][y]['cls_id']+','+data['classes'][y]['cls_grade']+','+data['classes'][y]['cls_maxstudents']+','+data['classes'][y]['cls_academicyear']+',\''+data['classes'][y]['cls_name']+'\',\''+data['classes'][y]['cls_code']+'\','+data['classes'][y]['cls_branch']+')"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a> | '+statusbtn+'</td></tr>');
// 											}	
// 										}
// 									}
// 								}
// 							}
// 						}
// 					}
// 				}
// 			}
// 			else
// 			{
// 				$('#classestbl').append('<tr style="background-color:#e2e6e9"><td colspan="5">No recored found</td></tr>');
// 			}
// 		}
// 	},	
// 	"json"
// 	);
// }

</script>
