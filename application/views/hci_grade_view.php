<div class="row">
	<div class="col-md-12">
		<h3 class="page-header"><i class="fa fa-graduation-cap"></i>GRADE</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
			<li><i class="fa fa-cog"></i>Settings</li>
			<li><i class="fa fa-graduation-cap"></i>Grade</li>
		</ol>
	</div>
</div>
<div>
	<br>
	<div class="row">
		<div class="col-md-6">
		  	<section class="panel">
			    <header class="panel-heading">
			        Manage Grades
			    </header>
		      	<div class="panel-body">	
		          	<form class="form-horizontal" role="form" method="post" action="<?php echo base_url('hci_grade/save_grade')?>" id="grd_form" autocomplete="off" novalidate>
		              	<div class="form-group">
		              		<input type="hidden" id="grd_id" name="grd_id">
		                  	<label for="grname" class="col-md-2 control-label">Grade</label>
		                  	<div class="col-md-6">
		                      	<input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="grname" name="grname" placeholder="">
		                  </div>
		              	</div>
		              	<div class="form-group">
		                  	<label for="gr_code" class="col-md-2 control-label">Code</label>
		                  	<div class="col-md-6">
		                      	<input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="gr_code" name="gr_code" placeholder="">
		                  </div>
		              	</div>
		              	<div class="form-group">
		                  	<label for="gr_code" class="col-md-2 control-label">Grade to Promote</label>
		                  	<div class="col-md-6">
		                      	<select class="form-control" id="grpromo" name="grpromo">
		                  			<option value="">Select Grade</option>
		                  			
		                  			<?php
                                    $grd = $this->db->get('hci_grade')->result_array();
                                    foreach($grd as $row):
                                        $selected="";
                                        // if($row['grd_id']==$this->hci_studentreg_model->show_val('pre_class')){
                                        //     $selected="selected";
                                        // }
                                        ?>
                                        <option value="<?php echo $row['grd_id'];?>"  <?php echo $selected?>>
                                           <?php echo $row['grd_name'];?>
                                        </option>
                                        
                                        <?php
                                    endforeach;
                                ?>
		                      	</select>
		                  </div>
		              	</div>
		              	<div class="form-group">
		                  	<div class="col-md-offset-2 col-md-11">
		                      	<button type="submit" class="btn btn-info">Save</button> 
		                      	<button type="reset" class="btn btn-default">Reset</button>
		                  	</div>
		              	</div>
		          	</form>
		      	</div>
		  	</section>
	  	</div>
	  	<div class="col-md-6">
		  	<section class="panel">
			    <header class="panel-heading">
			       Look Up
			    </header>
		      	<div class="panel-body">	
		          	<table class="table">
		          		<thead>
		          			<tr>
		          				<th>Grade</th>
		          				<th>Code</th>
		          				<th>Actions</th>
		          			</tr>
		          		</thead>
		          		<tbody>
	          				<?php
	          					foreach ($grd_info as $grd) 
	          					{
	          						
	          						echo "<tr>";
	          						echo "<td>".$grd['grd_name']."</td>";
	          						echo "<td>".$grd['grd_code']."</td>";
	          						echo "<td><a class='btn btn-info btn-xs' onclick='event.preventDefault();edit_group_load(".$grd['grd_id'].",\"".$grd['grd_name']."\",\"".$grd['grd_promotegrd']."\",\"".$grd['grd_code']."\")'><span class='glyphicon glyphicon-pencil' aria-hidden='true'></span></a>| ";

	          						if($grd["grd_isused"]==0)
							    	{
	          							echo "<a class='btn btn-danger btn-xs' onclick='event.preventDefault();delete_group_load
	          						     (".$grd['grd_id'].")'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></a>| ";
	          						}

							    	if($grd["grd_status"]=="A")
							    	{
							    		echo "<button onclick='event.preventDefault();change_status(".$grd["grd_id"].",\"D\")' class='btn btn-warning btn-xs'><span class='glyphicon glyphicon-ban-circle' aria-hidden='true'></span></button>";
							    	}
							    	else
							    	{
							    		echo "<button onclick='event.preventDefault();change_status(".$grd["grd_id"].",\"A\")' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-ok-circle' aria-hidden='true'></span></button>";
							    	}
							    	
	          						echo "</td></tr>";
	          					}
	          				?>
		          		</tbody>
		          	</table>
		      	</div>
		  	</section>
	  	</div>
	</div>
</div>
<script type="text/javascript">

$.validate({
   	form : '#grd_form'
});

function edit_group_load(id,name,promo,code)
{
	$('#grd_id').val(id);
	$('#grname').val(name);
	$('#grpromo').val(promo);
	$('#gr_code').val(code);
}

function delete_group_load(id)
{	
	$.post("<?php echo base_url('hci_grade/remove_grade')?>",{"grd_id":id},
	function(data)
	{	
		if(data == 'denied')
		{
    		funcres = {status:"denied", message:"You have no right to proceed the action"};
    		result_notification(funcres);
		}
		else
		{
			location.reload();
		}
	},	
	"json"
	);
}

function change_status(id,new_s)
{	
	$.post("<?php echo base_url('hci_grade/change_status')?>",{"grd_id":id,"new_s":new_s},
	function(data)
	{
		if(data == 'denied')
		{
    		funcres = {status:"denied", message:"You have no right to proceed the action"};
    		result_notification(funcres);
		}
		else
		{	
			location.reload();
		}
	},	
	"json"
	);
}

</script>
