<?php                           
    $predata = $this->auth->get_ugroup_options();
?>
<div class="row">
    <div class="col-md-12">
        <h3 class="page-header"><i class="fa fa-users"></i> USER GROUP MANAGEMENT</h3>
        <ol class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
            <li><i class="fa fa-lock"></i>System Access</li>
            <li><i class="fa fa-users"></i>User Group</li>
        </ol>
    </div>
</div>
<div>
<section class="panel">
    <header class="panel-heading">
        <div class="col-md-4">
            Create / Edit User Group
        </div>
    </header>
    <form class="form-horizontal" role="form" method="post" action="<?php echo base_url('user/save_usergroup')?>" id="ugroupform" autocomplete="off" novalidate>
    <div class="panel-body">  
        <div class="row">
            <div class="col-md-5">  
                <br>
                <div class="form-group">
                    <label for="grp_name" class="col-md-3 control-label">Name</label>
                    <div class="col-md-9">
                        <input type="hidden" name="grp_id" id="grp_id">
                        <input type="text" data-validation="required" data-validation-error-msg-required="Field can not be empty" class="form-control" id="grp_name" name="grp_name" placeholder="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="acc_level" class="col-md-3 control-label">Access Level</label>
                    <div class="col-md-9">
                        <select type="text" class="form-control" onchange="changegrpview(this.value)" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="acc_level" name="acc_level">
                            <option value=''></option>
                            <?php
                                echo $predata['ops'];
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="comp" class="col-md-3 control-label">Company</label>
                    <div class="col-md-9">
                        <select type="text" class="form-control" data-validation="required" onchange="load_branches(this.value,'')" data-validation-error-msg-required="Field can not be empty" id="comp" name="comp" disabled>
                            <option value=''></option>
                            <?php
                                foreach ($company as $grp) 
                                {
                                    if($predata['comp']=='all')
                                    {
                                        echo "<option value='".$grp['grp_id']."'>".$grp['grp_name']."</option>";
                                    }
                                    else
                                    {
                                        if($predata['comp']==$grp['grp_id'])
                                        {
                                            echo "<option value='".$grp['grp_id']."'>".$grp['grp_name']."</option>";
                                        }
                                    }
                                }
                            ?>
                        </select>
                  </div>
                </div>
                <div class="form-group">
                    <label for="branch" class="col-md-3 control-label">Branch</label>
                    <div class="col-md-9">
                        <select type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="branch" name="branch" disabled>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <table id="ugrouptable" class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>User Group</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($groups as $grp) 
                            {
                                echo "<tr>";
                                echo "<td>".$grp['ug_name']."</td>"; 

                                if($predata['comp']=='all')
                                {
                                    echo "<td><a class='btn btn-info btn-xs' onclick='event.preventDefault();edit_group(".$grp['ug_id'].",\"".$grp['ug_name']."\",".$grp['ug_level'].",".((empty($grp['ug_company']))?"null":$grp['ug_company']).",".((empty($grp['ug_branch']))?"null":$grp['ug_branch']).")'>Edit</a></td>";
                                }
                                else
                                {
                                    if(($predata['comp']==$grp['ug_company']) && ($grp['ug_level']==3))
                                    {
                                       echo "<td><a class='btn btn-info btn-xs' onclick='event.preventDefault();edit_group(".$grp['ug_id'].",\"".$grp['ug_name']."\",".$grp['ug_level'].",".((empty($grp['ug_company']))?"null":$grp['ug_company']).",".((empty($grp['ug_branch']))?"null":$grp['ug_branch']).")'>Edit</a></td>";
                                    }
                                    else if($grp['ug_level']>3)
                                    {
                                        echo "<td><a class='btn btn-info btn-xs' onclick='event.preventDefault();edit_group(".$grp['ug_id'].",\"".$grp['ug_name']."\",".$grp['ug_level'].",".((empty($grp['ug_company']))?"null":$grp['ug_company']).",".((empty($grp['ug_branch']))?"null":$grp['ug_branch']).")'>Edit</a></td>";
                                    }
                                    else
                                    {
                                        echo "<td></td>";
                                    }
                                } 
                                
                                echo "</tr>";
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <button type="submit" class="btn btn-info">Save</button> 
        <button type="submit" class="btn btn-default">Reset</button>
    </div>
    </form>
</section>
</div>
<script type="text/javascript">
$.validate({
    form : '#ugroupform'
});

var table = $('#ugrouptable').DataTable( {
        "pageLength": 5,
        "dom" : '<"row"<"col-md-6"l><"col-md-6"f>>rt<"row"<"col-md-4"i><"col-md-8"p>><"clear">',
    } );

function edit_group(id,name,lvl,comp,brnch)
{
    $('#grp_id').val(id);
    $('#grp_name').val(name);
    $('#acc_level').val(lvl);
    $('#comp').val(comp);
    load_branches(comp,brnch);
    changegrpview(lvl);
}

function load_branches(id,br)
{
    $('#branch').empty();
    $('#branch').append("<option value=''></option>");

    level = $('#acc_level').val();

    if(level==3)
    {
        $('#branch').prop('disabled', false);
    }
    else
    {
        $('#branch').prop('disabled', true);
    }

    $.post("<?php echo base_url('company/load_branches')?>",{'id':id},
        function(data)
        { 
            if(data == 'denied')
            {
                funcres = {status:"denied", message:"You have no right to proceed the action"};
                result_notification(funcres);
            }
            else
            {  
                if(data.length>0)
                {   
                    for (i = 0; i<data.length; i++) {
                        if(br==data[i]['br_id'])
                        {
                            seltxt = 'selected';
                        }
                        else
                        {
                            seltxt = '';
                        }
                        $('#branch').append("<option value='"+data[i]['br_id']+"' "+seltxt+">["+data[i]['br_code']+'] - '+data[i]['br_name']+"</option>");
                    }
                }
            }
        },  
        "json"
    );
}

function changegrpview(level)
{
    if(level==3)
    {
        $('#comp').prop('disabled', false);   
    }
    else if(level==2)
    {
        $('#branch').val('');
        $('#comp').prop('disabled', false);  
        $('#branch').prop('disabled', true);
    }
    else
    {
        $('#branch').val('');
        $('#comp').val('');
        $('#comp').prop('disabled', true);  
        $('#branch').prop('disabled', true);
    }
}
</script>