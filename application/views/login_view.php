<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">

    <title><?php echo "HCSM :: ".$title;?></title>

    <!-- Bootstrap CSS -->    
    <link href="<?php echo base_url('css/bootstrap.min.css')?>" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="<?php echo base_url('css/bootstrap-theme.css')?>" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="<?php echo base_url('css/elegant-icons-style.css')?>" rel="stylesheet" />
    <link href="<?php echo base_url('css/font-awesome.css')?>" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="<?php echo base_url('css/style.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('css/style-responsive.css')?>" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
<body class="login-img3-body">
    <div class="container">
        <form class="login-form" autocomplete="off" action="<?php echo base_url('login/loginSubmit')?>" method="post" id = "login_form" name = "login_form">        
            <div class="login-wrap">
                <p class="login-img"><i class="icon_lock_alt"></i></p>
                <?php
                    if(isset($_GET['login']))
                    {
                        echo '<br><div class="row">';
                        if($_GET['login']=='logout')
                        {
                ?>
                            <div class="alert alert-success fade in" role="alert">  
                                Successfully Logout
                            </div>
                <?php
                        }

                        if($_GET['login']=='invalid')
                        {
                ?>
                            <div class="alert alert-danger fade in" role="alert">  
                                Invalid Login. Try again
                            </div>
                <?php
                        }
                        echo '</div>';
                    } 
                ?>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon_profile"></i></span>
                    <input type="text" class="form-control" placeholder="Username" autofocus id="username" name="username">
                </div>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon_key_alt"></i></span>
                    <input type="password" class="form-control" placeholder="Password" id="password" name="password">
                </div>
                <!-- <label class="checkbox">
                    <input type="checkbox" value="remember-me"> Remember me
                    <span class="pull-right"> <a href="#"> Forgot Password?</a></span>
                </label> -->
                <button class="btn btn-primary btn-lg btn-block" type="submit">Login</button>
            </div>
        </form>
        <div class="text-right">
            <div class="credits">
                <!-- 
                    All the links in the footer should remain intact. 
                    You can delete the links only if you purchased the pro version.
                    Licensing information: https://bootstrapmade.com/license/
                    Purchase the pro version form: https://bootstrapmade.com/buy/?theme=NiceAdmin
                -->
            </div>
        </div>
    </div>
</body>
</html>
