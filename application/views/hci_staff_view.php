<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/jquery-confirm.css'); ?>"> 
<script type="text/javascript" src="<?php echo base_url('js/jquery-confirm.js')?>"></script><!--jquery-->
<div class="row">
    <div class="col-md-12">
        <h3 class="page-header"><i class="fa fa-users"></i> CREATE / EDIT STAFF MEMBER</h3>
        <ol class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
            <li><i class="fa fa-cog"></i>Staff</li>
            <li><i class="fa fa-bank"></i>Add Staff</li>
        </ol>
    </div>
</div>
<div>
<form class="form-horizontal" role="form" method="post" action="<?php echo base_url('hci_staff/staff_save')?>" id="staffform" autocomplete="off" novalidate>
<section class="panel">
        <header class="panel-heading">
            Staff Information
        </header>
        <div class="panel-body">
            <div class="form-group">
                <input type="hidden" name="stf_id" id="stf_id">
                <label for="name" class="col-md-2 control-label">Name</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" placeholder="First_name" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="stf_firstname" name="stf_firstname" placeholder="" value="">
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" placeholder="last_name"  data-validation="required" data-validation-error-msg-required="Field can not be empty" id="stf_lastname" name="stf_lastname" placeholder="">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Branch</label>
                <div class="col-md-6">
                    <?php 
                        global $branchdrop;
                        global $selectedbr;
                        $extraattrs = 'id="stf_branch" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty"';
                        echo form_dropdown('stf_branch',$branchdrop,$selectedbr, $extraattrs); 
                    ?>
              </div>
            </div>
            <!-- <div class="form-group">
                <label for="brnum" class="col-md-2 control-label">Other Names</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="oname" name="post[other_names]" placeholder="">
                </div>
            </div> -->
            
            <div class="form-group">
                <div class="col-md-5">
                    <label class="col-md-5 control-label">Academic</label>
                        <input type="radio" data-validation="required" name="stf_group" class="col-md-1" id="stf_group1" value="1">

                    <label class="col-md-5 control-label">Non-Academic</label>
                        <input type="radio" data-validation="required" name="stf_group" class="col-md-1" id="stf_group2" value="2">
                </div>
            </div>
             <div class="form-group">
                <div class="col-md-5">
                    <label class="col-md-5 control-label">Department</label>
                        <div class="col-md-6">
                        	<select class="form-control col-md-1" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="stf_department" name="stf_department">

                        	<option value="">Select</option>
                        	<?php
                            foreach($deptinfo as $dept):
                                // if($dept['dept_status']=='A')
                                // {
                                //     $selected = 'selected';
                                // }
                                // else
                                // {
                                //     $selected = '';
                                // }
                                    if($dept['dept_status']=='A')
                                    {
                                ?>
                                <option value="<?php echo $dept['dept_id'];?>">
                                    <?php
                                        
                                        echo $dept['dept_name'];
                                    ?>
                                </option>
                                <?php
                                    }
                            endforeach;
                            ?>
                        	</select>
                        </div>
                </div>
                <div class="col-md-5">
                    <label class="col-md-5 control-label">Designation</label>
                        <div class="col-md-6">
                        	<select class="form-control col-md-1" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="stf_designation" name="stf_designation">

                        	<option value="#">Select</option>
                        	<?php
                            foreach($desiginfo as $desig):
                                // if($desig['desig_status']=='A')
                                // {
                                //     $selected = 'selected';
                                // }
                                // else
                                // {
                                //     $selected = '';
                                // }
                                if($desig['desig_status']=='A')
                                {
                                ?>
                                <option value="<?php echo $desig['desig_id'];?>">
                                    <?php 
                                        echo $desig['desig_name'];
                                    ?>
                                </option>
                                <?php
                                }
                            endforeach;
                            ?>
                        	</select>
                        </div>
                </div>
            </div>

            <!-- <div class="form-group">
                <div class="col-md-5">
                    <label class="col-md-5 control-label">Grade</label>
                        <div class="col-md-6">
                        	<select class="form-control col-md-1" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="st_religion" name="post[st_religion]">

                        	<option value="#">Select</option>
                        	<?php
                            foreach($grd_info as $grd):
                                if($grd['grd_status']=='A')
                                {
                                    $selected = 'selected';
                                }
                                else
                                {
                                    $selected = '';
                                }
                                ?>
                                <option value="<?php echo $grd['grd_id'];?>" <?php echo $selected?>>
                                    <?php echo $grd['grd_name'];?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        	</select>
                        </div>
                </div>
            </div> -->

            <div class="form-group">
                <label for="brnum" class="col-md-2 control-label">Email</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="stf_email" name="stf_email" placeholder="">
                </div>
            </div>

            <div class="form-group">
                <label for="brnum" class="col-md-2 control-label">Nationality</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="stf_nationality" name="stf_nationality" placeholder="">
                </div>
            </div>
            
            <div class="col-md-5">
                    <div class="form-group">
                        <label for="stf_maritalstat" class="col-sm-5 control-label">Marital Status </label>
                        <div class="col-sm-7">
                            <label class="col-md-3 control-label">Married</label>
                            <input type="radio" data-validation="required" name="stf_maritalstat" class="col-md-2" id="stf_maritalstat1" value="1">

                            <label class="col-md-3 control-label">Unmarried</label>
                            <input type="radio" data validation="required" name="stf_maritalstat" id="stf_maritalstat2" class="col-md-3" value="2">
                        </div>
                    </div>
                </div>
                <hr><hr>
               	<hr>
               	<div class="form-group">
                <div class="col-md-5">
                    <label class="col-md-5 control-label">Section</label>
                        <div class="col-md-6">
                        	<select class="form-control col-md-1" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="stf_section" name="post[stf_section]">

                        	<option value="#">Select</option>
                        	<?php
		                            foreach($sections as $section):
		                                // if($section['sec_status']=='A')
		                                // {
		                                //     $selected = 'selected';
		                                // }
		                                // else
		                                // {
		                                //     $selected = '';
		                                // }
                                        if($section['sec_status']=='A')
                                        {
		                                ?>
		                                <option value="<?php echo $section['sec_id'];?>" <?php echo $selected?>>
		                                    <?php echo $section['sec_name'];?>
		                                </option>
		                                <?php
                                        }
		                            endforeach;
	                            ?>
                        	</select>
                        </div>
                </div>
	                <!-- <div class="col-md-5">
	                    <label class="col-md-5 control-label">scheme</label>
	                        <div class="col-md-6">
	                        	<select class="form-control col-md-1" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="st_religion" name="post[st_religion]">

	                        	<option value="#">Select</option>
	                        	</select>
	                        </div>
	                </div> -->
            	</div>
               <br><br>
            <div class="form-group">
                <div class="col-md-offset-2 col-md-11">
                    <button type="submit" name="save_btn" id="save_btn" class="btn btn-info">Save</button>
                    <button type="reset" name="reset" class="btn btn-default">Reset</button>
                </div>
            </div>
        </div>
    </section>
    </form>
</div>
<script type="text/javascript">
$.validate({
    form : '#staffform'
});
</script>