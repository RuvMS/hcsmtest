<div class="row">
	<div class="col-md-12">
		<h3 class="page-header"><i class="fa fa-graduation-cap"></i>DESIGNATION</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
			<li><i class="fa fa-cog"></i>Configurations</li>
			<li><i class="fa fa-graduation-cap"></i>Designation</li>
		</ol>
	</div>
</div>
<div>
	<br>
	<div class="row">
		<div class="col-md-4">
		  	<section class="panel">
			    <header class="panel-heading">
			        Manage Designation
			    </header>
		      	<div class="panel-body">	
		          	<form class="form-horizontal" role="form" method="post" action="<?php echo base_url('hci_config/save_designation')?>" id="desig_form" autocomplete="off" novalidate>
		              	<div class="form-group">
		              		<input type="hidden" id="desig_id" name="desig_id">
		                  	<label for="desig_name" class="col-md-4 control-label">Designation</label>
		                  	<div class="col-md-6">
		                      	<input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="desig_name" name="desig_name" placeholder="">
		                  	</div>
		              	</div>
		              	<div class="form-group">
		                  	<div class="col-md-offset-2 col-md-11">
		                      	<button type="submit" class="btn btn-info">Save</button> 
		                      	<button type="reset" class="btn btn-default">Reset</button>
		                  	</div>
		              	</div>
		          	</form>
		      	</div>
		  	</section>
	  	</div>
	  	<div class="col-md-8">
		  	<section class="panel">
			    <header class="panel-heading">
			       Look Up
			    </header>
		      	<div class="panel-body">	
		          	<table class="display" id="desigtbl">
		          		<thead>
		          			<tr>
		          				<th>#</th>
		          				<th>Designation</th>
		          				<th>Actions</th>
		          			</tr>
		          		</thead>
		          		<tbody>
	          				<?php
	          					$i=1;
	          					if(!empty($desiginfo))
	          					{
	          						foreach ($desiginfo as $desig) 
		          					{
		          						echo "<tr>";
		          						echo "<td>".$i."</td>";
		          						echo "<td>".$desig['desig_name']."</td>";
		          						echo "<td><a class='btn btn-info btn-xs' onclick='event.preventDefault();edit_desig_load(".$desig['desig_id'].",\"".$desig['desig_name']."\")'><span class='glyphicon glyphicon-pencil' aria-hidden='true'></span></a>| ";

		          	// 					if($desig["desig_isused"]==0)
								    	// {
		          	// 						echo "<a class='btn btn-danger btn-xs' onclick='event.preventDefault();delete_group_load
		          	// 					     (".$desig['desig_id'].")'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></a>| ";
		          	// 					}

								    	if($desig["desig_status"]=="A")
								    	{
								    		echo "<button onclick='event.preventDefault();change_status(".$desig["desig_id"].",\"D\")' class='btn btn-warning btn-xs'><span class='glyphicon glyphicon-ban-circle' aria-hidden='true'></span></button>";
								    	}
								    	else
								    	{
								    		echo "<button onclick='event.preventDefault();change_status(".$desig["desig_id"].",\"A\")' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-ok-circle' aria-hidden='true'></span></button>";
								    	}
								    	
		          						echo "</td></tr>";

		          						$i++;
		          					}
	          					}

	          				?>
		          		</tbody>
		          	</table>
		      	</div>
		  	</section>
	  	</div>
	</div>
</div>
<script type="text/javascript">

$.validate({
   	form : '#desig_form'
});

$(document).ready(function() {
    $('#desigtbl').DataTable();
} );

function edit_desig_load(id,name)
{
	$('#desig_id').val(id);
	$('#desig_name').val(name);
}

// function delete_group_load(id)
// {	
// 	$.post("<?php echo base_url('hci_config/remove_dept')?>",{"grd_id":id},
// 	function(data)
// 	{	
// 		location.reload();
// 	},	
// 	"json"
// 	);
// }

function change_status(id,new_s)
{	
	$.post("<?php echo base_url('hci_config/change_desigstatus')?>",{"desig_id":id,"new_s":new_s},
	function(data)
	{	
		if(data == 'denied')
		{
    		funcres = {status:"denied", message:"You have no right to proceed the action"};
    		result_notification(funcres);
		}
		else
		{
			location.reload();
		}
	},	
	"json"
	);
}

</script>
