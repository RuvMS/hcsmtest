<div class="row">
	<div class="col-md-12">
		<h3 class="page-header"><i class="fa fa-graduation-cap"></i>DEPARTMENT</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
			<li><i class="fa fa-cog"></i>Configurations</li>
			<li><i class="fa fa-graduation-cap"></i>Department</li>
		</ol>
	</div>
</div>
<div>
	<br>
	<div class="row">
		<div class="col-md-4">
		  	<section class="panel">
			    <header class="panel-heading">
			        Manage Department
			    </header>
		      	<div class="panel-body">	
		          	<form class="form-horizontal" role="form" method="post" action="<?php echo base_url('hci_config/save_department')?>" id="dept_form" autocomplete="off" novalidate>
		          		<div class="form-group">
		                  	<label for="dep_branch" class="col-md-4 control-label">Branch</label>
		                  	<div class="col-md-6">
		                  		<?php 
		                  			global $branchdrop;
		                  			global $selectedbr;
		                  			$extraattrs = 'id="dep_branch" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" ';
		                  			echo form_dropdown('dep_branch',$branchdrop,$selectedbr, $extraattrs); 
		                  		?>
		                  </div>
		              	</div>
		              	<div class="form-group">
		              		<input type="hidden" id="dept_id" name="dept_id">
		                  	<label for="depname" class="col-md-4 control-label">Department</label>
		                  	<div class="col-md-6">
		                      	<input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="depname" name="depname" placeholder="">
		                  </div>
		              	</div>
		              	<div class="form-group">
		                  	<div class="col-md-offset-2 col-md-11">
		                      	<button type="submit" class="btn btn-info">Save</button> 
		                      	<button type="reset" class="btn btn-default">Reset</button>
		                  	</div>
		              	</div>
		          	</form>
		      	</div>
		  	</section>
	  	</div>
	  	<div class="col-md-8">
		  	<section class="panel">
			    <header class="panel-heading">
			       Look Up
			    </header>
		      	<div class="panel-body">	
		          	<table class="display" id="depettable">
		          		<thead>
		          			<tr>
		          				<th>#</th>
		          				<th>Department</th>
		          				<th>Actions</th>
		          			</tr>
		          		</thead>
		          		<tbody>
	          				<?php
	          					$i=1;
	          					if(!empty($deptinfo))
	          					{
	          						foreach ($deptinfo as $dept) 
		          					{
		          						echo "<tr>";
		          						echo "<td>".$i."</td>";
		          						echo "<td>".$dept['dept_name']."</td>";
		          						echo "<td><a class='btn btn-info btn-xs' onclick='event.preventDefault();edit_dept_load(".$dept['dept_id'].",\"".$dept['dept_name']."\")'><span class='glyphicon glyphicon-pencil' aria-hidden='true'></span></a>| ";

		          	// 					if($dept["dept_isused"]==0)
								    	// {
		          	// 						echo "<a class='btn btn-danger btn-xs' onclick='event.preventDefault();delete_group_load
		          	// 					     (".$dept['dept_id'].")'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></a>| ";
		          	// 					}

								    	if($dept["dept_status"]=="A")
								    	{
								    		echo "<button onclick='event.preventDefault();change_status(".$dept["dept_id"].",\"D\")' class='btn btn-warning btn-xs'><span class='glyphicon glyphicon-ban-circle' aria-hidden='true'></span></button>";
								    	}
								    	else
								    	{
								    		echo "<button onclick='event.preventDefault();change_status(".$dept["dept_id"].",\"A\")' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-ok-circle' aria-hidden='true'></span></button>";
								    	}
								    	
		          						echo "</td></tr>";

		          						$i++;
		          					}
	          					}
	          				?>
		          		</tbody>
		          	</table>
		      	</div>
		  	</section>
	  	</div>
	</div>
</div>
<script type="text/javascript">

$.validate({
   	form : '#dept_form'
});

$(document).ready(function() {
    $('#depettable').DataTable();
} );

function edit_dept_load(id,name)
{
	$('#dept_id').val(id);
	$('#depname').val(name);
}

// function delete_group_load(id)
// {	
// 	$.post("<?php echo base_url('hci_config/remove_dept')?>",{"grd_id":id},
// 	function(data)
// 	{	
// 		location.reload();
// 	},	
// 	"json"
// 	);
// }

function change_status(id,new_s)
{	
	$.post("<?php echo base_url('hci_config/change_deptstatus')?>",{"dept_id":id,"new_s":new_s},
	function(data)
	{	
		if(data == 'denied')
		{
    		funcres = {status:"denied", message:"You have no right to proceed the action"};
    		result_notification(funcres);
		}
		else
		{
			location.reload();
		}
	},	
	"json"
	);
}

</script>
