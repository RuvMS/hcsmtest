<div class="row">
    <div class="col-md-12">
        <h3 class="page-header"><i class="fa fa-check-square-o"></i> TRANSPORT REGISTRATION</h3>
        <ol class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
            <li><i class="fa fa-car"></i>Transport</li>
            <li><i class="fa fa-check-square-o"></i>Registration</li>
        </ol>
    </div>
</div>
<div>
<section class="panel">
    <header class="panel-heading">
        <div class="col-md-4">
            <div class="col-md-4">
                <h4>REG. NO : </h4>
            </div>
            <div class="col-md-4" id="tfsid">
                <h4>#<?php echo $regindex;?></h4>
            </div>
        </div>
    </header>
    <form class="form-horizontal" role="form" method="post" action="<?php echo base_url('hci_transport/registration_trans')?>" id="reg_form" autocomplete="off" novalidate>
    <div class="panel-body">  
        <div class="row">
            <div class="col-md-4">  
                <br>
                <br><br><br>
                <input type="hidden" name="treg_id" id="treg_id">
                <div class="form-group">
                    <label class="col-md-4 control-label">Branch</label>
                    <div class="col-md-8">
                        <?php 
                            global $branchdrop;
                            global $selectedbr;
                            $extraattrs = 'id="tran_branch" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty"';
                            echo form_dropdown('tran_branch',$branchdrop,$selectedbr, $extraattrs); 
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="student" class="col-md-4 control-label">Student</label>
                    <div class="col-md-8">
                        <select class="form-control select2" id="student"  data-validation="required" data-validation-error-msg-required="Field can not be empty" name="student" style="width: 100%;">
                            <option value=""></option>
                            <?php
                            foreach($stus as $stu):
                                ?>
                                <option value="<?php echo $stu['id'];?>">
                                    <?php echo $stu['st_id']." - ".$stu['family_name'].' '.$stu['other_names'];?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="route" class="col-md-4 control-label">Route</label>
                    <div class="col-md-8">
                        <select class="form-control" onchange="load_pickingpoints(this.value,null)" id="route" name="route" data-validation="required" data-validation-error-msg-required="Field can not be empty" style="width: 100%;">
                            <option value=""></option>
                            <?php
                            foreach($routes as $route):
                                ?>
                                <option value="<?php echo $route['id'];?>">
                                    <?php echo $route['route_title'];?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="pickpoint" class="col-md-4 control-label">Picking Point</label>
                    <div class="col-md-8">
                        <select class="form-control " id="pickpoint" data-validation="required" data-validation-error-msg-required="Field can not be empty" name="pickpoint"  style="width: 100%;">
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="feestruc" class="col-md-4 control-label">Fee Structure</label>
                    <div class="col-md-8">
                        <select class="form-control " id="feestruc" name="feestruc" data-validation="required" data-validation-error-msg-required="Field can not be empty" style="width: 100%;">
                            <option value=""></option>
                            <?php
                            foreach($fss as $fs):
                                if($fs['fs_iscurrent']==1)
                                {
                                    $selected = 'selected';
                                }
                                else
                                {
                                    $selected = '';
                                }
                                ?>
                                <option value="<?php echo $fs['fs_id'];?>" <?php echo $selected?>>
                                    <?php echo $fs['fs_name'];?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-6 control-label">Transport Discounts ,</label>
                </div>
                <br>
                <div class="row" style="margin-left:10px">
                    <div class="col-md-3" style="text-align : center"><strong>Description</strong></div>
                    <div class="col-md-2" style="text-align : center"><strong>Type</strong></div>
                    <div class="col-md-3" style="text-align : center"><strong>% / Val</strong></div>
                    <div class="col-md-2" style="text-align : center"><strong>Amount</strong></div>
                </div>
                <div class="clone_div" id="clone_div" style="margin-left:10px">
                    <div id="clonedInput1" class="clonedInput row">
                        <input type="hidden" class="discinput" name="iId[]" id="iId" value="0">
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" name="studisc" id="studisc" class="form-control" style="width:100%">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <select type="text" class="form-control new-entry" id="adj_type" name="adj_type" style="width:100%">
                                    <option value=''></option>
                                    <option value='A'>(+) Addition</option>
                                    <option value='D'>(-) Deduction</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <select type="text" class="form-control" id="adj_calamttype" name="adj_calamttype" style="width:100%">
                                    <option value=''></option>
                                    <option value='V'>Value</option>
                                    <option value='P'>Percentage (%)</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="text" name="amount" id="amount" class="form-control" style="width:100%">
                            </div>
                        </div>
                        <div class="col-md-2" style="text-align:left; padding-left:5px; padding-right:5px;">
                            <span class="button-group">
                                <button onclick="cloning()" type="button" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-plus"></span></button>
                                <button type="button" name = "remove_entry[]" class="btn btn-default btn-xs remove_entry"><span class="glyphicon glyphicon-minus"></span></button>
                            </span>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
            <div class="col-md-8">
                <table id="regtbl" class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Customer</th>
                            <th>Type</th>
                            <th>Route</th>
                            <th>PickPoint</th>
                            <th>Fee Structure</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if(!empty($registrations))
                            {
                                foreach ($registrations as $regi) 
                                {
                                    echo "<tr>";
                                    echo "<td>".$regi['treg_index']."</td>";
                                    echo "<td>".$regi['cus_name']."</td>";   
                                    echo "<td>".$regi['treg_custtype']."</td>"; 
                                    echo "<td>".$regi['route_title']."</td>"; 
                                    echo "<td>".$regi['place_name']."</td>"; 
                                    echo "<td>".$regi['fs_name']."</td>"; 
                                    echo "<td><a class='btn btn-info btn-xs' onclick='event.preventDefault();edit_transregi(".$regi['treg_id'].",".$regi['treg_customer'].",".$regi['treg_feestructure'].",".$regi['treg_route'].",".$regi['treg_pickpoint'].")'>Edit</a></td>";
                                    echo "</tr>";
                                }
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <button type="submit" class="btn btn-info">Save</button> 
        <button onclick="event.preventDefault();$('#reg_form').trigger('reset');$('#treg_id').val('');" class="btn btn-default">Reset</button>
    </div>
    </form>
</section>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/select2/select2.full.min.js'); ?>"></script>

<link rel="stylesheet" href="<?php echo base_url('assets/select2/select2.min.css') ?>">
<script type="text/javascript">
$.validate({
    form : '#reg_form'
});

var table = $('#regtbl').DataTable( {
        "pageLength": 5,
        "dom" : '<"row"<"col-md-6"l><"col-md-6"f>>rt<"row"<"col-md-4"i><"col-md-8"p>><"clear">',
    } );

$("#student").select2();

var cloneid=0;

function cloning()
{ 
    cloneid+=1;
    var container = document.getElementById('clone_div');
    var clone = $('#clonedInput1').clone();

    clone.find('#iId').attr('id','iId_' + cloneid);
    clone.find('#iId_'+cloneid).val(cloneid);

    clone.find('#studisc').attr('name','studisc_' + cloneid);
    clone.find('#studisc').attr('id','studisc_' + cloneid);
    clone.find('#studisc_'+cloneid).val('');

    clone.find('#adj_type').val('');
    clone.find('#adj_type').attr('name','adj_type_' + cloneid);
    clone.find('#adj_type').attr('id','adj_type_' + cloneid);

    // clone.find('#adj_fee').val('');
    // clone.find('#adj_fee').attr('name','adj_fee_' + cloneid);
    // clone.find('#adj_fee').attr('id','adj_fee_' + cloneid);

    clone.find('#adj_calamttype').val('');
    clone.find('#adj_calamttype').attr('name','adj_calamttype_' + cloneid);
    clone.find('#adj_calamttype').attr('id','adj_calamttype_' + cloneid);

    clone.find('#amount').val('');
    clone.find('#amount').attr('name','amount_' + cloneid);
    clone.find('#amount').attr('id','amount_' + cloneid);
   
    clone.find('.remove_entry').attr('id',cloneid);
    clone.find('.remove_entry').attr('onclick','$(this).parents(".clonedInput").remove()');
 
    $('.clone_div').append(clone);
    $('.clone_div').append('<br>');
    $("html").getNiceScroll().resize();
}


function load_pickingpoints(id,place)
{
    $.post("<?php echo base_url('hci_transport/load_pickingpoints')?>",{'id':id},
    function(data)
    {
        if(data == 'denied')
        {
            funcres = {status:"denied", message:"You have no right to proceed the action"};
            result_notification(funcres);
        }
        else
        { 
            $('#pickpoint').empty();
            $('#pickpoint').append('<option value=""></option>');
            if(data.length>0)
            {
                for (i = 0; i<data.length; i++) 
                {
                    sele_text = '';

                    if(place!=null)
                    {
                        if(data[i]['tr_place_id']==place)
                        {
                            sele_text = 'selected';
                        }
                    }

                    $('#pickpoint').append('<option value="'+data[i]['tr_place_id']+'" '+sele_text+'>'+data[i]['place_name']+'</option>');
                }                         
            }
        }
    },  
    "json"
    );
}

function edit_transregi(id,cust,fs,route,place)
{
    $('#treg_id').val(id);
    $('#student').val(cust).trigger('change');
    $('#feestruc').val(fs);
    $('#route').val(route);
    load_pickingpoints(fs,place);
    //$('#pickpoint').val(place);
}

</script>