<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/jquery-confirm.css'); ?>"> 
<script type="text/javascript" src="<?php echo base_url('js/jquery-confirm.js')?>"></script><!--jquery-->
<div class="row">
    <div class="col-md-12">
        <h3 class="page-header"><i class="fa fa-usd"></i> Receipt</h3>
        <ol class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
            <li><i class="fa fa-bar-chart-o"></i>Accounts</li>
            <li><i class="fa fa-usd"></i>Receipt</li>
        </ol>
    </div>
</div>
<div>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a id="lookup_tab" href="#lookuppanel" aria-controls="lookuppanel" role="tab" data-toggle="tab">Receipt Lookup</a></li>
        <li role="presentation"><a id="create_tab" href="#createpanel" aria-controls="createpanel" role="tab" data-toggle="tab">Create Receipt</a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="lookuppanel">
            <div class="panel">
                <header class="panel-heading">
                    Lookup
                </header>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="nav nav-pills">
                                <li role="presentation" id="r_pill"><a href="#" onclick="event.preventDefault();load_paymentlist('t');$('#type_temp').val('t')">Processed Receipts</a></li>
                                <li role="presentation" id="p_pill"><a href="#" onclick="event.preventDefault();load_paymentlist('p');$('#type_temp').val('p')">Pending to Process</a></li>
                                <!-- <li role="presentation" id="l_pill"><a href="#" onclick="event.preventDefault();load_paymentlist('l');$('#type_temp').val('l')">Left</a></li> -->
                            </ul>
                            <input type="hidden" name="type_temp" id="type_temp" value="t">
                        </div>
                    </div>
                    <table id="payTable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Rec.No</th>
                                <th>Cus.Type</th>
                                <th>Customer</th>
                                <th>Description</th>
                                <th>Receipt Date</th>
                                <th>Receipt Amount</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="createpanel">
        	<form class="form-horizontal" role="form" method="post" action="<?php echo base_url('hci_payment/save_payment')?>" id="payment_form" autocomplete="off" novalidate>
            <div class="panel">
                <header class="panel-heading" style="padding-top: 1px">
                    <div class="col-md-4">
    					<div class="col-md-6">
		                    <h4>RECEIPT NO : </h4>
		                </div>
		                <div class="col-md-6" id="payment_id">
		                	<h4><?php if(!empty($_SESSION['u_branch'])){echo $payindex[$_SESSION['u_branch']];}?></h4>
		                </div>
    				</div>
    				<div class="col-md-3">
    					<div class="form-group">
    						<label for="payment_branch" class="col-md-3 control-label" style="font-size: 12px;padding-top: 0px">Branch</label>
		                  	<div class="col-md-8">
		                  		<?php 
		                  			global $branchdrop;
		                  			global $selectedbr;
		                  			$extraattrs = 'id="payment_branch" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" onchange="loadindex(this.value)"';
		                  			echo form_dropdown('payment_branch',$branchdrop,$selectedbr, $extraattrs); 
		                  		?>
		                  </div>
		              	</div>
    				</div>
    				<div class="col-md-5">
    					<div class="form-group">
    						<label for="pay_stutype" class="col-md-4 control-label" style="font-size: 12px;padding-top: 0px">Type of Receipt</label>
			                <div class="col-md-8">
			                    <select class="form-control select2" id="inv_type"  data-validation="required" data-validation-error-msg-required="You must select the payment type to enter the amount" name="inv_type" onchange="change_view(this.value)" style="width: 100%;">
			                        <option value=""></option>
			                        <option value="INV">INVOICE</option>
			                        <option value="ADV">ADVANCE</option>
			                    </select>
			                </div>
    					</div>
    				</div>
                </header>
                <div class="panel-body">
                	<div class="row">
                		<div class="col-md-4">
                			<div class="form-group">
                				<label for="pay_stutype" class="col-md-4 control-label">Cus. Type</label>
				                <div class="col-md-8">
				                    <select class="form-control select2" id="pay_custype"  data-validation="required" data-validation-error-msg-required="Select customer type for payment" name="pay_custype" style="width: 100%;" onchange="load_customers(this.value)">
				                        <option value=""></option>
				                        <option value="STUDENT">Registered Student</option>
				                        <option value="TEMPSTU">Non-Registered Student</option>
				                        <option value="STAFF">Staff</option>
				                        <option value="EXTERNAL">External Customer</option>
				                    </select>
				                </div>
                			</div>
	    				</div>
	    				<div class="col-md-8">
                			<div class="form-group">
                				<label for="pay_student" class="col-md-2 control-label">Customer</label>
				                <div class="col-md-10">
				                    <select class="form-control select2" id="pay_customer"  data-validation="required" data-validation-error-msg-required="Customer can not be empty" name="pay_customer" style="width: 100%;" onchange="load_outstanding(this.value)">
				                        <option value=""></option>
				                    </select>
				                </div>
                			</div>
                		</div>
                	</div>
                	<br>
                	<div class="row">
                		<div class="col-md-4">
                			<div class="form-group">
		    					<label for="pay_totamt" class="col-md-4 control-label">Paid Amount</label>
				                <div class="col-md-8">
				                    <input type="text" data-validation="required number"  data-validation-allowing="float" data-validation-decimal-separator="." data-validation-error-msg-required="Amount can not be empty" class="form-control" id="pay_totamt" name="pay_totamt" style="width: 100%;text-align : right;padding-right:25px">
				                </div>
				            </div>
	    				</div>
	    				<div class="col-md-8">
                			<div class="form-group">
	                			<label for="pay_description" class="col-md-2 control-label">Description</label>
				                <div class="col-md-10">
				                    <input type="text" data-validation="required" data-validation-error-msg-required="Description can not be empty" class="form-control" id="pay_description" name="pay_description" style="width: 100%;">
				                </div>
				            </div>
                		</div>
                	</div>
                	<br>
                	<div class="row">
                		<div class="col-md-4">
                			<div class="form-group">
                				<label for="pay_outstands" class="col-md-4 control-label">Outstanding Bal.</label>
				                <div class="col-md-8">
				                    <input type="text" class="form-control" id="pay_outstands" name="pay_outstands" style="width: 100%;text-align : right;padding-right:25px" readonly>
				                </div>
                			</div>
	    				</div>
	    				<div class="col-md-4">
                			<div class="form-group">
                				<label for="pay_cohbalance" class="col-md-4 control-label">OnHandBalance</label>
				                <div class="col-md-8">
				                    <div class="input-group">
								      	<input type="text" class="form-control" id="pay_cohbalance" name="pay_cohbalance" style="width: 100%;text-align : right;padding-right:25px" readonly>
								      	<div class="input-group-addon"><input type="checkbox" id="usecoh" name="usecoh"></div>
								    </div>
								    check to use in calculation
				                </div>
                			</div>
                		</div>
                		<div class="col-md-4">
                			<div class="form-group">
				                <button type="submit" onclick="event.preventDefault();manual_view();" name="manualbtn" id="manualbtn" class="btn btn-info btn-sm">Manual Partition</button>
				                <button type="submit" onclick="event.preventDefault();automatic_paymentpartition();" name="automaticbtn" id="automaticbtn" class="btn btn-info btn-sm">Automatic Partition</button>
				                <button type="submit" onclick="event.preventDefault();reset_partitions()" name="resetbtn" id="resetbtn" class="btn btn-default btn-sm">Reset</button>
				            </div>
                		</div>
                	</div>
                	<hr>
                	<div class="row">
			    		<div class="col-md-8">
				            <table class="table table-bordered">
				            	<thead>
				            		<tr>
				            			<th>Invoice</th>
				            			<th>NetValue</th>
				            			<th>PaidAmount</th>
				            			<th>TotalCreditN.</th>
				            			<th>TotalDebitN.</th>
				            			<th>Balance</th>
				            			<th><input type='text' style='width: 100%;text-align : right;padding-right:25px' id='temptotdisplay' class='form-control' readonly></th>
				            		</tr>
				            	</thead>
				            	<tbody id="outs_table">
				            		<tr>
				            			<td colspan="7">No outstanding found</td>
				            		</tr>
				            	</tbody>
				            </table>
				            <br>
				            <div class="form-group">
                				<label for="pay_cohbalance" class="col-md-2 control-label">Remarks</label>
				                <div class="col-md-10">
				                    <textarea class="form-control" id="pay_remarks" name="pay_remarks" rows="3" style="width:100%"></textarea>
				                </div>
                			</div>
			    		</div>
			    		<div class="col-md-4">
			    			<div class="panel-heading" style="padding-top:5px">
								<div class="row">
									<div class="col-md-5">Pay. Method</div>
									<div class="col-md-7">
										<select class="form-control" id="paymode"  data-validation="required" data-validation-error-msg-required="You must select a payment method" name="paymode" onchange="showinput(this.value)">
					                        <option value="cash">Cash</option>
					                        <option value="cheque">Cheque</option>
					                        <option value="dirdeposit">Direct Deposit</option>
					                        <option value="card">Card</option>
					                        <option value="nondirect">Non-direct Deposit</option>
					                    </select>
									</div>
								</div>
							</div>
							<div class="panel-body" id="chequediv">
								<div class="form-group">
									<label for="" class="col-sm-3 control-label">Cheque No.</label>
					                <div class="col-sm-9">
					                    <input class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" type="text" name="chequeno" id="chequeno">
					                </div>
								</div>
								<div class="form-group">
					                <label for="" class="col-sm-3 control-label">Tran. Date</label>
					                <div class="col-sm-9">
					                    <div id="" class="input-group date" >
					                        <input class="form-control datepicker" data-validation="required" data-validation-error-msg-required="Field can not be empty" type="text" name="chequedate" id="chequedate"  data-format="YYYY-MM-DD" value="">
					                            <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span>
					                        </span>
					                    </div>
					                </div>
						        </div>
						        <div class="form-group">
					                <label for="" class="col-sm-3 control-label">Dated to</label>
					                <div class="col-sm-9">
					                    <div id="" class="input-group date" >
					                        <input class="form-control datepicker" data-validation="required" data-validation-error-msg-required="Field can not be empty" type="text" name="chequedateddate" id="chequedateddate"  data-format="YYYY-MM-DD" value="">
					                            <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span>
					                        </span>
					                    </div>
					                </div>
						        </div>
						        <div class="form-group">
									<label for="" class="col-sm-3 control-label">Bank</label>
					                <div class="col-sm-9">
					                    <select class="form-control" id="chequebank" name="chequebank"  data-validation="required" data-validation-error-msg-required="Field can not be empty" >
					                    	<option value=""></option>
					                    	<?php 
					                    		foreach ($banks as $bank) 
					                    		{
					                    	?>
					                    		<option value="<?php echo $bank['bnk_id']?>"><?php echo $bank['bnk_code']?> - <?php echo $bank['bnk_name']?></option>
					                    	<?php
					                    		}
					                    	?>
					                    </select>
					                </div>
								</div>
							</div>
							<div class="panel-body" id="dirdiv">
								<div class="form-group">
									<label for="" class="col-sm-3 control-label">Account No.</label>
					                <div class="col-sm-9">
					                    <input class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" data-validation-depends-on='paymode' data-validation-depends-on-value='dirdeposit' type="text" name="dir_account" id="dir_account">
					                </div>
								</div>
				                <div class="form-group">
				                    <label for="" class="col-sm-3 control-label">Tran. Date</label>
				                    <div class="col-sm-9">
				                        <div id="" class="input-group date" >
				                            <input class="form-control datepicker" data-validation="required" data-validation-error-msg-required="Field can not be empty" data-validation-depends-on='paymode' data-validation-depends-on-value='dirdeposit' type="text" name="bankdate" id="bankdate"  data-format="YYYY-MM-DD" value="">
				                                <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span>
				                            </span>
				                        </div>
				                    </div>
				                </div>
								<div class="form-group">
									<label for="" class="col-sm-3 control-label">Bank</label>
					                <div class="col-sm-9">
					                    <select class="form-control" id="dirbank" data-validation="required" data-validation-error-msg-required="Field can not be empty" data-validation-depends-on='paymode' data-validation-depends-on-value='dirdeposit' name="dirbank">
					                    	<option value=""></option>
					                    	<?php 
					                    		foreach ($banks as $bank) 
					                    		{
					                    	?>
					                    		<option value="<?php echo $bank['bnk_id']?>"><?php echo $bank['bnk_code']?> - <?php echo $bank['bnk_name']?></option>
					                    	<?php
					                    		}
					                    	?>
					                    </select>
					                </div>
								</div>
								<div class="form-group">
									<label for="" class="col-sm-3 control-label">Receipt No.</label>
					                <div class="col-sm-9">
					                    <input class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" data-validation-depends-on='paymode' data-validation-depends-on-value='dirdeposit' type="text" name="receiptno" id="receiptno">
					                </div>
								</div>
							</div>
							<div class="panel-body" id="carddiv">
								<div class="form-group">
									<label for="" class="col-sm-3 control-label">Card No.</label>
					                <div class="col-sm-9">
					                    <input class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" type="text" name="cardno" id="cardno">
					                </div>
								</div>
								<div class="form-group">
									<label for="" class="col-sm-3 control-label">Card Type</label>
					                <div class="col-sm-9">
					                    <select class="form-control select2" id="cardtype"  data-validation="required" data-validation-error-msg-required="Field can not be empty" name="cardtype" style="width: 100%;">
					                        <option value=""></option>
					                        <option value="VISA">VISA</option>
					                        <option value="MASTER">MASTER</option>
					                        <option value="AMEX">AMEX</option>
					                    </select>
					                </div>
								</div>
								<div class="form-group">
									<label for="" class="col-sm-3 control-label">Provider Bank</label>
					                <div class="col-sm-9">
					                    <select class="form-control" id="cardbank" name="cardbank" data-validation="required" data-validation-error-msg-required="Field can not be empty">
					                    	<option value=""></option>
					                    	<?php 
					                    		foreach ($banks as $bank) 
					                    		{
					                    	?>
					                    		<option value="<?php echo $bank['bnk_id']?>"><?php echo $bank['bnk_code']?> - <?php echo $bank['bnk_name']?></option>
					                    	<?php
					                    		}
					                    	?>
					                    </select>
					                </div>
								</div>
								<div class="form-group">
									<label for="" class="col-sm-3 control-label">Paid Bank</label>
					                <div class="col-sm-9">
					                    <select class="form-control" id="machinebank" name="machinebank" data-validation="required" data-validation-error-msg-required="Field can not be empty">
					                    	<option value=""></option>
					                    	<?php 
					                    		foreach ($banks as $bank) 
					                    		{
					                    	?>
					                    		<option value="<?php echo $bank['bnk_id']?>"><?php echo $bank['bnk_code']?> - <?php echo $bank['bnk_name']?></option>
					                    	<?php
					                    		}
					                    	?>
					                    </select>
					                </div>
								</div>
							</div>
			    		</div>
			    	</div>
                </div>
				<div class="panel-footer">
			    	<div class="form-group">
			          	<div class="col-md-11">
			              	<button type="submit" name="save_btn" id="save_btn" class="btn btn-info" onclick="event.preventDefault();save_payment()">Save</button> 
			                <button type="submit" onclick="event.preventDefault();confirm_process()" name="proc_btn" id="proc_btn" class="btn btn-info">Save & Process</button>
			              	<!-- <button type="submit" class="btn btn-default">Reset</button> -->
			          	</div>
			      	</div>
			    </div>
			    </form>
            </div>
        </div>
    </div>
</div>

<!--start test modal-->			
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">		
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">#<span id="paym_id"></span></h4>
			</div>
			
			<div class="modal-body">				
				<div class="col-md-12">
					<section class="panel">
						<div class="panel-body">							
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12"><div class="col-md-12">
											<div style="background:#003d99; height:16px;"></div>
									</div></div>
									
								</div><br/>
																	
								<div class="col-md-12 ">
										<div class="row">										
												<div class="col-md-7" id="cmp_id">
													<br/><span width="100px" id="cmp_id"></span><br/>
													<br/><span width="100px" id="cmp_id"></span><br/>
												</div>
											 
												<div class="col-md-5">
													<img src="########" align="right"/>
												</div>
											
										</div><br/>
										<div class="row">
											<div class="col-md-7">
												<span width="100px" id="customer_id"></span><br/>
												
											</div>
											<div class="col-md-5" >											
													<label style="width:75px; font-weight:bold;">Recipt:</label><span  align="center" id="recipt_id"> </span>
													<br />
													<label style="width:75px;font-weight:bold;">Recipt Date: </label><span  align="center" id="recipt_date"></span>
													<br />			
																		
											</div>
										</div>
									</div>										
										<div class="col-md-12">
											<hr style="background-color:#a1a1a1; height:3px;">											
											<div class="col-md-6">													
												<label style="font-size:18px;font-weight:bold;">Payment For :</label>												
											</div>
											<div class="col-md-6">
												<span style="font-size:14px; align:right;" id="total_pay"> </span>														
											</div><br/>																
											<hr style="background-color:#a1a1a1; height:3px;">													
										</div>					
										
								<div class="col-md-12">								
									<table class="table table-bordered">
										<thead>
											 <tr>
												<th>Description</th>
												<th>Amount (Rs:)</th>
												
												<!--<th style="font-size:14px;font-weight:bold;" width="25%">Outstanding</th>-->
												
											</tr>
										</thead>
										<tbody id="pay_tot">
											
										</tbody>
									</table>
								</div><br/>
								<div class="col-md-12"><label style="font-weight:bold;">Remarks :<span id="remarks"></span><br/><span id="remark"></span></label></div>								
								<br/>
								<div class="row">
									<div class="col-md-12">
											<div class="col-md-8" id="term_id">Thank You</div>
											<!--<div class="col-md-4" id="signature_id">Signature</div>-->
									</div>
								</div><br/><br/>
								<div class="col-md-12"><div class="col-md-12" style="background:#003d99;height:16px;" ></div></div>
								<div class="col-md-12"> <div class="col-md-6" style="font-size:10px;"><label>Created By :<span id="user"/></label></div> <div class="col-md-6" style="text-align:right;font-size:10px;"><label>Created Date :<span  id="created_date"/></label></div> </div>
							</div>
					</div>
				</section>				
			</div>			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>										  
			</div>
		</div>								
	</div>
</div>
</div>

<!--end test modal-->	




<script type="text/javascript" src="<?php echo base_url('js/moment.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/bootstrap-datetimepicker.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/select2/select2.full.min.js'); ?>"></script>

<link rel="stylesheet" href="<?php echo base_url('assets/select2/select2.min.css') ?>">
<script src='<?php echo base_url("assets/datepicker/bootstrap-datepicker.js")?>'></script>
<link rel="stylesheet" href="<?php echo base_url('assets/datepicker/datepicker3.css')?>">
<script type="text/javascript">

function loadindex(id)
{
	indexary = jQuery.parseJSON('<?php echo json_encode($payindex)?>');
	$('#payment_id').empty();
	$('#payment_id').append(indexary[id]);
}

$(document).ready(function() {
    $('#pay_customer').prop('disabled', true);
    $("#pay_totamt").attr("readonly", true); 
    $('#chequediv').hide();
	$('#dirdiv').hide();
	$('#carddiv').hide();

    load_paymentlist('t');
});

$('.datepicker').datepicker({
    autoclose: true
});

$.validate({
	modules : 'logic',
   	form : '#payment_form'
});

$("#pay_customer").select2();

function confirm_process()
{
	$.confirm({
        title: 'Process Payment',
        content: 'Are you sure you want to save and process payment? ',
        confirm: function(){
            $("#proc_form").append("<input type='hidden' name='btn_type' value='process' />");
            $("#proc_form" ).submit();
        },
        cancel: function(){
            
        }
    });
}

function save_payment()
{
	$('#errorpanel').empty();
	type=$('#inv_type').val();

	if(type=='ADV')
	{
		$("#payment_form" ).submit();
	}
	else
	{
		amt_errors = 0;
		total = 0;
		$('.invamt').each(function(){

			temp = this.id.split('_');
			curbal = Number($('#invbal_'+temp[1]).val());

			if(this.value>curbal)
			{
				++amt_errors;
			}
			total += Number(this.value);   
		});

		totpayment = Number($('#pay_totamt').val());

		if($('#usecoh').is(':checked'))
		{
			totpayment +=  Number($('#pay_cohbalance').val());
		}

		if(amt_errors>0)
		{
			$('#js_notif_alerts').append('<div id="notif_alerts" class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Paid Amount Exceeds the Balance to pay. Check the amounts.</div></div>');
		}
		else
		{
			if($('#pay_totamt').val()<=0)
			{
				$('#js_notif_alerts').append('<div id="notif_alerts" class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Amount Can not be Zero or (-)!</div></div>');
			}
			else if(parseFloat($('#pay_totamt').val()) != parseFloat($('#temptotdisplay').val()))
			{
				$('#js_notif_alerts').append('<div id="notif_alerts" class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Invoice total and paid total are mismatched!</div></div>');
			}
			else if(totpayment<total)
			{
				$('#js_notif_alerts').append('<div id="notif_alerts" class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Amount should be less than total of invoice amount</div></div>');
			}
			else
			{
				$("#payment_form" ).submit();
			}
		}
	}
    
}

function load_outstanding(cus)
{
	if(cus!='')
	{
		type = $('#pay_custype').val();
		$('#outs_table').empty();
		$.post("<?php echo base_url('hci_payment/load_outstanding')?>",{'cus':cus,'type':type},
		function(data)
		{
			if(data == 'denied')
			{
        		funcres = {status:"denied", message:"You have no right to proceed the action"};
        		result_notification(funcres);
			}
			else
			{
				$('#pay_outstands').val(data['outs']['outs_amount']);
				$('#pay_cohbalance').val(data['outs']['outs_cohbalance']);

				tempinvtot = 0;

				if(data['invoices'].length>0)
				{
					for (i = 0; i<data['invoices'].length; i++) 
					{
						if(data['invoices'][i]['inv_ispaid']==0)
						{
							$('#outs_table').append("<tr><td>[ "+data['invoices'][i]['inv_index']+" ] - "+data['invoices'][i]['inv_description']+"_"+data['invoices'][i]['inv_date']+"</td><td>"+Number(data['invoices'][i]['inv_netamount']).toFixed(2)+"</td><td>"+Number(data['invoices'][i]['inv_paidamount']).toFixed(2)+"</td><td>"+Number(data['invoices'][i]['inv_totcnote']).toFixed(2)+"</td><td>"+Number(data['invoices'][i]['inv_totdnote']).toFixed(2)+"</td><td>"+Number(data['invoices'][i]['inv_balanceamount']).toFixed(2)+"</td><td><input type='text' data-validation-depends-on='inv_type' data-validation-depends-on-value='INV' name='invoice["+data['invoices'][i]['inv_id']+"]' data-validation='required number'  data-validation-allowing='float' data-validation-decimal-separator='.' style='width: 100%;text-align : right;padding-right:25px' id='invoice_"+data['invoices'][i]['inv_id']+"' value='"+Number(data['invoices'][i]['inv_balanceamount']).toFixed(2)+"' class='form-control invamt' onkeyup='validate_input(this.id);calculate_manualtotal();' readonly><input type='hidden' name='invbal_"+data['invoices'][i]['inv_id']+"' id='invbal_"+data['invoices'][i]['inv_id']+"' value='"+Number(data['invoices'][i]['inv_balanceamount']).toFixed(2)+"'></td></tr>");

							inpname = 'invoice['+data['invoices'][i]['inv_id']+']';
							tempinvtot += Number(data['invoices'][i]['inv_balanceamount']);

						}
					}
				}
				else
				{
					$('#outs_table').append("<tr><td colspan='3'>No Outstanding Found</td></tr>");
				}

				$('#temptotdisplay').val(Number(tempinvtot).toFixed(2));
			}
		},	
		"json"
		);
	}
}

function showinput(id)
{
	if(id=='dirdeposit' || id=='nondirect')
	{
		$('#chequediv').hide();
		$('#dirdiv').show();
		$('#carddiv').hide();
	}
	else if(id=='cheque')
	{
		$('#chequediv').show();
		$('#dirdiv').hide();
		$('#carddiv').hide();
	}
	else if(id=='card')
	{
		$('#chequediv').hide();
		$('#dirdiv').hide();
		$('#carddiv').show();
	}
	else
	{
		$('#chequediv').hide();
		$('#dirdiv').hide();
		$('#carddiv').hide();
	}
}

function load_customers(type)
{
	$('#outs_table').empty();
	$('#outs_table').append("<tr><td colspan='3'>No Outstanding Found</td></tr>");

	$('#pay_customer').empty();
	$('#pay_customer').append('<option value=""></option>');
	$("#pay_customer").select2("val", "");

	branch = $('#payment_branch').val();

	if(type=='')
	{
		$('#pay_customer').prop('disabled', true);
	}
	else
	{
		$('#pay_customer').prop('disabled', false);
		$.post("<?php echo base_url('hci_payment/load_customers')?>",{'type':type,'branch':branch},
		function(data)
		{
			if(data == 'denied')
			{
        		funcres = {status:"denied", message:"You have no right to proceed the action"};
        		result_notification(funcres);
			}
			else
			{
				for (i = 0; i<data.length; i++) 
				{
					$('#pay_customer').append('<option value="'+data[i]['cus_id']+'">[ '+data[i]['cus_index']+' ] - '+data[i]['first_name']+' '+data[i]['second_name']+'</option>');
				}
			}
		},	
		"json"
		);
	}
	
}

function manual_view()
{
	$(".invamt").attr("readonly", false); 
	$('#temptotdisplay').val(0.00);
}

function validate_input(id)
{
	// temp = id.split('_');
	// curbal = $('#invbal_'+temp[1]).val();
	// curval = $('#'+id).val();
	// if(curval>curbal)
	// {
	// 	alert('cant exceed the invoice balance');
	// }
}

function automatic_paymentpartition()
{
	total = Number($('#pay_totamt').val());

	if($('#usecoh').is(':checked'))
	{
		total +=  Number($('#pay_cohbalance').val());
	}

	if($('#inv_type').val()=='INV')
	{
		$('.invamt').each(function(){
			if(total>0)
			{
				temp = this.id.split('_');
				curbal = Number($('#invbal_'+temp[1]).val());

				if(curbal<=total)
				{
					$('#'+this.id).val(Number(curbal).toFixed(2));
				}
				else
				{
					$('#'+this.id).val(Number(total).toFixed(2));
				}
				
				total -= curbal;
			}
			else
			{
				$('#'+this.id).val(Number(0).toFixed(2));
			}
		    
		});
	}
	else
	{
		$('#'+this.id).val(Number(0).toFixed(2));
	}

	$('#temptotdisplay').val(Number($('#pay_totamt').val()).toFixed(2));
}

function reset_partitions()
{
	tempinvtot = 0;
	$('.invamt').each(function(){
		temp = this.id.split('_');
		curbal = $('#invbal_'+temp[1]).val();
		$('#'+this.id).val(Number(curbal).toFixed(2));
		tempinvtot += Number(curbal);
	});

	$('#temptotdisplay').val(Number(tempinvtot).toFixed(2));
}

function calculate_manualtotal()
{
	tempinvtot = 0;
	$('.invamt').each(function(){
		tempinvtot += Number(this.value);
	});

	$('#temptotdisplay').val(Number(tempinvtot).toFixed(2));
}

function load_paymentlist(stat)
{
    $('#payTable').DataTable().destroy();

    if(stat == 'l')
    {
        $('#r_pill').removeClass("active");
        $('#l_pill').addClass("active ");
        $('#p_pill').removeClass("active");
    }
    else if(stat == 'p')
    {
        $('#r_pill').removeClass("active");
        $('#l_pill').removeClass("active");
        $('#p_pill').addClass("active ");
    }
    else
    {
        $('#r_pill').addClass("active ");
        $('#l_pill').removeClass("active");
        $('#p_pill').removeClass("active");
    }

    datalist = 	[{"data": "pay_index"},
	            {"data": "rec_no"},
	            {"data": "pay_custype"},
	            {"data": "customer"},
	            {"data": "pay_description"},
	            {"data": "pay_date"},
	            {"data": "pay_amount"},
	            {"data": "actions"},]

    $('#payTable').DataTable({
        "processing": true,
        "serverSide": true,
        "dom" : '<"row"<"col-md-6 form-group"l><"col-md-6 form-group text-left"f>>rt<"row"<"col-md-3"i><"col-md-9"p>><"clear">',
        "ajax": {
            url: "<?php echo base_url('hci_payment/load_payments')?>",
            type: 'POST',
            data:{'stat':stat}
        },
        "columns": datalist,
    });

    //$(".dataTables_filter").css('white-space','nowrap');
    //$(".dataTables_filter").css('width','100%');
    $(".dataTables_length select").addClass("form-control ");
    // $(".dataTables_filter label").addClass("control-label ");
    $(".dataTables_filter input").addClass("form-control ");
    $(".dataTables_paginate a").addClass("btn btn-sm ");


}

function print_receipt(id)
{
    print_docs = new Array();

    // if(id=="all")
    // {
    //     var x = 0;  
    //     $.each($('.inv_chk:checked'),function(index,value)
    //     {
    //         print_docs[x] = this.value;
    //         x = x+1;
    //     });
    // }
    // else
    // {
    print_docs[0] = id;
    // }

    if(print_docs.length > 0)
    {
        $.ajax(
        {
            url : "<?php echo base_url();?>/hci_payment/print_receipt",
            data : {"print_docs" :print_docs},
            type : 'POST',
            async : false,
            cache: false,
            dataType : 'text',
            success:function(data)
            {
            	if(data == 'denied')
				{
	        		funcres = {status:"denied", message:"You have no right to proceed the action"};
	        		result_notification(funcres);
				}
				else
				{
	                //console.log(data);
	                //window.location = '<?php echo base_url('index.php?print_invoice')?>';
	                var windowObject = window.open("data:application/pdf;base64," + data,'PDF','toolbar=0,titlebar=0,scrollbars=0,menubar=0,fullscreen=0');
            	}
            }
        });
    }
}

function change_view(type)
{
	if(type=='')
	{
		$("#pay_totamt").val('');
		$("#pay_totamt").attr("readonly", true);
	}
	else
	{
		if(type=='ADV')
		{
			$('#manualbtn').prop('disabled', true);
			$('#automaticbtn').prop('disabled', true);
			$('#resetbtn').prop('disabled', true);
			$(".invamt").attr("readonly", true); 
			$(".invamt").val(Number(0).toFixed(2));
		}
		else
		{
			$('#manualbtn').prop('disabled', false);
			$('#automaticbtn').prop('disabled', false);
			$('#resetbtn').prop('disabled', false);

			if($('#pay_totamt').val()=='')
			{
				reset_partitions();
			}
			else
			{
				automatic_paymentpartition();
			}
			$(".invamt").attr("readonly", true);
		}
		$("#pay_totamt").attr("readonly", false);
	}
}


/*Started tested*/

function view_paymentrecipt(id)
{
   $.post("<?php echo base_url('hci_payment/load_paymentrecipt')?>",{'id':id},
    function(data)
    { 
    	$('#cmp_id').empty();
    	$('#customer_id').empty();
    	$('#recipt_id').empty();
    	$('#recipt_date').empty();
    	$('#total_pay').empty();
    	$('#remarks').empty();
    	$('#user').empty();
    	$('#created_date').empty();
    	$('#paym_id').empty();
    	$('#pay_tot').empty();

		if(data == 'denied')
		{
    		funcres = {status:"denied", message:"You have no right to proceed the action"};
    		result_notification(funcres);
		}
		else
		{
	        $('#cmp_id').append(data['receipt']['rec_compname']);
			//+"<br/>"+data['receipt']['rec_compaddline1']+"<br/>"+data['receipt']['rec_compaddline2']+"<br/>"+data['receipt']['rec_compaddline3']
			$('#customer_id').append(data['receipt']['rec_cusname']);		
			$('#recipt_id').append("#"+data['receipt']['rec_index']);
			$('#recipt_date').append(data['receipt']['rec_date']);
			$('#total_pay').append(data['receipt']['rec_description']);
			$('#remarks').append(data['receipt']['rec_remarks']);
			$('#user').append(data['receipt']['rec_createdusername']);
			$('#created_date').append(data['receipt']['rec_createddate']);
			$('#paym_id').append(data['receipt']['rec_index']);
	       
			var inv_tot=0;

	        for (i = 0; i<data['invoice'].length; i++) 
	        {
	            $('#pay_tot').append("<tr><td>"+data['invoice'][i]['inv_index']+"&nbsp&nbsp&nbsp"+data['invoice'][i]['inv_description']+"</td><td style='text-align:right'>"+Number(data['invoice'][i]['payinv_amount']).toFixed(2).replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")+"</td></tr>");  
				inv_tot += Number(data['invoice'][i]['payinv_amount']);
	        }
			

	        $('#pay_tot').append("<tr><td style='text-align:right; font-weight:bold;'>Total Invoice Amount (Rs:)</td><td style='text-align:right'>"+Number(inv_tot).toFixed(2)+"</td></tr>");
			 $('#pay_tot').append("<tr><td style='text-align:right; font-weight:bold;'> Receipt Amount (Rs:) </td><td style='text-align:right'>"+Number(data['receipt']['rec_amount']).toFixed(2).replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")+"</td></tr>");
		}
		
	},
    "json"
    );
	
}
/*End tested*/

//.toLocaleString('en-US', { style: 'currency', currency: 'LKR' })





</script>