<div class="row">
	<div class="col-md-12">
		<h3 class="page-header"><i class="fa fa-file-text"></i> INVOICES</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
			<li><i class="fa fa-bar-chart-o"></i>Accounts</li>
			<li><i class="fa fa-file-text "></i>invoice</li>
		</ol>
	</div>
</div>

<div>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a id="pendinginvo_tab" href="#pending_tab" aria-controls="pending_tab" role="tab" data-toggle="tab">pending invoices</a></li>
        <li role="presentation"><a id="reprocess_tab" href="#repros_tab" aria-controls="repros_tab" role="tab" data-toggle="tab">Re-Process invoices</a></li>
        <li role="presentation"><a id="process_tab" href="#pro_tab" aria-controls="pro_tab" role="tab" data-toggle="tab">processed invoice</a></li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="pending_tab">
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <div class="panel-body"> 
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Invoice No</th>
                                        <th>Description</th>
                                        <th>Student</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($invs as $inv) 
                                        {
                                            if($inv['inv_status']=='P')
                                            {
                                                echo "<tr>";
                                                echo '<td><input name="inv_chk[]" class="inv_chk" value="'.$inv['inv_id'].'" type="checkbox"></td>';
                                                echo "<td>".$inv['inv_index']."</td>";
                                                echo "<td>".$inv['inv_description']."</td>";
                                                echo "<td>".$inv['inv_cusname']."</td>";
                                                echo "<td><button type='button' class='btn btn-info btn-xs' data-toggle='modal' data-target='#myModal' onclick='event.preventDefault();view_invoice(".$inv['inv_id'].")'>view</button> | <button type='button' class='btn btn-success btn-xs' onclick='event.preventDefault();print_invoice(".$inv['inv_id'].")'>Print</button></td>";
                                                // echo "<td><button type='button' class='btn btn-warning btn-xs' onclick='event.preventDefault();edit_invoice(".$inv['inv_id'].")'>Edit</button> | <button type='button' class='btn btn-info btn-xs' data-toggle='modal' data-target='#myModal' onclick='event.preventDefault();view_invoice(".$inv['inv_id'].")'>view</button> | <button type='button' class='btn btn-primary btn-xs' data-toggle='modal' data-target='#lettermodal' onclick='event.preventDefault();view_letter(".$inv['inv_id'].")'>Letter</button> | <button type='button' class='btn btn-success btn-xs' onclick='event.preventDefault();print_invoice(".$inv['inv_id'].")'>Print</button></td>";
                                                echo "</tr>";
                                            }
                                        }
                                    ?>
                                </tbody>
                            </table>
                            <div class="form-group">
                                <div class="col-md-11">
                                    <button type="submit" onclick="event.preventDefault();print_invoice('all')" name="regi_btn" class="btn btn-primary">Print Bulk Invoices</button>
                                    <!-- <button type="submit" onclick="event.preventDefault();process_invoice()" name="regi_btn" class="btn btn-success">Process</button> -->
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div> 
        <div role="tabpanel" class="tab-pane" id="repros_tab">
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <div class="panel-body"> 
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Invoice No</th>
                                        <th>Type</th>
                                        <th>Student</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($invs as $inv) 
                                        {
                                            if($inv['inv_status']=='R')
                                            {
                                                echo "<tr>";
                                                echo '<td><input name="inv_chk[]" class="inv_chk" value="'.$inv['inv_id'].'" type="checkbox"></td>';
                                                echo "<td>".$inv['inv_index']."</td>";
                                                echo "<td>".$inv['inv_description']."</td>";
                                                echo "<td>".$inv['inv_cusname']."</td>";
                                                echo "<td><button type='button' class='btn btn-info btn-xs' data-toggle='modal' data-target='#myModal' onclick='event.preventDefault();view_invoice(".$inv['inv_id'].")'>view</button> | <button type='button' class='btn btn-success btn-xs' onclick='event.preventDefault();print_invoice(".$inv['inv_id'].")'>Print</button></td>";
                                                echo "</tr>";
                                            }
                                        }
                                    ?>
                                </tbody>
                            </table>
                            <div class="form-group">
                                <div class="col-md-11">
                                    <button type="submit" onclick="event.preventDefault();print_invoice('all')" name="regi_btn" class="btn btn-primary">Print Bulk Invoices</button>
                                    <!-- <button type="submit" onclick="event.preventDefault();//process_invoice()" name="regi_btn" class="btn btn-success">Re-Process</button> -->
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="pro_tab">
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <div class="panel-body"> 
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Invoice No</th>
                                        <th>Type</th>
                                        <th>Student</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($invs as $inv) 
                                        {
                                            if($inv['inv_status']=='T')
                                            {
                                                echo "<tr>";
                                                echo "<td>".$inv['inv_index']."</td>";
                                                echo "<td>".$inv['inv_description']."</td>";
                                                echo "<td>".$inv['inv_cusname']."</td>";
                                                echo "<td><button type='button' class='btn btn-info btn-xs' data-toggle='modal' data-target='#myModal' onclick='event.preventDefault();view_invoice(".$inv['inv_id'].")'>view</button> | <button type='button' class='btn btn-success btn-xs' onclick='event.preventDefault();print_invoice(".$inv['inv_id'].")'>Print</button></td>";
                                                echo "</tr>";
                                            }
                                        }
                                    ?>
                                </tbody>
                            </table>
<!--                             <div class="form-group">
                                <div class="col-md-11">
                                    <button type="submit" onclick="event.preventDefault();print_invoice('all')" name="regi_btn" class="btn btn-primary">Print Bulk Invoices</button>
                                    <button type="submit" onclick="event.preventDefault();process_invoice()" name="regi_btn" class="btn btn-success">Process</button>
                                </div>
                            </div> -->
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Invoice-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">INVOICE : <span id="inv_id"></span></h4>
            </div>
            <div class="modal-body">
                 <div class="row">
                      <div class="col-md-2">
                          Student : 
                      </div>
                      <div class="col-md-6" id="inv_stu">
                          
                      </div>
                  </div>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Description</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                    <tbody id="inv_fees">
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
            </div>
    </div>
        </div>
</div>
<!-- Modal Letter-->
<!-- <div class="modal fade" id="lettermodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">REMINDER</h4>
            </div>
            <div class="modal-body">
                <br>
                <div class="row"><div class="col-md-2">Stu ID #</div><div class="col-md-10" id="let_stuid"></div></div>  
                <div class="row"><div class="col-md-2">Name</div><div class="col-md-10" id="let_stuname"></div></div> 
                <div class="row"><div class="col-md-2">Class</div><div class="col-md-10" id="let_class"></div></div>
                <br>
                <div class="row"><div class="col-md-12">
                    Dear Parent,<br><br>
                    The payment for Term/ Service and Transport of your child for the Second Term is given below.
                </div></div>
                <div class="row"><div class="col-md-2"></div><div class="col-md-8">
                <table class="table-bordered" style="font-size:12px;width:100%">
                    <thead>
                        <tr>
                            <th style="padding: 3px;">Description</th>
                            <th style="padding: 3px;">Amount</th>
                        </tr>
                    </thead>
                    <tbody id="let_fees">
                    </tbody>
                </table>
                </div></div>
                <div class="row"><div class="col-md-12">
                    We have attached a deposit slip herewith for your convenience. Please fill in all the details given above when making the deposit to the bank and confirm with the Bank Assistant to enter the student ID number (NG xxx) to the bank system. While having the Customer copy for your reference, please hand over the Office copy of the slip to the class teacher through the child�s SRB immediately after the payment. This will avoid miscommunications and inconvenience in the future. 
                    Please note that the Brought Forward mentioned above contains over dues of the First Term fees/ Balance Admission. As per the Management policy on long outstanding, we will be strict on clearing off the over dues before the beginning of the new academic term. 
                    <br>
                    Second Term Fee Payments must be done on or before 15th December 2016.
                    <br>
                    Settlement of all your dues can be paid by cash to the school office or if paid to the bank the deposit slip must be handed over to the school finance division on or before the 17th December 2016 as the school office will be closed for Christmas Holidays.  
                    <br>
                    � The cost of the school magazine is included. Please note that you are required to pay for one copy per family.
                    <br>
                    � Please note that management has decided on a revision of school transport fee with effect from January 2017. 
                    <br>
                    Payments can be made through Debit and Credit cards. However, dated cheques will not be accepted. 
                    We look forward to your invaluable cooperation with regards to this to avoid any further miscommunication.
                    <br><br><br>
                    Thank you<br>

                    Registrar<br>
                    Horizon College International
                </div></div>      
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
    </div>
        </div>
</div>  -->                               
<script type="text/javascript" src="<?php echo base_url('js/moment.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/bootstrap-datetimepicker.min.js'); ?>"></script>
<script type="text/javascript">

function view_invoice(id)
{
    $.post("<?php echo base_url('hci_accounts/load_invoicefees')?>",{'id':id},
    function(data)
    { 
        if(data == 'denied')
        {
            funcres = {status:"denied", message:"You have no right to proceed the action"};
            result_notification(funcres);
        }
        else
        {
            $('#inv_id').empty();
            $('#inv_stu').empty();
            $('#inv_id').append(data['invoice_data']['inv_index']+' - '+data['invoice_data']['inv_description']);
            $('#inv_stu').append(data['invoice_data']['inv_cusname']);
            $('#inv_fees').empty();

            for (i = 0; i<data['invoice_fees'].length; i++) 
            {
                $('#inv_fees').append("<tr><td>"+data['invoice_fees'][i]['invf_feedescription']+"</td><td style='text-align:right'>"+Number(data['invoice_fees'][i]['invf_netamount']).toFixed(2)+"</td></tr>");  
            }

            $('#inv_fees').append("<tr><td style='text-align:right'> G.Total </td><td style='text-align:right'>"+Number(data['invoice_data']['inv_totamount']).toFixed(2)+"</td></tr>");
            $('#inv_fees').append("<tr><td style='text-align:right'> Add </td><td style='text-align:right'>"+Number(Number(data['invoice_data']['inv_totadditions'])+Number(data['invoice_data']['inv_extraadditions'])).toFixed(2)+"</td></tr>");
            $('#inv_fees').append("<tr><td style='text-align:right'> Disc </td><td style='text-align:right'>"+Number(Number(data['invoice_data']['inv_totdiscounts'])+Number(data['invoice_data']['inv_extradiscounts'])).toFixed(2)+"</td></tr>");
            $('#inv_fees').append("<tr><td style='text-align:right'> Net Total </td><td style='text-align:right'>"+Number(data['invoice_data']['inv_netamount']).toFixed(2)+"</td></tr>");
        }
    },  
    "json"
    );
}

// function view_letter(id)
// {
//    $.post("<?php echo base_url('hci_accounts/f')?>",{'id':id},
//     function(data)
//     { 
//         $('#let_stuid').empty();
//         $('#let_class').empty();
//         $('#let_stuid').append(data['invoice_data']['st_id']);
//         $('#let_class').append(data['invoice_data']['family_name']+' '+data['invoice_data']['other_names']);
//         $('#let_fees').empty();

//         tot_amt = 0;
//         addition = 0.00;
//         discount = 0.00;

//         for (i = 0; i<data['invoice_fees'].length; i++) 
//         {
//             if(data['invoice_fees'][i]['fsf_fee']==1)
//             {
//                   // if(paytype==2)
//                   // {
//                   //     paytxt = " : 1st Payment";
//                   // }
//                   // else
//                   // {
//                   //     paytxt = " : Full Payment";
//                   // }
//                 paytxt = "";
//             }
//             else
//             {
//                 paytxt = "";
//             }

//             for(x = 0; x<data['invoice_disc'].length; x++)
//             {
//                 if(data['invoice_disc'][x]['adj_feecat']==data['invoice_fees'][i]['fsf_fee'])
//                 {
//                     addtodisc = false;
//                     if(data['invoice_disc'][x]['at_applyfor']==5)
//                     {
//                         if(data['invoice_disc'][x]['discfee'].length>0)
//                         {
//                             for(a = 0; a<data['invoice_disc'][x]['discfee'].length; a++)
//                             {
//                                 if(data['invoice_disc'][x]['discfee'][a]['aft_fsfee']==data['invoice_fees'][i]['fsf_id'])
//                                 {
//                                     if(data['invoice_fees'][i]['fsf_fee']==1)
//                                     {
//                                         if(data['invoice_data']['inv_description']=='Registration')
//                                         {
//                                             if(data['invoice_disc'][x]['discfee'][a]['aft_tot']==1)
//                                             {
//                                                 addtodisc = true;
//                                             }
//                                             else if(data['invoice_disc'][x]['discfee'][a]['aft_new']==1)
//                                             {
//                                                 addtodisc = true;
//                                             }
//                                         }
//                                         else if(data['invoice_data']['inv_description']=='AnnualPromo')
//                                         {
//                                             if(data['invoice_disc'][x]['discfee'][a]['aft_old']==1)
//                                             {
//                                                 addtodisc = true;
//                                             }
//                                         }
//                                     }
//                                     else
//                                     {
//                                         pay_type = '';
//                                         for(b = 0; b<data['payment_plan'].length; b++)
//                                         {
//                                             if(data['payment_plan'][b]['plan_fee']==data['invoice_fees'][i]['fsf_fee'])
//                                             {
//                                                 pay_type = data['payment_plan'][b]['plan_type'];
//                                             }
//                                         }

//                                         if(data['invoice_disc'][x]['adj_applyfor']==1)
//                                         {
//                                             if(pay_type == 'TA')
//                                             {
//                                                 addtodisc = true;
//                                             }
//                                         }
//                                         else
//                                         {
//                                             addtodisc = true;
//                                         }
//                                     }  
//                                 }
//                             }
//                         }
//                         else
//                         {
//                         addtodisc = true;
//                         }
//                     }
//                     else
//                     {
//                         if(data['invoice_disc'][x]['adj_feecat']==4)
//                         {
//                             sibs = data['invoice_fees'][i]['transiblingary'];
//                             sib_seq = 0;
//                             for(s = 1; s<sibs.length; s++)
//                             {
//                                 if(sibs[s]['es_admissionid']==data['invoice_data']['inv_student'])
//                                 {
//                                     sib_seq = s;
//                                 }
//                             }

//                             if(sib_seq==2 && data['invoice_disc'][x]['at_applyfor']==2)
//                             {
//                                 addtodisc = true;
//                             }

//                             if(sib_seq>=3 && data['invoice_disc'][x]['at_applyfor']==3)
//                             {
//                                 addtodisc = true;
//                             }
//                         }
//                         else
//                         {
//                             addtodisc = true;
//                         }
//                     }

//                     if(addtodisc==true)
//                     {
//                         if(data['invoice_disc'][x]['adj_type']=='A')
//                         {
//                             if(data['invoice_disc'][x]['adj_amttype']=='P')
//                             {
//                                 addition += Number((data['invoice_fees'][i]['invf_amount']*data['invoice_disc'][x]['adj_amount'])/100);
//                             }
//                             else
//                             {
//                                 addition += Number(data['invoice_disc'][x]['adj_amount']);
//                             }
                            
//                         }
//                         else
//                         {
//                             if(data['invoice_disc'][x]['adj_amttype']=='P')
//                             {
//                                 discount += Number((data['invoice_fees'][i]['invf_amount']*data['invoice_disc'][x]['adj_amount'])/100);
//                             }
//                             else
//                             {
//                                 discount += Number(data['invoice_disc'][x]['adj_amount']);
//                             }
//                         }
//                     }
//                 }
//             }

//             tot_amt += Number(data['invoice_fees'][i]['invf_amount']);

//             $('#let_fees').append("<tr><td  style='padding: 3px;'>"+data['invoice_fees'][i]['fc_name']+paytxt+"</td><td style='text-align:right;padding: 3px;'>"+Number(data['invoice_fees'][i]['invf_amount']).toFixed(2)+"</td></tr>");
              
//         }
//         net_tot = (tot_amt+addition)-discount;
//         $('#let_fees').append("<tr><td style='text-align:right;padding: 3px;'> G.Total </td><td style='text-align:right;padding: 3px;'>"+Number(tot_amt).toFixed(2)+"</td></tr>");
//         $('#let_fees').append("<tr><td style='text-align:right;padding: 3px;'> Add </td><td style='text-align:right;padding: 3px;'>"+Number(addition).toFixed(2)+"</td></tr>");
//         // $('#let_fees').append("<tr><td style='text-align:right;padding: 3px;'> Disc </td><td style='text-align:right;padding: 3px;'>"+Number(discount).toFixed(2)+"</td></tr>");
      
//         for(x = 0; x<data['other_adjs'].length; x++)
//         {
//             if(data['other_adjs'][x]['adj_type']=='A')
//             {
//                 if(data['other_adjs'][x]['adj_amttype']=='P')
//                 {
//                     add_amt = Number((net_tot*data['other_adjs'][x]['adj_amount'])/100);
//                 }
//                 else
//                 {
//                     add_amt = Number(data['other_adjs'][x]['adj_amount']);
//                 }

//                 net_tot += add_amt;

//                 $('#let_fees').append("<tr><td style='text-align:right;padding: 3px;'> "+data['other_adjs'][x]['adj_description']+" </td><td style='text-align:right;padding: 3px;'>"+Number(add_amt).toFixed(2)+"</td></tr>");
//             }
//             else
//             {
//                 if(data['other_adjs'][x]['adj_amttype']=='P')
//                 {
//                     ded_amount += Number((net_tot*data['other_adjs'][x]['adj_amount'])/100);
//                 }
//                 else
//                 {
//                     ded_amount += Number(data['other_adjs'][x]['adj_amount']);
//                 }

//                 net_tot -= ded_amount;

//                 $('#let_fees').append("<tr><td style='text-align:right;padding: 3px;'> "+data['other_adjs'][x]['adj_description']+" </td><td style='text-align:right;padding: 3px;'>"+Number(ded_amount).toFixed(2)+"</td></tr>");
//             }
//         }

//         $('#let_fees').append("<tr><td style='text-align:right;padding: 3px;'> Net Total </td><td style='text-align:right;padding: 3px;'>"+Number(net_tot).toFixed(2)+"</td></tr>");
//     },  
//     "json"
//     );
// }

function print_invoice(id)
{
    print_docs = new Array();

    if(id=="all")
    {
        var x = 0;  
        $.each($('.inv_chk:checked'),function(index,value)
        {
            print_docs[x] = this.value;
            x = x+1;
        });
    }
    else
    {
        print_docs[0] = id;
    }

    if(print_docs.length > 0)
    {
        $.ajax(
        {
            url : "<?php echo base_url();?>/hci_accounts/print_invoice",
            data : {"print_docs" :print_docs},
            type : 'POST',
            async : false,
            cache: false,
            dataType : 'text',
            success:function(data)
            {
                if(data == 'denied')
                {
                    funcres = {status:"denied", message:"You have no right to proceed the action"};
                    result_notification(funcres);
                }
                else
                {
                    //console.log(data);
                    //window.location = '<?php echo base_url('index.php?print_invoice')?>';
                    var windowObject = window.open("data:application/pdf;base64," + data,'PDF','toolbar=0,titlebar=0,scrollbars=0,menubar=0,fullscreen=0');
                }
            }
        });
    }
}

function process_invoice()
{
    // invlist = new Array();

    // var x = 0;  
    // $.each($('.inv_chk:checked'),function(index,value)
    // {
    //     invlist[x] = this.value;
    //     x = x+1;
    // });

    // if(invlist.length > 0)
    // {
    //     $.post("<?php echo base_url('hci_accounts/process_invoice')?>",{"invlist":invlist},
    //     function(data)
    //     {   
    //        location.reload();
    //     },  
    //     "json"
    //     );
    // }
}

function edit_invoice(id)
{
    window.location = '<?php echo base_url('hci_accounts/invoice_edit_view')?>?id=' + id;
}
</script>
