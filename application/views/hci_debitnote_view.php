<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/jquery-confirm.css'); ?>"> 
<script type="text/javascript" src="<?php echo base_url('js/jquery-confirm.js')?>"></script><!--jquery-->
<div class="row">
    <div class="col-md-12">
        <h3 class="page-header"><i class="fa fa-level-up"></i><i class="fa fa-file-text"></i></i> DEBIT NOTE</h3>
        <ol class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
            <li><i class="fa fa-bar-chart-o"></i>Accounts</li>
            <li><i class="fa fa-level-up"></i><i class="fa fa-file-text"></i>Debit Note</li>
        </ol>
    </div>
</div>
<div>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a id="lookup_tab" href="#lookuppanel" aria-controls="lookuppanel" role="tab" data-toggle="tab">Lookup</a></li>
        <li role="presentation"><a id="create_tab" href="#createpanel" aria-controls="createpanel" role="tab" data-toggle="tab">Create Debit Note</a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="lookuppanel">
            <div class="panel">
                <header class="panel-heading">
                    Lookup
                </header>
                <div class="panel-body">
                	<table id="dnote_table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Invoice</th>
                                <th>Cus.Type</th>
                                <th>Customer</th>
                                <th>Description</th>
                                <th>Date</th>
                                <th>Amount</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="createpanel">
            <div class="panel">
                <form class="form-horizontal" role="form" method="post" action="<?php echo base_url('hci_accounts/save_debitnote')?>" id="dnote_form" autocomplete="off" novalidate>
			    <div class="panel-heading">
                    <div class="col-md-12">
    					<div class="col-md-2">
		                    <h4>DEBIT NOTE ID : </h4>
		                </div>
		                <div class="col-md-2" id="debitnote_id">
		                	<h4><?php if(!empty($_SESSION['u_branch'])){echo $dnoteindex[$_SESSION['u_branch']];}?></h4>
		                </div>
		                <div class="col-md-4"></div>
		                <div class="col-md-4">
		                	<div class="form-group">
	    						<label for="dnote_branch" class="col-md-3 control-label" style="font-size: 12px;padding-top: 0px">Branch</label>
			                  	<div class="col-md-8">
			                  		<?php 
			                  			global $branchdrop;
			                  			global $selectedbr;
			                  			$extraattrs = 'id="dnote_branch" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" onchange="loadindex(this.value)"';
			                  			echo form_dropdown('dnote_branch',$branchdrop,$selectedbr, $extraattrs); 
			                  		?>
			                  </div>
			              	</div>
		                </div>
    				</div>
			    </div>
			    <div class="panel-body">
			    	<div class="row">
			    		<div class="col-md-12">
			    			<input type="hidden" name="dnote_id" id="dnote_id">
			    			<div class="row">
			    				<div class="col-md-5">
			    					<div class="form-group">
			    						<label for="dnote_stutype" class="col-md-3 control-label">Customer Type</label>
						                <div class="col-md-6">
						                    <select class="form-control select2" id="dnote_custype"  data-validation="required" data-validation-error-msg-required="Select customer type" name="dnote_custype" style="width: 100%;" onchange="load_customers(this.value)">
						                        <option value=""></option>
						                        <option value="STUDENT">Registered Student</option>
						                        <option value="TEMPSTU">Non-Registered Student</option>
						                        <option value="STAFF">Staff</option>
						                        <option value="EXTERNAL">External Customer</option>
						                    </select>
						                </div>
			    					</div>
			    				</div>
			    				<div class="col-md-7">
					                <div class="form-group">
					                	<label for="pay_student" class="col-md-2 control-label">Customer</label>
						                <div class="col-md-9">
						                    <select class="form-control select2" id="dnote_customer"  data-validation="required" data-validation-error-msg-required="Customer can not be empty" name="dnote_customer" style="width: 100%;" onchange="load_outstanding(this.value)">
						                        <option value=""></option>
						                    </select>
						                </div>
					                </div>
					            </div>
			    			</div>
			    			<br>
			    			<div class="row">
			    				<div class="col-md-5">
			    					<div class="form-group">
				    					<label for="dnote_outstands" class="col-md-3 control-label">Outstanding Bal.</label>
						                <div class="col-md-6">
						                    <input type="text" class="form-control" id="dnote_outstands" name="dnote_outstands" style="width: 100%;text-align : right;padding-right:25px" readonly>
						                </div>
				    				</div>
				    				<div class="form-group">
						                <label for="dnote_cohbalance" class="col-md-3 control-label">OnHandBalance</label>
						                <div class="col-md-6">
						                    <input type="text" class="form-control" id="dnote_cohbalance" name="dnote_cohbalance" style="width: 100%;text-align : right;padding-right:25px" readonly>
						                </div>
						            </div>
				    				<div class="form-group">
				    					<label for="dnote_totamt" class="col-md-3 control-label">Amount</label>
						                <div class="col-md-6">
						                    <input type="text" data-validation="required number"  data-validation-allowing="float" data-validation-decimal-separator="." data-validation-error-msg-required="Amount can not be empty" class="form-control" id="dnote_totamt" name="dnote_totamt" style="width: 100%;text-align : right;padding-right:25px">
						                </div>
				    				</div>
				    				<div class="form-group">
				    					<label for="dnote_remarks" class="col-md-3 control-label">Remarks</label>
						                <div class="col-md-9">
						                    <textarea class="form-control" id="dnote_remarks" name="dnote_remarks" rows="3"></textarea>
						                </div>
				    				</div>
			    				</div>
			    				<div class="col-md-7">
			    					<div id="email-error-dialog"></div>
			    					<table class="table table-bordered">
						            	<thead>
						            		<tr>
						            			<th>#</th>
						            			<th>Invoice</th>
						            			<th>NetValue</th>
						            			<th>PaidAmount</th>
						            			<th>TotalCreditN.</th>
						            			<th>TotalDebitN.</th>
						            			<th>Balance</th>
						            		</tr>
						            	</thead>
						            	<tbody id="outs_table">
						            		<tr>
						            			<td colspan="7">No outstanding found</td>
						            		</tr>
						            	</tbody>
						            </table>
			    				</div>
			    			</div>
			    		</div>
			    	</div>
			    </div>
			    <div class="panel-footer">
			    	<div class="form-group">
			          	<div class="col-md-11">
			              	<button type="submit" name="save_btn" id="save_btn" class="btn btn-info" onclick="event.preventDefault();save_dnote()">Save</button> 
			                <button type="submit" onclick="event.preventDefault();confirm_process()" name="proc_btn" id="proc_btn" class="btn btn-info">Save & Process</button>
			              	<!-- <button type="submit" class="btn btn-default">Reset</button> -->
			          	</div>
			      	</div>
			    </div>
			    </form>
            </div>
        </div>
    </div>
</div>


<!--started modal for dnote receipt-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">		
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><span id="dnote_indx"></span></h4>
			</div>			
			<div class="modal-body">				
				<div class="col-md-12">
					<section class="panel">
						<div class="panel-body">							
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<div class="col-md-12">
											<div style="background:#003d99; height:16px;"></div>
										</div>
									</div>
									
								</div><br/>
																	
									<div class="col-md-12 ">
										<div class="row">											
											 
												<div class="col-md-12">
													<img src="########" align="right"/>
												</div>
											
										</div><br/>
										<div class="row">
											<div class="col-md-6">
												<span width="100px" id="dnote_cus"></span><br/>
												
											</div>
											<div class="col-md-6" >											
													<label style="width:100px;font-weight:bold;">Debit Note ID:</label><span  style="text-align:right;" id="dnote_idx2"> </span>
													<br />
													<label style="width:100px; font-weight:bold;">Debit Note Date:</label><span  style="text-align:right;" id="dnote_date"></span>
													<br />										
											</div>
										</div>
									</div>										
									<div class="col-md-12">
											<hr style="background-color:#a1a1a1; height:3px;">											
											<div class="col-md-12">													
												<label style="font-size:18px;font-weight:bold;"><span id="dnote_des"></span></label>												
											</div>																										
											<hr style="background-color:#a1a1a1; height:3px;">													
									</div>					

								<div class="col-md-12"><label style="font-weight:bold;">Remarks :<span id="dnote_remarks"></span><br/><span id="remark"></span></label></div>								
								<br/>
								
								<div class="col-md-12"><div class="col-md-12" style="background:#003d99;height:16px;" ></div></div>
								<div class="col-md-12"> <div class="col-md-6" style="font-size:10px;"><label>Created By :<span id="user"/></label></div> <div class="col-md-6" style="text-align:right;font-size:10px;"><label>Created Date :<span  id="created_date"/></label></div> </div>
							</div>
					</div>
				</section>				
			</div>			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>										  
			</div>
		</div>								
	</div>
</div>
</div>
<!--end modal for dnote receipt-->











<script type="text/javascript" src="<?php echo base_url('js/moment.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/bootstrap-datetimepicker.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/select2/select2.full.min.js'); ?>"></script>

<link rel="stylesheet" href="<?php echo base_url('assets/select2/select2.min.css') ?>">
<script src='<?php echo base_url("assets/datepicker/bootstrap-datepicker.js")?>'></script>
<link rel="stylesheet" href="<?php echo base_url('assets/datepicker/datepicker3.css')?>">
<script type="text/javascript">

function loadindex(id)
{
	indexary = jQuery.parseJSON('<?php echo json_encode($dnoteindex)?>');
	$('#debitnote_id').empty();
	$('#debitnote_id').append(indexary[id]);
}

$(document).ready(function() {
    load_dnotelist();
    $('#dnote_customer').prop('disabled', true);
});

$.validate({
	modules : 'logic',
   	form : '#dnote_form',
   	// validateOnBlur : false, // disable validation when input looses focus
    // errorMessagePosition : 'bottom' // Instead of 'inline' which is default
});

$('.datepicker').datepicker({
    autoclose: true
});

$("#dnote_customer").select2();

function save_dnote()
{
	inv_no = $('input[name=invoice]:checked').val();
	inv_bal = $('#invbal_'+inv_no).val();
	amount = $('#dnote_totamt').val();

	if(amount<=0)
	{
		$('#js_notif_alerts').append('<div id="notif_alerts" class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Amount Can not be Zero or (-)!</div></div>');
	}
	// else if(inv_bal<amount)
	// {
	// 	$('#js_notif_alerts').append('<div id="notif_alerts" class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Amount Exceeds the Invoice AMount</div></div>');
	// }
	else
	{
		$("#dnote_form" ).submit();
	} 
}

function load_outstanding(cus)
{
	type = $('#dnote_custype').val();
	$('#outs_table').empty();
	$.post("<?php echo base_url('hci_payment/load_outstanding')?>",{'cus':cus,'type':type},
	function(data)
	{
		if(data == 'denied')
		{
    		funcres = {status:"denied", message:"You have no right to proceed the action"};
    		result_notification(funcres);
		}
		else
		{
			$('#dnote_outstands').val(data['outs']['outs_amount']);
			$('#dnote_cohbalance').val(data['outs']['outs_cohbalance']);

			if(data['invoices'].length>0)
			{
				for (i = 0; i<data['invoices'].length; i++) 
				{
			   		$('#outs_table').append("<tr><td><input type='radio' id='invoice_"+data['invoices'][i]['inv_id']+"' name='invoice' value='"+data['invoices'][i]['inv_id']+"'/><input type='hidden' name='invbal_"+data['invoices'][i]['inv_id']+"' id='invbal_"+data['invoices'][i]['inv_id']+"' value='"+Number(data['invoices'][i]['inv_balanceamount']).toFixed(2)+"'></td><td>[ "+data['invoices'][i]['inv_index']+" ] - "+data['invoices'][i]['inv_description']+"_"+data['invoices'][i]['inv_date']+"</td><td>"+Number(data['invoices'][i]['inv_netamount']).toFixed(2)+"</td><td>"+Number(data['invoices'][i]['inv_paidamount']).toFixed(2)+"</td><td>"+Number(data['invoices'][i]['inv_totcnote']).toFixed(2)+"</td><td>"+Number(data['invoices'][i]['inv_totdnote']).toFixed(2)+"</td><td>"+Number(data['invoices'][i]['inv_balanceamount']).toFixed(2)+"</td></tr>");

			   		$.validate({
					   	form : '#dnote_form',
					});
				}
			}
			else
			{
				$('#outs_table').append("<tr><td colspan='3'>No Outstanding Found</td></tr>");
			}
		}
	},	
	"json"
	);
}

function load_customers(type)
{
	$('#outs_table').empty();
	$('#outs_table').append("<tr><td colspan='3'>No Outstanding Found</td></tr>");

	$('#dnote_customer').empty();
	$('#dnote_customer').append('<option value=""></option>');
	$("#dnote_customer").select2("val", "");

	branch = $('#dnote_branch').val();

	if(type=='')
	{
		$('#dnote_customer').prop('disabled', true);
	}
	else
	{
		$('#dnote_customer').prop('disabled', false);
		$.post("<?php echo base_url('hci_payment/load_customers')?>",{'type':type,'branch':branch},
		function(data)
		{
			if(data == 'denied')
			{
        		funcres = {status:"denied", message:"You have no right to proceed the action"};
        		result_notification(funcres);
			}
			else
			{
				for (i = 0; i<data.length; i++) 
				{
					$('#dnote_customer').append('<option value="'+data[i]['cus_id']+'">[ '+data[i]['cus_index']+' ] - '+data[i]['first_name']+' '+data[i]['second_name']+'</option>');
				}
			}
		},	
		"json"
		);
	}
}

function load_dnotelist()
{
    $('#dnote_table').DataTable().destroy();

    $('#dnote_table').DataTable({
        "processing": true,
        "serverSide": true,
        "dom" : '<"row"<"col-md-6 form-group"l><"col-md-6 form-group text-left"f>>rt<"row"<"col-md-3"i><"col-md-9"p>><"clear">',
        "ajax": {
            url: "<?php echo base_url('hci_accounts/load_dnotelist')?>",
            type: 'POST',
        },
        "columns": [
            {"data": "dnote_index"},
            {"data": "inv_index"},
            {"data": "dnote_custype"},
            {"data": "dnote_cusname"},
            {"data": "dnote_description"},
            {"data": "dnote_date"},
            {"data": "dnote_totamt"},
            {"data": "actions"},
        ],
    });

    //$(".dataTables_filter").css('white-space','nowrap');
    //$(".dataTables_filter").css('width','100%');
    $(".dataTables_length select").addClass("form-control ");
    // $(".dataTables_filter label").addClass("control-label ");
    $(".dataTables_filter input").addClass("form-control ");
    $(".dataTables_paginate a").addClass("btn btn-sm ");
}

function print_dnote(id)
{
    print_docs = new Array();

    // if(id=="all")
    // {
    //     var x = 0;  
    //     $.each($('.inv_chk:checked'),function(index,value)
    //     {
    //         print_docs[x] = this.value;
    //         x = x+1;
    //     });
    // }
    // else
    // {
    print_docs[0] = id;
    // }

    if(print_docs.length > 0)
    {
        $.ajax(
        {
            url : "<?php echo base_url();?>/hci_accounts/print_dnote",
            data : {"print_docs" :print_docs},
            type : 'POST',
            async : false,
            cache: false,
            dataType : 'text',
            success:function(data)
            {
            	if(data == 'denied')
				{
	        		funcres = {status:"denied", message:"You have no right to proceed the action"};
	        		result_notification(funcres);
				}
				else
				{
	                //console.log(data);
	                //window.location = '<?php echo base_url('index.php?print_invoice')?>';
	                var windowObject = window.open("data:application/pdf;base64," + data,'PDF','toolbar=0,titlebar=0,scrollbars=0,menubar=0,fullscreen=0');
	            }
            }
        });
    }
}


function load_dnote_receipt(id){
	 $.post("<?php echo base_url('hci_accounts/load_dnote_receipt')?>",{'id':id},
	 function(data)
	{
		if(data == 'denied')
		{
    		funcres = {status:"denied", message:"You have no right to proceed the action"};
    		result_notification(funcres);
		}
		else
		{
			$('#dnote_indx').empty();
			$('#dnote_indx').append("#"+data['dnote']['dnote_index']);
			$('#dnote_idx2').empty();
			$('#dnote_idx2').append("#"+data['dnote']['dnote_index']);
			$('#dnote_cus').empty();
			$('#dnote_cus').append(data['dnote']['dnote_cusname']);
			$('#dnote_date').empty();
			$('#dnote_date').append(data['dnote']['dnote_date']);
			$('#dnote_des').empty();
			$('#dnote_des').append(data['dnote']['dnote_description']);
			$('#user').empty();
			$('#user').append(data['dnote']['dnote_createusername']);
			$('#created_date').empty();
			$('#created_date').append(data['dnote']['dnote_createddate']);
			$('#dnote_remarks').empty();
			$('#dnote_remarks').append(data['dnote']['cnote_remarks']);
		}
	},
	"json"
	);



}



</script>