
<div class="row">
    <div class="col-md-12">
        <h3 class="page-header"><i class="fa fa-users"></i> Student Lookup</h3>
        <ol class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
            <li><i class="fa fa-cog"></i>Student</li>
            <li><i class="fa fa-bank"></i>Student Lookup</li>
        </ol>
    </div>
</div>
<div>
    <div class="row">
        <div class="col-md-12">
            <section class="panel">
                <header class="panel-heading">
                    Student Lookup
                </header>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="nav nav-pills">
                                <li role="presentation" id="r_pill"><a href="#" onclick="event.preventDefault();load_student('r');$('#type_temp').val('r')">Registered</a></li>
                                <li role="presentation" id="p_pill"><a href="#" onclick="event.preventDefault();load_student('p');$('#type_temp').val('p')">Pending Registration</a></li>
                                <li role="presentation" id="l_pill"><a href="#" onclick="event.preventDefault();load_student('l');$('#type_temp').val('l')">Left</a></li>
                            </ul>
                            <input type="hidden" name="type_temp" id="type_temp" value="r">
                        </div>
                    </div>
                    <table id="stuTable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>#</th>
<!--                                 <th>Image</th> -->
                                <th>Admission</th>
                                <th>Name</th>
                                <th>Grade</th>
                                <th>Entered Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url('js/moment.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/bootstrap-datetimepicker.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/select2/select2.full.min.js'); ?>"></script>

<link rel="stylesheet" href="<?php echo base_url('assets/select2/select2.min.css') ?>">
<script src='<?=base_url("assets/datepicker/bootstrap-datepicker.js")?>'></script>
<link rel="stylesheet" href="<?=base_url('assets/datepicker/datepicker3.css')?>">
<script type="text/javascript">

$(document).ready(function() 
{
    load_student('r');
} );

function load_student(stat)
{
    $('#stuTable').DataTable().destroy();

    if(stat == 'l')
    {
        $('#r_pill').removeClass("active");
        $('#l_pill').addClass("active ");
        $('#p_pill').removeClass("active");
    }
    else if(stat == 'p')
    {
        $('#r_pill').removeClass("active");
        $('#l_pill').removeClass("active");
        $('#p_pill').addClass("active ");
    }
    else
    {
        $('#r_pill').addClass("active ");
        $('#l_pill').removeClass("active");
        $('#p_pill').removeClass("active");
    }

    $('#stuTable').DataTable({
        "processing": true,
        "serverSide": true,
        "dom" : '<"row"<"col-md-6 form-group"l><"col-md-6 form-group text-left"f>>rt<"row"<"col-md-3"i><"col-md-9"p>><"clear">',
        "ajax": {
            url: "<?php echo base_url('hci_student/load_student')?>",
            type: 'POST',
            data:{'stat':stat}
        },
        "columns": [
            {"data": "id"},
            {"data": "st_id"},
            {"data": "stu_name"},
            {"data": "grd_name"},
            {"data": "entered_date"},
            {"data": "actions"}
        ],
    });

    //$(".dataTables_filter").css('white-space','nowrap');
    //$(".dataTables_filter").css('width','100%');
    $(".dataTables_length select").addClass("form-control ");
    // $(".dataTables_filter label").addClass("control-label ");
    $(".dataTables_filter input").addClass("form-control ");
    $(".dataTables_paginate a").addClass("btn btn-sm ");
}

function load_stuprofview(stu,rt)
{
    window.location = '<?php echo base_url("hci_student/load_stuprofview")?>?id='+stu+'&rt='+rt;
}

function load_stueditview(stu,rt)
{
    $.post("<?php echo base_url('hci_student/set_studentdatasession')?>",{'id':stu,'rt':rt},
    function(data)
    {
       if(data)
       {
            load_edit_page('edit');
       } 
    },  
    "json"
    );
}

function load_tempsturegview(stu,rt)
{
    $.post("<?php echo base_url('hci_student/set_studentdatasession')?>",{'id':stu,'rt':rt},
    function(data)
    {
       if(data)
       {
            load_edit_page('regi');
       } 
    },  
    "json"
    );
}

function load_edit_page(type)
{
    window.location = '<?php echo base_url("hci_student/load_stueditview")?>?type='+type;
}


function load_assigngrade(stu)
{
    
}

function load_paymentinfo(stu)
{
    
}

function load_sturegister(stu)
{
    
}

</script>
<script>
    $(function () {
        //Initialize Select2 Elements
        $("#sib_select").select2();
    });
</script>
<script type="text/javascript">

    $('.datepicker').datepicker({
        autoclose: true
    });

    event_count = 0;

    function load_sibling_data(type)
    {
        sibs = $('#sib_select').val();
        fs   = $('#curr_fs').val();

        $.post("<?php echo base_url('hci_studentreg/load_sibling_data')?>",{'sibs':sibs,'fs':fs,'type':type},
        function(data)
        {
            if(data == 'denied')
            {
                funcres = {status:"denied", message:"You have no right to proceed the action"};
                result_notification(funcres);
            }
            else
            { 
                if(type=='sibs')
                {
                    if(sibs==null)
                    {
                        event_count = 0;
                    }
                    else
                    {
                        if(event_count==0)
                        {
                            if(data['siblings'] != null)
                            {
                                $("#sib_select").val(data['siblings']).trigger("change");
                                event_count++;
                            }
                        }
                    }
                }

                if(data['discounts'] != null)
                {
                    $('#discounts_list').empty();

                    for (i = 0; i<data['discounts'].length; i++) {

                        $('#discounts_list').append('<label class="label_check c_on" for="checkbox-01"><input name="discountschk[]" id="disc_'+data['discounts'][i]['adj_id']+'" value="'+data['discounts'][i]['adj_id']+'" type="checkbox"> '+data['discounts'][i]['adj_description']+'</label><br>');
                    }                         
                }
            }
        },  
        "json"
        );
    }
</script>