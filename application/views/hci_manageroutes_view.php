<div class="row">
    <div class="col-md-12">
        <h3 class="page-header"><i class="fa fa-road"></i> ROUTE MANAGEMENT</h3>
        <ol class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
            <li><i class="fa fa-car"></i>Transport</li>
            <li><i class="fa fa-road"></i>Route</li>
        </ol>
    </div>
</div>
<div>
<section class="panel">
    <header class="panel-heading">
        <div class="col-md-4">
            <div class="col-md-6">
                <h4>ROUTE NO : </h4>
            </div>
            <div class="col-md-6" id="rt_id">
                <h4>#<?php echo $routeindex;?></h4>
            </div>
        </div>
    </header>
    <form class="form-horizontal" role="form" method="post" action="<?php echo base_url('hci_transport/save_route')?>" id="route_form" autocomplete="off" novalidate>
    <div class="panel-body">  
        <div class="row">
            <div class="col-md-5">  
                <br>
                <div class="form-group">
                    <label class="col-md-2 control-label">Branch</label>
                    <div class="col-md-10">
                        <?php 
                            global $branchdrop;
                            global $selectedbr;
                            $extraattrs = 'id="rt_branch" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty"';
                            echo form_dropdown('rt_branch',$branchdrop,$selectedbr, $extraattrs); 
                        ?>
                    </div>
                </div>
                <br>
                <div class="form-group">
                    <label for="rt_name" class="col-md-2 control-label">Name</label>
                    <div class="col-md-10">
                        <input type="hidden" name="route_id" id="route_id">
                        <input type="text" data-validation="required" data-validation-error-msg-required="Field can not be empty" class="form-control" id="rt_name" name="rt_name" placeholder="">
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <table id="routetable" class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Route</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($routes as $rt) 
                            {
                                echo "<tr>";
                                echo "<td>".$rt['route_index']."</td>";
                                echo "<td>".$rt['route_title']."</td>";   
                                echo "<td><a class='btn btn-info btn-xs' onclick='event.preventDefault();edit_route(".$rt['id'].",\"".$rt['route_index']."\",\"".$rt['route_title']."\",".$rt['route_branch'].")'>Edit</a></td>";
                                echo "</tr>";
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <button type="submit" class="btn btn-info">Save</button> 
        <button type="submit" class="btn btn-default">Reset</button>
    </div>
    </form>
</section>
</div>
<script type="text/javascript">
$.validate({
    form : '#route_form'
});

var table = $('#routetable').DataTable( {
        "pageLength": 5,
        "dom" : '<"row"<"col-md-6"l><"col-md-6"f>>rt<"row"<"col-md-4"i><"col-md-8"p>><"clear">',
    } );

function edit_route(id,ind,name,branch)
{
    $('#route_id').val(id);
    $('#rt_name').val(name);
    $('#rt_branch').val(branch);
    $('#rt_id').empty();
    $('#rt_id').append('<h4>#'+ind+'</h4>');
}
</script>