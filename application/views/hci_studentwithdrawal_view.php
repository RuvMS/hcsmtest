<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/jquery-confirm.css'); ?>"> 
<script type="text/javascript" src="<?php echo base_url('js/jquery-confirm.js')?>"></script><!--jquery-->
<div class="row">
    <div class="col-md-12">
		<h3 class="page-header"><i class="fa fa-bank"></i> WITHDRAW STUDENT </h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
			<li><i class="fa fa-cog"></i>Student</li>
			<li><i class="fa fa-bank"></i>Withdraw</li>
		</ol>
	</div>
</div>
<div>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a id="lookup_tab" href="#indivwithd" aria-controls="indivwithd" role="tab" data-toggle="tab">Individual Student withdrawal</a></li>
        <li role="presentation"><a id="create_tab" href="#bulkwithd" aria-controls="bulkwithd" role="tab" data-toggle="tab">Bulk Students withdrawal</a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane" id="bulkwithd">
            <div class="panel">
                <header class="panel-heading">
                    Lookup
                </header>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="nav nav-pills">
                                <li role="presentation" id="r_pill"><a href="#" onclick="event.preventDefault();load_paymentlist('t');$('#type_temp').val('t')">Processed Receipts</a></li>
                                <li role="presentation" id="p_pill"><a href="#" onclick="event.preventDefault();load_paymentlist('p');$('#type_temp').val('p')">Pending to Process</a></li>
                                <!-- <li role="presentation" id="l_pill"><a href="#" onclick="event.preventDefault();load_paymentlist('l');$('#type_temp').val('l')">Left</a></li> -->
                            </ul>
                            <input type="hidden" name="type_temp" id="type_temp" value="t">
                        </div>
                    </div>
                    <table id="payTable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Rec.No</th>
                                <th>Cus.Type</th>
                                <th>Customer</th>
                                <th>Description</th>
                                <th>Receipt Date</th>
                                <th>Receipt Amount</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane active" id="indivwithd">
        	<form class="form-horizontal" role="form" method="post" action="<?php echo base_url('hci_student/save_individual_withdrawal')?>" id="indiviForm" autocomplete="off" novalidate>
            <div class="panel">
                <header class="panel-heading" style="padding-top: 1px">
                    <div class="col-md-4">
		                <h4>INDIVIDUAL STUDENT WITHDRAWAL</h4>
    				</div>
                </header>
                <div class="panel-body">
                	<div class="row">
                		<div class="col-md-6">
                			<div class="form-group">
	    						<label for="payment_branch" class="col-md-2 control-label" >Branch</label>
			                  	<div class="col-md-5">
			                  		<?php 
			                  			global $branchdrop;
			                  			global $selectedbr;
			                  			$extraattrs = 'id="withd_branch" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" onchange="loadindex(this.value)" style="width: 100%;"';
			                  			echo form_dropdown('withd_branch',$branchdrop,$selectedbr, $extraattrs); 
			                  		?>
			                  	</div>
			              	</div>
                			<div class="form-group">
                				<label class="col-md-2 control-label">Stu. Type</label>
				                <div class="col-md-5">
				                    <select class="form-control select2" id="withd_stutype"  data-validation="required" data-validation-error-msg-required="Select customer type for payment" name="withd_stutype" style="width: 100%;" onchange="load_customers(this.value)">
				                        <option value=""></option>
				                        <option value="STUDENT">Registered Student</option>
				                        <option value="TEMPSTU">Non-Registered Student</option>
				                        <option value="STAFF">Staff</option>
				                        <option value="EXTERNAL">External Customer</option>
				                    </select>
				                </div>
                			</div>
                			<div class="form-group">
                				<label class="col-md-2 control-label">Student</label>
				                <div class="col-md-10">
				                    <select class="form-control select2" id="withd_student"  data-validation="required" data-validation-error-msg-required="Customer can not be empty" name="withd_student" style="width: 100%;" onchange="load_sibling_data(this.value)">
				                        <option value=""></option>
				                    </select>
				                </div>
                			</div>
                			<div class="form-group">
                				<label class="col-md-2 control-label">Type of Withd.</label>
                				<div class="col-md-10">
                					<div class="radio">
	                					<label>
										  	<input type="radio" name="withd_type" id="withd_type_manage" value="MW"> Withdraw by Management
										</label>
									</div>
									<div class="radio">
										<label>
										  	<input type="radio" name="withd_type" id="withd_type_notend" value="WOE"> Withdraw without ending School Career
										</label>
									</div>
									<div class="radio">
										<label>
										  	<input type="radio" name="withd_type" id="withd_type_end" value="WE"> Withdraw after ending School Career
										</label>
									</div>
                				</div>
							</div>
					        <div class="form-group">
		                        <label class="col-sm-2 control-label">Withdraw On,</label>
		                        <div class="col-sm-4">
		                            <div id="" class="input-group date" >
		                                <input class="form-control datepicker" type="text" name="withd_date" id="withd_date"  data-format="YYYY-MM-DD">
		                                    <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span>
		                                </span>
		                            </div>
		                        </div>
		                    </div>
                		</div>
                		<div class="col-md-6">
                			<div class="form-group">
		    					<label class="col-md-2 control-label">Remarks</label>
				                <div class="col-md-10">
				                	<textarea class="form-control" rows="3"  id="withd_remarks" name="withd_remarks" style="width: 100%; height: 200px;"></textarea>
				                   <!--  <input type="text" data-validation="required number"  data-validation-allowing="float" data-validation-decimal-separator="." data-validation-error-msg-required="Amount can not be empty" class="form-control" > -->
				                </div>
				            </div>
                		</div>
                	</div>
                	<hr>
                	<section class="panel" id="siblingSection"></section>
                </div>
				<div class="panel-footer">
			    	<div class="form-group">
			          	<div class="col-md-11">
			              	<button type="submit" name="save_btn" id="save_btn" class="btn btn-info" onclick="event.preventDefault();save_payment()">Save</button> 
			          	</div>
			      	</div>
			    </div>
			    </form>
            </div>
        </div>
    </div>
</div>

<!--start test modal-->			
<div class="modal fade" id="discModal" tabindex="-1" role="dialog" aria-labelledby="discModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">		
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="discModalLabel">Manage Discount</h4>
			</div>
			<form class="form-horizontal">
			<div class="modal-body">				
				<input type="hidden" name="discId" id="discId" value="0">
				<input type="hidden" name="discStudent" id="discStudent" value="0">
                <div class="form-group">
                	<label class="col-sm-3 control-label">Description</label>
                	<div class="col-md-9">
                    	<input type="text" name="discDescription" id="discDescription" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                	<label class="col-sm-3 control-label">Type</label>
                	<div class="col-md-5">
	                    <select type="text" class="form-control" id="discType" name="discType">
	                        <option value=''></option>
	                        <option value='A'>(+) Addition</option>
	                        <option value='D'>(-) Deduction</option>
	                    </select>
	                </div>
                </div>
                <div class="form-group">
                	<label class="col-sm-3 control-label">Fee Category</label>
                	<div class="col-md-7">
	                    <select class="form-control" id="discFeeCat" name="discFeeCat">
	                        <option value=''></option>
	                        <option value='0'>Any Fee Category</option>
	                        <?php
	                            foreach ($fees as $fee)
	                            {
	                                echo "<option value='".$fee['fc_id']."'>".$fee['fc_name']."</option>";
	                            }
	                        ?>
	                    </select>
                    </div>
                </div>
                <div class="form-group">
                	<label class="col-sm-3 control-label">% / Val.</label>
                	<div class="col-md-5">
	                    <select type="text" class="form-control" id="discCalAmtType" name="discCalAmtType">
	                        <option value=''></option>
	                        <option value='V'>Value</option>
	                        <option value='P'>Percentage (%)</option>
	                    </select>
                    </div>
                </div>
                <div class="form-group">
                	<label class="col-sm-3 control-label">Amount</label>
                	<div class="col-md-5">
                    	<input type="text" name="discAmount" id="discAmount" class="form-control">
                    </div>
                </div>				
			</div>			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>										  
			</div>
			</form>
		</div>								
	</div>
</div>
</div>

<!--end test modal-->	




<script type="text/javascript" src="<?php echo base_url('js/moment.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/bootstrap-datetimepicker.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/select2/select2.full.min.js'); ?>"></script>

<link rel="stylesheet" href="<?php echo base_url('assets/select2/select2.min.css') ?>">
<script src='<?php echo base_url("assets/datepicker/bootstrap-datepicker.js")?>'></script>
<link rel="stylesheet" href="<?php echo base_url('assets/datepicker/datepicker3.css')?>">
<script type="text/javascript">

sibling_list = new Array();

$(document).ready(function() {
    $('#withd_student').prop('disabled', true);
});

$('.datepicker').datepicker({
    autoclose: true
});

$.validate({
	modules : 'logic',
   	form : '#indiviForm'
});

$("#withd_student").select2();

function load_customers(type)
{
	$('#withd_student').empty();
	$('#withd_student').append('<option value=""></option>');
	$("#withd_student").select2("val", "");

	branch = $('#withd_branch').val();

	if(type=='')
	{
		$('#withd_student').prop('disabled', true);
	}
	else
	{
		$('#withd_student').prop('disabled', false);
		$.post("<?php echo base_url('hci_payment/load_customers')?>",{'type':type,'branch':branch},
		function(data)
		{
			if(data == 'denied')
			{
        		funcres = {status:"denied", message:"You have no right to proceed the action"};
        		result_notification(funcres);
			}
			else
			{
				for (i = 0; i<data.length; i++) 
				{
					$('#withd_student').append('<option value="'+data[i]['cus_id']+'">[ '+data[i]['cus_index']+' ] - '+data[i]['first_name']+' '+data[i]['second_name']+'</option>');
				}
			}
		},	
		"json"
		);
	}
}

function load_sibling_data(stu)
{
	for (var member in sibling_list) delete sibling_list[member];

	if(stu!='')
	{
		type = $('#withd_stutype').val();
		$('#siblingSection').empty();
		$.post("<?php echo base_url('hci_student/load_sibling_data')?>",{'stu':stu,'type':type},
		function(data)
		{
			if(data == 'denied')
			{
        		funcres = {status:"denied", message:"You have no right to proceed the action"};
        		result_notification(funcres);
			}
			else
			{
				if(data['siblings'].length>0)
				{
					for (i = 0; i<data['siblings'].length; i++) 
					{
						sibling = data['siblings'][i];
						sibling_list[i] = new Array();
						sibling_list[i]['id'] 			= sibling['es_admissionid'];
						sibling_list[i]['st_id'] 		= sibling['st_id'];
						sibling_list[i]['family_name'] 	= sibling['family_name'];
						sibling_list[i]['other_names'] 	= sibling['other_names'];
						
						exstDiscs = new Array();

						if(sibling['discounts'].length>0)
						{
							for (x = 0; x<sibling['discounts'].length; x++) 
							{
								exstDiscs[x] = new Array();
								exstDiscs[x]['adj_id'] 			= sibling['discounts'][x]['adj_id'];
								exstDiscs[x]['adj_description'] = sibling['discounts'][x]['adj_description'];
								exstDiscs[x]['adj_type'] 		= sibling['discounts'][x]['adj_type'];
								exstDiscs[x]['adj_feecat'] 		= sibling['discounts'][x]['adj_feecat'];
								exstDiscs[x]['adj_applyfor']	= sibling['discounts'][x]['adj_applyfor'];
								exstDiscs[x]['adj_amttype'] 	= sibling['discounts'][x]['adj_amttype'];
								exstDiscs[x]['adj_amount'] 		= sibling['discounts'][x]['adj_amount'];
							}
						}

						sibling_list[i]['discounts'] = exstDiscs;
					}
				}
			}

			loadSiblingsView();
		},	
		"json"
		);
	}
}

function manageDiscountModal(action,student,id,desc,type,feecat,amttype,amount)
{
	if(action == 'E')
	{
		$('#discId').val(id);
		$('#discStudent').val(student);
		$('#discDescription').val(desc);
		$('#discType').val(type);
		$('#discFeeCat').val(feecat);
		$('#discCalAmtType').val(amttype);
		$('#discAmount').val(amount);
	}

	$('#discModal').modal('show');
}

function removeDiscountModal(discId,stu)
{
	tempDiscAry = sibling_list['sib_'+stu]['discs'];

	discIndex = null;

	for (i = 0; i<tempDiscAry.length; i++) 
	{
		if(tempDiscAry[i] == discId)
		{
			discIndex = i;
		}
	}

	sibling_list['sib_'+stu]['discs'].splice(discIndex, 1);

	loadSiblingsView();
}

function loadSiblingsView()
{
	$('#siblingSection').empty();

	if(jQuery.isEmptyObject(sibling_list))
	{
		$('#siblingSection').append('<div class="alert alert-warning" style="position: static;color:#383838;width:100%;text-align:center">No Sibling Found</div>');
	}
	else
	{
		siblingsStr = '';
		for (i = 0; i<sibling_list.length; i++) 
		{
			sibling = sibling_list[i];

			tableStr = '';
			if(sibling['discounts'].length>0)
			{
				for (x = 0; x<sibling['discounts'].length; x++) 
				{
					//editDiscBtn = '<button class="btn btn-default btn-xs" onclick="event.preventDefault();manageDiscountModal(\'E\','+sibling['id']+','+sibling['discounts'][x]['adj_id']+',\''+sibling['discounts'][x]['adj_description']+'\',\''+sibling['discounts'][x]['adj_type']+'\','+sibling['discounts'][x]['adj_feecat']+',\''+sibling['discounts'][x]['adj_amttype']+'\','+sibling['discounts'][x]['adj_amount']+')"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button>';
					remvDiscBtn = '<button class="btn btn-danger btn-xs" onclick="event.preventDefault();removeDiscountModal('+sibling['discounts'][x]['adj_id']+','+sibling['id']+')"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>';

					tableStr += '<tr>';
					tableStr += '<td>'+sibling['discounts'][x]['adj_description']+'</td>';
					tableStr += '<td style="text-align:center">'+(sibling['discounts'][x]['adj_type'] == 'D' ? "( - )" : "( + )")+'</td>';
					tableStr += '<td style="text-align:center">'+sibling['discounts'][x]['adj_feecat']+'</td>';
					tableStr += '<td style="text-align:right">'+(sibling['discounts'][x]['adj_amttype'] == 'P' ? sibling['discounts'][x]['adj_amount']+" %" : Number(sibling['discounts'][x]['adj_amount']).toFixed(2))+'</td>';
					tableStr += '<td style="text-align:center">'+remvDiscBtn+'</td>';
					tableStr += '</tr>';
				}
			}

			siblingsStr += '<div class="row"><div class="col-md-12"><h4>[ '+sibling['st_id']+' ] - '+sibling['family_name']+' '+sibling['other_names']+'</h4></div></div>';
			siblingsStr += '<br>';
			siblingsStr += '<table class="table table-bordered">';
			siblingsStr += '<thead>';
			siblingsStr += '<tr>';
			siblingsStr += '<th>Description</th><th>Type</th><th>Fee Cat.</th><th>Amount</th><th><button class="btn btn-info btn-xs" onclick="event.preventDefault();manageDiscountModal(\'A\','+sibling['id']+')"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button></th>';
			siblingsStr += '</tr>';
			siblingsStr += '</thead>';
			siblingsStr += '<tbody>'+tableStr+'</tbody>';
			siblingsStr += '</table>';
		}

		$('#siblingSection').append('<div class="panel-body">'+siblingsStr+'</div>');
	}

	// siblingsStr = '';
	// if(data['siblings'].length>0)
	// {
	// 	for (i = 0; i<data['siblings'].length; i++) 
	// 	{
			// sibling = data['siblings'][i];

			// tableStr = '';
			// if(sibling['discounts'].length>0)
			// {
			// 	for (x = 0; x<sibling['discounts'].length; x++) 
			// 	{
			
			// 	}
			// }
			// else
			// {
			// 	tableStr += '<tr><td colspan="5">No discount Found</td></tr>';
			// }

			
			// siblingsStr += '<div class="row"><div class="col-md-12"><h4>[ '+sibling['st_id']+' ] - '+sibling['family_name']+' '+sibling['other_names']+'</h4></div></div>';
			// siblingsStr += '<br>';
			// siblingsStr += '<table class="table table-bordered">';
			// siblingsStr += '<thead>';
			// siblingsStr += '<tr>';
			// siblingsStr += '<th>Description</th><th>Type</th><th>Fee Cat.</th><th>Amount</th><th><button class="btn btn-info btn-xs" onclick="event.preventDefault();manageDiscountModal(\'A\','+sibling['es_admissionid']+')"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button></th>';
			// siblingsStr += '</tr>';
			// siblingsStr += '</thead>';
			// siblingsStr += '<tbody>'+tableStr+'</tbody>';
			// siblingsStr += '</table>';

			
			// siblingsStr += '';
			// siblingsStr += '';
			// siblingsStr += '';
			// siblingsStr += '';
			
			// siblingsStr += '';
			// siblingsStr += '';
			// siblingsStr += '';
			// siblingsStr += '';
			// siblingsStr += '';
			// siblingsStr += '';
			// siblingsStr += '';
	// 	}
	// }
	// $('#siblingSection').append('<div class="panel-body">'+siblingsStr+'</div>');
}

function confirm_process()
{
	// $.confirm({
 //        title: 'Process Payment',
 //        content: 'Are you sure you want to save and process payment? ',
 //        confirm: function(){
 //            $("#proc_form").append("<input type='hidden' name='btn_type' value='process' />");
 //            $("#proc_form" ).submit();
 //        },
 //        cancel: function(){
            
 //        }
 //    });
}

function save_payment()
{
	// $('#errorpanel').empty();
	// type=$('#inv_type').val();

	// if(type=='ADV')
	// {
	// 	$("#indiviForm" ).submit();
	// }
	// else
	// {
	// 	amt_errors = 0;
	// 	total = 0;
	// 	$('.invamt').each(function(){

	// 		temp = this.id.split('_');
	// 		curbal = Number($('#invbal_'+temp[1]).val());

	// 		if(this.value>curbal)
	// 		{
	// 			++amt_errors;
	// 		}
	// 		total += Number(this.value);   
	// 	});

	// 	totpayment = Number($('#pay_totamt').val());

	// 	if($('#usecoh').is(':checked'))
	// 	{
	// 		totpayment +=  Number($('#pay_cohbalance').val());
	// 	}

	// 	if(amt_errors>0)
	// 	{
	// 		$('#js_notif_alerts').append('<div id="notif_alerts" class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Paid Amount Exceeds the Balance to pay. Check the amounts.</div></div>');
	// 	}
	// 	else
	// 	{
	// 		if($('#pay_totamt').val()<=0)
	// 		{
	// 			$('#js_notif_alerts').append('<div id="notif_alerts" class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Amount Can not be Zero or (-)!</div></div>');
	// 		}
	// 		else if(parseFloat($('#pay_totamt').val()) != parseFloat($('#temptotdisplay').val()))
	// 		{
	// 			$('#js_notif_alerts').append('<div id="notif_alerts" class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Invoice total and paid total are mismatched!</div></div>');
	// 		}
	// 		else if(totpayment<total)
	// 		{
	// 			$('#js_notif_alerts').append('<div id="notif_alerts" class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Amount should be less than total of invoice amount</div></div>');
	// 		}
	// 		else
	// 		{
	// 			$("#indiviForm" ).submit();
	// 		}
	// 	}
	// }
    
}

function showinput(id)
{
	// if(id=='dirdeposit' || id=='nondirect')
	// {
	// 	$('#chequediv').hide();
	// 	$('#dirdiv').show();
	// 	$('#carddiv').hide();
	// }
	// else if(id=='cheque')
	// {
	// 	$('#chequediv').show();
	// 	$('#dirdiv').hide();
	// 	$('#carddiv').hide();
	// }
	// else if(id=='card')
	// {
	// 	$('#chequediv').hide();
	// 	$('#dirdiv').hide();
	// 	$('#carddiv').show();
	// }
	// else
	// {
	// 	$('#chequediv').hide();
	// 	$('#dirdiv').hide();
	// 	$('#carddiv').hide();
	// }
}

function manual_view()
{
	// $(".invamt").attr("readonly", false); 
	// $('#temptotdisplay').val(0.00);
}

function validate_input(id)
{
	// temp = id.split('_');
	// curbal = $('#invbal_'+temp[1]).val();
	// curval = $('#'+id).val();
	// if(curval>curbal)
	// {
	// 	alert('cant exceed the invoice balance');
	// }
}

function automatic_paymentpartition()
{
	// total = Number($('#pay_totamt').val());

	// if($('#usecoh').is(':checked'))
	// {
	// 	total +=  Number($('#pay_cohbalance').val());
	// }

	// if($('#inv_type').val()=='INV')
	// {
	// 	$('.invamt').each(function(){
	// 		if(total>0)
	// 		{
	// 			temp = this.id.split('_');
	// 			curbal = Number($('#invbal_'+temp[1]).val());

	// 			if(curbal<=total)
	// 			{
	// 				$('#'+this.id).val(Number(curbal).toFixed(2));
	// 			}
	// 			else
	// 			{
	// 				$('#'+this.id).val(Number(total).toFixed(2));
	// 			}
				
	// 			total -= curbal;
	// 		}
	// 		else
	// 		{
	// 			$('#'+this.id).val(Number(0).toFixed(2));
	// 		}
		    
	// 	});
	// }
	// else
	// {
	// 	$('#'+this.id).val(Number(0).toFixed(2));
	// }

	// $('#temptotdisplay').val(Number($('#pay_totamt').val()).toFixed(2));
}

function reset_partitions()
{
	// tempinvtot = 0;
	// $('.invamt').each(function(){
	// 	temp = this.id.split('_');
	// 	curbal = $('#invbal_'+temp[1]).val();
	// 	$('#'+this.id).val(Number(curbal).toFixed(2));
	// 	tempinvtot += Number(curbal);
	// });

	// $('#temptotdisplay').val(Number(tempinvtot).toFixed(2));
}

function calculate_manualtotal()
{
	// tempinvtot = 0;
	// $('.invamt').each(function(){
	// 	tempinvtot += Number(this.value);
	// });

	// $('#temptotdisplay').val(Number(tempinvtot).toFixed(2));
}

function load_paymentlist(stat)
{
    // $('#payTable').DataTable().destroy();

    // if(stat == 'l')
    // {
    //     $('#r_pill').removeClass("active");
    //     $('#l_pill').addClass("active ");
    //     $('#p_pill').removeClass("active");
    // }
    // else if(stat == 'p')
    // {
    //     $('#r_pill').removeClass("active");
    //     $('#l_pill').removeClass("active");
    //     $('#p_pill').addClass("active ");
    // }
    // else
    // {
    //     $('#r_pill').addClass("active ");
    //     $('#l_pill').removeClass("active");
    //     $('#p_pill').removeClass("active");
    // }

    // datalist = 	[{"data": "pay_index"},
	   //          {"data": "rec_no"},
	   //          {"data": "pay_custype"},
	   //          {"data": "customer"},
	   //          {"data": "pay_description"},
	   //          {"data": "pay_date"},
	   //          {"data": "pay_amount"},
	   //          {"data": "actions"},]

    // $('#payTable').DataTable({
    //     "processing": true,
    //     "serverSide": true,
    //     "dom" : '<"row"<"col-md-6 form-group"l><"col-md-6 form-group text-left"f>>rt<"row"<"col-md-3"i><"col-md-9"p>><"clear">',
    //     "ajax": {
    //         url: "<?php echo base_url('hci_payment/load_payments')?>",
    //         type: 'POST',
    //         data:{'stat':stat}
    //     },
    //     "columns": datalist,
    // });

    // //$(".dataTables_filter").css('white-space','nowrap');
    // //$(".dataTables_filter").css('width','100%');
    // $(".dataTables_length select").addClass("form-control ");
    // // $(".dataTables_filter label").addClass("control-label ");
    // $(".dataTables_filter input").addClass("form-control ");
    // $(".dataTables_paginate a").addClass("btn btn-sm ");


}

function print_receipt(id)
{
    print_docs = new Array();

    // if(id=="all")
    // {
    //     var x = 0;  
    //     $.each($('.inv_chk:checked'),function(index,value)
    //     {
    //         print_docs[x] = this.value;
    //         x = x+1;
    //     });
    // }
    // else
    // {
    print_docs[0] = id;
    // }

    if(print_docs.length > 0)
    {
        $.ajax(
        {
            url : "<?php echo base_url();?>/hci_payment/print_receipt",
            data : {"print_docs" :print_docs},
            type : 'POST',
            async : false,
            cache: false,
            dataType : 'text',
            success:function(data)
            {
            	if(data == 'denied')
				{
	        		funcres = {status:"denied", message:"You have no right to proceed the action"};
	        		result_notification(funcres);
				}
				else
				{
	                //console.log(data);
	                //window.location = '<?php echo base_url('index.php?print_invoice')?>';
	                var windowObject = window.open("data:application/pdf;base64," + data,'PDF','toolbar=0,titlebar=0,scrollbars=0,menubar=0,fullscreen=0');
            	}
            }
        });
    }
}

function change_view(type)
{
	if(type=='')
	{
		$("#pay_totamt").val('');
		$("#pay_totamt").attr("readonly", true);
	}
	else
	{
		if(type=='ADV')
		{
			$('#manualbtn').prop('disabled', true);
			$('#automaticbtn').prop('disabled', true);
			$('#resetbtn').prop('disabled', true);
			$(".invamt").attr("readonly", true); 
			$(".invamt").val(Number(0).toFixed(2));
		}
		else
		{
			$('#manualbtn').prop('disabled', false);
			$('#automaticbtn').prop('disabled', false);
			$('#resetbtn').prop('disabled', false);

			if($('#pay_totamt').val()=='')
			{
				reset_partitions();
			}
			else
			{
				automatic_paymentpartition();
			}
			$(".invamt").attr("readonly", true);
		}
		$("#pay_totamt").attr("readonly", false);
	}
}


/*Started tested*/

function view_paymentrecipt(id)
{
   $.post("<?php echo base_url('hci_payment/load_paymentrecipt')?>",{'id':id},
    function(data)
    { 
    	$('#cmp_id').empty();
    	$('#customer_id').empty();
    	$('#recipt_id').empty();
    	$('#recipt_date').empty();
    	$('#total_pay').empty();
    	$('#remarks').empty();
    	$('#user').empty();
    	$('#created_date').empty();
    	$('#paym_id').empty();
    	$('#pay_tot').empty();

		if(data == 'denied')
		{
    		funcres = {status:"denied", message:"You have no right to proceed the action"};
    		result_notification(funcres);
		}
		else
		{
	        $('#cmp_id').append(data['receipt']['rec_compname']);
			//+"<br/>"+data['receipt']['rec_compaddline1']+"<br/>"+data['receipt']['rec_compaddline2']+"<br/>"+data['receipt']['rec_compaddline3']
			$('#customer_id').append(data['receipt']['rec_cusname']);		
			$('#recipt_id').append("#"+data['receipt']['rec_index']);
			$('#recipt_date').append(data['receipt']['rec_date']);
			$('#total_pay').append(data['receipt']['rec_description']);
			$('#remarks').append(data['receipt']['rec_remarks']);
			$('#user').append(data['receipt']['rec_createdusername']);
			$('#created_date').append(data['receipt']['rec_createddate']);
			$('#paym_id').append(data['receipt']['rec_index']);
	       
			var inv_tot=0;

	        for (i = 0; i<data['invoice'].length; i++) 
	        {
	            $('#pay_tot').append("<tr><td>"+data['invoice'][i]['inv_index']+"&nbsp&nbsp&nbsp"+data['invoice'][i]['inv_description']+"</td><td style='text-align:right'>"+Number(data['invoice'][i]['payinv_amount']).toFixed(2).replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")+"</td></tr>");  
				inv_tot += Number(data['invoice'][i]['payinv_amount']);
	        }
			

	        $('#pay_tot').append("<tr><td style='text-align:right; font-weight:bold;'>Total Invoice Amount (Rs:)</td><td style='text-align:right'>"+Number(inv_tot).toFixed(2)+"</td></tr>");
			 $('#pay_tot').append("<tr><td style='text-align:right; font-weight:bold;'> Receipt Amount (Rs:) </td><td style='text-align:right'>"+Number(data['receipt']['rec_amount']).toFixed(2).replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")+"</td></tr>");
		}
		
	},
    "json"
    );
	
}
/*End tested*/

//.toLocaleString('en-US', { style: 'currency', currency: 'LKR' })





</script>