<div class="row">
	<div class="col-md-12">
		<h3 class="page-header"><i class="fa fa-graduation-cap"></i>SUBJECT GROUP</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
			<li><i class="fa fa-cog"></i>Subject</li>
			<li><i class="fa fa-graduation-cap"></i>Subject Group</li>
		</ol>
	</div>
</div>
<div>
	<br>
	<div class="row">
		<div class="col-md-4">
		  	<section class="panel">
			    <header class="panel-heading">
			        Manage Subject Group
			    </header>
		      	<div class="panel-body">	
		          	<form class="form-horizontal" role="form" method="post" action="<?php echo base_url('hci_subject/save_subjectgroup')?>" id="subgrp_form" autocomplete="off" novalidate>
		              	<div class="form-group">
		              		<input type="hidden" id="subgrp_id" name="subgrp_id">
		                  	<label for="subgrp_name" class="col-md-4 control-label">Subject Group</label>
		                  	<div class="col-md-6">
		                      	<input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="subgrp_name" name="subgrp_name" placeholder="">
		                  	</div>
		              	</div>
		              	<div class="form-group">
		                  	<label for="subgrp_schm" class="col-md-4 control-label">Scheme</label>
		                  	<div class="col-md-6">
		                  		<select  class="form-control" id="subgrp_schm" name="subgrp_schm" placeholder="">
		                  			<?php
		                  				if(!empty($schemes))
			          					{
			          						echo "<option value=''></option>";
			          						foreach ($schemes as $schm) 
				          					{
				          						if($schm["schm_status"]=="A")
										    	{
				          							echo "<option value='".$schm['schm_id']."'>".$schm['schm_name']."</option>";
				          						}
				          					}
			          					}
		                  			?>
		                  		</select>
		                  	</div>
		              	</div>
		              	<div class="form-group">
		                  	<label for="subgrp_type" class="col-md-4 control-label">Type</label>
		                  	<div class="col-md-6">
		                      	<select type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="subgrp_type" name="subgrp_type" placeholder="">
		                      		<option value=""></option>
		                      		<option value="C">Compulsory</option>
		                      		<option value="O">Optional</option>
		                      	</select>
		                  	</div>
		              	</div>
		              	<div class="form-group">
		                  	<div class="col-md-offset-2 col-md-11">
		                      	<button type="submit" class="btn btn-info">Save</button> 
		                      	<button type="reset" class="btn btn-default">Reset</button>
		                  	</div>
		              	</div>
		          	</form>
		      	</div>
		  	</section>
	  	</div>
	  	<div class="col-md-8">
		  	<section class="panel">
			    <header class="panel-heading">
			       Look Up
			    </header>
		      	<div class="panel-body">	
		          	<table class="display" id="subgrp_tbl">
		          		<thead>
		          			<tr>
		          				<th>#</th>
		          				<th>Subject Group</th>
		          				<th>Scheme</th>
		          				<th>Compulsory/Optional</th>
		          				<th>Actions</th>
		          			</tr>
		          		</thead>
		          		<tbody>
	          				<?php
	          					$i=1;
	          					if(!empty($subgrplist))
	          					{
	          						foreach ($subgrplist as $subgrp) 
		          					{
		          						$typetxt = 'Compulsory';
		          						if($subgrp['subgrp_type']=='O')
		          						{
		          							$typetxt = 'Optional';
		          						}

		          						$schmid = 0;
		          						if(!empty($subgrp['subgrp_schm']))
		          						{
		          							$schmid = $subgrp['subgrp_schm'];
		          						}

		          						echo "<tr>";
		          						echo "<td>".$i."</td>";
		          						echo "<td>".$subgrp['subgrp_name']."</td>";
		          						echo "<td>".$subgrp['schm_name']."</td>";
		          						echo "<td>".$typetxt."</td>";
		          						echo "<td><a class='btn btn-info btn-xs' onclick='event.preventDefault();edit_subgrp_load(".$subgrp['subgrp_id'].",".$schmid.",\"".$subgrp['subgrp_name']."\",\"".$subgrp['subgrp_type']."\")'><span class='glyphicon glyphicon-pencil' aria-hidden='true'></span></a>| ";

		          	// 					if($subgrp["desig_isused"]==0)
								    	// {
		          	// 						echo "<a class='btn btn-danger btn-xs' onclick='event.preventDefault();delete_group_load
		          	// 					     (".$subgrp['desig_id'].")'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></a>| ";
		          	// 					}

								    	if($subgrp["subgrp_status"]=="A")
								    	{
								    		echo "<button onclick='event.preventDefault();change_status(".$subgrp["subgrp_id"].",\"D\")' class='btn btn-warning btn-xs'><span class='glyphicon glyphicon-ban-circle' aria-hidden='true'></span></button>";
								    	}
								    	else
								    	{
								    		echo "<button onclick='event.preventDefault();change_status(".$subgrp["subgrp_id"].",\"A\")' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-ok-circle' aria-hidden='true'></span></button>";
								    	}
								    	
		          						echo "</td></tr>";

		          						$i++;
		          					}
	          					}
	          				?>
		          		</tbody>
		          	</table>
		      	</div>
		  	</section>
	  	</div>
	</div>
</div>
<script type="text/javascript">

$.validate({
   	form : '#subgrp_form'
});

$(document).ready(function() {
    $('#subgrp_tbl').DataTable();
} );

function edit_subgrp_load(id,schm,name,type)
{
	$('#subgrp_id').val(id);
	$('#subgrp_name').val(name);
	$('#subgrp_type').val(type);
	$('#subgrp_schm').val(schm);
}

// function delete_group_load(id)
// {	
// 	$.post("<?php echo base_url('hci_subject/remove_dept')?>",{"grd_id":id},
// 	function(data)
// 	{	
// 		location.reload();
// 	},	
// 	"json"
// 	);
// }

function change_status(id,new_s)
{	
	$.post("<?php echo base_url('hci_subject/change_subgrpstatus')?>",{"subgrp_id":id,"new_s":new_s},
	function(data)
	{	
		if(data == 'denied')
		{
    		funcres = {status:"denied", message:"You have no right to proceed the action"};
    		result_notification(funcres);
		}
		else
		{
			location.reload();
		}
	},	
	"json"
	);
}

</script>
