<div class="row">
	<div class="col-md-12">
		<h3 class="page-header"><i class="fa fa-bank"></i> ADJUSMENT ( Additions / Deductions )</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
			<li><i class="fa fa-cog"></i>Settings</li>
			<li><i class="fa fa-bank"></i>Fee Structure</li>
			<li><i class="fa fa-bank"></i>Adjusments</li>
		</ol>
	</div>
</div>
<div>
	<!-- Nav tabs -->
  	<ul class="nav nav-tabs" role="tablist">
  		<li role="presentation" class="active"><a href="#lookuptab" aria-controls="lookuptab" role="tab" data-toggle="tab">Create / Edit Adjusments</a></li>
    	<li role="presentation"><a href="#create_tab" aria-controls="create_tab" role="tab" data-toggle="tab">Create / Edit Adjusments</a></li>
    	<li role="presentation"><a href="#config_tab" aria-controls="config_tab" role="tab" data-toggle="tab">Config Adjusments</a></li>
  	</ul>
  	<!-- Tab panes -->
  	<div class="tab-content">
	  	<div role="tabpanel" class="tab-pane active" id="lookuptab">
	  		<div class="row">
				<div class="col-md-12">
				  	<section class="panel">
					    <header class="panel-heading">
					       Look Up
					    </header>
				      	<div class="panel-body">	
				          	<table id="adjst_table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%" cellspacing="0">
				          		<thead>
				          			<tr>
				          				<th>Description</th>
				          				<th>Type</th>
				          				<th>Fee Category</th>
				          				<th>Amount</th>
				          				<th>Action</th>
				          			</tr>
				          		</thead>
				          		<tbody>
			          				<?php
			          					foreach ($adjs as $adj) 
			          					{
			          						if($adj['adj_type']=='A')
			          						{
			          							$type = '(+)';
			          						}
			          						else
			          						{
			          							$type = '(-)';
			          						}

			          						$cat = '-';
			          						foreach ($fees as $fee)
				          					{
				          						if($fee['fc_id']==$adj['adj_feecat'])
				          						{
				          							$cat = $fee['fc_name'];
				          						}
				          					}

			          						if($adj['adj_amttype']=='V')
			          						{
			          							$amt = number_format($adj['adj_amount'],2);
			          						}
			          						else
			          						{
			          							$amt = $adj['adj_amount'].' %';
			          						}

			          						echo "<tr>";
			          						echo "<td>".$adj['adj_description']."</td>";
			          						echo "<td>".$type."</td>";
			          						echo "<td>".$cat."</td>";
			          						echo "<td style='text-align:right'>".$amt."</td>";
			          						// echo "<td>".$adj['adj_status']."</td>";
			          						echo "<td><a class='btn btn-info btn-sm' onclick='event.preventDefault();edit_adj_load(".$adj['adj_id'].",\"".$adj['adj_description']."\",\"".$adj['adj_type']."\",\"".$adj['adj_amttype']."\",".$adj['adj_feecat'].",".$adj['adj_amount'].")'>Edit</a></td>";
			          						echo "</tr>";
			          					}
			          				?>
				          		</tbody>
				          	</table>
				      	</div>
				  	</section>
				</div>
			</div>
	  	</div>
    	<div role="tabpanel" class="tab-pane" id="create_tab">
    		<br>
    		<div class="row">
				<div class="col-md-12">
				  	<section class="panel">
					    <header class="panel-heading">
					        Manage Adjusments: 
					    </header>
				      	<div class="panel-body">
				      		<form class="form-horizontal" role="form" method="post" action="<?php echo base_url('hci_accounts/save_adjusment')?>" id="fs_form" autocomplete="off" novalidate>
					      		<div class="row">
					      			<div class="col-md-12 form-group">
					                  	<label for="adj_type" class="col-md-3 control-label">Type</label>
					                  	<div class="col-md-4">
					                      	<select type="text" class="form-control" id="adj_type" name="adj_type">
							              		<option value=''></option>
							              		<option value='A'>(+) Addition</option>
							              		<option value='D'>(-) Deduction</option>
							              	</select>
					                  	</div>
					              	</div>
					      		</div>
					      		<br>
					      		<div class="row">
					      			<div class="col-md-12 form-group">
					                  	<label for="adj_name" class="col-md-3 control-label">Description</label>
					                  	<div class="col-md-8">
					                  		<input type='hidden' id='adj_id' name='adj_id'>
					                      	<input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="adj_name" name="adj_name" placeholder="" value="">
					                  	</div>
					              	</div>
					      		</div>
					      		<br>
					      		<div class="row form-group">
			      					<label for="conf_adj" class="col-md-3 control-label">Apply For</label>
			      					<div class="col-md-4">
			      						<div class="radios">
											<label class="label_radio" for="radio-01">
											<input id="radio-01" name="adj_applyfor" data-validation="required" data-validation-error-msg-required="Select the application area of adjusment" value="1"  onclick="$('#fs_section').hide();$('#paymentplan_adj').hide();" checked="" type="radio">
											All Transactions
											</label>
											<label class="label_radio" for="radio-02">
											<input id="radio-02" name="adj_applyfor" data-validation="required" data-validation-error-msg-required="Select the application area of adjusment" value="2"  onclick="$('#fs_section').hide();$('#paymentplan_adj').hide();" type="radio">
											2nd Sibling 
											</label>
											<label class="label_radio" for="radio-03">
											<input id="radio-03" name="adj_applyfor" data-validation="required" data-validation-error-msg-required="Select the application area of adjusment" value="3"  onclick="$('#fs_section').hide();$('#paymentplan_adj').hide();" type="radio">
											3rd Sibling
											</label>
										</div>
			      					</div>
			      					<div class="col-md-4">
			      						<div class="radios">
											<label class="label_radio" for="radio-04">
											<input id="radio-04" name="adj_applyfor" data-validation="required" data-validation-error-msg-required="Select the application area of adjusment" value="4"  onclick="$('#fs_section').hide();$('#paymentplan_adj').hide();" type="radio">
											4th Sibling and onward
											</label>
											<label class="label_radio" for="radio-05">
											<input id="radio-05" name="adj_applyfor" data-validation="required" data-validation-error-msg-required="Select the application area of adjusment" value="5"  onclick="$('#fs_section').show();$('#paymentplan_adj').hide();" type="radio">
											Selected Fee Structure
											</label>
											<label class="label_radio" for="radio-06">
											<input id="radio-06" name="adj_applyfor" data-validation="required" data-validation-error-msg-required="Select the application area of adjusment" value="6"  onclick="$('#fs_section').hide();$('#paymentplan_adj').show();" type="radio">
											Payment method
											</label>
										</div>
			      					</div>
					      		</div>
					      		<br>
					      		<div class="row">
					      			<div class="col-md-12 form-group">
					                  	<label for="adj_fee" class="col-md-3 control-label">Fee Category</label>
					                  	<div class="col-md-6">
					                      	<select type="text" class="form-control" id="adj_fee" name="adj_fee">
							              		<option value=''></option>
							              		<option value='0'>Any Fee Category</option>
							              		<?php
						          					foreach ($fees as $fee)
						          					{
						          						echo "<option value='".$fee['fc_id']."'>".$fee['fc_name']."</option>";
						          					}
						          				?>
							              	</select>
					                  	</div>
					              	</div>
					      		</div>
					      		<br>
					      		<div class="row">
					      			<div class="col-md-12 form-group">
					                  	<label for="adj_calamttype" class="col-md-3 control-label">Value or %</label>
					                  	<div class="col-md-3">
					                      	<select type="text" class="form-control" id="adj_calamttype" name="adj_calamttype">
							              		<option value=''></option>
							              		<option value='V'>Value</option>
							              		<option value='P'>Percentage (%)</option>
							              	</select>
					                  	</div>
					              	</div>
					      		</div>
					      		<br>
					      		<div class="row">
					      			<div class="col-md-12 form-group">
					                  	<label for="adj_amt" class="col-md-3 control-label">Amount</label>
					                  	<div class="col-md-3">
					                      	<input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="adj_amt" name="adj_amt" placeholder="" value="">
					                  	</div>
					              	</div>
					      		</div>
					      		<div class="row">
						      		<div class="col-md-12 form-group">
						              	<div class="col-md-offset-3 col-md-9">
						              		<br>
						                  	<button type="submit" id="savebtn" name="asnewbtn" class="btn btn-info">Save</button> 
					                  	<button type="button" class="btn btn-default" onclick="location.reload()">Reset</button> 
						              	</div>
						          	</div>
					          	</div>
					        </form>
				      	</div>
				    </section>
				</div>
			</div>
    	</div>
    	<div role="tabpanel" class="tab-pane" id="config_tab">
    		<section class="panel">
			    <header class="panel-heading">
			        Config Adjusments: 
			    </header>
		      	<div class="panel-body">
		      		<form class="form-horizontal" role="form" method="post" action="<?php echo base_url('hci_accounts/config_adjusments')?>" id="conf_form" autocomplete="off" novalidate>
		      			<input type="hidden" id="conf_id" name="conf_id">
		      			<div class="row">
		      				<div class="col-md-4 form-group">
		      					<label for="conf_adj" class="col-md-4 control-label">Adjusment</label>
			                  	<div class="col-md-8">
			                      	<select type="text" class="form-control" onchange="load_paymentplan(this.value);" id="conf_adj" name="conf_adj">
					              		<option value=''></option>
					              		<?php
				          					foreach ($adjs as $adj) 
				          					{
				          						echo "<option value='".$adj['adj_id']."'>".$adj['adj_description']."</option>";
				          					}
				          				?>
					              	</select>
			                  	</div>
		      				</div>
		      				
			      		</div>
			      		<section id="fs_section">
			      			<hr>
			      			<div class="row">
				      			<div class="form-group">
				                  	<label for="conf_fs" class="col-md-2 control-label">Fee Structure</label>
				                  	<div class="col-md-4">
				                      	<select type="text" class="form-control" id="conf_fs" name="conf_fs" onchange="load_feestructure_data(this.value)">
						              		<option value=''></option>
						              		<?php
					          					foreach ($fss as $fs)
					          					{
					          						echo "<option value='".$fs['es_feemasterid']."'>".$fs['fee_description']."</option>";
					          					}
					          				?>
						              	</select>
				                  	</div>
				              	</div>
				      		</div>
				      		<br>
				      		<div class="row">
				      			<div class="col-md-12">
				      				<table class="table table-bordered">
						          		<thead id='fs_header'>
						          			<tr>
						          				<th rowspan='2' class='text-center'>Grade</th>
						          			<?php
					          					foreach ($fees as $fee)
					          					{
					          						if($fee['fc_id']==1)
					          						{
					          							echo "<th colspan='3' class='text-center'>".$fee['fc_name']."</th>";
					          						}
					          						else
					          						{
					          							echo "<th rowspan='2' class='text-center'>".$fee['fc_name']."</th>";
					          						}	
					          					}
					          				?>
					          				</tr>
					          				<tr>
						          			<?php
					          					foreach ($fees as $fee)
					          					{
					          						if($fee['fc_id']==1)
					          						{
					          							echo "<th class='text-center'>Total Amount</th><th class='text-center'>slabwise-New</th><th class='text-center'>slabwise-old</th>";
					          						}	
					          					}
					          				?>
					          				</tr>
						          		</thead>
						          		<tbody id="fs_body">
						          			<?php
					          					foreach ($grd_info as $grd) 
					          					{
					          						echo "<tr>";
					          						echo "<td>".$grd['grd_name']."</td>";
					          						foreach ($fees as $fee)
						          					{
						          						if($fee['fc_id']==1)
					          							{
					          								echo "<td><input type='checkbox' id='amt_".$grd['grd_id']."_".$fee['fc_id']."' name='amts[]' class='amt_input form-control' value=''></td>";
					          								echo "<td><input type='checkbox' id='amtnew_".$grd['grd_id']."_".$fee['fc_id']."' name='amtnew[]' class='amt_input form-control' value=''></td>";
					          								echo "<td><input type='checkbox' id='amtold_".$grd['grd_id']."_".$fee['fc_id']."' name='amtold[]' class='amt_input form-control' value=''></td>";
					          							}
					          							else
					          							{
					          								echo "<td><input type='checkbox' id='amt_".$grd['grd_id']."_".$fee['fc_id']."' name='amts[]' value='' class='amt_input form-control'></td>";
					          							}
						          					}
					          						echo "</tr>";
					          					}
					          				?>
						          		</tbody>
						          	</table>
				      			</div>
				      		</div>
			      		</section>
			      		<section id="paymentplan_adj">
			      			<hr>
			      			<div class="row">
				      			<div class="form-group">
				                  	<label for="adj_plan" class="col-md-2 control-label">Payment Plan</label>
				                  	<div class="col-md-4">
				                      	<select type="text" class="form-control" id="adj_plan" name="adj_plan" onchange="load_periods(this.value)">
						              		<option value=''></option>
						              	</select>
				                  	</div>
				              	</div>
				      		</div>
				      		<br>
				      		<div class="row">
				      			<div class="form-group">
				                  	<label for="numperiods" class="col-md-2 control-label">Apply for period</label>
				                  	<div class="col-md-4">
				                      	<select type="text" class="form-control" id="numperiods" name="numperiods">
						              		<option value=''></option>
						              	</select>
				                  	</div>
				              	</div>
				      		</div>
			      		</section>
			      		<section>
			      			<hr>
			      			<div class="row">
				      			<div class="col-md-4">
				      				<div class="radios">
										<label class="label_radio" for="radio-06">
										<input id="radio-06" name="conf_appstu" value="1"  checked="" onclick="load_student_add(this.value)" type="radio">
										All Students
										</label>
									</div>
				      			</div>
				      			<div class="col-md-4">
				      				<div class="radios">
										<label class="label_radio" for="radio-07">
										<input id="radio-07" name="conf_appstu" value="2" onclick="load_student_add(this.value)" type="radio">
										Selected Student/s
										</label>
									</div>
				      			</div>
				      		</div>
				      		<div class="row" id="stu_div">
				      			<div class="col-md-6 form-group">
			      					<label for="conf_adj" class="col-md-2 control-label">Student</label>
				                  	<div class="col-md-8">
				                      	<select class="form-control select2" id="stulist" name="stulist"  style="width: 100%;">
                                             <?php
                                                // $students = $this->db->get('hci_preadmission')->result_array();
                                                foreach($reg_stu as $row):
                                                    ?>
                                                    <option value="<?php echo $row['id'] ?>">
                                                        <?php echo '[ '.$row['st_id'].' ] '.$row['other_names'];?>
                                                    </option>
                                                    <?php
                                                endforeach;
                                                ?>

                                        </select>
				                  	</div>
				                  	<div class="col-md-2">
				                  		<button class="btn btn-primary btn-sm" onclick="event.preventDefault();add_students_list()">Add</button>
				                  	</div>
			      				</div>
			      				<div class="col-md-6">
			      					<table class="table table-bordered">
			      						<thead>
			      							<tr>
			      								<th>Name</th>
			      								<th>Action</th>
			      							</tr>
			      						</thead>
			      						<tbody id="stulistbody">
			      							<tr><td colspan="2">No student selected</td></tr>
			      						</tbody>
			      					</table>
			      				</div>
				      		</div>
			      		</section>
			      		<br>
			      		<div class="row">
				      		<div class="form-group">
				              	<div class="col-md-offset-1 col-md-11">
				              		<br>
				                  	<button type="submit" id="conf_save" name="conf_save" class="btn btn-info">Save</button> 
			                  		<button type="button" class="btn btn-default" onclick="location.reload()">Reset</button> 
				              	</div>
				          	</div>
			          	</div>
		      		</form>
		      	</div>
		    </section>
    	</div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/select2/select2.full.min.js'); ?>"></script>

<link rel="stylesheet" href="<?php echo base_url('assets/select2/select2.min.css') ?>">
<script type="text/javascript" src="<?php echo base_url('js/moment.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/bootstrap-datetimepicker.min.js'); ?>"></script>
<script type="text/javascript">

stu_list = new Array();

$.validate({
   	form : '#fs_form'
});

$.validate({
   	form : '#conf_form'
});

$('#adjst_table').DataTable();

$( document ).ready(function() {
	load_student_add(1);
	$(".select2").select2();
	$('#fs_section').hide();
	$('#paymentplan_adj').hide();
});

function edit_adj_load(adj_id,adj_description,adj_type,adj_amttype,adj_feecat,adj_amount)
{
	$('#adj_type').val(adj_type);
    $('#adj_id').val(adj_id);
    $('#adj_name').val(adj_description);
    $('#adj_fee').val(adj_feecat);
    $('#adj_calamttype').val(adj_amttype);
    $('#adj_amt').val(adj_amount);
}

function load_student_add(rdovalue)
{
	if(rdovalue==1)
	{
		$('#stu_div').hide();
	}
	else
	{
		$('#stu_div').show();
	}
}

function add_students_list()
{
	stuid = $('#stulist').val();
	stuname = $("#stulist option:selected").text();

	if(jQuery.isEmptyObject(stu_list))
	{
		$('#stulistbody').empty();
	}

	if(!(stu_list['stu_'+stuid]))
	{
		stu_list['stu_'+stuid] = {"id":stuid,"name":stuname};
		$('#stulistbody').append('<tr id="sturw_'+stuid+'"><td><input type="hidden" name="selstu[]" value="'+stuid+'">'+stuname+'</td><td><button class="btn btn-danger btn-xs" onclick="event.preventDefault();delete_student('+stuid+')"><i class="fa fa-times"></i></button></td></tr>');
	}
	else
	{
		alert('Student already exists');
	}
}

function delete_student(id)
{
	delete stu_list['stu_'+id];
	
    $('#sturw_' + id).remove();

    if(jQuery.isEmptyObject(stu_list))
    	$('#stulistbody').append('<tr><td colspan="2">No student selected</td></tr>');
}

function load_feestructure_data(id)
{
	$.post("<?php echo base_url('hci_fee_structure/load_feestructure_data')?>",{'fs_id':id},
		function(data)
		{
			load_feetemp_amounts(data['fee_aftemp'],data['fee_tftemp']);
		},	
		"json"
	);
}

function load_feetemp_amounts(aft,tft)
{
	$.post("<?php echo base_url('hci_fee_structure/load_feetemp_amounts')?>",{'aft':aft,'tft':tft},
		function(data)
		{	
			if(data == 'denied')
			{
        		funcres = {status:"denied", message:"You have no right to proceed the action"};
        		result_notification(funcres);
			}
			else
			{
				if(aft!=null)
				{
					if(data['afts'].length>0)
					{
						for (i = 0; i<data['afts'].length; i++) 
						{			
							$('#amt_'+data['afts'][i]['fsf_grade']+'_'+data['afts'][i]['fsf_fee']).val(data['afts'][i]['fsf_id']);
							if(data['afts'][i]['fsf_fee']==1)
							{
								$('#amtnew_'+data['afts'][i]['fsf_grade']+'_'+data['afts'][i]['fsf_fee']).val(data['afts'][i]['fsf_id']);	
								$('#amtold_'+data['afts'][i]['fsf_grade']+'_'+data['afts'][i]['fsf_fee']).val(data['afts'][i]['fsf_id']);
							}
						}
					}
					
				}

				if(tft!=null)
				{
					if(data['tfts'].length>0)
					{
						for (i = 0; i<data['tfts'].length; i++) 
						{					
							$('#amt_'+data['tfts'][i]['fsf_grade']+'_'+data['tfts'][i]['fsf_fee']).val(data['tfts'][i]['fsf_id']);
							if(data['tfts'][i]['fsf_fee']==1)
							{
								$('#amtnew_'+data['tfts'][i]['fsf_grade']+'_'+data['tfts'][i]['fsf_fee']).val(data['tfts'][i]['fsf_id']);	
								$('#amtold_'+data['tfts'][i]['fsf_grade']+'_'+data['tfts'][i]['fsf_fee']).val(data['tfts'][i]['fsf_id']);
							}
						}
					}
					
				}
			}
		},	
		"json"
	);
}

function load_paymentplan(adj_id)
{
	$('#adj_plan').empty();
	$.post("<?php echo base_url('hci_fee_structure/load_paymentplan')?>",{'id':adj_id},
		function(data)
		{	
			if(data == 'denied')
			{
        		funcres = {status:"denied", message:"You have no right to proceed the action"};
        		result_notification(funcres);
			}
			else
			{
				$('#adj_plan').append('<option value=""></option>');
				if(data.length>0)
				{
					for (i = 0; i<data.length; i++) 
					{			
						$('#adj_plan').append('<option value="'+data[i]['plan_id']+'">'+data[i]['plan_name']+'</option>');
					}
				}
			}
		},	
		"json"
	);
}

function load_periods(plan)
{
	$('#numperiods').empty();
	$.post("<?php echo base_url('hci_fee_structure/load_periods')?>",{'id':plan},
		function(data)
		{
			if(data == 'denied')
			{
        		funcres = {status:"denied", message:"You have no right to proceed the action"};
        		result_notification(funcres);
			}
			else
			{	
				$('#numperiods').append('<option value=""></option>');
				if(data>0)
				{
					for (i = 1; i<=data; i++) 
					{			
						$('#numperiods').append('<option value="'+i+'">'+i+'</option>');
					}
				}
			}
		},	
		"json"
	);
}

</script>