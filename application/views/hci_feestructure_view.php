<div class="row">
	<div class="col-md-12">
		<h3 class="page-header"><i class="fa fa-bank"></i> FEE TEMPLATE</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
			<li><i class="fa fa-cog"></i>Settings</li>
			<li><i class="fa fa-bank"></i>Fee Template</li>
		</ol>
	</div>
</div>
<div>
	<section class="panel">
		<form class="form-horizontal" role="form" method="post" action="<?php echo base_url('hci_fee_structure/save_fee_structure')?>" id="fs_form" autocomplete="off" novalidate>
		<input type="hidden" name="screenstatus" id="screenstatus" value="view">
	    <header class="panel-heading"> 
	        <div class="row">
	        	<div class="col-md-4">
	        		Manage Fee Template : 
	        	</div>
				<div class="col-md-2"></div>
				<div class="col-md-3">
					<div class="form-group">
						<label for="fs_branch" class="col-md-3 control-label" style="font-size: 12px;padding-top: 0px">Template</label>
						<div class="col-md-9">
							<select type="text" class="form-control" id="temp_type" name="temp_type"  data-validation="required" data-validation-error-msg-required="Field can not be empty" onchange="load_feetemplate_list();changeview();">
			              		<option value=''>---Select Template Type---</option>
			             		<option value='1'>Admission Template</option>
			             		<option value='2'>Term Fee Template</option>
			              	</select>
			            </div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label for="fs_branch" class="col-md-3 control-label" style="font-size: 12px;padding-top: 0px">Branch</label>
		                <div class="col-md-9">
	                  		<?php 
	                  			global $branchdrop;
	                  			global $selectedbr;
	                  			$extraattrs = 'id="fs_branch" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" onchange="load_feetemplate_list();"';
	                  			echo form_dropdown('fs_branch',$branchdrop,$selectedbr, $extraattrs); 
	                  		?>
		                </div>
					</div>
				</div>
			</div>
	    </header>
      	<div class="panel-body" id="edit_view">
      		<br>
      		<div class="row">
  				<div class="form-group col-md-4">
                  	<label for="ft_name" class="col-md-3 control-label">Description</label>
                  	<div class="col-md-9">
                  		<input type='hidden' id='ft_id' name='ft_id'>
                      	<input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="ft_name" name="ft_name" placeholder="" value="">
                  	</div>
              	</div>
              	<div class="form-group col-md-4">
                  	<label for="eff_date" class="col-md-3 control-label">Eff. Date</label>
                  	<div class="col-md-9">
	                  	<div id="eff_date_div" class="input-group date">
				    		<input class="form-control" type="text" name="eff_date" id="eff_date"  data-format="YYYY-MM-DD">
				    		<span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span>
				    		</span>
			    		</div>
			    	</div>
              	</div>
              	<div class="form-group col-md-3">
                  	<label for="is_curr" class="col-md-10 control-label">Current Fee-Structure</label>
                  	<div style="padding-top: 10px" class="col-md-1">
                      	<input type="checkbox" id="is_curr" name="is_curr" >
                    </div>
              	</div> 	
      		</div>	
      		<br>
      		<div class="row">
      			<div class="col-md-2">
      				<div class="row" id="admissionamtperiod">
      					<div class="col-md-12">
      					<?php
	      					foreach ($fees as $fee)
	      					{
	      						if($fee['fc_feestructure']==1)
	      						{
	      				?>
	      							<div class="form-group row">
	      								<div class="col-md-12">
											<label class="control-label" style="font-size: 12px;padding-top: 0px"><?php echo $fee['fc_name'];?></label>
											<div class="">
												<select type="text" class="form-control feeamtperiod" id="amtperiod_<?php echo $fee['fc_id']?>" name="amtperiod_<?php echo $fee['fc_id'];?>"  data-validation="required" data-validation-error-msg-required="Field can not be empty">
								              		<option value=''>---Select Period of Amount---</option>
								             		<option value='1'>Total Terms</option>
								             		<option value='2'>One Term</option>
								              	</select>
								            </div>
							            </div>
									</div>
	      				<?php
	      						}	
	      					}
	      				?>
	      				</div>
      				</div>
      				<div class="row" id="termamtperiod">
      					<div class="col-md-12">
      					<?php
	      					foreach ($fees as $fee)
	      					{
	      						if($fee['fc_feestructure']==2)
	      						{
	      				?>
	      							<div class="form-group row">
	      								<div class="col-md-12">
											<label class="control-label" style="font-size: 12px;padding-top: 0px"><?php echo $fee['fc_name'];?></label>
											<div class="">
												<select type="text" class="form-control feeamtperiod" id="amtperiod_<?php echo $fee['fc_id']?>" name="amtperiod_<?php echo $fee['fc_id'];?>"  data-validation="required" data-validation-error-msg-required="Field can not be empty">
								              		<option value=''>---Select Period of Amount---</option>
								             		<option value='1'>Total Terms</option>
								             		<option value='2'>One Term</option>
								              	</select>
								            </div>
								        </div>
									</div>
	      				<?php
	      						}	
	      					}
	      				?>
	      				</div>
      				</div>
      			</div>
      			<div class="col-md-8">
      				<table class="table table-bordered" id="adm_edit_tbl">
		          		<thead id='adm_header'>
		          			<tr>
		          				<th rowspan='2' class='text-center'>Grade</th>
		          			<?php
	          					foreach ($fees as $fee)
	          					{
	          						if($fee['fc_feestructure']==1)
	          						{
	          							echo "<th colspan='3' class='text-center'>".$fee['fc_name']."<input name='admintemplatefees[]' type='hidden' value='".$fee['fc_id']."'></th>";
	          						}	
	          					}
	          				?>
	          				</tr>
	          				<tr>
		          			<?php
	          					foreach ($fees as $fee)
	          					{
	          						if($fee['fc_feestructure']==1)
	          						{
	          							echo "<th class='text-center'>Total Amount</th><th class='text-center'>slabwise-New</th><th class='text-center'>slabwise-old</th>";
	          						}	
	          					}
	          				?>
	          				</tr>
		          		</thead>
		          		<tbody id="adm_body">
		          			<?php
	          					foreach ($grd_info as $grd) 
	          					{ 
	          						if($grd['grd_status']=='A')
	          						{
	          							echo "<tr><input name='admintemplategrades[]' type='hidden' value='".$grd['grd_id']."'>";
		          						echo "<td>".$grd['grd_name']."</td>";
		          						foreach ($fees as $fee)
			          					{
			          						if($fee['fc_feestructure']==1)
		          							{
		          								echo "<td><input type='text' id='amt_".$grd['grd_id']."_".$fee['fc_id']."' name='amt_".$grd['grd_id']."_".$fee['fc_id']."' class='amt_input form-control' value='0.00'></td>";
		          								echo "<td><input type='text' id='amtnew_".$grd['grd_id']."_".$fee['fc_id']."' name='amtnew_".$grd['grd_id']."_".$fee['fc_id']."' class='amt_input form-control' value='0.00'></td>";
		          								echo "<td><input type='text' id='amtold_".$grd['grd_id']."_".$fee['fc_id']."' name='amtold_".$grd['grd_id']."_".$fee['fc_id']."' class='amt_input form-control' value='0.00'></td>";
		          							}
			          					}
		          						echo "</tr>";
	          						}
	          					}
	          				?>
		          		</tbody>
		          	</table>
		          	<table class="table table-bordered" id="trm_edit_tbl">
		          		<thead id='trm_header'>
		          			<tr>
		          				<th rowspan='2' class='text-center'>Grade</th>
		          			<?php
	          					foreach ($fees as $fee)
	          					{
	          						if($fee['fc_feestructure']==2)
	          						{
	          							echo "<th rowspan='2' class='text-center'>".$fee['fc_name']."</th><input name='termtemplatefees[]' type='hidden' value='".$fee['fc_id']."'>";
	          						}	
	          					}
	          				?>
	          				</tr>
		          		</thead>
		          		<tbody id="trm_body">
		          			<?php
	          					foreach ($grd_info as $grd) 
	          					{ 
	          						if($grd['grd_status']=='A')
	          						{
	          							echo "<tr><input name='termtemplategrades[]' type='hidden' value='".$grd['grd_id']."'>";
		          						echo "<td>".$grd['grd_name']."</td>";
		          						foreach ($fees as $fee)
			          					{
			          						if($fee['fc_feestructure']==2)
		          							{
		          								echo "<td><input type='text' id='amt_".$grd['grd_id']."_".$fee['fc_id']."' name='amt_".$grd['grd_id']."_".$fee['fc_id']."' value='0.00' class='amt_input form-control'></td>";
		          							}
			          					}
		          						echo "</tr>";
	          						}
	          					}
	          				?>
		          		</tbody>
		          	</table>
      			</div>
      			<div class="col-md-2"></div>
      		</div>
      		<div class="row form-group">
              	<div class="col-md-11">
              		<br>
              		<button type="submit" id="savebtn" name="asnewbtn" class="btn btn-info">Save</button> 
              		<button type="submit" id="asnewbtn" name="asnewbtn" class="btn btn-info">Save as new</button> 
                  	<button type="submit" id="changesbtn" name="changesbtn" class="btn btn-info">Save Changes</button> 
                  	<button type="button" class="btn btn-default" onclick="location.reload()">Back</button> 
              	</div>
          	</div>
          	</form>
      	</div>
      	<div class="panel-body" id="fs_view">
      		<div class="row">
      			<div class="col-md-2"></div>
      			<div class="col-md-5 form-group">
      				<div class="col-md-12" id="description_div">
	                  	<select type="text" class="form-control" id="curr_fs" name="curr_fs" onchange="load_feetemplate_data(this.value)">
		              	</select>
	              	</div>
      			</div>
      			<div class="col-md-3">
      				<button type="button" class="btn btn-default btn-sm" onclick="load_add_new()">Add New</button>
		      		<button type="button" class="btn btn-default btn-sm" onclick="load_edit_view()">Edit</button>
      				<button type="button" class="btn btn-default btn-sm" onclick="load_save_as()">Save as New</button>
      			</div>
      			<div class="col-md-2"></div>
      		</div>
      		<div class="row">
      			<div class="col-md-2"></div>
      			<div class="col-md-6" id="templateNameDiv"></div>
      		</div>
      		<br>	
      		<div class="row">
      			<div class="col-md-2">
      				<div class="row" id="viewadmissionamtperiod">
      					<div class="col-md-12">
      					<?php
	      					foreach ($fees as $fee)
	      					{
	      						if($fee['fc_feestructure']==1)
	      						{
	      				?>
	      							<div class="form-group row">
	      								<div class="col-md-12">
											<label class="control-label" style="font-size: 12px;padding-top: 0px"><?php echo $fee['fc_name'];?></label>
											<div id="viewamtperiod_<?php echo $fee['fc_id']?>" class="viewamtperiod">
								            </div>
							            </div>
									</div>
	      				<?php
	      						}	
	      					}
	      				?>
	      				</div>
      				</div>
      				<div class="row" id="viewtermamtperiod">
      					<div class="col-md-12">
      					<?php
	      					foreach ($fees as $fee)
	      					{
	      						if($fee['fc_feestructure']==2)
	      						{
	      				?>
	      							<div class="form-group row">
	      								<div class="col-md-12">
											<label class="control-label" style="font-size: 12px;padding-top: 0px"><?php echo $fee['fc_name'];?></label>
											<div id="viewamtperiod_<?php echo $fee['fc_id']?>" class="viewamtperiod">
								            </div>
							            </div>
									</div>
	      				<?php
	      						}	
	      					}
	      				?>
	      				</div>
      				</div>
      			</div>
      			<div class="col-md-8">
      				<table class="table table-bordered" id="adm_view_tbl">
		          		<thead id='admview_header'>
		          			<tr>
		          				<th rowspan='2' class='text-center'>Grade</th>
		          			<?php
	          					foreach ($fees as $fee)
	          					{
	          						if($fee['fc_feestructure']==1)
	          						{
	          							echo "<th colspan='3' class='text-center'>".$fee['fc_name']."</th>";
	          						}	
	          					}
	          				?>
	          				</tr>
	          				<tr>
		          			<?php
	          					foreach ($fees as $fee)
	          					{
	          						if($fee['fc_feestructure']==1)
	          						{
	          							echo "<th class='text-center'>Total Amount</th><th class='text-center'>slabwise-New</th><th class='text-center'>slabwise-old</th>";
	          						}	
	          					}
	          				?>
	          				</tr>
		          		</thead>
		          		<tbody id="admview_body">
		          			<?php
	          					foreach ($grd_info as $grd) 
	          					{
	          						if($grd['grd_status']=='A')
	          						{
		          						echo "<tr>";
		          						echo "<td>".$grd['grd_name']."</td>";
		          						foreach ($fees as $fee)
			          					{
			          						if($fee['fc_feestructure']==1)
		          							{
		          								echo "<td style='text-align:right' id='amttd_".$grd['grd_id']."_".$fee['fc_id']."''></td>";
		          								echo "<td style='text-align:right' id='amtnewtd_".$grd['grd_id']."_".$fee['fc_id']."'></td>";
		          								echo "<td style='text-align:right' id='amtoldtd_".$grd['grd_id']."_".$fee['fc_id']."'></td>";
		          							}
			          					}
		          						echo "</tr>";
	          						}
	          						
	          					}
	          				?>
		          		</tbody>
		          	</table>
		          	<table class="table table-bordered" id="trm_view_tbl">
		          		<thead id='trmview_header'>
		          			<tr>
		          				<th rowspan='2' class='text-center'>Grade</th>
		          			<?php
	          					foreach ($fees as $fee)
	          					{
	          						if($fee['fc_feestructure']==2)
	          						{
	          							echo "<th rowspan='2' class='text-center'>".$fee['fc_name']."</th>";
	          						}	
	          					}
	          				?>
	          				</tr>
		          		</thead>
		          		<tbody id="trmview_body">
		          			<?php
	          					foreach ($grd_info as $grd) 
	          					{
	          						if($grd['grd_status']=='A')
	          						{
		          						echo "<tr>";
		          						echo "<td>".$grd['grd_name']."</td>";
		          						foreach ($fees as $fee)
			          					{
		          							if($fee['fc_feestructure']==2)
		          							{
		          								echo "<td style='text-align:right' id='amttd_".$grd['grd_id']."_".$fee['fc_id']."'></td>";
		          							}
			          					}
		          						echo "</tr>";
		          					}
	          					}
	          				?>
		          		</tbody>
		          	</table>
      			</div>
      			<div class="col-md-2"></div>
      		</div>
      	</div>
  	</section>
</div>
<script type="text/javascript" src="<?php echo base_url('js/moment.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/bootstrap-datetimepicker.min.js'); ?>"></script>
<script type="text/javascript">

$.validate({
   	form : '#fs_form'
});

$.validate({
   	form : '#grp_form'
});

$.validate({
   	form : '#fee_form'
});
$('#eff_date_div').datetimepicker({ defaultDate: "<?php echo date('Y-m-d');?>",  pickTime: false});

$( document ).ready(function() {
	load_feetemplate_list();
	$("#edit_view").hide();
	$("#adm_view_tbl").hide();
	$("#trm_view_tbl").hide();
	$("#adm_edit_tbl").hide();
	$("#trm_edit_tbl").hide();
	$("#admissionamtperiod").hide();
	$("#termamtperiod").hide();
	$("#viewadmissionamtperiod").hide();
	$("#viewtermamtperiod").hide();

	//$("#changesbtn").attr("disabled", true);
});

function load_add_new()
{
	temptype = $('#temp_type').val();

	$("#ft_id").val('');
	$("#ft_name").val('');
	$("#eff_date").val('');
	$('#is_curr').prop('checked', false);
	$(".amt_input").val(0.00);
	$('#savebtn').show();
	$('#asnewbtn').hide();
	$('#changesbtn').hide();
	$('.feeamtperiod').val('');

	if(temptype == 1)
	{
		$("#adm_edit_tbl").show();
		$("#trm_edit_tbl").hide();
		$("#admissionamtperiod").show();
		$("#termamtperiod").hide();
	}
	else if(temptype == 2)
	{
		$("#adm_edit_tbl").hide();
		$("#trm_edit_tbl").show();
		$("#admissionamtperiod").hide();
		$("#termamtperiod").show();
	}
	else
	{
		$("#adm_edit_tbl").hide();
		$("#trm_edit_tbl").hide();
		$("#admissionamtperiod").hide();
		$("#termamtperiod").hide();
	}

	$("#edit_view").show();
	$("#fs_view").hide();
	$("#screenstatus").val('add');
}

function changeview()
{
	screenstatus = $("#screenstatus").val();

	if(screenstatus == 'add')
	{
		load_add_new();
	}
}

function load_edit_view()
{
	temptype = $('#temp_type').val();
	if(temptype == 1)
	{
		$("#adm_edit_tbl").show();
		$("#trm_edit_tbl").hide();
		$("#admissionamtperiod").show();
		$("#termamtperiod").hide();
	}
	else if(temptype == 2)
	{
		$("#adm_edit_tbl").hide();
		$("#trm_edit_tbl").show();
		$("#admissionamtperiod").hide();
		$("#termamtperiod").show();
	}
	$('#savebtn').hide();
	$('#asnewbtn').hide();
	$('#changesbtn').show();
	$("#edit_view").show();
	$("#fs_view").hide();

	$('#fs_branch option:not(:selected)').prop('disabled', true);
	$('#temp_type option:not(:selected)').prop('disabled', true);

	$("#screenstatus").val('edit');
}

function load_save_as()
{
	temptype = $('#temp_type').val();

	if(temptype == 1)
	{
		$("#adm_edit_tbl").show();
		$("#trm_edit_tbl").hide();
		$("#admissionamtperiod").show();
		$("#termamtperiod").hide();
	}
	else if(temptype == 2)
	{
		$("#adm_edit_tbl").hide();
		$("#trm_edit_tbl").show();
		$("#admissionamtperiod").hide();
		$("#termamtperiod").show();
	}

	$("#ft_id").val('');
	$("#ft_name").val('');
	$("#eff_date").val('');
	$('#is_curr').prop('checked', false);
	$('#savebtn').hide();
	$('#asnewbtn').show();
	$('#changesbtn').hide();
	$("#edit_view").show();
	$("#fs_view").hide();

	$('#temp_type option:not(:selected)').prop('disabled', true);

	$("#screenstatus").val('asnew');
}

function load_feetemplate_data(id)
{
	$(".viewamtperiod").empty();
	$(".feeamtperiod").val('');
	$.post("<?php echo base_url('hci_fee_structure/load_feetemplate_data')?>",{'ft_id':id},
		function(data)
		{
			if(data == 'denied')
			{
        		funcres = {status:"denied", message:"You have no right to proceed the action"};
        		result_notification(funcres);
			}
			else
			{
				$("#fs_branch").val(data['ft_branch']);
				$('#temp_type').val(data['ft_feecat']);
				$("#ft_id").val(data['ft_id']);
				$("#ft_name").val(data['ft_name']);
				$("#eff_date").val(data['ft_effectdate']);

				$('#templateNameDiv').empty();

				if(data['ft_feecat']==1)
				{
					$('#templateNameDiv').append('Admission Template :: '+data['ft_name']);
				}
				else
				{
					$('#templateNameDiv').append('Term Template :: '+data['ft_name']);
				}
				

				if(data['ft_iscurrent']==1)
				{
					$('#is_curr').prop('checked', true);
				}
				else
				{
					$('#is_curr').prop('checked', false);
				}

				for (i = 0; i<data['amountperiods'].length; i++) 
				{
					$("#amtperiod_"+data['amountperiods'][i]['amtprd_feecategory']).val(data['amountperiods'][i]['amtprd_option']);
					if(data['amountperiods'][i]['amtprd_option']==1)
					{
						displayamtprd = 'Total Terms';
					}
					else
					{
						displayamtprd = 'One Term';
					}
					$("#viewamtperiod_"+data['amountperiods'][i]['amtprd_feecategory']).append(displayamtprd);
				}

				load_feetemplate_amounts(data['ft_id'],data['ft_feecat']);
			}
		},	
		"json"
	);
}

function load_feetemplate_amounts(id,type)
{
	$.post("<?php echo base_url('hci_fee_structure/load_feetemplate_amounts')?>",{'temp':id},
		function(data)
		{	
			if(data == 'denied')
			{
        		funcres = {status:"denied", message:"You have no right to proceed the action"};
        		result_notification(funcres);
			}
			else
			{
				if(type==1)
				{
					if(data.length>0)
					{
						for (i = 0; i<data.length; i++) 
						{
							$('#amttd_'+data[i]['fsf_grade']+'_'+data[i]['fsf_fee']).empty();
							$('#amtnewtd_'+data[i]['fsf_grade']+'_'+data[i]['fsf_fee']).empty();
							$('#amtoldtd_'+data[i]['fsf_grade']+'_'+data[i]['fsf_fee']).empty();
							
							$('#amt_'+data[i]['fsf_grade']+'_'+data[i]['fsf_fee']).val(data[i]['fsf_amt']);
							$('#amttd_'+data[i]['fsf_grade']+'_'+data[i]['fsf_fee']).append(data[i]['fsf_amt']);
							if(data[i]['fsf_fee']==1)
							{
								$('#amtnew_'+data[i]['fsf_grade']+'_'+data[i]['fsf_fee']).val(data[i]['fsf_slabnew']);	
								$('#amtold_'+data[i]['fsf_grade']+'_'+data[i]['fsf_fee']).val(data[i]['fsf_slabold']);
								$('#amtnewtd_'+data[i]['fsf_grade']+'_'+data[i]['fsf_fee']).append(data[i]['fsf_slabnew']);
								$('#amtoldtd_'+data[i]['fsf_grade']+'_'+data[i]['fsf_fee']).append(data[i]['fsf_slabold']);
							}
						}
					}
					$("#adm_view_tbl").show();
					$("#trm_view_tbl").hide();
					$("#viewadmissionamtperiod").show();
					$("#viewtermamtperiod").hide();
				}

				if(type==2)
				{
					if(data.length>0)
					{
						for (i = 0; i<data.length; i++) 
						{
							$('#amttd_'+data[i]['fsf_grade']+'_'+data[i]['fsf_fee']).empty();
							$('#amtnewtd_'+data[i]['fsf_grade']+'_'+data[i]['fsf_fee']).empty();
							$('#amtoldtd_'+data[i]['fsf_grade']+'_'+data[i]['fsf_fee']).empty();
							
							$('#amt_'+data[i]['fsf_grade']+'_'+data[i]['fsf_fee']).val(data[i]['fsf_amt']);
							$('#amttd_'+data[i]['fsf_grade']+'_'+data[i]['fsf_fee']).append(data[i]['fsf_amt']);
							if(data[i]['fsf_fee']==1)
							{
								$('#amtnew_'+data[i]['fsf_grade']+'_'+data[i]['fsf_fee']).val(data[i]['fsf_slabnew']);	
								$('#amtold_'+data[i]['fsf_grade']+'_'+data[i]['fsf_fee']).val(data[i]['fsf_slabold']);
								$('#amtnewtd_'+data[i]['fsf_grade']+'_'+data[i]['fsf_fee']).append(data[i]['fsf_slabnew']);
								$('#amtoldtd_'+data[i]['fsf_grade']+'_'+data[i]['fsf_fee']).append(data[i]['fsf_slabold']);
							}
						}
					}
					$("#adm_view_tbl").hide();
					$("#trm_view_tbl").show();
					$("#viewadmissionamtperiod").hide();
					$("#viewtermamtperiod").show();
				}
			}
		},	
		"json"
	);
}

function load_feetemplate_list()
{
	$('#curr_fs').empty();
	$('#curr_fs').append("<option value=''></option>");

	$.post("<?php echo base_url('hci_fee_structure/load_feetemplate_list')?>",{'type':$('#temp_type').val(),'branch':$('#fs_branch').val()},
		function(data)
		{
			if(data == 'denied')
			{
        		funcres = {status:"denied", message:"You have no right to proceed the action"};
        		result_notification(funcres);
			}
			else
			{
				adminlist = '<optgroup label="Admission Template">';
				termlist = '<optgroup label="Term Template">';
				admcount = 0;
				trmcount = 0;
				for (i = 0; i<data.length; i++) 
				{
					if(data[i]['ft_feecat']==1)
					{
						adminlist += "<option value='"+data[i]['ft_id']+"'>"+data[i]['ft_name']+"</option>";
						admcount++;
					}
					
					if(data[i]['ft_feecat']==2)
					{
						termlist += "<option value='"+data[i]['ft_id']+"'>"+data[i]['ft_name']+"</option>";
						trmcount++;
					}
				}

				adminlist += '</optgroup>';
				termlist += '</optgroup>';

				if(admcount>0)
				{
					$('#curr_fs').append(adminlist);
				}
				if(trmcount>0)
				{
					$('#curr_fs').append(termlist);
				}
				
			}
		},	
		"json"
	);

	$("#adm_view_tbl").hide();
	$("#trm_view_tbl").hide();
	$("#viewadmissionamtperiod").hide();
	$("#viewtermamtperiod").hide();
}
</script>