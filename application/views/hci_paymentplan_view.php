<div class="row">
	<div class="col-md-12">
		<h3 class="page-header"><i class="fa fa-bank"></i> PAYMENT SCHEME</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
			<li><i class="fa fa-cog"></i>Settings</li>
			<li><i class="fa fa-bank"></i>Payment Scheme</li>
		</ol>
	</div>
</div>
<div>
    <br>
	<div class="row">
		<div class="col-md-6">
		  	<section class="panel">
			    <header class="panel-heading">
			        Payment Plan
			    </header>
		      	<div class="panel-body">	
		          	<form class="form-horizontal" role="form" method="post" action="<?php echo base_url('hci_fee_structure/save_paymentplan')?>" id="payplan_form" autocomplete="off" novalidate>
		          		<div class="form-group">
		                  	<label for="description" class="col-md-2 control-label">Description</label>
		                  	<div class="col-md-5">
		                  		<input type="hidden" name="pplan_id" id="pplan_id">
		                      	<input type="text" data-validation="required" data-validation-error-msg-required="Field can not be empty" class="form-control" id="description" name="description" placeholder="">
		                  	</div>
		              	</div>
		              	<div class="form-group">
		              		<label for="fee_cat" class="col-md-2 control-label">Fee Category </label>
                            <div class="col-md-6">
                                <select type="text" class="form-control" id="fee_cat" name="fee_cat">
                                    <option value=''></option>
                                    <?php
                                        foreach ($fees as $fee) 
                                        {
                                            echo "<option value='".$fee['fc_id']."'>".$fee['fc_name']."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
		              	</div>
		              	<div class="form-group">
		              		<label for="ins_type" class="col-md-2 control-label">Ins. Type </label>
                            <div class="col-md-6">
                            	<!-- onchange="load_inputs(this.value)"  -->
                                <select type="text" data-validation='required' data-validation-error-msg-required='Field can not be empty' class="form-control" id="ins_type" name="ins_type">
                                    <option value=''></option>
                                    <option value='TA'>Total Amount</option>
                                    <option value='TO'>Two Term + one Term</option>
                                    <option value='T'>Per Term</option>
                                    <option value='M'>Monthly</option>
                                </select>
                            </div>
		              	</div>
		              	<!-- <div class="row" id="ins_div">
		              	</div> -->
		              	<hr>
		              	<div class="form-group">
		                  	<div class="col-md-offset-2 col-md-11">
		                  		<br>
		                      	<button type="submit" class="btn btn-info">Save</button> 
		                      	<button type="reset" class="btn btn-default">Reset</button>
		                  	</div>
		              	</div>
		          	</form>
		      	</div>
		  	</section>
	  	</div>
	  	<div class="col-md-6">
		  	<section class="panel">
			    <header class="panel-heading">
			       Look Up
			    </header>
		      	<div class="panel-body">	
		          	<table class="table">
		          		<thead>
		          			<tr>
		          				<th>Plan</th>
		          				<th>Fee Category</th>
		          				<th>Type</th>
		          				<th>Actions</th>
		          			</tr>
		          		</thead>
		          		<tbody>
	          				<?php
	          					foreach ($plans as $pln) 
	          					{
	          						if($pln['plan_type']=='T')
									{
										$type_display = 'Per Term';
									}
									else if($pln['plan_type']=='M')
									{
										$type_display = 'Monthly';
									}
									else if($pln['plan_type']=='TO')
									{
										$type_display = 'Two Terms + One Term';
									}
									else
									{
										$type_display = 'Total Amount';
									}
	          						echo "<tr>";
	          						echo "<td>".$pln['plan_name']."</td>";
	          						echo "<td>".$pln['fc_name']."</td>";
	          						echo "<td>".$type_display."</td>";		
	          						echo "<td><a class='btn btn-info btn-sm' onclick='event.preventDefault();edit_pay_plan(".$pln['plan_id'].")'>Edit</a></td>";
	          						echo "</tr>";
	          						// foreach ($pln['inst'] as $ins) 
	          						// {
	          						// 	echo "<tr>";
	          						// 	echo "<td></td>";
	          						// 	echo "<td>".$ins['ins_number']."</td>";
		          					// 	echo "<td>".$ins['ins_ddate']."</td>";
		          					// 	echo "<td></td>";	
		          					// 	echo "</tr>";	
	          						// }
	          					}
	          				?>
		          		</tbody>
		          	</table>
		      	</div>
		  	</section>
	  	</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url('js/moment.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/bootstrap-datetimepicker.min.js'); ?>"></script>
<script type="text/javascript">

$.validate({
   	form : '#payplan_form'
});

$( document ).ready(function() {
	
});

function edit_pay_plan(id)
{
	$('#ins_div').empty();

	$.post("<?php echo base_url('hci_fee_structure/edit_pay_plan')?>",{'id':id},
		function(data)
		{
			if(data == 'denied')
			{
	    		funcres = {status:"denied", message:"You have no right to proceed the action"};
	    		result_notification(funcres);
			}
			else
			{
				$('#pplan_id').val(data['plan_id']);
				$('#fee_cat').val(data['plan_fee']);
				$('#ins_type').val(data['plan_type']);
				$('#description').val(data['plan_name']);

				// if(data['plan_type']=='T')
				// {
				// 	ins_num = 3;
				// }
				// else if(data['plan_type']=='M')
				// {
				// 	ins_num = 12;				
				// }
				// else
				// {
				// 	ins_num = 0;
				// }
				
				// for (i = 0; i<ins_num; i++) {
				// 	$('#ins_div').append("<div class='col-md-12'><div class='row'><div class='col-md-2'><input type='hidden' id='ins_id_"+data['inst'][i]['ins_number']+"' name='ins_id_"+data['inst'][i]['ins_number']+"'></div>"
				// 		+"<div class='col-md-2'><input value='"+data['inst'][i]['ins_number']+"' type='text'  class='form-control' data-validation='required' data-validation-error-msg-required='Field can not be empty' id='ins_"+data['inst'][i]['ins_number']+"' name='ins_"+data['inst'][i]['ins_number']+"'></div>"
				// 		+"<div class='col-md-5'><div id='ddate_"+data['inst'][i]['ins_number']+"_div' class='input-group date'><input class='form-control' type='text' name='ddate_"+data['inst'][i]['ins_number']+"' id='ddate_"+data['inst'][i]['ins_number']+"' data-validation='required' data-validation-error-msg-required='Field can not be empty' data-format='YYYY-MM-DD'><span class='input-group-addon'><span class='glyphicon-calendar glyphicon'></span></span></div></div>"
				// 		+"</div></div>");

				//    	$('#ddate_'+data['inst'][i]['ins_number']+'_div').datetimepicker({ defaultDate: data['inst'][i]['ins_ddate'],  pickTime: false});
				// }
			}
		},	
		"json"
	);
}

function load_inputs(type)
{
	if(type=='T')
	{
		num = 3;
	}
	else if(type=='M')
	{
		num = 12;
	}
	else
	{
		num = 0;
	}

	$('#ins_div').empty();
	for (i = 1; i<=num; i++) {
	   	$('#ins_div').append("<div class='col-md-12'><div class='row'><div class='col-md-2'><input type='hidden' id='ins_id_"+i+"' name='ins_id_"+i+"'></div>"
			+"<div class='col-md-2'><input value='"+i+"' type='text'  class='form-control' data-validation='required' data-validation-error-msg-required='Field can not be empty' id='ins_"+i+"' name='ins_"+i+"'></div>"
			+"<div class='col-md-5'><div id='ddate_"+i+"_div' class='input-group date'><input class='form-control' type='text' name='ddate_"+i+"' id='ddate_"+i+"' data-validation='required' data-validation-error-msg-required='Field can not be empty' data-format='YYYY-MM-DD'><span class='input-group-addon'><span class='glyphicon-calendar glyphicon'></span></span></div></div>"
			+"</div></div>");

	   	$('#ddate_'+i+'_div').datetimepicker({ defaultDate: "<?php echo date('Y-m-d');?>",  pickTime: false});
	}
}

</script>
