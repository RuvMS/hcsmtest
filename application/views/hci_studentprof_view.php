

<script type="text/javascript" src="<?php echo base_url('js/jquery-confirm.js')?>"></script>
<div class="row">
    <div class="col-md-12">
        <h3 class="page-header"><i class="fa fa-users"></i> STUDENT PROFILE</h3>
        <ol class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
            <li><i class="fa fa-cog"></i>Student</li>
            <li><i class="fa fa-bank"></i>Student Profile</li>
        </ol>
    </div>
</div>
<div>
    <div class="row" id="search_div">

    </div>
<div class="row"  id="contentarea">
    <div class="col-md-12">
        <section class="panel">
                <header class="panel-heading" >
                    General Information
                </header>
                
                <div class="panel-body">
                    <div class="col-md-15" id="siblings">
                    <div class="col-md-4">
                    <?php echo "Name : ".$stu_info['st_details']['family_name']." ".$stu_info['st_details']['other_names'];?>
                     </div>
                     <div>

          <!-- Nav tabs -->
                          <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-graduation-cap"></i>Student Info.</a></li>
                            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><i class="fa fa-graduation-cap"></i> Performance</a></li>
                            <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab"><i class="fa fa-money"></i> Payments</a></li>
                            <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab"><i class="fa fa-users"></i> Family Info</a></li>
                            <li role="presentation"><a href="#other" aria-controls="other" role="tab" data-toggle="tab"><i class="fa fa-folder-open"></i> Other</a></li>
                            <li role="presentation"><a href="#temp" aria-controls="temp" role="tab" data-toggle="tab"><i class="fa fa-folder-open"></i> Temp</a></li>
                          </ul>
                     
                        

                          <!-- Tab panes -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="home">
                                <table bordrt="1">
                                    <tr>
                                        <td width="150" height="150"><img src="/hcsm/img/1.jpg" width="150" height="150"></td>
                                    </tr>
                                </table>
                                <table>
                                    <thead>
                                        <tr>
                                            <td width="250">
                                                <?php
                                                    if($stu_info['st_details']['st_id']!="")
                                                    {
                                                        echo "Student_ID : ".$stu_info['st_details']['st_id'];
                                                    }
                                                    else{
                                                        echo " ";
                                                    }
                                                ?>
                                            </td>
                                            </tr>
                                            <tr>
                                            <td width="400">
                                                <?php
                                                    if($stu_info['st_details']['grd_name']!="")
                                                    {
                                                        echo "Grade : ".$stu_info['st_details']['grd_name'];
                                                    }
                                                    else{
                                                        echo "Grade : -- ";
                                                    }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="250">
                                                <?php
                                                    if($stu_info['st_details']['gender']=='M')
                                                    {
                                                        echo "Gender : Male";
                                                    }
                                                    else{
                                                        echo "Gender : Female";
                                                    }
                                                ?>
                                            </td></tr>
                                            <tr>
                                            <td width="400">
                                                <?php
                                                    if($stu_info['st_details']['dob']!="")
                                                    {
                                                        echo "Birthday : ".$stu_info['st_details']['dob'];
                                                    }
                                                    else{
                                                        echo " ";
                                                    }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            
                                            <td width="250">
                                                <?php echo "Academic year : ".$stu_info['st_details']['st_accyear'];?>
                                            </td></tr>
                                            <tr>
                                            <td width="400">
                                                <?php
                                                    if($stu_info['st_details']['local_nationality']!="")
                                                    {
                                                        echo "Nationality : ".$stu_info['st_details']['local_nationality'];
                                                    }
                                                    else{
                                                        echo "Nationality : -- ";
                                                    }
                                                ?>
                                            </td>
                                        </tr><tr>
                                            <td width="250">
                                               <?php echo "Religion : ".$stu_info['st_details']['rel_name'];?>
                                            </td>
                                        </tr>
                                        <tr><td height="20"></td></tr>
                                    </thead>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="profile">performance</div>
                            <div role="tabpanel" class="tab-pane" id="messages">

                           <!--  payment -->

                            <table border="0">
                                    <tr>
                                        <td rowspan='2' width="150" height="150"><img src="/hcsm/img/1.jpg" width="150" height="150"></td>
                                        <td width="100"></td>
                                        <td>
                                            
                                            <?php
                                                    if($stu_info['st_details']['st_id']!="")
                                                    {
                                                        echo "Student_ID : ".$stu_info['st_details']['st_id'];
                                                    }
                                                    else{
                                                        echo " ";
                                                    }
                                                ?><br/>

                                            <?php
                                                    if($stu_info['st_details']['grd_name']!="")
                                                    {
                                                        echo "Grade : ".$stu_info['st_details']['grd_name'];
                                                    }
                                                    else{
                                                        echo "Grade : -- ";
                                                    }
                                                ?><br/>

                                            <?php
                                                    if($stu_info['st_details']['gender']=='M')
                                                    {
                                                        echo "Gender : Male";
                                                    }
                                                    else{
                                                        echo "Gender : Female";
                                                    }
                                                ?><br/>

                                            <?php
                                                    if($stu_info['st_details']['dob']!="")
                                                    {
                                                        echo "Birthday : ".$stu_info['st_details']['dob'];
                                                    }
                                                    else{
                                                        echo " ";
                                                    }
                                                ?><br/>
                                            
                                     </td>
                                     <td>
                                         <?php echo "Accedemic year : ".$stu_info['st_details']['st_accyear'];?><br/>

                                        <?php
                                                    if($stu_info['st_details']['local_nationality']!="")
                                                    {
                                                        echo "Nationality : ".$stu_info['st_details']['local_nationality'];
                                                    }
                                                    else{
                                                        echo "Nationality : -- ";
                                                    }
                                                ?><br/>

                                        <?php echo "Religian : ".$stu_info['st_details']['rel_name'];?><br/><br/>

                                     </td>
                                    </tr>
                                </table>
                                <table border="0">
                                    <tr>
                                        <td width="300"></td>
                                        <td width="350">
                                            <?php
                                                echo "Admission Template :: ".$stu_info['fee_mas']['admtemp'];
                                            ?>
                                        </td>
                                        <td width="350">
                                            <?php
                                                echo "Term Template :: ".$stu_info['fee_mas']['trmtemp'];
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="10"></td>
                                    </tr>
                                </table>

                        <center>
                            <div class="col-md-12">
                                <table border="1">
                                    <thead id='fsview_header'>
                                            <tr>
                                                <th rowspan='2' class='text-center'>Grade</th>
                                                    
                                                    <?php
                                                        foreach($stu_info['fees'] as $fee)
                                                            {
                                                                if($fee['fc_id']==1)
                                                                {
                                                                    echo "<th colspan='3' class='text-center'>".$fee['fc_name']."</th>";
                                                                }
                                                                elseif($fee['fc_id']==2)
                                                                {
                                                                    echo "<th rowspan='2' class='text-center'>".$fee['fc_name']."</th>";
                                                                } 
                                                                elseif($fee['fc_id']==3)
                                                                {
                                                                    echo "<th rowspan='2' class='text-center'>".$fee['fc_name']."</th>";
                                                                }    
                                                            }
                                                    ?>   
                                            </tr> 
                                            <tr>
                                                <?php
                                                    foreach ($stu_info['fees'] as $fee)
                                                    {
                                                        if($fee['fc_id']==1)
                                                        {
                                                            echo "<th class='text-center' bgcolor='#D2F6FB'>Total Amount</th><th class='text-center' bgcolor='#D2F6FB'>slabwise-New</th><th class='text-center' bgcolor='#D2F6FB'>slabwise-old</th>";
                                                        }   
                                                    }
                                                ?>
                                            </tr>                     
                                    </thead>
                                        <tbody id="fsview_body">
                                            <?php
                                                foreach ($stu_info['grd_info'] as $grd) 
                                                {
                                                    echo "<tr>";
                                                    echo "<td bgcolor='#DED9E8'>".$grd['grd_name']."</td>";
                                                    foreach ($stu_info['fees'] as $fee)
                                                    {
                                                        
                                                        if($fee['fc_id']==1)
                                                        {

                                                            foreach ($grd['afts'] as $afts) 
                                                            {
                                                               if($fee['fc_id']==$afts['fsf_fee'])
                                                               {
                                                                    echo "<td style='text-align:right'>".$afts['fsf_amt']."</td>";
                                                                    echo "<td style='text-align:right'>".$afts['fsf_slabnew']."</td>";
                                                                    echo "<td style='text-align:right'>".$afts['fsf_slabold']."</td>";
                                                               }     
                                                            }

                                                            
                                                        }
                                                        else if($fee['fc_id']==2 || $fee['fc_id']==3)
                                                        {
                                                            foreach ($grd['tfts'] as $tfts) 
                                                            {
                                                               if($fee['fc_id']==$tfts['fsf_fee'])
                                                               {
                                                                    echo "<td style='text-align:right'>".$tfts['fsf_amt']."</td>";
                                                                    
                                                               }     
                                                            }
                                                           
                                                        }
                                                    }
                                                    echo "</tr>";
                                                }
                                            ?>
                                    </tbody>    
                            </table>
                        </div>
                    </center>
                            <table>
                                    <tr>
                                        <td height="20"></td>
                                    </tr>
                                    <tr>
                                    <td width="150"></td>
                                    <td width="100"></td>                           
                                    <td>
                                        <?php
                                            if($stu_info['st_details']['st_addpaymethod']=='1')
                                            {
                                                echo "Preferred Admission Payment method  : Total Payment";
                                            }
                                            elseif($stu_info['st_details']['st_addpaymethod']=='2')
                                            {
                                                echo "Preferred Admission Payment method  : Slabwise Payment";
                                            }
                                            else{
                                                echo "Preferred Admission Payment method  : --";
                                            }
                                        ?><br/>
                                    </td>
                                    </tr>
                                    <tr>
                                        <td height="30"></td>
                                    </tr>
                            </table>

                    <center>
                        <div class="col-md-12">
                            <table border="1">
                            <thead id='fsview_header'>
                                <tr>
                                    <th rowspan='2' class='text-center'></th>
                                <?php
                                    foreach ($stu_info['fees'] as $fee) 
                                    {
                                        echo "<th bgcolor='#D2F6FB'><center>".$fee['fc_name']."</center></th>";
                                    }
                                ?>
                                </tr>
                            </thead>
                            <tbody id='fsview_header'>
                                <tr>
                                    <td bgcolor="#DED9E8">Payment Scheme</td>
                                <?php
                                    foreach ($stu_info['fees'] as $fee) 
                                    {
                                      
                                        $payplantxt = '-';

                                        foreach ($stu_info['pay_plan'] as $pay) 
                                        {      
                                            if($fee['fc_id']==$pay['plan_fee'])
                                            {
                                                $payplantxt = $pay['plan_name'];
                                            }        
                                        }
                                        echo "<th><center>".$payplantxt."</center></th>";
                                    }
                                ?>
                                </tr>
                                <tr>
                                    <td bgcolor="#DED9E8">Discount (%)</td>
                                <?php
                                    
                                    foreach ($stu_info['fees'] as $fee) 
                                    { 
                                        $feediscstr = '-';
                                        foreach ($stu_info['discounts'] as $disc) 
                                        {
                                            if($fee['fc_id']==$disc['adj_feecat'])
                                            {
                                                if($feediscstr=='-')
                                                {
                                                    $feediscstr = $disc['adj_description'];
                                                }
                                                else
                                                {
                                                    $feediscstr .= $disc['adj_description'];
                                                }

                                                if($disc['adj_amttype']=='P')
                                                {
                                                    if($disc['adj_amount']!='0')
                                                    {
                                                        $feediscstr .= " (".$disc['adj_amount']."%)</br> ";
                                                    }
                                                    else
                                                    {
                                                        $feediscstr .= " ";
                                                    }
                                                }
                                                else
                                                {
                                                    $feediscstr .= "(".number_format($disc['adj_amount'],2).")";
                                                }
                                            }               
                                        }
                                        echo "<th>".$feediscstr."</th>";
                                    }
                                    
                                ?>
                                </tr>
                                <tr>
                                    <th>Other Discounts</th>
                                    <?php
                                        $feeextradiscstr = '-';
                                        foreach ($stu_info['discounts'] as $disc) 
                                        {
                                            if($disc['adj_feecat']==0)
                                            {
                                                if($feeextradiscstr=='-')
                                                {
                                                    $feeextradiscstr = $disc['adj_description'];
                                                }
                                                else
                                                {
                                                    $feeextradiscstr .= $disc['adj_description'];
                                                }

                                                if($disc['adj_amttype']=='P')
                                                {
                                                    if($disc['adj_amount']!='0')
                                                    {
                                                        $feeextradiscstr .= " (".$disc['adj_amount']."%)</br> ";
                                                    }
                                                    else
                                                    {
                                                        $feeextradiscstr .= " ";
                                                    }
                                                }
                                                else
                                                {
                                                    $feeextradiscstr .= "(".number_format($disc['adj_amount'],2).")";
                                                }
                                            }               
                                        }
                                        echo "<th colspan='".count($stu_info['fees'])."'>".$feeextradiscstr."</th>";
                                    ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </center>
            </div>
                            
                            <div role="tabpanel" class="tab-pane" id="settings">
                                <table>
                                    <tr>
                                        <td width="150" height="150"><img src="/hcsm/img/1.jpg" width="150" height="150"></td>
                                        <td width="200"></td>
                                        <td width="700">
                                            <?php echo "Father_Name : ".$stu_info['st_details']['dad_name'];?><br/>
                                            <?php echo "Address : ".$stu_info['st_details']['dad_addy'].', '.$stu_info['st_details']['dad_add2'].', '.$stu_info['st_details']['dad_addcity'];?><br>
                                            <?php echo "Home : ".$stu_info['st_details']['dad_home']; ?><br/>
                                            <?php echo " Mobile : ".$stu_info['st_details']['dad_mobile'];?><br/>
                                            <?php echo " Email : ".$stu_info['st_details']['dad_email'];?>
                                        </td>
                                    </tr>
                                </table>
                                <table>
                                    <thead>
                                        <tr>
                                            <td width="150">
                                                <?php
                                                    if($stu_info['st_details']['st_id']!="")
                                                    {
                                                        echo "Student_ID : ".$stu_info['st_details']['st_id'];
                                                    }
                                                    else{
                                                        echo " ";
                                                    }
                                                ?>
                                            </td>
                                            <td width="150"></td>
                                            <td width="700">
                                             <?php echo "Mother_Name : ".$stu_info['st_details']['mom_name'];?>   
                                            </td>
                                            </tr>
                                            <tr>
                                            <td width="150">
                                                <?php
                                                    if($stu_info['st_details']['grd_name']!="")
                                                    {
                                                        echo "Grade : ".$stu_info['st_details']['grd_name'];
                                                    }
                                                    else{
                                                        echo "Grade : -- ";
                                                    }
                                                ?>
                                            </td>
                                            <td width="100"></td>
                                            <td width="700">
                                            <?php echo "Address : ".$stu_info['st_details']['mom_addy'].', '.$stu_info['st_details']['mom_add2'].', '.$stu_info['st_details']['mom_addcity'];?>
                                        </td>
                                        </tr>
                                        <tr>
                                            <td width="150">
                                                <?php
                                                    if($stu_info['st_details']['gender']=='M')
                                                    {
                                                        echo "Gender : Male";
                                                    }
                                                    else{
                                                        echo "Gender : Female";
                                                    }
                                                ?>
                                            </td>
                                            <td width="150"></td>
                                            <td width="700">
                                                <?php echo "Home : ".$stu_info['st_details']['mom_home'];?>
                                            </td>
                                            </tr>
                                            <tr>
                                            <td width="150">
                                                <?php
                                                    if($stu_info['st_details']['dob']!="")
                                                    {
                                                        echo "Birthday : ".$stu_info['st_details']['dob'];
                                                    }
                                                    else{
                                                        echo " ";
                                                    }
                                                ?>
                                            </td>
                                            <td width="150"></td>
                                            <td width="700">
                                            <?php echo "Mobile : ".$stu_info['st_details']['mom_mobile'];?>
                                        </td>
                                        </tr>
                                        <tr>
                                            
                                            <td width="200">
                                                <?php echo "Accedemic year : ".$stu_info['st_details']['st_accyear'];?>
                                            </td>
                                            <td width="150"></td>
                                            <td width="700">
                                            <?php echo "Email : ".$stu_info['st_details']['mom_email'];?>
                                            </td>
                                            </tr>
                                            <tr>
                                            <td width="150">
                                                <?php
                                                    if($stu_info['st_details']['local_nationality']!="")
                                                    {
                                                        echo "Nationality : ".$stu_info['st_details']['local_nationality'];
                                                    }
                                                    else{
                                                        echo "Nationality : -- ";
                                                    }
                                                ?>
                                            </td>
                                            <td width="150"></td>
                                            <td width="700">
                                            
                                            </td>
                                        </tr><tr>
                                            <td width="200">
                                               <?php echo "Religian : ".$stu_info['st_details']['rel_name'];?>
                                            </td>
                                            <td width="150"></td>
                                            <td width="700">
                                                <?php echo "Guardion_Name : ".$stu_info['st_details']['guar_name'];?>
                                            </td>
                                        </tr>
                                        <tr><td width="200"></td>
                                        <td width="150"></td>
                                        <td width="700"><?php echo "Address : ".$stu_info['st_details']['guar_addy'].', '.$stu_info['st_details']['guar_add2'].', '.$stu_info['st_details']['guar_addcity'];?></td>
                                        </tr>
                                        </tr>
                                        <tr><td width="200"></td>
                                        <td width="150"></td>
                                        <td width="700"><?php echo "Home : ".$stu_info['st_details']['guar_home'];?></td>
                                        </tr>
                                        </tr>
                                        <tr><td width="200"></td>
                                        <td width="150"></td>
                                        <td width="700"><?php echo "Mobile : ".$stu_info['st_details']['guar_mobile'];?></td>
                                        </tr>
                                        <tr><td width="200" height="30"></td>
                                        <td width="150" height="30"></td>
                                        <td width="700" height="30"></td>
                                        </tr>
                                        <tr><td width="200"></td>
                                        <td width="150"></td>
                                        <td width="700"><?php echo "Emergency_contact_person : ".$stu_info['st_details']['emg_name'];?></td>
                                        </tr>
                                        <tr><td width="200"></td>
                                        <td width="150"></td>
                                        <td width="700"><?php echo "Address : ".$stu_info['st_details']['emg_addy'];?></td>
                                        </tr>
                                        <tr><td width="200"></td>
                                        <td width="150"></td>
                                        <td width="700"><?php echo "Home : ".$stu_info['st_details']['emg_home'];?></td>
                                        </tr>
                                        <tr><td width="200"></td>
                                        <td width="150"></td>
                                        <td width="700"><?php echo "Mobile : ".$stu_info['st_details']['emg_mobile'];?></td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="other"><!-- other -->
                                <table>
                                    <tr>
                                        <td width="150" height="150"><img src="/hcsm/img/1.jpg" width="150" height="150"></td>
                                        <td width="200"></td>
                                        <td width="700">
                                            <?php echo "<font color='green'>School History</font>";?><br/>
                                            <?php echo "Age which the child started formal schooling <font color='black'>: </font>".$stu_info['st_details']['school_age'];?></br>
                                            <?php echo "Previous_School_Name <font color='black'>:</font> ".$stu_info['st_details']['1sch_name']; ?><br/>
                                            <?php echo "School_Address <font color='black'>:</font> ".$stu_info['st_details']['1sch_addy'];?><br/>
                                            <?php echo "Language of Instruction <font color='black'>:</font> ".$stu_info['st_details']['1sch_lang'];?><br/>
                                            <?php
                                                    if($stu_info['st_details']['ffrom']!="" && $stu_info['st_details']['fto']!="")
                                                    {
                                                        echo "From <font color='black'>:</font> ".$stu_info['st_details']['ffrom']." <font color='blue'>| </font>";
                                                        echo "To <font color='black'>:</font> ".$stu_info['st_details']['fto'];
                                                    }
                                                    else{
                                                        echo "From : -- ";
                                                        echo "To : -- ";
                                                    }
                                                ?>
                                        </td>
                                    </tr>
                                </table>
                            
                                <table>
                                    <thead>
                                        <tr>
                                            <td width="150">
                                                <?php
                                                    if($stu_info['st_details']['st_id']!="")
                                                    {
                                                        echo "Student_ID : ".$stu_info['st_details']['st_id'];
                                                    }
                                                    else{
                                                        echo " ";
                                                    }
                                                ?>
                                            </td>
                                            <td width="150"></td>
                                            <td width="700">
                                             <?php echo "Current Year/Grade : ".$stu_info['st_details']['1st_gc'];?>   
                                            </td>
                                            </tr>
                                            <tr>
                                            <td width="150">
                                                <?php
                                                    if($stu_info['st_details']['grd_name']!="")
                                                    {
                                                        echo "Grade : ".$stu_info['st_details']['grd_name'];
                                                    }
                                                    else{
                                                        echo "Grade : -- ";
                                                    }
                                                ?>
                                            </td>
                                            <td width="100"></td>
                                            <td width="700">
                                            <?php if($stu_info['st_details']['mgt_wd'] == 'no'){
                                                echo "Was your child withdraw from any school on request from its Management : NO ";
                                                }
                                                if($stu_info['st_details']['mgt_wd'] == 'yes'){
                                                 echo "Was your child withdraw from any school on request from its Management : Yes ";   
                                                }
                                                else{
                                                  echo "Was your child withdraw from any school on request from its Management : -- ";   
                                                }
                                            ?>
                                        </td>
                                        </tr>
                                        <tr>
                                            <td width="150">
                                                <?php
                                                    if($stu_info['st_details']['gender']=='M')
                                                    {
                                                        echo "Gender : Male";
                                                    }
                                                    else{
                                                        echo "Gender : Female";
                                                    }
                                                ?>
                                            </td>
                                            <td width="150"></td>
                                            <td width="700">
                                                <?php 
                                                if($stu_info['st_details']['mgt_exp']!=""){
                                                  echo "If yes , explanation : ".$stu_info['st_details']['mgt_exp'];  
                                                }
                                                else{
                                                    echo "If No , explanation : - ";
                                                }
                                                ?>
                                            </td>
                                            </tr>
                                            <tr>
                                            <td width="150">
                                                <?php
                                                    if($stu_info['st_details']['dob']!="")
                                                    {
                                                        echo "Birthday : ".$stu_info['st_details']['dob'];
                                                    }
                                                    else{
                                                        echo " ";
                                                    }
                                                ?>
                                            </td>
                                            <td width="150"></td>
                                            <td width="700">
                                            <?php 
                                            if($stu_info['st_details']['int_act']!=" "){
                                                  echo "special interests of school activities in which the child is/has been involved : ".$stu_info['st_details']['int_act'];
                                                }
                                            else{
                                                  echo "special interests of school activities in which the child is/has been involved : --";
                                            }
                                            ?>
                                            
                                        </td>
                                        </tr>
                                        <tr>
                                            
                                            <td width="200">
                                                <?php echo "Accedemic year : ".$stu_info['st_details']['st_accyear'];?>
                                            </td>
                                            <td width="150"></td>
                                            <td width="700">
                                            
                                            </td>
                                            </tr>
                                            <tr>
                                            <td width="150">
                                                <?php
                                                    if($stu_info['st_details']['local_nationality']!="")
                                                    {
                                                        echo "Nationality : ".$stu_info['st_details']['local_nationality'];
                                                    }
                                                    else
                                                    {
                                                        echo "Nationality : -- ";
                                                    }
                                                ?>
                                            </td>
                                            <td width="150"></td>
                                            <td width="700">
                                                <?php
                                                if($stu_info['st_details']['eng_xp']== 'yes')
                                                {
                                                    echo "Mother tongue,Has the child had instruction in or experience of English : yes";
                                                }
                                                else
                                                {
                                                    echo "Mother tongue,Has the child had instruction in or experience of English :  no";
                                                }
                                                ?>
                                            </td>
                                        </tr><tr>
                                            <td width="200">
                                               <?php echo "Religian : ".$stu_info['st_details']['rel_name'];?>
                                            </td>
                                            <td width="150"></td>
                                            <td width="700">
                                                <?php 
                                                if($stu_info['st_details']['eng_sit']!=" ")
                                                {
                                                    echo "If yes , in what situation ? : " .$stu_info['st_details']['eng_sit'];
                                                }
                                                else
                                                {
                                                    echo "If yes , in what situation ? : --";
                                                }

                                                ?>
                                            </td>
                                        </tr>
                                        <tr><td width="200"></td>
                                        <td width="150"></td>
                                        <td width="700">
                                        <?php 
                                            if($stu_info['st_details']['eng_len']!=" ")
                                            {
                                                echo "Far how long : ".$stu_info['st_details']['eng_len'];
                                            }
                                            else
                                            {
                                                echo "Far how long : -- ";
                                            }
                                        ?></td>
                                        </tr>
                                        </tr>
                                        <tr>
                                        <td width="200" height="30"></td>
                                        <td width="150" height="30"></td>
                                        <td width="700" height="30"></td>
                                        </tr>
                                        <tr>
                                        <td width="200"></td>
                                        <td width="150"></td>
                                        <td width="700">
                                        <?php 
                                            if($stu_info['st_details']['enr_sped']=='yes')
                                            {
                                                echo "Has your child been enrolled in any type special Education Programme : yes";
                                            }
                                            if($stu_info['st_details']['enr_sped']=='on')
                                            {
                                                echo "Has your child been enrolled in any type special Education Programme : no";
                                            }
                                            else
                                            {
                                                echo "Has your child been enrolled in any type special Education Programme : --";
                                            }
                                        ?>
                                            
                                        </td>
                                        </tr>
                                        <tr><td width="200"></td>
                                        <td width="150"></td>
                                        <td width="700">
                                            <?php
                                                if($stu_info['st_details']['sped_xp']!="")
                                                {
                                                    echo "If yes , explanation : ".$stu_info['st_details']['sped_xp'];
                                                }
                                                else
                                                {
                                                    echo "If yes , explanation : --";
                                                }
                                            ?>
                                        </td>
                                        </tr>
                                        <tr>
                                        <td width="200" height="30"></td>
                                        <td width="150" height="30"></td>
                                        <td width="700" height="30"></td>
                                        </tr>
                                        <tr><td width="200"></td>
                                        <td width="150"></td>
                                        <td width="700">
                                        <?php 
                                            if($stu_info['st_details']['med_test']=='yes')
                                            {
                                                echo "Has your child been tested by a learning Specialist or Psychologist : yes";
                                            }
                                            if($stu_info['st_details']['med_test']=='on')
                                            {
                                                echo "Has your child been tested by a learning Specialist or Psychologist : no";
                                            }
                                            else
                                            {
                                                echo "Has your child been tested by a learning Specialist or Psychologist : --";
                                            }
                                        ?></td>
                                        </tr>
                                        <tr><td width="200"></td>
                                        <td width="150"></td>
                                        <td width="700">
                                        <?php 
                                            if($stu_info['st_details']['med_xp']!="")
                                            {
                                               echo "If yes , explanation : ".$stu_info['st_details']['med_xp'];
                                            }
                                            else
                                            {
                                               echo "If yes , explanation : -- ";
                                            }
                                        ?>   
                                        </td>
                                        <tr>
                                        <td width="200" height="30"></td>
                                        <td width="150" height="30"></td>
                                        <td width="700" height="30"></td>
                                        </tr>
                                        </tr>
                                        <tr><td width="200"></td>
                                        <td width="150"></td>
                                        <td width="700">
                                        <?php 
                                            if($stu_info['st_details']['med_dis']=='yes')
                                            {
                                               echo "Does the child have physical or medical disability : yes"; 
                                            }
                                            if($stu_info['st_details']['med_dis']=='on')
                                            {
                                               echo "Does the child have physical or medical disability : no";
                                            }
                                            else
                                            {
                                                echo "Does the child have physical or medical disability : -- ";
                                            }
                                        ?>
                                        </td>
                                        </tr>
                                        <tr><td width="200"></td>
                                        <td width="150"></td>
                                        <td width="700">
                                        <?php 
                                            if($stu_info['st_details']['med_xp']!="")
                                            {
                                               echo "If yes , explanation : ".$stu_info['st_details']['med_xp']; 
                                            }
                                            else
                                            {
                                                echo "If yes , explenation : -- ";
                                            }
                                        ?>
                                        </td>
                                        </tr>
                                        <tr>
                                        <td width="200" height="30"></td>
                                        <td width="150" height="30"></td>
                                        <td width="700" height="30"></td>
                                        </tr>
                                        <tr>
                                        <td width="200"></td>
                                        <td width="150"></td>
                                        <td width="700">
                                        <?php  echo "<font color='green'>Admission Data </font>";
                                        ?>
                                        </td>
                                        </tr>
                                        <tr>
                                        <td width="200"></td>
                                        <td width="150"></td>
                                        <td width="700">
                                        <?php  
                                            if($stu_info['st_details']['ex_date']!=" ")
                                            {
                                                echo "Expected date of Enrollment : ".$stu_info['st_details']['ex_date'];
                                            }
                                            else
                                            {
                                                echo "Expected date of Enrollment : --";
                                            }
                                        ?>
                                        </td>
                                        </tr>
                                        <tr>
                                        <td width="200"></td>
                                        <td width="150"></td>
                                        <td width="700">
                                        <?php  
                                            if($stu_info['st_details']['len_stay']!=" ")
                                            {
                                                echo "Expected Length Of Stay : ".$stu_info['st_details']['len_stay'];
                                            }
                                            else
                                            {
                                                echo "Expected Length Of Stay : --";
                                            }
                                        ?>
                                        </td>
                                        </tr>
                                        <tr>
                                        <td width="200"></td>
                                        <td width="150"></td>
                                        <td width="700">
                                        <?php
                                            if($stu_info['st_details']['curriculum']!=" ")
                                            {
                                                echo "Curriculum : ".$stu_info['st_details']['curriculum'];
                                            }
                                            else
                                            {
                                                echo "Curriculum : --";
                                            }
                                        ?>
                                        </td>
                                        </tr>
                                        <tr>
                                        <td width="200"></td>
                                        <td width="150"></td>
                                        <td width="700">
                                        <?php
                                            if($stu_info['st_details']['exclude_transport']=='0')
                                            {
                                                echo "Would you like to send clild by school bus ? : Yes ";
                                            }
                                            if($stu_info['st_details']['exclude_transport']=='1')
                                            {
                                                echo "Would you like to send clild by school bus ? : No";
                                            }
                                            else if($stu_info['st_details']['exclude_transport']=="")
                                            {
                                                echo "Would you like to send clild by school bus ? : --";
                                            }
                                        ?>
                                        </td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>

                    </div>
                        
                </div>    
            </div>
        </section>    
   </div>
</div>
<script type="text/javascript">

</script>