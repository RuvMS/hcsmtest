<div class="row">
	<div class="col-md-12">
		<h3 class="page-header"><i class="fa fa-bank"></i> SHUFFLE STUDENT </h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
			<li><i class="fa fa-cog"></i>Student</li>
			<li><i class="fa fa-bank"></i>Shuffling</li>
		</ol>
	</div>
</div>
<div>
	<section class="panel">
	    <header class="panel-heading"> 
	        <div class="row">
	        	<div class="col-md-2">
	        		Shuffle Students 
	        	</div>
			</div>
	    </header>
        <form class="form-horizontal" role="form" method="post" id="shuflform" autocomplete="off" novalidate>
      	<div class="panel-body" id="edit_view">
      		<div class="row">
  				<div class="form-group col-md-3">
                    <label for="fax" class="col-md-3 control-label">Academic Year</label>
                    <div class="col-md-9">
                        <select class="form-control select2" data-validation="required" data-validation-error-msg-required="Field cannot be empty" id="cls_academicyear" name="cls_academicyear"  style="width: 100%;">
                        <option value="all">---Select academic year---</option>
                            <?php
                            foreach($ay_info as $row):
                                if($row['ac_iscurryear']==1)
                                {
                                    $selected = 'selected';
                                }
                                else
                                {
                                    $selected = '';
                                }
                                ?>
                                <option value="<?php echo $row['es_ac_year_id'];?>" <?php echo $selected;?>>
                                    <?php echo $row['ac_startdate']." - ".$row['ac_enddate'];?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-3">
                    <label for="cls_branch" class="col-md-3 control-label">Branch</label>
                    <div class="col-md-9">
                        <?php 
                            global $branchdrop;
                            global $selectedbr;
                            $extraattrs = 'id="cls_branch" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" onchange="load_class_list()"';
                            echo form_dropdown('cls_branch',$branchdrop,$selectedbr, $extraattrs); 
                        ?>
                  </div>
                </div>
              	<div class="form-group col-md-3">
                  	<label for="cls_grade" class="col-md-3 control-label">Grade</label>
                  	<div class="col-md-9">
                      	<select class="form-control" id="cls_grade" name="cls_grade">
                  			<option value="all">---Select Grade---</option>
                  			<?php
                            $grd = $this->db->get('hci_grade')->result_array();
                            foreach($grd as $row):
                                $selected="";
                                // if($row['grd_id']==$this->hci_studentreg_model->show_val('pre_class')){
                                //     $selected="selected";
                                // }
                                ?>
                                <option value="<?php echo $row['grd_id'];?>"  <?php echo $selected?>>
                                   <?php echo $row['grd_name'];?>
                                </option>
                                
                                <?php
                            endforeach;
                        ?>
                      	</select>
                  	</div>
              	</div> 
              	<div class="col-md-3">
                   <a class='btn btn-info btn-sm'  onclick='event.preventDefault();load_class_stulist()'>Search</a> <a class='btn btn-info btn-sm'  onclick='event.preventDefault();load_shuffleview()'>Shuffle Students</a> 
                </div>	
      		</div>	
      		<br>
      		<div class="row" id="stulist">
      			<div class="col-md-12">
      				<table class="table table-bordered">
		          		<thead id='clshead'>
		          		</thead>
		          		<tbody id="clsbody">
		          		</tbody>
		          	</table>
      			</div>
      		</div>
            <div class="row" id="shuffleview">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead id='shuflhead'>
                        </thead>
                        <tbody id="shuflbody">
                        </tbody>
                    </table>
                </div>
            </div>
      	</div>
        <br>
        <div class="row">
            <div class="form-group">
                <div class="col-md-offset-1 col-md-11">
                    <br>
                    <button type="submit" class="btn btn-info" onclick="event.preventDefault();save_suffling()">Save</button> 
                    <button type="button" class="btn btn-default" onclick="event.preventDefault();reset_view()">Back</button> 
                </div>
            </div>
        </div>
        </form>
  	</section>
</div>
<script type="text/javascript" src="<?php echo base_url('js/moment.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/bootstrap-datetimepicker.min.js'); ?>"></script>
<script type="text/javascript">

$( document ).ready(function() {
    $('#shuffleview').hide();
    $('#stulist').show();
});

function load_class_stulist()
{
    acyear = $('#cls_academicyear').val();
    grade  = $('#cls_grade').val();
    branch = $('#cls_branch').val();
    $('#clshead').empty();
    $('#clsbody').empty();

	$.post("<?php echo base_url('hci_student/load_class_stulist')?>",{'acyear':acyear,'grade':grade,'branch':branch},
	function(data)
	{
        if(data == 'denied')
        {
            funcres = {status:"denied", message:"You have no right to proceed the action"};
            result_notification(funcres);
        }
        else
        {
            headstr = '<tr>';  
            for (i = 0; i<data.length; i++) 
            {
                headstr += '<th>'+data[i]['cls_code']+'</th>'; 
            }
            headstr += '</tr>';

            bodystr = '<tr>';
            for (i = 0; i<data.length; i++) 
            { 
                bodystr += '<td>';
                if(data[i]['stulist'].length > 0)
                {
                    stulist = data[i]['stulist'];
                    for (x = 0; x<stulist.length; x++) 
                    {
                        bodystr += stulist[x]['st_id']+' - '+stulist[x]['family_name']+' '+stulist[x]['other_names']+'<br>';
                    }
                }
                else
                {
                    bodystr += 'No Student Found'; 
                }
                bodystr += '</td>'; 
            }
            bodystr += '</tr>';

            $('#clshead').append(headstr); 
            $('#clsbody').append(bodystr); 
        } 
	},	
	"json"
	);

    $('#shuffleview').hide();
    $('#stulist').show();
}

function load_shuffleview()
{
    acyear = $('#cls_academicyear').val();
    grade  = $('#cls_grade').val();
    branch = $('#cls_branch').val();
    $('#shuflhead').empty();
    $('#shuflbody').empty();

    $.post("<?php echo base_url('hci_student/load_shuffledata')?>",{'acyear':acyear,'grade':grade,'branch':branch},
    function(data)
    {
        if(data == 'denied')
        {
            funcres = {status:"denied", message:"You have no right to proceed the action"};
            result_notification(funcres);
        }
        else
        {
            headstr = '<tr>'; 
            headstr += '<th>Student</th>'; 
            for (i = 0; i<data['classes'].length; i++) 
            {
                headstr += '<th>'+data['classes'][i]['cls_code']+'</th>'; 
            }
            headstr += '</tr>';
            $('#shuflhead').append(headstr); 

            for (i = 0; i<data['stu_list'].length; i++) 
            { 
                bodystr = '<tr>';
                bodystr += '<td>'+data['stu_list'][i]['st_id']+' - '+data['stu_list'][i]['family_name']+' '+data['stu_list'][i]['other_names']+'</td>';
                for (x = 0; x<data['classes'].length; x++) 
                {
                    checked = '';

                    if(data['classes'][x]['cls_id']==data['stu_list'][i]['gp_class'])
                    {
                        checked = 'checked';
                    }
                    bodystr += '<td><input name="stuchk['+data['stu_list'][i]['id']+']" value="'+data['classes'][x]['cls_id']+'" type="radio" '+checked+'></td>'; 
                }
                bodystr += '</tr>';
                $('#shuflbody').append(bodystr); 
            }
        }
    },  
    "json"
    );

    $('#shuffleview').show();
    $('#stulist').hide();
}

function save_suffling()
{
    $.ajax(
    {
        url : "<?php echo base_url('hci_student/save_suffling')?>",
        type : 'POST',
        async : false,
        cache: false,
        dataType : 'json',
        data: $('#shuflform').serialize(),
        success:function(data)
        {
            if(data == 'denied')
            {
                funcres = {status:"denied", message:"You have no right to proceed the action"};
                result_notification(funcres);
            }
            else
            {
                load_class_stulist();
                $('#shuffleview').hide();
                $('#stulist').show();
                result_notification(data);
            }
        }
    });
}
</script>