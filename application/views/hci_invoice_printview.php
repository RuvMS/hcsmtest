<?php
        $this->load->library('tcpdf/tcpdf');
        $pdf = new TCPDF('P','mm',array('210','297'),true,'UTF-8',false);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->SetTitle('Invoice');
        $pdf->SetRightMargin(0);
        $pdf->SetFont('dejavusans', '', 10, '', true);
        foreach ($invlist as $inv) 
        {
             // $text += $inv['invoice_data']['inv_id'].$inv['other_names'];
             //$name = $inv['invoice_data']['other_names'];
            $tablerw = '';
            $x=0;
            foreach ($inv['invoice_fees'] as $fee) 
            {
                $tablerw .= "<tr><td>".$fee['invf_feedescription']."</td><td style='text-align:right'>".number_format($fee['invf_netamount'],2)."</td></tr>";
                $x++;
            }
            $tablerw .= "<tr><td>Total</td><td style='text-align:right'>".number_format($inv['invoice_data']['inv_totamount'],2)."</td></tr>";
            $tablerw .= "<tr><td>Discounts</td><td style='text-align:right'>".number_format((floatval($inv['invoice_data']['inv_totdiscounts'])+floatval($inv['invoice_data']['inv_extradiscounts'])),2)."</td></tr>";
            $tablerw .= "<tr><td>Other</td><td style='text-align:right'>".number_format((floatval($inv['invoice_data']['inv_totadditions'])+floatval($inv['invoice_data']['inv_extraadditions'])),2)."</td></tr>";
            $tablerw .= "<tr><td>Net Total</td><td style='text-align:right'>".number_format(($inv['invoice_data']['inv_netamount']),2)."</td></tr>";

            $html = '<div class="row"><div class="col-md-2"><strong>Stu ID #</strong>'.$inv['invoice_data']['inv_cusindex'].'</div></div>  '.
                    '<strong>Name: </strong>'. $inv['invoice_data']['inv_cusname'].'<br><br>'. 
                    '<strong>Class: </strong>'.$inv['invoice_data']['inv_description'].'<br>'.
                    '<div class="row"><div class="col-md-12">
                        Dear Parent,<br><br>
                        The payment for Term/ Service and Transport of your child for the Second Term is given below.
                    </div></div>'.
                        '<div class="row"><div class="col-md-2"></div><div class="col-md-8"><center>
                            <table style="font-size:12px;width:100%;">
                                <thead>
                                    <tr>
                                        <th style="padding: 3px;">Description</th>
                                        <th style="padding: 3px;">Amount</th>
                                    </tr>
                                </thead>
                                <tbody id="let_fees">'.$tablerw.'</tbody>'.
                            '</table></center>
                        </div></div>'.
                '<div class="row"><div class="col-md-12">
                    We have attached a deposit slip herewith for your convenience. Please fill in all the details given above when making the deposit to the bank and confirm with the Bank Assistant to enter the student ID number (NG xxx) to the bank system. While having the Customer copy for your reference, please hand over the Office copy of the slip to the class teacher through the child’s SRB immediately after the payment. This will avoid miscommunications and inconvenience in the future. 
                    Please note that the Brought Forward mentioned above contains over dues of the First Term fees/ Balance Admission. As per the Management policy on long outstanding, we will be strict on clearing off the over dues before the beginning of the new academic term. 
                    <br>
                    Second Term Fee Payments must be done on or before 15th December 2016.
                    <br>
                    Settlement of all your dues can be paid by cash to the school office or if paid to the bank the deposit slip must be handed over to the school finance division on or before the 17th December 2016 as the school office will be closed for Christmas Holidays.  
                    <br>
                    • The cost of the school magazine is included. Please note that you are required to pay for one copy per family.
                    <br>
                    • Please note that management has decided on a revision of school transport fee with effect from January 2017. 
                    <br>
                    Payments can be made through Debit and Credit cards. However, dated cheques will not be accepted. 
                    We look forward to your invaluable cooperation with regards to this to avoid any further miscommunication.
                    <br><br><br>
                    Thank you<br>

                    Registrar<br>
                    Horizon College International
                </div></div>'; 

            $pdf->AddPage();
            $pdf->writeHTML($html);
        }
        
        $pdf->Output('Invoice.pdf','I');
 ?>