<?php
        $this->load->library('tcpdf/tcpdf');
        $pdf = new TCPDF('P','mm',array('210','297'),true,'UTF-8',false);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->SetTitle('Credit Note');
        $pdf->SetRightMargin(0);
        $pdf->SetFont('dejavusans', '', 10, '', true);
        foreach ($cnotelist as $cnote) 
        {

            $html = 'Student : '.$cnote['cnote']['cnote_cusindex'].' - '.$cnote['cnote']['cnote_cusname'].
                    '<br>Amount : LKR '.number_format($cnote['cnote']['cnote_totamt'],2).
                    '<br>Credit Note for Invoice : '.$cnote['invoice']['inv_index']; 

            $pdf->AddPage();
            $pdf->writeHTML($html);
        }
        
        $pdf->Output('CreditNote.pdf','I');
 ?>