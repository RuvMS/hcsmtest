<div class="row">
    <div class="col-md-12">
        <h3 class="page-header"><i class="fa fa-users"></i> CHILD CARE CENTER FEE STRUCTURE</h3>
        <ol class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
            <li><i class="fa fa-cog"></i>Settings</li>
            <li><i class="fa fa-bank"></i>Child Care Centre Fee Structure Management</li>
        </ol>
    </div>
</div>
<div>
    <br>
    <div class="row">
        <div class="col-md-6">
            <section class="panel">
                <header class="panel-heading">
                    Create / Edit Fee Structure
                </header>
                <div class="panel-body">    
                    <form class="form-horizontal" role="form" method="post" action="<?php echo base_url('hci_childcare/save_feestructure')?>" id="route_form" autocomplete="off" novalidate>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Branch</label>
                            <div class="col-md-6">
                                <?php 
                                    global $branchdrop;
                                    global $selectedbr;
                                    $extraattrs = 'id="fs_branch" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty"';
                                    echo form_dropdown('fs_branch',$branchdrop,$selectedbr, $extraattrs); 
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="description" class="col-md-3 control-label">Fee Structure</label>
                            <div class="col-md-6">
                                <input type="hidden" name="fs_id" id="fs_id">
                                <input type="text" data-validation="required" data-validation-error-msg-required="Field can not be empty" class="form-control" id="description" name="description" placeholder="">
                            </div>
                        </div>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Admission</th>
                                    <th>Monthly</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Fullday</td>
                                    <td>
                                        <input type="text" data-validation="required" data-validation-error-msg-required="Field can not be empty" class="form-control" id="feeamt_1_1" name="feeamt_1_1" placeholder="">
                                    </td>
                                    <td>
                                        <input type="text" data-validation="required" data-validation-error-msg-required="Field can not be empty" class="form-control" id="feeamt_2_1" name="feeamt_2_1" placeholder="">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Halfday</td>
                                    <td>
                                        <input type="text" data-validation="required" data-validation-error-msg-required="Field can not be empty" class="form-control" id="feeamt_1_2" name="feeamt_1_2" placeholder="">
                                    </td>
                                    <td>
                                        <input type="text" data-validation="required" data-validation-error-msg-required="Field can not be empty" class="form-control" id="feeamt_2_2" name="feeamt_2_2" placeholder="">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        <div class="form-group">
                            <div class="col-md-offset-3 col-md-11">
                                <br>
                                <button type="submit" class="btn btn-info">Save</button> 
                                <button type="submit" class="btn btn-default">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
        <div class="col-md-6">
            <section class="panel">
                <header class="panel-heading">
                   Look Up
                </header>
                <div class="panel-body">  
                    Look up goes here  
                    <!-- <table class="table">
                        <thead>
                            <tr>
                                <th>Picking Point</th>
                                <th>Route</th>
                                <th>Type</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach ($picks as $pick) 
                                {
                                    $type = 'Beyond 2km';
                                    if($pick['tpp_stype']==1)
                                    {
                                        $type = 'Within 2km';
                                    }
                                    echo "<tr>";
                                    echo "<td>".$pick['place_name']."</td>";   
                                    echo "<td>".$pick['route_title']."</td>";   
                                    echo "<td>".$type."</td>";   
                                    echo "<td><a class='btn btn-info btn-sm' onclick='event.preventDefault();edit_pay_plan(".$pick['tr_place_id'].")'>Edit</a></td>";
                                    echo "</tr>";
                                }
                            ?>
                        </tbody>
                    </table> -->
                </div>
            </section>
        </div>
    </div>
</div>