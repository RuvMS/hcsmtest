<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/jquery-confirm.css'); ?>"> 
<script type="text/javascript" src="<?php echo base_url('js/jquery-confirm.js')?>"></script><!--jquery-->
<div class="row">
    <div class="col-md-11">
        <h3 class="page-header"><i class="fa fa-users"></i> REGISTRATION FORM</h3>
        <ol class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
            <li><i class="fa fa-cog"></i>Settings</li>
            <li><i class="fa fa-bank"></i>Admission</li>
        </ol>
    </div>
    <div class="col-md-1"><br><br><a id="fullviewlink" onclick="event.preventDefault();change_view(null)" class="btn btn-sm btn-info">Full View</a><input type="hidden" name="formview" id="formview" value="major"></div>
</div>
<div>
    <form class="form-horizontal" role="form" method="post"  id="reg_form" action="<?php echo base_url('hci_studentreg/new_studentreg')?>"  autocomplete="off" novalidate>
    <section class="panel majordetail">
        <br>
        <div class="panel-body">
            <div class="row">
                <div class="form-group col-md-6">
                    <label class="col-md-3 control-label">Branch <span style="color:red;font-size: 16px">*</span></label>
                    <div class="col-md-6">
                        <?php 
                            global $branchdrop;
                    
                            if($this->hci_studentreg_model->show_val('es_enquiryid'))
                            {
                                $selectedbr = $this->hci_studentreg_model->show_val('eq_branch');
                            }
                            else
                            {
                                global $selectedbr;
                            }

                            $extraattrs = 'id="st_branch" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" onchange="load_feestructures(this.value);load_branch_student(this.value);"';
                            echo form_dropdown('post[st_branch]',$branchdrop,$selectedbr, $extraattrs); 
                        ?>
                  </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="fax" class="col-md-3 control-label">Year <span style="color:red;font-size: 16px">*</span></label>
                    <div class="col-md-9">
                        <select class="form-control select2" data-validation="required" data-validation-error-msg-required="Field cannot be empty" id="st_grade" name="post[st_grade]"  style="width: 100%;" onchange="display_prefmethod_view(null,this.value)">
                        <option value="">---Select Grade---</option>
                            <?php
                            $grade = $this->db->get('hci_grade')->result_array();
                            foreach($grade as $row):
                                $selected="";
                                if($row['grd_id']==$this->hci_studentreg_model->show_val('pre_class')){
                                    $selected="selected";
                                }
                                ?>
                                <option value="<?php echo $row['grd_id'];?>"  <?php echo $selected?>>
                                    <?php echo $row['grd_name'];?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label for="fax" class="col-md-3 control-label">Academic Year <span style="color:red;font-size: 16px">*</span></label>
                    <div class="col-md-9">
                        <select class="form-control select2" data-validation="required" onchange="load_academic_data(this.value)" data-validation-error-msg-required="Field cannot be empty" id="acc_year" name="post[st_accyear]"  style="width: 100%;">
                        <option value="">---Select academic year---</option>
                            <?php
                            foreach($acc_years as $row):
                                if($row['ac_iscurryear']==1)
                                {
                                    $selected = 'selected';
                                }
                                else
                                {
                                    $selected = '';
                                }
                                ?>
                                <option value="<?php echo $row['es_ac_year_id'];?>" <?php echo $selected;?>>
                                    <?php echo $row['ac_startdate']." - ".$row['ac_enddate'];?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="fax" class="col-md-3 control-label">Term <span style="color:red;font-size: 16px">*</span></label>
                    <div class="col-md-9">
                        <select class="form-control select2" data-validation="required" data-validation-error-msg-required="Field cannot be empty" id="st_term" name="post[st_term]"  style="width: 100%;">
                            <option value="">---Select Term---</option>
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label for="fax" class="col-md-3 control-label">Intake <span style="color:red;font-size: 16px">*</span></label>
                    <div class="col-md-9">
                        <select class="form-control select2" data-validation="required" data-validation-error-msg-required="Field cannot be empty" id="st_intake" name="post[st_intake]"  style="width: 100%;">
                            <option value="">---Select Intake---</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="panel majordetail">
        <header class="panel-heading">
            Student Information
        </header>
        <div class="panel-body">
            <div class="form-group">
                <?php 
                    $firstname = $this->hci_studentreg_model->show_val('eq_name');
                    $lastname = $this->hci_studentreg_model->show_val('eq_lastname');
                ?>
                <label for="name" class="col-md-2 control-label">Family Name <span style="color:red;font-size: 16px">*</span></label>
                <div class="col-md-8">
                    <input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="fname" name="post[family_name]" placeholder="" value="<?php echo $lastname;?>">
                </div>
            </div>
            <input type="hidden" id="st_enqid" name="post[st_enqid]" value="<?php echo $this->hci_studentreg_model->show_val('es_enquiryid'); ?>">
            <div class="form-group">
                <label for="brnum" class="col-md-2 control-label">Other Names <span style="color:red;font-size: 16px">*</span></label>
                <div class="col-md-8">
                    <input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="oname" name="post[other_names]" placeholder="" value="<?php echo $firstname;?>">
                </div>
            </div>
            <?php
                $ss = $this->hci_studentreg_model->show_val('eq_sex');
                $selected_gender1 =  '';
                $selected_gender2 =  '';
                if($ss == 'M')
                {
                    $selected_gender1 =  'checked="checked"';
                }elseif($ss == 'F')
                {
                    $selected_gender2 =  'checked="checked"';
                }
            ?>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="comcode" class="col-sm-5 control-label">Gender </label>
                        <div class="col-sm-7">
                            <label class="col-md-3 control-label">Male</label>
                            <input type="radio" name="post[gender]" class="col-md-1" id="gender" value="M" <?php echo $selected_gender1 ?>>

                            <label class="col-md-3 control-label">Female</label>
                            <input type="radio" name="post[gender]" id="gender" class="col-md-1" value="F" <?php echo  $selected_gender2 ?>>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Date Of Birth</label>
                        <div class="col-sm-6">
                            <div id="" class="input-group date" >
                                <input class="form-control datepicker" type="text" name="post[dob]" id="birthday"  data-format="YYYY-MM-DD" value="<?php echo $this->hci_studentreg_model->show_val('eq_dob')?>">
                                    <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="comcode" class="col-sm-5 control-label">Nationality </label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="local_nationality" name="post[local_nationality]" placeholder="" value="">
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <label for="st_religion" class="col-md-3 control-label">Religion</label>
                        <div class="col-md-6">
                            <select class="form-control" id="st_religion" name="post[st_religion]"  style="width: 100%;">
                                <option value=""></option>
                                <?php
                                    $rlgn = $this->db->get('hgc_religion')->result_array();
                                    foreach($rlgn as $row):
                                        $selected="";
                                        // if($row['grd_id']==$this->hci_studentreg_model->show_val('pre_class')){
                                        //     $selected="selected";
                                        // }
                                        ?>
                                        <option value="<?php echo $row['rel_id'];?>"  <?php echo $selected?>>
                                            <?php echo $row['rel_name'];?>
                                        </option>
                                        <?php
                                    endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <br><br>
            <div class="form-group">
                <label for="fax" class="col-md-5 control-label">Student Information should be communicated with <span style="color:red;font-size: 16px">*</span></label>
                <div class="col-md-6">
                    <div class="col-sm-7">
                        <label class="col-md-3 control-label">Father</label>
                        <input type="radio" data-validation="required" name="post[st_infComPerson]" class="col-md-1" id="m" value="1">

                        <label class="col-md-3 control-label">Mother</label>
                        <input type="radio" data-validation="required" name="post[st_infComPerson]" id="m" class="col-md-1 "  value="2">

                        <label class="col-md-3 control-label">Guardian</label>

                        <input type="radio" data-validation="required" name="post[st_infComPerson]" id="m" class="col-md-1 "  value="3">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="panel minordetails">
        <header class="panel-heading">
            For Foreign Students
        </header>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="comcode" class="col-sm-5 control-label">Citizenship </label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="citizenship" name="post[citizenship]" placeholder="" value="">
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Nationality </label>
                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="foreign_nationality" name="post[foreign_nationality]" placeholder=""  value="">

                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="comcode" class="col-sm-5 control-label">Passport Number </label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="passport" name="post[passport]" placeholder=""  data-validation-optional="true" value="">
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Place/Date Of Issue </label>
                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="issue" name="post[issue]" placeholder=""  data-validation-optional="true" value="">

                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="comcode" class="col-sm-5 control-label">First Language </label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="language" name="post[language]" placeholder="" data-validation-optional="true" value="">
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Language spoken at home </label>
                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="lang_home" name="post[lang_home]" placeholder="" data-validation-optional="true" value="">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="panel minordetails">
        <header class="panel-heading">
            Personal Data : Family
        </header>
        <div class="panel-body">
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Mother's Name</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="mname" name="post[mom_name]" placeholder="" value="<?php echo $this->hci_studentreg_model->show_val('eq_mothername')?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">If mother works in staff</label>
                <div class="col-md-4">
                    <select class="form-control select2" id="st_motherstaff" name="post[st_motherstaff]" onchange="stuff_mother(this.value)" onclick='//event.preventDefault();view_staff("<?php echo $data['stf_id'];?>")' style="width: 100%;">
                    <option value="">---Select Mother---</option>
                        <?php
                         $staf = $this->db->get('hgc_staff')->result_array();
                        foreach($staf as $row):
                           
                            if($row['stf_gender']=='F' && $row['stf_status']=='A')
                            {
                            ?>
                            <option value="<?php echo $row['stf_id'];?>">
                                <?php echo $row['stf_firstname'].' '.$row['stf_lastname'];?>
                            </option>
                            <?php
                            }
                        endforeach;
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Address </label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="maddress1" name="post[mom_addy]" placeholder="" onkeyup="$('#faddress1').val(this.value)" value="<?php echo $this->hci_studentreg_model->show_val('eq_motheraddress1')?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label"></label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="maddress2" name="post[mom_add2]" placeholder=""  onkeyup="$('#faddress2').val(this.value)" value="<?php echo $this->hci_studentreg_model->show_val('eq_motheraddress2')?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label"></label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="maddress3" name="post[mom_addcity]" placeholder="City" onkeyup="$('#faddress3').val(this.value)" value="<?php echo $this->hci_studentreg_model->show_val('eq_mothercity')?>">
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="comcode" class="col-sm-5 control-label">Home Number </label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" data-validation="number length" data-validation-length="10-10" data-validation-error-msg-number="Invalid. Please Try Again. ex: 0111234567" data-validation-error-msg-length="Must be 10 characters long" id="mhome" name="post[mom_home]" onkeyup="$('#fhome').val(this.value)" placeholder=""  data-validation-optional="true" value="<?php echo $this->hci_studentreg_model->show_val('eq_phno2')?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Mobile Number </label>
                        <div class="col-sm-6">

                            <input type="text" class="form-control" data-validation="number length" data-validation-length="10-10" data-validation-error-msg-number="Invalid. Please Try Again. ex: 0111234567" data-validation-error-msg-length="Must be 10 characters long" id="mom_mobile" name="post[mom_mobile]" placeholder=""  data-validation-optional="true" value="<?php echo $this->hci_studentreg_model->show_val('eq_mothermobile')?>">

                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="comcode" class="col-sm-5 control-label">Alt. Number </label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" data-validation="number length" data-validation-length="10-10" data-validation-error-msg-number="Invalid. Please Try Again. ex: 0111234567" data-validation-error-msg-length="Must be 10 characters long" id="malt" name="post[mom_alt]" placeholder=""  data-validation-optional="true" value="<?php echo $this->hci_studentreg_model->show_val('eq_motheraltno')?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Foreign Number </label>
                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="mom_fornum" name="post[mom_fornum]" placeholder="" value="<?php echo $this->hci_studentreg_model->show_val('eq_foreignnum2')?>">

                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Email Address</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="memail" name="post[mom_email]" placeholder="" data-validation="email" data-validation-error-msg-email="Invalid E-mail"  data-validation-optional="true" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Father's Name</label>
                <div class="col-md-3">
                    <input type="text" class="form-control" id="fathername" name="post[dad_name]" placeholder="" value="<?php echo $this->hci_studentreg_model->show_val('eq_fathername')?>">
                </div>

                <label for="fax" class="col-md-2 control-label">If father works in staff</label>
                <div class="col-md-4">
                    <select class="form-control select2" id="st_fatherstaff" name="post[st_fatherstaff]" onchange="staff_father(this.value)" onclick='//event.preventDefault();view_staff("<?php echo $data['stf_id'];?>")' style="width: 100%;">
                    <option value="">---Select Father---</option>
                        <?php
                         $staf = $this->db->get('hgc_staff')->result_array();
                        foreach($staf as $row):
                            
                            if($row['stf_gender']=='M' && $row['stf_status']=='A')
                            {
                            ?>
                            <option value="<?php echo $row['stf_id'];?>">
                                <?php echo $row['stf_firstname'].' '.$row['stf_lastname'];?>
                            </option>
                            <?php
                            }
                        endforeach;
                        ?>
                    </select>
                </div>

            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Address</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="faddress1" name="post[dad_addy]" placeholder="" value="<?php echo $this->hci_studentreg_model->show_val('eq_address')?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label"></label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="faddress2" name="post[dad_add2]" placeholder="" value="<?php echo $this->hci_studentreg_model->show_val('eq_fatheraddress2')?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label"></label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="faddress3" name="post[dad_addcity]" placeholder="City" value="<?php echo $this->hci_studentreg_model->show_val('eq_city')?>">
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="comcode" class="col-sm-5 control-label">Home Number </label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" data-validation="number length" data-validation-length="10-10" data-validation-error-msg-number="Invalid. Please Try Again. ex: 0111234567" data-validation-error-msg-length="Must be 10 characters long" id="fhome" name="post[dad_home]" placeholder=""  data-validation-optional="true" value="<?php echo $this->hci_studentreg_model->show_val('eq_phno')?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Mobile Number </label>
                        <div class="col-sm-6">

                            <input type="text" class="form-control" data-validation="number length" data-validation-length="10-10" data-validation-error-msg-number="Invalid. Please Try Again. ex: 0111234567" data-validation-error-msg-length="Must be 10 characters long" id="dad_mobile" name="post[dad_mobile]" placeholder=""  data-validation-optional="true" value="<?php echo $this->hci_studentreg_model->show_val('eq_mobile')?>">

                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="comcode" class="col-sm-5 control-label">Alt. Number </label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" data-validation="number length" data-validation-length="10-10" data-validation-error-msg-number="Invalid. Please Try Again. ex: 0111234567" data-validation-error-msg-length="Must be 10 characters long" id="malt" name="post[dad_alt]" placeholder=""  data-validation-optional="true" value="<?php echo $this->hci_studentreg_model->show_val('eq_altno')?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Foreign Number </label>
                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="mom_fornum" name="post[dad_fornum]" placeholder="" value="<?php echo $this->hci_studentreg_model->show_val('eq_foreignnum1')?>">

                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Email Address</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="dad_email" name="post[dad_email]" placeholder="" data-validation="email" data-validation-error-msg-email="Invalid E-mail"  data-validation-optional="true" value="<?php echo $this->hci_studentreg_model->show_val('eq_emailid')?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Name Of Guardian</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="guar_name" name="post[guar_name]" placeholder="" value="<?php echo $this->hci_studentreg_model->show_val('eq_personname')?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Address</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="guar_addy" name="post[guar_addy]" placeholder=""  value="<?php echo $this->hci_studentreg_model->show_val('eq_personaddress1')?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label"></label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="guar_add2" name="post[guar_add2]" placeholder="" value="<?php echo $this->hci_studentreg_model->show_val('eq_personaddress2')?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label"></label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="guar_addcity" name="post[guar_addcity]" placeholder="City" value="<?php echo $this->hci_studentreg_model->show_val('eq_personcity')?>">
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="comcode" class="col-sm-5 control-label">Home Number </label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" data-validation="number length" data-validation-length="10-10" data-validation-error-msg-number="Invalid. Please Try Again. ex: 0111234567" data-validation-error-msg-length="Must be 10 characters long" id="guar_home" name="post[guar_home]" placeholder="" data-validation="number length"  data-validation-optional="true" value="">
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Mobile Number </label>
                        <div class="col-sm-6">

                            <input type="text" class="form-control" data-validation="number length" data-validation-length="10-10" data-validation-error-msg-number="Invalid. Please Try Again. ex: 0111234567" data-validation-error-msg-length="Must be 10 characters long" id="guar_mobile" name="post[guar_mobile]" placeholder=""  data-validation-optional="true" value="<?php echo $this->hci_studentreg_model->show_val('eq_personmobile')?>">

                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Email Address</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="guar_email" name="post[guar_email]" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="">
                </div>
            </div>
        </div>
    </section>
    <section class="panel minordetails">
        <header class="panel-heading">
            Emergency Contact ( Other Than Parent/ Guardian)
        </header>
        <div class="panel-body">
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Name</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="pname" name="post[emg_name]" placeholder="" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Address</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="paddress1" name="post[emg_addy]" placeholder="" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label"></label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="paddress2" name="post[emg_add2]" placeholder="" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label"></label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="paddress3" name="post[emg_addcity]" placeholder="City" value="">
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="comcode" class="col-sm-5 control-label">Home Number </label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" data-validation="number length" data-validation-length="10-10" data-validation-error-msg-number="Invalid. Please Try Again. ex: 0111234567" data-validation-error-msg-length="Must be 10 characters long" id="grade" name="post[emg_home]" placeholder=""  data-validation-optional="true" value="">
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Mobile Number </label>
                        <div class="col-sm-6">

                            <input type="text" class="form-control" data-validation="number length" data-validation-length="10-10" data-validation-error-msg-number="Invalid. Please Try Again. ex: 0111234567" data-validation-error-msg-length="Must be 10 characters long" id="grade" name="post[emg_mobile]" placeholder=""  data-validation-optional="true" value="">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="panel minordetails">
        <header class="panel-heading">
            School History - Student
        </header>
        <div class="panel-body">
            <div class="form-group">
                <label for="fax" class="col-md-4 control-label">Age which the child started formal schooling</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="paddress3" name="post[school_age]" value="">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label bold " style=" padding-left: 264px;color: green; ">Name/Address of the recent school attended.</label>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Name</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="pname" name="post[1sch_name]" placeholder="" value="<?php echo $this->hci_studentreg_model->show_val('eq_prev_university')?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Address</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="paddress1" name="post[1sch_addy]" placeholder="" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label"></label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="paddress2" name="post[1sch_add2]" placeholder="" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label"></label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="paddress3" name="post[1sch_addcity]" placeholder="" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Language of Instruction</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="paddress3" name="post[1sch_lang]" placeholder="" value="">
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="comcode" class="col-sm-5 control-label">From</label>
                        <div class="col-sm-7">
                            <div id="" class="input-group date" >
                                <input class="form-control datepicker" type="text" name="post[ffrom]" id="birthday"  data-format="YYYY-MM-DD" value="">
                                    <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">To</label>
                        <div class="col-sm-6">

                            <div id="" class="input-group date" >
                                <input class="form-control datepicker" type="text" name="post[fto]" id="birthday"  data-format="YYYY-MM-DD" value="">
                                    <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span>
                                </span>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Current Year/Grade</label>
                <div class="col-md-5">
                    <select class="form-control select2" name="post[st_curgrade]"  style="width: 100%;">
                    <option value="">---Select Current Year---</option>
                    <?php
                    $grade = $this->db->get('hci_grade')->result_array();
                    foreach($grade as $row):
                        $selected="";
                        if($row['grd_id']==$this->hci_studentreg_model->show_val('eq_class')){
                            $selected="selected";
                        }
                        ?>
                        <option value="<?php echo $row['grd_id'];?>"  <?php echo $selected?>>
                            <?php echo $row['grd_name'];?>
                        </option>
                        <?php
                    endforeach;
                    ?>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-5 control-label">Was your child withdraw from any school on request from its Management</label>
                <div class="col-md-5">
                    <div class="col-sm-7">
                        <label class="col-md-3 control-label">No</label>
                        <input type="radio" name="post[mgt_wd]" class="col-md-1" id="ss" value="-">

                        <label class="col-md-3 control-label">Yes</label>
                        <input type="radio" name="post[mgt_wd]" id="ss" class="col-md-1"  value="on">
                    </div>
                </div>                   
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">If yes , Please explain</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="" name="post[mgt_exp]" placeholder="" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-4 control-label">Please list special interests of school activities in which the child is/has been involved</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="" name="post[int_act]" placeholder="" value="">
                </div>
            </div>
        </div>
    </section>
    <section class="panel minordetails">
        <div class="panel-body">
            <br>
            <div class="form-group">
                <label for="fax" class="col-md-5 control-label">If english is not the child's first language /mother tongue,Has the child had instruction in or experience of English  </label>
                <div class="col-md-6">
                    <div class="col-sm-7">
                        <label class="col-md-3 control-label">No</label>
                        <input type="radio" name="post[eng_xp]" class="col-md-1" id="m" value="-">

                        <label class="col-md-3 control-label">Yes</label>
                        <input type="radio" name="post[eng_xp]" id="m" class="col-md-1 "  value="on">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">If yes , in what situation ?</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="" placeholder="" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Far how long</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="" placeholder="" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-5 control-label">Has your child been enrolled in any type special Education Programme  </label>
                <div class="col-md-6">
                    <div class="col-sm-7">
                        <label class="col-md-3 control-label">No</label>
                        <input type="radio" name="post[enr_sped]" class="col-md-1" id="g" value="-">

                        <label class="col-md-3 control-label">Yes</label>
                        <input type="radio" name="post[enr_sped]" id="g" class="col-md-1 "  value="on">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">If yes , Please explain</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="" placeholder="" name="post[sped_xp]" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-5 control-label">Has your child been tested by a learning Specialist or Psychologist   </label>
                <div class="col-md-6">
                    <div class="col-sm-7">
                        <label class="col-md-3 control-label">No</label>
                        <input type="radio" name="post[med_test]" class="col-md-1" id="p" value="-">

                        <label class="col-md-3 control-label">Yes</label>
                        <input type="radio" name="post[med_test]" id="p" class="col-md-1 "  value="on">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">If yes , Please explain</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="" placeholder="" name="post[med_xp]" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-5 control-label">Does the child have physical or medical disability </label>
                <div class="col-md-6">
                    <div class="col-sm-7">
                        <label class="col-md-3 control-label">No</label>
                        <input type="radio" name="post[med_dis]" class="col-md-1" id="d" value="-">

                        <label class="col-md-3 control-label">Yes</label>
                        <input type="radio" name="post[med_dis]" id="d" class="col-md-1 "  value="on">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">If yes , Please explain</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="" placeholder="" name="post[dis_xp]" value="">
                </div>
            </div>
        </div>
    </section>
    <section class="panel minordetails">
        <header class="panel-heading">
            Employment Data - Father
        </header>
        <div class="panel-body">
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Business Affiliation</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="dad_aff" name="post[dad_aff]" placeholder="" value="<?php echo $this->hci_studentreg_model->show_val('eq_fathersplaceofwork')?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Position</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="dad_pos" name="post[dad_pos]" placeholder="" value="<?php echo $this->hci_studentreg_model->show_val('eq_fathersoccupation')?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Business Address</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="dad_biz_addy" name="post[dad_biz_addy]" placeholder="" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Telephone</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="dad_biz_tel" name="post[dad_biz_tel]" placeholder="" data-validation="number length" data-validation-length="10-10" data-validation-error-msg-number="Invalid. Please Try Again. ex: 0111234567" data-validation-error-msg-length="Must be 10 characters long" data-validation-optional="true" value="">
                </div>
            </div>
        </div>
    </section>
    <section class="panel minordetails">
        <header class="panel-heading">
            Employment Data - Mother
        </header>
        <div class="panel-body">
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Business Affiliation</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="mom_aff" name="post[mom_aff]" placeholder="" value="<?php echo $this->hci_studentreg_model->show_val('eq_motherplaceofwork')?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Position</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="mom_pos" name="post[mom_pos]" placeholder="" value="<?php echo $this->hci_studentreg_model->show_val('eq_motheroccupation')?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Business Address</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="mom_biz_addy" name="post[mom_biz_addy]" placeholder="" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Telephone</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="mom_biz_tel" name="post[mom_biz_tel]" placeholder="" data-validation="number length" data-validation-length="10-10" data-validation-error-msg-number="Invalid. Please Try Again. ex: 0111234567" data-validation-error-msg-length="Must be 10 characters long" data-validation-optional="true" value="">
                </div>
            </div>
        </div>
    </section>
    <section class="panel minordetails">
        <header class="panel-heading">
            Admission Data
        </header>
        <div class="panel-body">
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Expected date of Enrollment</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="" name="post[ex_date]" placeholder="" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Expected Length Of Stay</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="" name="post[len_stay]" placeholder="" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Curriculum</label>
                <div class="col-md-5"><!-- <?=$selected?> -->
                    <select class="form-control select2" name="post[curriculum]"  style="width: 100%;">
                        <?php
                            // $selected="";
                            // if($row['grd_id']==$this->hci_studentreg_model->show_val('st_grade')){
                            //     $selected="selected";
                            // }
                        ?>
                        <option value=""></option>
                        <option value="UK">UK</option>
                        <option value="Local">Local</option>
                    </select>
                </div>
            </div>
        </div>
    </section>
    <section class="panel majordetail">
        <header class="panel-heading">
            Payment Information
        </header>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="brnum" class="col-md-4 control-label">Student Siblings </label>
                        <div class="col-md-8">
                                <select class="form-control select2" value="" onchange="load_sibling_data('sibs')" name="es_sibiling[]" multiple="multiple" style="width: 100%;" id="sib_select">
                                </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="st_admintemplate" class="col-md-4 control-label">Admission Template <span style="color:red;font-size: 16px">*</span></label>
                        <div class="col-md-8">
                            <select type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Select Admission Fee Template" id="st_admintemplate" name="post[st_admintemplate]" onchange="display_prefmethod_view(this.value,null)">
                             <!-- onchange="load_sibling_data('fss')" -->
                                <option value="">---Select Fee Structure---</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" id="adm_paymethod">
                        <label for="fax" class="col-md-4 control-label">Preferred Admission Payment method ? <span style="color:red;font-size: 16px">*</span></label>
                        <div class="col-md-8">
                            <div class="col-sm-7">
                                <label class="col-md-3 control-label">Total Payment</label>
                                <input type="radio" name="post[st_addpaymethod]"  class="col-md-1 st_addpaymethod" data-validation="required" value="1">

                                <label class="col-md-3 control-label">Slabwise Payments</label>
                                <input type="radio" name="post[st_addpaymethod]"  class="col-md-1 st_addpaymethod" data-validation="required"  value="2">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="st_termtemplate" class="col-md-4 control-label">Term Template <span style="color:red;font-size: 16px">*</span></label>
                        <div class="col-md-8">
                            <select type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Select Term Fee Template" id="st_termtemplate" name="post[st_termtemplate]">
                                <option value="">---Select Fee Structure---</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6" id="siblinginfo">
                </div>
            </div>
            <br>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Default Payment Scheme <span style="color:red;font-size: 16px">*</span></label>
            </div>
            <div class="form-group" id="paymentschemediv">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th></th>
                            <?php
                                foreach ($fees as $fee) 
                                {
                                    if($fee['fc_feestructure']==1 || $fee['fc_feestructure']==2)
                                    {
                                        echo "<th>".$fee['fc_name']."</th>";
                                    }
                                }
                            ?>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Payment Scheme</td>
                            <?php
                                foreach ($fees as $fee) 
                                {
                                    if($fee['fc_feestructure']==1 || $fee['fc_feestructure']==2)
                                    {
                                        echo "<td>";
                                        if(!empty($fee['plans']))
                                        {
                                            echo "<select data-validation='required' data-validation-error-msg-required='Field can not be empty' id='pplan_".$fee['fc_id']."' name='pplan[]' class='form-control paymentplaninput'>";
                                            echo "<option></option>";
                                            foreach ($fee['plans'] as $pln) 
                                            {
                                                if($pln['plan_default']==1)
                                                {
                                                    $selected = 'selected';
                                                }
                                                else
                                                {
                                                    $selected = '';
                                                }

                                                echo "<option value='".$pln['plan_id']."' ".$selected.">".$pln['plan_name']."</option>";
                                                
                                            }
                                            echo "</select>";
                                        }
                                        else
                                        {
                                            echo "<select id='pplan_".$fee['fc_id']."' name='pplan[]' class='form-control'>";
                                            echo "<option></option>";
                                            echo "</select>";
                                        }
                                        
                                        echo "</td>";
                                    }
                                }
                            ?>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Applicable Discounts</label>
                <!-- <div class="col-md-6" id="discounts_list">
                    <?php
                        foreach ($siblingdiscs as $sibdisc) 
                        {
                    ?>
                            <label class="label_check c_on" for="checkbox-01"><input name="discountschk[]" value="<?php echo $sibdisc['adj_id']?>" type="checkbox"> <?php echo $sibdisc['adj_description']?></label><br>
                    <?php
                        }
                    ?>
                </div> -->
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">   
                        <div class="col-md-1"></div>
                        <div class="col-md-11"> 
                            <div>
                                <div class="row">
                                    <div class="form-group col-md-3" style="text-align : center"><strong>Description</strong></div>
                                    <div class="form-group col-md-2" style="text-align : center"><strong>Type</strong></div>
                                    <div class="form-group col-md-2" style="text-align : center"><strong>Fee Cat.</strong></div>
                                    <div class="form-group col-md-2" style="text-align : center"><strong>% / Val</strong></div>
                                    <div class="form-group col-md-1" style="text-align : center"><strong>Amount</strong></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-11">
                            <div class="clone_div" id="clone_div">
                                <div id="clonedInput1" class="clonedInput row">
                                    <input type="hidden" class="discinput" name="iId[]" id="iId" value="0">
                                    <div class="form-group col-md-3">
                                        <input type="text" name="studisc" id="studisc" class="form-control">
                                    </div>
                                    <div class="form-group col-md-2">
                                        <select type="text" class="form-control new-entry" id="adj_type" name="adj_type">
                                            <option value=''></option>
                                            <option value='A'>(+) Addition</option>
                                            <option value='D'>(-) Deduction</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <select class="form-control" id="adj_fee" name="adj_fee">
                                            <option value=''></option>
                                            <option value='0'>Any Fee Category</option>
                                            <?php
                                                foreach ($fees as $fee)
                                                {
                                                    echo "<option value='".$fee['fc_id']."'>".$fee['fc_name']."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <select type="text" class="form-control" id="adj_calamttype" name="adj_calamttype">
                                            <option value=''></option>
                                            <option value='V'>Value</option>
                                            <option value='P'>Percentage (%)</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <input type="text" name="amount" id="amount" class="form-control">
                                    </div>
                                    <div class="col-md-1" style="text-align:left; padding-left:5px; padding-right:5px;">
                                        <span class="button-group">
                                            <button onclick="cloning()" type="button" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-plus"></span></button>
                                            <button type="button" name = "remove_entry[]" class="btn btn-default btn-xs remove_entry"><span class="glyphicon glyphicon-minus"></span></button>
                                        </span>
                                    </div>
                                </div>                          
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
    <section class="panel">
        <div class="panel-body">
            <br>
            <div class="form-group">
                <div class="col-md-offset-2 col-md-11">
                    <button type="submit" name="save_btn" id="save_btn" class="btn btn-info">Save</button>
                    <button type="submit" onclick="event.preventDefault();verify_data()" name="regi_btn" id="regi_btn" class="btn btn-info">Register</button>
                    <button onclick="event.preventDefault();$('#reg_form').trigger('reset');$('#st_enqid').val('');" class="btn btn-default">Reset</button>
                </div>
            </div>
        </div>
    </section>
    </form>
</div>
<!-- Modal Invoice-->
<div class="modal fade bs-example-modal-lg" id="confinputmodal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                Verify Student Details
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3"><label>Branch</label></div>
                    <div class="col-md-9" id="verfi_branch"></div>
                </div>
                <div class="row">
                    <div class="col-md-3"><label>Year</label></div>
                    <div class="col-md-9" id="verfi_year"></div>
                </div>
                <div class="row">
                    <div class="col-md-3"><label>Academic Year</label></div>
                    <div class="col-md-9" id="verfi_accyear"></div>
                </div>
                <div class="row">
                    <div class="col-md-3"><label>Term</label></div>
                    <div class="col-md-9" id="verfi_term"></div>
                </div>
                <div class="row">
                    <div class="col-md-3"><label>Intake</label></div>
                    <div class="col-md-9" id="verfi_intake"></div>
                </div>
                <div class="row">
                    <div class="col-md-3"><label>Family Name</label></div>
                    <div class="col-md-9" id="verfi_fname"></div>
                </div>
                <div class="row">
                    <div class="col-md-3"><label>Other Name</label></div>
                    <div class="col-md-9" id="verfi_oname"></div>
                </div>
                <div class="row">
                    <div class="col-md-3"><label>Siblings</label></div>
                    <div class="col-md-9" id="verfi_sibs"></div>
                </div>
                <div class="row">
                    <div class="col-md-3"><label>Admission Template</label></div>
                    <div class="col-md-9" id="verfi_admtemp"></div>
                </div>
                <div class="row">
                    <div class="col-md-3"><label>Term Template</label></div>
                    <div class="col-md-9" id="verfi_trmtemp"></div>
                </div>
                <div class="row">
                    <div class="col-md-3"><label>Admission Payment category</label></div>
                    <div class="col-md-9" id="verfi_admpaymeth"></div>
                </div>
                <div class="row">
                    <div class="col-md-3"><label>Payment Plan</label></div>
                    <div class="col-md-9" id="verfi_payplan">
                        <?php
                            foreach ($fees as $fee) 
                            {
                                if($fee['fc_feestructure']==1 || $fee['fc_feestructure']==2)
                                {
                                    echo "<div class='row'><div class='col-md-4'>".$fee['fc_name']."</div><div class='col-md-8 paymentplandisplay' id='verifplan_".$fee['fc_id']."'></div></div>";
                                }
                            }
                        ?>   
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3"><label>Discounts</label></div>
                    <div class="col-md-9">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Discount</th>
                                    <th>Type</th>
                                    <th>Fee Category</th>
                                    <th>Value / %</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody id="verfi_discs">
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3"><label>Invoice Details</label></div>
                    <div class="col-md-9">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Description</th>
                                    <th>Net Total</th>
                                    <th>Discounts</th>
                                    <th>Add. Charges</th>
                                    <th>Grand Total</th>
                                </tr>
                            </thead>
                            <tbody id="verfi_invoice">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="event.preventDefault();confirm_regi('C');">Close</button>
                <button type="button" class="btn btn-primary" onclick="event.preventDefault();confirm_regi('R');">Register Student</button>
            </div>
    </div>
        </div>
</div>
<script type="text/javascript" src="<?php echo base_url('js/moment.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/bootstrap-datetimepicker.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/select2/select2.full.min.js'); ?>"></script>

<link rel="stylesheet" href="<?php echo base_url('assets/select2/select2.min.css') ?>">
<script src='<?php echo base_url("assets/datepicker/bootstrap-datepicker.js")?>'></script>
<link rel="stylesheet" href="<?php echo base_url('assets/datepicker/datepicker3.css')?>">

<script type="text/javascript">

function verify_data()
{
    $('#verfi_branch').empty();
    $('#verfi_year').empty();
    $('#verfi_accyear').empty();
    $('#verfi_term').empty();
    $('#verfi_intake').empty();
    $('#verfi_fname').empty();
    $('#verfi_oname').empty();
    $('#verfi_sibs').empty();
    $('#verfi_admtemp').empty();
    $('#verfi_trmtemp').empty();
    $('#verfi_admpaymeth').empty();
    $(".paymentplandisplay").empty();
    $('#verfi_discs').empty();
    $('#verfi_invoice').empty();

    $('#verfi_branch').append(': '+$("#st_branch option:selected").text());
    $('#verfi_year').append(': '+$("#st_grade option:selected").text());
    $('#verfi_accyear').append(': '+$("#acc_year option:selected").text());
    $('#verfi_term').append(': '+$("#st_term option:selected").text());
    $('#verfi_intake').append(': '+$("#st_intake option:selected").text());
    $('#verfi_fname').append(': '+$('#fname').val());
    $('#verfi_oname').append(': '+$('#oname').val());
    $('#verfi_sibs').append(': '+$("#siblinginfo").html());
    $('#verfi_admtemp').append(': '+$("#st_admintemplate option:selected").text());
    $('#verfi_trmtemp').append(': '+$("#st_termtemplate option:selected").text());

    $(".paymentplaninput").each(function() {
        temp = this.id.split('_');
        $('#verifplan_'+temp[1]).append(': '+$(this).find('option:selected').text());
    });

    admissionmethod = '-';
    $(".st_addpaymethod").each(function() {
        if(this.checked)
        {
            if(this.value == 1)
            {
                admissionmethod = 'Total Payment';
            }
            else
            {
                admissionmethod = 'Slabwise Payments';
            }
        }
    });

    $('#verfi_admpaymeth').append(': '+admissionmethod);

    discstr = '';
    $(".discinput").each(function() {

        if(this.value == 0)
        {
            prefix = '';
        }
        else
        {
            prefix = '_'+this.value;
        }

        studisc = $('#studisc'+prefix).val();
        adj_type = $('#adj_type'+prefix).val();
        adj_fee = $('#adj_fee'+prefix).val();
        adj_calamttype = $('#adj_calamttype'+prefix).val();
        amount = $('#amount'+prefix).val();

        if(amount != '' || amount >0)
        {
            discstr += '<tr><td>'+studisc+'</td><td>'+$("#adj_type"+prefix+" option:selected").text()+'</td><td>'+$("#adj_fee"+prefix+" option:selected").text()+'</td><td>'+$("#adj_calamttype"+prefix+" option:selected").text()+'</td><td>'+amount+'</td></tr>';
        }
    });
    $('#verfi_discs').append(discstr);
    
    branch  = $('#st_branch').val();
    grade   = $('#st_grade').val();
    astemp  = $('#st_admintemplate').val();
    tmtemp  = $('#st_termtemplate').val();
    term    = $('#st_term').val();

    $.post("<?php echo base_url('hci_studentreg/confirm_regi')?>",{'branch':branch,'grade':grade,'tmtemp':tmtemp,'astemp':astemp,'term':term},
        function(data)
        {
            if(data == 'denied')
            {
                funcres = {status:"denied", message:"You have no right to proceed the action"};
                result_notification(funcres);
            }
            else
            {
                invoicestr = '';
                invtotal = 0;
                if(data['fees'].length>0)
                {
                    for (i = 0; i<data['fees'].length; i++) 
                    {
                        paymentplan = $('#pplan_'+data['fees'][i]['fsf_fee']).val();

                        if(data['fees'][i]['fc_feestructure'] == 1 && admissionmethod == 'Slabwise Payments')
                        {
                            nettotal = Number(data['fees'][i]['fsf_slabnew']);
                        }
                        else
                        {
                            nettotal = Number(data['fees'][i]['fsf_amt']);
                        }

                        if(data['fees'][i]['fc_feestructure'] != 1)
                        {
                            if(data['fees'][i]['amtprd_option']==2)
                            {
                                termamt = nettotal;
                            }
                            else
                            {
                                termamt = nettotal/3;
                            }

                            if(data['termdata']['term_number'] == 1)
                            {
                                if(paymentplan=='TA')
                                {
                                    nettotal = termamt*3;
                                }
                                else if(paymentplan=='TO')
                                {
                                    nettotal = termamt*2;
                                }
                                else
                                {
                                    nettotal = termamt;
                                }
                            }
                            else if(data['termdata']['term_number']==2)
                            {
                                if(paymentplan=='TA')
                                {
                                    nettotal = termamt*2;
                                }
                                else
                                {
                                    nettotal = termamt;
                                }
                            }
                            else
                            {
                                nettotal = termamt;
                            }
                        }

                        discountscalc = 0;
                        additionscalc = 0;

                        $(".discinput").each(function() {
                            if(this.value == 0)
                            {
                                prefix = '';
                            }
                            else
                            {
                                prefix = '_'+this.value;
                            }

                            studisc = $('#studisc'+prefix).val();
                            adj_type = $('#adj_type'+prefix).val();
                            adj_fee = $('#adj_fee'+prefix).val();
                            adj_calamttype = $('#adj_calamttype'+prefix).val();
                            amount = $('#amount'+prefix).val();

                            if(adj_calamttype=='V')
                            {
                                adj_amt = amount;
                            }
                            else
                            {
                                adj_amt = (nettotal*Number(amount))/100;
                            }

                            if(nettotal>0 && (adj_fee == data['fees'][i]['fsf_fee'] || adj_fee == 0))
                            {
                                if(adj_type == 'A')
                                {
                                    additionscalc += Number(adj_amt);
                                }
                                else
                                {
                                    discountscalc += Number(adj_amt);
                                }
                            }
                        });

                        temptotal = (nettotal-discountscalc+additionscalc);

                        extradiscountscalc = 0;
                        extraadditionscalc = 0;

                        for (j = 0; j<data['discounts'].length; j++) 
                        {
                            if(data['discounts'][j]['adj_amttype']=='V')
                            {
                                commonadj_amt = data['discounts'][j]['adj_amount'];
                            }
                            else
                            {
                                commonadj_amt = (temptotal*Number(data['discounts'][j]['adj_amount']))/100;
                            }

                            if(temptotal>0)
                            {
                                if(data['discounts'][j]['adj_type'] == 'A')
                                {
                                    extraadditionscalc += Number(commonadj_amt);
                                }
                                else
                                {
                                    extradiscountscalc += Number(commonadj_amt);
                                }
                            }
                        }

                        if(nettotal>0)
                        {
                            invoicestr += '<tr>';
                            invoicestr += '<td>REGISTRATION - [ '+data['fees'][i]['fc_index']+' ] '+data['fees'][i]['fc_name']+'</td>';
                            invoicestr += '<td>'+Number(nettotal).toFixed(2)+'</td>';
                            invoicestr += '<td>'+Number(discountscalc+extradiscountscalc).toFixed(2)+'</td>';
                            invoicestr += '<td>'+Number(additionscalc+extraadditionscalc).toFixed(2)+'</td>';
                            invoicestr += '<td>'+Number(temptotal-extradiscountscalc+extraadditionscalc).toFixed(2)+'</td>';
                            invoicestr += '</tr>';

                            invtotal += (temptotal-extradiscountscalc+extraadditionscalc);
                        } 
                    }
                    invoicestr += '<tr>';
                    invoicestr += '<td colspan="4"><strong>Total Amount</strong></td>';
                    invoicestr += '<td><strong>'+Number(invtotal).toFixed(2)+'</strong></td>';
                    invoicestr += '</tr>';
                }
                else
                {
                    invoicestr += '<tr>';
                    invoicestr += '<td colspan="5"><strong>No invoice found</strong></td>';
                    invoicestr += '</tr>';
                }
                $('#verfi_invoice').append(invoicestr);
            }
        },  
        "json"
    );

    $('#confinputmodal').modal('show');
}

function confirm_regi(status) 
{
    $('#confinputmodal').modal('toggle');
    if(status == 'R')
    {
        $.confirm({
            title: 'Register Student',
            content: 'Are you sure you want to register the student? ',
            confirm: function(){
                $("#reg_form").append("<input type='hidden' name='btn_type' value='regi' />");
                $("#reg_form" ).submit();
            },
            cancel: function(){
                
            }
        });
    }     
}

var cloneid=0;

function cloning()
{ 
    cloneid+=1;
    var container = document.getElementById('clone_div');
    var clone = $('#clonedInput1').clone();

    clone.find('#iId').attr('id','iId_' + cloneid);
    clone.find('#iId_'+cloneid).val(cloneid);

    clone.find('#studisc').attr('name','studisc_' + cloneid);
    clone.find('#studisc').attr('id','studisc_' + cloneid);
    clone.find('#studisc_'+cloneid).val('');

    clone.find('#adj_type').val('');
    clone.find('#adj_type').attr('name','adj_type_' + cloneid);
    clone.find('#adj_type').attr('id','adj_type_' + cloneid);

    clone.find('#adj_fee').val('');
    clone.find('#adj_fee').attr('name','adj_fee_' + cloneid);
    clone.find('#adj_fee').attr('id','adj_fee_' + cloneid);

    clone.find('#adj_calamttype').val('');
    clone.find('#adj_calamttype').attr('name','adj_calamttype_' + cloneid);
    clone.find('#adj_calamttype').attr('id','adj_calamttype_' + cloneid);

    clone.find('#amount').val('');
    clone.find('#amount').attr('name','amount_' + cloneid);
    clone.find('#amount').attr('id','amount_' + cloneid);
   
    clone.find('.remove_entry').attr('id',cloneid);
    clone.find('.remove_entry').attr('onclick','$(this).parents(".clonedInput").remove()');
 
    $('.clone_div').append(clone);
    $("html").getNiceScroll().resize();
}

function load_branch_student(id)
{
    $('#sib_select').empty();
    $('#sib_select').append("<option value=''></option>");

    $.get("<?php echo base_url('hci_studentreg/load_branch_student')?>",{'id':id},
    function(data)
    { 
        if(data == 'denied')
        {
            funcres = {status:"denied", message:"You have no right to proceed the action"};
            result_notification(funcres);
        }
        else
        {
            for (i = 0; i<data.length; i++) 
            {
                $('#sib_select').append("<option value='"+data[i]['id']+"'>"+data[i]['text']+"</option>");
            }
        }
    },  
    "json"
    );
}

// $('#fs_date_div').datetimepicker({ defaultDate: "<?php echo date('Y-m-d');?>",  pickTime: false});
// $('#fe_date_div').datetimepicker({ defaultDate: "<?php echo date('Y-m-d');?>",  pickTime: false});
// $('#as_date_div').datetimepicker({ defaultDate: "<?php echo date('Y-m-d');?>",  pickTime: false});
// $('#ae_date_div').datetimepicker({ defaultDate: "<?php echo date('Y-m-d');?>",  pickTime: false});


$.validate({
    form : '#reg_form',
    modules : 'logic',
});

$(document).ready(function(){
    $( "#sibling" ).autocomplete({
        source:"<?php echo base_url("hci_studentreg/new_studentreg/search/");?>"
    });

    acc_years = JSON.parse('<?php echo json_encode($acc_years);?>');

    curraccyear = null;
    for (i = 0; i<acc_years.length; i++)
    {
        if(acc_years[i]['ac_iscurryear']==1)
        {
            curraccyear = acc_years[i]['es_ac_year_id'];
        }
    }
    load_academic_data(curraccyear);
    display_prefmethod_view(null,null);
    $('#adm_paymethod').hide();
    change_view('major');
    load_feestructures('<?php echo $selectedbr;?>');
    load_branch_student('<?php echo $selectedbr;?>');
});

</script>
<script>
    

$( "#sib_select" ).select2();

</script>
<script type="text/javascript">

$('.datepicker').datepicker({
    autoclose: true
});

event_count = 0;

function load_sibling_data(type)
{
    sibs = $('#sib_select').val();
    $('#siblinginfo').empty();

    $.post("<?php echo base_url('hci_studentreg/load_sibling_data')?>",{'sibs':sibs,'type':type},
    function(data)
    { 
        if(data == 'denied')
        {
            funcres = {status:"denied", message:"You have no right to proceed the action"};
            result_notification(funcres);
        }
        else
        {
            if(event_count==0)
            {
                if(data['siblings'].length>0)
                {
                    if(type=='sibs')
                    {
                        if(sibs==null)
                        {
                            event_count = 0;
                        }
                        else
                        {
                            $("#sib_select").val(data['siblings']).trigger("change");
                            event_count++;
                        }

                        $('#st_termtemplate').val(data['fsdata']['st_termtemplate']);
                        $('#st_admintemplate').val(data['fsdata']['st_admintemplate']);
                        $('#mname').val(data['fsdata']['mom_name']);
                        $('#maddress1').val(data['fsdata']['mom_addy']);
                        $('#maddress2').val(data['fsdata']['mom_add2']);
                        $('#maddress3').val(data['fsdata']['mom_addcity']);

                        if(data['fsdata']['mom_home']=='-')
                        {
                            $('#mhome').val('');
                        }
                        else
                        {
                            $('#mhome').val(data['fsdata']['mom_home']);
                        }

                        if(data['fsdata']['mom_mobile']=='-')
                        {
                            $('#mom_mobile').val('');
                        }
                        else
                        {
                            $('#mom_mobile').val(data['fsdata']['mom_mobile']);
                        }

                        if(data['fsdata']['mom_email']=='-')
                        {
                            $('#memail').val('');
                        }
                        else
                        {
                            $('#memail').val(data['fsdata']['mom_email']);
                        }

                        $('#fathername').val(data['fsdata']['dad_name']);
                        $('#faddress1').val(data['fsdata']['dad_addy']);
                        $('#faddress2').val(data['fsdata']['dad_add2']);
                        $('#faddress3').val(data['fsdata']['dad_addcity']);

                        if(data['fsdata']['dad_home']=='-')
                        {
                            $('#fhome').val('');
                        }
                        else
                        {
                            $('#fhome').val(data['fsdata']['dad_home']);
                        }

                        if(data['fsdata']['dad_mobile']=='-')
                        {
                            $('#dad_mobile').val('');
                        }
                        else
                        {
                            $('#dad_mobile').val(data['fsdata']['dad_mobile']);
                        }

                        if(data['fsdata']['dad_email']=='-')
                        {
                            $('#dad_email').val('');
                        }
                        else
                        {
                            $('#dad_email').val(data['fsdata']['dad_email']);
                        }

                        $('#guar_name').val(data['fsdata']['guar_name']);
                        $('#guar_addy').val(data['fsdata']['guar_addy']);
                        $('#guar_add2').val(data['fsdata']['guar_add2']);
                        $('#guar_addcity').val(data['fsdata']['guar_addcity']);

                        if(data['fsdata']['guar_home']=='-')
                        {
                            $('#guar_home').val('');
                        }
                        else
                        {
                            $('#guar_home').val(data['fsdata']['guar_home']);
                        }

                        if(data['fsdata']['guar_mobile']=='-')
                        {
                            $('#guar_mobile').val('');
                        }
                        else
                        {
                            $('#guar_mobile').val(data['fsdata']['guar_mobile']);
                        }

                        if(data['fsdata']['guar_email']=='-')
                        {
                            $('#guar_email').val('');
                        }
                        else
                        {
                            $('#guar_email').val(data['fsdata']['guar_email']);
                        }

                        $('#dad_aff').val(data['fsdata']['dad_aff']);
                        $('#dad_pos').val(data['fsdata']['dad_pos']);
                        $('#dad_biz_addy').val(data['fsdata']['dad_biz_addy']);

                        if(data['fsdata']['dad_biz_tel']=='-')
                        {
                            $('#dad_biz_tel').val('');
                        }
                        else
                        {
                            $('#dad_biz_tel').val(data['fsdata']['dad_biz_tel']);
                        }

                        $('#mom_aff').val(data['fsdata']['mom_aff']);
                        $('#mom_pos').val(data['fsdata']['mom_pos']);
                        $('#mom_biz_addy').val(data['fsdata']['mom_biz_addy']);

                        if(data['fsdata']['mom_biz_tel']=='-')
                        {
                            $('#mom_biz_tel').val('');
                        }
                        else
                        {
                            $('#mom_biz_tel').val(data['fsdata']['mom_biz_tel']);
                        }
                    }
                }

                if(data['sibsfsdata'].length>0)
                {
                    sibsfsdata = data['sibsfsdata'];

                    contentstr = '<div class="alert alert-info" style="position: static;width: 100%;border-radius: 5px;" role="alert">';
                    contentstr += '<table class="table table-bordered">';
                    contentstr += '<caption style="text-align:left;color:black"><h5>Sibling Details</h5></caption>';
                    contentstr += '<thead><th>Student</th><th>Admission Te.</th><th>Term Template</th></thead>';
                    contentstr += '<tbody>';

                    for (i = 0; i<sibsfsdata.length; i++) 
                    {
                        contentstr += '<tr>';
                        contentstr += '<td>'+sibsfsdata[i]['st_id']+'</td>';
                        contentstr += '<td>'+sibsfsdata[i]['admintemp']+'</td>';
                        contentstr += '<td>'+sibsfsdata[i]['termtemp']+'</td>';
                        contentstr += '</td>';
                    }

                    contentstr += '</tbody>';
                    contentstr += '</table>';
                    contentstr += '</div>';

                    $('#siblinginfo').append(contentstr);
                }
            }
        }
    },  
    "json"
    );

    $("html").getNiceScroll().resize();
}

function load_academic_data(id)
{
    $('#st_term').empty();
    $('#st_intake').empty();
    $('#st_term').append("<option value=''></option>");
    $('#st_intake').append("<option value=''></option>");
    if(id!="")
    {
        $.post("<?php echo base_url('hci_studentreg/load_academic_data')?>",{'id':id},
            function(data)
            {
                if(data == 'denied')
                {
                    funcres = {status:"denied", message:"You have no right to proceed the action"};
                    result_notification(funcres);
                }
                else
                {   
                    if(data['terms'].length>0)
                    {   
                        for (i = 0; i<data['terms'].length; i++) {
                            termsel = '';
                            if(data['terms'][i]['term_id']==data['currterm'])
                            {
                                termsel = 'selected';
                            }
                            $('#st_term').append("<option value='"+data['terms'][i]['term_id']+"' "+termsel+">Term - "+data['terms'][i]['term_number']+" [ "+data['terms'][i]['term_sdate']+" - "+data['terms'][i]['term_edate']+" ] </option>");
                        }
                    }

                    if(data['intakes'].length>0)
                    {   
                        for (i = 0; i<data['intakes'].length; i++) {
                            intsel = '';
                            if(data['intakes'][i]['int_id']==data['currintake'])
                            {
                                intsel = 'selected';
                            }
                            $('#st_intake').append("<option value='"+data['intakes'][i]['int_id']+"' "+intsel+">"+data['intakes'][i]['int_name']+" [ "+data['intakes'][i]['int_start']+" - "+data['intakes'][i]['int_end']+" ] </option>");
                        }
                    }
                }
            },  
            "json"
        );
    }
}

function stuff_mother(id)
{
    $.post("<?php echo base_url('hci_studentreg/get_staff_details')?>",{'id':id},
        function(data)
        {
            if(data == 'denied')
            {
                funcres = {status:"denied", message:"You have no right to proceed the action"};
                result_notification(funcres);
            }
            else
            {
                $('#mname').val(data['stf_firstname']+['  ']+data['stf_lastname']);
                $('#maddress1').val(data['stf_adress1']);
                $('#maddress2').val(data['stf_adress2']);
                $('#maddress3').val(data['stf_city']);
                $('#mhome').val(data['stf_contactnum']);
                $('#mom_mobile').val(data['stf_mobileno']);
                $('#memail').val(data['stf_email']);
            }
        },  
        "json"
    );
}

function staff_father(id)
{
    $.post("<?php echo base_url('hci_studentreg/get_staff_details')?>",{'id':id},
        function(data)
        {
            if(data == 'denied')
            {
                funcres = {status:"denied", message:"You have no right to proceed the action"};
                result_notification(funcres);
            }
            else
            {
                $('#fathername').val(data['stf_firstname']+['  ']+data['stf_lastname']);
                $('#faddress1').val(data['stf_adress1']);
                $('#faddress2').val(data['stf_adress2']);
                $('#faddress3').val(data['stf_city']);
                $('#fhome').val(data['stf_contactnum']);
                $('#dad_mobile').val(data['stf_mobileno']);
                $('#femail').val(data['stf_email']);
            }
        },  
        "json"
    );
}

function display_prefmethod_view(ft,grd)
{
    if(grd == null)
    {
        grd = $('#st_grade').val();
    }

    if(ft == null)
    {
        ft = $('#st_admintemplate').val();
    }

    if(ft != '' && grd != '')
    {
        $.post("<?php echo base_url('hci_studentreg/display_prefmethod_view')?>",{'ft':ft,'grd':grd},
            function(data)
            {
                if(data == 'denied')
                {
                    funcres = {status:"denied", message:"You have no right to proceed the action"};
                    result_notification(funcres);
                }
                else
                {
                    if(data == true)
                    {
                        $('#adm_paymethod').show();
                    }
                    else
                    {
                        $('#adm_paymethod').hide();
                    }
                }
            },  
            "json"
        );
    }
    else
    {
        $('#adm_paymethod').hide();
    }
}

function change_view(type)
{
    if(type==null)
    {
        currview = $('#formview').val();
    }
    else
    {
        currview = type;
    }

    if(currview == 'major')
    {
        $('#formview').val('minor');
        $('.minordetails').hide();
        $('a#fullviewlink').text('Show All');
    }
    else
    {
        $('#formview').val('major');
        $('.minordetails').show();
        $('a#fullviewlink').text('Compulsory');
    }

    $("html").getNiceScroll().resize();
}

function load_feestructures(branch)
{
    $('#st_admintemplate').empty();
    $('#st_admintemplate').append("<option value=''></option>");

    $('#st_termtemplate').empty();
    $('#st_termtemplate').append("<option value=''></option>");
    if(branch!="")
    {
        $.post("<?php echo base_url('hci_studentreg/load_feestructures')?>",{'id':branch},
            function(data)
            {
                if(data == 'denied')
                {
                    funcres = {status:"denied", message:"You have no right to proceed the action"};
                    result_notification(funcres);
                }
                else
                {   
                    if(data.length>0)
                    {   
                        for (i = 0; i<data.length; i++) {
                            if(data[i]['ft_feecat']==1)
                            {
                                seltxt = '';

                                if(data[i]['ft_iscurrent']==1)
                                {
                                    seltxt = 'Selected';
                                }

                                $('#st_admintemplate').append("<option value='"+data[i]['ft_id']+"' "+seltxt+">"+data[i]['ft_name']+"</option>");
                            }

                            if(data[i]['ft_feecat']==2)
                            {
                                seltxt = '';

                                if(data[i]['ft_iscurrent']==1)
                                {
                                    seltxt = 'Selected';
                                }

                                $('#st_termtemplate').append("<option value='"+data[i]['ft_id']+"' "+seltxt+">"+data[i]['ft_name']+"</option>");
                            }
                        }
                    }

                    display_prefmethod_view(null,null);
                }
            },  
            "json"
        );
    }
}
</script>