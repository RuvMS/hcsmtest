<?php
        $this->load->library('tcpdf/tcpdf');
        $pdf = new TCPDF('L','mm',array('210','148'),true,'UTF-8',false);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->SetTitle('Receipt');
        //$pdf->SetRightMargin(0);
        $pdf->SetFont('dejavusans', '', 10, '', true);
        foreach ($paylist as $pay) 
        {
			$tablerw = '';
            $x=0;
			$sum=0;
			
            foreach ($pay['invoice'] as $fee) 
            {
			
                $tablerw .= "<tr><td>".$fee['inv_index']." - ".$fee['inv_description']."</td><td style='text-align:right'><p>".number_format($fee['payinv_amount'],2)."</p></td></tr>";
                $x++;
				$sum += $fee['payinv_amount'];
            }
			$tablerw .= "<tr><td><span>Invoice Amount </span></td><td><span>".number_format($sum,2)."</span></td></tr>";
                

            $html ='<style> 
						span {text-align:right;font-weight:bold;font-size:9px; height:20px;}
						p{text-align:right;font-size:10px;}							
					</style>
			
					<div style="height:100px">
							<table><tr><th style="width:30%">logo<img src="" alt="logo" style="width:50px; height:50px;"/></th><th style="width:70%;font-size:12px;text-align:left;font-weight:bold;"> '.$pay['receipt']['rec_compname'].' </th></tr></table>
					</div><br/>
										<div></div>		
											<table>
												<tr>
													<th style="width:60%;font-size:8px;"><label style="font-weight:bold;">Name :</label>'.$pay['receipt']['rec_cusname'].'<br/><br/><label style="font-weight:bold;">Pay Method :</label></th>
													<th style="width:40%;text-align:right; font-size:8px;"> Receipt: #'.$pay['receipt']['rec_index'].'<br>Receipt Date :'.$pay['receipt']['rec_date'].'</th>
														
												</tr>
											</table><br>					
											<hr height="1px">
												<table style="line-height:30px;">
													<tr>
														<th>Description :   '.$pay['receipt']['rec_description'].'</th>													
													</tr>
												</table>
											<hr height="1px">																				
										<div></div>
										<table border="0.5" style="font-size:09px;">
											<thead>
												<tr>
													<th style=" height:18px; text-align:center; font-size:09px;">Description</th>
													<th style=" height:18px; text-align:center; font-size:09px;">Amount</th>
												</tr>
											</thead>
											<tbody>
												'.$tablerw.'
												<tr><td style="height:20px;"><br/><label style="font-weight:bold;font-size:9px; text-align:right;"><br/>Receipt Total </label> </td><td style="font-weight:bold;text-align:right;"><br/><label><br/>'.number_format($pay['receipt']['rec_amount'],2).'</label></td></tr>
											</tbody>
										</table>
										<br/><br/>
										<div style="font-size:8px;">Remarks :'.$pay['receipt']['rec_remarks'].'</div>
										
										<br/>
											<div style="font-size:8px;">Payment Received. Thank you.</div>
										<br/>
										<hr style="height:0px;"/>
									<table><tr><th style="font-size:6;">Created By :'.$pay['receipt']['rec_createdusername'].'</th><th style="text-align:right; font-size:6;">Created Date :'.$pay['receipt']['rec_createddate'].'</th></tr></table>
								<div style="page-break-before: always;"> '; 
								
								
								
            $pdf->AddPage();
            $pdf->writeHTML($html);
        }
        
        $pdf->Output('Receipt.pdf','I');
 ?>