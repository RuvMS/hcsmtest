<div class="row">
	<div class="col-md-12">
		<h3 class="page-header"><i class="fa fa-graduation-cap"></i>EDUCATIONAL SCHEME</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
			<li><i class="fa fa-cog"></i>Educational Structure</li>
			<li><i class="fa fa-graduation-cap"></i>Educational Scheme</li>
		</ol>
	</div>
</div>
<div>
	<br>
	<div class="row">
		<div class="col-md-4">
		  	<section class="panel">
			    <header class="panel-heading">
			        Manage Educational Scheme
			    </header>
		      	<div class="panel-body">	
		          	<form class="form-horizontal" role="form" method="post" action="<?php echo base_url('hci_edustructure/save_eduscheme')?>" id="schemeform" autocomplete="off" novalidate>
		              	<div class="form-group">
		              		<input type="hidden" id="schm_id" name="schm_id">
		                  	<label for="schm_section" class="col-md-4 control-label">Section</label>
		                  	<div class="col-md-6">
		                  		<select  class="form-control" id="schm_section" name="schm_section" placeholder="">
		                  			<?php
		                  				if(!empty($sections))
			          					{
			          						echo "<option value=''></option>";
			          						foreach ($sections as $section) 
				          					{
				          						if($section["sec_status"]=="A")
										    	{
				          							echo "<option value='".$section['sec_id']."'>".$section['sec_name']."</option>";
				          						}
				          					}
			          					}
		                  			?>
		                  		</select>
		                  	</div>
		              	</div>
		              	<div class="form-group">
		                  	<label for="schm_name" class="col-md-4 control-label">Scheme</label>
		                  	<div class="col-md-6">
		                      	<input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="schm_name" name="schm_name" placeholder="">
		                  	</div>
		              	</div>
		              	<div class="form-group">
		                  	<div class="col-md-offset-2 col-md-11">
		                      	<button type="submit" class="btn btn-info">Save</button> 
		                      	<button type="reset" class="btn btn-default">Reset</button>
		                  	</div>
		              	</div>
		          	</form>
		      	</div>
		  	</section>
	  	</div>
	  	<div class="col-md-8">
		  	<section class="panel">
			    <header class="panel-heading">
			       Look Up
			    </header>
		      	<div class="panel-body">	
		          	<table class="display" id="schm_tbl">
		          		<thead>
		          			<tr>
		          				<th>#</th>
		          				<th>Section</th>
		          				<th>Scheme</th>
		          				<th>Actions</th>
		          			</tr>
		          		</thead>
		          		<tbody>
	          				<?php
	          					$i=1;
	          					if(!empty($schemes))
	          					{
	          						foreach ($schemes as $schm) 
		          					{
		          						echo "<tr>";
		          						echo "<td>".$i."</td>";
		          						echo "<td>".$schm['sec_name']."</td>";
		          						echo "<td>".$schm['schm_name']."</td>";
		          						echo "<td><a class='btn btn-info btn-xs' onclick='event.preventDefault();edit_schm_load(".$schm['schm_id'].",".$schm['schm_section'].",\"".$schm['schm_name']."\")'><span class='glyphicon glyphicon-pencil' aria-hidden='true'></span></a>| ";

								    	if($schm["schm_status"]=="A")
								    	{
								    		echo "<button onclick='event.preventDefault();change_status(".$schm["schm_id"].",\"D\")' class='btn btn-warning btn-xs'><span class='glyphicon glyphicon-ban-circle' aria-hidden='true'></span></button>";
								    	}
								    	else
								    	{
								    		echo "<button onclick='event.preventDefault();change_status(".$schm["schm_id"].",\"A\")' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-ok-circle' aria-hidden='true'></span></button>";
								    	}
								    	
		          						echo "</td></tr>";

		          						$i++;
		          					}
	          					}
	          				?>
		          		</tbody>
		          	</table>
		      	</div>
		  	</section>
	  	</div>
	</div>
</div>
<script type="text/javascript">

$.validate({
   	form : '#schemeform'
});

$(document).ready(function() {
    $('#schm_tbl').DataTable();
} );

function edit_schm_load(id,sec,name)
{
	$('#schm_id').val(id);
	$('#schm_name').val(name);
	$('#schm_section').val(sec);
}

// function delete_group_load(id)
// {	
// 	$.post("<?php echo base_url('hci_edustructure/remove_dept')?>",{"grd_id":id},
// 	function(data)
// 	{	
// 		location.reload();
// 	},	
// 	"json"
// 	);
// }

function change_status(id,new_s)
{	
	$.post("<?php echo base_url('hci_edustructure/change_schemestatus')?>",{"schm_id":id,"new_s":new_s},
	function(data)
	{	
		if(data == 'denied')
		{
    		funcres = {status:"denied", message:"You have no right to proceed the action"};
    		result_notification(funcres);
		}
		else
		{
			location.reload();
		}
	},	
	"json"
	);
}

</script>
