<div class="row">
    <div class="col-md-12">
        <h3 class="page-header"><i class="fa fa-database "></i> MONTH-END PROCESS</h3>
        <ol class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
            <li><i class="fa fa-bar-chart-o"></i>Accounts</li>
            <li><i class="fa fa-calendar"></i>Month-end</li>
        </ol>
    </div>
</div>
<div>
  	<section class="panel">
	    <header class="panel-heading">
	        Run Month-end Process
	    </header>
      	<div class="panel-body">	
          	<form class="form-horizontal" role="form" method="post" action="<?php echo base_url('hci_accounts/run_monthend')?>" id="dept_form" autocomplete="off" novalidate>
              	<div class="form-group">
                    <div class="col-md-8">
                        <?php
                            $sdate = '';
                            $edate = '';
                            $disabled = '';
                            $butntext = '';
                            $linktext = '';
                            $linkhref = '';
                            $divstyle = '';

                            if(isset($monthenddates['ac_isyearendrun']))
                            {
                                $disabled = 'disabled';
                                $butntext = 'Current Academic Year is not defined';
                                $linktext = 'Set the Current Academic Year';
                                $linkhref = 'company?tab_id=ay_tab';
                            }
                            else if(isset($monthenddates['ispromoted']))
                            {
                                $disabled = 'disabled';
                                $butntext = 'Grade Promotion for this year is not performed';
                                $linktext = 'Perform Grade Promotion';
                                $linkhref = 'hci_student/annualgradepromotion_view';
                            }
                            else if(empty($monthenddates))
                            {
                                $disabled = 'disabled';
                                $butntext = 'No Month left to proceed';
                                $divstyle = 'style="display:none"';
                            }
                            else
                            {
                                $sdate = $monthenddates['start_date'];
                                $edate = $monthenddates['last_date'];
                                $divstyle = 'style="display:none"';

                                if($monthenddates['type'] == 'MONTH')
                                {
                                    $butntext = 'Run Month-End from '.$monthenddates['start_date'].' to '.$monthenddates['last_date'];
                                }
                                else
                                {
                                    $butntext = 'Run Year End for academic year '.$monthenddates['start_date'].' - '.$monthenddates['last_date'];
                                }

                                if(isset($monthenddates['createNextAccYear']) && ($monthenddates['createNextAccYear']))
                                {
                                    $divstyle = '';
                                    $linktext = 'Create next academic year';
                                    $linkhref = 'company?tab_id=ay_tab';
                                }
                            }
                        ?>
                        <input type="hidden" name="sdate" id="sdate" value="<?php echo $sdate;?>">
                        <input type="hidden" name="edate" id="edate" value="<?php echo $edate;?>">
                        <button type="submit" class="btn btn-danger col-md-6" <?php echo $disabled;?>><?php echo $butntext;?></button> 
                        <div <?php echo $divstyle;?>><a class="" href="<?php echo base_url($linkhref)?>"><?php echo $linktext;?></a></div>
                    </div>
                    <div class="col-md-4">
                        <?php
                            if((isset($monthenddates['previoustermsary'])) || (isset($monthenddates['currTermInv'])) || (isset($monthenddates['nextTermInv'])))
                            {
                        ?>
                                <strong>Generate Invoices For,</strong><br>
                        <?php
                            }

                            if((isset($monthenddates['previoustermsary'])) && (!empty($monthenddates['previoustermsary'])))
                            {
                                foreach ($monthenddates['previoustermsary'] as $prev) 
                                {
                        ?>
                                    <div class="checkbox">
                                        <label><input type="checkbox" name="PrevTermInv[]" value="<?php echo $prev['term_id'];?>" checked>Term - <?php echo $prev['term_number'];?> [ <?php echo $prev['term_sdate'].'-'.$prev['term_edate'];?> ]</label>
                                    </div><br>
                        <?php
                                }
                            }
                        ?>
                        <?php
                            if((isset($monthenddates['currTermInv'])) && (!empty($monthenddates['currTermInv'])))
                            {
                        ?>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="currTermInv" value="<?php echo $monthenddates['currTermInv']['term_id'];?>">Current Term [ <?php echo $monthenddates['currTermInv']['term_sdate'].'-'.$monthenddates['currTermInv']['term_edate'];?> ]</label>
                                </div><br>
                        <?php
                            }
                            if((isset($monthenddates['nextTermInv'])) && ($monthenddates['nextTermInv'] != null))
                            {
                        ?>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="nextTermInv" value="<?php echo $monthenddates['nextTermInv']['term_id'];?>">Next Term [ <?php echo $monthenddates['nextTermInv']['term_sdate'].'-'.$monthenddates['nextTermInv']['term_edate'];?> ]</label>
                                </div>
                        <?php 
                            }
                        ?>
                    </div>
              	</div>
          	</form>
      	</div>
  	</section>
</div>