<div class="row">
	<div class="col-md-12">
		<h3 class="page-header"><i class="fa fa-bank"></i> FEE CATEGORY</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
			<li><i class="fa fa-cog"></i>Settings</li>
			<li><i class="fa fa-bank"></i>Fee Category</li>
		</ol>
	</div>
</div>
<div>
	<div class="row">
		<div class="col-md-6">
		  	<section class="panel">
			    <header class="panel-heading">
			        Manage Fee Category
			    </header>
		      	<div class="panel-body">	
		          	<form class="form-horizontal" role="form" method="post" action="<?php echo base_url('hci_fee_structure/save_fee')?>" id="fee_form" autocomplete="off" novalidate>
		              	<div class="form-group">
		              		<input type="hidden" id="fee_id" name="fee_id">
		                  	<label for="feecat" class="col-md-2 control-label">Fee Category</label>
		                  	<div class="col-md-10">
		                      	<input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="feecat" name="feecat" placeholder="">
		                  </div>
		              	</div>
		              	<div class="form-group">
		                  	<label for="invcode" class="col-md-2 control-label">Invoice Code</label>
		                  	<div class="col-md-6">
		                      	<input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="invcode" name="invcode" placeholder="">
		                  </div>
		              	</div>
		              	<div class="form-group">
		                    <label for="feestructure" class="col-md-2 control-label">Template Type</label>
		                    <div class="col-md-6">
		                        <select type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="feestructure" name="feestructure">
		                            <option value=''></option>
		                            <option value='1'>Admission</option>
		                            <option value='2'>Term</option>
		                            <option value='3'>transport</option>
		                            <option value='4'>childcare</option>
		                            <option value='5'>No template</option>
		                        </select>
		                    </div>
		                </div>
		              	<div class="form-group">
		                  	<label for="glcode" class="col-md-2 control-label">GL Code</label>
		                  	<div class="col-md-6">
		                      	<input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="glcode" name="glcode" placeholder="">
		                  </div>
		              	</div>
		              	<div class="form-group">
		                  	<label for="grldescription" class="col-md-2 control-label">GL Descrip.</label>
		                  	<div class="col-md-10">
		                      	<input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="grldescription" name="grldescription" placeholder="">
		                  </div>
		              	</div>
		              	<br>
		              	<div class="form-group">
		                  	<div class="col-md-offset-2 col-md-11">
		                  		<br>
		                      	<button type="submit" class="btn btn-info">Save</button> 
		                      	<button type="reset" class="btn btn-default" onclick="$('#feestructure option:not(:selected)').prop('disabled', false);$.validate({form : '#fee_form'});">Reset</button>
		                  	</div>
		              	</div>
		          	</form>
		      	</div>
		  	</section>
	  	</div>
	  	<div class="col-md-6">
		  	<section class="panel">
			    <header class="panel-heading">
			       Look Up
			    </header>
		      	<div class="panel-body">	
		          	<table class="table">
		          		<thead>
		          			<tr>
		          				<th>Fee Category</th>
		          				<th>GL Info</th>
		          				<th>Actions</th>
		          			</tr>
		          		</thead>
		          		<tbody id="fee_table_body">
		          			<?php
	          					foreach ($fees as $fee)
	          					{
	          						$linktxt = "<a class='btn btn-info btn-xs' onclick='event.preventDefault();edit_fee_load(".$fee['fc_id'].",\"".$fee['fc_name']."\",\"".$fee['fc_invcode']."\",\"".$fee['fc_feestructure']."\",\"".$fee['fc_glcode']."\",\"".$fee['fc_gldescription']."\")'>Edit</a>";

	          						if($fee['fc_feestructure']==1 || $fee['fc_feestructure']==2 || $fee['fc_feestructure']==3 || $fee['fc_feestructure']==4)
	          						{
	          							//$linktxt = '';
	          						}
	          						else
	          						{
	          							$linktxt .= " <a class='btn btn-danger btn-xs' onclick='event.preventDefault();delete_feecat(".$fee['fc_id'].",\"".$fee['fc_name']."\")'>Delete</a>";
	          						}
	          						
	          						echo "<tr>";
	          						echo "<td>".$fee['fc_invcode']." - ".$fee['fc_name']."</td>";
	          						echo "<td>[ ".$fee['fc_glcode']." ] - ".$fee['fc_gldescription']."</td>";
	          						echo "<td>".$linktxt."</td>";
	          						echo "</tr>";
	          					}
	          				?>
		          		</tbody>
		          	</table>
		      	</div>
		  	</section>
	  	</div>
	</div>
</div>
<script type="text/javascript">
$.validate({
   	form : '#fee_form'
});

$( document ).ready(function() {
});

function edit_fee_load(id,name,invcd,fstype,glcode,gldesc)
{
	$('#feestructure option:not(:selected)').prop('disabled', false);

	$('#fee_id').val(id);
	$('#feecat').val(name);
	$('#invcode').val(invcd);
	$('#feestructure').val(fstype);
	$('#glcode').val(glcode);
	$('#grldescription').val(gldesc);

	if(fstype==1 || fstype==2 || fstype==3 || fstype==4)
	{
		$('#feecat').prop('readonly',true);
		$('#invcode').prop('readonly',true);
		$('#feestructure option:not(:selected)').prop('disabled', true);
	}
	else
	{
		$('#feecat').prop('readonly',false);
		$('#invcode').prop('readonly',false);
	}
}

function delete_feecat(id,name)
{	
	$.post("<?php echo base_url('hci_fee_structure/delete_feecat')?>",{"fc_id":id},
	function(data)
	{	
		if(data == 'denied')
		{
    		funcres = {status:"denied", message:"You have no right to proceed the action"};
    		result_notification(funcres);
		}
		else
		{
			location.reload();
		}
	},	
	"json"
	);
}

</script>