<?php
        $this->load->library('tcpdf/tcpdf');
        $pdf = new TCPDF('P','mm',array('210','297'),true,'UTF-8',false);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->SetTitle('Debit Note');
        $pdf->SetRightMargin(0);
        $pdf->SetFont('dejavusans', '', 10, '', true);
        foreach ($dnotelist as $dnote) 
        {

            $html = 'Student : '.$dnote['dnote']['dnote_cusindex'].' - '.$dnote['dnote']['dnote_cusname'].
                    '<br>Amount : LKR '.number_format($dnote['dnote']['dnote_totamt'],2).
                    '<br>Credit Note for Invoice : '.$dnote['invoice']['inv_index']; 

            $pdf->AddPage();
            $pdf->writeHTML($html);
        }
        
        $pdf->Output('DebitNote.pdf','I');
 ?>