<div class="row">
    <div class="col-md-12">
        <h3 class="page-header"><i class="fa fa-file-archive-o"></i> TRANSPORT FEE STRUCTURE</h3>
        <ol class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
            <li><i class="fa fa-car"></i>Transport</li>
            <li><i class="fa fa-file-archive-o"></i>Fee Structure</li>
        </ol>
    </div>
</div>
<div>
<section class="panel">
    <header class="panel-heading">
        <div class="col-md-4">
            <div class="col-md-8">
                <h4>FEE STRUCTURE NO : </h4>
            </div>
            <div class="col-md-4" id="tfsid">
                <h4>#<?php echo $fsindex;?></h4>
            </div>
        </div>
    </header>
    <form class="form-horizontal" role="form" method="post" action="<?php echo base_url('hci_transport/save_feestructure')?>" id="fs_form" autocomplete="off" novalidate>
    <div class="panel-body">  
        <div class="row">
            <div class="col-md-6">  
                <br>
                <div class="form-group">
                    <label class="col-md-4 control-label">Branch</label>
                    <div class="col-md-8">
                        <?php 
                            global $branchdrop;
                            global $selectedbr;
                            $extraattrs = 'id="fs_branch" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty"';
                            echo form_dropdown('fs_branch',$branchdrop,$selectedbr, $extraattrs); 
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="description" class="col-md-4 control-label">Fee Structure</label>
                    <div class="col-md-8">
                        <input type="hidden" name="fs_id" id="fs_id">
                        <input type="text" data-validation="required" data-validation-error-msg-required="Field can not be empty" class="form-control" id="description" name="description" placeholder="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="is_curr" class="col-md-4 control-label">Current Fee-Structure</label>
                    <div style="padding-top: 10px;" class="col-md-1">
                        <input type="checkbox" id="is_curr" name="is_curr" >
                    </div>
                </div> 
                <table class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Beyond 2km</th>
                            <th>Within 2km</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>School Month</td>
                            <td>
                                <input type="text" data-validation="required number" data-validation-allowing="float" data-validation-error-msg-required="Field can not be empty" data-validation-decimal-separator="." class="form-control" id="feeamt_2_0" name="feeamt_2_0" placeholder="" style="text-align : right;padding-right:25px">
                            </td>
                            <td>
                                <input type="text" data-validation="required number" data-validation-allowing="float" data-validation-error-msg-required="Field can not be empty" data-validation-decimal-separator="." class="form-control" id="feeamt_1_0" name="feeamt_1_0" placeholder="" style="text-align : right;padding-right:25px">
                            </td>
                        </tr>
                        <tr>
                            <td>Holiday Month</td>
                            <td>
                                <input type="text" data-validation="required number" data-validation-allowing="float" data-validation-error-msg-required="Field can not be empty" data-validation-decimal-separator="." class="form-control" id="feeamt_2_1" name="feeamt_2_1" placeholder="" style="text-align : right;padding-right:25px">
                            </td>
                            <td>
                                <input type="text" data-validation="required number" data-validation-allowing="float" data-validation-error-msg-required="Field can not be empty" data-validation-decimal-separator="." class="form-control" id="feeamt_1_1" name="feeamt_1_1" placeholder="" style="text-align : right;padding-right:25px">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-5">
                <table id="fstable" class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Fee Structure</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($feestrucs as $struct) 
                            {
                                echo "<tr>";
                                echo "<td>".$struct['fs_index']."</td>"; 
                                echo "<td>".$struct['fs_name']."</td>";     
                                echo "<td><a class='btn btn-info btn-xs' onclick='event.preventDefault();edit_feestructure(".$struct['fs_id'].")'>Edit</a></td>";
                                echo "</tr>";
                            }
                        ?>
                    </tbody>
                </table>
            </div>
            
        </div>
    </div>
    <div class="panel-footer">
        <button type="submit" class="btn btn-info">Save</button> 
        <button type="submit" class="btn btn-default">Reset</button>
    </div>
    </form>
</section>
</div>
<script type="text/javascript">
$.validate({
    form : '#fs_form'
});

var table = $('#fstable').DataTable( {
        "pageLength": 5,
        "dom" : '<"row"<"col-md-6"l><"col-md-6"f>>rt<"row"<"col-md-4"i><"col-md-8"p>><"clear">',
    } );

function edit_feestructure(id)
{
    $.post("<?php echo base_url('hci_transport/load_transfeestructures')?>",{'id':id},
    function(data)
    { 
        if(data == 'denied')
        {
            funcres = {status:"denied", message:"You have no right to proceed the action"};
            result_notification(funcres);
        }
        else
        {
            $('#fs_id').val(data['fs_id']);
            $('#description').val(data['fs_name']);
            $('#fs_branch').val(data['fs_branch']);

            $('#tfsid').empty();
            $('#tfsid').append('<h4>#'+data['fs_index']+'</h4>');

            if(data['fs_iscurrent']==1)
            {
                $('#is_curr').prop('checked', true);
            }
            else
            {
                $('#is_curr').prop('checked', false);
            }

            if(data['fees'].length>0)
            {
                for (i = 0; i<data['fees'].length; i++) {

                    $('#feeamt_'+data['fees'][i]['fsf_distance']+'_'+data['fees'][i]['fsf_isholiday']).val(data['fees'][i]['fsf_amount']);
                }                         
            }
        }
    },  
    "json"
    );
}
</script>