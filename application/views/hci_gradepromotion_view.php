<div class="row">
    <div class="col-md-12">
        <h3 class="page-header"><i class="fa fa-users"></i> Annual Grade Promotion</h3>
        <ol class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
            <li><i class="fa fa-cog"></i>Student</li>
            <li><i class="fa fa-bank"></i>Grade Promotion</li>
        </ol>
    </div>
</div>
<div>
    <div class="row">
        <div class="col-md-12">
            <section class="panel">
                <form class="form-horizontal" role="form" method="post"  id="promo_form" action="<?php echo base_url('hci_student/promote_students')?>"  autocomplete="off" novalidate>
                <header class="panel-heading">
                    Student Lookup
                </header>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-5 form-group">
                            <label class="col-md-3 control-label" style="/*font-size: 12px;padding-top: 0px*/">Promote From</label>
                            <div class="col-md-9">
                                <select type="text" class="form-control" id="curr_ayear" name="curr_ayear" style="width: 100%;" data-validation="required" data-validation-error-msg-required="Field can not be empty">
                                    <?php
                                    foreach($ay_info as $ay):
                                        $selected = '';
                                        if($ay['ac_iscurryear']==1)
                                        {
                                            $selected = 'selected';
                                        }
                                    ?>
                                        <option value="<?php echo $ay['es_ac_year_id'];?>" <?php echo $selected;?>>
                                        <?php echo $ay['ac_startdate']." - ".$ay['ac_enddate'];?>
                                        </option>
                                    <?php
                                    endforeach;
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-5 form-group">
                            <label class="col-md-3 control-label" style="/*font-size: 12px;padding-top: 0px*/">Promote To</label>
                            <div class="col-md-9">
                                <select type="text" class="form-control" id="ayear" name="ayear" style="width: 100%;" data-validation="required" data-validation-error-msg-required="Field can not be empty">
                                    <?php
                                    foreach($ay_info as $ay):
                                        $selected = '';
                                    ?>
                                        <option value="<?php echo $ay['es_ac_year_id'];?>" <?php echo $selected;?>>
                                        <?php echo $ay['ac_startdate']." - ".$ay['ac_enddate'];?>
                                        </option>
                                    <?php
                                    endforeach;
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-5 form-group">
                            <label class="col-md-3 control-label" style="/*font-size: 12px;padding-top: 0px*/">Grade</label>
                            <div class="col-md-9">
                                <select class="form-control" id="grade" name="grade" style="width: 100%;">
                                    <option value="">Select Grade</option>
                                    <?php
                                    foreach($grd_info as $row):
                                        ?>
                                        <option value="<?php echo $row['grd_id'];?>">
                                            <?php echo $row['grd_name'];?>
                                        </option>
                                        <?php
                                    endforeach;
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-5 form-group">
                            <label class="col-md-3 control-label" style="/*font-size: 12px;padding-top: 0px*/">Branch</label>
                            <div class="col-md-9">
                                <?php 
                                    global $branchdrop;
                                    global $selectedbr;

                                    $branchdrop[""] = "Select Branch";
                                    $extraattrs = 'id="promobranch" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" style="width: 100%;" ';
                                    echo form_dropdown('promobranch',$branchdrop,$selectedbr, $extraattrs); 
                                ?>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <a class='btn btn-info btn-sm' onclick='event.preventDefault();load_student()'>Search</a>
                        </div>
                    </div>
                    <hr>
                    <table id="stuTable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Current Grade</th>
                                <th>Grade Promote To</th>
                            </tr>
                        </thead>
                        <tbody id="stubody">
                            
                        </tbody>
                    </table>
                </div>
                <footer>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" name="save_btn" id="save_btn" class="btn btn-info">Promote</button>
                        </div>
                    </div>
                </footer>
                </form>
            </section>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url('js/moment.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/bootstrap-datetimepicker.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/select2/select2.full.min.js'); ?>"></script>

<link rel="stylesheet" href="<?php echo base_url('assets/select2/select2.min.css') ?>">
<script src='<?=base_url("assets/datepicker/bootstrap-datepicker.js")?>'></script>
<link rel="stylesheet" href="<?=base_url('assets/datepicker/datepicker3.css')?>">
<script type="text/javascript">

// $(document).ready(function() 
// {
//     load_student(null);
// } );

$.validate({
    form : '#promo_form',
    modules : 'logic',
});

function load_student()
{
    ayear       = $('#ayear').val();
    brnch       = $('#promobranch').val(); 
    grade       = $('#grade').val(); 
    curr_ayear  = $('#curr_ayear').val(); 

    $.post("<?php echo base_url('hci_student/load_stubygrade')?>",{'ayear':ayear,'grd':grade,'brnch':brnch,'curr_ayear':curr_ayear},
    function(data)
    { 
        $('#stubody').empty();
        if(data == 'denied')
        {
            funcres = {status:"denied", message:"You have no right to proceed the action"};
            result_notification(funcres);
        }
        else
        {   
            if(data['stu_list'].length>0)
            {
                //alert(data.length);
                for (i = 0; i<data['stu_list'].length; i++) 
                {
                    gradeselect = '<select name="stugrd_'+data['stu_list'][i]['id']+'" id="stugrd_'+data['stu_list'][i]['id']+'" class="form-control">'
                    gradeselect += '<option></option>';

                    for (x = 0; x<data['grades'].length; x++) 
                    {
                        selectedtxt = '';

                        if(data['grades'][x]['grd_id'] == data['stu_list'][i]['next_id'])
                        {
                            selectedtxt = 'selected';
                        }

                        gradeselect += '<option value="'+data['grades'][x]['grd_id']+'" '+selectedtxt+'>'+data['grades'][x]['grd_name']+'</option>';
                    } 

                    if(data['stu_list'][i]['next_id'] == 0)
                    {
                        gradeselect += '<option value="0" selected>Ends School Career</option>';
                    }
                    
                    gradeselect += '</select>';

                    $('#stubody').append('<tr><td>'+data['stu_list'][i]['st_id']+'<input type="hidden" name="promostu[]" value="'+data['stu_list'][i]['id']+'"></td><td>'+data['stu_list'][i]['family_name']+' '+data['stu_list'][i]['other_names']+'</td><td>'+data['stu_list'][i]['curr']+'<input type="hidden" name="stucurggrd_'+data['stu_list'][i]['id']+'" value="'+data['stu_list'][i]['gp_currgrade']+'"></td><td>'+gradeselect+'<tr>');
                }                         
            }
        }
    },  
    "json"
    );
}
</script>