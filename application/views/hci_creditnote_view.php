<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/jquery-confirm.css'); ?>"> 
<script type="text/javascript" src="<?php echo base_url('js/jquery-confirm.js')?>"></script><!--jquery-->
<div class="row">
    <div class="col-md-12">
        <h3 class="page-header"><i class="fa fa-level-down"></i><i class="fa fa-file-text"></i> CREDIT NOTE</h3>
        <ol class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
            <li><i class="fa fa-bar-chart-o"></i>Accounts</li>
            <li><i class="fa fa-level-down"></i><i class="fa fa-file-text"></i>Credit Note</li>
        </ol>
    </div>
</div>
<div>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a id="lookup_tab" href="#lookuppanel" aria-controls="lookuppanel" role="tab" data-toggle="tab">Lookup</a></li>
        <li role="presentation"><a id="create_tab" href="#createpanel" aria-controls="createpanel" role="tab" data-toggle="tab">Create Credit Note</a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="lookuppanel">
            <div class="panel">
                <header class="panel-heading">
                    Lookup
                </header>
                <div class="panel-body">
                	<table id="cnote_table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Invoice</th>
                                <th>Cus.Type</th>
                                <th>Customer</th>
                                <th>Description</th>
                                <th>Date</th>
                                <th>Amount</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="createpanel">
            <div class="panel">
                <form class="form-horizontal" role="form" method="post" action="<?php echo base_url('hci_accounts/save_creditnote')?>" id="cnote_form" autocomplete="off" novalidate>
			    <div class="panel-heading">
                    <div class="col-md-12">
    					<div class="col-md-2">
		                    <h4>CREDIT NOTE ID : </h4>
		                </div>
		                <div class="col-md-2" id="creditnote_id">
		                	<h4><?php if(!empty($_POST['u_branch'])){echo $cnoteindex[$_POST['u_branch']];}?></h4>
		                </div>
		                <div class="col-md-4"></div>
		                <div class="col-md-4">
		                	<div class="form-group">
	    						<label for="cnote_branch" class="col-md-3 control-label" style="font-size: 12px;padding-top: 0px">Branch</label>
			                  	<div class="col-md-8">
			                  		<?php 
			                  			global $branchdrop;
			                  			global $selectedbr;
			                  			$extraattrs = 'id="cnote_branch" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" onchange="loadindex(this.value)"';
			                  			echo form_dropdown('cnote_branch',$branchdrop,$selectedbr, $extraattrs); 
			                  		?>
			                  </div>
			              	</div>
		                </div>
    				</div>
			    </div>
			    <div class="panel-body">
			    	<div class="row">
			    		<div class="col-md-12">
			    			<input type="hidden" name="pay_id" id="pay_id">
			    			<div class="row">
			    				<div class="col-md-5">
			    					<div class="form-group">
			    						<label for="cnote_stutype" class="col-md-3 control-label">Customer Type</label>
						                <div class="col-md-6">
						                    <select class="form-control select2" id="cnote_custype"  data-validation="required" data-validation-error-msg-required="Select customer type" name="cnote_custype" style="width: 100%;" onchange="load_customers(this.value)">
						                        <option value=""></option>
						                        <option value="STUDENT">Registered Student</option>
						                        <option value="TEMPSTU">Non-Registered Student</option>
						                        <option value="STAFF">Staff</option>
						                        <option value="EXTERNAL">External Customer</option>
						                    </select>
						                </div>
			    					</div>
			    				</div>
			    				<div class="col-md-7">
					                <div class="form-group">
					                	<label for="pay_student" class="col-md-2 control-label">Customer</label>
						                <div class="col-md-9">
						                    <select class="form-control select2" id="cnote_customer"  data-validation="required" data-validation-error-msg-required="Customer can not be empty" name="cnote_customer" style="width: 100%;" onchange="load_outstanding(this.value)">
						                        <option value=""></option>
						                    </select>
						                </div>
					                </div>
					            </div>
			    			</div>
			    			<br>
			    			<div class="row">
			    				<div class="col-md-5">
			    					<div class="form-group">
				    					<label for="cnote_outstands" class="col-md-3 control-label">Outstanding Bal.</label>
						                <div class="col-md-6">
						                    <input type="text" class="form-control" id="cnote_outstands" name="cnote_outstands" style="width: 100%;text-align : right;padding-right:25px" readonly>
						                </div>
				    				</div>
				    				<div class="form-group">
						                <label for="cnote_cohbalance" class="col-md-3 control-label">OnHandBalance</label>
						                <div class="col-md-6">
						                    <input type="text" class="form-control" id="cnote_cohbalance" name="cnote_cohbalance" style="width: 100%;text-align : right;padding-right:25px" readonly>
						                </div>
						            </div>
				    				<div class="form-group">
				    					<label for="cnote_totamt" class="col-md-3 control-label">Amount</label>
						                <div class="col-md-6">
						                    <input type="text" data-validation="required number"  data-validation-allowing="float" data-validation-decimal-separator="." data-validation-error-msg-required="Amount can not be empty" class="form-control" id="cnote_totamt" name="cnote_totamt" style="width: 100%;text-align : right;padding-right:25px">
						                </div>
				    				</div>
				    				<div class="form-group">
				    					<label for="cnote_remarks" class="col-md-3 control-label">Remarks</label>
						                <div class="col-md-9">
						                    <textarea class="form-control" id="cnote_remarks" name="cnote_remarks" rows="3"></textarea>
						                </div>
				    				</div>
			    				</div>
			    				<div class="col-md-7">
			    					<div id="email-error-dialog"></div>
			    					<table class="table table-bordered">
						            	<thead>
						            		<tr>
						            			<th>#</th>
						            			<th>Invoice</th>
						            			<th>NetValue</th>
						            			<th>PaidAmount</th>
						            			<th>TotalCreditN.</th>
						            			<th>TotalDebitN.</th>
						            			<th>Balance</th>
						            		</tr>
						            	</thead>
						            	<tbody id="outs_table">
						            		<tr>
						            			<td colspan="7">No outstanding found</td>
						            		</tr>
						            	</tbody>
						            </table>
			    				</div>
			    			</div>
			    		</div>
			    	</div>
			    </div>
			    <div class="panel-footer">
			    	<div class="form-group">
			          	<div class="col-md-11">
			              	<button type="submit" name="save_btn" id="save_btn" class="btn btn-info" onclick="event.preventDefault();save_cnote()">Save</button> 
			                <button type="submit" onclick="event.preventDefault();confirm_process()" name="proc_btn" id="proc_btn" class="btn btn-info">Save & Process</button>
			              	<!-- <button type="submit" class="btn btn-default">Reset</button> -->
			          	</div>
			      	</div>
			    </div>
			    </form>
            </div>
        </div>
    </div>
</div>


<!--started modal for cnote receipt-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">		
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><span id="cnote_idx"></span></h4>
			</div>			
			<div class="modal-body">				
				<div class="col-md-12">
					<section class="panel">
						<div class="panel-body">							
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<div class="col-md-12">
											<div style="background:#003d99; height:16px;"></div>
										</div>
									</div>
									
								</div><br/>
																	
									<div class="col-md-12 ">
										<div class="row">											
											 
												<div class="col-md-12">
													<img src="########" align="right"/>
												</div>
											
										</div><br/>
										<div class="row">
											<div class="col-md-6">
												<span width="100px" id="cnote_cus"></span><br/>
												
											</div>
											<div class="col-md-6" >											
													<label style="width:100px;font-weight:bold;">Credit Note ID:</label><span  style="text-align:right;" id="cnote_idx2"> </span>
													<br />
													<label style="width:100px; font-weight:bold;">Credit Note Date:</label><span  style="text-align:right;" id="cnote_date"></span>
													<br />										
											</div>
										</div>
									</div>										
									<div class="col-md-12">
											<hr style="background-color:#a1a1a1; height:3px;">											
											<div class="col-md-12">													
												<label style="font-size:18px;font-weight:bold;"><span id="cnote_des"></span></label>												
											</div>																										
											<hr style="background-color:#a1a1a1; height:3px;">													
									</div>					

								<div class="col-md-12"><label style="font-weight:bold;">Remarks :<span id="cnote_remarks"></span><br/><span id="remark"></span></label></div>								
								<br/>
								
								<div class="col-md-12"><div class="col-md-12" style="background:#003d99;height:16px;" ></div></div>
								<div class="col-md-12"> <div class="col-md-6" style="font-size:10px;"><label>Created By :<span id="user"/></label></div> <div class="col-md-6" style="text-align:right;font-size:10px;"><label>Created Date :<span  id="created_date"/></label></div> </div>
							</div>
					</div>
				</section>				
			</div>			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>										  
			</div>
		</div>								
	</div>
</div>
</div>
<!--end modal for cnote receipt-->






<script type="text/javascript" src="<?php echo base_url('js/moment.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/bootstrap-datetimepicker.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/select2/select2.full.min.js'); ?>"></script>

<link rel="stylesheet" href="<?php echo base_url('assets/select2/select2.min.css') ?>">
<script src='<?php echo base_url("assets/datepicker/bootstrap-datepicker.js")?>'></script>
<link rel="stylesheet" href="<?php echo base_url('assets/datepicker/datepicker3.css')?>">
<script type="text/javascript">

function loadindex(id)
{
	indexary = jQuery.parseJSON('<?php echo json_encode($cnoteindex)?>');
	$('#creditnote_id').empty();
	$('#creditnote_id').append(indexary[id]);
}

$(document).ready(function() {
    load_cnotelist();
    $('#cnote_customer').prop('disabled', true);
});

$.validate({
	modules : 'logic',
   	form : '#cnote_form',
   	// validateOnBlur : false, // disable validation when input looses focus
    // errorMessagePosition : 'bottom' // Instead of 'inline' which is default
});

$('.datepicker').datepicker({
    autoclose: true
});

$("#cnote_customer").select2();

function save_cnote()
{
	inv_no = $('input[name=invoice]:checked').val();
	inv_bal = $('#invbal_'+inv_no).val();
	amount = $('#cnote_totamt').val();

	if(amount<=0)
	{
		$('#js_notif_alerts').append('<div id="notif_alerts" class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Amount Can not be Zero or (-)!</div></div>');
	}
	// else if(inv_bal<amount)
	// {
	// 	$('#js_notif_alerts').append('<div id="notif_alerts" class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Amount Exceeds the Invoice AMount</div></div>');
	// }
	else
	{
		$("#cnote_form" ).submit();
	} 
}

function load_outstanding(cus)
{
	type = $('#cnote_custype').val();
	$('#outs_table').empty();
	$.post("<?php echo base_url('hci_payment/load_outstanding')?>",{'cus':cus,'type':type},
	function(data)
	{
		if(data == 'denied')
		{
    		funcres = {status:"denied", message:"You have no right to proceed the action"};
    		result_notification(funcres);
		}
		else
		{
			$('#cnote_outstands').val(data['outs']['outs_amount']);
			$('#cnote_cohbalance').val(data['outs']['outs_cohbalance']);

			if(data['invoices'].length>0)
			{
				for (i = 0; i<data['invoices'].length; i++) 
				{
					// data-validation='required' data-validation-error-msg-required='Invoice must be selected' data-validation-error-msg-container='#email-error-dialog'
			   		$('#outs_table').append("<tr><td><input type='radio' id='invoice_"+data['invoices'][i]['inv_id']+"' name='invoice' value='"+data['invoices'][i]['inv_id']+"'/><input type='hidden' name='invbal_"+data['invoices'][i]['inv_id']+"' id='invbal_"+data['invoices'][i]['inv_id']+"' value='"+Number(data['invoices'][i]['inv_balanceamount']).toFixed(2)+"'></td><td>[ "+data['invoices'][i]['inv_index']+" ] - "+data['invoices'][i]['inv_description']+"_"+data['invoices'][i]['inv_date']+"</td><td>"+Number(data['invoices'][i]['inv_netamount']).toFixed(2)+"</td><td>"+Number(data['invoices'][i]['inv_paidamount']).toFixed(2)+"</td><td>"+Number(data['invoices'][i]['inv_totcnote']).toFixed(2)+"</td><td>"+Number(data['invoices'][i]['inv_totdnote']).toFixed(2)+"</td><td>"+Number(data['invoices'][i]['inv_balanceamount']).toFixed(2)+"</td></tr>");

			  //  		$.validate({
					//    	form : '#cnote_form',
					// });
				}
			}
			else
			{
				$('#outs_table').append("<tr><td colspan='3'>No Outstanding Found</td></tr>");
			}
		}
	},	
	"json"
	);
}

function load_customers(type)
{
	$('#outs_table').empty();
	$('#outs_table').append("<tr><td colspan='3'>No Outstanding Found</td></tr>");

	$('#cnote_customer').empty();
	$('#cnote_customer').append('<option value=""></option>');
	$("#cnote_customer").select2("val", "");

	branch = $('#cnote_branch').val();

	if(type=='')
	{
		$('#cnote_customer').prop('disabled', true);
	}
	else
	{
		$('#cnote_customer').prop('disabled', false);
		$.post("<?php echo base_url('hci_payment/load_customers')?>",{'type':type,'branch':branch},
		function(data)
		{
			if(data == 'denied')
			{
        		funcres = {status:"denied", message:"You have no right to proceed the action"};
        		result_notification(funcres);
			}
			else
			{
				for (i = 0; i<data.length; i++) 
				{
					$('#cnote_customer').append('<option value="'+data[i]['cus_id']+'">[ '+data[i]['cus_index']+' ] - '+data[i]['first_name']+' '+data[i]['second_name']+'</option>');
				}
			}
		},	
		"json"
		);
	}
}

function load_cnotelist()
{
    $('#cnote_table').DataTable().destroy();

    $('#cnote_table').DataTable({
        "processing": true,
        "serverSide": true,
        "dom" : '<"row"<"col-md-6 form-group"l><"col-md-6 form-group text-left"f>>rt<"row"<"col-md-3"i><"col-md-9"p>><"clear">',
        "ajax": {
            url: "<?php echo base_url('hci_accounts/load_cnotelist')?>",
            type: 'POST',
        },
        "columns": [
            {"data": "cnote_index"},
            {"data": "inv_index"},
            {"data": "cnote_custype"},
            {"data": "cnote_cusname"},
            {"data": "cnote_description"},
            {"data": "cnote_date"},
            {"data": "cnote_totamt"},
            {"data": "actions"},
        ],
    });

    //$(".dataTables_filter").css('white-space','nowrap');
    //$(".dataTables_filter").css('width','100%');
    $(".dataTables_length select").addClass("form-control ");
    // $(".dataTables_filter label").addClass("control-label ");
    $(".dataTables_filter input").addClass("form-control ");
    $(".dataTables_paginate a").addClass("btn btn-sm ");
}

function print_cnote(id)
{
    print_docs = new Array();

    // if(id=="all")
    // {
    //     var x = 0;  
    //     $.each($('.inv_chk:checked'),function(index,value)
    //     {
    //         print_docs[x] = this.value;
    //         x = x+1;
    //     });
    // }
    // else
    // {
    print_docs[0] = id;
    // }

    if(print_docs.length > 0)
    {
        $.ajax(
        {
            url : "<?php echo base_url();?>/hci_accounts/print_cnote",
            data : {"print_docs" :print_docs},
            type : 'POST',
            async : false,
            cache: false,
            dataType : 'text',
            success:function(data)
            {
            	if(data == 'denied')
				{
	        		funcres = {status:"denied", message:"You have no right to proceed the action"};
	        		result_notification(funcres);
				}
				else
				{
	                //console.log(data);
	                //window.location = '<?php echo base_url('index.php?print_invoice')?>';
	                var windowObject = window.open("data:application/pdf;base64," + data,'PDF','toolbar=0,titlebar=0,scrollbars=0,menubar=0,fullscreen=0');
	            }
            }
        });
    }
}



function load_cnote_receipt(id){
	 $.post("<?php echo base_url('hci_accounts/load_cnote_receipt')?>",{'id':id},
	 function(data)
	{
		if(data == 'denied')
		{
    		funcres = {status:"denied", message:"You have no right to proceed the action"};
    		result_notification(funcres);
		}
		else
		{
			$('#cnote_idx').empty();
			$('#cnote_idx').append("#"+data['cnote']['cnote_index']);
			$('#cnote_idx2').empty();
			$('#cnote_idx2').append("#"+data['cnote']['cnote_index']);
			$('#cnote_cus').empty();
			$('#cnote_cus').append(data['cnote']['cnote_cusname']);
			$('#cnote_date').empty();
			$('#cnote_date').append(data['cnote']['cnote_date']);
			$('#cnote_des').empty();
			$('#cnote_des').append(data['cnote']['cnote_description']);
			$('#user').empty();
			$('#user').append(data['cnote']['cnote_createusername']);
			$('#created_date').empty();
			$('#created_date').append(data['cnote']['cnote_createddate']);
			$('#cnote_remarks').empty();
			$('#cnote_remarks').append(data['cnote']['cnote_remarks']);
		}
	},
	"json"
	);



}








</script>