<div class="row">
	<div class="col-md-12">
		<h3 class="page-header"><i class="fa fa-bank"></i> EDIT INVOICE</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
			<li><i class="fa fa-cog"></i>Accounts</li>
			<li><i class="fa fa-bank"></i>Invoice</li>
		</ol>
	</div>
</div>
<div>
	<section class="panel">
	    <header class="panel-heading">
	        Invoice ID :: <span id="inv_id"></span>
	    </header>
	    <div class="panel-body"> 
	    	<div class="row">
                <div class="col-md-2">
                    Student : 
                </div>
               	<div class="col-md-6" id="inv_stu">
                </div>
            </div>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Description</th>
                        <th>Amount</th>
                        <th width="25%">Payment Plan</th>
                    </tr>
                </thead>
                <tbody id="inv_fees">
                </tbody>
            </table>
            <div class="form-group">
                <div class="col-md-11">
                    <button type="submit" onclick="event.preventDefault();regenerate_invoice()" name="regi_btn" class="btn btn-primary">Re-generate Invoice</button>
                    <button onclick="event.preventDefault();backtoinvoicelookup();" class="btn btn-default">Back</button>
                </div>
            </div>
	    </div>
    </section>
</div>
<script type="text/javascript">

$( document ).ready(function() {
	id = '<?php echo $_GET["id"]?>';
	view_invoice(id);
});

function view_invoice(id)
{
    $.post("<?php echo base_url('hci_accounts/load_invoicefees')?>",{'id':id},
    function(data)
    { 
        if(data == 'denied')
        {
            funcres = {status:"denied", message:"You have no right to proceed the action"};
            result_notification(funcres);
        }
        else
        {
            $('#inv_id').empty();
            $('#inv_stu').empty();
            $('#inv_id').append(id);
            $('#inv_stu').append(data['invoice_data']['family_name']+' '+data['invoice_data']['other_names']);
            $('#inv_fees').empty();

            tot_amt = 0;
            addition = 0.00;
            discount = 0.00;

            for (i = 0; i<data['invoice_fees'].length; i++) 
            {
                if(data['invoice_fees'][i]['fsf_fee']==1)
                {
                      // if(paytype==2)
                      // {
                      //     paytxt = " : 1st Payment";
                      // }
                      // else
                      // {
                      //     paytxt = " : Full Payment";
                      // }
                    paytxt = "";
                }
                else
                {
                    paytxt = "";
                }

                for(x = 0; x<data['invoice_disc'].length; x++)
                {
                    if(data['invoice_disc'][x]['adj_feecat']==data['invoice_fees'][i]['fsf_fee'])
                    {
                        addtodisc = false;
                        if(data['invoice_disc'][x]['at_applyfor']==5)
                        {
                            if(data['invoice_disc'][x]['discfee'].length>0)
                            {
                                for(a = 0; a<data['invoice_disc'][x]['discfee'].length; a++)
                                {
                                    if(data['invoice_disc'][x]['discfee'][a]['aft_fsfee']==data['invoice_fees'][i]['fsf_id'])
                                    {
                                        if(data['invoice_fees'][i]['fsf_fee']==1)
                                        {
                                            if(data['invoice_data']['inv_description']=='Registration')
                                            {
                                                if(data['invoice_disc'][x]['discfee'][a]['aft_tot']==1)
                                                {
                                                    addtodisc = true;
                                                }
                                                else if(data['invoice_disc'][x]['discfee'][a]['aft_new']==1)
                                                {
                                                    addtodisc = true;
                                                }
                                            }
                                            else if(data['invoice_data']['inv_description']=='AnnualPromo')
                                            {
                                                if(data['invoice_disc'][x]['discfee'][a]['aft_old']==1)
                                                {
                                                    addtodisc = true;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            pay_type = '';
                                            for(b = 0; b<data['payment_plan'].length; b++)
                                            {
                                                if(data['payment_plan'][b]['plan_fee']==data['invoice_fees'][i]['fsf_fee'])
                                                {
                                                    pay_type = data['payment_plan'][b]['plan_type'];
                                                }
                                            }

                                            if(data['invoice_disc'][x]['adj_applyfor']==1)
                                            {
                                                if(pay_type == 'TA')
                                                {
                                                    addtodisc = true;
                                                }
                                            }
                                            else
                                            {
                                                addtodisc = true;
                                            }
                                        }  
                                    }
                                }
                            }
                            else
                            {
                                addtodisc = true;
                            }
                        }
                        else
                        {
                            if(data['invoice_disc'][x]['adj_feecat']==4)
                            {
                                sibs = data['invoice_fees'][i]['transiblingary'];
                                sib_seq = 0;
                                for(s = 1; s<sibs.length; s++)
                                {
                                    if(sibs[s]['es_admissionid']==data['invoice_data']['inv_student'])
                                    {
                                        sib_seq = s;
                                    }
                                }

                                if(sib_seq==2 && data['invoice_disc'][x]['at_applyfor']==2)
                                {
                                    addtodisc = true;
                                }

                                if(sib_seq>=3 && data['invoice_disc'][x]['at_applyfor']==3)
                                {
                                    addtodisc = true;
                                }
                            }
                            else
                            {
                                addtodisc = true;
                            }
                        }

                        if(addtodisc==true)
                        {
                            if(data['invoice_disc'][x]['adj_type']=='A')
                            {
                                if(data['invoice_disc'][x]['adj_amttype']=='P')
                                {
                                    addition += Number((data['invoice_fees'][i]['invf_amount']*data['invoice_disc'][x]['adj_amount'])/100);
                                }
                                else
                                {
                                    addition += Number(data['invoice_disc'][x]['adj_amount']);
                                }
                                
                            }
                            else
                            {
                                if(data['invoice_disc'][x]['adj_amttype']=='P')
                                {
                                    discount += Number((data['invoice_fees'][i]['invf_amount']*data['invoice_disc'][x]['adj_amount'])/100);
                                }
                                else
                                {
                                    discount += Number(data['invoice_disc'][x]['adj_amount']);
                                }
                            }
                        }
                    }
                }
                tot_amt += Number(data['invoice_fees'][i]['invf_amount']);

                selectoptions = "";
                for(p = 0; p<data['allpayplans'].length; p++)
                {
                	if(data['allpayplans'][p]['plan_fee'] == data['invoice_fees'][i]['fsf_fee'])
                	{
                		selpayplan = '';
                		for(b = 0; b<data['payment_plan'].length; b++)
    	                {
    	                	if(data['payment_plan'][b]['plan_id']==data['allpayplans'][p]['plan_id'])
    	                	{
    	                		selpayplan = "selected";
    	                	}
    	                }

                		selectoptions += "<option value='"+data['allpayplans'][p]['plan_id']+"' "+selpayplan+">"+data['allpayplans'][p]['plan_name']+"</option>";
                	}         	
                }

                if(data['invoice_fees'][i]['fsf_fee']==1 || data['invoice_fees'][i]['fsf_fee']==2 || data['invoice_fees'][i]['fsf_fee']==3)
                {
                	$('#inv_fees').append("<tr><td>"+data['invoice_fees'][i]['fc_name']+paytxt+"</td><td style='text-align:right'>"+Number(data['invoice_fees'][i]['invf_amount']).toFixed(2)+"</td><td><select id='' name='input_payplan[]' class='form-control input_payplan'>"+selectoptions+"</select></td></tr>"); 
                }
                else
                {
                	$('#inv_fees').append("<tr><td>"+data['invoice_fees'][i]['fc_name']+paytxt+"</td><td style='text-align:right'>"+Number(data['invoice_fees'][i]['invf_amount']).toFixed(2)+"</td><td></td></tr>"); 
                }
            }
            net_tot = (tot_amt+addition)-discount;
            $('#inv_fees').append("<tr><td style='text-align:right'> G.Total </td><td style='text-align:right'>"+Number(tot_amt).toFixed(2)+"</td><td></td></tr>");
            $('#inv_fees').append("<tr><td style='text-align:right'> Add </td><td style='text-align:right'>"+Number(addition).toFixed(2)+"</td><td></td></tr>");
            $('#inv_fees').append("<tr><td style='text-align:right'> Disc </td><td style='text-align:right'>"+Number(discount).toFixed(2)+"</td><td></td></tr>");
          
            for(x = 0; x<data['other_adjs'].length; x++)
            {
                if(data['other_adjs'][x]['adj_type']=='A')
                {
                    if(data['other_adjs'][x]['adj_amttype']=='P')
                    {
                        add_amt = Number((net_tot*data['other_adjs'][x]['adj_amount'])/100);
                    }
                    else
                    {
                        add_amt = Number(data['other_adjs'][x]['adj_amount']);
                    }

                    net_tot += add_amt;

                    $('#inv_fees').append("<tr><td style='text-align:right'> "+data['other_adjs'][x]['adj_description']+" </td><td style='text-align:right'>"+Number(add_amt).toFixed(2)+"</td></tr>");
                }
                else
                {
                    if(data['other_adjs'][x]['adj_amttype']=='P')
                    {
                        ded_amount += Number((net_tot*data['other_adjs'][x]['adj_amount'])/100);
                    }
                    else
                    {
                        ded_amount += Number(data['other_adjs'][x]['adj_amount']);
                    }

                    net_tot -= ded_amount;

                    $('#inv_fees').append("<tr><td style='text-align:right'> "+data['other_adjs'][x]['adj_description']+" </td><td style='text-align:right'>"+Number(ded_amount).toFixed(2)+"</td></tr>");
                }
            }

            $('#inv_fees').append("<tr><td style='text-align:right'> Net Total </td><td style='text-align:right'>"+Number(net_tot).toFixed(2)+"</td></tr>");
        }
    },  
    "json"
    );
}

function regenerate_invoice()
{
	id = '<?php echo $_GET["id"]?>';
    plans = new Array();

    var x = 0;  
    $.each($('.input_payplan'),function(index,value)
    {
        plans[x] = this.value;
        x = x+1;
    });

    $.ajax(
    {
        url : "<?php echo base_url();?>/hci_accounts/regenerate_invoice",
        data : {"plans" :plans,"id":id},
        type : 'POST',
        async : false,
        cache: false,
        dataType : 'text',
        success:function(data)
        {
        	//location.reload();
        }
    });
}

function backtoinvoicelookup()
{
	 window.location = '<?php echo base_url('hci_accounts/invoice_view')?>';
}
</script>