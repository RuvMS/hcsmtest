<div class="row">
	<div class="col-md-12">
		<h3 class="page-header"><i class="fa fa-users"></i> USER MANAGEMENT</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
			<li><i class="fa fa-cog"></i>Settings</li>
			<li><i class="fa fa-user"></i>User</li>
		</ol>
	</div>
</div>
<div>
  	<!-- Nav tabs -->
  	<ul class="nav nav-tabs" role="tablist">
    	<li role="presentation" class="active"><a href="#user_tab" aria-controls="user_tab" role="tab" data-toggle="tab">User</a></li>
    	<li role="presentation"><a href="#group_tab" aria-controls="group_tab" role="tab" data-toggle="tab">User Group</a></li>
  	</ul>
  	<!-- Tab panes -->
  	<div class="tab-content">
    	<div role="tabpanel" class="tab-pane active" id="user_tab">
    		<br>
    		<div class="row">
			  	<div class="col-md-12">
				  	<section class="panel">
					    <header class="panel-heading">
					       Look Up
					    </header>
				      	<div class="panel-body">	
				          	<table class="table">
				          		<thead>
				          			<tr>
				          				<th>User</th>
				          				<th>User Employee</th>
				          				<th>User Group</th>
				          				<th>Group</th>
				          				<th>Branch</th>
				          				<th>Actions</th>
				          			</tr>
				          		</thead>
				          		<tbody>
			          				<?php
			          					foreach ($users as $usr) 
			          					{
			          						if($usr['user_status']=='A')
			          						{
			          							$status_btn = "<a class='btn btn-warning btn-sm' onclick='event.preventDefault();update_status(".$usr['user_id'].",\"D\")'>Deactivate</a>";
			          						}
			          						else
			          						{
			          							$status_btn = "<a class='btn btn-success btn-sm' onclick='event.preventDefault();update_status(".$usr['user_id'].",\"A\")'>Activate</a>";
			          						}
			          						echo "<tr>";
			          						echo "<td>".$usr['user_name']."</td>";
			          						echo "<td>".$usr['user_employee']."</td>";
			          						echo "<td>".($usr['user_ugroup']!=null?$usr['ug_name']:'-')."</td>";
			          						echo "<td>".($usr['user_group']!=null?$usr['grp_name']:'-')."</td>";
			          						echo "<td>".($usr['user_branch']!=null?$usr['br_name']:'-')."</td>";
			          						echo "<td><a class='btn btn-info btn-sm' onclick='event.preventDefault();reset_user(".$usr['user_id'].")'>Reset</a> ".$status_btn." <a class='btn btn-danger btn-sm' onclick='event.preventDefault();delete_user(".$usr['user_id'].")'>Delete</a></td>";
			          						echo "</tr>";
			          					}
			          				?>
				          		</tbody>
				          	</table>
				      	</div>
				  	</section>
			  	</div>
			  	<!-- <div class="col-md-4">
				  	<section class="panel">
					    <header class="panel-heading">
					        Manage Users
					    </header>
				      	<div class="panel-body">
				          	<form class="form-horizontal" role="form" method="post" action="<?php echo base_url('user/update_user')?>" id="user_form" autocomplete="off" novalidate>
				              	<div class="form-group">
				              		<input type="hidden" id="user_id" name="user_id">
				                  	<label for="username" class="col-md-3 control-label">Username</label>
				                  	<div class="col-md-9">
				                      	<input type="text" class="form-control" data-validation="required length alphanumeric" data-validation-error-msg-required="Field can not be empty" data-validation-length="5-12" 
		 data-validation-error-msg-length="Between 5-12 chars" data-validation-error-msg-alphanumeric="Should be alphanumeric"  id="username" name="username" placeholder="">
				                  	</div>
				              	</div>
				              	<div class="form-group">
				                  	<label for="description" class="col-md-3 control-label" class="form-control" >Password</label>
				                  	<div class="col-md-9">
				                  		    <input class="form-control" type="password" id="password" name="password" data-validation="required strength" data-validation-strength="1" data-validation-error-msg-required="Field can not be empty">
				                  	</div>
				              	</div>
				              	<div class="form-group">
				                  	<div class="col-md-offset-2 col-md-11">
				                      	<button type="submit" class="btn btn-info">Save</button> 
				                      	<button type="submit" class="btn btn-default">Reset</button>
				                  	</div>
				              	</div>
				          	</form>
				      	</div>
				  	</section>
			  	</div> -->
			</div>
    	</div>
    	<div role="tabpanel" class="tab-pane" id="group_tab">
    		<br>
    		<div class="row">
				<div class="col-md-6">
				  	<section class="panel">
					    <header class="panel-heading">
					        Manage User Groups
					    </header>
				      	<div class="panel-body">
				          	<form class="form-horizontal" role="form" method="post" action="<?php echo base_url('user/save_usergroup')?>" id="grp_form" autocomplete="off" novalidate>
				              	<div class="form-group">
				              		<input type="hidden" id="group_id" name="group_id">
				                  	<label for="grname" class="col-md-2 control-label">Group Name</label>
				                  	<div class="col-md-10">
				                      	<input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="grname" name="grname" placeholder="">
				                      	<!-- <p class="help-block">Example block-level help text here.</p> -->
				                  	</div>
				              	</div>
				              	<div class="form-group">
				                  	<label for="description" class="col-md-2 control-label" class="form-control" >Description</label>
				                  	<div class="col-md-10">
				                  		<textarea class="form-control" name="description" id="description"></textarea>
				                  	</div>
				              	</div>
				              	<div class="form-group">
				                  	<label for="acclevel" class="col-md-2 control-label" class="form-control" >Access Level</label>
				                  	<div class="col-md-10">
				                  		<input type="text" class="form-control" data-validation="required number"  data-validation-error-msg-required="Field can not be empty" data-validation-error-msg-number="Invalid. Please Try Again." id="acclevel" name="acclevel" placeholder="">
				                  	</div>
				              	</div>
				              	<div class="form-group">
				                  	<div class="col-md-offset-2 col-md-11">
				                      	<button type="submit" class="btn btn-info">Save</button> 
				                      	<button type="submit" class="btn btn-default">Reset</button>
				                  	</div>
				              	</div>
				          	</form>
				      	</div>
				  	</section>
			  	</div>
			  	<div class="col-md-6">
				  	<section class="panel">
					    <header class="panel-heading">
					       Look Up
					    </header>
				      	<div class="panel-body">	
				          	<table class="table">
				          		<thead>
				          			<tr>
				          				<th>User Group</th>
				          				<th>Description</th>
				          				<th>Access Level</th>
				          				<th>Actions</th>
				          			</tr>
				          		</thead>
				          		<tbody>
			          				<?php
			          					foreach ($groups as $grp) 
			          					{
			          						echo "<tr>";
			          						echo "<td>".$grp['ug_name']."</td>";
			          						echo "<td>".$grp['ug_description']."</td>";
			          						echo "<td>".$grp['ug_level']."</td>";
			          						echo "<td><a class='btn btn-info btn-sm' onclick='event.preventDefault();edit_group_load(".$grp['ug_id'].",\"".$grp['ug_name']."\",\"".$grp['ug_description']."\",\"".$grp['ug_level']."\")'>Edit</a></td>";
			          						echo "</tr>";
			          					}
			          				?>
				          		</tbody>
				          	</table>
				      	</div>
				  	</section>
			  	</div>
			</div>
    	</div>
  	</div>
</div>
<script type="text/javascript">
$.validate({
   	form : '#user_form',
   	modules : 'security',
   	onModulesLoaded : function() {
    var optionalConfig = {
      fontSize: '12pt',
      padding: '4px',
      bad : 'Very bad',
      weak : 'Weak',
      good : 'Good',
      strong : 'Strong'
    };

    $('input[name="password"]').displayPasswordStrength(optionalConfig);
	}
});

$.validate({
   	form : '#grp_form'
});

function edit_group_load(id,name,description,acclevel)
{
	$('#group_id').val(id);
	$('#grname').val(name);
	$('#description').val(description);
	$('#acclevel').val(acclevel);
}

function reset_user(id)
{
	$.post("<?php echo base_url('user/reset_user')?>",{"id":id},
	function(data)
	{
		if(data == 'denied')
		{
    		funcres = {status:"denied", message:"You have no right to proceed the action"};
    		result_notification(funcres);
		}
		else
		{	
			location.reload();
		}
	},	
	"json"
	);
}

function update_status(id,status)
{
	$.post("<?php echo base_url('user/update_status')?>",{"id":id,"status":status},
	function(data)
	{
		if(data == 'denied')
		{
    		funcres = {status:"denied", message:"You have no right to proceed the action"};
    		result_notification(funcres);
		}
		else
		{	
			location.reload();
		}
	},	
	"json"
	);
}

function delete_user(id,status)
{
	$.post("<?php echo base_url('user/delete_user')?>",{"id":id},
	function(data)
	{
		if(data == 'denied')
		{
    		funcres = {status:"denied", message:"You have no right to proceed the action"};
    		result_notification(funcres);
		}
		else
		{	
			location.reload();
		}
	},	
	"json"
	);
}
</script>
