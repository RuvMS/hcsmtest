<div class="row">
    <div class="col-md-12">
        <h3 class="page-header"><i class="fa fa-key"></i> USER GROUP MANAGEMENT</h3>
        <ol class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
            <li><i class="fa fa-lock"></i>System Access</li>
            <li><i class="fa fa-key"></i>Access Rights</li>
        </ol>
    </div>
</div>
<div>
<div class="panel">
    <header class="panel-heading">
        Manage Group Access Rights
    </header>
    <form class="form-horizontal" role="form" method="post" action="<?php echo base_url('authmanage/set_rights')?>" id="ar_form" autocomplete="off" novalidate>
    <div class="panel-body">
    	<div class="row">
  			<div class="col-md-5">
  				<div class="form-group">
                  	<label for="group" class="col-md-3 control-label">User Group</label>
                  	<div class="col-md-9">
                      	<select type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="group" name="group">
                      		<option value=''></option>
                      		<?php
	          					foreach ($groups as $grp) 
	          					{
	          						if($grp['ug_level']>3)
	          						{
	          							echo "<option value='".$grp['ug_id']."'>".$grp['ug_name']."</option>";
	          						}
	          					}
	          				?>
                      	</select>
                  	</div>
              	</div>
  			</div>
  			<div class="col-md-5">
  				<div class="form-group">
                  	<label for="module" class="col-md-3 control-label">Module</label>
                  	<div class="col-md-9">
                      	<select type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="module" name="module">
                      		<option value='all'>All</option>
                      		<?php
	          					foreach ($pre_data['modules'] as $mod) 
	          					{
	          						echo "<option value='".$mod['func_module']."'>".$mod['func_module']."</option>";
	          					}
	          				?>
                      	</select>
                  	</div>
              	</div>
  			</div>
  		</div>
  		<br>
  		<div class="row">
  			<div class="col-md-5">
  				<div class="form-group">
                  	<label for="comp" class="col-md-3 control-label">Company</label>
                  	<div class="col-md-9">
                      	<select type="text" class="form-control" data-validation="required" onchange="load_branches(this.value)" data-validation-error-msg-required="Field can not be empty" id="comp" name="comp">
                      		<option value=''></option>
                      		<?php
	          					foreach ($company as $grp) 
	          					{
	          						echo "<option value='".$grp['grp_id']."'>".$grp['grp_name']."</option>";
	          					}
	          				?>
                      	</select>
                  </div>
              	</div>
  			</div>
  			<div class="col-md-5">
  				<div class="form-group">
                  	<label for="branch" class="col-md-3 control-label">Branch</label>
                  	<div class="col-md-9">
                      	<select type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="branch" name="branch">
                      	</select>
                  	</div>
              	</div>
  			</div>
  			<div class="col-md-1"><a class='btn btn-info btn-sm' onclick='event.preventDefault();search_right()'>Search</a></div>
  		</div>
  		<hr>
  		<table class="table table-bordered">
      		<thead>
      			<tr>
      				<th>Sub Module</th>
      				<th>Description</th>
      				<th>Select</th>
      			</tr>
      		</thead>
      		<tbody id="grp_table">
  				<tr><td colspan='5'>Select User Group to Load Rights</td></tr>
      		</tbody>
      	</table>
    </div>
    <div class="panel-footer">
        <button type="submit" class="btn btn-info">Save</button> 
        <button type="submit" class="btn btn-default">Reset</button>
    </div>
    </form>
</div>
</div>

<script type="text/javascript">

$.validate({
   	form : '#ar_form'
});


function search_right()
{
	group  = $('#group').val();
	module = $('#module').val();
	branch = $('#branch').val();

	$('#grp_table').empty();

	$.post("<?php echo base_url('authmanage/search_right')?>",{"group":group,"module":module,"branch":branch},
		function(data)
		{	
			if(data == 'denied')
			{
        		funcres = {status:"denied", message:"You have no right to proceed the action"};
        		result_notification(funcres);
			}
			else
			{
				if(data.length>0)
				{	
					for (i = 0; i<data.length; i++) 
					{
						numchecked = 0;
						funcs = data[i]['functions'];

						for (x = 0; x<funcs.length; x++) 
						{
							if(funcs[x]['rgt_hasrgt']=='A')
							{
								++numchecked;
							}
						}

						if((funcs.length)==numchecked)
						{
							ch_stat = 'checked';
						}
						else
						{
							ch_stat = '';
						}

						modid = data[i]['func_module'].replace(/\s+/g, '_');

						$('#grp_table').append("<tr class='success'><td  colspan='2'><strong>"+data[i]['func_module']+"</strong></td><td><input type='checkbox' class='mod_check' id='"+data[i]['func_module']+"' name='mod_check[]' onclick='select_module_all(this.id,this.value)' value='ar_"+modid+"' "+ch_stat+"></td></tr>");

						for (x = 0; x<funcs.length; x++) 
						{
							if(funcs[x]['rgt_hasrgt']=='A')
							{
								func_stat = 'checked';
							}
							else
							{
								func_stat = '';
							}

							$('#grp_table').append("<tr><td>"+funcs[x]['func_submodule']+"</td><td>"+funcs[x]['func_description']+"</td><td><input type='checkbox' class='ar_"+modid+"' name='ar_check[]' value='"+funcs[x]['func_id']+"' "+func_stat+"></td></tr>");
						}
					}
				}
				else
				{
					$('#grp_table').append("<tr><td colspan='5'>Select User Group to Load Rights</td></tr>");
				}
			}
		},	
		"json"
	);
}

function load_branches(id)
{
	$('#branch').empty();
	$('#branch').append("<option value=''></option>");

	$.post("<?php echo base_url('company/load_branches')?>",{'id':id},
		function(data)
		{	
			if(data == 'denied')
			{
        		funcres = {status:"denied", message:"You have no right to proceed the action"};
        		result_notification(funcres);
			}
			else
			{
				if(data.length>0)
				{
					for (i = 0; i<data.length; i++) {
					   	$('#branch').append("<option value='"+data[i]['br_id']+"'>["+data[i]['br_code']+'] - '+data[i]['br_name']+"</option>");
					}
				}
			}
		},	
		"json"
	);
}

function select_module_all(id,value)
{
	if(document.getElementById(id).checked)
	{
		$('.'+value).prop('checked', true);
	}
	else
	{
		$('.'+value).prop('checked', false);
	}
}

</script>
