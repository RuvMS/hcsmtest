<div class="row">
    <div class="col-md-12">
        <h3 class="page-header"><i class="fa fa-map-marker"></i> PICKING POINT MANAGEMENT</h3>
        <ol class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
            <li><i class="fa fa-car"></i>Transport</li>
            <li><i class="fa fa-map-marker"></i>Picking Point</li>
        </ol>
    </div>
</div>
<div>
<section class="panel">
    <header class="panel-heading">
        <div class="col-md-4">
            <div class="col-md-8">
                <h4>PICKING POINT NO : </h4>
            </div>
            <div class="col-md-4" id="pp_id">
                <h4>#<?php echo $pointindex;?></h4>
            </div>
        </div>
    </header>
    <form class="form-horizontal" role="form" method="post" action="<?php echo base_url('hci_transport/save_pickpoint')?>" id="pp_form" autocomplete="off" novalidate>
    <div class="panel-body">  
        <div class="row">
            <div class="col-md-5">  
                <br>
                <div class="form-group">
                    <label for="pp_route" class="col-md-3 control-label">Route</label>
                    <div class="col-md-8">
                        <select class="form-control select2" id="pp_route" name="pp_route"  style="width: 100%;" data-validation="required" data-validation-error-msg-required="Field can not be empty">
                            <option value=""></option>
                            <?php
                            foreach($routes as $rt):
                                ?>
                                <option value="<?php echo $rt['id'];?>">
                                    <?php echo $rt['route_title'];?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="pick_point" class="col-md-3 control-label">Picking Point</label>
                    <div class="col-md-6">
                        <input type="hidden" name="pick_id" id="pick_id">
                        <input type="text" data-validation="required" data-validation-error-msg-required="Field can not be empty" class="form-control" id="pick_point" name="pick_point" placeholder="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="Pick_dist" class="col-md-3 control-label">Distance From School</label>
                    <div class="col-md-5">
                        <select class="form-control" id="Pick_dist" name="Pick_dist"  style="width: 100%;" data-validation="required" data-validation-error-msg-required="Field can not be empty">
                            <option value=""></option>
                            <option value="1">Within 2 km</option>
                            <option value="2">Beyond 2 km</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <table id="pptable" class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Picking Point</th>
                            <th>Route</th>
                            <th>Type</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($picks as $pick) 
                            {
                                $type = 'Beyond 2km';
                                if($pick['tpp_stype']==1)
                                {
                                    $type = 'Within 2km';
                                }
                                echo "<tr>";
                                echo "<td>".$pick['tpp_index']."</td>";
                                echo "<td>".$pick['place_name']."</td>";   
                                echo "<td>".$pick['route_title']."</td>";   
                                echo "<td>".$type."</td>";   
                                echo "<td><a class='btn btn-info btn-xs' onclick='event.preventDefault();edit_pickpoint(".$pick['tr_place_id'].",".$pick['tpp_route'].",\"".$pick['place_name']."\",\"".$pick['tpp_index']."\",".$pick['tpp_stype'].")'>Edit</a></td>";
                                echo "</tr>";
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <button type="submit" class="btn btn-info">Save</button> 
        <button type="submit" class="btn btn-default">Reset</button>
    </div>
    </form>
</section>
</div>
<script type="text/javascript">
$.validate({
    form : '#pp_form'
});

var table = $('#pptable').DataTable( {
        "pageLength": 5,
        "dom" : '<"row"<"col-md-6"l><"col-md-6"f>>rt<"row"<"col-md-4"i><"col-md-8"p>><"clear">',
    } );

function edit_pickpoint(id,tr,name,ind,dist)
{
    $('#pick_id').val(id);
    $('#pp_route').val(tr);
    $('#pick_point').val(name);
    $('#Pick_dist').val(dist);
    $('#pp_id').empty();
    $('#pp_id').append('<h4>#'+ind+'</h4>');
}
</script>