<div class="row">
	<div class="col-md-12">
		<h3 class="page-header"><i class="fa fa-graduation-cap"></i>SUBJECT</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
			<li><i class="fa fa-cog"></i>Subject</li>
		</ol>
	</div>
</div>
<div>
	<br>
	<div class="row">
		<div class="col-md-6">
		  	<section class="panel">
			    <header class="panel-heading">
			        Manage Subject
			    </header>
		      	<div class="panel-body">	
		          	<form class="form-horizontal" role="form" method="post" action="<?php echo base_url('hci_subject/save_subject')?>" id="subjform" autocomplete="off" novalidate>
		          		<div class="form-group">
		                  	<label for="sub_name" class="col-md-4 control-label">Subject</label>
		                  	<div class="col-md-6">
		                      	<input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="sub_name" name="sub_name" placeholder="">
		                  	</div>
		              	</div>
		          		<div class="form-group">
		                  	<label for="sub_curriculum" class="col-md-4 control-label">Curriculum</label>
		                  	<div class="col-md-6">
		                  		<select  class="form-control" id="sub_curriculum" name="sub_curriculum" placeholder="" onchange="load_schemelist(this.value,null)">
		                  			<?php
		                  				if(!empty($curriculum))
			          					{
			          						echo "<option value=''></option>";
			          						foreach ($curriculum as $cur) 
				          					{
				          						echo "<option value='".$cur['cur_id']."'>".$cur['cur_name']."</option>";
				          					}
			          					}
		                  			?>
		                  		</select>
		                  	</div>
		              	</div>
		          		<div class="form-group">
		              		<input type="hidden" id="sub_id" name="sub_id">
		                  	<label for="sub_section" class="col-md-4 control-label">Section</label>
		                  	<div class="col-md-6">
		                  		<select  class="form-control" id="sub_section" name="sub_section" placeholder="" onchange="load_schemelist(this.value)">
		                  			<?php
		                  				if(!empty($sections))
			          					{
			          						echo "<option value=''></option>";
			          						foreach ($sections as $section) 
				          					{
				          						if($section["sec_status"]=="A")
										    	{
				          							echo "<option value='".$section['sec_id']."'>".$section['sec_name']."</option>";
				          						}
				          					}
			          					}
		                  			?>
		                  		</select>
		                  	</div>
		              	</div>
		              	<div id="schemes_div" class=""></div>
		              	<div class="form-group">
		                  	<label for="sub_type" class="col-md-4 control-label">Type</label>
		                  	<div class="col-md-6">
		                      	<select type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="sub_type" name="sub_type" placeholder="">
		                      		<option value=""></option>
		                      		<option value="C">Compulsory</option>
		                      		<option value="O">Optional</option>
		                      	</select>
		                  	</div>
		              	</div>
		              	<!-- <div class="form-group">
		                  	<label for="sub_mainorsel" class="col-md-4 control-label">Main/NS</label>
		                  	<div class="col-md-6">
		                      	<select type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="sub_mainorsel" name="sub_mainorsel" placeholder="">
		                      		<option value=""></option>
		                      		<option value="M">Main</option>
		                      		<option value="S">NS</option>
		                      	</select>
		                  	</div>
		              	</div> -->
		              	<div class="form-group">
		                  	<label class="col-md-4 control-label">Sub - Subjects</label>
		              	</div>
						<div class="clone_div" id="clone_div">
					    	<div id="clonedInput1" class="clonedInput row">
					    		<input type="hidden" name="ssubid[]" id="ssubid">
					    		<div class="col-md-3"></div>
								<div class="form-group col-md-5">
									<input type="text" class="form-control" id="ssubname" name="ssubname[]" placeholder="">
								</div>
								<div class="col-md-2">
									<span class="button-group">
										<button onclick="cloning(null,null)" type="button" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-plus"></span></button>
										<button type="button" name = "remove_entry[]" class="btn btn-default btn-xs remove_entry"><span class="glyphicon glyphicon-minus"></span></button>
									</span>
								</div>
							</div>							
						</div>
		              	<div class="form-group">
		                  	<div class="col-md-offset-2 col-md-11">
		                      	<button type="submit" class="btn btn-info">Save</button> 
		                      	<button type="reset" class="btn btn-default">Reset</button>
		                  	</div>
		              	</div>
		          	</form>
		      	</div>
		  	</section>
	  	</div>
	  	<div class="col-md-6">
		  	<section class="panel">
			    <header class="panel-heading">
			       Look Up
			    </header>
		      	<div class="panel-body">	
		          	<table class="display" id="sub_tbl">
		          		<thead>
		          			<tr>
		          				<th>#</th>
		          				<th>Subject</th>
		          				<th>Curriculum</th>
		          				<th>Section</th>
		          				<th>Type</th>
		          				<!-- <th>Subject</th> -->
		          				<th>Actions</th>
		          			</tr>
		          		</thead>
		          		<tbody>
	          				<?php
	          					$i=1;
	          					if(!empty($subjects))
	          					{
	          						foreach ($subjects as $sub) 
		          					{
		          						$typetxt = 'Compulsory';
		          						if($sub['sub_type']=='O')
		          						{
		          							$typetxt = 'Optional';
		          						}

		          						echo "<tr>";
		          						echo "<td>".$i."</td>";
		          						echo "<td>".$sub['sub_name']."</td>";
		          						echo "<td>".$sub['cur_name']."</td>";
		          						echo "<td>".$sub['sec_name']."</td>";
		          						echo "<td>".$typetxt."</td>";
		          						echo "<td><a class='btn btn-info btn-xs' onclick='event.preventDefault();edit_sub_load(".$sub['sub_id'].")'><span class='glyphicon glyphicon-pencil' aria-hidden='true'></span></a>| ";

		          	// 					if($sub["desig_isused"]==0)
								    	// {
		          	// 						echo "<a class='btn btn-danger btn-xs' onclick='event.preventDefault();delete_group_load
		          	// 					     (".$sub['desig_id'].")'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></a>| ";
		          	// 					}

								    	if($sub["sub_status"]=="A")
								    	{
								    		echo "<button onclick='event.preventDefault();change_status(".$sub["sub_id"].",\"D\")' class='btn btn-warning btn-xs'><span class='glyphicon glyphicon-ban-circle' aria-hidden='true'></span></button>";
								    	}
								    	else
								    	{
								    		echo "<button onclick='event.preventDefault();change_status(".$sub["sub_id"].",\"A\")' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-ok-circle' aria-hidden='true'></span></button>";
								    	}
								    	
		          						echo "</td></tr>";

		          						$i++;
		          					}
	          					}
	          				?>
		          		</tbody>
		          	</table>
		      	</div>
		  	</section>
	  	</div>
	</div>
</div>
<script type="text/javascript">

$.validate({
   	form : '#subjform'
});

function load_schemelist(id,selectedlst)
{
	$('#schemes_div').empty();
	$.post("<?php echo base_url('hci_subject/load_schemelist')?>",{"id":id},
	function(data)
	{	
		if(data == 'denied')
		{
    		funcres = {status:"denied", message:"You have no right to proceed the action"};
    		result_notification(funcres);
		}
		else
		{
			for (i = 0; i<data.length; i++) 
			{
				checkedtxt = "";
				disabledtxt = "disabled";
				selectedid = "";
				if(selectedlst!=null)
				{
					for (x = 0; x<selectedlst.length; x++) 
					{
						if(selectedlst[x]['subschm_scheme']==data[i]['schm_id'])
						{
							checkedtxt = "checked='checked'";
							disabledtxt = "";
							selectedid = selectedlst[x]['subschm_subgroup']
						}
						
					}
				}
				
				optiontx = '<option value=""></option>';

				for (x = 0; x<data[i]['subjgrp'].length; x++) 
				{
					selectedtxt = "";
					if(selectedid==data[i]['subjgrp'][x]['subgrp_id'])
					{
						selectedtxt = "selected";
					}
					optiontx += '<option value="'+data[i]['subjgrp'][x]['subgrp_id']+'" '+selectedtxt+'>'+data[i]['subjgrp'][x]['subgrp_name']+'</option>';
				}

				checkbxtx = '<label class="label_check c_on" for="schemecheck"><input name="schemecheck[]" id="'+data[i]['schm_name']+'" value="'+data[i]['schm_id']+'" type="checkbox" onclick="show_hide_select(this.id,this.value)" '+checkedtxt+'> '+data[i]['schm_name']+'</label>';

				selecttxt  = '<select class="form-control" name="schm_'+data[i]['schm_id']+'" id="schm_'+data[i]['schm_id']+'" '+disabledtxt+'>'+optiontx+'</select>';

	        	$('#schemes_div').append('<div class="row"><div class="col-md-3"></div><div class="col-md-3">'+checkbxtx+'</div><div class="col-md-5">'+selecttxt+'</div><div class="col-md-1"></div></div><br>'); 
	        }
	    }
	},	
	"json"
	);
}

function show_hide_select(id,val)
{
	if($("#"+id).is(':checked'))
	{
		$('#schm_'+val).prop('disabled', false);
	}
	else
	{
		$('#schm_'+val).prop('disabled', 'disabled');
	}
}

$(document).ready(function() {
    $('#sub_tbl').DataTable();
} );

function edit_sub_load(id)
{
	$( ".temprw" ).each(function( index ) {
		 $(this).parents(".clonedInput").remove();
	});

	$('#ssubname').val('');
	$('#ssubname').attr('name','ssubname[]');
	$('#ssubid').val('');
	$('#ssubid').attr('name','ssubid[]');

	$.post("<?php echo base_url('hci_subject/edit_sub_load')?>",{"sub_id":id},
	function(data)
	{
		if(data == 'denied')
		{
    		funcres = {status:"denied", message:"You have no right to proceed the action"};
    		result_notification(funcres);
		}
		else
		{	
			$('#sub_id').val(data['subject']['sub_id']);
			$('#sub_name').val(data['subject']['sub_name']);
			$('#sub_curriculum').val(data['subject']['sub_curriculum']);
			$('#sub_section').val(data['subject']['sub_section']);
			$('#sub_type').val(data['subject']['sub_type']);

			for (i = 0; i<data['subsubjects'].length; i++) 
			{
				if(i==0)
				{
					$('#ssubname').val(data['subsubjects'][i]['ssub_name']);
					$('#ssubname').attr('name','ssubname_' +data['subsubjects'][i]['ssub_id']);

					$('#ssubid').val(data['subsubjects'][i]['ssub_id']);
					$('#ssubid').attr('name','ssubid[' +data['subsubjects'][i]['ssub_id']+']');
				}
				else
				{
					cloning(data['subsubjects'][i]['ssub_id'],data['subsubjects'][i]['ssub_name']);
				}
			}

			load_schemelist(data['subject']['sub_section'],data['schemes']);
		}
	},	
	"json"
	);
}

var cloneid=0;

function cloning(ssubid,ssubname)
{ 
 	cloneid+=1;
 	//alert(cloneid);
    var container = document.getElementById('clone_div');
    var clone = $('#clonedInput1').clone();

    clone.find('#ssubid').attr("class","form-control temprw");
    if(ssubid!=null)
    {
    	clone.find('#ssubid').val(ssubid);
		clone.find('#ssubid').attr('name','ssubid[' +ssubid+']');
		clone.find('#ssubid').removeAttr("id");
    }
    else
    {
    	clone.find('#ssubid').val('');
		clone.find('#ssubid').attr('name','ssubidnew[]');
		clone.find('#ssubid').removeAttr("id");
    }

    if(ssubname!=null)
    {
    	clone.find('#ssubname').val(ssubname);
		clone.find('#ssubname').attr('name','ssubname_' +ssubid);
		clone.find('#ssubname').removeAttr("id");
    }
    else
    {
    	clone.find('#ssubname').val('');
		clone.find('#ssubname').attr('name','ssubname[]');
		clone.find('#ssubname').removeAttr("id");
    }

    clone.find('.remove_entry').attr('id',cloneid);
    clone.find('.remove_entry').attr('onclick','$(this).parents(".clonedInput").remove();Calculate_gr_tot()');
 
 	$('.clone_div').append(clone);
}

// function delete_group_load(id)
// {	
// 	$.post("<?php echo base_url('hci_subject/remove_dept')?>",{"grd_id":id},
// 	function(data)
// 	{	
// 		location.reload();
// 	},	
// 	"json"
// 	);
// }

function change_status(id,new_s)
{	
	$.post("<?php echo base_url('hci_subject/change_substatus')?>",{"sub_id":id,"new_s":new_s},
	function(data)
	{	
		if(data == 'denied')
		{
    		funcres = {status:"denied", message:"You have no right to proceed the action"};
    		result_notification(funcres);
		}
		else
		{
			location.reload();
		}
	},	
	"json"
	);
}

</script>
