<div class="row">
	<div class="col-md-12">
		<h3 class="page-header"><i class="fa fa-bank"></i> FEE STRUCTURE</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
			<li><i class="fa fa-cog"></i>Settings</li>
			<li><i class="fa fa-bank"></i>Fee Structure</li>
		</ol>
	</div>
</div>
<div>
	<section class="panel">
		<form class="form-horizontal" role="form" method="post" action="<?php echo base_url('hci_fee_structure/save_fee_structure')?>" id="fs_form" autocomplete="off" novalidate>
	    <header class="panel-heading"> 
	        <div class="row">
	        	<div class="col-md-4">
	        		Manage Fee Structure : 
	        	</div>
				<div class="col-md-3"></div>
				<div class="col-md-5">
					<div class="form-group">
						<label for="fs_branch" class="col-md-4 control-label" style="font-size: 12px;padding-top: 0px">Branch</label>
		                <div class="col-md-8">
	                  		<?php 
	                  			global $branchdrop;
	                  			global $selectedbr;
	                  			$extraattrs = 'id="fs_branch" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" ';
	                  			echo form_dropdown('fs_branch',$branchdrop,$selectedbr, $extraattrs); 
	                  		?>
		                </div>
					</div>
				</div>
			</div>
	    </header>
      	<div class="panel-body" id="edit_view">
      		<div class="row">
  				<div class="form-group col-md-4">
                  	<label for="fs_name" class="col-md-3 control-label">Description</label>
                  	<div class="col-md-9">
                  		<input type='hidden' id='fs_id' name='fs_id'>
                      	<input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="fs_name" name="fs_name" placeholder="" value="">
                  	</div>
              	</div>
              	<div class="form-group col-md-4">
                  	<label for="eff_date" class="col-md-3 control-label">Eff. Date</label>
                  	<div class="col-md-9">
	                  	<div id="eff_date_div" class="input-group date">
				    		<input class="form-control" type="text" name="eff_date" id="eff_date"  data-format="YYYY-MM-DD">
				    		<span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span>
				    		</span>
			    		</div>
			    	</div>
              	</div>
              	<div class="form-group col-md-3">
                  	<label for="is_curr" class="col-md-10 control-label">Current Fee-Structure</label>
                  	<div style="padding-top: 10px" class="col-md-1">
                      	<input type="checkbox" id="is_curr" name="is_curr" >
                    </div>
              	</div> 	
      		</div>	
      		<div class="row">
  				<div class="form-group col-md-4">
                  	<label for="af_template" class="col-md-3 control-label">AF-Template</label>
                  	<div class="col-md-9">
                      	<select type="text" class="form-control" id="af_template" name="af_template" onchange="load_feetemp_amounts(this.value,null)">
		              		<option value=''></option>
		              		<?php
		      					foreach ($temps as $templ) 
		      					{
		      						if($templ['ft_feecat']==1)
		      						{
		      							echo "<option value='".$templ['ft_id']."'>".$templ['ft_name']."</option>";
		      						}
		      					}
		      				?>
		              	</select>
                  	</div>
              	</div>
              	<div class="form-group col-md-4">
                  	<label for="tf_template" class="col-md-3 control-label">TF-Template</label>
                  	<div class="col-md-9">
	                  	<select type="text" class="form-control" id="tf_template" name="tf_template" onchange="load_feetemp_amounts(null,this.value)">
		              		<option value=''></option>
		              		<?php
		      					foreach ($temps as $templ) 
		      					{
		      						if($templ['ft_feecat']!=1)
		      						{
		      							echo "<option value='".$templ['ft_id']."'>".$templ['ft_name']."</option>";
		      						}
		      					}
		      				?>
		              	</select>
			    	</div>
              	</div>
              	<div class="col-md-4">
              		<div class="row">
              			<label for="new_af" class="col-md-6 control-label">Save new AF Template</label>
	                  	<div class="col-md-1" style="padding-top: 10px">
	                  		<input type="checkbox" id="new_af" name="new_af" >
	                  	</div>
	                  	<div class="col-md-5">
	                  		<input type="text" data-validation="required" data-validation-error-msg-required="Field can not be empty" name="Af_name" id="Af_name" class="form-control">   	
	                    </div>
              		</div>
              		<br>
              		<div class="row">
              			<label for="new_tf" class="col-md-6 control-label">Save new TF Template</label>
	                  	<div class="col-md-1" style="padding-top: 10px">
	                  		<input type="checkbox" id="new_tf" name="new_tf" >
	                  	</div>
	                  	<div class="col-md-5">
	                  		<input type="text" data-validation="required" data-validation-error-msg-required="Field can not be empty" name="Tf_name" id="Tf_name" class="form-control">  	
	                    </div>
              		</div>
                </div>  
      		</div>	
      		<br>
      		<div class="row">
      			<div class="col-md-12">
      				<table class="table table-bordered">
		          		<thead id='fs_header'>
		          			<tr>
		          				<th rowspan='2' class='text-center'>Grade</th>
		          			<?php
	          					foreach ($fees as $fee)
	          					{
	          						if($fee['fc_id']==1)
	          						{
	          							echo "<th colspan='3' class='text-center'>".$fee['fc_name']."</th>";
	          						}
	          						else if($fee['fc_id']==2 || $fee['fc_id']==3)
	          						{
	          							echo "<th rowspan='2' class='text-center'>".$fee['fc_name']."</th>";
	          						}	
	          					}
	          				?>
	          				</tr>
	          				<tr>
		          			<?php
	          					foreach ($fees as $fee)
	          					{
	          						if($fee['fc_id']==1)
	          						{
	          							echo "<th class='text-center'>Total Amount</th><th class='text-center'>slabwise-New</th><th class='text-center'>slabwise-old</th>";
	          						}	
	          					}
	          				?>
	          				</tr>
		          		</thead>
		          		<tbody id="fs_body">
		          			<?php
	          					foreach ($grd_info as $grd) 
	          					{ 
	          						if($grd['grd_status']=='A')
	          						{
	          							echo "<tr>";
		          						echo "<td>".$grd['grd_name']."</td>";
		          						foreach ($fees as $fee)
			          					{
			          						if($fee['fc_id']==1)
		          							{
		          								echo "<td><input type='text' id='amt_".$grd['grd_id']."_".$fee['fc_id']."' name='amt_".$grd['grd_id']."_".$fee['fc_id']."_tot' class='amt_input form-control' value='0.00'></td>";
		          								echo "<td><input type='text' id='amtnew_".$grd['grd_id']."_".$fee['fc_id']."' name='amtnew_".$grd['grd_id']."_".$fee['fc_id']."' class='amt_input form-control' value='0.00'></td>";
		          								echo "<td><input type='text' id='amtold_".$grd['grd_id']."_".$fee['fc_id']."' name='amtold_".$grd['grd_id']."_".$fee['fc_id']."' class='amt_input form-control' value='0.00'></td>";
		          							}
		          							else if($fee['fc_id']==2 || $fee['fc_id']==3)
		          							{
		          								echo "<td><input type='text' id='amt_".$grd['grd_id']."_".$fee['fc_id']."' name='amt_".$grd['grd_id']."_".$fee['fc_id']."' value='0.00' class='amt_input form-control'></td>";
		          							}
			          					}
		          						echo "</tr>";
	          						}
	          					}
	          				?>
		          		</tbody>
		          	</table>
      			</div>
      		</div>
      		<div class="row form-group">
              	<div class="col-md-11">
              		<br>
              		<button type="submit" id="savebtn" name="asnewbtn" class="btn btn-info">Save</button> 
              		<button type="submit" id="asnewbtn" name="asnewbtn" class="btn btn-info">Save as new</button> 
                  	<button type="submit" id="changesbtn" name="changesbtn" class="btn btn-info">Save Changes</button> 
                  	<button type="button" class="btn btn-default" onclick="location.reload()">Back</button> 
              	</div>
          	</div>
          	</form>
      	</div>
      	<div class="panel-body" id="fs_view">
      		<div class="row">
      			<div class="col-md-6 form-group">
      				<div class="col-md-12" id="description_div">
	                  	<select type="text" class="form-control" id="curr_fs" name="curr_fs" onchange="load_feestructure_data(this.value)">
		              		<option value=''>---Check Fee Structure---</option>
		              		<?php
		      					foreach ($fss as $fs) 
		      					{
		      						$seltxt = '';

		      						if($fs['fee_iscurrent']==1)
		      						{
		      							$seltxt = 'Selected';
		      						}

		      						echo "<option value='".$fs['es_feemasterid']."' ".$seltxt.">[ ".$fs['fee_code'].' ] - '.$fs['fee_description']."</option>";
		      					}
		      				?>
		              	</select>
	              	</div>
      			</div>
      			<div class="col-md-6">
      				<button type="button" class="btn btn-default btn-sm" onclick="load_add_new()">Add New</button>
		      		<button type="button" class="btn btn-default btn-sm" onclick="load_edit_view()">Edit</button>
      				<button type="button" class="btn btn-default btn-sm" onclick="load_save_as()">Save as New</button>
      			</div>
      		</div>
      		<div class="row">
      			<div class="col-md-6" id="adm_temp"></div>
      			<div class="col-md-6" id="term_temp"></div>
      		</div>
      		<br>	
      		<div class="row">
      			<div class="col-md-12">
      				<table class="table table-bordered">
		          		<thead id='fsview_header'>
		          			<tr>
		          				<th rowspan='2' class='text-center'>Grade</th>
		          			<?php
	          					foreach ($fees as $fee)
	          					{
	          						if($fee['fc_id']==1)
	          						{
	          							echo "<th colspan='3' class='text-center'>".$fee['fc_name']."</th>";
	          						}
	          						else if($fee['fc_id']==2 || $fee['fc_id']==3)
	          						{
	          							echo "<th rowspan='2' class='text-center'>".$fee['fc_name']."</th>";
	          						}	
	          					}
	          				?>
	          				</tr>
	          				<tr>
		          			<?php
	          					foreach ($fees as $fee)
	          					{
	          						if($fee['fc_id']==1)
	          						{
	          							echo "<th class='text-center'>Total Amount</th><th class='text-center'>slabwise-New</th><th class='text-center'>slabwise-old</th>";
	          						}	
	          					}
	          				?>
	          				</tr>
		          		</thead>
		          		<tbody id="fsview_body">
		          			<?php
	          					foreach ($grd_info as $grd) 
	          					{
	          						echo "<tr>";
	          						echo "<td>".$grd['grd_name']."</td>";
	          						foreach ($fees as $fee)
		          					{
		          						if($fee['fc_id']==1)
	          							{
	          								echo "<td style='text-align:right' id='amttd_".$grd['grd_id']."_".$fee['fc_id']."''></td>";
	          								echo "<td style='text-align:right' id='amtnewtd_".$grd['grd_id']."_".$fee['fc_id']."'></td>";
	          								echo "<td style='text-align:right' id='amtoldtd_".$grd['grd_id']."_".$fee['fc_id']."'></td>";
	          							}
	          							else if($fee['fc_id']==2 || $fee['fc_id']==3)
	          							{
	          								echo "<td style='text-align:right' id='amttd_".$grd['grd_id']."_".$fee['fc_id']."'></td>";
	          							}
		          					}
	          						echo "</tr>";
	          					}
	          				?>
		          		</tbody>
		          	</table>
      			</div>
      		</div>
      	</div>
  	</section>
</div>
<!-- Modal -->
<!-- <div class="modal fade" id="fee_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
    		<form class="form-horizontal" role="form" method="post" action="<?php echo base_url('fee_structure/save_feegroups')?>" id="fee_form" autocomplete="off" novalidate>
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Add/ Edit Fee</h4>
      		</div>
      		<div class="modal-body">
      		<input type="hidden" name="fee_fs" id="fee_fs">
        		
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        		<button type="button" class="btn btn-primary">Save changes</button>
      		</div>
    	</div>
  	</div>
</div> -->
<!-- Modal -->
<!-- <div class="modal fade" id="grp_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
    		<form class="form-horizontal" role="form" method="post" action="<?php echo base_url('fee_structure/save_feegroups')?>" id="grp_form" autocomplete="off" novalidate>
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Manage Fee Group</h4>
      		</div>
      		<div class="modal-body">
      			<input type="hidden" name="grp_fs" id="grp_fs">
        		<?php
        			$groups = "<option value=''></option>";

        			foreach ($grd_info as $grd) 
        			{
        				for($i=1;$i<=10;$i++)
	        			{
	        				$groups.="<option value='".$grd['grd_id'].'_'.$i."'>Group - ".$i."</option>";
	        			}

        				echo '<div class="row form-group">';
        				echo '<label for="name" class="col-md-4 control-label">'.$grd['grd_name'].'</label>';
				        echo '<div class="col-md-4">';
        				echo '<select data-validation="required" data-validation-error-msg-required="Field can not be empty" class="form-control" id="grd_'.$grd['grd_id'].'" name="gradegrp[]">'.$groups.'</select>';
        				echo '</div>';
        				echo "</div>";
        				$groups = "<option value=''></option>";
        			}
        		?>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        		<button type="submit" class="btn btn-primary">Save changes</button>
      		</div>
      		</form>
    	</div>
  	</div>
</div> -->
<script type="text/javascript" src="<?php echo base_url('js/moment.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/bootstrap-datetimepicker.min.js'); ?>"></script>
<script type="text/javascript">

$.validate({
   	form : '#fs_form'
});

$.validate({
   	form : '#grp_form'
});

$.validate({
   	form : '#fee_form'
});
$('#eff_date_div').datetimepicker({ defaultDate: "<?php echo date('Y-m-d');?>",  pickTime: false});

$( document ).ready(function() {
	load_feestructure_data(null);
	$("#edit_view").hide();
	//$("#changesbtn").attr("disabled", true);
});

function load_add_new()
{
	$("#fs_id").val('');
	$("#fs_name").val('');
	$("#eff_date").val('');
	$(".amt_input").val(0.00);
	$('#savebtn').show();
	$('#asnewbtn').hide();
	$('#changesbtn').hide();
	$("#edit_view").show();
	$("#fs_view").hide();
	$('#is_curr').prop('checked', false);
	$("#af_template").val('');
	$("#tf_template").val('');
	$("#Af_name").val('');
	$("#Tf_name").val('');
}

function load_edit_view()
{
	$('#savebtn').hide();
	$('#asnewbtn').hide();
	$('#changesbtn').show();
	$("#edit_view").show();
	$("#fs_view").hide();
}

function load_save_as()
{
	$("#fs_id").val('');
	$("#fs_name").val('');
	$("#eff_date").val('');
	$('#is_curr').prop('checked', false);
	$('#savebtn').hide();
	$('#asnewbtn').show();
	$('#changesbtn').hide();
	$("#edit_view").show();
	$("#fs_view").hide();
}

function load_feestructure_data(id)
{
	$.post("<?php echo base_url('hci_fee_structure/load_feestructure_data')?>",{'fs_id':id},
		function(data)
		{
			if(data == 'denied')
			{
        		funcres = {status:"denied", message:"You have no right to proceed the action"};
        		result_notification(funcres);
			}
			else
			{
				$("#fs_id").val(data['es_feemasterid']);
				$("#fs_name").val(data['fee_description']);
				$("#eff_date").val(data['fee_effdate']);
				$("#af_template").val(data['fee_aftemp']);
				$("#tf_template").val(data['fee_tftemp']);
				$("#fs_branch").val(data['fee_branch']);

				$('#adm_temp').empty();
				$('#term_temp').empty();

				$('#adm_temp').append('Admission Template :: '+data['admtemp']);
				$('#term_temp').append('Term Template :: '+data['trmtemp']);

				if(data['fee_iscurrent']==1)
				{
					$('#is_curr').prop('checked', true);
				}
				else
				{
					$('#is_curr').prop('checked', false);
				}

				load_feetemp_amounts(data['fee_aftemp'],data['fee_tftemp']);
			}
		},	
		"json"
	);
}

function load_feetemp_amounts(aft,tft)
{
	$.post("<?php echo base_url('hci_fee_structure/load_feetemp_amounts')?>",{'aft':aft,'tft':tft},
		function(data)
		{	
			if(data == 'denied')
			{
        		funcres = {status:"denied", message:"You have no right to proceed the action"};
        		result_notification(funcres);
			}
			else
			{
				if(aft!=null)
				{
					if(data['afts'].length>0)
					{
						for (i = 0; i<data['afts'].length; i++) 
						{
							$('#amttd_'+data['afts'][i]['fsf_grade']+'_'+data['afts'][i]['fsf_fee']).empty();
							$('#amtnewtd_'+data['afts'][i]['fsf_grade']+'_'+data['afts'][i]['fsf_fee']).empty();
							$('#amtoldtd_'+data['afts'][i]['fsf_grade']+'_'+data['afts'][i]['fsf_fee']).empty();
							
							$('#amt_'+data['afts'][i]['fsf_grade']+'_'+data['afts'][i]['fsf_fee']).val(data['afts'][i]['fsf_amt']);
							$('#amttd_'+data['afts'][i]['fsf_grade']+'_'+data['afts'][i]['fsf_fee']).append(data['afts'][i]['fsf_amt']);
							if(data['afts'][i]['fsf_fee']==1)
							{
								$('#amtnew_'+data['afts'][i]['fsf_grade']+'_'+data['afts'][i]['fsf_fee']).val(data['afts'][i]['fsf_slabnew']);	
								$('#amtold_'+data['afts'][i]['fsf_grade']+'_'+data['afts'][i]['fsf_fee']).val(data['afts'][i]['fsf_slabold']);
								$('#amtnewtd_'+data['afts'][i]['fsf_grade']+'_'+data['afts'][i]['fsf_fee']).append(data['afts'][i]['fsf_slabnew']);
								$('#amtoldtd_'+data['afts'][i]['fsf_grade']+'_'+data['afts'][i]['fsf_fee']).append(data['afts'][i]['fsf_slabold']);
							}
						}
					}
					$('#Af_name').val($("#af_template option:selected").text());
				}

				if(tft!=null)
				{
					if(data['tfts'].length>0)
					{
						for (i = 0; i<data['tfts'].length; i++) 
						{
							$('#amttd_'+data['tfts'][i]['fsf_grade']+'_'+data['tfts'][i]['fsf_fee']).empty();
							$('#amtnewtd_'+data['tfts'][i]['fsf_grade']+'_'+data['tfts'][i]['fsf_fee']).empty();
							$('#amtoldtd_'+data['tfts'][i]['fsf_grade']+'_'+data['tfts'][i]['fsf_fee']).empty();
							
							$('#amt_'+data['tfts'][i]['fsf_grade']+'_'+data['tfts'][i]['fsf_fee']).val(data['tfts'][i]['fsf_amt']);
							$('#amttd_'+data['tfts'][i]['fsf_grade']+'_'+data['tfts'][i]['fsf_fee']).append(data['tfts'][i]['fsf_amt']);
							if(data['tfts'][i]['fsf_fee']==1)
							{
								$('#amtnew_'+data['tfts'][i]['fsf_grade']+'_'+data['tfts'][i]['fsf_fee']).val(data['tfts'][i]['fsf_slabnew']);	
								$('#amtold_'+data['tfts'][i]['fsf_grade']+'_'+data['tfts'][i]['fsf_fee']).val(data['tfts'][i]['fsf_slabold']);
								$('#amtnewtd_'+data['tfts'][i]['fsf_grade']+'_'+data['tfts'][i]['fsf_fee']).append(data['tfts'][i]['fsf_slabnew']);
								$('#amtoldtd_'+data['tfts'][i]['fsf_grade']+'_'+data['tfts'][i]['fsf_fee']).append(data['tfts'][i]['fsf_slabold']);
							}
						}
					}
					$('#Tf_name').val($("#tf_template option:selected").text());
				}
			}
		},	
		"json"
	);
}
</script>