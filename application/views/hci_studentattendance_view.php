<div class="row">
	<div class="col-md-12">
		<h3 class="page-header"><i class="fa fa-bank"></i> STUDENT SCHOOL ATTENDANCE </h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
			<li><i class="fa fa-calendar"></i>Attendance</li>
			<li><i class="fa fa-calendar"></i>School Attendance</li>
		</ol>
	</div>
</div>
<div>
	<section class="panel">
	    <header class="panel-heading"> 
	        <div class="row">
	        	<div class="col-md-6">
	        		Mark Student School Attendance 
	        	</div>
			</div>
	    </header>
        <form class="form-horizontal" role="form" method="post" id="attenform" autocomplete="off" novalidate>
      	<div class="panel-body" id="edit_view">
      		<div class="row">
  				<div class="form-group col-md-4">
                    <label for="fax" class="col-md-3 control-label">Academic Year</label>
                    <div class="col-md-9">
                        <select class="form-control select2" data-validation="required" data-validation-error-msg-required="Field cannot be empty" id="cls_academicyear" name="cls_academicyear"  style="width: 100%;" readonly>
                        <option value="all">---Select academic year---</option>
                            <?php
                            foreach($ay_info as $row):
                                if($row['ac_iscurryear']==1)
                                {
                                    $selected = 'selected';
                                }
                                else
                                {
                                    $selected = '';
                                }
                                ?>
                                <option value="<?php echo $row['es_ac_year_id'];?>" <?php echo $selected;?>>
                                    <?php echo $row['ac_startdate']." - ".$row['ac_enddate'];?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
              	<div class="form-group col-md-4">
                    <label for="attendate" class="col-md-3 control-label">Date</label>
                    <div class="col-md-9">
                        <div id="" class="input-group date" >
                            <input class="form-control datepicker" type="text" name="attendate" id="attendate"  data-format="YYYY-MM-DD" data-validation="required" data-validation-error-msg-required="Field can not be empty" value="">
                            <span class="input-group-addon"><span class="glyphicon-calendar "></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-3">
                    <label for="cls_branch" class="col-md-3 control-label">Branch</label>
                    <div class="col-md-9">
                        <?php 
                            global $branchdrop;
                            global $selectedbr;
                            $extraattrs = 'id="cls_branch" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty"';
                            echo form_dropdown('cls_branch',$branchdrop,$selectedbr, $extraattrs); 
                        ?>
                  </div>
                </div>
      		</div>
            <br>
            <div class="row">
                <div class="form-group col-md-4">
                    <label for="cls_grade" class="col-md-3 control-label">Grade</label>
                    <div class="col-md-9">
                        <select class="form-control" id="cls_grade" name="cls_grade" onchange="load_classlist(this.value)">
                            <option value="all">---Select Grade---</option>
                            <?php
                            $grd = $this->db->get('hci_grade')->result_array();
                            foreach($grd as $row):
                                $selected="";
                                // if($row['grd_id']==$this->hci_studentreg_model->show_val('pre_class')){
                                //     $selected="selected";
                                // }
                                ?>
                                <option value="<?php echo $row['grd_id'];?>"  <?php echo $selected?>>
                                   <?php echo $row['grd_name'];?>
                                </option>
                                
                                <?php
                            endforeach;
                        ?>
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <label for="classselect" class="col-md-3 control-label">Class</label>
                    <div class="col-md-9">
                        <select class="form-control" id="classselect" name="classselect">
                            <option value="all">---Select Class---</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                   <a class='btn btn-info btn-sm'  onclick='event.preventDefault();search_attendance()'>Search</a>
                </div>  
            </div>	
      		<br>
      		<div class="row">
      			<div class="col-md-12">
      				<table class="table table-bordered" id="stulist">
		          		<thead id='stulisthead'>
                            <tr>
                                <td>Student</td>
                                <td>Select Absentees<!-- <input id='selall' value='' type='checkbox' onclick="select_allchks()" class='form-control'> --></td>
                                <td>Remarks</td>
                            </tr>
		          		</thead>
		          		<tbody id="stulistbody">
                            <tr><td colspan='3'>No Outstanding Found</td></tr>
		          		</tbody>
		          	</table>
      			</div>
      		</div>
      	</div>
        <br>
        <div class="row">
            <div class="form-group">
                <div class="col-md-offset-1 col-md-11">
                    <br>
                    <button type="submit" class="btn btn-info" onclick="event.preventDefault();save_attendance()">Save</button> 
                    <button type="button" class="btn btn-default" onclick="event.preventDefault();reset_view()">Back</button> 
                </div>
            </div>
        </div>
        </form>
  	</section>
</div>
<script type="text/javascript" src="<?php echo base_url('js/moment.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/bootstrap-datetimepicker.min.js'); ?>"></script>
<script type="text/javascript">

$('#attendate').datetimepicker({ defaultDate: "<?php echo date('Y-m-d');?>",  pickTime: false});
$("#attendate").prop('disabled', true);

function load_classlist(id)
{
    acyear = $('#cls_academicyear').val();
    grade  = $('#cls_grade').val();
    branch = $('#cls_branch').val();

    $.post("<?php echo base_url('hci_attendance/load_classlist')?>",{'acyear':acyear,'grade':grade,'branch':branch},
        function(data)
        {
            $('#classselect').empty();
            if(data == 'denied')
            {
                funcres = {status:"denied", message:"You have no right to proceed the action"};
                result_notification(funcres);
            }
            else
            {
                $('#classselect').append("<option value=''></option>");
                if(data.length>0)
                {   
                    for (i = 0; i<data.length; i++) {
                       $('#classselect').append("<option value='"+data[i]['cls_id']+"'>"+data[i]['cls_code']+"</option>");
                    }
                }
            }
        },  
        "json"
    );
}

function search_attendance()
{
    acyear = $('#cls_academicyear').val();
    grade  = $('#cls_grade').val();
    date   = $('#attendate').val();
    clss   = $('#classselect').val();
    branch = $('#cls_branch').val();

    $('#stulistbody').empty();
    $.post("<?php echo base_url('hci_attendance/search_attendance')?>",{'acyear':acyear,'grade':grade,'date':date,'clss':clss,'branch':branch},
    function(data)
    {
        if(data == 'denied')
        {
            funcres = {status:"denied", message:"You have no right to proceed the action"};
            result_notification(funcres);
        }
        else
        {
            if(data.length>0)
            {
                for (i = 0; i<data.length; i++) 
                {
                    chcktxt = '';

                    if(data[i]['isabsent']==1)
                    {
                        chcktxt = 'checked';
                    }

                    $('#stulistbody').append("<tr><td>[ "+data[i]['st_id']+" ] - "+data[i]['family_name']+" "+data[i]['other_names']+"</td><td><input name='stuchk["+data[i]['clsstu_student']+"]' value='"+data[i]['clsstu_student']+"' class='stuchklist' type='checkbox' "+chcktxt+"></td><td><input name='remarks["+data[i]['clsstu_student']+"]' id='remarks_'"+data[i]['clsstu_student']+"' value='' type='text' class='form-control'><input name='atnstudent[]' value='"+data[i]['clsstu_student']+"' type='hidden' class='form-control'></td></tr>");
                }
            }
            else
            {
                $('#stulistbody').append("<tr><td colspan='3'>No Outstanding Found</td></tr>");
            }
        }
    },  
    "json"
    );
}

function select_allchks()
{
    if($("#selall").is(':checked'))
    {
        $('.stuchklist').attr('checked', true);
    }
    else
    {
        $('.stuchklist').attr('checked', false);
    }
}

function save_attendance()
{
    $.ajax(
    {
        url : "<?php echo base_url('hci_attendance/save_attendance')?>",
        type : 'POST',
        async : false,
        cache: false,
        dataType : 'json',
        data: $('#attenform').serialize() + "&attendate=" + $('#attendate').val(),
        success:function(data)
        {
            if(data == 'denied')
            {
                funcres = {status:"denied", message:"You have no right to proceed the action"};
                result_notification(funcres);
            }
            else
            {
                search_attendance();
                result_notification(data);
            }
        }
    });
}
</script>