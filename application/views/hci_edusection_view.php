<div class="row">
	<div class="col-md-12">
		<h3 class="page-header"><i class="fa fa-graduation-cap"></i>EDUCATIONAL SECTION</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
			<li><i class="fa fa-cog"></i>Educational Structure</li>
			<li><i class="fa fa-graduation-cap"></i>Educational Section</li>
		</ol>
	</div>
</div>
<div>
	<br>
	<div class="row">
		<div class="col-md-4">
		  	<section class="panel">
			    <header class="panel-heading">
			        Manage Educational Section
			    </header>
		      	<div class="panel-body">	
		          	<form class="form-horizontal" role="form" method="post" action="<?php echo base_url('hci_edustructure/save_edusection')?>" id="sectionform" autocomplete="off" novalidate>
		              	<div class="form-group">
		              		<input type="hidden" id="sec_id" name="sec_id">
		                  	<label for="sec_name" class="col-md-4 control-label">Section</label>
		                  	<div class="col-md-6">
		                      	<input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="sec_name" name="sec_name" placeholder="">
		                  	</div>
		              	</div>
		              	<div class="form-group">
		                  	<div class="col-md-offset-2 col-md-11">
		                      	<button type="submit" class="btn btn-info">Save</button> 
		                      	<button type="reset" class="btn btn-default">Reset</button>
		                  	</div>
		              	</div>
		          	</form>
		      	</div>
		  	</section>
	  	</div>
	  	<div class="col-md-8">
		  	<section class="panel">
			    <header class="panel-heading">
			       Look Up
			    </header>
		      	<div class="panel-body">	
		          	<table class="display" id="section_tbl">
		          		<thead>
		          			<tr>
		          				<th>#</th>
		          				<th>Section</th>
		          				<th>Actions</th>
		          			</tr>
		          		</thead>
		          		<tbody>
	          				<?php
	          					$i=1;
	          					if(!empty($sections))
	          					{
	          						foreach ($sections as $section) 
		          					{
		          						echo "<tr>";
		          						echo "<td>".$i."</td>";
		          						echo "<td>".$section['sec_name']."</td>";
		          						echo "<td><a class='btn btn-info btn-xs' onclick='event.preventDefault();edit_section_load(".$section['sec_id'].",\"".$section['sec_name']."\")'><span class='glyphicon glyphicon-pencil' aria-hidden='true'></span></a>| ";

		          	// 					if($subgrp["desig_isused"]==0)
								    	// {
		          	// 						echo "<a class='btn btn-danger btn-xs' onclick='event.preventDefault();delete_group_load
		          	// 					     (".$subgrp['desig_id'].")'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></a>| ";
		          	// 					}

								    	if($section["sec_status"]=="A")
								    	{
								    		echo "<button onclick='event.preventDefault();change_status(".$section["sec_id"].",\"D\")' class='btn btn-warning btn-xs'><span class='glyphicon glyphicon-ban-circle' aria-hidden='true'></span></button>";
								    	}
								    	else
								    	{
								    		echo "<button onclick='event.preventDefault();change_status(".$section["sec_id"].",\"A\")' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-ok-circle' aria-hidden='true'></span></button>";
								    	}
								    	
		          						echo "</td></tr>";

		          						$i++;
		          					}
	          					}
	          				?>
		          		</tbody>
		          	</table>
		      	</div>
		  	</section>
	  	</div>
	</div>
</div>
<script type="text/javascript">

$.validate({
   	form : '#sectionform'
});

$(document).ready(function() {
    $('#section_tbl').DataTable();
} );

function edit_section_load(id,name)
{
	$('#sec_id').val(id);
	$('#sec_name').val(name);
}

// function delete_group_load(id)
// {	
// 	$.post("<?php echo base_url('hci_edustructure/remove_dept')?>",{"grd_id":id},
// 	function(data)
// 	{	
// 		location.reload();
// 	},	
// 	"json"
// 	);
// }

function change_status(id,new_s)
{	
	$.post("<?php echo base_url('hci_edustructure/change_sectionstatus')?>",{"sec_id":id,"new_s":new_s},
	function(data)
	{	
		if(data == 'denied')
		{
    		funcres = {status:"denied", message:"You have no right to proceed the action"};
    		result_notification(funcres);
		}
		else
		{
			location.reload();
		}
	},	
	"json"
	);
}

</script>
