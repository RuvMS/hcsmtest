<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/jquery-confirm.css'); ?>"> 
<script type="text/javascript" src="<?php echo base_url('js/jquery-confirm.js')?>"></script><!--jquery-->
<div class="row">
    <div class="col-md-12">
        <h3 class="page-header"><i class="fa fa-users"></i> STUDENT INFORMATION</h3>
        <ol class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="<?php echo base_url('dashboard')?>">Home</a></li>
            <li><i class="fa fa-cog"></i>Settings</li>
            <li><i class="fa fa-bank"></i>Student Edit</li>
        </ol>
    </div>
</div>
<div>
    <form class="form-horizontal" role="form" method="post"  id="reg_form" action="<?php echo base_url('hci_student/update_student_info')?>"  autocomplete="off" novalidate>
    <section class="panel" id="grd_panel">
        <br>
        <div class="panel-body">
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Year/Grade</label>
                <div class="col-md-5">
                    <select class="form-control select2" id="st_grade" name="st_grade"  style="width: 100%;">
                        <?php
                        $grade = $this->db->get('hci_grade')->result_array();
                        foreach($grade as $row):
                            $selected="";
                            if($row['grd_id']==$stu_data['stu_data']['st_grade']){
                                $selected="selected";
                            }
                            ?>
                            <option value="<?php echo $row['grd_id'];?>"  <?php echo $selected?>>
                                <?php echo $row['grd_name'];?>
                            </option>
                            <?php
                        endforeach;
                        ?>
                    </select>
                </div>
            </div>
            <br>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Accademic Year</label>
                <div class="col-md-5">
                    <select class="form-control select2" data-validation="required" data-validation-error-msg-required="Field cannot be empty" id="acc_year" name="post[st_accyear]"  style="width: 100%;">
                    <option value="">---Select Grade---</option>
                        <?php
                        foreach($acc_years as $row):
                            if($row['ac_iscurryear']==$stu_data['stu_data']['st_accyear'])
                            {
                                $selected = 'selected';
                            }
                            ?>
                            <option value="<?php echo $row['es_ac_year_id'];?>" <?php echo $selected;?>>
                                <?php echo $row['ac_startdate']." - ".$row['ac_enddate'];?>
                            </option>
                            <?php
                        endforeach;
                        ?>
                    </select>
                </div>
            </div>
        </div>
    </section>
    <section class="panel" id="stuinfo_panel">
        <header class="panel-heading">
            Student Information
        </header>
        <div class="panel-body">
            <div class="form-group">
                <label for="name" class="col-md-2 control-label">Family Name</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" data-validation="required" data-validation-error-msg-required="Field can not be empty" id="fname" name="family_name" placeholder="" value="<?php echo $stu_data['stu_data']['family_name']  ?>">
                </div>
            </div>
            <input type="hidden" id="id" name="id" value="<?php echo $stu_data['stu_data']['id'];?>">
            <input type="hidden" id="ref_t" name="ref_t" value="<?php echo $_GET['rt']?>">
            <div class="form-group">
                <label for="brnum" class="col-md-2 control-label">Other Names</label>
                <div class="col-md-8">
                    <input type="text" data-validation="required" data-validation-error-msg-required="field cannot be empty" class="form-control" id="oname" name="other_names" placeholder="" value="<?php echo $stu_data['stu_data']['other_names']  ?>">
                </div>
            </div>
            <?php
            $ss = $stu_data['stu_data']['st_gender'];
            $selected_gender1 =  'unchecked';
            $selected_gender2 =  'unchecked';
            if($ss == 1)
            {
                $selected_gender1 =  'checked="checked"'; 
            }elseif($ss == 2)
            {
                $selected_gender2 =  'checked="checked"';
            }
            ?>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="comcode" class="col-sm-5 control-label">Gender </label>
                        <div class="col-sm-7">
                            <label class="col-md-3 control-label">Male</label>
                            <input type="radio" data-validation="required" name="st_gender" class="col-md-1" id="gender" value="1" <?php echo $selected_gender1 ?>>

                            <label class="col-md-3 control-label">Female</label>
                            <input type="radio" data-validation="required" name="st_gender" id="gender" class="col-md-1" value="2" <?php echo $selected_gender2 ?>>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Date Of Birth</label>
                        <div class="col-sm-6">
                            <div id="" class="input-group date" >
                                <input class="form-control datepicker" data-validation="required" data-validation-error-msg-required="Field cannot be empty" type="text" name="dob" id="birthday"  data-format="YYYY-MM-DD" value="<?php echo $stu_data['stu_data']['dob']?>">
                                    <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="comcode" class="col-sm-5 control-label">Nationality </label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="local_nationality" name="local_nationality" placeholder=""  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['local_nationality']?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <label for="st_religion" class="col-md-3 control-label">Religion</label>
                        <div class="col-md-6">
                            <select class="form-control" data-validation="required" data-validation-error-msg-required="field cannot be empty" id="st_religion" name="st_religion"  style="width: 100%;">
                                <option value=""></option>
                                <?php
                                    $rlgn = $this->db->get('hgc_religion')->result_array();
                                    foreach($rlgn as $row):
                                        $selected="";
                                        if($row['rel_id']==$stu_data['stu_data']['st_religion']){
                                            $selected="selected";
                                        }
                                        ?>
                                        <option value="<?php echo $row['rel_id'];?>"  <?php echo $selected?>>
                                            <?php echo $row['rel_name'];?>
                                        </option>
                                        <?php
                                    endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="panel" id="foreign_panel">
        <header class="panel-heading">
            For Foreign Students
        </header>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="comcode" class="col-sm-5 control-label">Citizenship </label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="citizenship" name="citizenship" placeholder=""   data-validation-optional="true" value="<?php echo $stu_data['stu_data']['citizenship']?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Nationality </label>
                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="foreign_nationality" name="foreign_nationality" placeholder=""   data-validation-optional="true" value="<?php echo $stu_data['stu_data']['foreign_nationality']?>">

                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="comcode" class="col-sm-5 control-label">Passport Number</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="passport" name="passport" placeholder=""  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['passport']?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Place/Date Of Issue</label>
                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="issue" name="issue" placeholder=""  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['issue']?>">

                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="comcode" class="col-sm-5 control-label">First Language</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="language" name="language" placeholder="" data-validation-optional="true" value="<?php echo $stu_data['stu_data']['language']?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Language spoken at home</label>
                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="lang_home" name="lang_home" placeholder="" data-validation-optional="true" value="<?php echo $stu_data['stu_data']['lang_home']?>">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="panel" id="personal_panel">
        <header class="panel-heading">
            Personal Data : Family
        </header>
        <div class="panel-body">
            <div class="form-group">
                <label for="brnum" class="col-md-2 control-label">Student Siblings</label>
                <div class="col-md-8">

                        <select class="form-control select2" value="" onchange="load_sibling_data('sibs')" name="es_sibiling[]" multiple style="width: 100%;" id="sib_select">

                            <?php
                            // $students = $this->db->get('hci_preadmission')->result_array();
                            foreach($reg_stu as $row):
                                ?>
                                <option value="<?php echo $row['id'] ?>">
                                    <?php echo '[ '.$row['st_id'].' ] '.$row['other_names'];?>
                                </option>
                                <?php
                            endforeach;
                            ?>

                        </select>
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Mother's Name</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="mname" name="post[mom_name]" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true"  value="<?php echo $stu_data['stu_data']['mom_name']  ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Address</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="maddress1" name="post[mom_addy]" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty" onkeyup="$('#faddress1').val(this.value)" data-validation-optional="true" value="<?php echo $stu_data['stu_data']['mom_addy']  ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label"></label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="maddress2" name="post[mom_add2]" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty"  onkeyup="$('#faddress2').val(this.value)"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['mom_add2']  ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label"></label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="maddress3" name="post[mom_addcity]" placeholder="City" onkeyup="$('#faddress3').val(this.value)" data-validation-optional="true" value="<?php echo $stu_data['stu_data']['mom_addcity']  ?>">
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="comcode" class="col-sm-5 control-label">Home Number</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" data-validation="number length" data-validation-length="10-10" data-validation-error-msg-number="Invalid. Please Try Again. ex: 0111234567" data-validation-error-msg-length="Must be 10 characters long" id="mhome" name="post[mom_home]" onkeyup="$('#fhome').val(this.value)" placeholder=""  data-validation-optional="true"  value="<?php echo $stu_data['stu_data']['mom_home']  ?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Mobile Number</label>
                        <div class="col-sm-6">

                            <input type="text" class="form-control" data-validation="number length" data-validation-length="10-10" data-validation-error-msg-number="Invalid. Please Try Again. ex: 0111234567" data-validation-error-msg-length="Must be 10 characters long" id="mom_mobile" name="post[mom_mobile]" placeholder=""  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['mom_mobile']  ?>">

                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Email Address</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="memail" name="post[mom_email]" placeholder="" data-validation="email" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['mom_email']  ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Father's Name</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="fathername" name="post[dad_name]" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['dad_name']  ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Address</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="faddress1" name="post[dad_addy]" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['dad_addy']  ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label"></label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="faddress2" name="post[dad_add2]" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['dad_add2']  ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label"></label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="faddress3" name="post[dad_addcity]" placeholder="City" data-validation="required" data-validation-error-msg-required="Field can not be empty"   data-validation-optional="true" value="<?php echo $stu_data['stu_data']['dad_addcity']  ?>">
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="comcode" class="col-sm-5 control-label">Home Number </label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" data-validation="number length" data-validation-length="10-10" data-validation-error-msg-number="Invalid. Please Try Again. ex: 0111234567" data-validation-error-msg-length="Must be 10 characters long" id="fhome" name="post[dad_home]" placeholder=""  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['dad_home']  ?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Mobile Number </label>
                        <div class="col-sm-6">

                            <input type="text" class="form-control" data-validation="number length" data-validation-length="10-10" data-validation-error-msg-number="Invalid. Please Try Again. ex: 0111234567" data-validation-error-msg-length="Must be 10 characters long" id="dad_mobile" name="post[dad_mobile]" placeholder=""  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['dad_mobile']  ?>">

                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Email Address</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="dad_email" name="post[dad_email]" placeholder="" data-validation-optional="true" value="<?php echo $stu_data['stu_data']['dad_email']  ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Name Of Guardian</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="guar_name" name="post[guar_name]" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['guar_name']  ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Address</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="guar_addy" name="post[guar_addy]" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['guar_addy']  ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label"></label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="guar_add2" name="post[guar_add2]" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['guar_add2']  ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label"></label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="guar_addcity" name="post[guar_addcity]" placeholder="City" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['guar_addcity']  ?>">
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="comcode" class="col-sm-5 control-label">Home Number </label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" data-validation="number length" data-validation-length="10-10" data-validation-error-msg-number="Invalid. Please Try Again. ex: 0111234567" data-validation-error-msg-length="Must be 10 characters long" id="guar_home" name="post[guar_home]" placeholder="" data-validation="number length"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['guar_home']  ?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Mobile Number </label>
                        <div class="col-sm-6">

                            <input type="text" class="form-control" data-validation="number length" data-validation-length="10-10" data-validation-error-msg-number="Invalid. Please Try Again. ex: 0111234567" data-validation-error-msg-length="Must be 10 characters long" id="guar_mobile" name="post[guar_mobile]" placeholder=""  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['guar_mobile']  ?>">

                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Email Address</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="guar_email" name="post[guar_email]" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['guar_email']  ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-5 control-label">Student Information should be communicated with  </label>
                <div class="col-md-6">
                    <div class="col-sm-7">
                        <?php
                        $sel_txt_cp1 =  'unchecked';
                        $sel_txt_cp2 =  'unchecked';
                        $sel_txt_cp3 =  'unchecked';
                        if($stu_data['stu_data']['st_infComPerson'] == 1)
                        {
                            $sel_txt_cp1 =  'checked="checked"';
                        }
                        elseif($stu_data['stu_data']['st_infComPerson'] == 2)
                        {
                            $sel_txt_cp2 =  'checked="checked"';
                        }
                        elseif($stu_data['stu_data']['st_infComPerson'] == 3)
                        {
                            $sel_txt_cp3 =  'checked="checked"';
                        }
                        ?>
                        <label class="col-md-3 control-label">Father</label>
                        <input type="radio" data-validation="required" name="post[st_infComPerson]" class="col-md-1" id="m" value="1" <?=$sel_txt_cp1?>>

                        <label class="col-md-3 control-label">Mother</label>
                        <input type="radio" data-validation="required" name="post[st_infComPerson]" id="m" class="col-md-1 "  value="2" <?=$sel_txt_cp2?>>

                        <label class="col-md-3 control-label">Guardian</label>

                        <input type="radio" data-validation="required" name="post[st_infComPerson]" id="m" class="col-md-1 "  value="3" <?=$sel_txt_cp3?>>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="panel" id="emergency_panel">
        <header class="panel-heading">
            Emergency Contact ( Other Than Parent/ Guardian)
        </header>
        <div class="panel-body">
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Name</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="pname" name="emg_name" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['emg_name']?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Address</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="paddress1" name="emg_addy" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['emg_addy']?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label"></label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="paddress2" name="emg_add2" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['emg_add2']?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label"></label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="paddress3" name="emg_addcity" placeholder="City" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['emg_addcity']?>">
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="comcode" class="col-sm-5 control-label">Home Number </label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="emg_home" name="emg_home" placeholder=""  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['emg_home']?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Mobile Number </label>
                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="emg_mobile" name="emg_mobile" placeholder=""  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['emg_mobile']?>">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="panel" id="history_panel">
        <header class="panel-heading">
            School History - Student
        </header>
        <div class="panel-body">
            <div class="form-group">
                <label for="fax" class="col-md-4 control-label">Age which the child started formal schooling</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="paddress3" name="school_age" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['school_age']?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label bold " style=" padding-left: 264px;color: crimson; ">Name/Address of the recent school attended.</label>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Name</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="pname" name="1sch_name" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['1sch_name']?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Address</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="paddress1" name="1sch_addy" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['1sch_addy']?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label"></label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="paddress2" name="1sch_add2" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['1sch_add2']?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label"></label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="paddress3" name="1sch_addcity" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['1sch_addcity']?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Language of Instruction</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="paddress3" name="1sch_lang" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['1sch_lang']?>">
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="comcode" class="col-sm-5 control-label">From</label>
                        <div class="col-sm-7">
                            <div id="" class="input-group date" >
                                <input class="form-control datepicker" type="text" name="ffrom" id="birthday"  data-format="YYYY-MM-DD" value="<?php echo $stu_data['stu_data']['ffrom']?>">
                                    <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">To</label>
                        <div class="col-sm-6">

                            <div id="" class="input-group date" >
                                <input class="form-control datepicker" type="text" name="fto" id="birthday"  data-format="YYYY-MM-DD" value="<?php echo $stu_data['stu_data']['fto']?>">
                                    <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span>
                                </span>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Current Year/Grade</label>
                <div class="col-md-5">
                    <select class="form-control select2" name="st_curgrade"  style="width: 100%;">
                    <?php
                    $grade = $this->db->get('hci_grade')->result_array();
                    foreach($grade as $row):
                        $selected="";
                        if($row['grd_id']==$stu_data['stu_data']['st_curgrade']){
                            $selected="selected";
                        }
                        ?>
                        <option value="<?php echo $row['grd_id'];?>"  <?php echo $selected?>>
                            <?php echo $row['grd_name'];?>
                        </option>
                        <?php
                    endforeach;
                    ?>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-5 control-label">Was your child withdraw from any school on request from its Management</label>
                <div class="col-md-5">
                    <div class="col-sm-7">
                        <?php
                        $sel_txt_mw1 =  'unchecked';
                        $sel_txt_mw2 =  'unchecked';
                        if($stu_data['stu_data']['mgt_wd'] == '-')
                        {
                            $sel_txt_mw1 =  'checked="checked"';
                        }
                        elseif($stu_data['stu_data']['mgt_wd'] == 'on')
                        {
                            $sel_txt_mw2 =  'checked="checked"';
                        }
                        ?>
                        <label class="col-md-3 control-label">No</label>
                        <input type="radio" name="mgt_wd" class="col-md-1" id="ss" value="-" <?php echo $sel_txt_mw1?>>

                        <label class="col-md-3 control-label">Yes</label>
                        <input type="radio" name="mgt_wd" id="ss" class="col-md-1"  value="on" <?php echo $sel_txt_mw2?>>
                    </div>
                </div>                   
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">If yes , Please explain</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="mgt_exp" name="mgt_exp" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['mgt_exp']?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-4 control-label">Please list special interests of school activities in which the child is/has been involved</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="int_act" name="int_act" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['int_act']?>">
                </div>
            </div>
        </div>
    </section>
    <section class="panel" id="extra_panel">
        <div class="panel-body">
            <br>
            <div class="form-group">
                <label for="fax" class="col-md-5 control-label">If english is not the child's first language /mother tongue,Has the child had instruction in or experience of English  </label>
                <div class="col-md-6">
                    <div class="col-sm-7">
                        <?php
                        $sel_txt_ee1 =  'unchecked';
                        $sel_txt_ee2 =  'unchecked';
                        if($stu_data['stu_data']['eng_xp'] == '-')
                        {
                            $sel_txt_ee1 =  'checked="checked"';
                        }
                        elseif($stu_data['stu_data']['eng_xp'] == 'on')
                        {
                            $sel_txt_ee2 =  'checked="checked"';
                        }
                        ?>
                        <label class="col-md-3 control-label">No</label>
                        <input type="radio" name="eng_xp" class="col-md-1" id="m" value="-" <?php echo $sel_txt_ee1?>>

                        <label class="col-md-3 control-label">Yes</label>
                        <input type="radio" name="eng_xp" id="m" class="col-md-1 "  value="on" <?php echo $sel_txt_ee2?>>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">If yes , in what situation ?</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="eng_sit" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty" name="eng_sit"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['eng_sit']?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Far how long</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="eng_len" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty" name="eng_len"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['eng_len']?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-5 control-label">Has your child been enrolled in any type special Education Programme  </label>
                <div class="col-md-6">
                    <div class="col-sm-7">
                        <?php
                        $sel_txt_sep1 =  'unchecked';
                        $sel_txt_sep2 =  'unchecked';
                        if($stu_data['stu_data']['enr_sped'] == '-')
                        {
                            $sel_txt_sep1 =  'checked="checked"';
                        }
                        elseif($stu_data['stu_data']['enr_sped'] == 'on')
                        {
                            $sel_txt_sep2 =  'checked="checked"';
                        }
                        ?>
                        <label class="col-md-3 control-label">No</label>
                        <input type="radio" name="enr_sped" class="col-md-1" id="g" value="-" <?php echo $sel_txt_sep1?>>

                        <label class="col-md-3 control-label">Yes</label>
                        <input type="radio" name="enr_sped" id="g" class="col-md-1 "  value="on" <?php echo $sel_txt_sep2?>>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">If yes , Please explain</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="sped_xp" placeholder="" name="sped_xp" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['sped_xp']?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-5 control-label">Has your child been tested by a learning Specialist or Psychologist   </label>
                <div class="col-md-6">
                    <div class="col-sm-7">
                        <?php
                        $sel_txt_mt1 =  'unchecked';
                        $sel_txt_mt2 =  'unchecked';
                        if($stu_data['stu_data']['med_test'] == '-')
                        {
                            $sel_txt_mt1 =  'checked="checked"';
                        }
                        elseif($stu_data['stu_data']['med_test'] == 'on')
                        {
                            $sel_txt_mt2 =  'checked="checked"';
                        }
                        ?>
                        <label class="col-md-3 control-label">No</label>
                        <input type="radio" name="med_test" class="col-md-1" id="p" value="-" <?php echo $sel_txt_mt1?>>

                        <label class="col-md-3 control-label">Yes</label>
                        <input type="radio" name="med_test" id="p" class="col-md-1 "  value="on" <?php echo $sel_txt_mt2?>>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">If yes , Please explain</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty" name="med_xp"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['med_xp']?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-5 control-label">Does the child have physical or medical disability </label>
                <div class="col-md-6">
                    <div class="col-sm-7">
                        <?php
                        $sel_txt_pmd1 =  'unchecked';
                        $sel_txt_pmd2 =  'unchecked';
                        if($stu_data['stu_data']['med_dis'] == '-')
                        {
                            $sel_txt_pmd1 =  'checked="checked"';
                        }
                        elseif($stu_data['stu_data']['med_dis'] == 'on')
                        {
                            $sel_txt_pmd2 =  'checked="checked"';
                        }
                        ?>
                        <label class="col-md-3 control-label">No</label>
                        <input type="radio" name="med_dis" class="col-md-1" id="d" value="-" <?php echo $sel_txt_pmd1?>>

                        <label class="col-md-3 control-label">Yes</label>
                        <input type="radio" name="med_dis" id="d" class="col-md-1 "  value="on" <?php echo $sel_txt_pmd2?>>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">If yes , Please explain</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty" name="dis_xp"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['dis_xp']?>">
                </div>
            </div>
        </div>
    </section>
    <section class="panel" id="busiinfof_panel">
        <header class="panel-heading">
            Employment Data - Father
        </header>
        <div class="panel-body">
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Business Affiliation</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="dad_aff" name="dad_aff" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['dad_aff']?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Position</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="dad_pos" name="dad_pos" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['dad_pos']?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Business Address</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="dad_biz_addy" name="dad_biz_addy" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['dad_biz_addy']?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Telephone</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="dad_biz_tel" name="dad_biz_tel" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['dad_biz_tel']?>">
                </div>
            </div>
        </div>
    </section>
    <section class="panel" >
        <header class="panel-heading">
            Employment Data - Mother
        </header>
        <div class="panel-body">
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Business Affiliation</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="mom_aff" name="mom_aff" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['mom_aff']?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Position</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="mom_pos" name="mom_pos" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['mom_pos']?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Business Address</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="mom_biz_addy" name="mom_biz_addy" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['mom_biz_addy']?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Telephone</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="mom_biz_tel" name="mom_biz_tel" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['mom_biz_tel']?>">
                </div>
            </div>
        </div>
    </section>
    <section class="panel">
        <header class="panel-heading">
            Admission Data
        </header>
        <div class="panel-body">
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Expected date of Enrollment</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="" name="ex_date" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['ex_date']?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Expected Length Of Stay</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="" name="len_stay" placeholder="" data-validation="required" data-validation-error-msg-required="Field can not be empty"  data-validation-optional="true" value="<?php echo $stu_data['stu_data']['len_stay']?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Curriculum</label>
                <div class="col-md-5"><!-- <?php echo $selected?> -->
                    <select class="form-control select2" name="curriculum"  style="width: 100%;">
                        <?php
                            $sel_curriculum1="";
                            $sel_curriculum2="";
                            if($stu_data['stu_data']['curriculum']=='UK')
                            {
                                $sel_curriculum1="selected";
                            }
                            else if($stu_data['stu_data']['curriculum']=='Local')
                            {
                                $sel_curriculum2="selected";
                            }
                        ?>
                        <option value=""></option>
                        <option value="UK" <?php echo $sel_curriculum1?>>UK</option>
                        <option value="Local" <?php echo $sel_curriculum2?>>Local</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Would you like to send clild by school bus ? </label>
                <div class="col-md-6">
                    <div class="col-sm-7">
                        <?php
                        $sel_txt_tran1 =  'unchecked';
                        $sel_txt_tran2 =  'unchecked';
                        if($stu_data['stu_data']['exclude_transport'] == '1')
                        {
                            $sel_txt_tran1 =  'checked="checked"';
                        }
                        elseif($stu_data['stu_data']['exclude_transport'] == '0')
                        {
                            $sel_txt_tran2 =  'checked="checked"';
                        }
                        ?>
                        <label class="col-md-3 control-label">No</label>
                        <input type="radio" name="exclude_transport" class="col-md-1" id="exclude_transport" value="1" <?php echo $sel_txt_tran1?>>

                        <label class="col-md-3 control-label">Yes</label>
                        <input type="radio" name="exclude_transport" id="exclude_transport2" class="col-md-1 "  value="0" <?php echo $sel_txt_tran2?>>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="panel">
        <header class="panel-heading">
            Payment Information
        </header>
        <div class="panel-body">
            <div class="form-group">
                <label for="curr_fs" class="col-md-2 control-label">Fee Structure </label>
                <div class="col-md-6">
                    <select type="text" class="form-control" data-validation="required" data-validation-error-msg-required="select fee structure" id="curr_fs" name="post[st_feestructure]"  onchange="load_sibling_data('fss')">
                        <option value="">---Select Fee Structure---</option>
                        <?php
                            foreach ($fss as $fs) 
                            {
                                $seltxt = '';

                                if($stu_data['stu_data']['st_feestructure']==null)
                                {
                                    if($fs['fee_iscurrent']==1)
                                    {
                                        $seltxt = 'Selected';
                                    }
                                }
                                else if($stu_data['stu_data']['st_feestructure']==$fs['es_feemasterid'])
                                {
                                    $seltxt = 'Selected';
                                }

                                echo "<option value='".$fs['es_feemasterid']."' ".$seltxt.">".$fs['fee_description']." ( ".$fs['admtemp']." , ".$fs['trmtemp']." )</option>";
                            }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Preferred Admission Payment method ? </label>
                <div class="col-md-6">
                    <div class="col-sm-7">
                        <?php
                        $sel_txt_apm1 =  'unchecked';
                        $sel_txt_apm2 =  'unchecked';
                        if($stu_data['stu_data']['st_addpaymethod'] == '1')
                        {
                            $sel_txt_apm1 =  'checked="checked"';
                        }
                        elseif($stu_data['stu_data']['st_addpaymethod'] == '0')
                        {
                            $sel_txt_apm2 =  'checked="checked"';
                        }
                        ?>
                        <label class="col-md-3 control-label">Total Payment</label>
                        <input type="radio" name="post[st_addpaymethod]" class="col-md-1" value="1" checked="true" <?=$sel_txt_apm1?>>

                        <label class="col-md-3 control-label">Slabwise Payments</label>
                        <input type="radio" name="post[st_addpaymethod]" class="col-md-1 "  value="2" <?=$sel_txt_apm2?>>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="fax" class="col-md-2 control-label">Applicable Discounts</label>
                <div class="col-md-6" id="discounts_list">
                    
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <div class="col-sm-12">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th></th>
                                <?php
                                    foreach ($fees as $fee) 
                                    {
                                        echo "<th>".$fee['fc_name']."</th>";
                                    }
                                ?>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Payment Scheme</td>
                                    <?php
                                        foreach ($fees as $fee) 
                                        {
                                            echo "<td>";
                                            if(!empty($fee['plans']))
                                            {
                                                echo "<select id='pplan_".$fee['fc_id']."' name='pplan[]' class='form-control'
                                                data-validation='required' data-validation-error-msg-required='field cannot be empty'>";
                                                echo "<option></option>";
                                                foreach ($fee['plans'] as $pln) 
                                                {
                                                    $selected = '';

                                                    foreach ($stu_data['payplans'] as $selpln) {
                                                        if($selpln['spp_paymentplan']==$pln['plan_id'])
                                                        {
                                                            $selected = 'selected';
                                                        }
                                                    }
                                                   
                                                    echo "<option value='".$pln['plan_id']."' ".$selected.">".$pln['plan_name']."</option>";
                                                    
                                                }
                                                echo "</select>";
                                            }
                                            else
                                            {
                                                echo "<select id='pplan_".$fee['fc_id']."' name='pplan[]' class='form-control'>";
                                                echo "<option></option>";
                                                echo "</select>";
                                            }
                                            
                                            echo "</td>";
                                        }
                                    ?>
                                </tr>
                                <tr>
                                    <td>Discount (%)</td>
                                <?php
                                    foreach ($fees as $fee) 
                                    {
                                        echo "<td>";
                                        echo "<input type='text' name='discinp[".$fee['fc_id']."]' id='discinp_".$fee['fc_id']."' class='form-control'>";
                                        echo "</td>";
                                    }
                                ?>
                                </tr>
                                <tr>
                                    <td>Apply For</td>
                                <?php
                                    foreach ($fees as $fee) 
                                    {
                                        echo "<td>";
                                        echo "<select type='text' class='form-control' id='adj_appfor_".$fee['fc_id']."' name='adj_appfor[".$fee['fc_id']."]'><option value=''></option><option value='1'>Total Amount</option><option value='2'>Individual Term</option><option value='3'>Admission : Slab</option><option value='4'>Installment</option></select>";
                                        echo "</td>";
                                    }
                                ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="panel">
        <div class="panel-body">
            <br>
            <div class="form-group">
                <div class="col-md-offset-2 col-md-11">
                    <button type="submit" name="save_btn" id="save_btn" class="btn btn-info">Save Changes</button>
                    <button type="submit" onclick="event.preventDefault();confirm_regi()" name="regi_btn" id="regi_btn" class="btn btn-info">Register</button>
                    <button onclick="event.preventDefault();$('#reg_form').trigger('reset');$('#id');$('#ref_t').val('');" class="btn btn-default">Reset</button>
                </div>
            </div>
        </div>
    </section>
    </form>
</div>

<script type="text/javascript" src="<?php echo base_url('js/moment.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/bootstrap-datetimepicker.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/select2/select2.full.min.js'); ?>"></script>

<link rel="stylesheet" href="<?php echo base_url('assets/select2/select2.min.css') ?>">
<script src='<?php echo base_url("assets/datepicker/bootstrap-datepicker.js")?>'></script>
<link rel="stylesheet" href="<?php echo base_url('assets/datepicker/datepicker3.css')?>">

<script type="text/javascript">

function confirm_regi() 
{
    $.confirm({
        title: 'Register Student',
        content: 'Are you sure you want to register the student? ',
        confirm: function(){
            $("#reg_form").append("<input type='hidden' name='btn_type' value='regi' />");
            $("#reg_form" ).submit();
        },
        cancel: function(){
            
        }
    });
    
}

$.validate({
    form : '#reg_form'
});


$(document).ready(function(){
    $( "#sibling" ).autocomplete({
        source:"<?php echo base_url("hci_studentreg/new_studentreg/search/");?>"
    });
});

$(document).ready(function(){
    $( "#sibling" ).autocomplete({
        source:"<?php echo base_url("hci_studentreg/new_studentreg/search/");?>"
    });

    temp_ary = JSON.parse('<?php echo JSON_encode($stu_data['siblings']);?>');

    $("#sib_select").val(temp_ary).trigger("change");

    reftable = '<?php echo $_GET['rt']?>';

    if(reftable=='d')
    {
        // $("#st_grade").attr('disabled','disabled');
        // $("#sib_select").prop("disabled", true);
        // $("#curr_fs").attr('disabled','disabled');
        // $(".discchk").attr('disabled','disabled');
        // $(".typerdo").attr('disabled','disabled');
    }
});

$(function () {
    //Initialize Select2 Elements
    $("#sib_select").select2();
});

$('.datepicker').datepicker({
    autoclose: true
});

event_count = 0;

function load_sibling_data(type)
{
    sibs = $('#sib_select').val();
    fs   = $('#curr_fs').val();
    stu  = '<?php echo $stu_data['stu_data']['id'];?>';

    $.post("<?php echo base_url('hci_studentreg/load_sibling_data')?>",{'sibs':sibs,'fs':fs,'type':type,'stu':stu,'isedit':true},
    function(data)
    { 
        if(data == 'denied')
        {
            funcres = {status:"denied", message:"You have no right to proceed the action"};
            result_notification(funcres);
        }
        else
        {
            if(type=='sibs')
            {
                if(sibs==null)
                {
                    event_count = 0;
                }
                else
                {
                    if(event_count==0)
                    {
                        if(data['siblings'] != null)
                        {
                            $("#sib_select").val(data['siblings']).trigger("change");
                            event_count++;
                        }
                    }
                }

            }

            if(data['discounts'] != null)
            {
                $('#discounts_list').empty();

                for (i = 0; i<data['discounts'].length; i++) {

                    if(data['discounts'][i]['adj_isspec']==1)
                    {
                        $('#discinp_'+data['discounts'][i]['adj_feecat']).val(data['discounts'][i]['adj_amount']);
                        $('#adj_appfor_'+data['discounts'][i]['adj_feecat']).val(data['discounts'][i]['adj_applyfor']);
                    }
                    else
                    {
                        if(data['discounts'][i]['selected']==1)
                        {
                            checktext = 'checked="checked"';
                        }
                        else
                        {
                            checktext = '';
                        }

                        $('#discounts_list').append('<label class="label_check c_on" for="checkbox-01"><input name="discountschk[]" id="disc_'+data['discounts'][i]['adj_id']+'" value="'+data['discounts'][i]['adj_id']+'" type="checkbox" '+checktext+'> '+data['discounts'][i]['adj_description']+'</label><br>');
                    }

                    
                }                         
            }
        }
    },  
    "json"
    );
}


    // function load_sibling_data(type)
    // {
    //     sibs = $('#sib_select').val();
    //     fs   = $('#curr_fs').val();

    //     $.post("<?php echo base_url('hci_studentreg/load_sibling_data')?>",{'sibs':sibs,'fs':fs,'type':type},
    //     function(data)
    //     { 
    //         if(type=='sibs')
    //         {
    //             if(sibs==null)
    //             {
    //                 event_count = 0;
    //             }
    //             else
    //             {
    //                 if(event_count==0)
    //                 {
    //                     if(data['siblings'] != null)
    //                     {
    //                         $("#sib_select").val(data['siblings']).trigger("change");
    //                         event_count++;
    //                     }
    //                 }
    //             }

    //             $('#curr_fs').val(data['fs']);
    //             $('#mname').val(data['fsdata']['mom_name']);
    //             $('#maddress1').val(data['fsdata']['mom_addy']);
    //             $('#maddress2').val(data['fsdata']['mom_add2']);
    //             $('#maddress3').val(data['fsdata']['mom_addcity']);

    //             if(data['fsdata']['mom_home']=='-')
    //             {
    //                 $('#mhome').val('');
    //             }
    //             else
    //             {
    //                 $('#mhome').val(data['fsdata']['mom_home']);
    //             }

    //             if(data['fsdata']['mom_mobile']=='-')
    //             {
    //                 $('#mom_mobile').val('');
    //             }
    //             else
    //             {
    //                 $('#mom_mobile').val(data['fsdata']['mom_mobile']);
    //             }

    //             if(data['fsdata']['mom_email']=='-')
    //             {
    //                 $('#memail').val('');
    //             }
    //             else
    //             {
    //                 $('#memail').val(data['fsdata']['mom_email']);
    //             }

    //             $('#fathername').val(data['fsdata']['dad_name']);
    //             $('#faddress1').val(data['fsdata']['dad_addy']);
    //             $('#faddress2').val(data['fsdata']['dad_add2']);
    //             $('#faddress3').val(data['fsdata']['dad_addcity']);

    //             if(data['fsdata']['dad_home']=='-')
    //             {
    //                 $('#fhome').val('');
    //             }
    //             else
    //             {
    //                 $('#fhome').val(data['fsdata']['dad_home']);
    //             }

    //             if(data['fsdata']['dad_mobile']=='-')
    //             {
    //                 $('#dad_mobile').val('');
    //             }
    //             else
    //             {
    //                 $('#dad_mobile').val(data['fsdata']['dad_mobile']);
    //             }

    //             if(data['fsdata']['dad_email']=='-')
    //             {
    //                 $('#dad_email').val('');
    //             }
    //             else
    //             {
    //                 $('#dad_email').val(data['fsdata']['dad_email']);
    //             }

    //             $('#guar_name').val(data['fsdata']['guar_name']);
    //             $('#guar_addy').val(data['fsdata']['guar_addy']);
    //             $('#guar_add2').val(data['fsdata']['guar_add2']);
    //             $('#guar_addcity').val(data['fsdata']['guar_addcity']);

    //             if(data['fsdata']['guar_home']=='-')
    //             {
    //                 $('#guar_home').val('');
    //             }
    //             else
    //             {
    //                 $('#guar_home').val(data['fsdata']['guar_home']);
    //             }

    //             if(data['fsdata']['guar_mobile']=='-')
    //             {
    //                 $('#guar_mobile').val('');
    //             }
    //             else
    //             {
    //                 $('#guar_mobile').val(data['fsdata']['guar_mobile']);
    //             }

    //             if(data['fsdata']['guar_email']=='-')
    //             {
    //                 $('#guar_email').val('');
    //             }
    //             else
    //             {
    //                 $('#guar_email').val(data['fsdata']['guar_email']);
    //             }

    //             $('#dad_aff').val(data['fsdata']['dad_aff']);
    //             $('#dad_pos').val(data['fsdata']['dad_pos']);
    //             $('#dad_biz_addy').val(data['fsdata']['dad_biz_addy']);

    //             if(data['fsdata']['dad_biz_tel']=='-')
    //             {
    //                 $('#dad_biz_tel').val('');
    //             }
    //             else
    //             {
    //                 $('#dad_biz_tel').val(data['fsdata']['dad_biz_tel']);
    //             }

    //             $('#mom_aff').val(data['fsdata']['mom_aff']);
    //             $('#mom_pos').val(data['fsdata']['mom_pos']);
    //             $('#mom_biz_addy').val(data['fsdata']['mom_biz_addy']);

    //             if(data['fsdata']['mom_biz_tel']=='-')
    //             {
    //                 $('#mom_biz_tel').val('');
    //             }
    //             else
    //             {
    //                 $('#mom_biz_tel').val(data['fsdata']['mom_biz_tel']);
    //             }
    //         }

    //         if(data['discounts'] != null)
    //         {
    //             $('#discounts_list').empty();

    //             for (i = 0; i<data['discounts'].length; i++) {

    //                 if(data['discounts'][i]['at_applyfor']==2 || data['discounts'][i]['at_applyfor']==3 || data['discounts'][i]['at_applyfor']==4)
    //                 {
    //                     checktext = 'checked="checked"';
    //                 }
    //                 else
    //                 {
    //                     checktext = '';
    //                 }

    //                 $('#discounts_list').append('<label class="label_check c_on" for="checkbox-01"><input name="discountschk[]" id="disc_'+data['discounts'][i]['adj_id']+'" value="'+data['discounts'][i]['adj_id']+'" type="checkbox" '+checktext+'> '+data['discounts'][i]['adj_description']+'</label><br>');
    //             }                         
    //         }
    //     },  
    //     "json"
    //     );
    // }

</script>