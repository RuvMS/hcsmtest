<?php
class Hci_edustructure_model extends CI_model
{

function get_sections()
{
	//$this->db->where('sec_status','A');
	$sections = $this->db->get('hci_section')->result_array();

	return $sections;
}

function save_edusection()
{
	$sec_id 	= $this->input->post('sec_id');
	$sec_name 	= $this->input->post('sec_name');

	$sectionsv['sec_name'] = $sec_name;
	$sectionsv['sec_status'] = 'A';

	if(empty($sec_id))
	{
		$save = $this->db->insert('hci_section',$sectionsv);
	}
	else
	{
		$this->db->where('sec_id',$sec_id);
		$save = $this->db->Update('hci_section',$sectionsv);
	}
	
	return $save;
}

function change_sectionstatus()
{
	$this->db->where('sec_id',$this->input->post('sec_id'));
	$result = $this->db->update('hci_section',array('sec_status'=>$this->input->post('new_s')));

	if($result)
	{
		if($this->input->post('new_s')=='A')
		{
			$this->session->set_flashdata('flashSuccess', 'Educational Section Activated Successfully.');
		}
		else
		{
			$this->session->set_flashdata('flashSuccess', 'Educational Section Deactivated Successfully.');
		}
		
	}
	else
	{
		if($this->input->post('new_s')=='A')
		{
			$this->session->set_flashdata('flashError', 'Failed to Activate Educational Section. Retry.');
		}
		else
		{
			$this->session->set_flashdata('flashError', 'Failed to Deactivate Educational Section. Retry.');
		}
	}

	return $result;
}

function get_schemes()
{
	//$this->db->where('sec_status','A');
	$this->db->select('hci_scheme.*,hci_section.sec_name');
	$this->db->join('hci_section','hci_section.sec_id=hci_scheme.schm_section','left');
	$schms = $this->db->get('hci_scheme')->result_array();

	return $schms;
}

function save_eduscheme()
{
	$schm_id 	= $this->input->post('schm_id');
	$schm_name 	= $this->input->post('schm_name');
	$schm_section 	= $this->input->post('schm_section');

	$schmsv['schm_name'] = $schm_name;
	$schmsv['schm_section'] = $schm_section;
	$schmsv['schm_status'] = 'A';

	if(empty($schm_id))
	{
		$save = $this->db->insert('hci_scheme',$schmsv);
	}
	else
	{
		$this->db->where('schm_id',$schm_id);
		$save = $this->db->Update('hci_scheme',$schmsv);
	}
	
	return $save;
}

function change_schemestatus()
{
	$this->db->where('schm_id',$this->input->post('schm_id'));
	$result = $this->db->update('hci_scheme',array('schm_status'=>$this->input->post('new_s')));

	if($result)
	{
		if($this->input->post('new_s')=='A')
		{
			$this->session->set_flashdata('flashSuccess', 'Educational Scheme Activated Successfully.');
		}
		else
		{
			$this->session->set_flashdata('flashSuccess', 'Educational Scheme Deactivated Successfully.');
		}
		
	}
	else
	{
		if($this->input->post('new_s')=='A')
		{
			$this->session->set_flashdata('flashError', 'Failed to Activate Educational Scheme. Retry.');
		}
		else
		{
			$this->session->set_flashdata('flashError', 'Failed to Deactivate Educational Scheme. Retry.');
		}
	}

	return $result;
}
}