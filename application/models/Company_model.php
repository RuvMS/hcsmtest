<?php

class Company_model extends CI_model {

function update_comp_info()
{
	$comp_id 	= $this->input->post('comp_id');
	$name 		= $this->input->post('name');
	$brnum 		= $this->input->post('brnum');
	$comcode 	= $this->input->post('comcode');
	$addl1 		= $this->input->post('addl1');
	$addl2 		= $this->input->post('addl2');
	$city 		= $this->input->post('city');
	$country 	= $this->input->post('country');
	$telephone 	= $this->input->post('telephone');
	$fax 		= $this->input->post('fax');

	$comp_save['comp_name'] 	= $name;
	$comp_save['comp_brno'] 	= $brnum;
	$comp_save['comp_code'] 	= $comcode;
	$comp_save['comp_addl1'] 	= $addl1;
	$comp_save['comp_addl2'] 	= $addl2;
	$comp_save['comp_city'] 	= $city;
	$comp_save['comp_country'] 	= $country;
	$comp_save['comp_telephone']= $telephone;
	$comp_save['comp_fax'] 		= $fax;

	if(empty($comp_id))
	{
		$save = $this->db->insert('hgc_company',$comp_save);
	}
	else
	{
		$this->db->where('comp_id',$comp_id);
		$save = $this->db->update('hgc_company',$comp_save);
	}
	
	return $save;
}

function get_comp_info()
{
	$this->db->select('*');
	$this->db->where('comp_id',1);
	$comp_info = $this->db->get('hgc_company')->row_array();
	return $comp_info;
}

function save_group()
{
	$group_id 	= $this->input->post('group_id');
	$grname 	= $this->input->post('grname');

	$grp_save['grp_name'] 	= $grname;

	if(empty($group_id))
	{
		$save = $this->db->insert('hgc_group',$grp_save);
	}
	else
	{
		$this->db->where('grp_id',$group_id);
		$save = $this->db->update('hgc_group',$grp_save);
	}
	
	return $save;
}

function get_grp_info()
{
	$this->db->where('ug_id',$this->session->userdata('u_ugroup'));
	$authgroup = $this->db->get('hgc_usergroup')->row_array();

	$this->db->select('*');
	if($authgroup['ug_level']==2 || $authgroup['ug_level']==3)
	{
		$this->db->where('grp_id',$authgroup['ug_company']);
	}
	$comp = $this->db->get('hgc_group')->result_array();

	return $comp;
}

function save_branch()
{
	$br_id 		= $this->input->post('br_id');
	$group 		= $this->input->post('brgrp');
	$name 		= $this->input->post('brname');
	$comcode 	= $this->input->post('brcode');
	$addl1 		= $this->input->post('braddl1');
	$addl2 		= $this->input->post('braddl2');
	$city 		= $this->input->post('brcity');
	$country 	= $this->input->post('brcountry');
	$telephone 	= $this->input->post('brtelephone');
	$fax 		= $this->input->post('brfax');

	$br_save['br_group'] 	= $group;
	$br_save['br_name'] 	= $name;
	$br_save['br_code'] 	= $comcode;
	$br_save['br_addl1'] 	= $addl1;
	$br_save['br_addl2'] 	= $addl2;
	$br_save['br_city'] 	= $city;
	$br_save['br_country'] 	= $country;
	$br_save['br_telephone']= $telephone;
	$br_save['br_fax'] 		= $fax;

	if(empty($br_id))
	{
		$save = $this->db->insert('hgc_branch',$br_save);
	}
	else
	{
		$this->db->where('br_id',$br_id);
		$save = $this->db->update('hgc_branch',$br_save);
	}
	
	return $save;
}

function load_branches()
{
	$this->db->where('ug_id',$this->session->userdata('u_ugroup'));
	$authgroup = $this->db->get('hgc_usergroup')->row_array();

	$this->db->select('*');
	$this->db->where('br_group',$this->input->post('id'));
	if($authgroup['ug_level']==3)
	{
		$this->db->where('br_id',$authgroup['ug_branch']);
	}
	$branches = $this->db->get('hgc_branch')->result_array();

	return $branches;
}

function edit_branch_load()
{
	$this->db->select('*');
	$this->db->where('br_id',$this->input->post('id'));
	$br_data = $this->db->get('hgc_branch')->row_array();

	return $br_data;
}

function save_fyear()
{
	$fy_id = $this->input->post('fy_id');
	$fs_date = $this->input->post('fs_date');
	$fe_date = $this->input->post('fe_date');

	$fy_save['fi_startdate'] = $fs_date;
	$fy_save['fi_enddate'] = $fe_date;

	if(isset($_POST['is_curr']))
	{
		$update = $this->db->update('hci_finance_master',array('fi_iscurryear'=>0));
		$fy_save['fi_iscurryear'] = 1;
	}
	else
	{
		$fy_save['fi_iscurryear'] = 0;
	}

	if(empty($fy_id))
	{
		$result = $this->db->insert('hci_finance_master',$fy_save);
	}
	else
	{
		$this->db->where('es_finance_masterid',$fy_id);
		$result = $this->db->update('hci_finance_master',$fy_save);
	}
	
	return $result;
}

function get_fy_info()
{
	$this->db->select('*');
	$fy_res = $this->db->get('hci_finance_master')->result_array();

	return $fy_res;
}

function save_ayear()
{
	$ay_id = $this->input->post('ay_id');
	$as_date = $this->input->post('as_date');
	$ae_date = $this->input->post('ae_date');
	$terms	= $this->input->post('terms');
	$intakes= $this->input->post('intakes');

	$this->db->trans_begin();

	$ay_save['ac_startdate'] = $as_date;
	$ay_save['ac_enddate'] = $ae_date;

	if(isset($_POST['is_curr_a']))
	{
		$this->db->update('hgc_academicyears',array('ac_iscurryear'=>0));
		$ay_save['ac_iscurryear'] = 1;
	}
	else
	{
		$ay_save['ac_iscurryear'] = 0;
	}

	if(empty($ay_id))
	{
		$this->db->insert('hgc_academicyears',$ay_save);
		$ay_id = $this->db->insert_id();
	}
	else
	{
		$this->db->where('es_ac_year_id',$ay_id);
		$this->db->update('hgc_academicyears',$ay_save);
	}

	for($x=1; $x<=$terms;$x++)
	{
		$trm_id = $this->input->post('trm_id_'.$x);

		$term_save['term_number'] 	= $this->input->post('term_'.$x);
		$term_save['term_sdate'] 	= $this->input->post('sdate_'.$x);
		$term_save['term_edate'] 	= $this->input->post('edate_'.$x);
		$term_save['term_acyear'] 	= $ay_id;

		if(empty($trm_id))
		{
			$result = $this->db->insert('hci_term',$term_save);
		}
		else
		{
			$this->db->where('term_id',$trm_id);
			$result = $this->db->update('hci_term',$term_save);
		}
	}

	for($x=1; $x<=$intakes;$x++)
	{
		$int_id = $this->input->post('int_id_'.$x);

		$int_save['int_number'] 	= $this->input->post('intnum_'.$x);
		$int_save['int_name'] 	= $this->input->post('intname_'.$x);
		$int_save['int_start'] 	= $this->input->post('intsdate_'.$x);
		$int_save['int_end'] 	= $this->input->post('intedate_'.$x);
		$int_save['int_acyear'] 	= $ay_id;

		if(empty($int_id))
		{
			$result = $this->db->insert('hci_intake',$int_save);
		}
		else
		{
			$this->db->where('int_id',$int_id);
			$result = $this->db->update('hci_intake',$int_save);
		}
	}

	if ($this->db->trans_status() === FALSE)
	{
	    $this->db->trans_rollback();
		return false;
	}
	else
	{
		$this->db->trans_commit();
		return true;	
	}
}

function get_ay_info()
{
	$this->db->select('*');
	$ay_res = $this->db->get('hgc_academicyears')->result_array();

	return $ay_res;
}

function load_terms()
{
	$today= date("Y-m-d");
    $this->db->where('int_start <=',$today);
    $this->db->where('int_end >=',$today);

    $intake = $this->db->get('hci_intake')->row_array();
	$this->db->where('term_acyear',$this->input->post('id'));
	$terms = $this->db->get('hci_term')->result_array();

	$this->db->where('int_acyear',$this->input->post('id'));
	$intakes = $this->db->get('hci_intake')->result_array();

	$this->db->where('int_start <=',$today);
    $this->db->where('int_end >=',$today);
    $currintake = $this->db->get('hci_intake')->row_array();

    $this->db->where('term_sdate <=',$today);
    $this->db->where('term_edate >=',$today);
    $currterm = $this->db->get('hci_term')->row_array();

	$all = array(
		'terms' => $terms,
		'intakes' => $intakes,
		'currintake' => $currintake['int_id'],
		'currterm' => $currterm['term_id']
		);

	return $all;
}

function load_termsperiods()
{
	$this->db->where('tprd_status','A');
	$this->db->where('tprd_term',$this->input->post('id'));
	$period = $this->db->get('hci_termperiod')->result_array();

	return $period;
}

function save_termperiod()
{
	$tp_term = $this->input->post('tp_term');
	$tp_periods = $this->input->post('tp_periods');
	$prd_id = $this->input->post('prdid');

	$this->db->trans_begin();

	$this->db->where('term_id',$tp_term);
	$this->db->update('hci_term',array('term_periods'=>$tp_periods));

	$this->db->where('tprd_term',$tp_term);
	$this->db->update('hci_termperiod',array('tprd_status'=>'D'));

	foreach ($prd_id as $key => $pdr) 
	{

		$prdsv['tprd_name'] = $this->input->post('tprd_'.$key);
		$prdsv['tprd_sdate'] = $this->input->post('tpsdate_'.$key);
		$prdsv['tprd_edate'] = $this->input->post('tpedate_'.$key);
		$prdsv['tprd_status'] = 'A';
		$prdsv['tprd_term'] = $tp_term;

		if(isset($_POST['isholymonth_'.$key]))
		{
			$prdsv['tprd_isholyday'] = 1;
		}
		else
		{
			$prdsv['tprd_isholyday'] = 0;
		}

		if($pdr=='')
		{
			$this->db->insert('hci_termperiod',$prdsv);			
		}
		else
		{
			$this->db->where('tprd_id',$pdr);
			$this->db->update('hci_termperiod',$prdsv);
		}
	}

	if ($this->db->trans_status() === FALSE)
	{
	    $this->db->trans_rollback();
		return false;
	}
	else
	{
		$this->db->trans_commit();
		return true;	
	}
}
}