<?php
class Hci_config_model extends CI_model
{

function get_deptinfo()
{
	$branchlist = $this->auth->get_accessbranch();

	$this->db->where_in('dept_branch',$branchlist);
	$deptinfo = $this->db->get('hci_department')->result_array();

	return $deptinfo;
}

function save_department()
{
	$dept_id 	= $this->input->post('dept_id');
	$depname 	= $this->input->post('depname');
	$dep_branch = $this->input->post('dep_branch');

	$deptsv['dept_name'] = $depname;
	$deptsv['dept_status'] = 'A';
	$deptsv['dept_branch'] = $dep_branch;

	if(empty($dept_id))
	{
		$save = $this->db->insert('hci_department',$deptsv);
	}
	else
	{
		$this->db->where('dept_id',$dept_id);
		$save = $this->db->Update('hci_department',$deptsv);
	}
	
	return $save;
}

function change_deptstatus()
{
	$branchlist = $this->auth->get_accessbranch();

	$this->db->where('dept_id',$this->input->post('dept_id'));
	$this->db->where_in('dept_branch',$branchlist);
	$result = $this->db->update('hci_department',array('dept_status'=>$this->input->post('new_s')));

	if($result)
	{
		if($this->input->post('new_s')=='A')
		{
			$this->session->set_flashdata('flashSuccess', 'Department Activated Successfully.');
		}
		else
		{
			$this->session->set_flashdata('flashSuccess', 'Department Deactivated Successfully.');
		}
		
	}
	else
	{
		if($this->input->post('new_s')=='A')
		{
			$this->session->set_flashdata('flashError', 'Failed to Activate Department. Retry.');
		}
		else
		{
			$this->session->set_flashdata('flashError', 'Failed to Deactivate Department. Retry.');
		}
	}

	return $result;
}

function get_desiginfo()
{
	//$this->db->where('dept_status','A');
	$desiginfo = $this->db->get('hci_designation')->result_array();

	return $desiginfo;
}

function save_designation()
{
	$desig_id 	= $this->input->post('desig_id');
	$desig_name 	= $this->input->post('desig_name');

	$desigsv['desig_name'] = $desig_name;
	$desigsv['desig_status'] = 'A';

	if(empty($desig_id))
	{
		$save = $this->db->insert('hci_designation',$desigsv);
	}
	else
	{
		$this->db->where('desig_id',$desig_id);
		$save = $this->db->Update('hci_designation',$desigsv);
	}
	
	return $save;
}

function change_desigstatus()
{
	$this->db->where('desig_id',$this->input->post('desig_id'));
	$result = $this->db->update('hci_designation',array('desig_status'=>$this->input->post('new_s')));

	if($result)
	{
		if($this->input->post('new_s')=='A')
		{
			$this->session->set_flashdata('flashSuccess', 'Designation Activated Successfully.');
		}
		else
		{
			$this->session->set_flashdata('flashSuccess', 'Designation Deactivated Successfully.');
		}
		
	}
	else
	{
		if($this->input->post('new_s')=='A')
		{
			$this->session->set_flashdata('flashError', 'Failed to Activate Designation. Retry.');
		}
		else
		{
			$this->session->set_flashdata('flashError', 'Failed to Deactivate Designation. Retry.');
		}
	}

	return $result;
}

}