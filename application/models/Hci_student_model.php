<?php
class Hci_student_model extends CI_model 
{

function load_student()
{
	$brlist = $this->auth->get_accessbranch();

    $draw = $_POST["draw"];//counter used by DataTables to ensure that the Ajax returns from server-side processing requests are drawn in sequence by DataTables
    $orderByColumnIndex  = $_POST['order'][0]['column'];// index of the sorting column (0 index based - i.e. 0 is the first record)
    $orderBy = $_POST['columns'][$orderByColumnIndex]['data'];//Get name of the sorting column from its index
    $orderType = $_POST['order'][0]['dir']; // ASC or DESC
    $start  = $_POST["start"];//Paging first record indicator.
    $length = $_POST['length'];//Number of records that the table can display in the current draw
    $stat = $_POST['stat'];

    if($stat == 'p')
    {
        $this->db->select('st_details_temp.*,hci_grade.grd_name');
        $this->db->join('hci_grade','hci_grade.grd_id=st_details_temp.st_grade');
        $this->db->where('st_status','P');
    }
    else
    {
        $this->db->select('st_details.*,hci_grade.grd_name');
        $this->db->join('hci_grade','hci_grade.grd_id=st_details.st_grade');

        if($stat == 'r')
        {
            $this->db->where('st_status','R');
        }  
        else if($stat == 'l')
        {
            $this->db->where('st_status','L');
        }
    }

    if(!empty($_POST['search']['value']))
    {
    	for($i=0 ; $i<count($_POST['columns']);$i++){
	        if($_POST['columns'][$i]['data']!='actions')
            {
                $column = $_POST['columns'][$i]['data'];//we get the name of each column using its index from POST request
                if($i==0)
                {
                    $this->db->like($column,$_POST['search']['value']);
                }
                else
                {
                    if($column == 'stu_name')
                    {
                        $this->db->or_like("CONCAT_WS(' ', family_name, other_names)",$_POST['search']['value']);
                    } 
                    else
                    {
                        $this->db->or_like($column,$_POST['search']['value']);
                    }                   
                }
            }
	    }

	    $this->db->where('id >=',$start);
    }
    else
    {
    	$this->db->where('id >=',$start); 
    }
    
    $this->db->where_in('st_branch',$brlist);
    $this->db->limit($length);
    $this->db->order_by($orderBy,$orderType);
    if($stat == 'p')
    {
        $reg_stu = $this->db->get('st_details_temp')->result_array();
        $reftbl = 'dt';
    }
    else
    {
        $reg_stu = $this->db->get('st_details')->result_array();
        $reftbl = 'd';
    }

    $x = 0;
    foreach ($reg_stu as $stu) 
    {
        $regbtn = '';

        if($reftbl == 'dt')
        {
            $regbtn = '| <button type="button" class="btn btn-info btn-xs" onclick="event.preventDefault();load_tempsturegview('.$stu['id'].',\''.$reftbl.'\')"><span class="glyphicon glyphicon-registration-mark" aria-hidden="true"></span></button>';
        }
        else
        {
            $curryear = $this->db->get_where('hgc_academicyears',array('ac_iscurryear' => 1))->row_array();

            $this->db->select('hci_grade.*');
            $this->db->join('hci_grade','hci_grade.grd_id=hci_stuacademicdetails.gp_currgrade');
            $this->db->where('gp_ayear',$curryear['es_ac_year_id']);
            $this->db->where('gp_student',$stu['id']);
            $this->db->where('gp_branch',$stu['st_branch']);
            $stucurrgrade = $this->db->get('hci_stuacademicdetails')->row_array();

            $reg_stu[$x]['grd_name'] = $stucurrgrade['grd_name'];
        }

        $reg_stu[$x]['actions'] = '<button type="button" class="btn btn-success btn-xs" onclick="event.preventDefault();load_stuprofview('.$stu['id'].',\''.$reftbl.'\')"><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span></button>'.'|'.
                                    '<button type="button" class="btn btn-info btn-xs" onclick="event.preventDefault();load_stueditview('.$stu['id'].',\''.$reftbl.'\')"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button>'.$regbtn;

        $reg_stu[$x]['stu_name'] = $stu['family_name']." ".$stu['other_names'];
        $x++;
    }

    if(!empty($_POST['search']['value']))
    {
    	for($i=0 ; $i<count($_POST['columns']);$i++){
            if($_POST['columns'][$i]['data']!='actions')
            {
    	        $column = $_POST['columns'][$i]['data'];//we get the name of each column using its index from POST request
    	        if($i==0)
    	        {
    	        	$this->db->like($column,$_POST['search']['value']);
    	        }
    	       	else
    	       	{
    	       		if($column == 'stu_name')
                    {
                        $this->db->or_like("CONCAT_WS(' ', family_name, other_names)",$_POST['search']['value']);
                    } 
                    else
                    {
                        $this->db->or_like($column,$_POST['search']['value']);
                    } 
    	       	}
            }
	    }

	    $this->db->where('id >=',$start);
    }
    
    $this->db->order_by('id','DESC');
    $this->db->limit(1);

    if($stat == 'p')
    {
        $this->db->where('st_status','P');
        $all_stu = $this->db->get('st_details_temp')->result_array();
    }
    else
    {
        if($stat == 'r')
        {
            $this->db->where('st_status','R');
        }  
        else if($stat == 'l')
        {
            $this->db->where('st_status','L');
        }
        $all_stu = $this->db->get('st_details')->result_array();
    }

    if(empty($all_stu))
    {
        $count = 0;
    }
    else
    {
        $count = $all_stu[0]['id'];
    }

    // $data = array();
    // foreach ($reg_stu as $stu ) 
    // {
    //     $data[] = $row ;
    // }

	$output = array(
                "draw" => intval($draw),
			    "recordsTotal" => $count,
			    "recordsFiltered" => $count,
			    "data" => $reg_stu
            );

    return $output;
}

function load_student_data()
{
    $id = $_SESSION['sessionstu'];
    $rt = $_SESSION['sessionstutype'];

    $brlist = $this->auth->get_accessbranch();

    $this->db->where('id',$id);
    $this->db->where_in('st_branch',$brlist);
    if($rt == 'dt')
    {
        $studata = $this->db->get('st_details_temp')->row_array();
    }
    else
    {
        $studata = $this->db->get('st_details')->row_array();
    }

    $sibary             = array();
    $payplans           = array();
    
    if(!empty($studata))
    {
        $this->db->select('es_family');
        if($rt == 'dt')
        {
            $this->db->where('hci_sibling.es_reftable','st_details_temp');
        }
        else
        {
            $this->db->where('hci_sibling.es_reftable','st_details');
        }
        $this->db->where('es_admissionid',$id);
        $family = $this->db->get('hci_sibling')->row_array();

        if(!empty($family))
        {
            $this->db->select('hci_sibling.*,st_details.other_names,st_details.family_name');
            $this->db->join('st_details','st_details.id=hci_sibling.es_admissionid');
            $this->db->where('hci_sibling.es_reftable','st_details');
            $this->db->where('hci_sibling.es_family',$family['es_family']);
            $this->db->where('hci_sibling.es_admissionid !=',$id);
            $this->db->order_by('hci_sibling.es_seqnumber');
            $siblings = $this->db->get('hci_sibling')->result_array(); 

            $firstsib = null;
            foreach ($siblings as $sib) 
            {
                $sibary[] = $sib['es_admissionid'];
                if($sib['es_seqnumber']==1)
                {
                    $firstsib = $sib['es_admissionid'];
                }
            }
            $famseq = count($sibary)+1;

            $this->db->select('st_feestructure');
            $this->db->where('id',$firstsib);
            $fsfees = $this->db->get('st_details')->row_array();

            if($fsfees['st_feestructure']==$studata['st_feestructure'])
            {
                $feesary = array(0,$fsfees['st_feestructure']);
            }
            else
            {
                $feesary = array(0,$fsfees['st_feestructure'],$studata['st_feestructure']);
            }
        }
        else
        {
            $siblings = null;
            $firstsib = null;
            $famseq   = 1;
            $feesary = array(0,$studata['st_feestructure']);
        }

        $this->db->where('spp_customer',$id);
        if($rt == 'dt')
        {
            $this->db->where('spp_reftable','st_details_temp');
        }
        else
        {
            $this->db->where('spp_reftable','st_details');
        }
        $this->db->where('spp_status','A');
        $payplans = $this->db->get('hci_cuspaymentplan')->result_array();

        $payplanidlist = array_column($payplans, 'spp_paymentplan');

    }

    if(empty($sibary))
    {
        $sibary = array('nostu');
    }

    $all = array(
        "stu_data" => $studata,
        "siblings" => $sibary,
        "payplans" => $payplanidlist,
        );

    return $all;
}

function load_student_discounts()
{
    $id = $_SESSION['sessionstu'];
    $rt = $_SESSION['sessionstutype'];

    $this->db->select('hci_adjusment.*');
    $this->db->join('hci_adjusment','hci_adjusment.adj_id = hci_adjsstudent.ats_adjstemp');
    $this->db->where('hci_adjsstudent.ats_student',$id);
    if($rt == 'dt')
    {
        $this->db->where('hci_adjsstudent.ats_reftable','st_details_temp');
    }
    else
    {
        $this->db->where('hci_adjsstudent.ats_reftable','st_details');
    }
    $this->db->where('hci_adjsstudent.ats_status','A');
    $discounts = $this->db->get('hci_adjsstudent')->result_array();

    return $discounts;
}

function update_student_info()
{
    $ref_t        = $this->input->post('ref_t');
    $id           = $this->input->post('id');
    $today= date("Y-m-d");

    $this->db->trans_begin();

    $this->db->where('fee_tftemp',$this->input->post('st_termtemplate'));
    $this->db->where('fee_aftemp',$this->input->post('st_admintemplate'));
    $fsexist = $this->db->get('hci_feemaster')->row_array();

    if(empty($fsexist))
    {
        $brcode = $this->auth->get_brcode($this->input->post('st_branch'));

        $fs_save['fee_description'] = 'SPECIAL_'.$this->input->post('st_termtemplate').'_'.$this->input->post('st_admintemplate');
        $fs_save['fee_status']      = "A";
        $fs_save['fee_effdate']     = $today;
        $fs_save['fee_aftemp']      = $this->input->post('st_admintemplate');
        $fs_save['fee_tftemp']      = $this->input->post('st_termtemplate');
        $fs_save['fee_branch']      = $this->input->post('st_branch');
        $fs_save['fee_iscurrent']   = 0;
        $fs_save['fee_code'] = $this->sequence->generate_sequence('FS/'.$brcode.'/',3);
        $result = $this->db->insert('hci_feemaster',$fs_save);

        $fsid = $this->db->insert_id();
    }
    else
    {
        $fsid = $fsexist['es_feemasterid'];
    }

    if($ref_t=='dt')
    {
        $updatedata['st_grade']     = $this->input->post('st_grade');
        $updatedata['st_branch']    = $this->input->post('st_branch');
        $updatedata['st_accyear']   = $this->input->post('st_accyear');
        $updatedata['st_term']      = $this->input->post('st_term');
        $updatedata['st_intake']    = $this->input->post('st_intake');
        $updatedata['entered_date'] = $today;
    }

    $updatedata['other_names']  = $this->input->post('other_names');
    $updatedata['family_name']  = $this->input->post('family_name');
    $updatedata['gender']       = $this->input->post('gender');
    $updatedata['dob']          = $this->input->post('dob');    
    $updatedata['st_religion']  = $this->input->post('st_religion');
    $updatedata['local_nationality'] = $this->input->post('local_nationality'); 
    $updatedata['st_infComPerson']   = $this->input->post('st_infComPerson');

    $updatedata['citizenship']  = $this->input->post('citizenship');
    $updatedata['passport']     = $this->input->post('passport');
    $updatedata['issue']        = $this->input->post('issue');
    $updatedata['language']     = $this->input->post('language');
    $updatedata['lang_home']    = $this->input->post('lang_home');  
    $updatedata['foreign_nationality'] = $this->input->post('foreign_nationality');

    $updatedata['mom_name']     = $this->input->post('mom_name');
    $updatedata['mom_addy']     = $this->input->post('mom_addy');
    $updatedata['mom_add2']     = $this->input->post('mom_add2');
    $updatedata['mom_addcity']  = $this->input->post('mom_addcity');
    $updatedata['mom_home']     = $this->input->post('mom_home');
    $updatedata['mom_mobile']   = $this->input->post('mom_mobile');
    $updatedata['mom_alt']      = $this->input->post('mom_alt');
    $updatedata['mom_fornum']   = $this->input->post('mom_fornum');
    $updatedata['mom_email']    = $this->input->post('mom_email');
    $updatedata['dad_name']     = $this->input->post('dad_name');
    $updatedata['dad_addy']     = $this->input->post('dad_addy');
    $updatedata['dad_add2']     = $this->input->post('dad_add2');
    $updatedata['dad_addcity']  = $this->input->post('dad_addcity');
    $updatedata['dad_home']     = $this->input->post('dad_home');
    $updatedata['dad_mobile']   = $this->input->post('dad_mobile');
    $updatedata['dad_alt']      = $this->input->post('dad_alt');
    $updatedata['dad_fornum']   = $this->input->post('dad_fornum');
    $updatedata['dad_email']    = $this->input->post('dad_email');
    $updatedata['guar_name']    = $this->input->post('guar_name');
    $updatedata['guar_addy']    = $this->input->post('guar_addy');
    $updatedata['guar_add2']    = $this->input->post('guar_add2');
    $updatedata['guar_addcity'] = $this->input->post('guar_addcity');
    $updatedata['guar_home']    = $this->input->post('guar_home');
    $updatedata['guar_mobile']  = $this->input->post('guar_mobile');
    $updatedata['guar_email']   = $this->input->post('guar_email');
    $updatedata['st_motherstaff'] = $this->input->post('st_motherstaff');
    $updatedata['st_fatherstaff'] = $this->input->post('st_fatherstaff');
    
    $updatedata['emg_name']     = $this->input->post('emg_name');
    $updatedata['emg_addy']     = $this->input->post('emg_addy');
    $updatedata['emg_add2']     = $this->input->post('emg_add2');
    $updatedata['emg_addcity']  = $this->input->post('emg_addcity');
    $updatedata['emg_home']     = $this->input->post('emg_home');
    $updatedata['emg_mobile']   = $this->input->post('emg_mobile');

    $updatedata['school_age']   = $this->input->post('school_age');
    $updatedata['1sch_name']    = $this->input->post('1sch_name');
    $updatedata['1sch_addy']    = $this->input->post('1sch_addy');
    $updatedata['1sch_add2']    = $this->input->post('1sch_add2');
    $updatedata['1sch_addcity'] = $this->input->post('1sch_addcity');
    $updatedata['1sch_lang']    = $this->input->post('1sch_lang');
    $updatedata['ffrom']        = $this->input->post('ffrom');
    $updatedata['fto']          = $this->input->post('fto');
    $updatedata['st_curgrade']  = $this->input->post('st_curgrade');
    $updatedata['mgt_wd']       = $this->input->post('mgt_wd');
    $updatedata['mgt_exp']      = $this->input->post('mgt_exp');
    $updatedata['int_act']      = $this->input->post('int_act');
    $updatedata['eng_xp']       = $this->input->post('eng_xp');
    $updatedata['eng_sit']      = $this->input->post('eng_sit');
    $updatedata['eng_len']      = $this->input->post('eng_len');
    $updatedata['enr_sped']     = $this->input->post('enr_sped');
    $updatedata['sped_xp']      = $this->input->post('sped_xp');
    $updatedata['med_test']     = $this->input->post('med_test');
    $updatedata['med_xp']       = $this->input->post('med_xp');
    $updatedata['med_dis']      = $this->input->post('med_dis');
    $updatedata['dis_xp']       = $this->input->post('dis_xp');

    $updatedata['dad_aff']      = $this->input->post('dad_aff');
    $updatedata['dad_pos']      = $this->input->post('dad_pos');
    $updatedata['dad_biz_addy'] = $this->input->post('dad_biz_addy');
    $updatedata['dad_biz_tel']  = $this->input->post('dad_biz_tel');

    $updatedata['mom_aff']      = $this->input->post('mom_aff');
    $updatedata['mom_pos']      = $this->input->post('mom_pos');
    $updatedata['mom_biz_addy'] = $this->input->post('mom_biz_addy');
    $updatedata['mom_biz_tel']  = $this->input->post('mom_biz_tel');

    $updatedata['ex_date']      = $this->input->post('ex_date');
    $updatedata['len_stay']     = $this->input->post('len_stay');
    $updatedata['curriculum']   = $this->input->post('curriculum');

    $updatedata['st_feestructure']  = $fsid;
    $updatedata['st_admintemplate'] = $this->input->post('st_admintemplate');
    $updatedata['st_termtemplate']  = $this->input->post('st_termtemplate');

    if($ref_t=='dt' && isset($_POST['st_addpaymethod']))
    {
        $updatedata['st_addpaymethod']  = $this->input->post('st_addpaymethod');
    }

    if($ref_t=='dt')
    {
        $this->db->where('id',$id);
        $this->db->update('st_details_temp',$updatedata);
    }
    else
    {
        $this->db->where('id',$id);
        $this->db->update('st_details',$updatedata);
    }

    //-----------------------------siblings----------------

    $sibilins     = $this->input->post('es_sibiling');

    if($ref_t=='dt')
    {
        $this->db->where('es_reftable','st_details_temp');
    }
    else
    {
        $this->db->where('es_reftable','st_details');
    }
    $this->db->where_in('es_admissionid',$id);
    $studentfamily = $this->db->get('hci_sibling')->row_array();

    if(!empty($sibilins))
    {
        $this->db->where('es_reftable','st_details');
        $this->db->where_in('es_admissionid',$sibilins);
        $family = $this->db->get('hci_sibling')->result_array();
    }

    if(!empty($studentfamily))
    {
        if(empty($sibilins) || ((!empty($sibilins)) && ((empty($family)) || ($studentfamily['es_family']!=$family[0]['es_family']))))
        {
            $this->db->where('es_sibilingid',$studentfamily['es_sibilingid']);
            $this->db->delete('hci_sibling');

            if($ref_t=='d')
            {
                $this->db->where('es_family',$studentfamily['es_family']);
                $this->db->where('es_seqnumber >',$studentfamily['es_seqnumber']);
                $this->db->where('es_reftable','st_details');
                $laterjoinedstu = $this->db->get('hci_sibling')->result_array();

                foreach ($laterjoinedstu as $latersib) 
                {
                    $this->db->set('es_seqnumber', 'es_seqnumber-1', FALSE);
                    $this->db->where('es_sibilingid',$latersib['es_sibilingid']);
                    $this->db->update('hci_sibling');
                }
            }

            if((!empty($sibilins)) && ((empty($family)) || ($studentfamily['es_family']!=$family[0]['es_family'])))
            {
                $tot_families = $this->db->count_all('hci_stfamily');

                if(empty($family))
                {
                    $this->db->insert('hci_stfamily',array("sf_num"=>($tot_families+1)));
                    $family_id = $this->db->insert_id();
                }
                else
                {
                    $family_id = $family[0]['es_family'];
                }

                sort($sibilins);

                foreach ($sibilins as $key => $ns) 
                {
                    $this->db->where('es_reftable','st_details');
                    $this->db->where('es_admissionid',$ns);
                    $sibls = $this->db->get('hci_sibling')->row_array();

                    $siblins_save['es_family'] = $family_id;
                    $siblins_save['es_admissionid'] = $ns;
                    $siblins_save['es_seqnumber'] = $key+1;
                    $siblins_save['es_reftable'] = 'st_details';

                    if(empty($sibls))
                    {
                        $this->db->insert('hci_sibling',$siblins_save);
                    }
                }

                $add_tosib['es_family'] = $family_id;
                $add_tosib['es_admissionid'] = $id;
                $add_tosib['es_seqnumber'] = count($sibilins)+1;

                if($ref_t=='dt')
                {
                    $add_tosib['es_reftable'] = 'st_details_temp';
                }
                else
                {
                    $add_tosib['es_reftable'] = 'st_details';
                }

                $this->db->insert('hci_sibling',$add_tosib);
            }
        }
    }
    else
    {
        if(!empty($sibilins))
        {
            $tot_families = $this->db->count_all('hci_stfamily');

            if(empty($family))
            {
                $this->db->insert('hci_stfamily',array("sf_num"=>($tot_families+1)));
                $family_id = $this->db->insert_id();
            }
            else
            {
                $family_id = $family[0]['es_family'];
            }

            sort($sibilins);

            foreach ($sibilins as $key => $ns) 
            {
                $this->db->where('es_reftable','st_details');
                $this->db->where('es_admissionid',$ns);
                $sibls = $this->db->get('hci_sibling')->row_array();

                $siblins_save['es_family'] = $family_id;
                $siblins_save['es_admissionid'] = $ns;
                $siblins_save['es_seqnumber'] = $key+1;
                $siblins_save['es_reftable'] = 'st_details';

                if(empty($sibls))
                {
                    $this->db->insert('hci_sibling',$siblins_save);
                }
            }

            $add_tosib['es_family'] = $family_id;
            $add_tosib['es_admissionid'] = $id;
            $add_tosib['es_seqnumber'] = count($sibilins)+1;

            if($ref_t=='dt')
            {
                $add_tosib['es_reftable'] = 'st_details_temp';
            }
            else
            {
                $add_tosib['es_reftable'] = 'st_details';
            }

            $this->db->insert('hci_sibling',$add_tosib);
        }
    }

    //-------------------------payment scheme---------------------

    $pplans = $this->input->post('pplan');

    if($ref_t=='dt')
    {
        $savespp['spp_reftable'] = 'st_details_temp';
    }
    else
    {
        $savespp['spp_reftable'] = 'st_details';
    }

    $this->db->set('spp_status','D');
    $this->db->where('spp_reftable',$savespp['spp_reftable']);
    $this->db->where('spp_customer',$id);
    $this->db->update('hci_cuspaymentplan');

    foreach ($pplans as $plan) 
    {
        if(!empty($plan))
        {
            $this->db->where('spp_customer',$id);
            $this->db->where('spp_reftable',$savespp['spp_reftable']);
            $this->db->where('spp_paymentplan',$plan);
            $isexist = $this->db->get('hci_cuspaymentplan')->row_array();

            if(empty($isexist))
            {
                $savespp['spp_status'] = 'A';
                $savespp['spp_customer'] = $id;
                $savespp['spp_paymentplan'] = $plan;

                $this->db->insert('hci_cuspaymentplan',$savespp);
            }
            else
            {
                $updatepp['spp_status'] = 'A';
                $this->db->where('spp_id',$isexist['spp_id']);
                $this->db->update('hci_cuspaymentplan',$updatepp);
            }
        }
    }

    //------------------Discounts-----------------

    $discounts = array();

    $new_discount = $this->input->post('iId');
    
    foreach ($new_discount as $dis) 
    {
        if($dis>0)
        {
            $pref = "_".$dis;
        }
        else
        {
            $pref = '';
        }

        $discid         = $this->input->post('discid'.$pref);
        $adj_type       = $this->input->post('adj_type'.$pref);
        $adj_name       = $this->input->post('studisc'.$pref);
        $adj_fee        = $this->input->post('adj_fee'.$pref);
        $adj_calamttype = $this->input->post('adj_calamttype'.$pref);
        $adj_amt        = $this->input->post('amount'.$pref);

        if(!empty($adj_name))
        {
            if(empty($discid))
            {
                $save_adj['adj_description']   = $adj_name;
                $save_adj['adj_type']          = $adj_type;
                $save_adj['adj_feecat']        = $adj_fee;
                $save_adj['adj_applyfor']      = 7;
                $save_adj['adj_amttype']       = $adj_calamttype;
                $save_adj['adj_amount']        = $adj_amt;
                $save_adj['adj_status']        = 'A';
                $save_adj['adj_isspec']        = 1;
                // $save_adj['adj_glcode']        = $;
                // $save_adj['adj_gldescription'] = $;
                $this->db->insert('hci_adjusment',$save_adj);

                $newdiscid = $this->db->insert_id();

                array_push($discounts,$newdiscid);
            }
            else
            {
                array_push($discounts,$discid);
            }
        }
    }

    if(!empty($discounts))
    {
        foreach ($discounts  as $disc) 
        {
            $this->db->where('ats_student',$id);
            $this->db->where('ats_adjstemp',$disc);
            if($ref_t=='dt')
            {
                $this->db->where('ats_reftable','st_details_temp');
            }
            else
            {
                $this->db->where('ats_reftable','st_details');
            }
            $discAssigned = $this->db->get('hci_adjsstudent')->row_array();


            if(empty($discAssigned))
            {
                if($ref_t=='dt')
                {
                    $disc_save['ats_reftable'] = 'st_details_temp';
                }
                else
                {
                    $disc_save['ats_reftable'] = 'st_details';
                }

                $disc_save['ats_status'] = 'A';
                $disc_save['ats_adjstemp'] = $disc;
                $disc_save['ats_student'] = $id;

                $this->db->insert('hci_adjsstudent',$disc_save);
            }
            
        }
    }

    $this->db->set('ats_status','D');
    if($ref_t=='dt')
    {
        $this->db->where('ats_reftable','st_details_temp');
    }
    else
    {
        $this->db->where('ats_reftable','st_details');
    }
    $this->db->where('ats_student',$id);
    if(!empty($discounts))
    {
        $this->db->where_not_in('ats_adjstemp',$discounts);
    }
    $this->db->update('hci_adjsstudent');

    if ($this->db->trans_status() === FALSE)
    {
        $this->db->trans_rollback();
        return false;
    }
    else
    {
        $this->db->trans_commit(); 
        return true;
    }
}

function registerpendingstu()
{
    $ref_t        = $this->input->post('ref_t');
    $id           = $this->input->post('id');
    $today= date("Y-m-d");

    $this->db->trans_begin();

    $this->db->where('fee_tftemp',$this->input->post('st_termtemplate'));
    $this->db->where('fee_aftemp',$this->input->post('st_admintemplate'));
    $fsexist = $this->db->get('hci_feemaster')->row_array();

    if(empty($fsexist))
    {
        $brcode = $this->auth->get_brcode($this->input->post('st_branch'));

        $fs_save['fee_description'] = 'SPECIAL_'.$this->input->post('st_termtemplate').'_'.$this->input->post('st_admintemplate');
        $fs_save['fee_status']      = "A";
        $fs_save['fee_effdate']     = $today;
        $fs_save['fee_aftemp']      = $this->input->post('st_admintemplate');
        $fs_save['fee_tftemp']      = $this->input->post('st_termtemplate');
        $fs_save['fee_branch']      = $this->input->post('st_branch');
        $fs_save['fee_iscurrent']   = 0;
        $fs_save['fee_code'] = $this->sequence->generate_sequence('FS/'.$brcode.'/',3);
        $result = $this->db->insert('hci_feemaster',$fs_save);

        $fsid = $this->db->insert_id();
    }
    else
    {
        $fsid = $fsexist['es_feemasterid'];
    }

    // -------------------------------Registration--------------------

    $this->db->where('term_id',$this->input->post('st_term'));
    $terms_date = $this->db->get('hci_term')->row_array();

    $this->db->where('br_id',$this->input->post('st_branch'));
    $branch = $this->db->get('hgc_branch')->row_array();

    $this->db->where('st_accyear',$this->input->post('st_accyear'));
    $this->db->where('st_grade',$this->input->post('st_grade'));
    $int_grd_count = $this->db->count_all_results('st_details');

    $regdata['st_status'] = 'R';
    $regdata['st_lastpromoted'] = $this->input->post('st_accyear');
    $regdata['st_tempreid'] = $id;

    $regdata['st_grade']     = $this->input->post('st_grade');
    $regdata['st_branch']    = $this->input->post('st_branch');
    $regdata['st_accyear']   = $this->input->post('st_accyear');
    $regdata['st_term']      = $this->input->post('st_term');
    $regdata['st_intake']    = $this->input->post('st_intake');
    $regdata['entered_date'] = $today;
    $regdata['other_names']  = $this->input->post('other_names');
    $regdata['family_name']  = $this->input->post('family_name');
    $regdata['gender']       = $this->input->post('gender');
    $regdata['dob']          = $this->input->post('dob');    
    $regdata['st_religion']  = $this->input->post('st_religion');
    $regdata['local_nationality'] = $this->input->post('local_nationality'); 
    $regdata['st_infComPerson']   = $this->input->post('st_infComPerson');

    $regdata['citizenship']  = $this->input->post('citizenship');
    $regdata['passport']     = $this->input->post('passport');
    $regdata['issue']        = $this->input->post('issue');
    $regdata['language']     = $this->input->post('language');
    $regdata['lang_home']    = $this->input->post('lang_home');  
    $regdata['foreign_nationality'] = $this->input->post('foreign_nationality');

    $regdata['mom_name']     = $this->input->post('mom_name');
    $regdata['mom_addy']     = $this->input->post('mom_addy');
    $regdata['mom_add2']     = $this->input->post('mom_add2');
    $regdata['mom_addcity']  = $this->input->post('mom_addcity');
    $regdata['mom_home']     = $this->input->post('mom_home');
    $regdata['mom_mobile']   = $this->input->post('mom_mobile');
    $regdata['mom_alt']      = $this->input->post('mom_alt');
    $regdata['mom_fornum']   = $this->input->post('mom_fornum');
    $regdata['mom_email']    = $this->input->post('mom_email');
    $regdata['dad_name']     = $this->input->post('dad_name');
    $regdata['dad_addy']     = $this->input->post('dad_addy');
    $regdata['dad_add2']     = $this->input->post('dad_add2');
    $regdata['dad_addcity']  = $this->input->post('dad_addcity');
    $regdata['dad_home']     = $this->input->post('dad_home');
    $regdata['dad_mobile']   = $this->input->post('dad_mobile');
    $regdata['dad_alt']      = $this->input->post('dad_alt');
    $regdata['dad_fornum']   = $this->input->post('dad_fornum');
    $regdata['dad_email']    = $this->input->post('dad_email');
    $regdata['guar_name']    = $this->input->post('guar_name');
    $regdata['guar_addy']    = $this->input->post('guar_addy');
    $regdata['guar_add2']    = $this->input->post('guar_add2');
    $regdata['guar_addcity'] = $this->input->post('guar_addcity');
    $regdata['guar_home']    = $this->input->post('guar_home');
    $regdata['guar_mobile']  = $this->input->post('guar_mobile');
    $regdata['guar_email']   = $this->input->post('guar_email');
    $regdata['st_motherstaff'] = $this->input->post('st_motherstaff');
    $regdata['st_fatherstaff'] = $this->input->post('st_fatherstaff');
    
    $regdata['emg_name']     = $this->input->post('emg_name');
    $regdata['emg_addy']     = $this->input->post('emg_addy');
    $regdata['emg_add2']     = $this->input->post('emg_add2');
    $regdata['emg_addcity']  = $this->input->post('emg_addcity');
    $regdata['emg_home']     = $this->input->post('emg_home');
    $regdata['emg_mobile']   = $this->input->post('emg_mobile');

    $regdata['school_age']   = $this->input->post('school_age');
    $regdata['1sch_name']    = $this->input->post('1sch_name');
    $regdata['1sch_addy']    = $this->input->post('1sch_addy');
    $regdata['1sch_add2']    = $this->input->post('1sch_add2');
    $regdata['1sch_addcity'] = $this->input->post('1sch_addcity');
    $regdata['1sch_lang']    = $this->input->post('1sch_lang');
    $regdata['ffrom']        = $this->input->post('ffrom');
    $regdata['fto']          = $this->input->post('fto');
    $regdata['st_curgrade']  = $this->input->post('st_curgrade');
    $regdata['mgt_wd']       = $this->input->post('mgt_wd');
    $regdata['mgt_exp']      = $this->input->post('mgt_exp');
    $regdata['int_act']      = $this->input->post('int_act');
    $regdata['eng_xp']       = $this->input->post('eng_xp');
    $regdata['eng_sit']      = $this->input->post('eng_sit');
    $regdata['eng_len']      = $this->input->post('eng_len');
    $regdata['enr_sped']     = $this->input->post('enr_sped');
    $regdata['sped_xp']      = $this->input->post('sped_xp');
    $regdata['med_test']     = $this->input->post('med_test');
    $regdata['med_xp']       = $this->input->post('med_xp');
    $regdata['med_dis']      = $this->input->post('med_dis');
    $regdata['dis_xp']       = $this->input->post('dis_xp');

    $regdata['dad_aff']      = $this->input->post('dad_aff');
    $regdata['dad_pos']      = $this->input->post('dad_pos');
    $regdata['dad_biz_addy'] = $this->input->post('dad_biz_addy');
    $regdata['dad_biz_tel']  = $this->input->post('dad_biz_tel');

    $regdata['mom_aff']      = $this->input->post('mom_aff');
    $regdata['mom_pos']      = $this->input->post('mom_pos');
    $regdata['mom_biz_addy'] = $this->input->post('mom_biz_addy');
    $regdata['mom_biz_tel']  = $this->input->post('mom_biz_tel');

    $regdata['ex_date']      = $this->input->post('ex_date');
    $regdata['len_stay']     = $this->input->post('len_stay');
    $regdata['curriculum']   = $this->input->post('curriculum');

    $regdata['st_feestructure']  = $fsid;
    $regdata['st_admintemplate'] = $this->input->post('st_admintemplate');
    $regdata['st_termtemplate']  = $this->input->post('st_termtemplate');

    if(isset($_POST['st_addpaymethod']))
    {
        $regdata['st_addpaymethod']  = $this->input->post('st_addpaymethod');
    }

    $registration = $this->db->insert('st_details',$regdata);

    $new_id = $this->db->insert_id();

    //--------------------------------Student Index-----------------------

    $this->db->where('es_ac_year_id',$regdata['st_accyear']);
    $acc_year = $this->db->get('hgc_academicyears')->row_array();

    $this->db->where('int_id',$regdata['st_intake']);
    $intake = $this->db->get('hci_intake')->row_array();

    $this->db->where('grd_id',$regdata['st_grade']);
    $grd_data = $this->db->get('hci_grade')->row_array();

    $this->db->select('st_id');
    $this->db->where('id !=',$new_id);
    $this->db->order_by('id','DESC');
    $this->db->limit(1);
    $tot_stu_count = $this->db->get('st_details')->row_array();

    if(empty($tot_stu_count))
    {
        $tot_count = 1;
    }
    else
    {
        $temp_idary = explode('/', $tot_stu_count['st_id']);
        $tot_count = $temp_idary[4]+1;
    }
    
    $curr_year = date('y', strtotime($acc_year['ac_startdate']));
    $curr_intake = date('m', strtotime($intake['int_start']));
    $grd_code = $grd_data['grd_code'];

    $stu_index = $branch['br_code'].'/'.$curr_year.'/'.$curr_intake.'/'.$grd_code.'-'.($int_grd_count+1).'/'.($tot_count);

    $this->db->where('id',$new_id);
    $index_update = $this->db->update('st_details',array('st_id'=>$stu_index));

    $this->db->where('ug_level',5);
    $usergrp = $this->db->get('hgc_usergroup')->row_array();

    $saveuser['user_name'] = $stu_index;
    $saveuser['user_password'] = md5('123');
    $saveuser['user_ugroup'] = $usergrp['ug_id'];
    $saveuser['user_employee'] = $new_id;
    $saveuser['user_group'] = $branch['br_group'];
    $saveuser['user_branch'] = $branch['br_id'];
    $saveuser['user_status'] = 'A';

    $useradd = $this->db->insert('hgc_user',$saveuser);

    //-----------------------------siblings----------------

    $sibilins     = $this->input->post('es_sibiling');

    $this->db->where('es_reftable','st_details_temp');
    $this->db->where_in('es_admissionid',$id);
    $studentfamily = $this->db->get('hci_sibling')->row_array();

    if(!empty($sibilins))
    {
        $this->db->where('es_reftable','st_details');
        $this->db->where_in('es_admissionid',$sibilins);
        $family = $this->db->get('hci_sibling')->result_array();
    }

    if(!empty($studentfamily))
    {
        if(empty($sibilins) || ((!empty($sibilins)) && ((empty($family)) || ($studentfamily['es_family']!=$family[0]['es_family']))))
        {
            $this->db->where('es_sibilingid',$studentfamily['es_sibilingid']);
            $this->db->delete('hci_sibling');
        }
    }

    $savesibling = false;
    if(((empty($studentfamily)) && (!empty($sibilins))) || (((!empty($studentfamily)) && ((!empty($sibilins)) && ((empty($family)) || ($studentfamily['es_family']!=$family[0]['es_family']))))))
    {
        $tot_families = $this->db->count_all('hci_stfamily');

        if(empty($family))
        {
            $this->db->insert('hci_stfamily',array("sf_num"=>($tot_families+1)));
            $family_id = $this->db->insert_id();
        }
        else
        {
            $family_id = $family[0]['es_family'];
        }

        sort($sibilins);

        foreach ($sibilins as $key => $ns) 
        {
            $this->db->where('es_reftable','st_details');
            $this->db->where('es_admissionid',$ns);
            $sibls = $this->db->get('hci_sibling')->row_array();

            $siblins_save['es_family'] = $family_id;
            $siblins_save['es_admissionid'] = $ns;
            $siblins_save['es_seqnumber'] = $key+1;
            $siblins_save['es_reftable'] = 'st_details';

            if(empty($sibls))
            {
                $this->db->insert('hci_sibling',$siblins_save);
            }
        }

        $add_tosib['es_family'] = $family_id;
        $add_tosib['es_admissionid'] = $new_id;
        $add_tosib['es_seqnumber'] = count($sibilins)+1;
        $add_tosib['es_reftable'] = 'st_details';

        $savesibling = $this->db->insert('hci_sibling',$add_tosib);
    }

    //-------------------------payment scheme---------------------

    $pplans = $this->input->post('pplan');

    $savespp['spp_reftable'] = 'st_details_temp';

    $this->db->set('spp_status','D');
    $this->db->where('spp_reftable',$savespp['spp_reftable']);
    $this->db->where('spp_customer',$id);
    $this->db->update('hci_cuspaymentplan');

    foreach ($pplans as $plan) 
    {
        if(!empty($plan))
        {
            $this->db->where('spp_customer',$id);
            $this->db->where('spp_reftable',$savespp['spp_reftable']);
            $this->db->where('spp_paymentplan',$plan);
            $isexist = $this->db->get('hci_cuspaymentplan')->row_array();

            if(empty($isexist))
            {
                $savespp['spp_status'] = 'A';
                $savespp['spp_customer'] = $id;
                $savespp['spp_paymentplan'] = $plan;

                $this->db->insert('hci_cuspaymentplan',$savespp);
            }
            else
            {
                $updatepp['spp_status'] = 'A';
                $this->db->where('spp_id',$isexist['spp_id']);
                $this->db->update('hci_cuspaymentplan',$updatepp);
            }
        }
    }

    $this->db->where('spp_reftable',$savespp['spp_reftable']);
    $this->db->where('spp_customer',$id);
    $paymentplanupdate = $this->db->update('hci_cuspaymentplan',array('spp_customer' => $new_id, 'spp_reftable' => 'st_details'));

    //------------------Discounts-----------------

    $discounts = array();

    $new_discount = $this->input->post('iId');
    
    foreach ($new_discount as $dis) 
    {
        if($dis>0)
        {
            $pref = "_".$dis;
        }
        else
        {
            $pref = '';
        }

        $discid         = $this->input->post('discid'.$pref);
        $adj_type       = $this->input->post('adj_type'.$pref);
        $adj_name       = $this->input->post('studisc'.$pref);
        $adj_fee        = $this->input->post('adj_fee'.$pref);
        $adj_calamttype = $this->input->post('adj_calamttype'.$pref);
        $adj_amt        = $this->input->post('amount'.$pref);

        if(!empty($adj_name))
        {
            if(empty($discid))
            {
                $save_adj['adj_description']   = $adj_name;
                $save_adj['adj_type']          = $adj_type;
                $save_adj['adj_feecat']        = $adj_fee;
                $save_adj['adj_applyfor']      = 7;
                $save_adj['adj_amttype']       = $adj_calamttype;
                $save_adj['adj_amount']        = $adj_amt;
                $save_adj['adj_status']        = 'A';
                $save_adj['adj_isspec']        = 1;
                // $save_adj['adj_glcode']        = $;
                // $save_adj['adj_gldescription'] = $;
                $this->db->insert('hci_adjusment',$save_adj);

                $newdiscid = $this->db->insert_id();

                array_push($discounts,$newdiscid);
            }
            else
            {
                array_push($discounts,$discid);
            }
        }
    }

    if(!empty($discounts))
    {
        foreach ($discounts  as $disc) 
        {
            $this->db->where('ats_student',$id);
            $this->db->where('ats_adjstemp',$disc);
            $this->db->where('ats_reftable','st_details_temp');
            $discAssigned = $this->db->get('hci_adjsstudent')->row_array();

            if(empty($discAssigned))
            {
                $disc_save['ats_reftable'] = 'st_details_temp';
                $disc_save['ats_status'] = 'A';
                $disc_save['ats_adjstemp'] = $disc;
                $disc_save['ats_student'] = $id;

                $this->db->insert('hci_adjsstudent',$disc_save);
            }
            
        }
    }

    $this->db->set('ats_status','D');
    $this->db->where('ats_reftable','st_details_temp');
    $this->db->where('ats_student',$id);
    if(!empty($discounts))
    {
        $this->db->where_not_in('ats_adjstemp',$discounts);
    }
    $this->db->update('hci_adjsstudent');

    $this->db->where('ats_reftable','st_details_temp');
    $this->db->where('ats_student',$id);
    $discountsave = $this->db->update('hci_adjsstudent',array('ats_student' => $new_id, 'ats_reftable' => 'st_details'));
  
    $this->db->select('hci_payment.*,hci_receipt.rec_createdusername');
    $this->db->join('hci_receipt','hci_receipt.rec_payid=hci_payment.pay_id');
    $this->db->where('pay_custype','TEMPSTU');
    $this->db->where('pay_customer',$id);
    $this->db->where('pay_type','ADV');
    $advancepayments = $this->db->get('hci_payment')->result_array();

    //var_dump($advancepayments);die();

    foreach ($advancepayments as $payment) 
    {
        $this->load->model('hci_accounts_model');
        $this->hci_accounts_model->update_transaction('STUDENT',$new_id,$payment['pay_id'],'ADVANCEPAY',$payment['pay_description'],$payment['pay_date'],'CR',$payment['pay_amount'],$payment['pay_createuser'],0,$payment['pay_createdate'],$payment['rec_createdusername'],$branch['br_code']);
    }

    if ($this->db->trans_status() === FALSE)
    {
        $this->db->trans_rollback();
        return false;
    }
    else
    {
        $this->db->trans_commit(); 
        return true;
    }
}

function load_stubygrade()
{
    $curr_ayear = $this->input->post('curr_ayear');
    $grade      = $this->input->post('grd');
    $ayear      = $this->input->post('ayear');
    $brnch      = $this->input->post('brnch');

    $this->db->where('grd_status','A');
    $totalactivegrades = $this->db->get('hci_grade')->result_array();

    if(empty($grade))
    {
        $grades = array_column($totalactivegrades, 'grd_id');
    }
    else
    {
        $grades = array($grade);
    }

    $this->db->select('st_details.*,hci_stuacademicdetails.*,currgrd.grd_name as "curr",nextgrd.grd_name as "next",nextgrd.grd_id as "next_id"');
    $this->db->join('st_details','st_details.id=hci_stuacademicdetails.gp_student');
    $this->db->join('hci_grade as currgrd','currgrd.grd_id=hci_stuacademicdetails.gp_currgrade','left');
    $this->db->join('hci_grade as nextgrd','nextgrd.grd_id=currgrd.grd_promotegrd','left');
    $this->db->where('gp_ayear',$curr_ayear);
    $this->db->where_in('gp_currgrade',$grades);
    $this->db->where('gp_branch',$brnch);
    $this->db->order_by('gp_currgrade','ASC');
    $stu_list = $this->db->get('hci_stuacademicdetails')->result_array();

    // $this->db->where('promo_fromaccyear',$curr_ayear);
    // $this->db->where('promo_accyear',$ayear);
    // $promo_detail = $this->db->get('hci_gradepromotion')->row_array();

    $x = 0;
    foreach ($stu_list as $stu) 
    {
        if($stu_list[$x]['next_id'] == NULL)
        {
            $stu_list[$x]['next_id'] = 0;
        }

        $this->db->join('hci_grade','hci_grade.grd_id=hci_stuacademicdetails.gp_currgrade');
        $this->db->where('gp_ayear',$ayear);
        $this->db->where_in('gp_student',$stu['id']);
        $this->db->where('gp_branch',$brnch);
        $upgradedGrade = $this->db->get('hci_stuacademicdetails')->row_array();

        //var_dump($upgradedGrade);

        if(!empty($upgradedGrade))
        {
            $stu_list[$x]['next_id'] = $upgradedGrade['gp_currgrade'];
            $stu_list[$x]['next'] = $upgradedGrade['grd_name'];
        }

        $x++;
    }

    $all = array(
        'grades' => $totalactivegrades,
        'stu_list' => $stu_list,
        //'promo_detail' => $promo_detail,
        );

    return $all;
}

function promote_students()
{
    $ayear      = $this->input->post('ayear');
    $grade      = $this->input->post('grade');
    $branch     = $this->input->post('promobranch');
    $curr_ayear = $this->input->post('curr_ayear');

    $this->db->trans_begin();

    if(!empty($grade))
        $this->db->where('grd_id',$grade);
    $this->db->where('grd_status','A');
    $activegrades = $this->db->get('hci_grade')->result_array();

    foreach ($activegrades as $grd) 
    {
        $this->db->where('promo_fromaccyear',$curr_ayear);
        $this->db->where('promo_accyear',$ayear);
        $this->db->where('promo_grade',$grd['grd_id']);
        $this->db->where('promo_branch',$branch);
        $ispromoted = $this->db->get('hci_gradepromotion')->row_array();

        if(empty($ispromoted))
        {
            $promoSave['promo_fromaccyear'] = $curr_ayear;
            $promoSave['promo_accyear']     = $ayear;
            $promoSave['promo_grade']       = $grd['grd_id'];
            $promoSave['promo_branch']      = $branch;

            $this->db->insert('hci_gradepromotion',$promoSave);

            $promoId = $this->db->insert_id();
        }
        else
        {
            $promoId = $ispromoted['promo_id'];
        }

        $this->db->where('gp_ayear',$ayear);
        $this->db->where('gp_refpromo',NULL);
        $this->db->update('hci_stuacademicdetails',array('gp_refpromo' => $promoId));

        if(isset($_POST['promostu']))
        {
            $promostu   = $this->input->post('promostu');

            foreach ($promostu as $stu) 
            {
                $currgrd = $this->input->post('stucurggrd_'.$stu);
                $nextgrd = $this->input->post('stugrd_'.$stu);

                if($currgrd == $grd['grd_id'])
                {
                    $this->db->where('gp_ayear',$curr_ayear);
                    $this->db->where('gp_student',$stu);
                    $this->db->where('gp_branch',$branch);
                    $stuCurrClass = $this->db->get('hci_stuacademicdetails')->row_array();
                    
                    $studentClass = NULL;

                    if((!empty($stuCurrClass)) && ($stuCurrClass['gp_class'] != NULL))
                    {
                        $this->db->where('yrcls_branch',$branch);
                        $this->db->where('yrcls_academicyear',$ayear);
                        $this->db->where('yrcls_grade',$grd['grd_id']);
                        $this->db->where('yrcls_class',$stuCurrClass['gp_class']);
                        $yearClassExists = $this->db->get('hci_accyear_class')->row_array();

                        if(empty($yearClassExists))
                        {   
                            $yearClass['yrcls_academicyear'] = $ayear;
                            $yearClass['yrcls_grade']        = $grd['grd_id'];
                            $yearClass['yrcls_branch']       = $branch;
                            $yearClass['yrcls_class']        = $stuCurrClass['gp_class'];
                            $yearClass['yrcls_status']       = 'A';
                            $yearClass['yrcls_maxstudents']  = 25;

                            $this->db->insert('hci_accyear_class',$yearClass);
                        }

                        $studentClass = $stuCurrClass['gp_class'];
                    }
                
                    $stuupdate['st_lastpromoted'] = $ayear;

                    if($nextgrd == 0)
                    {
                        $stuAcaYear['gp_isleft']        = 1;

                        $stuupdate['st_status'] = 'PL';
                        $stuupdate['st_leftyear'] = $ayear;
                    }

                    $this->db->where('id',$stu);
                    $this->db->update('st_details',$stuupdate);

                    $this->db->where('gp_ayear',$ayear);
                    $this->db->where('gp_student',$stu);
                    $this->db->where('gp_branch',$branch);
                    $stuPromoted = $this->db->get('hci_stuacademicdetails')->row_array();
                    
                    $stuAcaYear['gp_currgrade']     = $nextgrd;

                    if(empty($stuPromoted))
                    {
                        $stuAcaYear['gp_refpromo']      = $promoId;
                        $stuAcaYear['gp_ayear']         = $ayear;
                        $stuAcaYear['gp_student']       = $stu;
                        $stuAcaYear['gp_promotedfrom']  = $currgrd;
                        $stuAcaYear['gp_branch']        = $branch;
                        $stuAcaYear['gp_class']         = $studentClass;

                        $this->db->insert('hci_stuacademicdetails',$stuAcaYear);
                    }
                    else
                    {
                        $this->db->where('gp_id',$stuPromoted['gp_id']);
                        $this->db->update('hci_stuacademicdetails',$stuAcaYear);
                    }
                }
            }
        }
    }
    
    if ($this->db->trans_status() === FALSE)
    {
        $this->db->trans_rollback();
        return false;
    }
    else
    {
        $this->db->trans_commit(); 
        return true;
    }
}

function stu_profile_data()
{
    $brlist = $this->auth->get_accessbranch();
    $id = $this->input->get('id');
    $rt = $this->input->get('rt');

    $all = array();

    if($rt == 'dt')
    {
        $table = 'st_details_temp';
    }
    else
    {
        $table = 'st_details';
    }

    $this->db->select($table.'.*,hci_grade.grd_name,hgc_religion.rel_name','hgc_academicyears.ac_startdate');
    $this->db->join('hci_grade','hci_grade.grd_id='.$table.'.st_grade','left');
    $this->db->join('hgc_religion','hgc_religion.rel_id='.$table.'.st_religion','left');
    $this->db->join('hgc_academicyears','hgc_academicyears.es_ac_year_id='.$table.'.st_accyear','left');
    $this->db->where($table.'.id',$id);
    $this->db->where_in('st_branch',$brlist);
    $st_details = $this->db->get($table)->row_array();

    if(!empty($st_details))
    {
        if($rt != 'dt')
        {
            $curryear = $this->db->get_where('hgc_academicyears',array('ac_iscurryear' => 1))->row_array();

            $this->db->select('hci_grade.*');
            $this->db->join('hci_grade','hci_grade.grd_id=hci_stuacademicdetails.gp_currgrade');
            $this->db->where('gp_ayear',$curryear['es_ac_year_id']);
            $this->db->where('gp_student',$id);
            $this->db->where('gp_branch',$st_details['st_branch']);
            $stucurrgrade = $this->db->get('hci_stuacademicdetails')->row_array();

            $st_details['grd_name'] = $stucurrgrade['grd_name'];
        }
        
        $this->db->select('hci_feemaster.*,at.ft_name as admtemp,ft.ft_name as trmtemp');
        $this->db->join('hci_feetemplate as at', 'hci_feemaster.fee_aftemp = at.ft_id','left');
        $this->db->join('hci_feetemplate as ft', 'hci_feemaster.fee_tftemp = ft.ft_id','left');
        $this->db->where('es_feemasterid',$st_details['st_feestructure']);
        $fee_mas = $this->db->get('hci_feemaster')->row_array();

        $this->db->select('*');
        $this->db->where('grd_status','A');
        $grd_info = $this->db->get('hci_grade')->result_array();

        $this->db->select('*');
        $fees = $this->db->get('hci_feecategory')->result_array();

        $this->db->select('hci_cuspaymentplan.*,hci_paymentplan.plan_name,hci_paymentplan.plan_fee');
        $this->db->join('hci_paymentplan','hci_paymentplan.plan_id = hci_cuspaymentplan.spp_paymentplan','left');
        $this->db->where('hci_paymentplan.plan_status','A');
        $this->db->where('hci_cuspaymentplan.spp_status','A');
        $this->db->where('hci_cuspaymentplan.spp_customer',$id);
        $pay_plan = $this->db->get('hci_cuspaymentplan')->result_array();
        
        $this->db->select('hci_adjusment.*');
        $this->db->join('hci_adjusment','hci_adjusment.adj_id = hci_adjsstudent.ats_adjstemp');
        $this->db->where('hci_adjsstudent.ats_student',$id);
        if($rt == 'dt')
        {
            $this->db->where('hci_adjsstudent.ats_reftable','st_details_temp');
        }
        else
        {
            $this->db->where('hci_adjsstudent.ats_reftable','st_details');
        }
        $this->db->where('hci_adjsstudent.ats_status','A');
        $discounts = $this->db->get('hci_adjsstudent')->result_array();

        $x = 0;
        foreach ($grd_info as $grd)
        {
            $this->db->where('fsf_feetemplate',$fee_mas['fee_aftemp']);
            $this->db->where('fsf_grade',$grd['grd_id']);
            $afts = $this->db->get('hci_feestructure_fees')->result_array();

            $this->db->where('fsf_feetemplate',$fee_mas['fee_tftemp']);
            $this->db->where('fsf_grade',$grd['grd_id']);
            $tfts = $this->db->get('hci_feestructure_fees')->result_array(); 

            $grd_info[$x]['afts'] = $afts;
            $grd_info[$x]['tfts'] = $tfts;
            $x++; 
        }


        // $this->db->where_in('fsf_feetemplate',array($fs_data['fee_aftemp'],$fs_data['fee_tftemp']));
        // $fees = $this->db->get('hci_feestructure_fees')->result_array();

        // $all = array(
        //  'fs_data' => $fs_data,
        //  'fees' => $fees
        //  );

        // return $fs_data;
        $this->db->where('tran_custype','STUDENT');
        $this->db->where('tran_customer',$id);
        $trans = $this->db->get('hci_transaction')->result_array();

        $all = array(
            'st_details' => $st_details,
            'fee_mas' => $fee_mas,
            'grd_info' => $grd_info,
            'fees' => $fees,
            'pay_plan' => $pay_plan,
            'discounts' => $discounts,
            'trans' => $trans          
        );
    }

    return $all;
}

function load_students()
{
    $type = $this->input->post('type');
    $brlist = $this->auth->get_accessbranch();

    $this->db->where_in('st_branch',$brlist);
    if($type == 1)
    {
        $this->db->where('st_status','R');
        $students = $this->db->get('st_details')->result_array();
    }
    else
    {
        $students = $this->db->get('st_details_temp')->result_array();
    }

    return $students;
}

function load_class_stulist()
{
    $acyear = $this->input->post('acyear');
    $grade  = $this->input->post('grade');
    $branch  = $this->input->post('branch');

    $this->db->select('*');
    $this->db->join('hci_class','hci_class.cls_id=hci_accyear_class.yrcls_class');
    $this->db->where('yrcls_academicyear',$acyear);
    $this->db->where('yrcls_grade',$grade);
    $this->db->where('yrcls_branch',$branch);
    $this->db->where('yrcls_status','A');
    $classes = $this->db->get('hci_accyear_class')->result_array();

    $x = 0;
    foreach ($classes as $cls) 
    {
        $this->db->select('st_details.*');
        $this->db->join('st_details','st_details.id=hci_stuacademicdetails.gp_student');
        $this->db->where('gp_currgrade',$grade);
        $this->db->where('gp_branch',$branch);
        $this->db->where('gp_class',$cls['cls_id']);
        $this->db->where('gp_ayear',$acyear);
        $stulist = $this->db->get('hci_stuacademicdetails')->result_array();

        $classes[$x]['stulist'] = $stulist;
        $x++;
    }

    return $classes;
}

function load_shuffledata()
{
    $acyear = $this->input->post('acyear');
    $grade  = $this->input->post('grade');
    $branch  = $this->input->post('branch');

    $this->db->select('*');
    $this->db->join('st_details','st_details.id=hci_stuacademicdetails.gp_student');
    $this->db->where('gp_currgrade',$grade);
    $this->db->where('gp_branch',$branch);
    $this->db->where('gp_ayear',$acyear);
    $stu_list = $this->db->get('hci_stuacademicdetails')->result_array();

    $this->db->select('*');
    $this->db->join('hci_class','hci_class.cls_id=hci_accyear_class.yrcls_class');
    $this->db->where('yrcls_academicyear',$acyear);
    $this->db->where('yrcls_grade',$grade);
    $this->db->where('yrcls_branch',$branch);
    $this->db->where('yrcls_status','A');
    $classes = $this->db->get('hci_accyear_class')->result_array();

    $all = array(
        "stu_list" => $stu_list,
        "classes" => $classes
        );
    
    return $all;
}

function save_suffling()
{
    $acyear = $this->input->post('cls_academicyear');
    $grade  = $this->input->post('cls_grade');
    $branch = $this->input->post('cls_branch');;
    $stuchk = $this->input->post('stuchk');

    $this->db->trans_begin();

    foreach ($stuchk as $key => $value) 
    {
        $this->db->where('gp_currgrade',$grade);
        $this->db->where('gp_ayear',$acyear);
        $this->db->where('gp_student',$key);
        $this->db->where('gp_branch',$branch);
        $isexist = $this->db->get('hci_stuacademicdetails')->row_array();

        if(($isexist['gp_class'] != 0) && ($isexist['gp_class'] != $value))
        {
            $this->db->where('clscng_acayear',$acyear);
            $this->db->where('clscng_branch',$branch);
            $this->db->where('clscng_grade',$grade);
            $this->db->where('clscng_student',$key);
            $this->db->order_by('clscng_id','DESC');
            $this->db->limit(1);
            $classchanged = $this->db->get('hci_classchange')->row_array();

            $accYearDetail = $this->db->get_where('hgc_academicyears',array('es_ac_year_id'=>$acyear))->row_array();

            $clscngSave['clscng_acayear']   = $acyear;
            $clscngSave['clscng_branch']    = $branch;
            $clscngSave['clscng_grade']     = $grade;
            $clscngSave['clscng_student']   = $key;

            if(empty($classchanged))
            {
                $clscngSave['clscng_class']     = $isexist['gp_class'];
                $clscngSave['clscng_startdate'] = $accYearDetail['ac_startdate'];
                $clscngSave['clscng_enddate']   = date("Y-m-d", strtotime("-1 day"));
                $this->db->insert('hci_classchange',$clscngSave);
            }
            else
            {
                $this->db->where('clscng_id',$classchanged['clscng_id']);
                $this->db->update('hci_classchange',array('clscng_enddate' => date("Y-m-d", strtotime("-1 day"))));
            }

            $clscngSave['clscng_class']     = $value;
            $clscngSave['clscng_startdate'] = date("Y-m-d");;
            $clscngSave['clscng_enddate']   = $accYearDetail['ac_enddate'];

            $this->db->insert('hci_classchange',$clscngSave);
        }

        $this->db->where('gp_id',$isexist['gp_id']);
        $this->db->update('hci_stuacademicdetails',array('gp_class'=>$value));
    }

    if ($this->db->trans_status() === FALSE)
    {
        $this->db->trans_rollback();
        $res['status'] = 'Failed';
        $res['message'] = 'Failed to shuffle students';
    }
    else
    {
        $this->db->trans_commit(); 
        $res['status'] = 'success';
        $res['message'] = 'Students shuffled successfully.';
    }

    return $res;
}

function load_sibling_data()
{
    $type  = $this->input->post('type');
    $stu        = $this->input->post('stu');

    if($type == 'STUDENT')
    {
        $reftable = 'st_details'; 
    }
    else
    {
        $reftable = 'st_details_temp'; 
    }
  
    // Sub Query
    $this->db->select('es_family')->from('hci_sibling')->where('es_admissionid',$stu)->where('es_reftable',$reftable);
    $family =  $this->db->get_compiled_select();
     
    // Main Query
    $siblings = $this->db->select('hci_sibling.*,st_details.st_id,st_details.family_name,st_details.other_names,st_details.st_termtemplate,st_details.st_admintemplate,st_details.st_status,st_details.family_name,st_details.family_name,st_details.family_name,st_details.family_name')
             ->join('st_details','st_details.id = hci_sibling.es_admissionid')
             ->from('hci_sibling')
             ->where("es_family IN ($family)", NULL, FALSE)
             ->where('es_admissionid !=',$stu)
             ->where('es_consideInFamily',1)
             ->order_by('es_seqnumber')
             ->where('es_reftable',$reftable)
             ->get()
             ->result_array();

    foreach ($siblings as $key => $value) 
    {
        $this->db->select('hci_adjusment.*');
        $this->db->join('hci_adjusment','hci_adjusment.adj_id=hci_adjsstudent.ats_adjstemp');
        $this->db->where('ats_reftable',$reftable);
        $this->db->where('ats_student',$value['es_admissionid']);
        $this->db->where('ats_status','A');
        $discounts = $this->db->get('hci_adjsstudent')->result_array();

        $siblings[$key]['discounts'] = $discounts;
    }


    $nonConsideredSibs = $this->db->select('hci_sibling.*,st_details.st_id,st_details.family_name,st_details.other_names,st_details.st_termtemplate,st_details.st_admintemplate,st_details.st_status,st_details.family_name,st_details.family_name,st_details.family_name,st_details.family_name')
             ->join('st_details','st_details.id = hci_sibling.es_admissionid')
             ->from('hci_sibling')
             ->where("es_family IN ($family)", NULL, FALSE)
             ->where('es_admissionid !=',$stu)
             ->where('es_consideInFamily',0)
             ->order_by('es_seqnumber')
             ->where('es_reftable',$reftable)
             ->get()
             ->result_array();

    foreach ($nonConsideredSibs as $key => $value) 
    {
        $this->db->select('hci_adjusment.*');
        $this->db->join('hci_adjusment','hci_adjusment.adj_id=hci_adjsstudent.ats_adjstemp');
        $this->db->where('ats_reftable',$reftable);
        $this->db->where('ats_student',$value['es_admissionid']);
        $this->db->where('ats_status','A');
        $discounts = $this->db->get('hci_adjsstudent')->result_array();

        $nonConsideredSibs[$key]['discounts'] = $discounts;
    }

    $all = array(
        "siblings" => $siblings,
        "nonConsideredSibs" => $nonConsideredSibs,
        );

    return $all;
}
}