<?php
class Hci_subject_model extends CI_model
{

function get_subjectgrps()
{
	//$this->db->where('subgrp_status','A');
	$this->db->select('hci_subjectgroup.*,hci_scheme.schm_name');
	$this->db->join('hci_scheme','hci_scheme.schm_id=hci_subjectgroup.subgrp_schm','left');
	$subgrps = $this->db->get('hci_subjectgroup')->result_array();

	return $subgrps;
}

function save_subjectgroup()
{
	$subgrp_id 	= $this->input->post('subgrp_id');
	$subgrp_name 	= $this->input->post('subgrp_name');

	$subgrpsv['subgrp_name'] = $subgrp_name;
	$subgrpsv['subgrp_status'] = 'A';
	$subgrpsv['subgrp_type'] = $this->input->post('subgrp_type');
	$subgrpsv['subgrp_schm'] = $this->input->post('subgrp_schm');

	if(empty($subgrp_id))
	{
		$save = $this->db->insert('hci_subjectgroup',$subgrpsv);
	}
	else
	{
		$this->db->where('subgrp_id',$subgrp_id);
		$save = $this->db->Update('hci_subjectgroup',$subgrpsv);
	}
	
	return $save;
}

function change_subgrpstatus()
{
	$this->db->where('subgrp_id',$this->input->post('subgrp_id'));
	$result = $this->db->update('hci_subjectgroup',array('subgrp_status'=>$this->input->post('new_s')));

	if($result)
	{
		if($this->input->post('new_s')=='A')
		{
			$this->session->set_flashdata('flashSuccess', 'Subject group Activated Successfully.');
		}
		else
		{
			$this->session->set_flashdata('flashSuccess', 'Subject group Deactivated Successfully.');
		}
		
	}
	else
	{
		if($this->input->post('new_s')=='A')
		{
			$this->session->set_flashdata('flashError', 'Failed to Activate Subject group. Retry.');
		}
		else
		{
			$this->session->set_flashdata('flashError', 'Failed to Deactivate Subject group. Retry.');
		}
	}

	return $result;
}

function load_schemelist()
{
	$this->db->where('schm_section',$this->input->post('id'));
	$schemes = $this->db->get('hci_scheme')->result_array();

	$i = 0;
	foreach ($schemes as $schm) 
	{
		$this->db->where('subgrp_schm',$schm['schm_id']);
		$subjgrp = $this->db->get('hci_subjectgroup')->result_array();

		$schemes[$i]['subjgrp'] = $subjgrp;
		$i++;
	}

	return $schemes;
}

function get_curriculum()
{
	$curriculums = $this->db->get('hci_curriculum')->result_array();

	return $curriculums;
}

function save_subject()
{
	$sub_id = $this->input->post('sub_id');
	$sub_name = $this->input->post('sub_name');
	$sub_curriculum = $this->input->post('sub_curriculum');
	$sub_type = $this->input->post('sub_type');
	$sub_mainorsel = $this->input->post('sub_mainorsel');
	$ssubid = $this->input->post('ssubid');
	$ssubname = $this->input->post('ssubname');
	$schemecheck = $this->input->post('schemecheck');
	$sub_section = $this->input->post('sub_section');

	$this->db->trans_begin();

	$subsv['sub_name'] = $sub_name;
	$subsv['sub_curriculum'] = $sub_curriculum;
	$subsv['sub_type'] = $sub_type;
	// $subsv['sub_mainorsel'] = $sub_mainorsel;
	$subsv['sub_section'] = $sub_section;
	$subsv['sub_status'] = 'A';

	if(empty($sub_id))
	{
		$this->db->insert('hci_subject',$subsv);
		$sub_id = $this->db->insert_id();
	}
	else
	{
		$this->db->where('sub_id',$sub_id);
		$this->db->update('hci_subject',$subsv);
	}

	$this->db->where('ssub_subject',$sub_id);
	$this->db->update('hci_subsubject',array('ssub_status'=>'D'));

	foreach ($ssubid as $key => $ssub) 
	{ 	
		if(!empty($ssub))
		{
			$this->db->where('ssub_id',$key);
			$this->db->update('hci_subsubject',array('ssub_status'=>'A','ssub_name'=>$this->input->post('ssubname_'.$key)));
		}
	}

	foreach ($ssubname as $ssub) 
	{
		if(!empty($ssub))
		{
			$ssubsv['ssub_name'] = $ssub;
			$ssubsv['ssub_subject'] = $sub_id;
			$ssubsv['ssub_status'] = 'A';

			$this->db->insert('hci_subsubject',$ssubsv);
		}
	}

	$this->db->where('subschm_subject',$sub_id);
	$this->db->update('hci_subjectscheme',array('subschm_status'=>'D'));

	$this->db->where('subjgrp_subject',$sub_id);
	$this->db->update('hci_subjsgroup',array('subjgrp_status'=>'D'));
	
	foreach ($schemecheck as $scheme) 
	{
		$this->db->select('subschm_id');
		$this->db->where('subschm_subject',$sub_id);
		$this->db->where('subschm_scheme',$scheme);
		$subschm = $this->db->get('hci_subjectscheme')->row_array();

		$schmsubsv['subschm_subject'] = $sub_id;
		$schmsubsv['subschm_scheme'] = $scheme;
		$schmsubsv['subschm_status'] = 'A';
		$schmsubsv['subschm_subgroup'] = $this->input->post('schm_'.$scheme);

		if(empty($subschm))
		{
			$this->db->insert('hci_subjectscheme',$schmsubsv);
		}
		else
		{
			$this->db->where('subschm_id',$subschm['subschm_id']);
			$this->db->update('hci_subjectscheme',$schmsubsv);
		}
	}

	if ($this->db->trans_status() === FALSE)
	{
	    $this->db->trans_rollback();
		return false;
	}
	else
	{
		$this->db->trans_commit();
		return true;	
	}
}

function get_subjects()
{
	$this->db->select('hci_subject.*,hci_curriculum.cur_name,hci_section.sec_name');
	$this->db->join('hci_curriculum','hci_curriculum.cur_id=hci_subject.sub_curriculum');
	$this->db->join('hci_section','hci_section.sec_id=hci_subject.sub_section');
	//$this->db->where('hci_section.sub_status','A');
	$subjects = $this->db->get('hci_subject')->result_array();

	return $subjects;
}

function change_substatus()
{
	$this->db->where('sub_id',$this->input->post('sub_id'));
	$result = $this->db->update('hci_subject',array('sub_status'=>$this->input->post('new_s')));

	if($result)
	{
		if($this->input->post('new_s')=='A')
		{
			$this->session->set_flashdata('flashSuccess', 'Subject Activated Successfully.');
		}
		else
		{
			$this->session->set_flashdata('flashSuccess', 'Subject Deactivated Successfully.');
		}
		
	}
	else
	{
		if($this->input->post('new_s')=='A')
		{
			$this->session->set_flashdata('flashError', 'Failed to Activate Subject. Retry.');
		}
		else
		{
			$this->session->set_flashdata('flashError', 'Failed to Deactivate Subject. Retry.');
		}
	}

	return $result;
}

function edit_sub_load()
{
	$sub_id = $this->input->post('sub_id');

	$this->db->where('sub_id',$sub_id);
	$subject = $this->db->get('hci_subject')->row_array();

	$this->db->where('ssub_subject',$sub_id);
	$this->db->where('ssub_status','A');
	$subsubjects = $this->db->get('hci_subsubject')->result_array();

	$this->db->where('subschm_subject',$sub_id);
	$this->db->where('subschm_status','A');
	$schemes = $this->db->get('hci_subjectscheme')->result_array();

	$all = array(
		"subject" => $subject,
		"subsubjects" => $subsubjects,
		"schemes" => $schemes,
		);

	return $all;
}
}