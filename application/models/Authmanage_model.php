<?php
class Authmanage_model extends CI_model {

function get_function_info()
{
	$this->db->select('*');
	$this->db->where('func_id',$this->input->post('id'));
	$funcinfo = $this->db->get('hgc_authfunction')->row_array();

	return $funcinfo;
}

function update_function_details()
{
	$func_id 	 = $this->input->post('func_id');
	$funcmodule  = $this->input->post('funcmodule');
	$submodule 	 = $this->input->post('submodule');
	$description = $this->input->post('description');
	$func_type   = $this->input->post('func_type');
	$ui_type 	 = $this->input->post('ui_type');

	$this->db->where('func_module',$funcmodule);
	$mcount = $this->db->count_all_results('hgc_authfunction');

	$updatefunc['func_module']   	= $funcmodule;
	$updatefunc['func_submodule'] 	= $submodule;
	$updatefunc['func_description'] = $description;
	$updatefunc['func_type'] 		= $func_type;
	$updatefunc['func_uitype'] 		= $ui_type;
	$updatefunc['func_status'] 		= 'A';

	if(isset($_POST['editable']))
	{
		$updatefunc['func_isedit'] = 1;
	}
	else
	{
		$updatefunc['func_isedit'] = 0;
	}

	$this->db->where('func_id',$func_id);
	$this->db->update('hgc_authfunction',$updatefunc);
}

function load_submodule()
{
	$this->db->distinct('func_submodule');
	$this->db->select('func_submodule');
	$this->db->where('func_isedit',1);
	$this->db->like('func_module',$this->input->post('mainmod'));
	$submods = $this->db->get('hgc_authfunction')->result_array();

	return $submods;
}

function get_pre_data()
{
	$this->db->distinct('func_module');
	$this->db->select('func_module');
	$this->db->where('func_isedit',1);
	$modules = $this->db->get('hgc_authfunction')->result_array();

	$functions = $this->db->get('hgc_authfunction')->result_array();

	$all['modules'] = $modules;
	$all['functions'] = $functions;

	return $all;
}

function search_right()
{
	$group  = $this->input->post('group');
	$module = $this->input->post('module');
	$branch = $this->input->post('branch');

	$this->db->distinct('func_module');
	$this->db->select('func_module');
	$this->db->where('func_isedit',1);
	if($module!='all')
	{
		$this->db->where('func_module',$module);
	}
	$modules = $this->db->get('hgc_authfunction')->result_array();

	$y = 0;
	foreach ($modules as $mod) 
	{
		$this->db->select('*');
		$this->db->where('func_status','A');
		$this->db->where('func_isedit',1);
		$this->db->where('func_module',$mod['func_module']);
		$this->db->order_by('func_submodule');	
		$functions = $this->db->get('hgc_authfunction')->result_array();

		$x = 0;
		foreach ($functions as $func) 
		{
			$this->db->select('*');
			$this->db->where('rgt_refid',$func['func_id']);
			$this->db->where('rgt_type','function');
			$this->db->where('rgt_usergroup',$group);
			$this->db->where('rgt_branch',$branch);
			$this->db->where('rgt_status','A');
			$right = $this->db->get('hgc_authright')->row_array();

			if(empty($right))
			{
				$functions[$x]['rgt_id'] 	= '';
				$functions[$x]['rgt_hasrgt']= '';
			}
			else
			{
				$functions[$x]['rgt_id'] 	= $right['rgt_id'];
				$functions[$x]['rgt_hasrgt']= $right['rgt_status'];
			}
			$x++;
		}

		$modules[$y]['functions'] = $functions;
		$y++; 
	}
	
	return $modules;
}

function set_rights()
{
	$module = $this->input->post('module');
	$group  = $this->input->post('group');
	$funcs  = $this->input->post('ar_check');
	$branch = $this->input->post('branch');

	$this->db->trans_begin();

	if($module=='all')
	{
		$this->db->where('rgt_usergroup',$group);
		$this->db->where('rgt_branch',$branch);
		$this->db->where('rgt_type','function');
		$this->db->update('hgc_authright',array('rgt_status'=>'D'));
	}
	else
	{
		$this->db->where('func_module',$module);	
		$functions = $this->db->get('hgc_authfunction')->result_array();

		foreach ($functions as $fun) 
		{
			$this->db->where('rgt_usergroup',$group);
			$this->db->where('rgt_type','function');
			$this->db->where('rgt_branch',$branch);
			$this->db->where('rgt_refid',$fun['func_id']);
			$this->db->update('hgc_authright',array('rgt_status'=>'D'));
		}
	}

	foreach ($funcs as $func) 
	{
		$this->db->where('rgt_usergroup',$group);
		$this->db->where('rgt_type','function');
		$this->db->where('rgt_refid',$func);
		$this->db->where('rgt_branch',$branch);
		$rgtexist = $this->db->get('hgc_authright')->row_array();

		$savergt['rgt_status'] = 'A';

		if(empty($rgtexist))
		{
			$savergt['rgt_usergroup'] = $group;
			$savergt['rgt_type'] = 'function';
			$savergt['rgt_refid'] = $func;
			$savergt['rgt_branch'] = $branch;

			$this->db->insert('hgc_authright',$savergt);
		}
		else
		{
			$this->db->where('rgt_id',$rgtexist['rgt_id']);
			$this->db->update('hgc_authright',$savergt);
		}
		
	}

	if ($this->db->trans_status() === FALSE)
    {
        $this->db->trans_rollback();
        $this->session->set_flashdata('flashError', 'Failed to Register Function. Retry.');
        return false;
    }
    else
    {
        $this->db->trans_commit();
        $this->session->set_flashdata('flashSuccess', 'Function Registered successfully.');
        return true;    
    }
}

}