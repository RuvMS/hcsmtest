
<?php
class Hci_attendance_model extends CI_model
{
function load_classlist()
{
	$grade = $this->input->post('grade');
    $acyear = $this->input->post('acyear');
    $branch = $this->input->post('branch');

	$this->db->select('*');
    $this->db->where('cls_academicyear',$acyear);
    $this->db->where('cls_grade',$grade);
    $this->db->where('cls_branch',$branch);
    $this->db->where('cls_status','A');
    $classes = $this->db->get('hci_class')->result_array();

    return $classes;
}	

function search_attendance()
{
	$grade = $this->input->post('grade');
    $acyear = $this->input->post('acyear');
    $date = $this->input->post('date');
    $clss = $this->input->post('clss');
    $branch = $this->input->post('branch');

	$this->db->select('hci_classstudent.*,st_details.st_id,st_details.family_name,st_details.other_names');
	$this->db->join('st_details','st_details.id=hci_classstudent.clsstu_student');
	$this->db->where('hci_classstudent.clsstu_ayear',$acyear);
	$this->db->where('hci_classstudent.clsstu_grade',$grade);
	$this->db->where('hci_classstudent.clsstu_class',$clss);
	$this->db->where('hci_classstudent.clsstu_branch',$branch);
	$this->db->where('hci_classstudent.clsstu_status','A');
	$stulist = $this->db->get('hci_classstudent')->result_array();

	$x = 0;
	foreach ($stulist as $stu) 
	{
		$this->db->where('ssatn_ayear',$acyear);
		$this->db->where('ssatn_grade',$grade);
		$this->db->where('ssatn_class',$clss);
		$this->db->where('ssatn_date',$date);
		$this->db->where('ssatn_student',$stu['clsstu_student']);
		$attendance = $this->db->get('hci_schoolattendance')->row_array();

		if(empty($attendance))
		{
			$stulist[$x]['isabsent'] = 1;
		}
		else
		{
			if($attendance['ssatn_isabsent']==1)
			{
				$stulist[$x]['isabsent'] = 1;
			}
			else
			{
				$stulist[$x]['isabsent'] = 0;
			}
		}
		$x++;
	}

	return $stulist;
}

function save_attendance()
{
	$grade = $this->input->post('cls_grade');
    $acyear = $this->input->post('cls_academicyear');
    $date = $this->input->post('attendate');
    $clss = $this->input->post('classselect');

    $this->db->trans_begin();

    $this->db->where('ssatn_ayear',$acyear);
    $this->db->where('ssatn_grade',$grade);
    $this->db->where('ssatn_class',$clss);
    $this->db->where('ssatn_date',$date);
    $this->db->update('hci_schoolattendance',array('ssatn_isabsent'=>0,'ssatn_remarks'=>''));

    if(isset($_POST['atnstudent']) && isset($_POST['stuchk']))
    {
    	$stulist = $this->input->post('atnstudent');
    	$remarks = $this->input->post('remarks');
    	$stuchk = $this->input->post('stuchk');

    	foreach ($stuchk as $stu) 
    	{
    		$this->db->where('ssatn_ayear',$acyear);
    		$this->db->where('ssatn_grade',$grade);
    		$this->db->where('ssatn_class',$clss);
    		$this->db->where('ssatn_date',$date);
    		$this->db->where('ssatn_student',$stu);
    		$isexist = $this->db->get('hci_schoolattendance')->row_array();


			$atn['ssatn_isabsent'] = 1;
			$atn['ssatn_remarks'] = $remarks[$stu];

    		if(empty($isexist))
    		{
    			$atn['ssatn_ayear'] = $acyear;
				$atn['ssatn_grade'] = $grade;
				$atn['ssatn_class'] = $clss;
				$atn['ssatn_date'] 	= $date;
				$atn['ssatn_student'] = $stu;

				$this->db->insert('hci_schoolattendance',$atn);
    		}
    		else
    		{
    			$this->db->where('ssatn_id',$isexist['ssatn_id']);
    			$this->db->update('hci_schoolattendance',$atn);
    		}
    	}

    }
    else
    {

    }

    if ($this->db->trans_status() === FALSE)
    {
        $this->db->trans_rollback();
        $res['status'] = 'Failed';
        $res['message'] = 'Failed to save attendance';
    }
    else
    {
        $this->db->trans_commit();
        $res['status'] = 'success';
        $res['message'] = 'Attendance saved successfully.';   
    }
    return $res;
}
}