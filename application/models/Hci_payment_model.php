<?php
class Hci_payment_model extends CI_model
{

function save_payment()
{
    $use_coh_bal = 0;
	$this->db->trans_begin();

    $this->db->where('br_id',$this->input->post('payment_branch'));
    $branch = $this->db->get('hgc_branch')->row_array();
    $user = $this->session->userdata('u_id');
	
    $paysv['pay_index'] = $this->sequence->generate_sequence('PAY'.$branch['br_code'].date('y'),5); 
	$paysv['pay_date'] = $rec['rec_date'] = date('Y-m-d');
    $paysv['pay_custype'] = $rec['rec_custype'] = $this->input->post('pay_custype');
	$paysv['pay_customer'] = $rec['rec_customer'] = $this->input->post('pay_customer');
	$paysv['pay_amount'] = $rec['rec_amount'] = $this->input->post('pay_totamt');
    $paysv['pay_outstanding'] = $rec['rec_outstanding'] = $this->input->post('pay_outstands');
    $paysv['pay_cohbalance'] = $rec['rec_cohvbalance'] = $this->input->post('pay_cohbalance');
    $paysv['pay_type'] = $rec['rec_type'] = $this->input->post('inv_type');
    $paysv['pay_description'] = $rec['rec_description'] = $this->input->post('pay_description');
    $paysv['pay_createuser'] = $rec['rec_createduser'] = $user;
    $paysv['pay_createdate'] = $rec['rec_createddate'] = date('Y-m-d h:i:sa');
    $paysv['pay_branch'] = $rec['rec_branch'] = $this->input->post('payment_branch');
    $paysv['pay_remarks'] = $rec['rec_remarks'] = $this->input->post('pay_remarks');

    // if(isset($_POST['btn_type']))
    // {
    //     $paysv['pay_status'] = 'T';
    // }
    // else
    // {
        $paysv['pay_status'] = 'P';
    // }

    $this->db->insert('hci_payment',$paysv);
    $pay_id = $this->db->insert_id();

    if($paysv['pay_custype']=="STUDENT")
    {
        $reftable = 'st_details';
        $this->db->select('id as "cus_id",st_id as "cus_index",other_names as "second_name",family_name as "first_name"');
        $this->db->where('id',$paysv['pay_customer']);
    }
    else if($paysv['pay_custype']=="TEMPSTU")
    {
        $reftable = 'st_details_temp';
        $this->db->select('id as "cus_id",st_id as "cus_index",other_names as "second_name",family_name as "first_name"');
        $this->db->where('id',$paysv['pay_customer']);
    }
    else if($paysv['pay_custype']=="STAFF")
    {
        $reftable = 'hgc_staff';
        $this->db->select('stf_id as "cus_id",stf_index as "cus_index",stf_lastname as "second_name",stf_firstname as "first_name"');
        $this->db->where('stf_id',$paysv['pay_customer']);
    }
    else
    {
        // $reftable = 'hgc_extcustomer';
        // $this->db->where('id',$paysv['pay_customer']);
    }
    $customer = $this->db->get($reftable)->row_array();

    $rec['rec_payid'] = $pay_id;
    $rec['rec_index'] = $this->sequence->generate_sequence('REC'.$branch['br_code'].date('y'),5);
    $rec['rec_cusindex'] = $customer['cus_index'];
    $rec['rec_cusname'] = $customer['first_name'].' '.$customer['second_name'];
    $rec['rec_compname']     = $this->session->userdata('u_compname').' - '.$branch['br_name'];
    $rec['rec_compaddline1'] = $branch['br_addl1'];
    $rec['rec_compaddline2'] = $branch['br_addl2'];
    $rec['rec_compaddline3'] = $branch['br_city'];
    $rec['rec_createdusername'] = $this->session->userdata('u_name');

    $this->db->insert('hci_receipt',$rec);
    $rec_id = $this->db->insert_id();

	if($paysv['pay_type'] == 'INV')
	{
		$invoiceary = $_POST['invoice'];
        $inv_payment    = $paysv['pay_amount'];
        $inv_onbalance  = $paysv['pay_cohbalance'];

        $inv_total = 0;

        if(!empty($invoiceary))
        {
            foreach ($invoiceary as $key => $value) 
            {
                if($value>0)
                {
                    $old_balance = $this->input->post('invbal_'.$key);

                    $payinv['payinv_payment'] = $pay_id;
                    $payinv['payinv_invoice'] = $key;
                    
                    if($inv_payment>$old_balance)
                    {
                        $payinv['payinv_amount'] = $old_balance;
                        $payinv['payinv_usedbalance'] = 0;
                        $inv_payment -= $old_balance;
                    }
                    else
                    {
                        $payinv['payinv_amount'] = $inv_payment;

                        if(isset($_POST['usecoh']))
                        {
                            $use_coh_bal = 1;
                            if($inv_onbalance>($old_balance-$inv_payment))
                            {
                                $payinv['payinv_usedbalance'] = $old_balance-$inv_payment;
                                $inv_onbalance -= $old_balance-$inv_payment;
                            }
                            else
                            {
                                $payinv['payinv_usedbalance'] = $inv_onbalance;
                                $inv_onbalance = 0;
                            }
                        }
                        else
                        {
                            $payinv['payinv_usedbalance'] = 0;
                        }
                        $inv_payment = 0;
                    }

                    $this->db->insert('hci_paymentinvoice',$payinv);

                    $this->db->set('inv_paidamount','inv_paidamount+'.$payinv['payinv_amount'],FALSE);
                    $this->db->set('inv_usedohbalance','inv_usedohbalance+'.$payinv['payinv_usedbalance'],FALSE);
                    $this->db->set('inv_balanceamount','inv_balanceamount-'.($payinv['payinv_amount']+$payinv['payinv_usedbalance']),FALSE);
                    if(($old_balance-($payinv['payinv_amount']+$payinv['payinv_usedbalance']))<=0)
                    {
                        $this->db->set('inv_ispaid',1);
                    }
                    $this->db->where('inv_id',$key);
                    $this->db->update('hci_invoice');

                    $inv_total += $value;
                }
            }
        }

        if($inv_total>$payinv['payinv_amount'] && isset($_POST['usecoh']))
        {
            $usedbal = $inv_total-$payinv['payinv_amount'];

            if($usedbal > 0)
            {
                $this->db->where('pay_id',$pay_id);
                $this->db->update('hci_payment',array('pay_usedbalance'=>$usedbal));

                $this->db->where('rec_id',$rec_id);
                $this->db->update('hci_receipt',array('rec_usedbalance'=>$usedbal));
            }
        }
	}
    else
    {
        $inv_total = 0;
    }

	if(isset($_POST['paymode']))
	{
        $gl_code = '0201';
        $cashamt['paymeth_method'] = $_POST['paymode'];
		$cashamt['paymeth_amount'] = $paysv['pay_amount'];
		$cashamt['paymeth_payment']= $pay_id;

        if($_POST['paymode'] == 'cheque')
        {
            $gl_code = '0205';
            $cashamt['paymeth_ref'] = $this->input->post('chequeno');
            $cashamt['paymeth_refdate'] = $this->input->post('chequedate');

            $cheque['cheq_number'] = $this->input->post('chequeno');
            $cheque['cheq_date'] = $this->input->post('chequedate');
            $cheque['cheq_status'] = 'P';
            $cheque['cheq_receipt'] = $rec['rec_index'];
            $cheque['cheq_payment'] = $pay_id;
            $cheque['cheq_datedto'] = $this->input->post('chequedateddate');
            $cheque['cheq_bank'] = $this->input->post('chequebank');
            $cheque['cheq_branch'] = $this->input->post('payment_branch');

            $this->db->insert('hci_cheque',$cheque);
        }

        if($_POST['paymode'] == 'dirdeposit')
        {
            $gl_code = '0150';
            $cashamt['paymeth_ref'] = $this->input->post('receiptno');
            $cashamt['paymeth_refdate'] = $this->input->post('bankdate');
            $cashamt['paymeth_bank'] = $this->input->post('bankdate');
            $cashamt['paymeth_bankaccount'] = $this->input->post('bankdate');
        }

        if($_POST['paymode'] == 'card')
        {
            $gl_code = '0400';
            $cashamt['paymeth_ref'] = $this->input->post('cardno');
            $cashamt['paymeth_bank'] = $this->input->post('machinebank');

            $this->db->where('card_number',$cashamt['paymeth_ref']);
            $cardexist = $this->db->get('hci_card')->row_array();

            if(empty($cardexist))
            {
                $card['card_deorcr'] = 'CREDIT';
                $card['card_type'] = $this->input->post('cardtype');
                $card['card_number'] = $this->input->post('cardno');
                $card['card_issueingbank'] = $this->input->post('cardbank');
                $card['card_owner'] = $paysv['pay_customer'];

                $this->db->insert('hci_card',$card);
            }
        }

        if($_POST['paymode']=='nondirect')
        {
            $gl_code = '4855';
            $cashamt['paymeth_ref'] = $this->input->post('receiptno');
            $cashamt['paymeth_refdate'] = $this->input->post('bankdate');
            $cashamt['paymeth_bank'] = $this->input->post('bankdate');
            $cashamt['paymeth_bankaccount'] = $this->input->post('bankdate');
        }

		$this->db->insert('hci_paymentmethod',$cashamt);
	}


    $this->load->model('hci_accounts_model');
    $this->hci_accounts_model->update_transaction($paysv['pay_custype'],$paysv['pay_customer'],$pay_id,'PAYMENT',$paysv['pay_description'],$paysv['pay_date'],'CR',$paysv['pay_amount'],$user,$paysv['pay_cohbalance'],$paysv['pay_createdate'],$rec['rec_createdusername'],$branch['br_code'],$use_coh_bal);
    $this->hci_accounts_model->accountsErpIntegration($gl_code,'ded',$paysv['pay_amount'],$pay_id,'P');

	// if(isset($_POST['btn_type']))
 //    {
 //    	$erp_db = $this->load->database('erp_db', TRUE); 
	//     $br_pref = '3';

	//     $erp_db->select('value');
	//     $erp_db->where('name','f_year');
	//     $f_year = $erp_db->get($br_pref.'_sys_prefs')->row_array();

 //        $erp_db->where('trans_type',0);
 //        $pattern = $erp_db->get($br_pref.'_reflines')->row_array();

 //        $refs['id'] = $pattern['pattern'];
 //        $refs['type'] = 0;
 //        $refs['reference'] = $pattern['pattern'];
 //        $erp_db->insert($br_pref.'_refs',$refs);

 //        $this->db->where('pay_id',$pay_id);
 //        $this->db->update('hci_payment',array('pay_refsid' => $pattern['pattern']));

 //        $autrail['type'] = 0;
 //        $autrail['trans_no'] = $pattern['pattern'];
 //        $autrail['user'] = 1;
 //        $autrail['fiscal_year'] = $f_year['value'];
 //        $autrail['gl_date'] = date('Y-m-d');
 //        $autrail['gl_seq'] = 0;
 //        $erp_db->insert($br_pref.'_audit_trail',$autrail);
 //        $gl_transid = $erp_db->insert_id();

 //        $this->db->where('pay_id',$pay_id);
 //        $this->db->update('hci_payment',array('pay_gltransid' => $gl_transid));

 //        $erp_db->set('pattern', 'pattern+1', FALSE);
 //        $erp_db->where('trans_type',0);
 //        $erp_db->update($br_pref.'_reflines');

 //        $gl_trans['type'] = 0;
 //        $gl_trans['type_no'] = $gl_transid;
 //        $gl_trans['tran_date'] = date('Y-m-d');
 //        $gl_trans['dimension_id'] = 0;
 //        $gl_trans['dimension2_id'] = 0;
 //        $gl_trans['account'] = '0054';
 //        $gl_trans['amount'] = $this->input->post('pay_totamt');
        
 //        $erp_db->insert($br_pref.'_gl_trans',$gl_trans);
 //        $grlupdateid = $erp_db->insert_id();

 //        $this->db->where('pay_id',$pay_id);
 //        $result = $this->db->update('hci_payment',array('pay_grlupdateid' => $grlupdateid,'pay_status'=>'T'));
	// }

	if ($this->db->trans_status() === FALSE)
    {
        $this->db->trans_rollback();
        $this->msg->set('admission', "Failed to process Receipt. retry");
        return false;
    }
    else
    {
        $this->db->trans_commit(); 
        $this->msg->set('admission', "Receipt processed successfully");
        return $pay_id;
    }
}

function get_branchdetails()
{
    $this->db->where('br_id',$this->session->userdata('u_branch'));
    $branch = $this->db->get('hgc_branch')->row_array();
}

function load_payments()
{
    $draw = $_POST["draw"];//counter used by DataTables to ensure that the Ajax returns from server-side processing requests are drawn in sequence by DataTables
    $orderByColumnIndex  = $_POST['order'][0]['column'];// index of the sorting column (0 index based - i.e. 0 is the first record)
    $orderBy = $_POST['columns'][$orderByColumnIndex]['data'];//Get name of the sorting column from its index
    $orderType = $_POST['order'][0]['dir']; // ASC or DESC
    $start  = $_POST["start"];//Paging first record indicator.
    $length = $_POST['length'];//Number of records that the table can display in the current draw
    $stat = $_POST['stat'];

    $brlist = $this->auth->get_accessbranch();

    $this->db->select('*');
    if($stat == 't')
    {
        $this->db->where('pay_status','T');
    }  
    else if($stat == 'p')
    {
        $this->db->where('pay_status','p');
    }
    $this->db->where_in('pay_branch',$brlist);
    $this->db->where('pay_id >=',$start);
    $this->db->limit($length);
    $this->db->order_by($orderBy,$orderType);
    $payments = $this->db->get('hci_payment')->result_array();

    $x = 0; 
    foreach ($payments as $payment) 
    {
        $this->db->select('*');
        $this->db->where('rec_payid',$payment['pay_id']);
        $rec = $this->db->get('hci_receipt')->row_array();

        $payments[$x]['customer'] = '[ '.$rec['rec_cusindex'].' ] - '.$rec['rec_cusname'];
        $payments[$x]['rec_no'] = $rec['rec_index'];

        // $payments[$x]['actions'] = '<div class="btn-row"><div class="btn-group">'.
        //                             '<button type="button" class="btn btn-primary btn-xs" onclick="event.preventDefault();load_stuprofview('.$stu['id'].',\''.$reftbl.'\')">view Profile</button>'.
        //                             '<div class="btn-group">'.
        //                             '<button type="button" class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown">'.
        //                             'Actions'.
        //                             '<span class="caret"></span>'.
        //                             '</button>'.
        //                             '<ul class="dropdown-menu">'.
        //                             '<li><a href="#" onclick="event.preventDefault();load_stueditview('.$stu['id'].',\''.$reftbl.'\')">Edit</a></li>'.
        //                             '<li><a href="#" onclick="event.preventDefault();load_assigngrade('.$stu['id'].',\''.$reftbl.'\')">Assign Grade</a></li>'.
        //                             '<li><a href="#" onclick="event.preventDefault();load_paymentinfo('.$stu['id'].',\''.$reftbl.'\')">Payment Info</a></li>'.
        //                             '<li><a href="#" onclick="event.preventDefault();load_sturegister('.$stu['id'].',\''.$reftbl.'\')">Register</a></li>'.
        //                             '</ul>'.
        //                             '</div>'.
        //                             '</div></div>';

        if($payment['pay_status'] == 'P')
        {
            $editbtn = ' | <button type="button" class="btn btn-info btn-xs" onclick="event.preventDefault();edit_payment('.$payment['pay_id'].')"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button>';
            $procbtn = ' | <button type="button" class="btn btn-info btn-xs" onclick="event.preventDefault();process_payment('.$payment['pay_id'].')"><span class="glyphicon glyphicon-transfer" aria-hidden="true"></span></button>';
        }
        else
        {
            $editbtn = '';
            $procbtn = '';
        }

        $payments[$x]['actions'] = '<button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#myModal" onclick="event.preventDefault();view_paymentrecipt('.$payment['pay_id'].')"><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span></button>'.
								' | <button type="button" class="btn btn-success btn-xs" onclick="event.preventDefault();print_receipt('.$payment['pay_id'].')"><span class="glyphicon glyphicon-print" aria-hidden="true"></span></button>'.$editbtn.$procbtn;
        $x++;
    }

    $outputary = array();

    foreach ($payments as $key => $value) 
    {
        if(!empty($_POST['search']['value']))
        {
            $display = false;
            for($i=0 ; $i<count($_POST['columns']);$i++){

                if($_POST['columns'][$i]['data']!='actions')
                {
                    $column = $_POST['columns'][$i]['data'];//we get the name of each column using its index from POST request

                    $strexist = stripos($value[$column],$_POST['search']['value']);

                    if($strexist)
                    {
                        $outputary[] = $value;
                    }
                    
                }
            }
        }
        else
        {
            $outputary[] = $value;
        }
    }

    $count = count($outputary);

	$output = array(
        "draw" => intval($draw),
	    "recordsTotal" => $count,
	    "recordsFiltered" => $count,
	    "data" => $outputary
    );

    return $output;
}

function load_payment_details($pay)
{
    $brlist = $this->auth->get_accessbranch();

    $this->db->select('*');
    $this->db->where('pay_id',$pay);
    $this->db->where_in('pay_branch',$brlist);
    $payment = $this->db->get('hci_payment')->row_array();

    $receipt = array();
    $invoice = array();
    $methpd = array();
    $cheque = array();
    $card = array();

    if(!empty($payment))
    {
        $this->db->select('*');
        $this->db->where('rec_payid',$pay);
        $receipt = $this->db->get('hci_receipt')->row_array();

        $this->db->select('hci_paymentinvoice.*,hci_invoice.inv_description,hci_invoice.inv_index');
        $this->db->join('hci_invoice','hci_invoice.inv_id=hci_paymentinvoice.payinv_invoice','left');
        $this->db->where('hci_paymentinvoice.payinv_payment',$pay);
        $invoice = $this->db->get('hci_paymentinvoice')->result_array();

        $this->db->where('paymeth_payment',$pay);
        $methpd = $this->db->get('hci_paymentmethod')->row_array();

        $this->db->where('cheq_payment',$pay);
        $cheque = $this->db->get('hci_cheque')->row_array();

        if($methpd['paymeth_method']=="card")
        {
            $this->db->where('card_number',$methpd['paymeth_ref']);
            $card = $this->db->get('hci_card')->row_array();
        }
    }

    $all = array(
        "payment" => $payment,
        "receipt" => $receipt,
        "invoice" => $invoice,
        "methpd"  => $methpd,
        "cheque"  => $cheque,
        "card"    => $card
        );

    return $all;
}

function load_customers()
{
    $type = $this->input->post('type');
    $branch = $this->input->post('branch');

    if($type=="STUDENT")
    {
        $reftable = 'st_details';
        $this->db->select('id as "cus_id",st_id as "cus_index",other_names as "second_name",family_name as "first_name"');
        $this->db->where('st_status','R');
        $this->db->where('st_branch',$branch);
    }
    else if($type=="TEMPSTU")
    {
        $reftable = 'st_details_temp';
        $this->db->select('id as "cus_id",st_id as "cus_index",other_names as "second_name",family_name as "first_name"');
        $this->db->where('st_branch',$branch);
    }
    else if($type=="STAFF")
    {
        $reftable = 'hgc_staff';
        $this->db->select('stf_id as "cus_id",stf_index as "cus_index",stf_lastname as "second_name",stf_firstname as "first_name"');
        $this->db->where('stf_branch',$branch);
    }
    else
    {
        $reftable = 'hgc_extcustomer';
    }

    $customers = $this->db->get($reftable)->result_array();

    return $customers;
}

function load_banklist()
{
    $this->db->select('*');
    $this->db->group_by('bnk_code');
    $banks =  $this->db->get('hgc_bank')->result_array();

    return $banks;
}

}