<?php
class Hci_accounts_model extends CI_model
{

function get_invoices()
{
    $brlist = $this->auth->get_accessbranch();

    var_dump($brlist);
    
    $this->db->select('*');
    $this->db->where_in('inv_branch',$brlist);
    $inv_info = $this->db->get('hci_invoice')->result_array();
    return $inv_info;
}

function load_invoicefees($inv)
{
    $brlist = $this->auth->get_accessbranch();

    $this->db->select('*');
    $this->db->where('inv_id',$inv);
    $this->db->where_in('inv_branch',$brlist);
    $invoice_data = $this->db->get('hci_invoice')->row_array();

    $invoice_fees = array();
    $invoice_disc = array();
    $payment_plan = array();

    if(!empty($invoice_data))
    {
        $this->db->select('*');
        $this->db->where('invf_invoice',$inv);
        $invoice_fees = $this->db->get('hci_invoicefee')->result_array();

        $this->db->where('id_invoice',$inv);
        $invoice_disc = $this->db->get('hci_stuinvdiscount')->result_array();

        $this->db->where('ipp_invoice',$inv);
        $payment_plan = $this->db->get('hci_cusinvpaymentplan')->result_array();
    }

    $all = array(
        'invoice_data' => $invoice_data,
        'invoice_fees' => $invoice_fees,
        'invoice_disc' => $invoice_disc,
        'payment_plan' => $payment_plan,
        );
    
    return $all;
}

function save_adjusment()
{
    $adj_type = $this->input->post('adj_type');
    $adj_id = $this->input->post('adj_id');
    $adj_name = $this->input->post('adj_name');
    $adj_fee = $this->input->post('adj_fee');
    $adj_appfor = $this->input->post('adj_appfor');
    $adj_calamttype = $this->input->post('adj_calamttype');
    $adj_amt = $this->input->post('adj_amt');
 
    $save_adj['adj_description'] = $adj_name; 
    $save_adj['adj_type']        = $adj_type; 
    $save_adj['adj_feecat']      = $adj_fee; 
    $save_adj['adj_applyfor']    = $adj_appfor; 
    $save_adj['adj_amttype']     = $adj_calamttype;   
    $save_adj['adj_amount']      = $adj_amt; 
    $save_adj['adj_status']      = 'A'; 
    $save_adj['adj_isspec']      = 0;

    $this->db->trans_begin();

    if(empty($adj_id))
    {
        $this->db->insert('hci_adjusment',$save_adj);
    }
    else
    {
        $this->db->where('adj_id',$adj_id);
        $this->db->update('hci_adjusment',$save_adj);
    }

    if ($this->db->trans_status() === FALSE)
    {
        $this->db->trans_rollback();
        return false;
    }
    else
    {
        $this->db->trans_commit();
        return true;    
    }
}

function load_adjusment()
{
    $this->db->select('*');
    $this->db->where('adj_isspec',0);
    $adjs = $this->db->get('hci_adjusment')->result_array();

    return $adjs;
}

function config_adjusments()
{
    $conf_id = $this->input->post('conf_id');
    $conf_adj = $this->input->post('conf_adj');
    $conf_appfor = $this->input->post('conf_appfor');
    $conf_appstu = $this->input->post('conf_appstu');
    $selstu = $this->input->post('selstu');
    $conf_fs = $this->input->post('conf_fs');
    $amts = $this->input->post('amts');
    $amtnew = $this->input->post('amtnew');
    $amtold = $this->input->post('amtold');
    $payplan = $this->input->post('adj_plan');
    $numofins = $this->input->post('numperiods');

    $this->db->trans_begin();

    $this->db->where('adj_id',$conf_adj);
    $adj_data = $this->db->get('hci_adjusment')->row_array();

    $save_temp['at_adjusment'] = $conf_adj;
    $save_temp['at_applyfor'] = $conf_appfor;
    $save_temp['at_students'] = $conf_appstu;
    $save_temp['at_feestructure'] = $conf_fs;
    $save_temp['at_paymentplan'] = $payplan;
    $save_temp['at_numofinstals'] = $numofins;
    ;
    if(empty($conf_id))
    {
        $this->db->insert('hci_adjstemplate',$save_temp);
        $conf_id = $this->db->insert_id();
    }
    else
    {
        $this->db->where('at_id',$conf_id);
        $this->db->update('hci_adjstemplate',$save_temp);

        $this->db->where('ats_adjstemp',$conf_id);
        $this->db->where('ats_reftable','st_details');
        $this->db->update('hci_adjsstudent',array('ats_status'=>'D'));

        $this->db->where('atf_adjstemp',$conf_id);
        $this->db->update('hci_adjsfees',array('aft_status'=>'D','aft_tot'=>null,'aft_new'=>null,'aft_old'=>null));
    }

    if($conf_appstu==2)
    {
        foreach ($selstu as $stu) 
        {
            $this->db->where('ats_adjstemp',$conf_id);
            $this->db->where('ats_student',$stu);
            $this->db->where('ats_reftable','st_details');
            $stu_ext = $this->db->get('hci_adjsstudent')->row_array();

            $stu_save['ats_status'] = 'A';

            if(empty($stu_ext))
            {
                $stu_save['ats_adjstemp'] = $conf_id;
                $stu_save['ats_student'] = $stu;
                $stu_save['ats_reftable'] = 'st_details';

                $this->db->insert('hci_adjsstudent',$stu_save);
            }
            else
            {
                $this->db->where('ats_id',$stu_ext['ats_id']);
                $this->db->update('hci_adjsstudent',$stu_save);
            }
        }
    }

    if($conf_appfor==2)
    {
        if($adj_data['adj_feecat']==1 || $adj_data['adj_feecat']==2 || $adj_data['adj_feecat']==3)
        {
            $this->db->select('es_admissionid');
            $this->db->where('es_seqnumber',2);
            $this->db->where('es_reftable','st_details');
            $sib2s = $this->db->get('hci_sibling')->result_array();

            foreach ($sib2s as $sib2) 
            {
                $this->db->where('ats_adjstemp',$conf_id);
                $this->db->where('ats_student',$sib2['es_admissionid']);
                $this->db->where('ats_reftable','st_details');
                $sib2_ext = $this->db->get('hci_adjsstudent')->row_array();

                $sib2_save['ats_status'] = 'A';

                if(empty($sib2_ext))
                {
                    $sib2_save['ats_adjstemp'] = $conf_id;
                    $sib2_save['ats_student'] = $sib2['es_admissionid'];
                    $sib2_save['ats_reftable'] = 'st_details';

                    $this->db->insert('hci_adjsstudent',$sib2_save);
                }
                else
                {
                    $this->db->where('ats_id',$sib2_ext['ats_id']);
                    $this->db->update('hci_adjsstudent',$sib2_save);
                }
            }
        }
    }

    if($conf_appfor==3)
    {
        if($adj_data['adj_feecat']==1 || $adj_data['adj_feecat']==2 || $adj_data['adj_feecat']==3)
        {
            $this->db->select('es_admissionid');
            $this->db->where('es_seqnumber',3);
            $this->db->where('es_reftable','st_details');
            $sib3s = $this->db->get('hci_sibling')->result_array();

            foreach ($sib3s as $sib3) 
            {
                $this->db->where('ats_adjstemp',$conf_id);
                $this->db->where('ats_student',$sib3['es_admissionid']);
                $this->db->where('ats_reftable','st_details');
                $sib3_ext = $this->db->get('hci_adjsstudent')->row_array();

                $sib3_save['ats_status'] = 'A';

                if(empty($sib3_ext))
                {
                    $sib3_save['ats_adjstemp'] = $conf_id;
                    $sib3_save['ats_student'] = $sib3['es_admissionid'];
                    $sib3_save['ats_reftable'] = 'st_details';

                    $this->db->insert('hci_adjsstudent',$sib3_save);
                }
                else
                {
                    $this->db->where('ats_id',$sib3_ext['ats_id']);
                    $this->db->update('hci_adjsstudent',$sib3_save);
                }
            }
        }
    }

    if($conf_appfor==4)
    {
        if($adj_data['adj_feecat']==1 || $adj_data['adj_feecat']==2 || $adj_data['adj_feecat']==3)
        {
            $this->db->select('es_admissionid');
            $this->db->where('es_seqnumber >=',4);
            $this->db->where('es_reftable','st_details');
            $sib4s = $this->db->get('hci_sibling')->result_array();

            foreach ($sib4s as $sib4) 
            {
                $this->db->where('ats_adjstemp',$conf_id);
                $this->db->where('ats_student',$sib4['es_admissionid']);
                $this->db->where('ats_reftable','st_details');
                $sib4_ext = $this->db->get('hci_adjsstudent')->row_array();

                $sib4_save['ats_status'] = 'A';

                if(empty($sib4_ext))
                {
                    $sib4_save['ats_adjstemp'] = $conf_id;
                    $sib4_save['ats_student'] = $sib4['es_admissionid'];
                    $sib4_save['ats_reftable'] = 'st_details';

                    $this->db->insert('hci_adjsstudent',$sib4_save);
                }
                else
                {
                    $this->db->where('ats_id',$sib4_ext['ats_id']);
                    $this->db->update('hci_adjsstudent',$sib4_save);
                }
            }
        }
    }

    if($conf_appfor==5 && !empty($conf_fs))
    {   
        foreach ($amts as $amt) 
        {
            $this->db->where('atf_adjstemp',$conf_id);
            $this->db->where('aft_fsfee',$amt);
            $amt_ext = $this->db->get('hci_adjsfees')->row_array();

            $amt_save['aft_status'] = 'A';

            if(empty($amt_ext))
            {
                $amt_save['atf_adjstemp'] = $conf_id;
                $amt_save['aft_fsfee'] = $amt;
                $amt_save['aft_tot'] = 1;
                // $amt_save['aft_old'] = $

                $this->db->insert('hci_adjsfees',$amt_save);
            }
            else
            {
                $this->db->where('atf_id',$amt_ext['atf_id']);
                $this->db->update('hci_adjsfees',$amt_save);
            }
        }

        foreach ($amtnew as $new) 
        {
            $this->db->where('atf_adjstemp',$conf_id);
            $this->db->where('aft_fsfee',$new);
            $new_saved = $this->db->get('hci_adjsfees')->row_array();

            $new_save['aft_status'] = 'A';
            $new_save['aft_new'] = 1;

            if(empty($new_saved))
            {
                $new_save['atf_adjstemp'] = $conf_id;
                $new_save['aft_fsfee'] = $new;  

                $this->db->insert('hci_adjsfees',$new_save);
            }
            else
            {
                $this->db->where('atf_id',$new_saved['atf_id']);
                $this->db->update('hci_adjsfees',$new_save);
            }
        }

        foreach ($amtold as $old) 
        {
            $this->db->where('atf_adjstemp',$conf_id);
            $this->db->where('aft_fsfee',$old);
            $old_saved = $this->db->get('hci_adjsfees')->row_array();

            $old_save['aft_status'] = 'A';
            $old_save['aft_old'] = 1;

            if(empty($old_saved))
            {
                $old_save['atf_adjstemp'] = $conf_id;
                $old_save['aft_fsfee'] = $old;  

                $this->db->insert('hci_adjsfees',$old_save);
            }
            else
            {
                $this->db->where('atf_id',$old_saved['atf_id']);
                $this->db->update('hci_adjsfees',$old_save);
            }
        }
    }

    if ($this->db->trans_status() === FALSE)
    {
        $this->db->trans_rollback();
        return false;
    }
    else
    {
        $this->db->trans_commit();
        return true;    
    }
}

function regenerate_invoice()
{
    $invoice = $this->input->post('id');
    $plans = $this->input->post('plans');

    $this->db->trans_begin();

    $this->db->where('inv_id',$invoice);
    $invdata = $this->db->get('hci_invoice')->row_array();

    $this->db->where('spp_student',$invdata['inv_student']);
    $this->db->update('hci_stupaymentplan',array('spp_status' => 'D'));

    foreach ($plans as $plan) 
    {
        $this->db->where('spp_student',$invdata['inv_student']);
        $this->db->where('spp_paymentplan',$plan);
        $isext = $this->db->get('hci_stupaymentplan')->row_array();

        if(empty($isext))
        {
            $plansave['spp_student'] = $invdata['inv_student'];
            $plansave['spp_paymentplan'] = $plan;
            $plansave['spp_status'] = 'A';
            $plansave['spp_reftable'] = 'st_details';

            $this->db->insert('hci_stupaymentplan',$plansave);
        }
        else
        {
            $this->db->where('spp_id',$isext['spp_id']);
            $this->db->update('hci_stupaymentplan',array('spp_status' => 'A'));
        }
    }

    $this->db->select('*');
    $this->db->join('hci_feestructure_fees','hci_feestructure_fees.fsf_id=hci_invoicefee.invf_fee','left');
    $this->db->where('hci_invoicefee.invf_invoice',$invdata['inv_id']);
    $this->db->where('hci_invoicefee.invf_reftype','C');

    $invoice_fees = $this->db->get('hci_invoicefee')->result_array();

    foreach ($invoice_fees as $fees) 
    {
        if($fees['fsf_fee']==2 || $fees['fsf_fee']==3)
        {
            $this->db->select('hci_paymentplan.*');
            $this->db->join('hci_paymentplan','hci_paymentplan.plan_id=hci_stupaymentplan.spp_paymentplan');
            $this->db->where('hci_stupaymentplan.spp_student',$invdata['inv_student']);
            $this->db->where('hci_paymentplan.plan_fee',$fees['fsf_fee']);
            $this->db->where('hci_stupaymentplan.spp_status','A');
            $payment_plan = $this->db->get('hci_stupaymentplan')->row_array();

            if($payment_plan['plan_type']=='M')
            {
                $save_fee['invf_amount'] = $fees['fsf_amt'];
            }
            else if($payment_plan['plan_type']=='T')
            {
                $save_fee['invf_amount'] = $fees['fsf_amt'];
            }
            else
            {
                $save_fee['invf_amount'] = $fees['fsf_amt']*3;
            }

            $this->db->where('invf_id',$fees['invf_id']);
            $this->db->update('hci_invoicefee',$save_fee);//transfered
        }
        
    }

    if($invdata['inv_status']=='T')
    {
        $this->db->where('inv_id',$invdata['inv_id']);
        $this->db->update('hci_invoice',array('inv_status' => 'R'));//transfered
    }

    if ($this->db->trans_status() === FALSE)
    {
        $this->db->trans_rollback();
        $this->msg->set('admission', "Failed to regenerate invoice. retry");
    }
    else
    {
        $this->db->trans_commit(); 
        $this->msg->set('admission', "Invoice regenerated successfully");
    }
}

function get_monthend_dates()
{
    $this->db->order_by('mend_id','DESC');
    $this->db->limit(1);
    $lastmonthend = $this->db->get('hci_monthend')->row_array();

    $monthenddate = 1;

    if(empty($lastmonthend))
    {
        $start_date = 'The Beginning of Time';
    }
    else
    {
        $lastmonthenddate = $lastmonthend['mend_edate'];
        $start_date = date("Y-m-d", strtotime("+1 day", strtotime($lastmonthenddate)));
    }  

    if(date('d')>=$monthenddate)
    {
        $last_date = date('Y-m-d',strtotime("-".(date('d')-$monthenddate)." day", strtotime(date("Y-m-d"))));
    }
    else
    {
        $last_date = date('Y-m-d', strtotime("-1 month", strtotime(date("Y-m-d"))));
        $last_date = date('Y-m-d',strtotime("+".($monthenddate-date('d',strtotime($last_date)))." day", strtotime($last_date)));
    }

    if($start_date!='The Beginning of Time')
    {
        if($last_date<$start_date)
        {
            $all = array();
        }
        else
        {
            $all = array(
                'start_date' => $start_date,
                'last_date'  => $last_date
            );
        }
    }
    else
    {
        $all = array(
            'start_date' => $start_date,
            'last_date'  => $last_date
        );
    }
    
    return $all;
}

function run_monthend()
{
    $this->db->order_by('mend_id','DESC');
    $this->db->limit(1);
    $lastmonthend = $this->db->get('hci_monthend')->row_array();

    $monthenddate = 1;

    if(empty($lastmonthend))
    {
        $lastmonthenddate = '2017-01-'.$monthenddate;
    }
    else
    {
        $lastmonthenddate = $lastmonthend['mend_edate'];
    }  

    $start_date = date("Y-m-d", strtotime("+1 day", strtotime($lastmonthenddate)));
    $start_month = date("m", strtotime("+1 month", strtotime($lastmonthenddate)));

    // if(date('d')>=$monthenddate)
    // {
    //     $last_date = date('Y-m-d',strtotime("-".(date('d')-$monthenddate)." day", strtotime(date("Y-m-d"))));
    // }
    // else
    // {
    //     $last_date = date('Y-m-d', strtotime("-1 month", strtotime(date("Y-m-d"))));
    //     $last_date = date('Y-m-d',strtotime("+".($monthenddate-date('d',strtotime($last_date)))." day", strtotime($last_date)));
    // }

    $last_date = '2017-09-25';
    
    $last_month = date("m",strtotime($last_date));

    $monthsary = array();

    for ($i=$start_month; $i <= $last_month; $i++) 
    { 
        array_push($monthsary, (date('Y').'-'.str_pad(($i),2,"0",STR_PAD_LEFT).'-'.$monthenddate));
    }

    $this->db->trans_begin();

    foreach ($monthsary as $month) 
    {
        $mendup['mend_sdate'] = $start_date;
        $mendup['mend_edate'] = $month;
        $mendup['mend_month'] = date('m',strtotime($month));

        $this->erp_integration($month);

        $start_date = date("Y-m-d", strtotime("+1 month", strtotime($start_date))); // nextmonthstartdate
        $nextend = date("Y-m-d", strtotime("+1 month", strtotime($month)));

        $this->db->where('term_sdate >=',$start_date);
        $this->db->where('term_sdate <=',$nextend);
        $istermstart = $this->db->get('hci_term')->row_array(); 

        if(!empty($istermstart))
        {
            $this->db->select('id');
            // $this->db->where('st_branch',$this->session->userdata('u_branch'));
            $this->db->where('st_term !=',$istermstart['term_id']);
            $this->db->where('wd_date is NULL', NULL, FALSE);
            $this->db->or_where('wd_date >',$start_date);
            $students = $this->db->get('st_details')->result_array(); 

            foreach ($students as $stu) 
            {
                $this->db->where('br_id',$stu['st_branch']);
                $branch = $this->db->get('hgc_branch')->row_array();

                $this->generate_invoice('STUDENT',$stu['id'],'TERMEND',$istermstart,$start_date,$branch);
            }
        }

        $mendup['mend_date'] = date('Y-m-d');

        $this->db->insert('hci_monthend',$mendup);
    }

    if ($this->db->trans_status() === FALSE)
    {
        $this->db->trans_rollback();
        $this->msg->set('admission', "Failed to process Grade Promotion. retry");
        return false;
    }
    else
    {
        $this->db->trans_commit(); 
        $this->msg->set('admission', "Grade Promotion processed successfully");
        return true;
    }
}

function invoicefeesbreakdown($invtype,$inv_id,$term,$trandate,$extraary)
{
    $monthenddate = 1;
    $currdate = date("d",strtotime($trandate));

    $this->db->where('es_ac_year_id',$term['term_acyear']);
    $accyear = $this->db->get('hgc_academicyears')->row_array();

    $yendmonth = date("m",strtotime($accyear['ac_enddate']));
    $tendmonth = date("m",strtotime($term['term_edate']));
    $tstamonth = date("m",strtotime($term['term_sdate']));
    $tranmonth = date("m",strtotime($trandate));

    if($tendmonth<$tstamonth)
    {
        $tmonthdiff = (13-$tstamonth)+$tendmonth;
    }
    else
    {
        $tmonthdiff = ($tendmonth-$tstamonth)+1;
    }

    if($trandate > $term['term_sdate'])
    {
        if($tendmonth<$tranmonth)
        {
            $monthdiff = (13-$tranmonth)+$tendmonth;
        }
        else
        {
            $monthdiff = ($tendmonth-$tranmonth)+1;  
        }

        if($currdate>$monthenddate)
        {
            $startmonth = $tranmonth+1;
        }
        else
        {
            $startmonth = $tranmonth;
        }
    }
    else
    {
            $monthdiff = $tmonthdiff;
            $startmonth = $tstamonth;
    }

    if($yendmonth<$tendmonth)
    {
        $balmonths = (12-$tendmonth)+$yendmonth;
    }
    else
    {
        $balmonths = ($yendmonth-$tendmonth);  
    }

    $this->db->where('inv_id',$inv_id);
    $invoice_data = $this->db->get('hci_invoice')->row_array();

    $this->db->select('*');
    $this->db->where('invf_invoice',$inv_id);
    $invoice_fees = $this->db->get('hci_invoicefee')->result_array();

    $x = 0;
    $numofmonths = 0;
    $lastmonth;
    $feedetails;
    foreach ($invoice_fees as $invfee) 
    {
        $this->db->where('fc_id',$invfee['invf_feecat']);
        $feedetails = $this->db->get('hci_feecategory')->row_array();

        $this->db->join('hci_paymentplan','hci_paymentplan.plan_id=hci_cusinvpaymentplan.ipp_payplan');
        $this->db->where('hci_paymentplan.plan_fee',$invfee['invf_feecat']);
        $this->db->where('hci_cusinvpaymentplan.ipp_invoice',$inv_id);
        $paymplan = $this->db->get('hci_cusinvpaymentplan')->row_array();

        if($paymplan['plan_type']=='TA')
        {
            $numofmonths = $monthdiff+$balmonths;
            $lastmonth = intval($yendmonth);
        }
        else if($paymplan['plan_type']=='T')
        {
            $numofmonths = $monthdiff;
            $lastmonth = intval($tendmonth);
        }
        else
        {
            $numofmonths = 1;
            $lastmonth = intval($tranmonth);
        }
    }   

    $monthsary = array();

    if($startmonth>$lastmonth)
    {
        for ($i=intval($startmonth); $i <= 12; $i++) 
        { 
            array_push($monthsary, (date('Y').'-'.str_pad(($i),2,"0",STR_PAD_LEFT).'-'.$monthenddate));
            $startmonth = 1;
        } 

        for ($i=intval($startmonth); $i <= $lastmonth; $i++) 
        { 
            array_push($monthsary, (date('Y').'-'.str_pad(($i),2,"0",STR_PAD_LEFT).'-'.$monthenddate));
        }
    }
    else
    {
        for ($i=intval($startmonth); $i <= $lastmonth; $i++) 
        { 
            array_push($monthsary, (date('Y').'-'.str_pad(($i),2,"0",STR_PAD_LEFT).'-'.$monthenddate));
        }
    }

    $x = 1;
    foreach ($monthsary as $month) 
    {
        $smi_save['smi_invoice'] = $inv_id;
        $smi_save['smi_customer'] = $invoice_data['inv_cusindex'].' - '.$invoice_data['inv_cusname'];
        $smi_save['smi_subnumber'] = $x;
        $smi_save['smi_month'] = $month;
        $smi_save['smi_status'] = 'A';

        $this->db->insert('hci_stumonthlyinvoice',$smi_save);
        $minv_id = $this->db->insert_id();

        if($invoice_data['inv_type'] == 'TFS')
        {
            $monthlyportion = $extraary[$month];
        }
        else
        {
            $monthlyportion = $invoice_data['inv_totamount']/$numofmonths;
        }

        $minv_details['smid_description'] = $feedetails['fc_gldescription'];
        $minv_details['smid_glcode'] = $feedetails['fc_glcode'];
        $minv_details['smid_monthinv'] = $minv_id;
        $minv_details['smid_rdcrtype'] = 'CR';
        $minv_details['smid_action'] = 'D';
        $minv_details['smid_amount'] = $monthlyportion;

        $this->db->insert('hci_stumonthinvdetail',$minv_details);

        if($x == 1)
        {
            $this->db->select('hci_stuinvdiscount.*,hci_adjusment.adj_glcode,hci_adjusment.adj_gldescription');
            $this->db->join('hci_adjusment','hci_adjusment.adj_id=hci_stuinvdiscount.id_adjusment');
            $this->db->where('hci_stuinvdiscount.id_invoice',$inv_id);
            $invdiscs = $this->db->get('hci_stuinvdiscount')->result_array();

            foreach ($invdiscs as $disc) 
            {
                $minv_details['smid_description'] = $disc['adj_gldescription'];
                $minv_details['smid_glcode'] = $disc['adj_glcode'];
                $minv_details['smid_monthinv'] = $minv_id;
                $minv_details['smid_amount'] = $disc['id_calcamount'];
                $minv_details['smid_rdcrtype'] = 'CR';
                $minv_details['smid_action'] = 'D';

                $this->db->insert('hci_stumonthinvdetail',$minv_details);
            }

            $differed = $invoice_data['inv_totamount']-$monthlyportion;
            $minv_details['smid_action'] = 'D';
            $minv_details['smid_rdcrtype'] = 'CR';
        }
        else
        {
            $differed = $monthlyportion;
            $minv_details['smid_action'] = 'A';
            $minv_details['smid_rdcrtype'] = 'DR';
        }

        if($differed>0)
        {
            $minv_details['smid_description'] = 'Differed  Revenue';
            $minv_details['smid_glcode'] = '0201';
            $minv_details['smid_monthinv'] = $minv_id;
            $minv_details['smid_amount'] = $differed;

            $this->db->insert('hci_stumonthinvdetail',$minv_details);  
        }
        
        if($x == 1)
        {
            $minv_details['smid_description'] = 'Accounts Receivables-School Students';
            $minv_details['smid_glcode'] = '0054';
            $minv_details['smid_monthinv'] = $minv_id;
            $minv_details['smid_amount'] = $invoice_data['inv_netamount'];
            $minv_details['smid_action'] = 'A';
            $minv_details['smid_rdcrtype'] = 'DR';

            $this->db->insert('hci_stumonthinvdetail',$minv_details);
        }

       $x++; 
    }

    return true;
}

function generate_invoice($custype,$cusref,$invtype,$term,$start_date,$branchinfo)
{
    $this->db->select('st_details.*,hci_feemaster.fee_aftemp,hci_feemaster.fee_tftemp');
    $this->db->join('hci_feemaster','hci_feemaster.es_feemasterid=st_details.st_feestructure','left');
    $this->db->where('id',$cusref);
    $cus = $this->db->get('st_details')->row_array(); 

    $cus['id'] = intval($cus['id']);

    if($term['term_number'] == 1)
    {
        $description = '1STTERM';
    }
    else if($term['term_number'] == 2)
    {
       $description = '2NDTERM';
    }
    else if($term['term_number'] == 3)
    {
       $description = '3RDTERM';
    }

    $this->db->where('ats_student',$cus['id']);
    $this->db->where('ats_reftable','st_details');
    $this->db->where('ats_status','A');
    $discounts =$this->db->get('hci_adjsstudent')->result_array();

    $save_inv['inv_customer']       = $cus['id'];
    $save_inv['inv_date']           = $start_date;
    $save_inv['inv_status']         = 'P';
    $save_inv['inv_ispaid']         = 0;
    $save_inv['inv_paidamount']     = 0;
    $save_inv['inv_cusindex']       = $cus['st_id'];
    $save_inv['inv_cusname']        = $cus['family_name'].' '.$cus['other_names'];
    $save_inv['inv_feestructure']   = $cus['st_feestructure'];
    $save_inv['inv_fstype']         = 'hci_feemaster';

    if($cus['st_infComPerson']==1)
    {
        $comperson = 'dad';
    }
    else if($cus['st_infComPerson']==2)
    {
        $comperson = 'mom';
    }
    else
    {
        $comperson = 'guar';
    }
    $save_inv['inv_cusaddline1']    = $cus[$comperson.'_addy'];
    $save_inv['inv_cusaddline2']    = $cus[$comperson.'_add2'];
    $save_inv['inv_cusaddline3']    = $cus[$comperson.'_addcity'];
    $save_inv['inv_custype']        = $custype;

    $save_inv['inv_compname']     = $this->session->userdata('u_compname').' - '.$branchinfo['br_name'];
    $save_inv['inv_compaddline1'] = $branchinfo['br_addl1'];
    $save_inv['inv_compaddline2'] = $branchinfo['br_addl2'];
    $save_inv['inv_compaddline3'] = $branchinfo['br_city'];
    $save_inv['inv_branch']       = $branchinfo['br_id'];

    if($invtype!='TRANSPORT')
    {
        $this->db->join('hci_feecategory','hci_feecategory.fc_id=hci_feestructure_fees.fsf_fee');
        $this->db->where('hci_feestructure_fees.fsf_grade',$cus['st_grade']);
        $this->db->where_in('hci_feestructure_fees.fsf_feetemplate',array($cus['fee_aftemp'],$cus['fee_tftemp']));
        $fee_template = $this->db->get('hci_feestructure_fees')->result_array();

        foreach ($fee_template as $fee) 
        {
            $pplans = $this->get_paymentplan($cusref,$fee['fsf_fee'],'st_details');

            $inv_totamount = 0;

            if($fee['fsf_fee']==1)
            {
                if($invtype=='REGISTRATION')
                {
                    $description = $invtype;
                    $inv_totamount  = $fee['fsf_amt'];
                }
                else
                {
                    if(($term['term_number'] == 1) && ($cus['st_addpaymethod']==2))
                    {
                        $inv_totamount  = $fee['fsf_slabold'];
                    }
                }
            }
            else if($fee['fsf_fee']==3)
            {   
                $this->db->where('amtprd_feetemplate',$cus['st_termtemplate']);
                $this->db->where('amtprd_feecategory',3);
                $periodoption = $this->db->get('hci_feetemplateamountperiod')->row_array();

                if($invtype=='REGISTRATION')
                {
                    $inv_totamount  = $fee['fsf_amt'];
                }
                else
                {
                    if($term['term_number']==1)
                    {
                        $inv_totamount  = $fee['fsf_amt'];
                    }
                }
            }
            else
            {   
                $this->db->where('amtprd_feetemplate',$cus['st_termtemplate']);
                $this->db->where('amtprd_feecategory',$fee['fsf_fee']);
                $periodoption = $this->db->get('hci_feetemplateamountperiod')->row_array();

                if($pplans['plan_type']=='M' || $pplans['plan_type']=='T')
                {
                    $inv_totamount  = $fee['fsf_amt'];
                }
                else
                {
                    if($term['term_number']==1)
                    {
                        $inv_totamount  = $fee['fsf_amt']*3;
                    }
                    // else if($term['term_number']==2)
                    // {
                    //     $inv_totamount  = $fee['fsf_amt']*2;
                    // }
                    // else
                    // {
                    //     $inv_totamount  = $fee['fsf_amt'];
                    // }
                }
            }

            if($inv_totamount>0)
            { 
                $save_inv['inv_index']          = $this->sequence->generate_sequence('INV'.$branchinfo['br_code'].date('y'),5);  
                $save_inv['inv_type']           = $fee['fc_invcode'];
                $save_inv['inv_description']    = $description.' - '.$fee['fc_name'];
                $save_inv['inv_createdate']     = date('Y-m-d h:i:sa');
                $save_inv['inv_createuser']     = $this->session->userdata('u_id');
                $save_inv['inv_createusername'] = $this->session->userdata('u_name');
                $save_inv['inv_updateuser']     = $this->session->userdata('u_id');
                $save_inv['inv_totamount']      = $inv_totamount;
                $save_inv['inv_netamount']      = $inv_totamount;
                $save_inv['inv_balanceamount']  = $inv_totamount;
                  
                $this->db->insert('hci_invoice',$save_inv);
                $inv_id = $this->db->insert_id();

                $adjusments = $this->apply_discount($fee['fsf_fee'],$pplans['plan_type'],$discounts,$invtype,$inv_totamount,$inv_id);
        
                $save_fee['invf_invoice']        = $inv_id;
                $save_fee['invf_fee']            = $fee['fsf_id'];
                $save_fee['invf_feecat']         = $fee['fsf_fee'];
                $save_fee['invf_reftype']        = 'hci_feemaster';
                $save_fee['invf_feedescription'] = $fee['fc_index'].' - '.$fee['fc_name'];
                $save_fee['invf_amount']         = $inv_totamount;
                $save_fee['invf_discount']       = $adjusments[1];
                $save_fee['invf_addition']       = $adjusments[0];
                $save_fee['invf_netamount']      = $inv_totamount-$adjusments[1]+$adjusments[0];
                $save_fee['invf_paidamount']     = 0;
                $save_fee['invf_balanceamount']  = $inv_totamount-$adjusments[1]+$adjusments[0];
        
                $this->db->insert('hci_invoicefee',$save_fee);

                $otheradjusments = $this->apply_discount(NULL,NULL,NULL,$invtype,$inv_totamount,$inv_id);

                $updt_inv['inv_totdiscounts']   = $adjusments[1]+$otheradjusments[1];
                $updt_inv['inv_totadditions']   = $adjusments[0]+$otheradjusments[0];
                $updt_inv['inv_netamount']      = $inv_totamount-($adjusments[1]+$otheradjusments[1])+($adjusments[0]+$otheradjusments[0]);
                $updt_inv['inv_balanceamount']  = $inv_totamount-($adjusments[1]+$otheradjusments[1])+($adjusments[0]+$otheradjusments[0]);

                $this->db->where('inv_id',$inv_id);
                $this->db->update('hci_invoice',$updt_inv);

                $this->update_transaction($custype,$cusref,$inv_id,'INVOICE',$description.' - '.$fee['fc_name'],$start_date,'DR',$updt_inv['inv_netamount'],$save_inv['inv_createuser'],0,$save_inv['inv_createdate'],$save_inv['inv_createusername'],$branchinfo['br_code']);

                $inv_plan['ipp_invoice'] = $inv_id;
                $inv_plan['ipp_payplan'] = $pplans['plan_id'];
                $inv_plan['ipp_fee']     = $fee['fsf_fee'];

                $this->db->insert('hci_cusinvpaymentplan',$inv_plan);

                $this->invoicefeesbreakdown($invtype,$inv_id,$term,$start_date,NULL);
            } 
        }
    }
     
    if($invtype=='TRANSPORT' || $invtype=='TERMEND')  
    {
        if($cus['st_includetrans']=='1')
        {
            $pplans = $this->get_paymentplan($cusref,4,'st_details');

            $datenow = date("d",strtotime($start_date));
            $monthnow = date("m",strtotime($start_date));

            $this->db->where('treg_custtype','STUDENT');
            $this->db->where('treg_customer',$cus['id']);
            $tregdata = $this->db->get('hci_transregistration')->row_array();

            $this->db->where('tr_place_id',$tregdata['treg_pickpoint']);
            $distance = $this->db->get('hci_transpickpoint')->row_array();

            $datetermend = date("d",strtotime($term['term_edate']));
            $monthtermend = date("m",strtotime($term['term_edate']));

            $monthdiff = ($monthtermend - $monthnow)+1;

            $transpFee = 0;
            $tranfeetype = '';
            $tranfeeind = '';
            $transfeecode = '';

            $transmonthsary = array();
            if($monthnow>$monthtermend)
            {
                for ($i=intval($monthnow); $i <= 12; $i++) 
                { 
                    array_push($transmonthsary, $i);
                    $monthnow = 1;
                } 

                for ($i=intval($monthnow); $i <= $monthtermend; $i++) 
                { 
                    array_push($transmonthsary, $i);
                }
            }
            else
            {
                for ($i=intval($monthnow); $i <= $monthtermend; $i++) 
                { 
                    array_push($transmonthsary, $i);
                }
            }

            $inv_division = array();
            foreach ($transmonthsary as $month) 
            {
                if($month == $monthtermend)
                {
                    $this->db->where('fsf_isholiday',1);
                }
                else
                {
                    $this->db->where('fsf_isholiday',0);
                }

                $this->db->join('hci_feecategory','hci_feecategory.fc_id=hci_trans_fsfee.fsf_fee');
                $this->db->where('fsf_distance',$distance['tpp_stype']);
                $this->db->where('fsf_feestructure',$tregdata['treg_feestructure']);
                $amount = $this->db->get('hci_trans_fsfee')->row_array();

                if(($invtype=='TRANSPORT') && ($month != $monthtermend) && ($datenow>15))
                {
                    $tmonthpay = $amount['fsf_amount']/2;
                }
                else
                {
                    $tmonthpay = $amount['fsf_amount'];
                }

                $inv_division[$month] = $tmonthpay;
                
                $transpFee += $tmonthpay;
                $tranfeetype = $amount['fc_name'];
                $tranfeeind = $amount['fc_index'];
                $transfeecode = $amount['fc_invcode'];
            }
            
            if($transpFee>0)
            { 
                $save_inv['inv_index']          = $this->sequence->generate_sequence('INV'.$branchinfo['br_code'].date('y'),5);  
                $save_inv['inv_type']           = $transfeecode;
                $save_inv['inv_description']    = $description.' - '.$tranfeetype;
                $save_inv['inv_createdate']     = date('Y-m-d h:i:sa');
                $save_inv['inv_createuser']     = $this->session->userdata('u_id');
                $save_inv['inv_createusername'] = $this->session->userdata('u_name');
                $save_inv['inv_updateuser']     = $this->session->userdata('u_id');
                $save_inv['inv_totamount']      = $transpFee;
                $save_inv['inv_netamount']      = $transpFee;
                $save_inv['inv_balanceamount']  = $transpFee;
                  
                $this->db->insert('hci_invoice',$save_inv);
                $inv_id = $this->db->insert_id();

                $adjusments = $this->apply_discount(4,$pplans['plan_type'],$discounts,$invtype,$transpFee,$inv_id);
        
                $save_fee['invf_invoice']        = $inv_id;
                $save_fee['invf_fee']            = $tregdata['treg_feestructure'];
                $save_fee['invf_feecat']         = 4;
                $save_fee['invf_reftype']        = 'hci_trans_feestructure';
                $save_fee['invf_feedescription'] = $tranfeeind.' - '.$tranfeetype;
                $save_fee['invf_amount']         = $transpFee;
                $save_fee['invf_discount']       = $adjusments[1];
                $save_fee['invf_addition']       = $adjusments[0];
                $save_fee['invf_netamount']      = $transpFee-$adjusments[1]+$adjusments[0];
                $save_fee['invf_paidamount']     = 0;
                $save_fee['invf_balanceamount']  = $transpFee-$adjusments[1]+$adjusments[0];
        
                $this->db->insert('hci_invoicefee',$save_fee);

                $otheradjusments = $this->apply_discount(NULL,NULL,NULL,$invtype,$transpFee,$inv_id);

                $updt_inv['inv_totdiscounts']   = $adjusments[1]+$otheradjusments[1];
                $updt_inv['inv_totadditions']   = $adjusments[0]+$otheradjusments[0];
                $updt_inv['inv_netamount']      = $transpFee-($adjusments[1]+$otheradjusments[1])+($adjusments[0]+$otheradjusments[0]);
                $updt_inv['inv_balanceamount']  = $transpFee-($adjusments[1]+$otheradjusments[1])+($adjusments[0]+$otheradjusments[0]);

                $this->db->where('inv_id',$inv_id);
                $this->db->update('hci_invoice',$updt_inv);

                $this->update_transaction($custype,$cusref,$inv_id,'INVOICE',$description.' - '.$tranfeetype,$start_date,'DR',$updt_inv['inv_netamount'],$save_inv['inv_createuser'],0,$save_inv['inv_createdate'],$save_inv['inv_createusername'],$branchinfo['br_code']);

                $inv_plan['ipp_invoice'] = $inv_id;
                $inv_plan['ipp_payplan'] = $pplans['plan_id'];
                $inv_plan['ipp_fee']     = 4;

                $this->db->insert('hci_cusinvpaymentplan',$inv_plan);

                $this->invoicefeesbreakdown($invtype,$inv_id,$term,$start_date,$inv_division);
            } 
        }
    }

    return true;
}

function erp_integration($month)
{
    $erp_db = $this->load->database('erp_db', TRUE); 
    $br_pref = '3';

    $erp_db->select('value');
    $erp_db->where('name','f_year');
    $f_year = $erp_db->get($br_pref.'_sys_prefs')->row_array();

    $this->db->where('smi_month',$month);
    $invlist = $this->db->get('hci_stumonthlyinvoice')->result_array();

    foreach ($invlist as $inv) 
    {
        $erp_db->select('MAX(trans_no) as trans');
        $erp_db->where('type',0);
        $erp_db->order_by('trans_no','asc');
        $jurnal = $erp_db->get($br_pref.'_journal')->row_array();

        $erp_db->where('trans_type',0);
        $pattern = $erp_db->get($br_pref.'_reflines')->row_array();

        $refs['id'] = $jurnal['trans'] + 1;
        $refs['type'] = 0;
        $refs['reference'] = $pattern['pattern'];
        $erp_db->insert($br_pref.'_refs',$refs);
        $refs_id = $erp_db->insert_id();

        $this->db->where('smi_id',$inv['smi_id']);
        $this->db->update('hci_stumonthlyinvoice',array('smi_refsid' => $jurnal['trans'] + 1));

        $autrail['type'] = 0;
        $autrail['trans_no'] = $jurnal['trans'] + 1;
        $autrail['user'] = 1;
        $autrail['fiscal_year'] = $f_year['value'];
        $autrail['gl_date'] = date('Y-m-d');
        $autrail['gl_seq'] = 0;
        $erp_db->insert($br_pref.'_audit_trail',$autrail);
        $gl_transid = $erp_db->insert_id();

        $this->db->where('smi_id',$inv['smi_id']);
        $this->db->update('hci_stumonthlyinvoice',array('smi_gltransid' => $gl_transid));

        $this->db->where('smid_monthinv',$inv['smi_id']);
        $invoice_data = $this->db->get('hci_stumonthinvdetail')->result_array();

        foreach ($invoice_data as $detail) 
        {
            $gl_trans['type'] = 0;
            $gl_trans['type_no'] = $jurnal['trans'] + 1;
            $gl_trans['tran_date'] = date('Y-m-d');
            $gl_trans['dimension_id'] = 0;
            $gl_trans['dimension2_id'] = 0;
            $gl_trans['account'] = $detail['smid_glcode'];
            if($detail['smid_action']=='D')
            {
                $gl_trans['amount'] = 0-($detail['smid_amount']);
            }
            else
            {
                $gl_trans['amount'] = $detail['smid_amount'];
            }
            
            $erp_db->insert($br_pref.'_gl_trans',$gl_trans);
            $grlupdateid = $erp_db->insert_id();

            $this->db->where('smid_id',$detail['smid_id']);
            $result = $this->db->update('hci_stumonthinvdetail',array('simd_grlupdateid' => $grlupdateid));
        }

        $jnl['type']= '0';
        $jnl['trans_no']= $jurnal['trans'] + 1;
        $jnl['tran_date']= date('Y-m-d');
        $jnl['reference']= $pattern['pattern'];
        $jnl['event_date']= date('Y-m-d');
        $jnl['doc_date']= date('Y-m-d');
        $jnl['currency']= 'LKR';
        $jnl['amount']= $gl_trans['amount'];
        $jnl['rate']= '1';
        $erp_db->insert($br_pref.'_journal',$jnl);

        $erp_db->set('pattern', 'pattern+1', FALSE);
        $erp_db->where('trans_type',0);
        $erp_db->update($br_pref.'_reflines');
    }

    return true;
}

function load_outstanding()
{
    $type = $this->input->post('type');
    $cus  = $this->input->post('cus');

    $this->db->where('outs_custype',$type);
    $this->db->where('outs_customer',$cus);
    $outs = $this->db->get('hci_cusoutstanding')->row_array();

    if(empty($outs))
    {
        $outs['outs_amount'] = 0.00;
        $outs['outs_cohbalance'] = 0.00;
    }

    $this->db->where('inv_custype',$type);
    // $this->db->where('inv_ispaid',0);
    $this->db->where('inv_customer',$cus);
    $this->db->order_by('inv_date',"asc");
    $invoices = $this->db->get('hci_invoice')->result_array();

    $y = 0;
    foreach ($invoices as $inv) 
    {
        $this->db->select('*');
        $this->db->where('invf_invoice',$inv['inv_id']);
        $invoice_fees = $this->db->get('hci_invoicefee')->result_array();

        $invoices[$y]['invoice_fees'] = $invoice_fees;
        $y++;
    }

    $all = array(
        "outs" => $outs,
        "invoices" => $invoices
        );

    return $all;
}

function get_paymentplan($cusref,$fee,$custype)
{
    $this->db->select('hci_paymentplan.*');
    $this->db->join('hci_paymentplan','hci_paymentplan.plan_id=hci_cuspaymentplan.spp_paymentplan');
    $this->db->where('hci_cuspaymentplan.spp_reftable',$custype);
    $this->db->where('hci_cuspaymentplan.spp_customer',$cusref);
    $this->db->where('hci_paymentplan.plan_fee',$fee);
    $pplans = $this->db->get('hci_cuspaymentplan')->row_array();

    return $pplans;
}

function get_branchdetails()
{
    $this->db->where('br_id',$this->session->userdata('u_branch'));
    $branch = $this->db->get('hgc_branch')->row_array();
}

function apply_discount($fee,$paytype,$discounts,$invtype,$inv_totamount,$inv_id)
{
    $tot_disc = 0;
    $tot_adds = 0;

    if($fee==NULL)
    {
        $this->db->join('hci_adjusment','hci_adjusment.adj_id=hci_adjstemplate.at_adjusment');
        $this->db->where('hci_adjstemplate.at_applyfor',1);
        $this->db->where('hci_adjusment.adj_status','A');
        $other_adjs = $this->db->get('hci_adjstemplate')->result_array();

        foreach ($other_adjs as $adjs) 
        {
            $addition = 0;
            $discount = 0;

            if($adjs['adj_type']=='A')
            {
                if($adjs['adj_amttype']=='P')
                {
                    $addition = ($inv_totamount*$adjs['adj_amount']/100);
                }
                else
                {
                    $addition = Number($adjs['adj_amount']);
                }
                $inv_dis['id_calcamount']  = $addition;
            
            }
            else
            {
                if($adjs['adj_amttype']=='P')
                {
                    $discount = ($inv_totamount*$adjs['adj_amount']/100);
                }
                else
                {
                    $discount = Number($adjs['adj_amount']);
                }
                $inv_dis['id_calcamount']  = $discount;
            }

            $inv_dis['id_invoice']     = $inv_id;
            $inv_dis['id_feecat']      = 0;
            $inv_dis['id_adjusment']   = $adjs['adj_id'];
            $inv_dis['id_adjstemp']    = $adjs['at_id'];
            $inv_dis['id_amttype']     = $adjs['adj_amttype'];
            $inv_dis['id_description'] = $adjs['adj_description'];
            $inv_dis['id_amount']      = $adjs['adj_amount'];
            $inv_dis['id_type']        = $adjs['adj_type'];

            $this->db->insert('hci_stuinvdiscount',$inv_dis);

            $tot_adds += $addition;
            $tot_disc += $discount;
        }
    }
    else
    {
        foreach ($discounts  as $disc) 
        {
            $this->db->join('hci_adjusment','hci_adjusment.adj_id=hci_adjstemplate.at_adjusment');
            $this->db->where('at_id',$disc['ats_adjstemp']);
            $temp_data = $this->db->get('hci_adjstemplate')->row_array();
            
            $addition = 0;
            $discount = 0;

            $addtodisc = FALSE;

            if($temp_data['adj_feecat']==$fee && $temp_data['adj_status']=='A')
            {
                if($temp_data['at_applyfor']==5)
                {
                    $this->db->where('atf_adjstemp',$temp_data['at_id']);
                    $discfee = $this->db->get('hci_adjsfees')->result_array();

                    if(!empty($discfee))
                    {
                        foreach($discfee as $dfee)
                        {
                            if($dfee['aft_fsfee']==$temp_data['adj_feecat'])
                            {
                                if($temp_data['adj_feecat']==1)
                                {
                                    if($invtype=='REGISTRATION')
                                    {
                                        if($dfee['aft_tot']==1)
                                        {
                                            $addtodisc = TRUE;
                                        }
                                        else if($dfee['aft_new']==1)
                                        {
                                            $addtodisc = TRUE;
                                        }
                                    }
                                    else if($invtype=='1STTERM')
                                    {
                                        if($dfee['aft_old']==1)
                                        {
                                            $addtodisc = TRUE;
                                        }
                                    }
                                }
                                else
                                {
                                    if($disc['adj_applyfor']==1)
                                    {
                                        if($paytype == 'TA')
                                        {
                                            $addtodisc = TRUE;
                                        }
                                    }
                                    else
                                    {
                                        $addtodisc = TRUE;
                                    }
                                }  
                            }
                        }
                    }
                    else
                    {
                        $addtodisc = TRUE;
                    }
                }
                else
                {
                    $addtodisc = TRUE;
                }
            }

            if($addtodisc==TRUE)
            {
                if($temp_data['adj_type']=='A')
                {
                    if($temp_data['adj_amttype']=='P')
                    {
                        $addition = ($inv_totamount*$temp_data['adj_amount']/100);
                    }
                    else
                    {
                        $addition = Number($temp_data['adj_amount']);
                    }
                    $inv_dis['id_calcamount']  = $addition;
                
                }
                else
                {
                    if($temp_data['adj_amttype']=='P')
                    {
                        $discount = ($inv_totamount*$temp_data['adj_amount']/100);
                    }
                    else
                    {
                        $discount = Number($temp_data['adj_amount']);
                    }
                    $inv_dis['id_calcamount']  = $discount;
                }

                $inv_dis['id_invoice']     = $inv_id;
                $inv_dis['id_feecat']      = $fee;
                $inv_dis['id_adjusment']   = $disc['ats_adjstemp'];
                $inv_dis['id_adjstemp']    = $temp_data['at_id'];
                $inv_dis['id_amttype']     = $temp_data['adj_amttype'];
                $inv_dis['id_description'] = $temp_data['adj_description'];
                $inv_dis['id_amount']      = $temp_data['adj_amount'];
                $inv_dis['id_type']        = $temp_data['adj_type'];

                $this->db->insert('hci_stuinvdiscount',$inv_dis);
            }
            
            $tot_adds += $addition;
            $tot_disc += $discount;
        }
    }

    $adjstary = array($tot_adds,$tot_disc);

    return $adjstary;
}

function update_transaction($custype,$cusref,$inv_id,$trantype,$description,$trandate,$type,$amount,$user,$invtotal,$timestamp,$username,$brcode)
{
    $this->db->where('outs_custype',$custype);
    $this->db->where('outs_customer',$cusref);
    $isexist = $this->db->get('hci_cusoutstanding')->row_array();

    $isdataexist = $isexist;
    $outssave['outs_custype'] = $custype;
    $outssave['outs_customer'] = $cusref;

    if(empty($isexist))
    {
        $isexist['outs_amount'] = 0;
        $isexist['outs_cohbalance'] = 0;
    }

    if($type=='DR')
    {
        $outssave['outs_amount'] = $isexist['outs_amount']+$amount;
        if(($isexist['outs_cohbalance']-$amount)>0)
        {
            $outssave['outs_cohbalance'] = $isexist['outs_cohbalance']-$amount;
        }
        else
        {
            $outssave['outs_cohbalance'] = 0;
        }
        $outssave['outs_cohbalance'] = $isexist['outs_cohbalance'];   
        $transave['tran_amount'] = $amount;
    }
    else
    {
        if($trantype=='PAYMENT')
        {
            if($invtotal>$amount)
            {
                $outssave['outs_amount'] = $isexist['outs_amount']-$amount;
                $outssave['outs_cohbalance'] = $isexist['outs_cohbalance']-($invtotal-$amount); 
                $transave['tran_amount'] = $invtotal;
            }
            else
            {
                $outssave['outs_amount'] = $isexist['outs_amount']-$amount;
                $outssave['outs_cohbalance'] = $isexist['outs_cohbalance']+($amount-$invtotal); 
                $transave['tran_amount'] = $amount;
            } 
        }
        else
        {
            $outssave['outs_amount'] = $isexist['outs_amount']-$amount;
            if(($amount-$isexist['outs_amount'])>0)
            {
                $outssave['outs_cohbalance'] = $amount-$isexist['outs_amount'];
            }
            else
            {
                $outssave['outs_cohbalance'] = 0;
            }
            $transave['tran_amount'] = $amount;
        }  
    }

    if(empty($isdataexist))
    {  
        $this->db->insert('hci_cusoutstanding',$outssave);
    }
    else
    {
        $this->db->where('outs_id',$isexist['outs_id']);
        $this->db->update('hci_cusoutstanding',$outssave);
    }

    $transave['tran_index'] = $this->sequence->generate_sequence('TRAN'.$brcode.date('y'),7);
    $transave['tran_custype'] = $custype;
    $transave['tran_customer'] = $cusref;
    $transave['tran_type'] = $trantype;
    $transave['tran_refid'] = $inv_id;
    $transave['tran_description'] = $description;
    $transave['tran_date'] = $trandate;
    $transave['tran_crordr'] = $type;
    $transave['tran_createuser'] = $user;
    $transave['tran_createusername'] = $username;
    $transave['tran_createdate'] = $timestamp;
    $transave['tran_balance'] = $outssave['outs_amount'];
    $transave['tran_cashonhand'] = $outssave['outs_cohbalance'];

    $this->db->insert('hci_transaction',$transave);

    return true;
}

function load_cusdata($type,$id)
{
    if($type=="STAFF")
    {
        $reftable = 'hgc_staff';
        $this->db->select('stf_id as "cus_id",stf_index as "cus_index",stf_lastname as "second_name",stf_firstname as "first_name",stf_adress1 as "add1",stf_adress2 as "add2",stf_city as "city"');
        $this->db->where('stf_id',$id);
    }
    else if($type=="EXTERNAL")
    {
        $reftable = 'hgc_extcustomer';
    }
    else
    {
        if($type=="TEMPSTU")
        {
            $reftable = 'st_details_temp';
        } 
        else if($type=="STUDENT" )
        {
            $reftable = 'st_details'; 
        }

        $this->db->select('st_infComPerson');
        $this->db->where('id',$id);
        $contpers = $this->db->get($reftable)->row_array();

        if($contpers['st_infComPerson']==1)
        {
            $comperson = 'dad';
        }
        else if($contpers['st_infComPerson']==2)
        {
            $comperson = 'mom';
        }
        else
        {
            $comperson = 'guar';
        }

        $this->db->select('id as "cus_id",st_id as "cus_index",other_names as "second_name",family_name as "first_name",'.$comperson.'_addy as "add1",'.$comperson.'_add2 as "add2",'.$comperson.'_addcity as "city"');
        $this->db->where('id',$id);
    }

    $customer = $this->db->get($reftable)->row_array();

    return $customer;
}

function save_creditnote()
{
    $this->db->trans_begin();

    $invoiceid = $this->input->post('invoice');

    if(empty($invoiceid))
    {
        $cust = $this->load_cusdata($this->input->post('cnote_custype'),$this->input->post('cnote_customer'));

        $cnotesave['cnote_cusindex']     = $cust['cus_index'];
        $cnotesave['cnote_cusname']      = $cust['first_name'].' '.$cust['second_name'];
        $cnotesave['cnote_cusaddline1']  = $cust['add1'];
        $cnotesave['cnote_cusaddline2']  = $cust['add2'];
        $cnotesave['cnote_cusaddline3']  = $cust['city'];
        $cnotesave['cnote_description']  = 'Credit Note For Invoice : - ';
        $glcode = 55;
    }
    else
    {
        $this->db->join('hci_feecategory','hci_feecategory.fc_invcode=hci_invoice.inv_type','left');
        $invoice = $this->db->get_where('hci_invoice',array('inv_id'=>$invoiceid))->row_array();

        $cnotesave['cnote_cusindex']     = $invoice['inv_cusindex'];
        $cnotesave['cnote_cusname']      = $invoice['inv_cusname'];
        $cnotesave['cnote_cusaddline1']  = $invoice['inv_cusaddline1'];
        $cnotesave['cnote_cusaddline2']  = $invoice['inv_cusaddline2'];
        $cnotesave['cnote_cusaddline3']  = $invoice['inv_cusaddline3'];
        $cnotesave['cnote_invoice']      = $this->input->post('invoice');
        $cnotesave['cnote_description']  = 'Credit Note For Invoice : '.$invoice['inv_index'];

        $this->db->set('inv_totcnote','inv_totcnote+'.$this->input->post('cnote_totamt'),FALSE);
        $this->db->set('inv_balanceamount','inv_balanceamount-'.$this->input->post('cnote_totamt'),FALSE);
        $this->db->where('inv_id',$this->input->post('invoice'));
        $this->db->update('hci_invoice');

        $glcode = $invoice['fc_glcode'];
    }

    

    $this->db->where('br_id',$this->input->post('cnote_branch'));
    $branch = $this->db->get('hgc_branch')->row_array();

    $cnotesave['cnote_date']         = Date('Y-m-d');
    $cnotesave['cnote_index']        = $this->sequence->generate_sequence('CRN'.$this->session->userdata('u_branchcode').date('y'),5);
    $cnotesave['cnote_custype']      = $this->input->post('cnote_custype');
    $cnotesave['cnote_customer']     = $this->input->post('cnote_customer');
    $cnotesave['cnote_outstands']    = $this->input->post('cnote_outstands');
    $cnotesave['cnote_cohbalance']   = $this->input->post('cnote_cohbalance');
    $cnotesave['cnote_totamt']       = $this->input->post('cnote_totamt');
    $cnotesave['cnote_status']       = 'A';
    $cnotesave['cnote_compname']     = $this->session->userdata('u_compname').' - '.$branch['br_name'];
    $cnotesave['cnote_compaddline1'] = $branch['br_addl1'];
    $cnotesave['cnote_compaddline2'] = $branch['br_addl2'];
    $cnotesave['cnote_compaddline3'] = $branch['br_city'];
    $cnotesave['cnote_branch']       = $this->input->post('cnote_branch');
    $cnotesave['cnote_createddate']  = date('Y-m-d h:i:sa');
    $cnotesave['cnote_createduser']  = $this->session->userdata('u_id');
    $cnotesave['cnote_createusername'] = $this->session->userdata('u_name');
    $cnotesave['cnote_remarks']      = $this->session->userdata('cnote_remarks');

    $this->db->insert('hci_creditnote',$cnotesave);
    $cnote_id = $this->db->insert_id();

    $this->update_transaction($cnotesave['cnote_custype'],$cnotesave['cnote_customer'],$cnote_id,'CREDITNOTE',$cnotesave['cnote_description'],$cnotesave['cnote_date'],'CR',$cnotesave['cnote_totamt'],$cnotesave['cnote_createduser'],0,$cnotesave['cnote_createddate'],$cnotesave['cnote_createusername'],$branch['br_code']);

    $this->hci_accounts_model->accountsErpIntegration($glcode,'ded',$cnotesave['cnote_totamt'],$cnote_id,'C');

    if ($this->db->trans_status() === FALSE)
    {
        $this->db->trans_rollback();
        $this->msg->set('admission', "Failed to Create Credit Note. retry");
        return false;
    }
    else
    {
        $this->db->trans_commit(); 
        $this->msg->set('admission', "Credit Note Created successfully");
        return true;
    }
}

function save_debitnote()
{
    $this->db->trans_begin();

    $invoiceid = $this->input->post('invoice');

    if(empty($invoiceid))
    {
        $cust = $this->load_cusdata($this->input->post('dnote_custype'),$this->input->post('dnote_customer'));

        $dnotesave['dnote_cusindex']     = $cust['cus_index'];
        $dnotesave['dnote_cusname']      = $cust['first_name'].' '.$cust['second_name'];
        $dnotesave['dnote_cusaddline1']  = $cust['add1'];
        $dnotesave['dnote_cusaddline2']  = $cust['add2'];
        $dnotesave['dnote_cusaddline3']  = $cust['city'];
        $dnotesave['dnote_description']  = 'Debit Note For Invoice : - ';
        $glcode = 55;
    }
    else
    {
        $this->db->join('hci_feecategory','hci_feecategory.fc_invcode=hci_invoice.inv_type','left');
        $invoice = $this->db->get_where('hci_invoice',array('inv_id'=>$invoiceid))->row_array();

        $dnotesave['dnote_cusindex']     = $invoice['inv_cusindex'];
        $dnotesave['dnote_cusname']      = $invoice['inv_cusname'];
        $dnotesave['dnote_cusaddline1']  = $invoice['inv_cusaddline1'];
        $dnotesave['dnote_cusaddline2']  = $invoice['inv_cusaddline2'];
        $dnotesave['dnote_cusaddline3']  = $invoice['inv_cusaddline3'];
        $dnotesave['dnote_invoice']      = $this->input->post('invoice');
        $dnotesave['dnote_description']  = 'Debit Note For Invoice : '.$invoice['inv_index'];

        $this->db->set('inv_totdnote','inv_totdnote+'.$this->input->post('dnote_totamt'),FALSE);
        $this->db->set('inv_balanceamount','inv_balanceamount+'.$this->input->post('dnote_totamt'),FALSE);
        // $this->db->set('inv_ispaid',0);
        $this->db->where('inv_id',$this->input->post('invoice'));
        $this->db->update('hci_invoice');

        $this->db->where('inv_id',$this->input->post('invoice'));
        $currinv = $this->db->get('hci_invoice')->row_array();

        if($currinv['inv_balanceamount']>0)
        {
            $this->db->set('inv_ispaid',0);
            $this->db->where('inv_id',$this->input->post('invoice'));
            $this->db->update('hci_invoice');
        }

        $glcode = $invoice['fc_glcode'];
    }

    $this->db->where('br_id',$this->input->post('dnote_branch'));
    $branch = $this->db->get('hgc_branch')->row_array();

    $dnotesave['dnote_date']         = Date('Y-m-d');
    $dnotesave['dnote_index']        = $this->sequence->generate_sequence('DBN'.$this->session->userdata('u_branchcode').date('y'),5);
    $dnotesave['dnote_custype']      = $this->input->post('dnote_custype');
    $dnotesave['dnote_customer']     = $this->input->post('dnote_customer');
    $dnotesave['dnote_outstands']    = $this->input->post('dnote_outstands');
    $dnotesave['dnote_cohbalance']   = $this->input->post('dnote_cohbalance');
    $dnotesave['dnote_totamt']       = $this->input->post('dnote_totamt');
    $dnotesave['dnote_status']       = 'A';
    $dnotesave['dnote_compname']     = $this->session->userdata('u_compname').' - '.$branch['br_name'];
    $dnotesave['dnote_compaddline1'] = $branch['br_addl1'];
    $dnotesave['dnote_compaddline2'] = $branch['br_addl2'];
    $dnotesave['dnote_compaddline3'] = $branch['br_city'];
    $dnotesave['dnote_branch']       = $this->input->post('dnote_branch');
    $dnotesave['dnote_createddate']  = date('Y-m-d h:i:sa');
    $dnotesave['dnote_createduser']  = $this->session->userdata('u_id');
    $dnotesave['dnote_createusername'] = $this->session->userdata('u_name');
    $dnotesave['dnote_remarks']      = $this->session->userdata('dnote_remarks');

    $this->db->insert('hci_debitnote',$dnotesave);
    $dnote_id = $this->db->insert_id();

    $this->update_transaction($dnotesave['dnote_custype'],$dnotesave['dnote_customer'],$dnote_id,'DEBITNOTE',$dnotesave['dnote_description'],$dnotesave['dnote_date'],'DR',$dnotesave['dnote_totamt'],$dnotesave['dnote_createduser'],$dnotesave['dnote_totamt'],$dnotesave['dnote_createddate'],$dnotesave['dnote_createusername'],$branch['br_code']);
    $this->hci_accounts_model->accountsErpIntegration($glcode,'add',$dnotesave['dnote_totamt'],$dnote_id,'D');

    if ($this->db->trans_status() === FALSE)
    {
        $this->db->trans_rollback();
        $this->msg->set('admission', "Failed to Create Debit Note. retry");
        return false;
    }
    else
    {
        $this->db->trans_commit(); 
        $this->msg->set('admission', "Debit Note Created successfully");
        return true;
    }
}

function load_dnotelist()
{
    $draw = $_POST["draw"];//counter used by DataTables to ensure that the Ajax returns from server-side processing requests are drawn in sequence by DataTables
    $orderByColumnIndex  = $_POST['order'][0]['column'];// index of the sorting column (0 index based - i.e. 0 is the first record)
    $orderBy = $_POST['columns'][$orderByColumnIndex]['data'];//Get name of the sorting column from its index
    $orderType = $_POST['order'][0]['dir']; // ASC or DESC
    $start  = $_POST["start"];//Paging first record indicator.
    $length = $_POST['length'];//Number of records that the table can display in the current draw
    // $stat = $_POST['stat'];

    $brlist = $this->auth->get_accessbranch();

    $this->db->select('*');
    $this->db->where_in('dnote_branch',$brlist);
    $this->db->where('dnote_id >=',$start);
    $this->db->limit($length);
    $this->db->order_by($orderBy,$orderType);
    $dnotes = $this->db->get('hci_debitnote')->result_array();

    $x = 0; 
    foreach ($dnotes as $dnote) 
    {
        $this->db->select('*');
        $this->db->where('inv_id',$dnote['dnote_id']);
        $inv = $this->db->get('hci_invoice')->row_array();

        $dnotes[$x]['inv_index'] = $inv['inv_index'];

        // if($dnote['pay_status'] == 'P')
        // {
        //     $editbtn = ' | <button type="button" class="btn btn-info btn-xs" onclick="event.preventDefault();edit_dnote('.$dnote['pay_id'].')"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button>';
        //     $procbtn = ' | <button type="button" class="btn btn-info btn-xs" onclick="event.preventDefault();process_dnote('.$dnote['pay_id'].')"><span class="glyphicon glyphicon-transfer" aria-hidden="true"></span></button>';
        // }
        // else
        // {
            $editbtn = '';
            $procbtn = '';
        // }

        $dnotes[$x]['actions'] = '<button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#myModal" onclick="event.preventDefault();load_dnote_receipt('.$dnote['dnote_id'].')"><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span></button>'.
								' | <button type="button" class="btn btn-success btn-xs" onclick="event.preventDefault();print_dnote('.$dnote['dnote_id'].')"><span class="glyphicon glyphicon-print" aria-hidden="true"></span></button>'.
									
								$editbtn.$procbtn;
        $x++;
    }

    $outputary = array();

    foreach ($dnotes as $key => $value) 
    {
        if(!empty($_POST['search']['value']))
        {
            $display = false;
            for($i=0 ; $i<count($_POST['columns']);$i++){

                if($_POST['columns'][$i]['data']!='actions')
                {
                    $column = $_POST['columns'][$i]['data'];//we get the name of each column using its index from POST request

                    $strexist = stripos($value[$column],$_POST['search']['value']);

                    if($strexist)
                    {
                        $outputary[] = $value;
                    }
                    
                }
            }
        }
        else
        {
            $outputary[] = $value;
        }
    }

    $count = count($outputary);

    $output = array(
        "draw" => intval($draw),
        "recordsTotal" => $count,
        "recordsFiltered" => $count,
        "data" => $outputary
    );

    return $output;
}

function load_cnotelist()
{
    $draw = $_POST["draw"];//counter used by DataTables to ensure that the Ajax returns from server-side processing requests are drawn in sequence by DataTables
    $orderByColumnIndex  = $_POST['order'][0]['column'];// index of the sorting column (0 index based - i.e. 0 is the first record)
    $orderBy = $_POST['columns'][$orderByColumnIndex]['data'];//Get name of the sorting column from its index
    $orderType = $_POST['order'][0]['dir']; // ASC or DESC
    $start  = $_POST["start"];//Paging first record indicator.
    $length = $_POST['length'];//Number of records that the table can display in the current draw
    // $stat = $_POST['stat'];

    $brlist = $this->auth->get_accessbranch();

    $this->db->select('*');
    $this->db->where_in('cnote_branch',$brlist);
    $this->db->where('cnote_id >=',$start);
    $this->db->limit($length);
    $this->db->order_by($orderBy,$orderType);
    $cnotes = $this->db->get('hci_creditnote')->result_array();

    $x = 0; 
    foreach ($cnotes as $cnote) 
    {
        $this->db->select('*');
        $this->db->where('inv_id',$cnote['cnote_id']);
        $inv = $this->db->get('hci_invoice')->row_array();

        $cnotes[$x]['inv_index'] = $inv['inv_index'];

        // if($cnote['pay_status'] == 'P')
        // {
        //     $editbtn = ' | <button type="button" class="btn btn-info btn-xs" onclick="event.preventDefault();edit_cnote('.$cnote['pay_id'].')"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button>';
        //     $procbtn = ' | <button type="button" class="btn btn-info btn-xs" onclick="event.preventDefault();process_cnote('.$cnote['pay_id'].')"><span class="glyphicon glyphicon-transfer" aria-hidden="true"></span></button>';
        // }
        // else
        // {
            $editbtn = '';
            $procbtn = '';
        // }

        $cnotes[$x]['actions'] = '<button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#myModal" onclick="event.preventDefault();load_cnote_receipt('.$cnote['cnote_id'].')"><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span></button>'.
								' | <button type="button" class="btn btn-success btn-xs" onclick="event.preventDefault();print_cnote('.$cnote['cnote_id'].')"><span class="glyphicon glyphicon-print" aria-hidden="true"></span></button>'.
									$editbtn.$procbtn;
        $x++;
    }

    $outputary = array();

    foreach ($cnotes as $key => $value) 
    {
        if(!empty($_POST['search']['value']))
        {
            $display = false;
            for($i=0 ; $i<count($_POST['columns']);$i++){

                if($_POST['columns'][$i]['data']!='actions')
                {
                    $column = $_POST['columns'][$i]['data'];//we get the name of each column using its index from POST request

                    $strexist = stripos($value[$column],$_POST['search']['value']);

                    if($strexist)
                    {
                        $outputary[] = $value;
                    }
                    
                }
            }
        }
        else
        {
            $outputary[] = $value;
        }
    }

    $count = count($outputary);

    $output = array(
        "draw" => intval($draw),
        "recordsTotal" => $count,
        "recordsFiltered" => $count,
        "data" => $outputary
    );

    return $output;
}

function load_cnote_details($cnote)
{
    $brlist = $this->auth->get_accessbranch();

    $this->db->select('*');
    $this->db->where('cnote_id',$cnote);
    $this->db->where_in('cnote_branch',$brlist);
    $cnote = $this->db->get('hci_creditnote')->row_array();

    $invoice = array();

    if(!empty($cnote))
    {
        $this->db->select('inv_index');
        $this->db->where('inv_id',$cnote['cnote_invoice']);
        $invoice = $this->db->get('hci_invoice')->row_array();
    }
    
    $all = array(
        "cnote" => $cnote,
        "invoice" => $invoice,
        );

    return $all;
}

function load_dnote_details($dnote)
{
    $brlist = $this->auth->get_accessbranch();

    $this->db->select('*');
    $this->db->where('dnote_id',$dnote);
    $this->db->where_in('dnote_branch',$brlist);
    $dnote = $this->db->get('hci_debitnote')->row_array();

    $invoice = array();

    if(!empty($dnote))
    {
        $this->db->select('inv_index');
        $this->db->where('inv_id',$dnote['dnote_invoice']);
        $invoice = $this->db->get('hci_invoice')->row_array();
    }

    $all = array(
        "dnote" => $dnote,
        "invoice" => $invoice,
        );

    return $all;
}

function load_chequelist()
{
    $draw = $_POST["draw"];//counter used by DataTables to ensure that the Ajax returns from server-side processing requests are drawn in sequence by DataTables
    $orderByColumnIndex  = $_POST['order'][0]['column'];// index of the sorting column (0 index based - i.e. 0 is the first record)
    $orderBy = $_POST['columns'][$orderByColumnIndex]['data'];//Get name of the sorting column from its index
    $orderType = $_POST['order'][0]['dir']; // ASC or DESC
    $start  = $_POST["start"];//Paging first record indicator.
    $length = $_POST['length'];//Number of records that the table can display in the current draw
    $stat = $_POST['stat'];

    $brlist = $this->auth->get_accessbranch();

    if($stat != 'A')
    {
        $this->db->where('cheq_status',$stat);
    }
    $this->db->where_in('cheq_branch',$brlist);
    $this->db->where('cheq_id >=',$start);
    $this->db->limit($length);
    $this->db->order_by($orderBy,$orderType);
    $cheques = $this->db->get('hci_cheque')->result_array();

    $x = 0; 
    foreach ($cheques as $cheque) 
    {
        $this->db->where('rec_index',$cheque['cheq_receipt']);
        $rec = $this->db->get('hci_receipt')->row_array();

        $cheques[$x]['customer'] = $rec['rec_cusindex'].' - '.$rec['rec_cusname'];
        $cheques[$x]['rec_description'] = $rec['rec_description'];
        $cheques[$x]['rec_amount'] = $rec['rec_amount'];

        $this->db->where('bnk_id',$cheque['cheq_bank']);
        $bank = $this->db->get('hgc_bank')->row_array();

        $cheques[$x]['bank'] = $bank['bnk_code'].' - '.$bank['bnk_name'];

        if($cheque['cheq_status'] == 'P')
        {
            $bounsbtn = '<button type="button" class="btn btn-danger btn-xs" onclick="event.preventDefault();bouns_cheque('.$cheque['cheq_id'].',\''.$cheque['cheq_number'].'\')"><span class="glyphicon glyphicon-floppy-remove" aria-hidden="true"></span></button>';
        }
        else
        {
            $bounsbtn = '';
        }

        $cheques[$x]['actions'] = $bounsbtn;
        $x++;
    }

    $outputary = array();

    foreach ($cheques as $key => $value) 
    {
        if(!empty($_POST['search']['value']))
        {
            $display = false;
            for($i=0 ; $i<count($_POST['columns']);$i++){

                if($_POST['columns'][$i]['data']!='actions')
                {
                    $column = $_POST['columns'][$i]['data'];//we get the name of each column using its index from POST request

                    $strexist = stripos($value[$column],$_POST['search']['value']);

                    if($strexist)
                    {
                        $outputary[] = $value;
                    }
                    
                }
            }
        }
        else
        {
            $outputary[] = $value;
        }
    }

    $count = count($outputary);

    $output = array(
        "draw" => intval($draw),
        "recordsTotal" => $count,
        "recordsFiltered" => $count,
        "data" => $outputary
    );

    return $output;
}

function bouns_cheque()
{
    $chequeid = $this->input->post('cheque_id');

    $this->db->where('cheq_id',$chequeid);
    $cheque = $this->db->get('hci_cheque')->row_array();

    $this->db->where('pay_id',$cheque['cheq_payment']);
    $paymnt = $this->db->get('hci_payment')->row_array();

    $this->db->where('payinv_payment',$paymnt['pay_id']);
    $paymntinv = $this->db->get('hci_paymentinvoice')->result_array();

    $this->db->trans_begin();

    $chtran['chtran_cheque'] = $chequeid;
    $chtran['chtran_description'] = 'Bounsed the Cheque Number : '.$cheque['cheq_number'].' [ Payment No. : '.$paymnt['pay_index'].' ]';
    $chtran['chtran_type'] = 'CHEQBOUNS';
    $chtran['chtran_date'] = date('Y-m-d');
    $chtran['chtran_user'] = $this->session->userdata('u_id');
    $chtran['chtran_timestamp'] = date('Y-m-d h:i:sa');
    $chtran['chtran_username']  = $this->session->userdata('u_name');
    $chtran['chtran_branch']    = $cheque['cheq_branch'];

    $this->db->insert('hci_chequetransaction',$chtran);
    $tranid = $this->db->insert_id();

    $this->db->where('cheq_id',$chequeid);
    $this->db->update('hci_cheque',array('cheq_status'=>'B'));

    foreach ($paymntinv as $inv) 
    {
        $this->db->set('inv_paidamount','inv_paidamount-'.$inv['payinv_amount'],FALSE);
        $this->db->set('inv_balanceamount','inv_balanceamount+'.$inv['payinv_amount'],FALSE);
        $this->db->set('inv_ispaid',0);
        $this->db->where('inv_id',$inv['payinv_invoice']);
        $this->db->update('hci_invoice');
    }

    $this->update_transaction($paymnt['pay_custype'],$paymnt['pay_customer'],$tranid,'CHEQBOUNS',$chtran['chtran_description'],$chtran['chtran_date'],'DR',$paymnt['pay_amount'],$chtran['chtran_user'],0,$chtran['chtran_timestamp'],$chtran['chtran_username']);


    if ($this->db->trans_status() === FALSE)
    {
        $this->db->trans_rollback();
        $this->msg->set('admission', "Failed to Bouns the Cheque. retry");
        return false;
    }
    else
    {
        $this->db->trans_commit(); 
        $this->msg->set('admission', "Cheque Bounsed successfully");
        return true;
    }
}

// except invoice erp integration. 
function accountsErpIntegration($gl_code,$type,$tranamount,$ref,$tran)
{
    $erp_db = $this->load->database('erp_db', TRUE); 
    $br_pref = '3';

    if($tran=='P')
    {
        $fld_id   = 'pay_id';
        $tbl_name = 'hci_payment';
        $fld_ref  = 'pay_refsid';
        $fld_glt  = 'pay_gltransid';
        $fld_grl  = 'pay_grlupdateid';
        $fld_grl2 = 'pay_grlupdateid2';
        $fld_stat = 'pay_status';
    }
    else if($tran=='C')
    {
        $fld_id   = 'cnote_id';
        $tbl_name = 'hci_creditnote';
        $fld_ref  = 'cnote_refsid';
        $fld_glt  = 'cnote_gltransid';
        $fld_grl  = 'cnote_grlupdateid';
        $fld_grl2 = 'cnote_grlupdateid2';
        $fld_stat = 'cnote_status';
    }
    else
    {
        $fld_id   = 'dnote_id';
        $tbl_name = 'hci_debitnote';
        $fld_ref  = 'dnote_refsid';
        $fld_glt  = 'dnote_gltransid';
        $fld_grl  = 'dnote_grlupdateid';
        $fld_grl2 = 'dnote_grlupdateid2';
        $fld_stat = 'dnote_status';
    }

    $erp_db->select('value');
    $erp_db->where('name','f_year');
    $f_year = $erp_db->get($br_pref.'_sys_prefs')->row_array();

    $erp_db->where('trans_type',0);
    $pattern = $erp_db->get($br_pref.'_reflines')->row_array();

    // eranga code
    $erp_db->select('MAX(trans_no) as trans');
    $erp_db->where('type',0);
    $erp_db->order_by('trans_no','asc');
    $jurnal = $erp_db->get($br_pref.'_journal')->row_array();

    $refs['id'] = $jurnal['trans'] + 1;// eranga code
    $refs['type'] = 0;
    $refs['reference'] = $pattern['pattern'];
    $erp_db->insert($br_pref.'_refs',$refs);

    $this->db->where($fld_id,$ref);
    $this->db->update($tbl_name,array($fld_ref => $pattern['pattern']));

    $autrail['type'] = 0;
    $autrail['trans_no'] =  $jurnal['trans'] + 1;// eranga code
    $autrail['user'] = 1;
    $autrail['fiscal_year'] = $f_year['value'];
    $autrail['gl_date'] = date('Y-m-d');
    $autrail['gl_seq'] = 0;
    $erp_db->insert($br_pref.'_audit_trail',$autrail);
    $gl_transid = $erp_db->insert_id();

    $this->db->where($fld_id,$ref);
    $this->db->update($tbl_name,array($fld_glt => $gl_transid));
                         
    // eranga code
    // if($paymode == 'cash')
    // {    
    //     $gl_code = '0201';
    // }
    // elseif($paymode == 'cheque')
    // {
    //     $gl_code = '0205';
    // }
    // elseif ($paymode == 'dirdeposit') 
    // {
    //     $gl_code = '0150';
    // }
    // elseif ($paymode == 'dirdeposit') 
    // {
    //     $gl_code = '4855';
    // }
    
    //  if($branch == 3){
    // $diamenton = '1'; 
    // }elseif($branch == 4){
    // $diamenton = '3'; 
    // }elseif ($branch == 5) {
    // $diamenton = '2'; 
    // }

    $gl_trans['type'] = 0;
    $gl_trans['type_no'] = $jurnal['trans'] + 1;// eranga code
    $gl_trans['tran_date'] = date('Y-m-d');
    $gl_trans['dimension_id'] = 0;
    $gl_trans['dimension2_id'] = 0;
    $gl_trans['account'] = $gl_code;
    if($tran == 'P' || $tran == 'C')
    {
        $gl_trans['amount'] = $tranamount;       
    }
    else
    {
        $gl_trans['amount'] = 0-($tranamount);
    }
    
    $erp_db->insert($br_pref.'_gl_trans',$gl_trans);
    $grlupdateid = $erp_db->insert_id();
                                             
    $cash['type'] = 0;
    $cash['type_no'] = $jurnal['trans'] + 1;
    $cash['tran_date'] = date('Y-m-d');
    $cash['account'] = '0054';
    if($tran == 'P' || $tran == 'C')
    {
        $cash['amount'] = 0-($tranamount);
    }
    else
    {
        $cash['amount'] = $tranamount;
    }
    $cash['dimension_id'] = 0;
    $erp_db->insert($br_pref.'_gl_trans',$cash);
    $grlupdateid2 = $erp_db->insert_id();
                                                    
    // eranga code
    $jnl['type']= 0;
    $jnl['trans_no']= $jurnal['trans'] + 1;
    $jnl['tran_date']= date('Y-m-d');
    $jnl['reference']= $pattern['pattern'];
    $jnl['event_date']= date('Y-m-d');
    $jnl['doc_date']= date('Y-m-d');
    $jnl['currency']= 'LKR';
    $jnl['amount']= ($tranamount);
    $jnl['rate']= 1;
    $erp_db->insert($br_pref.'_journal',$jnl);
                                                    
    $erp_db->set('pattern', 'pattern+1', FALSE);
    $erp_db->where('trans_type',0);
    $erp_db->update($br_pref.'_reflines');

    $this->db->where($fld_id,$ref);
    $result = $this->db->update($tbl_name,array($fld_grl => $grlupdateid,$fld_grl2 => $grlupdateid2,$fld_stat=>'T'));
}

}