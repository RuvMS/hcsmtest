<?php

class Hci_grade_model extends CI_model {

function save_grade()
{
	$grd_id 	= $this->input->post('grd_id');
	$grname 	= $this->input->post('grname');
	$grpromo    = $this->input->post('grpromo');
	$gr_code 	= $this->input->post('gr_code');

	$grd_save['grd_name'] 	= $grname;
	$grd_save['grd_status'] = 'A';
	$grd_save['grd_promotegrd'] = $grpromo;
	$grd_save['grd_code'] = $gr_code;

	if(empty($grd_id))
	{
		$save = $this->db->insert('hci_grade',$grd_save);
	}
	else
	{
		$this->db->where('grd_id',$grd_id);
		$save = $this->db->Update('hci_grade',$grd_save);
	}
	
	return $save;
}

function get_grd_info()
{
	$this->db->select('*');
	//$this->db->where('grd_status','A');
	$grd_info = $this->db->get('hci_grade')->result_array();

	$i = 0;
	foreach ($grd_info as $grd) 
	{
		$this->db->where('fsf_grade',$grd['grd_id']);
		$count = $this->db->count_all_results('hci_feestructure_fees');

		if($count==0)
		{
			$grd_info[$i]['grd_isused'] = 0;
		}
		else
		{
			$grd_info[$i]['grd_isused'] = 1;
		}

		$i++;
	}

	return $grd_info;
}

function remove_grade()
{
	$this->db->where('grd_id',$this->input->post('grd_id'));
	$result = $this->db->delete('hci_grade');

	if($result)
	{
		$this->session->set_flashdata('flashSuccess', 'Location Removed Successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to Remove Location. Retry.');
	}

	return $result;
}

function change_status()
{
	$this->db->where('grd_id',$this->input->post('grd_id'));
	$result = $this->db->update('hci_grade',array('grd_status'=>$this->input->post('new_s')));

	if($result)
	{
		if($this->input->post('new_s')=='A')
		{
			$this->session->set_flashdata('flashSuccess', 'Grade Activated Successfully.');
		}
		else
		{
			$this->session->set_flashdata('flashSuccess', 'Grade Deactivated Successfully.');
		}
		
	}
	else
	{
		if($this->input->post('new_s')=='A')
		{
			$this->session->set_flashdata('flashError', 'Failed to Activate Grade. Retry.');
		}
		else
		{
			$this->session->set_flashdata('flashError', 'Failed to Deactivate Grade. Retry.');
		}
	}

	return $result;
}

function save_class()
{
	$cls_id = $this->input->post('cls_id');
	$cls_name = $this->input->post('cls_name');
	$cls_code = $this->input->post('cls_code');

	$classsave['cls_name'] = $cls_name;
	$classsave['cls_code'] = $cls_code;
	$classsave['cls_status'] = 'A';

	if(empty($cls_id))
	{
		$save = $this->db->insert('hci_class',$classsave);
	}
	else
	{
		$this->db->where('cls_id',$cls_id);
		$save = $this->db->Update('hci_class',$classsave);
	}
	
	return $save;
}

function changeclass_status()
{
	$this->db->where('cls_id',$this->input->post('cls_id'));
	$result = $this->db->update('hci_class',array('cls_status'=>$this->input->post('new_s')));

	if($result)
	{
		if($this->input->post('new_s')=='A')
		{
			$this->session->set_flashdata('flashSuccess', 'Class Activated Successfully.');
		}
		else
		{
			$this->session->set_flashdata('flashSuccess', 'Class Deactivated Successfully.');
		}
		
	}
	else
	{
		if($this->input->post('new_s')=='A')
		{
			$this->session->set_flashdata('flashError', 'Failed to Activate Class. Retry.');
		}
		else
		{
			$this->session->set_flashdata('flashError', 'Failed to Deactivate Class. Retry.');
		}
	}

	return $result;
}

function get_classes_list($calledBy=null)
{
	if($calledBy != 'createview')
		$this->db->where('cls_status','A');

	return $this->db->get('hci_class')->result_array();
}

function load_year_classlist()
{
	$classeslist = $this->get_classes_list();

	$yrcls_branch  = $this->input->post('yrcls_branch');
	$yrcls_accyear = $this->input->post('yrcls_accyear');
	$yrcls_grade   = $this->input->post('yrcls_grade');

	$x = 0;
	foreach ($classeslist as $class) 
	{
		$this->db->where('yrcls_academicyear',$yrcls_accyear);
		$this->db->where('yrcls_grade',$yrcls_grade);
		$this->db->where('yrcls_branch',$yrcls_branch);
		$this->db->where('yrcls_status','A');
		$this->db->where('yrcls_class',$class['cls_id']);
		$is_assigned = $this->db->get('hci_accyear_class')->row_array();

		if(!empty($is_assigned))
		{
			$classeslist[$x]['is_assigned'] = 1;
			$classeslist[$x]['assiged_data'] = $is_assigned;
		}
		else
		{
			$classeslist[$x]['is_assigned'] = 0;
			$classeslist[$x]['assiged_data'] = array();
		}
		$x++;
	}

	return $classeslist;
}

function assign_class()
{
	if(isset($_POST['accyearClass']))
	{
		$yrcls_branch	= $this->input->post('yrcls_branch');
		$yrcls_accyear 	= $this->input->post('yrcls_accyear');
		$yrcls_grade 	= $this->input->post('yrcls_grade');
		$accyearClass 	= $this->input->post('accyearClass');

		$this->db->trans_begin();

		$this->db->where('yrcls_branch',$yrcls_branch);
		$this->db->where('yrcls_academicyear',$yrcls_accyear);
		$this->db->where('yrcls_grade',$yrcls_grade);
		$this->db->update('hci_accyear_class',array('yrcls_status' => 'D'));

		foreach ($accyearClass as $class)
		{
			$this->db->where('yrcls_branch',$yrcls_branch);
			$this->db->where('yrcls_academicyear',$yrcls_accyear);
			$this->db->where('yrcls_grade',$yrcls_grade);
			$this->db->where('yrcls_class',$class);
			$yearClassExists = $this->db->get('hci_accyear_class')->row_array();
			
			$yearClass['yrcls_status'] 		 = 'A';
			$yearClass['yrcls_maxstudents']  = $this->input->post('maxstu_'.$class);

			if(empty($yearClassExists))
			{	
				$yearClass['yrcls_academicyear'] = $yrcls_accyear;
				$yearClass['yrcls_grade'] 		 = $yrcls_grade;
				$yearClass['yrcls_branch'] 		 = $yrcls_branch;
				$yearClass['yrcls_class'] 		 = $class;

				$this->db->insert('hci_accyear_class',$yearClass);
			}
			else
			{
				$this->db->where('yrcls_id',$yearClassExists['yrcls_id']);
				$this->db->update('hci_accyear_class',$yearClass);
			}
		}

		if ($this->db->trans_status() === FALSE)
	    {
	        $this->db->trans_rollback();
	        return false;
	    }
	    else
	    {
	        $this->db->trans_commit();
	        return true;    
	    }
	}
	else
	{
		return false;
	}
}

function load_class_list()
{
	$acayear = $this->input->post('acayear');
	$grade = $this->input->post('grade');
	$branch = $this->input->post('branch');;

	$this->db->distinct('hci_class.cls_academicyear');
	$this->db->select('hgc_academicyears.*');
	$this->db->join('hgc_academicyears','hgc_academicyears.es_ac_year_id=hci_class.cls_academicyear');
	if($acayear!='all')
	{
		$this->db->where('hci_class.cls_academicyear',$acayear);
	}
	if($branch!='')
	{
		$this->db->where('hci_class.cls_branch',$branch);
	}
	$acayears = $this->db->get('hci_class')->result_array();

	$x = 0;
	foreach ($acayears as $value) 
	{
		$this->db->distinct('hci_class.cls_branch');
		$this->db->select('hgc_branch.*');
		$this->db->join('hgc_branch','hgc_branch.br_id=hci_class.cls_branch');
		$this->db->where('hci_class.cls_academicyear',$value['es_ac_year_id']);
		if($branch!='')
		{
			$this->db->where('hci_class.cls_branch',$branch);
		}
		$branches = $this->db->get('hci_class')->result_array();

		$this->db->distinct('hci_class.cls_grade');
		$this->db->select('hci_grade.*');
		$this->db->join('hci_grade','hci_grade.grd_id=hci_class.cls_grade');
		$this->db->where('hci_class.cls_academicyear',$value['es_ac_year_id']);
		if($grade!='all')
		{
			$this->db->where('hci_class.cls_grade',$grade);
		}
		if($branch!='')
		{
			$this->db->where('hci_class.cls_branch',$branch);
		}
		$grades = $this->db->get('hci_class')->result_array();

		$acayears[$x]['grades']=$grades;
		$acayears[$x]['branches']=$branches;
		$x++;
	}

	if($acayear!='all')
	{
		$this->db->where('cls_academicyear',$acayear);
	}
	if($grade!='all')
	{
		$this->db->where('cls_grade',$grade);
	}
	if($branch!='')
	{
		$this->db->where('hci_class.cls_branch',$branch);
	}
	$classes = $this->db->get('hci_class')->result_array();

	$all = array(
		'acayears' => $acayears,
		'classes' => $classes
		);
	
	return $all;
}

function get_assigned_list()
{
	//$this->db->
}
}