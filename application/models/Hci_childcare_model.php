<?php
class hci_childcare_model extends CI_model
{

function save_feestructure()
{
	$fs_id = $this->input->post('fs_id');
	$description = $this->input->post('description');
	$amt = $this->input->post('amt');
	$branch = $this->input->post('fs_branch');

	$fs_save['fs_name'] = $description;
	$fs_save['fs_branch'] = $branch;
	// $fs_save['fs_iscurrent'] = $;

	$this->db->trans_begin();

	if(empty($fs_id))
	{
		$this->db->insert('hci_childcare_feestructure',$fs_save);
		$fs_id = $this->db->insert_id();
	}
	else
	{
		$this->db->where('fs_id',$fs_id);
		$this->db->update('hci_childcare_feestructure',$fs_save);
	}

	$all_inps = $_POST;
	$all_inp_ary = array_keys($all_inps);

	foreach ($all_inp_ary as $inp) 
	{
		$temp = explode('_', $inp);

		if($temp[0]=='feeamt')
		{	
			$fee['fsf_amount'] = $this->input->post($inp);

			$this->db->where('fsf_type',$temp[1]);
			$this->db->where('fsf_fullhalf',$temp[2]);
			$this->db->where('fsf_feestructure',$fs_id);
			$isex = $this->db->get('hci_childcare_fsfee')->row_array();

			if(empty($isex))
			{
				$fee['fsf_type'] = $temp[1];
				$fee['fsf_feestructure'] = $fs_id;
				$fee['fsf_fullhalf'] = $temp[2];

				$this->db->insert('hci_childcare_fsfee',$fee);
			}
			else
			{
				$this->db->where('fsf_id',$isex['fsf_id']);
				$this->db->update('hci_childcare_fsfee',$fee);
			}
		}
	}
	
	if ($this->db->trans_status() === FALSE)
    {
        $this->db->trans_rollback();
        return false;
    }
    else
    {
        $this->db->trans_commit();
        return true;    
    }
}

function get_feestructure()
{
	$branchlist = $this->auth->get_accessbranch();

	$this->db->select('*');
	$this->db->where_in('fs_branch',$branchlist);
	$fss = $this->db->get('hci_childcare_feestructure')->result_array();
	return $fss;
}

function save_registration()
{
	$student = $this->input->post('student');
	$feestruc = $this->input->post('feestruc');

	$save['st_includetrans'] = '1';
	$save['st_transfeestructure'] = $feestruc;
	
	$this->db->where('id',$student);
	$this->db->update('st_details',$save);
	
	if ($this->db->trans_status() === FALSE)
    {
        $this->db->trans_rollback();
        return false;
    }
    else
    {
        $this->db->trans_commit();
        return true;    
    }
}
}