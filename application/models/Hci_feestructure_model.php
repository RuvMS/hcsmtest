<?php 
class Hci_feestructure_model extends CI_model {

function save_fee()
{
	$fee_id 		= $this->input->post('fee_id');
	$fee_cat		= $this->input->post('feecat');
	$invcode		= $this->input->post('invcode');
	$feestructure 	= $this->input->post('feestructure');
	$glcode			= $this->input->post('glcode');
	$grldescription	= $this->input->post('grldescription');

	$save_fee['fc_name']		 = $fee_cat;	
	$save_fee['fc_invcode']		 = $invcode;
	$save_fee['fc_feestructure'] = $feestructure;
	$save_fee['fc_glcode']		 = $glcode;
	$save_fee['fc_gldescription']= $grldescription;
	
	if(empty($fee_id))	
	{
		$feecatindx = $this->sequence->generate_sequence('FEE',3);
		$save_fee['fc_index'] = $feecatindx;
		$result = $this->db->insert('hci_feecategory',$save_fee);
		$fee_id = $this->db->insert_id();
	}	
	else
	{
		$this->db->where('fc_id',$fee_id);
		$result = $this->db->update('hci_feecategory',$save_fee);
	}

	return $result;
}

function get_feecats()
{
	$this->db->select('*');
	// $this->db->where('fc_includeinmain',1);
	$fees = $this->db->get('hci_feecategory')->result_array();

	return $fees;
}

function load_fee_templates()
{
	$this->db->select('*');
	$this->db->where_in('ft_branch',$this->input->post('id'));
	$temps = $this->db->get('hci_feetemplate')->result_array();

	return $temps;
}

function load_feestructure_data()
{
	$brlist = $this->auth->get_accessbranch();

	$this->db->select('hci_feemaster.*,at.ft_name as admtemp,ft.ft_name as trmtemp');
	$this->db->join('hci_feetemplate as at', 'hci_feemaster.fee_aftemp = at.ft_id','left');
    $this->db->join('hci_feetemplate as ft', 'hci_feemaster.fee_tftemp = ft.ft_id','left');

	if($_POST['fs_id']!=null)
	{
		$this->db->where('hci_feemaster.es_feemasterid',$_POST['fs_id']);
	}
	else
	{
		$this->db->where('hci_feemaster.fee_iscurrent',1);
	}
	$this->db->where_in('hci_feemaster.fee_branch',$brlist);
	$fs_data = $this->db->get('hci_feemaster')->row_array();



	// $this->db->where_in('fsf_feetemplate',array($fs_data['fee_aftemp'],$fs_data['fee_tftemp']));
	// $fees = $this->db->get('hci_feestructure_fees')->result_array();

	// $all = array(
	// 	'fs_data' => $fs_data,
	// 	'fees' => $fees
	// 	);

	return $fs_data;
}

function load_feetemplate_data()
{
	$brlist = $this->auth->get_accessbranch();

	$this->db->where('ft_id',$_POST['ft_id']);
	$this->db->where_in('ft_branch',$brlist);
	$fs_data = $this->db->get('hci_feetemplate')->row_array();

	$this->db->where('amtprd_feetemplate',$_POST['ft_id']);
	$amountperiods = $this->db->get('hci_feetemplateamountperiod')->result_array();

	$fs_data['amountperiods'] = $amountperiods;

	return $fs_data;
}

function save_fee_structure()
{
	$temp_type = $this->input->post('temp_type');
	$ft_name = $this->input->post('ft_name');
	$eff_date = $this->input->post('eff_date');
	$screenstatus = $this->input->post('screenstatus');
	$fs_branch = $this->input->post('fs_branch');
	$ft_id = $this->input->post('ft_id');

	$this->db->trans_begin();

	if(isset($_POST['is_curr']))
	{
		$this->db->where('ft_feecat',$temp_type);
		$update = $this->db->update('hci_feetemplate',array('ft_iscurrent'=>0));

		$tempsave['ft_iscurrent'] = 1;
	}
	else
	{
		$tempsave['ft_iscurrent'] = 0;
	}

	$tempsave['ft_name'] = $ft_name;
	$tempsave['ft_effectdate'] = $eff_date;

	if($screenstatus=='add' || $screenstatus=='asnew')
	{
		$tempsave['ft_feecat'] = $temp_type;
		$tempsave['ft_branch'] = $fs_branch;
		$this->db->insert('hci_feetemplate',$tempsave);
		$ft_id = $this->db->insert_id();
	}
	else
	{
		$this->db->where('ft_id',$ft_id);
		$this->db->update('hci_feetemplate',$tempsave);
	}

	if($temp_type == 1)
	{
		$templategrades = $this->input->post('admintemplategrades');
		$templatefees = $this->input->post('admintemplatefees');
	}
	else
	{
		$templategrades = $this->input->post('termtemplategrades');
		$templatefees = $this->input->post('termtemplatefees');
	}

	foreach ($templatefees as $fee) 
	{
		$this->db->where('amtprd_feetemplate',$ft_id);
		$this->db->where('amtprd_feecategory',$fee);
		$amtprdext = $this->db->get('hci_feetemplateamountperiod')->row_array();
		
		$amtprd['amtprd_option'] = $this->input->post('amtperiod_'.$fee);

		if(empty($amtprdext))
		{
			$amtprd['amtprd_feetemplate'] = $ft_id;
			$amtprd['amtprd_feecategory'] = $fee;

			$this->db->insert('hci_feetemplateamountperiod',$amtprd);
		}
		else
		{
			$this->db->where('amtprd_feetemplate',$ft_id);
			$this->db->where('amtprd_feecategory',$fee);
			$this->db->update('hci_feetemplateamountperiod',$amtprd);
		}

		foreach ($templategrades as $grd) 
		{
			$this->db->where('fsf_feetemplate',$ft_id);
			$this->db->where('fsf_grade',$grd);
			$this->db->where('fsf_fee',$fee);
			$feeext = $this->db->get('hci_feestructure_fees')->row_array();

			$savefee['fsf_amt'] = $this->input->post('amt_'.$grd.'_'.$fee);

			if($temp_type == 1)
			{
				$savefee['fsf_slabnew'] = $this->input->post('amtnew_'.$grd.'_'.$fee);
				$savefee['fsf_slabold'] = $this->input->post('amtold_'.$grd.'_'.$fee);
			}
			else
			{
				$savefee['fsf_slabnew'] = NULL;
				$savefee['fsf_slabold'] = NULL;
			}

			$savefee['fsf_status'] = 'A';

			if(empty($feeext))
			{
				$savefee['fsf_feetemplate'] = $ft_id;
				$savefee['fsf_grade'] = $grd;
				$savefee['fsf_fee'] = $fee;
				$this->db->insert('hci_feestructure_fees',$savefee);
			}
			else
			{
				$this->db->where('fsf_id',$feeext['fsf_id']);
				$this->db->update('hci_feestructure_fees',$savefee);
			}
		}
	}

    if ($this->db->trans_status() === FALSE)
	{
	    $this->db->trans_rollback();
		return false;
	}
	else
	{
		$this->db->trans_commit();
		return true;	
	}
}

function delete_feecat()
{
	$this->db->where('fc_id',$this->input->post('fc_id'));
	$result = $this->db->delete('hci_feecategory');

	if($result)
	{
		$this->session->set_flashdata('flashSuccess', 'Fee category Removed Successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to Remove Fee category. Retry.');
	}

	return $result;
}

function load_active_feestructures()
{
	$brlist = $this->auth->get_accessbranch();

	$this->db->select('hci_feemaster.*,at.ft_name as admtemp,ft.ft_name as trmtemp');
	$this->db->join('hci_feetemplate as at', 'hci_feemaster.fee_aftemp = at.ft_id','left');
    $this->db->join('hci_feetemplate as ft', 'hci_feemaster.fee_tftemp = ft.ft_id','left');
	$this->db->where('hci_feemaster.fee_status','A');
	$this->db->where_in('hci_feemaster.fee_branch',$brlist);
	$feests = $this->db->get('hci_feemaster')->result_array();

	return $feests;
}

function save_feegroups()
{
	$fs_id = $this->input->post('grp_fs');
	$gradegrp = $this->input->post('gradegrp');

	$this->db->trans_begin();

	foreach ($gradegrp as $grade) 
	{
		$temp = explode('_', $grade);

		$this->db->where('fg_fstructure',$fs_id);
		$this->db->where('fg_grade',$temp[0]);
		$isexist = $this->db->get('hci_feegroup')->row_array();

		$fg_save['fg_group'] = $temp[1];

		if(empty($isexist))
		{	
			$fg_save['fg_fstructure'] = $fs_id;
			$fg_save['fg_grade'] = $temp[0];

			$this->db->insert('hci_feegroup',$fg_save);
		}
		else
		{
			$this->db->where('fg_id',$isexist['fg_id']);
			$this->db->insert('hci_feegroup',$fg_save);
		}
	}

	if ($this->db->trans_status() === FALSE)
	{
	    $this->db->trans_rollback();
		return false;
	}
	else
	{
		$this->db->trans_commit();
		return true;	
	}
}

function load_feetemp_amounts()
{
	$this->db->where('fsf_feetemplate',$this->input->post('aft'));
	$afts = $this->db->get('hci_feestructure_fees')->result_array();

	$this->db->where('fsf_feetemplate',$this->input->post('tft'));
	$tfts = $this->db->get('hci_feestructure_fees')->result_array();

	$all = array(
		"afts" => $afts,
		"tfts" => $tfts
		);

	return $all;
}

function load_feetemplate_amounts()
{
	$this->db->where('fsf_feetemplate',$this->input->post('temp'));
	$amts = $this->db->get('hci_feestructure_fees')->result_array();

	return $amts;
}

function save_paymentplan()
{
	$description = $this->input->post('description');
	$fee_cat = $this->input->post('fee_cat');
	$ins_type = $this->input->post('ins_type');
	$pplan_id	= $this->input->post('pplan_id');

	$this->db->trans_begin();

	$plan_save['plan_name'] = $description;
	$plan_save['plan_fee'] = $fee_cat;
	$plan_save['plan_type'] = $ins_type;
	$plan_save['plan_status'] = 'A';

	if(empty($pplan_id))
	{
		$this->db->insert('hci_paymentplan',$plan_save);
		$pplan_id = $this->db->insert_id();
	}
	else
	{
		$this->db->where('plan_id',$pplan_id);
		$this->db->update('hci_paymentplan',$plan_save);

		$this->db->where('ins_plan',$pplan_id);
		$this->db->update('hci_instalment',array('ins_status'=>'D'));
	}

	// if($ins_type=='T')
	// {
	// 	$instalements = 3;
	// }
	// else if($ins_type=='M')
	// {
	// 	$instalements = 12;		
	// }
	// else
	// {
	// 	$instalements = 0;	
	// }

	// for($x=1; $x<=$instalements;$x++)
	// {
	// 	$ins_id = $this->input->post('ins_id_'.$x);

	// 	$ins_save['ins_number'] = $this->input->post('ins_'.$x);
	// 	$ins_save['ins_ddate'] 	= $this->input->post('ddate_'.$x);
	// 	$ins_save['ins_plan'] 	= $pplan_id;
	// 	$ins_save['ins_status'] = 'A';

	// 	$this->db->where('ins_plan',$pplan_id);
	// 	$this->db->where('ins_number',$x);
	// 	$ins_exist = $this->db->get('hci_instalment')->row_array();

	// 	if(empty($ins_exist))
	// 	{
	// 		$result = $this->db->insert('hci_instalment',$ins_save);
	// 	}
	// 	else
	// 	{
	// 		$this->db->where('ins_id',$ins_exist['ins_id']);
	// 		$result = $this->db->update('hci_instalment',$ins_save);
	// 	}
	// }

	if ($this->db->trans_status() === FALSE)
	{
	    $this->db->trans_rollback();
		return false;
	}
	else
	{
		$this->db->trans_commit();
		return true;	
	}
}

function load_payment_plans()
{
	$this->db->select('hci_paymentplan.*,hci_feecategory.fc_name');
	$this->db->join('hci_feecategory','hci_feecategory.fc_id=hci_paymentplan.plan_fee');
	$this->db->where('hci_paymentplan.plan_status','A');
	$plan = $this->db->get('hci_paymentplan')->result_array();
	
	$i = 0;
	foreach ($plan as $pln) 
	{
		$this->db->select('*');
		$this->db->where('ins_plan',$pln['plan_id']);
		$this->db->where('ins_status','A');
		$instalment = $this->db->get('hci_instalment')->result_array();
		$plan[$i]['inst'] = $instalment;
		$i++;
	}
	return $plan;
}

function edit_pay_plan()
{
	$this->db->select('*');
	$this->db->where('plan_id',$this->input->post('id'));
	$plan = $this->db->get('hci_paymentplan')->row_array();
	
	// $this->db->select('*');
	// $this->db->where('ins_plan',$this->input->post('id'));
	// $this->db->where('ins_status','A');
	// $instalment = $this->db->get('hci_instalment')->result_array();
	// $plan['inst'] = $instalment;

	return $plan;
}

function load_paymentplan()
{
	$this->db->select('*');
	$this->db->where('adj_id',$this->input->post('id'));
	$adjsmnt = $this->db->get('hci_adjusment')->row_array();

	$this->db->select('*');
	$this->db->where('plan_fee',$adjsmnt['adj_feecat']);
	$plans = $this->db->get('hci_paymentplan')->result_array();

	return $plans;
}

function load_periods()
{
	$this->db->select('*');
	$this->db->where('ins_plan',$this->input->post('id'));
	$this->db->where('ins_status','A');
	$plans = $this->db->get('hci_instalment')->result_array();

	return count($plans);
}

function load_feetemplate_list()
{	
	$type = $this->input->post('type');
	$branch = $this->input->post('branch');
	$brlist = $this->auth->get_accessbranch();

	if(!empty($type))
	{
		$this->db->where('ft_feecat',$type);
	}
	
	if(!empty($branch))
	{
		$this->db->where('ft_branch',$branch);
	}
	else
	{
		$this->db->where_in('ft_branch',$brlist);
	}

	$templist = $this->db->get('hci_feetemplate')->result_array();

	return $templist;
}

function confirm_regi()
{
	$branch = $this->input->post('branch');
	$grade = $this->input->post('grade');
	$tmtemp = $this->input->post('tmtemp');
	$astemp = $this->input->post('astemp');
	$term = $this->input->post('term');

	$this->db->where('term_id',$term);
    $termdata = $this->db->get('hci_term')->row_array();

	$this->db->join('hci_feecategory','hci_feecategory.fc_id=hci_feestructure_fees.fsf_fee');	
	$this->db->where('hci_feestructure_fees.fsf_grade',$grade);
	$this->db->where('hci_feestructure_fees.fsf_status','A');
	$this->db->where_in('hci_feestructure_fees.fsf_feetemplate',array($tmtemp,$astemp));
	$fees = $this->db->get('hci_feestructure_fees')->result_array();

	$x = 0;
	foreach ($fees as $fee) 
	{
		$this->db->where('amtprd_feetemplate',$fee['fsf_feetemplate']);
		$this->db->where('amtprd_feecategory',$fee['fsf_fee']);
		$amtperiod = $this->db->get('hci_feetemplateamountperiod')->row_array();

		$fees[$x]['amtprd_option'] = $amtperiod['amtprd_option'];
		$x++;
	}

	$this->db->where('adj_applyfor',1);
	$discounts = $this->db->get('hci_adjusment')->result_array();

	$all = array(
		'fees' => $fees,
		'discounts' => $discounts,
		'termdata' => $termdata
		);

	return $all;
}

}