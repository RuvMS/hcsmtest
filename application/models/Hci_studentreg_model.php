<?php
class Hci_studentreg_model extends CI_model
{
    public $obj=array();
    protected $_uploaded_file;
    //last inserted id
    public $last_insert=0;
    function index()
    {
        $data['main_content'] = 'hci_studentreg_view.php';
        $data['title'] = 'STU_REG';
        $this->load->view('includes/template', $data);
    }
    function get_stu_info()
    {
        $brlist = $this->auth->get_accessbranch();

        $this->db->select('*');
        $this->db->where_in('eq_branch',$brlist);
        $this->db->from('hci_enquiry');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;

    }
    function search($q){
        $this->db->select('pre_student_username');
        $this->db->like('pre_student_username', $q);
        $query = $this->db->get('hci_preadmission');
        $row_set = array();
        if($query->num_rows > 0){
            foreach ($query->result_array() as $row){
                $row_set[] = htmlentities(stripslashes($row['pre_student_username'])); //build an array
            }
            return $row_set;
        }
    }

    function get_registeed_Stu()
    {
        $this->db->select('hci_sibling.*,hci_preadmission.pre_student_username');
        $this->db->from('hci_sibling');
        $this->db->join('hci_preadmission','hci_preadmission.es_preadmissionid = hci_sibling.es_sibiling');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    function load_branch_student()
    {
        $this->db->select('*');
        $this->db->where('st_status','R');
        $this->db->where('st_branch',$this->input->get('id'));
        $reg_stu = $this->db->get('st_details')->result_array();

        $stuary = array();
        $x = 0;
        foreach ($reg_stu as $stu) 
        {
            $stuary[$x]['id'] = $stu['id'];
            $stuary[$x]['text'] = $stu['st_id'].' - '.$stu['family_name'].' '.$stu['other_names'];
            $x++;
        }

        return $stuary;
    }

    function registeed_Stu()
    {
        $brlist = $this->auth->get_accessbranch();
        $this->db->select('*');
        $this->db->where('st_status','R');
        $this->db->where_in('st_branch',$brlist);
        $reg_stu = $this->db->get('st_details')->result_array();

        return $reg_stu;
    }

    public function get_row($tbl="",$where="",$key=""){
        if($tbl == "" && $where == "")
            return false;
        if(is_array($where)) {
            $this->db->where($where);
        }
        $sql=$this->db->get($tbl);
        if($sql->num_rows() > 0 ){
            if($key!= ""){
                $row=$sql->row_array();
                return $row[$key];
            }else {
                return $sql->row_array();
            }
        }else
            return false;
    }

    public function insert($db="",$field="",$pic_id=""){
        if($field == "" && $db == "")
            return false;
        if($this->db->insert($db,$field)){
            $insert_id=$this->db->insert_id();
            $this->last_insert=$insert_id;
            //  $this->upload_files($db,$insert_id,$pic_id);
            return true;
        }
        else
            return false;
    }
    //update
    public function update($db="",$field="",$where=""){
        if($field == "" && $db == "")
            return false;
        if(is_array($where))
            $this->db->where($where);
        if($this->db->update($db,$field)){
            //   $this->upload_files($db,$where);
            return true;
        }
        else
            return false;
    }

    public function show_val($key=""){
        if(isset($this->obj[$key])){
            return  $this->obj[$key];
        }else {
            return "";
        }
    }
    public function get($tbl="",$where="",$order_by= "",$limit="-1",$start="-1"){
        if($tbl == "" && $where == "")
            return false;
        if(is_array($where)) {
            $this->db->where($where);
        }
        //limit
        if($limit >=0 && $start >=0) {
            $this->db->limit($limit, $start);
        }
        //order
        if($order_by!= "" ){
            $this->db->order_by($order_by['field'],$order_by['order']);
        }

        $sql=$this->db->get($tbl);
        if($sql->num_rows() > 0 ){

            return $sql->result_array();
        }else
            return false;

    }

    function get_feecat_pplans()
    {
        $this->db->select('*');
        $fees = $this->db->get('hci_feecategory')->result_array();

        $i = 0;
        foreach ($fees as $fee) 
        {
            $this->db->select('*');
            $this->db->where('plan_fee',$fee['fc_id']);
            $plan = $this->db->get('hci_paymentplan')->result_array();

            $fees[$i]['plans'] = $plan;
            $i++;
        }

        return $fees;
    }

    function get_staff_details()
    {
        $this->db->select('hgc_staff.*');
        // $this->db->where('stf_status','A');
        $this->db->where('stf_id' ,$this->input->post('id'));
        $stuf = $this->db->get('hgc_staff')->row_array();

        return $stuf;
    }

    function save_payplans($sid)
    {
        
    }

    function display_prefmethod_view()
    {
        $this->db->where('fsf_feetemplate',$this->input->post('ft'));
        $this->db->where('fsf_grade',$this->input->post('grd'));
        $feedata = $this->db->get('hci_feestructure_fees')->row_array();

        if(($feedata['fsf_slabnew'] == NULL) || ($feedata['fsf_slabnew'] == 0) || ($feedata['fsf_slabnew'] == $feedata['fsf_amt']))
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    function get_sibling_discounts()
    {
        $this->db->join('hci_feecategory','hci_feecategory.fc_id=hci_adjusment.adj_feecat');
        $this->db->where('adj_isspec',0);
        $this->db->where_in('adj_applyfor',array(2,3,4));
        $this->db->where('fc_feestructure !=',3);
        $discounts = $this->db->get('hci_adjusment')->result_array();

        return $discounts;
    }
}