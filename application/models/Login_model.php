<?php 
class Login_model extends CI_Model {

function authenticateLogin() 
{
	$name = $this->input->post('username');
	$pass = md5($this->input->post('password'));

	// $this->db->select('user.*,employee.emp_name,employee.emp_location,location.loc_name,designation.desi_name,designation.desi_access_level');
	// $this->db->join('employee','employee.emp_id=user.user_employee');
	// $this->db->join('designation','designation.desi_id=employee.emp_designation');
	// $this->db->join('location','location.loc_id=employee.emp_location');
	// $this->db->where("user.user_name",$this->input->post('usernm'));
	// $this->db->where("user.user_password", $pass);
	// $result = $this->db->get("user")->row_array();

	$this->db->select('*');
	$this->db->join('hgc_usergroup','hgc_usergroup.ug_id=hgc_user.user_ugroup');
	$this->db->where("hgc_user.user_name",$name);
	$this->db->where("hgc_user.user_password", $pass);
	$this->db->where("hgc_user.user_status",'A');
	$result = $this->db->get("hgc_user")->row_array();

	if(!empty($result))
	{
		$branch = $this->get_branchdetails($result['ug_branch']);

		$this->session->set_userdata('u_name',$result['user_name']);
		$this->session->set_userdata('u_id',$result['user_id']);
		$this->session->set_userdata('u_ugroup',$result['user_ugroup']);
		$this->session->set_userdata('u_emp',$result['user_employee']);
		$this->session->set_userdata('u_group',$result['user_group']);
		$this->session->set_userdata('u_branch',$result['user_branch']);
		$this->session->set_userdata('u_compname','Horizon College of Education - '.$branch['br_name']);
	    $this->session->set_userdata('u_compaddline1',$branch['br_addl1']);
	    $this->session->set_userdata('u_compaddline2',$branch['br_addl2']);
	    $this->session->set_userdata('u_compaddline3',$branch['br_city'].', '.$branch['br_country']);
	   	$this->session->set_userdata('u_branchcode',$branch['br_code']);
	}
	return $result;
}

function get_branchdetails($branch)
{
    $this->db->where('br_id',$branch);
    $branch = $this->db->get('hgc_branch')->row_array();
}

}