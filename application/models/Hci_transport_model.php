<?php
class Hci_transport_model extends CI_model
{
function save_route()
{
	$id = $this->input->post('route_id');
	$name = $this->input->post('rt_name');
	$rt_branch = $this->input->post('rt_branch');

	$rt_save['route_title'] = $name;
	$rt_save['route_branch'] = $rt_branch;
	$rt_save['status'] = 'A';

	$this->db->trans_begin();

	if(empty($id))
	{
		$rt_save['route_index'] = $this->sequence->generate_sequence('ROU',3);
		$this->db->insert('hci_trans_route',$rt_save);
	}
	else
	{
		$this->db->where('id',$id);
		$this->db->update('hci_trans_route',$rt_save);
	}
	
	if ($this->db->trans_status() === FALSE)
    {
        $this->db->trans_rollback();
        return false;
    }
    else
    {
        $this->db->trans_commit();
        return true;    
    }
}

function get_routes()
{	
	$brlist = $this->auth->get_accessbranch();

	$this->db->where('status','A');
	$this->db->where_in('route_branch',$brlist);
	$routes = $this->db->get('hci_trans_route')->result_array();

	return $routes;
}

function get_picking_points()
{
	$brlist = $this->auth->get_accessbranch();
	// $this->db->where('status','A');
	$this->db->join('hci_trans_route','hci_trans_route.id=hci_transpickpoint.tpp_route');
	$this->db->where_in('hci_trans_route.route_branch',$brlist);
	$picks = $this->db->get('hci_transpickpoint')->result_array();

	return $picks;
}

function save_pickpoint()
{
	$pp_route = $this->input->post('pp_route');
	$pick_id = $this->input->post('pick_id');
	$Pick_dist = $this->input->post('Pick_dist');
	$pick_point = $this->input->post('pick_point');
	
	$pp_save['place_name'] = $pick_point;
	$pp_save['tpp_route'] = $pp_route;
	$pp_save['tpp_stype'] = $Pick_dist;

	$this->db->trans_begin();

	if(empty($pick_id))
	{
		$pp_save['tpp_index'] = $this->sequence->generate_sequence('PCP',3);
		$this->db->insert('hci_transpickpoint',$pp_save);
	}
	else
	{
		$this->db->where('tr_place_id',$pick_id);
		$this->db->update('hci_transpickpoint',$pp_save);
	}
	
	if ($this->db->trans_status() === FALSE)
    {
        $this->db->trans_rollback();
        return false;
    }
    else
    {
        $this->db->trans_commit();
        return true;    
    }
}

function save_feestructure()
{
	$fs_id = $this->input->post('fs_id');
	$description = $this->input->post('description');
	$amt = $this->input->post('amt');
	$branch = $this->input->post('fs_branch');

	$fs_save['fs_name'] = $description;
	$fs_save['fs_branch'] = $branch;
	if(isset($_POST['is_curr']))
	{
		$fs_save['fs_iscurrent'] = 1;
	}
	else
	{
		$fs_save['fs_iscurrent'] = 0;
	}

	$this->db->trans_begin();

	if(empty($fs_id))
	{
		$fs_save['fs_index'] = $this->sequence->generate_sequence('TFS',3);
		$this->db->insert('hci_trans_feestructure',$fs_save);
		$fs_id = $this->db->insert_id();
	}
	else
	{
		$this->db->where('fs_id',$fs_id);
		$this->db->update('hci_trans_feestructure',$fs_save);
	}

	$all_inps = $_POST;
	$all_inp_ary = array_keys($all_inps);

	foreach ($all_inp_ary as $inp) 
	{
		$temp = explode('_', $inp);

		if($temp[0]=='feeamt')
		{	
			$fee['fsf_amount'] = $this->input->post($inp);

			$this->db->where('fsf_distance',$temp[1]);
			$this->db->where('fsf_isholiday',$temp[2]);
			$this->db->where('fsf_feestructure',$fs_id);
			$isex = $this->db->get('hci_trans_fsfee')->row_array();

			if(empty($isex))
			{
				$fee['fsf_fee'] = 4;
				$fee['fsf_distance'] = $temp[1];
				$fee['fsf_feestructure'] = $fs_id;
				$fee['fsf_isholiday'] = $temp[2];

				$this->db->insert('hci_trans_fsfee',$fee);
			}
			else
			{
				$this->db->where('fsf_id',$isex['fsf_id']);
				$this->db->update('hci_trans_fsfee',$fee);
			}
		}
	}
	
	if ($this->db->trans_status() === FALSE)
    {
        $this->db->trans_rollback();
        return false;
    }
    else
    {
        $this->db->trans_commit();
        return true;    
    }
}

function load_transfeestructures()
{
	$brlist = $this->auth->get_accessbranch();
	$id = $this->input->post('id');

	$this->db->where('fs_id',$id);
	$this->db->where_in('fs_branch',$brlist);
	$struct = $this->db->get('hci_trans_feestructure')->row_array();

	$this->db->where('fsf_feestructure',$struct['fs_id']);
	$fees = $this->db->get('hci_trans_fsfee')->result_array();

	$struct['fees'] = $fees;

	return $struct;
}

function get_feestructure()
{
	$brlist = $this->auth->get_accessbranch();
	$this->db->select('*');
	$this->db->where_in('fs_branch',$brlist);
	$fss = $this->db->get('hci_trans_feestructure')->result_array();
	return $fss;
}

function save_registration()
{
	$student 	= $this->input->post('student');
	$route 		= $this->input->post('route');
	$pickpoint 	= $this->input->post('pickpoint');
	$feestruc 	= $this->input->post('feestruc');
	$treg_id 	= $this->input->post('treg_id');
	$branch 	= $this->input->post('tran_branch');

	$this->db->where('es_reftable','st_details');
	$this->db->where('es_admissionid',$student);
	$family = $this->db->get('hci_sibling')->row_array();

	$this->db->join('hci_transregistration','hci_transregistration.treg_customer=hci_sibling.es_admissionid');
	$this->db->where('hci_sibling.es_reftable','st_details');
	$this->db->where('hci_sibling.es_family',$family['es_family']);
	$this->db->where('hci_transregistration.treg_status','A');
	$siblings = $this->db->get('hci_sibling')->result_array();

	$this->db->where('br_id',$branch);
    $branchinfo = $this->db->get('hgc_branch')->row_array();

	$treg_save['treg_customer'] 	= $student;
	$treg_save['treg_custtype'] 	= 'STUDENT';
	$treg_save['treg_feestructure'] = $feestruc;
	$treg_save['treg_route'] 		= $route;
	$treg_save['treg_pickpoint'] 	= $pickpoint;
	$treg_save['treg_status'] 		= 'A';
	$treg_save['treg_istempreg']	= 0;
	$treg_save['treg_date']			= date('Y-m-d');
	$treg_save['treg_famsequence'] 	= count($siblings)+1;
	$treg_save['treg_branch'] 		= $branch;

	if(empty($treg_id))
	{
		$treg_save['treg_index'] = $this->sequence->generate_sequence('TRG'.$branchinfo['br_code'],3);
		$this->db->insert('hci_transregistration',$treg_save);
		$treg_id = $this->db->insert_id();
	}
	else
	{
		$this->db->where('treg_id',$treg_id);
		$this->db->update('hci_transregistration',$treg_save);
	}

	$save['st_includetrans'] = '1';
	$save['st_transregisno'] = $treg_id;
	
	$this->db->where('id',$student);
	$this->db->update('st_details',$save);

	$discounts = array();

    $new_discount = $this->input->post('iId');
    
    foreach ($new_discount as $dis) 
    {
        if($dis>0)
        {
            $pref = "_".$dis;
        }
        else
        {
            $pref = '';
        }

        $adj_type = $this->input->post('adj_type'.$pref);
        $adj_name = $this->input->post('studisc'.$pref);
        $adj_calamttype = $this->input->post('adj_calamttype'.$pref);
        $adj_amt = $this->input->post('amount'.$pref);

        $transFedId = $this->db->get_where('hci_feecategory',array('fc_feestructure'=>3))->row_array();

        if(!empty($adj_name))
        {
            $save_adj['adj_description']   = $adj_name;
            $save_adj['adj_type']          = $adj_type;
            $save_adj['adj_feecat']        = $transFedId['fc_id'];
            $save_adj['adj_applyfor']      = 7;
            $save_adj['adj_amttype']       = $adj_calamttype;
            $save_adj['adj_amount']        = $adj_amt;
            $save_adj['adj_status']        = 'A';
            $save_adj['adj_isspec']        = 1;
            // $save_adj['adj_glcode']        = $;
            // $save_adj['adj_gldescription'] = $;
            $this->db->insert('hci_adjusment',$save_adj);

            $newdiscid = $this->db->insert_id();

            array_push($discounts,$newdiscid);
        }
    }

    if(!empty($discounts))
    {
        foreach ($discounts  as $disc) 
        {
            $disc_save['ats_reftable'] = 'st_details';
            $disc_save['ats_status'] = 'A';
            $disc_save['ats_adjstemp'] = $disc;
            $disc_save['ats_student'] = $student;

            $this->db->insert('hci_adjsstudent',$disc_save);
        }
    }

    $this->db->join('hci_adjusment','hci_adjusment.adj_id=hci_adjstemplate.at_adjusment');
    $this->db->where('hci_adjusment.adj_feecat',4);
    $discounts = $this->db->get('hci_adjstemplate')->result_array();

    foreach ($discounts as $discs) 
    {
    	$usedisc = false;
    	if($treg_save['treg_famsequence']==2 && $discs['at_applyfor']==2)
		{
			$usedisc = true;
		}
		
		if($treg_save['treg_famsequence']==3 && $discs['at_applyfor']==3)
		{
			$usedisc = true;
		}

		if($treg_save['treg_famsequence']>=4 && $discs['at_applyfor']==3)
		{
			$usedisc = true;
		}

		if($usedisc)
		{
		    $this->db->where('ats_student',$student);
		    $this->db->where('ats_reftable','st_details');
		    $this->db->where('ats_adjstemp',$discs['at_id']);
		    $isdiscexist = $this->db->get('hci_adjsstudent')->row_array();

		    $disc_save['ats_status'] = 'A';

		    if(empty($isdiscexist))
		    {
		    	$disc_save['ats_reftable'] = 'st_details';
			    $disc_save['ats_adjstemp'] = $discs['at_id'];
			    $disc_save['ats_student'] = intval($student);

		        $this->db->insert('hci_adjsstudent',$disc_save);
		    }
		    else
		    {
		        $this->db->where('ats_id',$isdiscexist['ats_id']);
		        $this->db->update('hci_adjsstudent',$disc_save);
		    }
		}
    }

    $this->db->where('plan_fee',4);
    $pplan = $this->db->get('hci_paymentplan')->row_array();

    $savespp['spp_reftable'] = 'st_details';

    $this->db->where('spp_customer',$student);
    $this->db->where('spp_reftable',$savespp['spp_reftable']);
    $this->db->where('spp_paymentplan',$pplan['plan_id']);
    $isexist = $this->db->get('hci_cuspaymentplan')->row_array();

    $savespp['spp_status'] = 'A';

    if(empty($isexist))
    {
        $savespp['spp_customer'] = $student;
        $savespp['spp_paymentplan'] = $pplan['plan_id'];

        $this->db->insert('hci_cuspaymentplan',$savespp);
    }
    else
    {
        $this->db->where('spp_id',$isexist['spp_id']);
        $this->db->update('hci_cuspaymentplan',$savespp);
    }

	$today= date("Y-m-d");

	$this->db->where('term_sdate <=',$today);
    $this->db->where('term_edate >=',$today);
    $currterm = $this->db->get('hci_term')->row_array();

	$this->load->model('hci_accounts_model');
	$this->hci_accounts_model->generate_invoice('STUDENT',$student,'TRANSPORT',$currterm,$today,$branchinfo);
	
	if ($this->db->trans_status() === FALSE)
    {
        $this->db->trans_rollback();
        return false;
    }
    else
    {
        $this->db->trans_commit();
        return true;    
    }
}

function load_pickingpoints()
{
	$this->db->where('tpp_route',$this->input->post('id'));
	$picks = $this->db->get('hci_transpickpoint')->result_array();

	return $picks;
}

function get_registrations()
{
	$brlist = $this->auth->get_accessbranch();

	$this->db->select('hci_transregistration.*,hci_trans_route.route_title,hci_transpickpoint.place_name,hci_trans_feestructure.fs_name');
	$this->db->join('hci_trans_route','hci_trans_route.id=hci_transregistration.treg_route');
	$this->db->join('hci_transpickpoint','hci_transpickpoint.tr_place_id=hci_transregistration.treg_pickpoint');
	$this->db->join('hci_trans_feestructure','hci_trans_feestructure.fs_id=hci_transregistration.treg_feestructure');
	$this->db->where_in('hci_transregistration.treg_branch',$brlist);
	$regs = $this->db->get('hci_transregistration')->result_array();

	$x = 0;
	foreach ($regs as $reg) 
	{
		if($reg['treg_custtype']=='STUDENT')
		{
			$this->db->select('st_id,family_name,other_names');
			$this->db->where('id',$reg['treg_customer']);
			if($reg['treg_istempreg']==1)
			{
				$studata = $this->db->get('st_details_temp')->row_array();
			}
			else
			{
				$studata = $this->db->get('st_details')->row_array();
			}

			$regs[$x]['cus_name'] = $studata['st_id']." - ".$studata['family_name'].' '.$studata['other_names'];
		}
		else
		{
			$regs[$x]['cus_name'] = 'External';
		}

		$x++;
	}

	return $regs;
}
}