<?php 
class User_model extends CI_Model {

function save_usergroup() 
{
	$group_id 	= $this->input->post('grp_id');
	$grname 	= $this->input->post('grp_name');
	$acc_level 	= $this->input->post('acc_level');

	$grp_save['ug_name'] 		= $grname;
	$grp_save['ug_status'] 	= 'A';
	$grp_save['ug_level'] = $acc_level;

	if($acc_level==2)
	{
		$grp_save['ug_company'] = $this->input->post('comp');
	}
	
	if($acc_level==3)
	{
		$grp_save['ug_company'] = $this->input->post('comp');
		$grp_save['ug_branch'] = $this->input->post('branch');
	}

	if(empty($group_id))
	{
		$save = $this->db->insert('hgc_usergroup',$grp_save);
	}
	else
	{
		$this->db->where('ug_id',$group_id);
		$save = $this->db->update('hgc_usergroup',$grp_save);
	}
	
	return $save;
}

function get_user_groups()
{
	$this->db->select('*');
	$this->db->where('ug_status','A');
	$groups = $this->db->get('hgc_usergroup')->result_array();

	return $groups;
}

function get_users_list()
{
	$this->db->select('hgc_user.*,hgc_usergroup.ug_name,hgc_group.grp_name,hgc_branch.br_name');
	$this->db->join('hgc_usergroup','hgc_usergroup.ug_id=hgc_user.user_ugroup');
	$this->db->join('hgc_group','hgc_group.grp_id=hgc_user.user_group','left');
	$this->db->join('hgc_branch','hgc_branch.br_id=hgc_user.user_branch','left');
	$users = $this->db->get('hgc_user')->result_array();

	return $users;
}

function reset_user()
{
	$id = $this->input->post('id');

	$this->db->where('user_id',$id);
	$result = $this->db->update('hgc_user',array('user_status'=>'A','user_password'=>md5('12345678')));

	if($result)
	{
		$this->session->set_flashdata('flashSuccess', 'User reset successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to reset User. Retry.');
	}

	return $result;
}

function update_status()
{
	$id = $this->input->post('id');
	$status = $this->input->post('status');

	if($status=='A')
	{
		$s_display = 'Activated';
		$f_display = 'Activate';
	}
	else
	{
		$s_display = 'Deactivated';
		$f_display = 'Deactivate';
	}

	$this->db->where('user_id',$id);
	$result = $this->db->update('hgc_user',array('user_status'=>$status));

	if($result)
	{
		$this->session->set_flashdata('flashSuccess', 'User '.$s_display.' successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to '.$f_display.' User. Retry.');
	}

	return $result;
}

function delete_user()
{
	$id = $this->input->post('id');

	$this->db->where('user_id',$id);
	$result = $this->db->delete('hgc_user');

	if($result)
	{
		$this->session->set_flashdata('flashSuccess', 'User Deleted successfully.');
	}
	else
	{
		$this->session->set_flashdata('flashError', 'Failed to Delete User. Retry.');
	}

	return $result;
}

}