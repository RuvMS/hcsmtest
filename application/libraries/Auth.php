<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 this class is used to check user privilages
*/
class auth
{

	var $CI = NULL;
	
	public function __construct()
	{
		$this->CI =& get_instance(); 
		$this->CI->load->database();
	}

	public function register_function()
	{
		/*URI Segment Capture*/
		$cur_url = $this->CI->uri->rsegments[1].($this->CI->uri->rsegments[2]!='index'?'/'.$this->CI->uri->rsegments[2]:'');

		$this->CI->db->where('func_url',$cur_url);
		$row = $this->CI->db->get('hgc_authfunction')->row_array();
	
		if(empty($row))
		{
			$funcdata['func_name'] 		= $this->CI->uri->rsegments[1].($this->CI->uri->rsegments[2]!='index'?' :: '.$this->CI->uri->rsegments[2]:'');
			$funcdata['func_url'] 		= $cur_url;
			$funcdata['func_status'] 	= 'A';
			$funcdata['func_isedit']	= 1;
			$funcdata['func_developer'] = DEVELOPER;
 
			$this->CI->db->insert('hgc_authfunction',$funcdata);
			$func_id = $this->CI->db->insert_id();

			// $rightdata['rgt_usergroup']	= 1;
			// $rightdata['rgt_user'] 		= 1;
			// $rightdata['rgt_type'] 		= 'function';
			// $rightdata['rgt_refid'] 	= $func_id;
			// $rightdata['rgt_status'] 	= 'A';
			// $rightdata['rgt_is']

			// $this->CI->db->insert('hgc_authright',$rightdata);

			redirect('authmanage/register_function/' . $func_id);

		}
		return true;
	}
	
	public function check_rights()
	{
		$cur_url = $this->CI->uri->rsegments[1].($this->CI->uri->rsegments[2]!='index'?'/'.$this->CI->uri->rsegments[2]:'');

		$this->CI->db->where('func_url',$cur_url);
		$func = $this->CI->db->get('hgc_authfunction')->row_array();

		if($func['func_isedit']==1)
		{
			$this->CI->db->where('ug_id',$this->CI->session->userdata('u_ugroup'));
			$authgroup = $this->CI->db->get('hgc_usergroup')->row_array();

			if($authgroup['ug_level']>3)
			{
				$this->CI->db->select('*');
				$this->CI->db->where('rgt_refid',$func['func_id']);
				$this->CI->db->where('rgt_type','function');
				$this->CI->db->where('rgt_usergroup',$this->CI->session->userdata('u_ugroup'));
				$this->CI->db->where('rgt_status','A');

				$right = $this->CI->db->get('hgc_authright')->result_array();

				if(empty($right))
				{
					return false;
				}
				else
				{
					return true;
				}
			}
			else
			{
				return true;
			}
		}
		else
		{
			return true;
		}
	}

	public function get_accessbranch()
	{
		$cur_url = $this->CI->uri->rsegments[1].($this->CI->uri->rsegments[2]!='index'?'/'.$this->CI->uri->rsegments[2]:'');

		$prev_brs = array();
		$this->CI->db->where('func_url',$cur_url);
		$func = $this->CI->db->get('hgc_authfunction')->row_array();

		$this->CI->db->where('ug_id',$_SESSION['u_ugroup']);
		$authgroup = $this->CI->db->get('hgc_usergroup')->row_array();

		if($func['func_isedit']==1)
		{
			if($authgroup['ug_level']>3)
			{
				$this->CI->db->select('*');
				$this->CI->db->where('rgt_refid',$func['func_id']);
				$this->CI->db->where('rgt_type','function');
				$this->CI->db->where('rgt_usergroup',$_SESSION['u_ugroup']);
				$this->CI->db->where('rgt_status','A');

				$right = $this->CI->db->get('hgc_authright')->result_array();

				foreach ($right as $rgt) 
				{
					array_push($prev_brs, $rgt['rgt_branch']);
				}
			}
			else
			{
				if($authgroup['ug_level']==2)
				{
					$this->CI->db->where('br_group',$authgroup['ug_company']);
				}

				if($authgroup['ug_level']==3)
				{
					$this->CI->db->where('br_id',$authgroup['ug_branch']);
				}

				$authbranch = $this->CI->db->get('hgc_branch')->result_array();

				foreach ($authbranch as $br) 
				{
					array_push($prev_brs, $br['br_id']);
				}
			}
		}

		if(empty($prev_brs))
		{
			$prev_brs[0] = null;
		}
		
		return $prev_brs;
	}

	public function check_navright()
	{
		$this->CI->db->distinct('func_module');
		$this->CI->db->select('func_module');
		$this->CI->db->where('func_status','A');
		$this->CI->db->where('func_isedit',1);
		$modules = $this->CI->db->get('hgc_authfunction')->result_array();

		$this->CI->db->where('ug_id',$this->CI->session->userdata('u_ugroup'));
		$authgroup = $this->CI->db->get('hgc_usergroup')->row_array();

		$x = 0;
		foreach ($modules as $mod) 
		{
			$this->CI->db->distinct('func_submodule');
			$this->CI->db->select('func_submodule');
			$this->CI->db->where('func_status','A');
			$this->CI->db->where('func_isedit',1);
			$this->CI->db->where('func_module',$mod['func_module']);
			$this->CI->db->where('func_type','Page Open');
			$submods = $this->CI->db->get('hgc_authfunction')->result_array();

			$subslist = array();

			foreach ($submods as $sub) 
			{	
				if($authgroup['ug_level']>3)
				{	
					$this->CI->db->select('*');
					$this->CI->db->join('hgc_authfunction','hgc_authfunction.func_id=hgc_authright.rgt_refid');
					$this->CI->db->where('rgt_usergroup',$this->CI->session->userdata('u_ugroup'));
					$this->CI->db->where('hgc_authfunction.func_submodule',$sub['func_submodule']);
					$this->CI->db->where('rgt_type','function');
					$this->CI->db->where('rgt_status','A');
					$right = $this->CI->db->get('hgc_authright')->result_array();

					if(!empty($right))
					{
						array_push($subslist, $sub['func_submodule']);
					}
				}
				else
				{
					array_push($subslist, $sub['func_submodule']);
				}
			}

			$modules[$x]['subslist'] = $subslist;
			$x++;
		}

		return $modules;
	}

	public function check_tab_access($type)
	{
		$this->CI->db->where('ug_id',$this->CI->session->userdata('u_ugroup'));
		$authgroup = $this->CI->db->get('hgc_usergroup')->row_array();

		if($type=="Group" || $type=="Company" || $type=="Fyear")
		{
			if($authgroup['ug_level']==1)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else if($type=="Branch" || $type=="Other")
		{
			// if($authgroup['ug_level']==1 || $authgroup['ug_level']==2)
			// {
				return true;
			// }
			// else
			// {
			// 	return false;
			// }
		}

		// $this->CI->db->where('func_status','A');
		// $this->CI->db->where('func_isedit',1);
		// if($type=="Group")
		// {
		// 	$this->CI->db->where('func_module','Configurations');
		// 	$this->CI->db->where('func_submodule','Group');
		// 	$this->CI->db->where('func_uitype !=','Full Page');
		// }
		// $functions = $this->CI->db->get('hgc_authfunction')->result_array();

		// if(!empty($functions))
		// {
		// 	$hasright = false;

		// 	foreach ($functions as $value) 
		// 	{
		// 		if($authgroup['ug_level']>3)
		// 		{
		// 			$this->CI->db->where('rgt_usergroup',$this->CI->session->userdata('u_ugroup'));
		// 		}
		// 		$this->CI->db->where('rgt_type','function');
		// 		$right = $this->CI->db->get('hgc_authright')->result_array();

		// 		if(!empty($right))
		// 		{
		// 			$hasright = true;
		// 		}
		// 	}

		// 	return $hasright;
		// }
		// else
		// {
		// 	return false;
		// }
	}

	public function get_ugroup_options()
	{
		$this->CI->db->where('ug_id',$this->CI->session->userdata('u_ugroup'));
		$authgroup = $this->CI->db->get('hgc_usergroup')->row_array();

		if($authgroup['ug_level']==1)
		{
			$ops = "<option value='1'>1</option>".
                   "<option value='2'>2</option>".
                   "<option value='3'>3</option>".
                   "<option value='4'>4</option>".
                   "<option value='5'>5</option>".
                   "<option value='6'>6</option>";
            $comp = 'all';
		}
		else if($authgroup['ug_level']==2)
		{
			$ops = "<option value='3'>3</option>".
				   "<option value='4'>4</option>".
                   "<option value='5'>5</option>".
                   "<option value='6'>6</option>";
            $comp = $this->CI->session->userdata('u_group');
		}
		else if($authgroup['ug_level']==3)
		{
			$ops = "<option value='4'>4</option>".
                   "<option value='5'>5</option>".
				   "<option value='6'>6</option>";
			$comp = $this->CI->session->userdata('u_group');
		}
		else
		{
			$ops = "";
			$comp = $this->CI->session->userdata('u_group');
		}
		$all = array(
			"ops" => $ops,
			"comp" => $comp
			);
		return $all;
	}

	public function get_brcode($brid)
	{
		$this->CI->db->where('br_id',$brid);
		$brnachdetail = $this->CI->db->get('hgc_branch')->row_array();

		return $brnachdetail['br_code'];
	}

	// public function check_link_access($link = '')
	// {
	// 	$e = explode('/',$link);

	// 	if(!array_key_exists(1,$e))
	// 		$e[1] = 'index';

	// 	$e = $e[0].$e[1];

	// 	$r = unserialize($this->CI->cache->file->get($this->CI->session->userdata('username')));

	// 	if(!empty($r)){
	//     	foreach ($r as $k) {
	//     		if($k == $e)
	//     			return true;
	//     	}
	//     }
	    
	//     return false;
	// }

	// public function check_module_access($module)
	// {
	// 	$r = $this->CI->session->userdata('m_rights');

	// 	if(!empty($r)){
	//     	foreach ($r as $k) {
	//     		if($k == $module)
	//     			return true;
	//     	}
	//     }

	//     return false;
	// }

	// public function check_submodule_access($smodule)
	// {
	// 	$r = $this->CI->session->userdata('sm_rights');

	// 	if(!empty($r)){
	//     	foreach ($r as $k) {
	//     		if($k == $smodule)
	//     			return true;
	//     	}
	//     }
	//     return false;
	// }

	// public function check_region_access()
	// {
	// 	$r = unserialize($this->CI->cache->file->get($this->CI->session->userdata('username')));

	// 	if(!empty($r))
	// 	{
	// 		foreach($r as $k)
	// 		{
	// 			if($k == 'alregion')
	// 				return true;
	// 		}
	// 	}

	// 	return false;
	// }

	// public function check_company_access()
	// {	
	// 	$r = unserialize($this->CI->cache->file->get($this->CI->session->userdata('username')));

	// 	if(!empty($r))
	// 	{
	// 		foreach($r as $k)
	// 		{
	// 			if($k == 'alcompany')
	// 				return true;
	// 		}
	// 	}

	// 	return false;
	// }

}	
