<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 this class is used to check user privilages
*/
class logger
{

	var $CI = NULL;
	
	public function __construct()
	{
		$this->CI =& get_instance(); 
		$this->CI->load->database();
	}

	public function systemlog($action,$status,$description,$date)
	{
		if($date==NULL)
		{
			$date = date('Y-m-d h:i:sa');
		}

		$logsave['log_code'] 		= $this->CI->sequence->generate_sequence('LOG',5);
		$logsave['log_action'] 		= $action;
		$logsave['log_status'] 		= $status;
		$logsave['log_description'] = $description;
		$logsave['log_timestamp'] 	= $date;
		$logsave['log_user'] 		= $this->CI->session->userdata('u_id');
		$logsave['log_project'] 	= $this->CI->session->userdata('u_project');

		$this->CI->db->insert('dfms_logger',$logsave);
		return true;
	}
}	
