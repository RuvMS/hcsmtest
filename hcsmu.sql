/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : hcsmu

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-04-12 12:03:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ci_sessions`
-- ----------------------------
DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ci_sessions
-- ----------------------------
INSERT INTO `ci_sessions` VALUES ('01cos0ielsj3pgkpa0chhs5dkjuu01kl', '127.0.0.1', '1523335682', 0x5F5F63695F6C6173745F726567656E65726174657C693A313532333333353638323B);
INSERT INTO `ci_sessions` VALUES ('8h6rlbr6uqjrttqqfjia0skkkgcc2dp4', '127.0.0.1', '1523336767', 0x5F5F63695F6C6173745F726567656E65726174657C693A313532333333363731363B);
INSERT INTO `ci_sessions` VALUES ('a0dkolvegeqn4pjdk83nrj5qprm7n051', '127.0.0.1', '1523270472', 0x5F5F63695F6C6173745F726567656E65726174657C693A313532333237303433373B755F6E616D657C733A353A2261646D696E223B755F69647C733A313A2231223B755F7567726F75707C733A313A2231223B755F656D707C4E3B755F67726F75707C4E3B755F6272616E63687C4E3B755F636F6D706E616D657C733A33313A22486F72697A6F6E20436F6C6C656765206F6620456475636174696F6E202D20223B755F636F6D706164646C696E65317C4E3B755F636F6D706164646C696E65327C4E3B755F636F6D706164646C696E65337C733A323A222C20223B755F6272616E6368636F64657C4E3B73657373696F6E7374757C733A313A2238223B73657373696F6E737475747970657C733A323A226474223B);
INSERT INTO `ci_sessions` VALUES ('ar95m12plgdegqedc05vmde40c9e5uct', '127.0.0.1', '1523272136', 0x5F5F63695F6C6173745F726567656E65726174657C693A313532333237323130343B755F6E616D657C733A353A2261646D696E223B755F69647C733A313A2231223B755F7567726F75707C733A313A2231223B755F656D707C4E3B755F67726F75707C4E3B755F6272616E63687C4E3B755F636F6D706E616D657C733A33313A22486F72697A6F6E20436F6C6C656765206F6620456475636174696F6E202D20223B755F636F6D706164646C696E65317C4E3B755F636F6D706164646C696E65327C4E3B755F636F6D706164646C696E65337C733A323A222C20223B755F6272616E6368636F64657C4E3B73657373696F6E7374757C733A313A2238223B73657373696F6E737475747970657C733A323A226474223B);
INSERT INTO `ci_sessions` VALUES ('iea22bbsef93e7abu655lj1l5i38ks23', '::1', '1523507270', 0x5F5F63695F6C6173745F726567656E65726174657C693A313532333530373234343B755F6E616D657C733A353A2261646D696E223B755F69647C733A313A2231223B755F7567726F75707C733A313A2231223B755F656D707C4E3B755F67726F75707C4E3B755F6272616E63687C4E3B755F636F6D706E616D657C733A33313A22486F72697A6F6E20436F6C6C656765206F6620456475636174696F6E202D20223B755F636F6D706164646C696E65317C4E3B755F636F6D706164646C696E65327C4E3B755F636F6D706164646C696E65337C733A323A222C20223B755F6272616E6368636F64657C4E3B);
INSERT INTO `ci_sessions` VALUES ('ivq1qgq4ukh1fmqpcblt8l11vgcnpnkh', '127.0.0.1', '1523267479', 0x5F5F63695F6C6173745F726567656E65726174657C693A313532333236373235303B755F6E616D657C733A353A2261646D696E223B755F69647C733A313A2231223B755F7567726F75707C733A313A2231223B755F656D707C4E3B755F67726F75707C4E3B755F6272616E63687C4E3B755F636F6D706E616D657C733A33313A22486F72697A6F6E20436F6C6C656765206F6620456475636174696F6E202D20223B755F636F6D706164646C696E65317C4E3B755F636F6D706164646C696E65327C4E3B755F636F6D706164646C696E65337C733A323A222C20223B755F6272616E6368636F64657C4E3B73657373696F6E7374757C733A313A2238223B73657373696F6E737475747970657C733A323A226474223B);
INSERT INTO `ci_sessions` VALUES ('mjcal25acjbhj433rgftd0k5s77ag7s8', '127.0.0.1', '1523273343', 0x5F5F63695F6C6173745F726567656E65726174657C693A313532333237323934373B755F6E616D657C733A353A2261646D696E223B755F69647C733A313A2231223B755F7567726F75707C733A313A2231223B755F656D707C4E3B755F67726F75707C4E3B755F6272616E63687C4E3B755F636F6D706E616D657C733A33313A22486F72697A6F6E20436F6C6C656765206F6620456475636174696F6E202D20223B755F636F6D706164646C696E65317C4E3B755F636F6D706164646C696E65327C4E3B755F636F6D706164646C696E65337C733A323A222C20223B755F6272616E6368636F64657C4E3B73657373696F6E7374757C733A313A2238223B73657373696F6E737475747970657C733A323A226474223B);
INSERT INTO `ci_sessions` VALUES ('n939ttcuob50gl2tk1m0n9hfuqg9a7ja', '127.0.0.1', '1523267580', 0x5F5F63695F6C6173745F726567656E65726174657C693A313532333236373535343B755F6E616D657C733A353A2261646D696E223B755F69647C733A313A2231223B755F7567726F75707C733A313A2231223B755F656D707C4E3B755F67726F75707C4E3B755F6272616E63687C4E3B755F636F6D706E616D657C733A33313A22486F72697A6F6E20436F6C6C656765206F6620456475636174696F6E202D20223B755F636F6D706164646C696E65317C4E3B755F636F6D706164646C696E65327C4E3B755F636F6D706164646C696E65337C733A323A222C20223B755F6272616E6368636F64657C4E3B73657373696F6E7374757C733A313A2238223B73657373696F6E737475747970657C733A323A226474223B);
INSERT INTO `ci_sessions` VALUES ('vrkpskuqipvr656m5d0hln45lc6ujd4c', '127.0.0.1', '1523327886', 0x5F5F63695F6C6173745F726567656E65726174657C693A313532333332373838363B);

-- ----------------------------
-- Table structure for `hci_adjsfees`
-- ----------------------------
DROP TABLE IF EXISTS `hci_adjsfees`;
CREATE TABLE `hci_adjsfees` (
  `atf_id` int(11) NOT NULL AUTO_INCREMENT,
  `atf_adjstemp` int(11) DEFAULT NULL,
  `aft_fsfee` int(11) DEFAULT NULL,
  `aft_tot` tinyint(4) DEFAULT NULL,
  `aft_new` tinyint(4) DEFAULT NULL,
  `aft_old` tinyint(4) DEFAULT NULL,
  `aft_status` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`atf_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of hci_adjsfees
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_adjsstudent`
-- ----------------------------
DROP TABLE IF EXISTS `hci_adjsstudent`;
CREATE TABLE `hci_adjsstudent` (
  `ats_id` int(11) NOT NULL AUTO_INCREMENT,
  `ats_adjstemp` int(11) DEFAULT NULL,
  `ats_student` int(11) DEFAULT NULL,
  `ats_status` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ats_reftable` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ats_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of hci_adjsstudent
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_adjstemplate`
-- ----------------------------
DROP TABLE IF EXISTS `hci_adjstemplate`;
CREATE TABLE `hci_adjstemplate` (
  `at_id` int(11) NOT NULL AUTO_INCREMENT,
  `at_adjusment` int(11) NOT NULL,
  `at_applyfor` int(11) NOT NULL,
  `at_students` int(11) NOT NULL,
  `at_feestructure` int(11) DEFAULT NULL,
  `at_paymentplan` int(11) DEFAULT NULL,
  `at_numofinstals` int(11) DEFAULT NULL,
  PRIMARY KEY (`at_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of hci_adjstemplate
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_adjusment`
-- ----------------------------
DROP TABLE IF EXISTS `hci_adjusment`;
CREATE TABLE `hci_adjusment` (
  `adj_id` int(11) NOT NULL AUTO_INCREMENT,
  `adj_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adj_type` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adj_feecat` int(11) DEFAULT NULL,
  `adj_applyfor` int(11) DEFAULT NULL,
  `adj_amttype` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adj_amount` decimal(10,2) DEFAULT NULL,
  `adj_status` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adj_isspec` tinyint(5) DEFAULT NULL,
  `adj_glcode` text COLLATE utf8_unicode_ci,
  `adj_gldescription` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`adj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of hci_adjusment
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_card`
-- ----------------------------
DROP TABLE IF EXISTS `hci_card`;
CREATE TABLE `hci_card` (
  `card_id` int(11) NOT NULL AUTO_INCREMENT,
  `card_deorcr` varchar(255) DEFAULT NULL,
  `card_type` varchar(255) DEFAULT NULL,
  `card_number` text,
  `card_issueingbank` int(11) DEFAULT NULL,
  `card_owner` int(11) DEFAULT NULL,
  PRIMARY KEY (`card_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of hci_card
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_cheque`
-- ----------------------------
DROP TABLE IF EXISTS `hci_cheque`;
CREATE TABLE `hci_cheque` (
  `cheq_id` int(11) NOT NULL AUTO_INCREMENT,
  `cheq_number` text,
  `cheq_date` date DEFAULT NULL,
  `cheq_status` varchar(5) DEFAULT NULL,
  `cheq_receipt` text,
  `cheq_payment` int(11) DEFAULT NULL,
  `cheq_datedto` date DEFAULT NULL,
  `cheq_bank` int(11) DEFAULT NULL,
  `cheq_branch` int(11) DEFAULT NULL,
  PRIMARY KEY (`cheq_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of hci_cheque
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_chequetransaction`
-- ----------------------------
DROP TABLE IF EXISTS `hci_chequetransaction`;
CREATE TABLE `hci_chequetransaction` (
  `chtran_id` int(11) NOT NULL AUTO_INCREMENT,
  `chtran_cheque` int(11) DEFAULT NULL,
  `chtran_description` varchar(255) DEFAULT NULL,
  `chtran_type` varchar(50) DEFAULT NULL,
  `chtran_date` date DEFAULT NULL,
  `chtran_user` int(11) DEFAULT NULL,
  `chtran_timestamp` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `chtran_username` varchar(75) DEFAULT NULL,
  `chtran_branch` int(11) DEFAULT NULL,
  PRIMARY KEY (`chtran_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of hci_chequetransaction
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_childcare_feestructure`
-- ----------------------------
DROP TABLE IF EXISTS `hci_childcare_feestructure`;
CREATE TABLE `hci_childcare_feestructure` (
  `fs_id` int(11) NOT NULL AUTO_INCREMENT,
  `fs_name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fs_iscurrent` int(11) DEFAULT NULL,
  `fs_branch` int(11) DEFAULT NULL,
  PRIMARY KEY (`fs_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of hci_childcare_feestructure
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_childcare_fsfee`
-- ----------------------------
DROP TABLE IF EXISTS `hci_childcare_fsfee`;
CREATE TABLE `hci_childcare_fsfee` (
  `fsf_id` int(11) NOT NULL AUTO_INCREMENT,
  `fsf_type` int(11) DEFAULT NULL,
  `fsf_fullhalf` int(11) DEFAULT NULL,
  `fsf_amount` decimal(10,2) DEFAULT NULL,
  `fsf_feestructure` int(11) DEFAULT NULL,
  PRIMARY KEY (`fsf_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=FIXED;

-- ----------------------------
-- Records of hci_childcare_fsfee
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_class`
-- ----------------------------
DROP TABLE IF EXISTS `hci_class`;
CREATE TABLE `hci_class` (
  `cls_id` int(11) NOT NULL AUTO_INCREMENT,
  `cls_academicyear` int(11) DEFAULT NULL,
  `cls_grade` int(11) DEFAULT NULL,
  `cls_name` varchar(10) DEFAULT NULL,
  `cls_code` varchar(10) DEFAULT NULL,
  `cls_status` varchar(5) DEFAULT NULL,
  `cls_building` int(11) DEFAULT NULL,
  `cls_branch` int(11) DEFAULT NULL,
  `cls_maxstudents` int(11) DEFAULT NULL,
  `cls_numofstudents` int(11) DEFAULT NULL,
  PRIMARY KEY (`cls_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of hci_class
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_classstudent`
-- ----------------------------
DROP TABLE IF EXISTS `hci_classstudent`;
CREATE TABLE `hci_classstudent` (
  `clsstu_id` int(11) NOT NULL AUTO_INCREMENT,
  `clsstu_class` int(11) DEFAULT NULL,
  `clsstu_ayear` int(11) DEFAULT NULL,
  `clsstu_grade` int(11) DEFAULT NULL,
  `clsstu_branch` int(11) DEFAULT NULL,
  `clsstu_student` int(11) DEFAULT NULL,
  `clsstu_assigneddate` date DEFAULT NULL,
  `clsstu_removeddate` date DEFAULT NULL,
  `clsstu_status` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`clsstu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of hci_classstudent
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_creditnote`
-- ----------------------------
DROP TABLE IF EXISTS `hci_creditnote`;
CREATE TABLE `hci_creditnote` (
  `cnote_id` int(11) NOT NULL AUTO_INCREMENT,
  `cnote_index` text,
  `cnote_date` date DEFAULT NULL,
  `cnote_custype` varchar(20) DEFAULT NULL,
  `cnote_customer` int(11) DEFAULT NULL,
  `cnote_invoice` int(11) DEFAULT NULL,
  `cnote_outstands` decimal(10,2) DEFAULT NULL,
  `cnote_cohbalance` decimal(10,2) DEFAULT NULL,
  `cnote_totamt` decimal(10,2) DEFAULT NULL,
  `cnote_description` varchar(255) DEFAULT NULL,
  `cnote_status` varchar(5) DEFAULT NULL,
  `cnote_compname` text,
  `cnote_compaddline1` text,
  `cnote_compaddline2` text,
  `cnote_compaddline3` text,
  `cnote_branch` int(11) DEFAULT NULL,
  `cnote_cusindex` text,
  `cnote_cusname` text,
  `cnote_cusaddline1` text,
  `cnote_cusaddline2` text,
  `cnote_cusaddline3` text,
  `cnote_createddate` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `cnote_createduser` int(11) DEFAULT NULL,
  `cnote_createusername` varchar(75) DEFAULT NULL,
  `cnote_remarks` text,
  `cnote_refsid` int(11) DEFAULT NULL,
  `cnote_gltransid` int(11) DEFAULT NULL,
  `cnote_grlupdateid` int(11) DEFAULT NULL,
  `cnote_grlupdateid2` int(11) DEFAULT NULL,
  PRIMARY KEY (`cnote_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of hci_creditnote
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_curriculum`
-- ----------------------------
DROP TABLE IF EXISTS `hci_curriculum`;
CREATE TABLE `hci_curriculum` (
  `cur_id` int(11) NOT NULL AUTO_INCREMENT,
  `cur_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`cur_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of hci_curriculum
-- ----------------------------
INSERT INTO `hci_curriculum` VALUES ('1', 'UK');
INSERT INTO `hci_curriculum` VALUES ('2', 'Local');

-- ----------------------------
-- Table structure for `hci_cusinvpaymentplan`
-- ----------------------------
DROP TABLE IF EXISTS `hci_cusinvpaymentplan`;
CREATE TABLE `hci_cusinvpaymentplan` (
  `ipp_id` int(11) NOT NULL AUTO_INCREMENT,
  `ipp_invoice` int(11) NOT NULL,
  `ipp_payplan` int(11) NOT NULL,
  `ipp_fee` int(11) DEFAULT NULL,
  PRIMARY KEY (`ipp_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=FIXED;

-- ----------------------------
-- Records of hci_cusinvpaymentplan
-- ----------------------------
INSERT INTO `hci_cusinvpaymentplan` VALUES ('1', '1', '1', '1');
INSERT INTO `hci_cusinvpaymentplan` VALUES ('2', '2', '2', '2');
INSERT INTO `hci_cusinvpaymentplan` VALUES ('3', '3', '1', '1');
INSERT INTO `hci_cusinvpaymentplan` VALUES ('4', '4', '2', '2');
INSERT INTO `hci_cusinvpaymentplan` VALUES ('5', '5', '1', '1');
INSERT INTO `hci_cusinvpaymentplan` VALUES ('6', '6', '2', '2');
INSERT INTO `hci_cusinvpaymentplan` VALUES ('7', '7', '1', '1');
INSERT INTO `hci_cusinvpaymentplan` VALUES ('8', '8', '2', '2');
INSERT INTO `hci_cusinvpaymentplan` VALUES ('9', '9', '1', '1');
INSERT INTO `hci_cusinvpaymentplan` VALUES ('10', '10', '2', '2');
INSERT INTO `hci_cusinvpaymentplan` VALUES ('11', '11', '1', '1');
INSERT INTO `hci_cusinvpaymentplan` VALUES ('12', '12', '2', '2');

-- ----------------------------
-- Table structure for `hci_cusoutstanding`
-- ----------------------------
DROP TABLE IF EXISTS `hci_cusoutstanding`;
CREATE TABLE `hci_cusoutstanding` (
  `outs_id` int(11) NOT NULL AUTO_INCREMENT,
  `outs_custype` varchar(255) DEFAULT NULL,
  `outs_customer` int(11) DEFAULT NULL,
  `outs_amount` decimal(10,2) DEFAULT NULL,
  `outs_cohbalance` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`outs_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of hci_cusoutstanding
-- ----------------------------
INSERT INTO `hci_cusoutstanding` VALUES ('1', 'TEMPSTU', '1', '-50000.00', '50000.00');
INSERT INTO `hci_cusoutstanding` VALUES ('2', 'TEMPSTU', '2', '-10000.00', '10000.00');
INSERT INTO `hci_cusoutstanding` VALUES ('3', 'TEMPSTU', '3', '-20000.00', '20000.00');
INSERT INTO `hci_cusoutstanding` VALUES ('4', 'TEMPSTU', '5', '-40000.00', '40000.00');
INSERT INTO `hci_cusoutstanding` VALUES ('5', 'TEMPSTU', '6', '-30000.00', '30000.00');
INSERT INTO `hci_cusoutstanding` VALUES ('6', 'STUDENT', '1', '253000.00', '50000.00');
INSERT INTO `hci_cusoutstanding` VALUES ('7', 'STUDENT', '2', '293000.00', '0.00');
INSERT INTO `hci_cusoutstanding` VALUES ('8', 'STUDENT', '3', '263000.00', '0.00');
INSERT INTO `hci_cusoutstanding` VALUES ('9', 'STUDENT', '4', '30000.00', '0.00');
INSERT INTO `hci_cusoutstanding` VALUES ('10', 'TEMPSTU', '7', '-50000.00', '50000.00');
INSERT INTO `hci_cusoutstanding` VALUES ('11', 'STUDENT', '5', '253000.00', '50000.00');
INSERT INTO `hci_cusoutstanding` VALUES ('12', 'TEMPSTU', '8', '0.00', '50000.00');
INSERT INTO `hci_cusoutstanding` VALUES ('13', 'STUDENT', '6', '0.00', '57000.00');

-- ----------------------------
-- Table structure for `hci_cuspaymentplan`
-- ----------------------------
DROP TABLE IF EXISTS `hci_cuspaymentplan`;
CREATE TABLE `hci_cuspaymentplan` (
  `spp_id` int(11) NOT NULL AUTO_INCREMENT,
  `spp_customer` int(11) DEFAULT NULL,
  `spp_paymentplan` int(11) DEFAULT NULL,
  `spp_status` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spp_reftable` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`spp_id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of hci_cuspaymentplan
-- ----------------------------
INSERT INTO `hci_cuspaymentplan` VALUES ('1', '1', '1', 'A', 'st_details');
INSERT INTO `hci_cuspaymentplan` VALUES ('2', '1', '2', 'A', 'st_details');
INSERT INTO `hci_cuspaymentplan` VALUES ('3', '1', '5', 'A', 'st_details');
INSERT INTO `hci_cuspaymentplan` VALUES ('4', '2', '1', 'A', 'st_details');
INSERT INTO `hci_cuspaymentplan` VALUES ('5', '2', '2', 'A', 'st_details');
INSERT INTO `hci_cuspaymentplan` VALUES ('6', '2', '5', 'A', 'st_details');
INSERT INTO `hci_cuspaymentplan` VALUES ('7', '3', '1', 'A', 'st_details_temp');
INSERT INTO `hci_cuspaymentplan` VALUES ('8', '3', '2', 'A', 'st_details_temp');
INSERT INTO `hci_cuspaymentplan` VALUES ('9', '3', '5', 'A', 'st_details_temp');
INSERT INTO `hci_cuspaymentplan` VALUES ('10', '4', '1', 'A', 'st_details_temp');
INSERT INTO `hci_cuspaymentplan` VALUES ('11', '4', '2', 'A', 'st_details_temp');
INSERT INTO `hci_cuspaymentplan` VALUES ('12', '4', '5', 'A', 'st_details_temp');
INSERT INTO `hci_cuspaymentplan` VALUES ('13', '3', '1', 'A', 'st_details');
INSERT INTO `hci_cuspaymentplan` VALUES ('14', '3', '2', 'A', 'st_details');
INSERT INTO `hci_cuspaymentplan` VALUES ('15', '3', '5', 'A', 'st_details');
INSERT INTO `hci_cuspaymentplan` VALUES ('16', '4', '1', 'A', 'st_details');
INSERT INTO `hci_cuspaymentplan` VALUES ('17', '4', '2', 'A', 'st_details');
INSERT INTO `hci_cuspaymentplan` VALUES ('18', '4', '5', 'A', 'st_details');
INSERT INTO `hci_cuspaymentplan` VALUES ('19', '5', '1', 'A', 'st_details');
INSERT INTO `hci_cuspaymentplan` VALUES ('20', '5', '2', 'A', 'st_details');
INSERT INTO `hci_cuspaymentplan` VALUES ('21', '5', '5', 'A', 'st_details');
INSERT INTO `hci_cuspaymentplan` VALUES ('22', '6', '1', 'A', 'st_details');
INSERT INTO `hci_cuspaymentplan` VALUES ('23', '6', '2', 'A', 'st_details');
INSERT INTO `hci_cuspaymentplan` VALUES ('24', '6', '5', 'A', 'st_details');
INSERT INTO `hci_cuspaymentplan` VALUES ('25', '9', '1', 'A', 'st_details_temp');
INSERT INTO `hci_cuspaymentplan` VALUES ('26', '9', '2', 'A', 'st_details_temp');
INSERT INTO `hci_cuspaymentplan` VALUES ('27', '9', '5', 'A', 'st_details_temp');

-- ----------------------------
-- Table structure for `hci_debitnote`
-- ----------------------------
DROP TABLE IF EXISTS `hci_debitnote`;
CREATE TABLE `hci_debitnote` (
  `dnote_id` int(11) NOT NULL AUTO_INCREMENT,
  `dnote_index` text,
  `dnote_date` date DEFAULT NULL,
  `dnote_custype` varchar(20) DEFAULT NULL,
  `dnote_customer` int(11) DEFAULT NULL,
  `dnote_invoice` int(11) DEFAULT NULL,
  `dnote_outstands` decimal(10,2) DEFAULT NULL,
  `dnote_cohbalance` decimal(10,2) DEFAULT NULL,
  `dnote_totamt` decimal(10,2) DEFAULT NULL,
  `dnote_description` varchar(255) DEFAULT NULL,
  `dnote_status` varchar(5) DEFAULT NULL,
  `dnote_compname` text,
  `dnote_compaddline1` text,
  `dnote_compaddline2` text,
  `dnote_compaddline3` text,
  `dnote_branch` int(11) DEFAULT NULL,
  `dnote_cusindex` text,
  `dnote_cusname` text,
  `dnote_cusaddline1` text,
  `dnote_cusaddline2` text,
  `dnote_cusaddline3` text,
  `dnote_createddate` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `dnote_createduser` int(11) DEFAULT NULL,
  `dnote_createusername` varchar(75) DEFAULT NULL,
  `dnote_remarks` text,
  `dnote_refsid` int(11) DEFAULT NULL,
  `dnote_gltransid` int(11) DEFAULT NULL,
  `dnote_grlupdateid` int(11) DEFAULT NULL,
  `dnote_grlupdateid2` int(11) DEFAULT NULL,
  PRIMARY KEY (`dnote_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of hci_debitnote
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_department`
-- ----------------------------
DROP TABLE IF EXISTS `hci_department`;
CREATE TABLE `hci_department` (
  `dept_id` int(11) NOT NULL AUTO_INCREMENT,
  `dept_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dept_status` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dept_branch` int(11) DEFAULT NULL,
  PRIMARY KEY (`dept_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of hci_department
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_designation`
-- ----------------------------
DROP TABLE IF EXISTS `hci_designation`;
CREATE TABLE `hci_designation` (
  `desig_id` int(11) NOT NULL AUTO_INCREMENT,
  `desig_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desig_status` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`desig_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of hci_designation
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_enquiry`
-- ----------------------------
DROP TABLE IF EXISTS `hci_enquiry`;
CREATE TABLE `hci_enquiry` (
  `es_enquiryid` int(11) NOT NULL AUTO_INCREMENT,
  `eq_serialno` int(11) NOT NULL,
  `eq_createdon` date NOT NULL,
  `eq_name` varchar(255) NOT NULL,
  `eq_lastname` varchar(255) NOT NULL,
  `eq_address` varchar(255) NOT NULL,
  `eq_fatheraddress2` varchar(255) NOT NULL,
  `eq_city` varchar(255) NOT NULL,
  `eq_wardname` varchar(255) NOT NULL,
  `eq_prev_university` varchar(255) NOT NULL,
  `eq_intake` int(11) NOT NULL,
  `eq_sex` varchar(255) NOT NULL,
  `eq_class` varchar(255) NOT NULL,
  `eq_phno` varchar(255) NOT NULL,
  `eq_mobile` varchar(255) NOT NULL,
  `eq_emailid` varchar(255) NOT NULL,
  `eq_reftype` varchar(255) NOT NULL,
  `eq_refname` varchar(255) NOT NULL,
  `eq_description` varchar(255) NOT NULL,
  `eq_formtype` varchar(255) NOT NULL,
  `eq_paymode` varchar(255) NOT NULL,
  `eq_chequeno` varchar(255) NOT NULL,
  `eq_bankname` varchar(255) NOT NULL,
  `eq_submitedon` date NOT NULL,
  `eq_acadamic` varchar(255) NOT NULL,
  `eq_marksobtain` int(11) NOT NULL,
  `eq_outof` int(11) NOT NULL,
  `eq_oralexam` varchar(255) NOT NULL,
  `eq_familyinteraction` varchar(255) NOT NULL,
  `eq_percentage` double NOT NULL,
  `eq_result` varchar(255) NOT NULL,
  `eq_amountpaid` varchar(255) NOT NULL,
  `eq_state` varchar(255) NOT NULL,
  `es_preadmissionid` int(11) NOT NULL,
  `eq_mothername` varchar(255) NOT NULL,
  `eq_motheraddress1` varchar(255) NOT NULL,
  `eq_motheraddress2` varchar(255) NOT NULL,
  `eq_mothercity` varchar(255) NOT NULL,
  `eq_fathername` varchar(255) NOT NULL,
  `eq_fathersoccupation` varchar(255) NOT NULL,
  `eq_motheroccupation` varchar(255) NOT NULL,
  `eq_mothermobile` varchar(255) NOT NULL,
  `eq_zip` varchar(255) NOT NULL,
  `college_id` int(11) NOT NULL,
  `eq_prv_acdmic` text NOT NULL,
  `eq_countryid` varchar(255) NOT NULL,
  `eq_dob` date NOT NULL,
  `eq_application_no` varchar(255) NOT NULL,
  `es_voucherentryid` int(11) NOT NULL,
  `pre_class` varchar(255) NOT NULL,
  `scat_id` varchar(255) NOT NULL,
  `eq_personname` varchar(255) NOT NULL,
  `eq_personaddress1` varchar(255) NOT NULL,
  `eq_personaddress2` varchar(255) NOT NULL,
  `eq_personcity` varchar(255) NOT NULL,
  `eq_personmobile` varchar(255) NOT NULL,
  `eq_physical_details1` varchar(255) NOT NULL,
  `eq_physical_details2` varchar(255) NOT NULL,
  `eq_personoccupation` varchar(255) NOT NULL,
  `physicaldisabilities` text,
  `allergicinfo` text,
  `underdoctorscare` text,
  `knowabt` int(11) DEFAULT NULL,
  `eq_emailid_2` varchar(255) DEFAULT NULL,
  `eq_emailid_3` varchar(255) DEFAULT NULL,
  `eq_foreignnum1` text,
  `eq_foreignnum2` text,
  `eq_phno2` varchar(255) DEFAULT NULL,
  `inqremarks` text,
  `eq_branch` int(11) DEFAULT NULL,
  `eq_code` text,
  `eq_altno` text,
  `eq_motheraltno` text,
  `eq_fathersplaceofwork` varchar(200) DEFAULT NULL,
  `eq_motherplaceofwork` varchar(200) DEFAULT NULL,
  `eq_personplaceofwork` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`es_enquiryid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of hci_enquiry
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_enquiry1`
-- ----------------------------
DROP TABLE IF EXISTS `hci_enquiry1`;
CREATE TABLE `hci_enquiry1` (
  `es_enquiryid` int(11) NOT NULL AUTO_INCREMENT,
  `eq_serialno` int(11) NOT NULL,
  `eq_createdon` date NOT NULL,
  `eq_name` varchar(255) NOT NULL,
  `eq_lastname` varchar(255) NOT NULL,
  `eq_address` varchar(255) NOT NULL,
  `eq_fatheraddress2` varchar(255) NOT NULL,
  `eq_city` varchar(255) NOT NULL,
  `eq_wardname` varchar(255) NOT NULL,
  `eq_prev_university` varchar(255) NOT NULL,
  `eq_intake` varchar(255) NOT NULL,
  `eq_sex` varchar(255) NOT NULL,
  `eq_class` varchar(255) NOT NULL,
  `eq_phno` varchar(255) NOT NULL,
  `eq_mobile` varchar(255) NOT NULL,
  `eq_emailid` varchar(255) NOT NULL,
  `eq_reftype` varchar(255) NOT NULL,
  `eq_refname` varchar(255) NOT NULL,
  `eq_description` varchar(255) NOT NULL,
  `eq_formtype` varchar(255) NOT NULL,
  `eq_paymode` varchar(255) NOT NULL,
  `eq_chequeno` varchar(255) NOT NULL,
  `eq_bankname` varchar(255) NOT NULL,
  `eq_submitedon` date NOT NULL,
  `eq_acadamic` varchar(255) NOT NULL,
  `eq_marksobtain` int(11) NOT NULL,
  `eq_outof` int(11) NOT NULL,
  `eq_oralexam` varchar(255) NOT NULL,
  `eq_familyinteraction` varchar(255) NOT NULL,
  `eq_percentage` double NOT NULL,
  `eq_result` varchar(255) NOT NULL,
  `eq_amountpaid` varchar(255) NOT NULL,
  `eq_state` varchar(255) NOT NULL,
  `es_preadmissionid` int(11) NOT NULL,
  `eq_mothername` varchar(255) NOT NULL,
  `eq_motheraddress1` varchar(255) NOT NULL,
  `eq_motheraddress2` varchar(255) NOT NULL,
  `eq_mothercity` varchar(255) NOT NULL,
  `eq_fathername` varchar(255) NOT NULL,
  `eq_fathersoccupation` varchar(255) NOT NULL,
  `eq_motheroccupation` varchar(255) NOT NULL,
  `eq_mothermobile` varchar(255) NOT NULL,
  `eq_zip` varchar(255) NOT NULL,
  `college_id` int(11) NOT NULL,
  `eq_prv_acdmic` text NOT NULL,
  `eq_countryid` varchar(255) NOT NULL,
  `eq_dob` date NOT NULL,
  `eq_application_no` varchar(255) NOT NULL,
  `es_voucherentryid` int(11) NOT NULL,
  `pre_class` varchar(255) NOT NULL,
  `scat_id` varchar(255) NOT NULL,
  `eq_personname` varchar(255) NOT NULL,
  `eq_personaddress1` varchar(255) NOT NULL,
  `eq_personaddress2` varchar(255) NOT NULL,
  `eq_personcity` varchar(255) NOT NULL,
  `eq_personmobile` varchar(255) NOT NULL,
  `eq_physical_details1` varchar(255) NOT NULL,
  `eq_physical_details2` varchar(255) NOT NULL,
  `eq_personoccupation` varchar(255) NOT NULL,
  PRIMARY KEY (`es_enquiryid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of hci_enquiry1
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_feecategory`
-- ----------------------------
DROP TABLE IF EXISTS `hci_feecategory`;
CREATE TABLE `hci_feecategory` (
  `fc_id` int(11) NOT NULL AUTO_INCREMENT,
  `fc_index` text,
  `fc_name` varchar(50) NOT NULL,
  `fc_invcode` varchar(3) DEFAULT NULL,
  `fc_feestructure` int(20) DEFAULT NULL,
  `fc_glcode` text,
  `fc_gldescription` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`fc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hci_feecategory
-- ----------------------------
INSERT INTO `hci_feecategory` VALUES ('1', 'FEE001', 'Admission Fee', 'AF', '1', '450', 'Admission Fee Income');
INSERT INTO `hci_feecategory` VALUES ('2', 'FEE002', 'Term Fee', 'TF', '2', '565', 'Term Fee Income');
INSERT INTO `hci_feecategory` VALUES ('3', 'FEE003', 'Service Fee', 'SF', '2', '562', 'Service Fee Template');
INSERT INTO `hci_feecategory` VALUES ('4', 'FEE004', 'Transport Fee', 'TR', '3', '555', 'Transport Fee Income');

-- ----------------------------
-- Table structure for `hci_feegroup`
-- ----------------------------
DROP TABLE IF EXISTS `hci_feegroup`;
CREATE TABLE `hci_feegroup` (
  `fg_id` int(11) NOT NULL AUTO_INCREMENT,
  `fg_fstructure` int(11) DEFAULT NULL,
  `fg_grade` int(11) DEFAULT NULL,
  `fg_group` int(11) DEFAULT NULL,
  PRIMARY KEY (`fg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hci_feegroup
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_feemaster`
-- ----------------------------
DROP TABLE IF EXISTS `hci_feemaster`;
CREATE TABLE `hci_feemaster` (
  `es_feemasterid` int(11) NOT NULL AUTO_INCREMENT,
  `fee_description` varchar(255) NOT NULL,
  `fee_status` varchar(11) NOT NULL,
  `fee_effdate` date DEFAULT NULL,
  `fee_aftemp` int(11) NOT NULL,
  `fee_tftemp` int(11) NOT NULL,
  `fee_extra1` varchar(255) NOT NULL,
  `fee_extra2` varchar(255) NOT NULL,
  `fee_fromdate` date NOT NULL,
  `fee_todate` date NOT NULL,
  `fee_iscurrent` tinyint(4) DEFAULT NULL,
  `fee_isspecial` tinyint(4) DEFAULT NULL,
  `fee_branch` int(11) DEFAULT NULL,
  `fee_code` text,
  PRIMARY KEY (`es_feemasterid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hci_feemaster
-- ----------------------------
INSERT INTO `hci_feemaster` VALUES ('1', 'SPECIAL_2_1', 'A', '2018-03-29', '1', '2', '', '', '0000-00-00', '0000-00-00', '0', null, null, 'FS//001');

-- ----------------------------
-- Table structure for `hci_feestructure_fees`
-- ----------------------------
DROP TABLE IF EXISTS `hci_feestructure_fees`;
CREATE TABLE `hci_feestructure_fees` (
  `fsf_id` int(11) NOT NULL AUTO_INCREMENT,
  `fsf_feetemplate` int(11) DEFAULT NULL,
  `fsf_grade` int(11) DEFAULT NULL,
  `fsf_fee` int(11) DEFAULT NULL,
  `fsf_amt` decimal(10,2) DEFAULT NULL,
  `fsf_slabnew` decimal(10,2) DEFAULT NULL,
  `fsf_slabold` decimal(10,2) DEFAULT NULL,
  `fsf_status` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`fsf_id`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hci_feestructure_fees
-- ----------------------------
INSERT INTO `hci_feestructure_fees` VALUES ('1', '1', '1', '1', '250000.00', '0.00', '0.00', 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('2', '1', '2', '1', '250000.00', '0.00', '0.00', 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('3', '1', '3', '1', '250000.00', '0.00', '0.00', 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('4', '1', '4', '1', '250000.00', '0.00', '0.00', 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('5', '1', '5', '1', '250000.00', '0.00', '0.00', 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('6', '1', '6', '1', '250000.00', '0.00', '0.00', 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('7', '1', '7', '1', '250000.00', '0.00', '0.00', 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('8', '1', '8', '1', '250000.00', '0.00', '0.00', 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('9', '1', '9', '1', '250000.00', '0.00', '0.00', 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('10', '1', '10', '1', '250000.00', '0.00', '0.00', 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('11', '1', '11', '1', '250000.00', '0.00', '0.00', 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('12', '1', '12', '1', '150000.00', '0.00', '0.00', 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('13', '1', '13', '1', '150000.00', '0.00', '0.00', 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('14', '1', '14', '1', '150000.00', '0.00', '0.00', 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('15', '1', '15', '1', '150000.00', '0.00', '0.00', 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('16', '1', '16', '1', '150000.00', '0.00', '0.00', 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('17', '2', '1', '2', '26500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('18', '2', '2', '2', '26500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('19', '2', '3', '2', '27500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('20', '2', '4', '2', '27500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('21', '2', '5', '2', '32500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('22', '2', '6', '2', '37600.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('23', '2', '7', '2', '39900.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('24', '2', '8', '2', '39900.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('25', '2', '9', '2', '40425.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('26', '2', '10', '2', '40425.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('27', '2', '11', '2', '40425.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('28', '2', '12', '2', '42750.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('29', '2', '13', '2', '42750.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('30', '2', '14', '2', '42750.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('31', '2', '15', '2', '47250.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('32', '2', '16', '2', '47250.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('33', '2', '1', '3', '0.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('34', '2', '2', '3', '6500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('35', '2', '3', '3', '6500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('36', '2', '4', '3', '6500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('37', '2', '5', '3', '8500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('38', '2', '6', '3', '8500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('39', '2', '7', '3', '8500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('40', '2', '8', '3', '8500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('41', '2', '9', '3', '8500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('42', '2', '10', '3', '8500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('43', '2', '11', '3', '8500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('44', '2', '12', '3', '8500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('45', '2', '13', '3', '8500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('46', '2', '14', '3', '8500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('47', '2', '15', '3', '8500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('48', '2', '16', '3', '8500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('49', '3', '1', '1', '250000.00', '100000.00', '0.00', 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('50', '3', '2', '1', '250000.00', '150000.00', '50000.00', 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('51', '3', '3', '1', '250000.00', '175000.00', '25000.00', 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('52', '3', '4', '1', '250000.00', '250000.00', '75000.00', 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('53', '3', '5', '1', '250000.00', '0.00', '0.00', 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('54', '3', '6', '1', '250000.00', '0.00', '0.00', 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('55', '3', '7', '1', '250000.00', '0.00', '0.00', 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('56', '3', '8', '1', '250000.00', '0.00', '0.00', 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('57', '3', '9', '1', '250000.00', '0.00', '0.00', 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('58', '3', '10', '1', '250000.00', '0.00', '0.00', 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('59', '3', '11', '1', '250000.00', '0.00', '0.00', 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('60', '3', '12', '1', '150000.00', '0.00', '0.00', 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('61', '3', '13', '1', '150000.00', '0.00', '0.00', 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('62', '3', '14', '1', '150000.00', '0.00', '0.00', 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('63', '3', '15', '1', '150000.00', '0.00', '0.00', 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('64', '3', '16', '1', '150000.00', '0.00', '0.00', 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('65', '4', '1', '2', '33000.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('66', '4', '2', '2', '34000.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('67', '4', '3', '2', '35000.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('68', '4', '4', '2', '40000.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('69', '4', '5', '2', '42000.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('70', '4', '6', '2', '45000.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('71', '4', '7', '2', '48000.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('72', '4', '8', '2', '51000.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('73', '4', '9', '2', '54000.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('74', '4', '10', '2', '57000.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('75', '4', '11', '2', '60000.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('76', '4', '12', '2', '63000.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('77', '4', '13', '2', '66000.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('78', '4', '14', '2', '70000.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('79', '4', '15', '2', '70000.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('80', '4', '16', '2', '70000.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('81', '4', '1', '3', '0.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('82', '4', '2', '3', '6500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('83', '4', '3', '3', '6500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('84', '4', '4', '3', '6500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('85', '4', '5', '3', '8500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('86', '4', '6', '3', '8500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('87', '4', '7', '3', '8500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('88', '4', '8', '3', '8500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('89', '4', '9', '3', '8500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('90', '4', '10', '3', '8500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('91', '4', '11', '3', '8500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('92', '4', '12', '3', '8500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('93', '4', '13', '3', '8500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('94', '4', '14', '3', '8500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('95', '4', '15', '3', '8500.00', null, null, 'A');
INSERT INTO `hci_feestructure_fees` VALUES ('96', '4', '16', '3', '8500.00', null, null, 'A');

-- ----------------------------
-- Table structure for `hci_feetemplate`
-- ----------------------------
DROP TABLE IF EXISTS `hci_feetemplate`;
CREATE TABLE `hci_feetemplate` (
  `ft_id` int(11) NOT NULL AUTO_INCREMENT,
  `ft_name` varchar(255) NOT NULL,
  `ft_feecat` int(11) NOT NULL,
  `ft_branch` int(11) DEFAULT NULL,
  `ft_iscurrent` tinyint(4) DEFAULT NULL,
  `ft_effectdate` date DEFAULT NULL,
  PRIMARY KEY (`ft_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hci_feetemplate
-- ----------------------------
INSERT INTO `hci_feetemplate` VALUES ('1', 'FS#9', '1', '1', '1', '2015-09-01');
INSERT INTO `hci_feetemplate` VALUES ('2', 'FS#7', '2', '1', '1', '2015-09-01');
INSERT INTO `hci_feetemplate` VALUES ('3', 'FS#10', '1', '1', '0', '2017-09-01');
INSERT INTO `hci_feetemplate` VALUES ('4', 'FS#8', '2', '1', '0', '2017-09-01');

-- ----------------------------
-- Table structure for `hci_feetemplateamountperiod`
-- ----------------------------
DROP TABLE IF EXISTS `hci_feetemplateamountperiod`;
CREATE TABLE `hci_feetemplateamountperiod` (
  `amtprd_id` int(11) NOT NULL AUTO_INCREMENT,
  `amtprd_feetemplate` int(11) NOT NULL,
  `amtprd_feecategory` int(11) NOT NULL,
  `amtprd_option` int(11) NOT NULL,
  PRIMARY KEY (`amtprd_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hci_feetemplateamountperiod
-- ----------------------------
INSERT INTO `hci_feetemplateamountperiod` VALUES ('1', '1', '1', '1');
INSERT INTO `hci_feetemplateamountperiod` VALUES ('2', '2', '2', '2');
INSERT INTO `hci_feetemplateamountperiod` VALUES ('3', '2', '3', '2');
INSERT INTO `hci_feetemplateamountperiod` VALUES ('4', '3', '1', '1');
INSERT INTO `hci_feetemplateamountperiod` VALUES ('5', '4', '2', '2');
INSERT INTO `hci_feetemplateamountperiod` VALUES ('6', '4', '3', '2');

-- ----------------------------
-- Table structure for `hci_finance_master`
-- ----------------------------
DROP TABLE IF EXISTS `hci_finance_master`;
CREATE TABLE `hci_finance_master` (
  `es_finance_masterid` int(11) NOT NULL AUTO_INCREMENT,
  `fi_startdate` date NOT NULL,
  `fi_enddate` date NOT NULL,
  `fi_iscurryear` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`es_finance_masterid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

-- ----------------------------
-- Records of hci_finance_master
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_grade`
-- ----------------------------
DROP TABLE IF EXISTS `hci_grade`;
CREATE TABLE `hci_grade` (
  `grd_id` int(11) NOT NULL AUTO_INCREMENT,
  `grd_name` varchar(30) NOT NULL,
  `grd_promotegrd` int(11) DEFAULT NULL,
  `grd_status` varchar(5) NOT NULL,
  `grd_code` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`grd_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hci_grade
-- ----------------------------
INSERT INTO `hci_grade` VALUES ('1', 'Play Group', '2', 'A', 'PG');
INSERT INTO `hci_grade` VALUES ('2', 'Nursery', '3', 'A', 'NR');
INSERT INTO `hci_grade` VALUES ('3', 'Kinder Garton', '4', 'A', 'KG');
INSERT INTO `hci_grade` VALUES ('4', 'Year 1', '5', 'A', 'Y1');
INSERT INTO `hci_grade` VALUES ('5', 'Year 2', '6', 'A', 'Y2');
INSERT INTO `hci_grade` VALUES ('6', 'Year  3', '7', 'A', 'Y3');
INSERT INTO `hci_grade` VALUES ('7', 'Year 4', '8', 'A', 'Y4');
INSERT INTO `hci_grade` VALUES ('8', 'Year 5', '9', 'A', 'Y5');
INSERT INTO `hci_grade` VALUES ('9', 'Year 6', '10', 'A', 'Y6');
INSERT INTO `hci_grade` VALUES ('10', 'Year 7', '11', 'A', 'Y7');
INSERT INTO `hci_grade` VALUES ('11', 'Year 8', '12', 'A', 'Y8');
INSERT INTO `hci_grade` VALUES ('12', 'Year 9', '13', 'A', 'Y9');
INSERT INTO `hci_grade` VALUES ('13', 'Year 10', '14', 'A', 'Y10');
INSERT INTO `hci_grade` VALUES ('14', 'Year 11', '15', 'A', 'Y11');
INSERT INTO `hci_grade` VALUES ('15', 'Year 12', '16', 'A', 'Y12');
INSERT INTO `hci_grade` VALUES ('16', 'Year 13', '0', 'A', 'Y13');
INSERT INTO `hci_grade` VALUES ('17', 'Lower Kindergarton', '4', 'D', 'LKG');

-- ----------------------------
-- Table structure for `hci_gradepromodetail`
-- ----------------------------
DROP TABLE IF EXISTS `hci_gradepromodetail`;
CREATE TABLE `hci_gradepromodetail` (
  `gp_id` int(11) NOT NULL AUTO_INCREMENT,
  `gp_refpromo` int(11) DEFAULT NULL,
  `gp_ayear` int(11) DEFAULT NULL,
  `gp_student` int(11) DEFAULT NULL,
  `gp_currgrade` int(11) DEFAULT NULL,
  `gp_nextgrade` int(11) DEFAULT NULL,
  `gp_branch` int(11) DEFAULT NULL,
  `gp_isleft` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`gp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=FIXED;

-- ----------------------------
-- Records of hci_gradepromodetail
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_gradepromotion`
-- ----------------------------
DROP TABLE IF EXISTS `hci_gradepromotion`;
CREATE TABLE `hci_gradepromotion` (
  `promo_id` int(11) NOT NULL AUTO_INCREMENT,
  `promo_accyear` int(11) NOT NULL,
  `promo_grade` int(11) NOT NULL,
  `promo_branch` int(11) NOT NULL,
  PRIMARY KEY (`promo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hci_gradepromotion
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_instalment`
-- ----------------------------
DROP TABLE IF EXISTS `hci_instalment`;
CREATE TABLE `hci_instalment` (
  `ins_id` int(11) NOT NULL AUTO_INCREMENT,
  `ins_number` int(11) DEFAULT NULL,
  `ins_plan` int(11) DEFAULT NULL,
  `ins_ddate` date DEFAULT NULL,
  `ins_status` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ins_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of hci_instalment
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_intake`
-- ----------------------------
DROP TABLE IF EXISTS `hci_intake`;
CREATE TABLE `hci_intake` (
  `int_id` int(11) NOT NULL AUTO_INCREMENT,
  `int_number` int(11) NOT NULL,
  `int_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `int_start` date NOT NULL,
  `int_end` date NOT NULL,
  `int_acyear` int(11) DEFAULT NULL,
  PRIMARY KEY (`int_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of hci_intake
-- ----------------------------
INSERT INTO `hci_intake` VALUES ('1', '1', 'September', '2016-09-01', '2016-12-31', '1');
INSERT INTO `hci_intake` VALUES ('2', '2', 'January', '2017-01-01', '2017-08-31', '1');
INSERT INTO `hci_intake` VALUES ('3', '1', 'September', '2017-09-01', '2018-04-30', '2');
INSERT INTO `hci_intake` VALUES ('4', '2', 'January', '2018-05-01', '2018-08-31', '2');

-- ----------------------------
-- Table structure for `hci_invoice`
-- ----------------------------
DROP TABLE IF EXISTS `hci_invoice`;
CREATE TABLE `hci_invoice` (
  `inv_id` int(11) NOT NULL AUTO_INCREMENT,
  `inv_index` text,
  `inv_customer` int(11) NOT NULL,
  `inv_date` date NOT NULL,
  `inv_status` varchar(5) NOT NULL,
  `inv_type` varchar(100) DEFAULT NULL,
  `inv_description` varchar(100) DEFAULT NULL,
  `inv_totamount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inv_totdiscounts` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inv_totadditions` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inv_extradiscounts` decimal(10,2) DEFAULT '0.00',
  `inv_extraadditions` decimal(10,2) DEFAULT '0.00',
  `inv_netamount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inv_totcnote` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inv_totdnote` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inv_ispaid` tinyint(1) NOT NULL DEFAULT '0',
  `inv_paidamount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inv_usedohbalance` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inv_cusindex` text,
  `inv_cusname` varchar(255) DEFAULT NULL,
  `inv_cusaddline1` text,
  `inv_cusaddline2` text,
  `inv_cusaddline3` text,
  `inv_custype` varchar(10) DEFAULT NULL,
  `inv_paycompletedate` date DEFAULT NULL,
  `inv_compname` varchar(255) DEFAULT NULL,
  `inv_compaddline1` text,
  `inv_compaddline2` text,
  `inv_compaddline3` text,
  `inv_feestructure` int(11) DEFAULT NULL,
  `inv_createdate` timestamp NULL DEFAULT NULL,
  `inv_createuser` int(11) DEFAULT NULL,
  `inv_createusername` varchar(75) DEFAULT NULL,
  `inv_updatedate` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `inv_updateuser` int(11) DEFAULT NULL,
  `inv_branch` int(11) DEFAULT NULL,
  `inv_fstype` int(11) DEFAULT NULL,
  `inv_balanceamount` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`inv_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of hci_invoice
-- ----------------------------
INSERT INTO `hci_invoice` VALUES ('1', 'INVMB1800066', '1', '2018-04-06', 'P', 'AF', 'REGISTRATION - Admission Fee', '250000.00', '0.00', '0.00', '0.00', '0.00', '250000.00', '0.00', '0.00', '0', '0.00', '0.00', 'MB/70/09/PG-1/1', 'uyuyuyu tyytytyty', '', '', '', 'STUDENT', null, 'Horizon College of Education -  - Malabe', 'xx', 'xxxxxxxxxxxx', 'xxxxxxxx', '1', '2018-04-06 06:26:58', '1', 'admin', null, '1', '1', '0', '250000.00');
INSERT INTO `hci_invoice` VALUES ('2', 'INVMB1800067', '1', '2018-04-06', 'P', 'TF', 'REGISTRATION - Term Fee', '53000.00', '0.00', '0.00', '0.00', '0.00', '53000.00', '0.00', '0.00', '0', '0.00', '0.00', 'MB/70/09/PG-1/1', 'uyuyuyu tyytytyty', '', '', '', 'STUDENT', null, 'Horizon College of Education -  - Malabe', 'xx', 'xxxxxxxxxxxx', 'xxxxxxxx', '1', '2018-04-06 06:26:58', '1', 'admin', null, '1', '1', '0', '53000.00');
INSERT INTO `hci_invoice` VALUES ('3', 'INVMB1800068', '2', '2018-04-06', 'P', 'AF', 'REGISTRATION - Admission Fee', '250000.00', '0.00', '0.00', '0.00', '0.00', '250000.00', '0.00', '0.00', '0', '0.00', '0.00', 'MB/70/09/PG-2/2', 'ghfghfgh hghgfhgfh', '', '', '', 'STUDENT', null, 'Horizon College of Education -  - Malabe', 'xx', 'xxxxxxxxxxxx', 'xxxxxxxx', '1', '2018-04-06 12:30:39', '1', 'admin', null, '1', '1', '0', '250000.00');
INSERT INTO `hci_invoice` VALUES ('4', 'INVMB1800069', '2', '2018-04-06', 'P', 'TF', 'REGISTRATION - Term Fee', '53000.00', '0.00', '0.00', '0.00', '0.00', '53000.00', '0.00', '0.00', '0', '0.00', '0.00', 'MB/70/09/PG-2/2', 'ghfghfgh hghgfhgfh', '', '', '', 'STUDENT', null, 'Horizon College of Education -  - Malabe', 'xx', 'xxxxxxxxxxxx', 'xxxxxxxx', '1', '2018-04-06 12:30:40', '1', 'admin', null, '1', '1', '0', '53000.00');
INSERT INTO `hci_invoice` VALUES ('5', 'INVMB1800070', '3', '2018-04-09', 'P', 'AF', 'REGISTRATION - Admission Fee', '250000.00', '0.00', '0.00', '0.00', '0.00', '250000.00', '0.00', '0.00', '0', '0.00', '0.00', 'MB/70/09/PG-3/3', 'fdgdgdfg fgfdgdfg', '', '', '', 'STUDENT', null, 'Horizon College of Education -  - Malabe', 'xx', 'xxxxxxxxxxxx', 'xxxxxxxx', '1', '2018-04-09 05:28:52', '1', 'admin', null, '1', '1', '0', '250000.00');
INSERT INTO `hci_invoice` VALUES ('6', 'INVMB1800071', '3', '2018-04-09', 'P', 'TF', 'REGISTRATION - Term Fee', '53000.00', '0.00', '0.00', '0.00', '0.00', '53000.00', '0.00', '0.00', '0', '0.00', '0.00', 'MB/70/09/PG-3/3', 'fdgdgdfg fgfdgdfg', '', '', '', 'STUDENT', null, 'Horizon College of Education -  - Malabe', 'xx', 'xxxxxxxxxxxx', 'xxxxxxxx', '1', '2018-04-09 05:28:52', '1', 'admin', null, '1', '1', '0', '53000.00');
INSERT INTO `hci_invoice` VALUES ('7', 'INVMB1800072', '4', '2018-04-09', 'P', 'AF', 'REGISTRATION - Admission Fee', '250000.00', '0.00', '0.00', '0.00', '0.00', '250000.00', '0.00', '0.00', '1', '250000.00', '0.00', 'MB/70/09/PG-4/4', 'fgdfgf fgfg', '', '', '', 'STUDENT', null, 'Horizon College of Education -  - Malabe', 'xx', 'xxxxxxxxxxxx', 'xxxxxxxx', '1', '2018-04-09 06:01:56', '1', 'admin', '2018-04-09 09:57:09', '1', '1', '0', '0.00');
INSERT INTO `hci_invoice` VALUES ('8', 'INVMB1800073', '4', '2018-04-09', 'P', 'TF', 'REGISTRATION - Term Fee', '53000.00', '0.00', '0.00', '0.00', '0.00', '53000.00', '0.00', '0.00', '1', '23000.00', '30000.00', 'MB/70/09/PG-4/4', 'fgdfgf fgfg', '', '', '', 'STUDENT', null, 'Horizon College of Education -  - Malabe', 'xx', 'xxxxxxxxxxxx', 'xxxxxxxx', '1', '2018-04-09 06:01:56', '1', 'admin', '2018-04-09 09:57:09', '1', '1', '0', '0.00');
INSERT INTO `hci_invoice` VALUES ('9', 'INVMB1800074', '5', '2018-04-09', 'P', 'AF', 'REGISTRATION - Admission Fee', '250000.00', '0.00', '0.00', '0.00', '0.00', '250000.00', '0.00', '0.00', '0', '0.00', '0.00', 'MB/70/09/PG-5/5', 'gdfgdfffffffffffffffff fdgdfgdfg', '', '', '', 'STUDENT', null, 'Horizon College of Education -  - Malabe', 'xx', 'xxxxxxxxxxxx', 'xxxxxxxx', '1', '2018-04-09 10:33:02', '1', 'admin', null, '1', '1', '0', '250000.00');
INSERT INTO `hci_invoice` VALUES ('10', 'INVMB1800075', '5', '2018-04-09', 'P', 'TF', 'REGISTRATION - Term Fee', '53000.00', '0.00', '0.00', '0.00', '0.00', '53000.00', '0.00', '0.00', '0', '0.00', '0.00', 'MB/70/09/PG-5/5', 'gdfgdfffffffffffffffff fdgdfgdfg', '', '', '', 'STUDENT', null, 'Horizon College of Education -  - Malabe', 'xx', 'xxxxxxxxxxxx', 'xxxxxxxx', '1', '2018-04-09 10:33:02', '1', 'admin', null, '1', '1', '0', '53000.00');
INSERT INTO `hci_invoice` VALUES ('11', 'INVMB1800076', '6', '2018-04-09', 'P', 'AF', 'REGISTRATION - Admission Fee', '250000.00', '0.00', '0.00', '0.00', '0.00', '250000.00', '0.00', '0.00', '1', '210000.00', '40000.00', 'MB/70/09/PG-6/6', 'ffgdfgdgdfg gfgdfgdfg', '', '', '', 'STUDENT', null, 'Horizon College of Education -  - Malabe', 'xx', 'xxxxxxxxxxxx', 'xxxxxxxx', '1', '2018-04-09 10:57:12', '1', 'admin', '2018-04-09 14:31:00', '1', '1', '0', '0.00');
INSERT INTO `hci_invoice` VALUES ('12', 'INVMB1800077', '6', '2018-04-09', 'P', 'TF', 'REGISTRATION - Term Fee', '53000.00', '0.00', '0.00', '0.00', '0.00', '53000.00', '0.00', '0.00', '1', '43000.00', '10000.00', 'MB/70/09/PG-6/6', 'ffgdfgdgdfg gfgdfgdfg', '', '', '', 'STUDENT', null, 'Horizon College of Education -  - Malabe', 'xx', 'xxxxxxxxxxxx', 'xxxxxxxx', '1', '2018-04-09 10:57:12', '1', 'admin', '2018-04-09 14:33:31', '1', '1', '0', '0.00');

-- ----------------------------
-- Table structure for `hci_invoicefee`
-- ----------------------------
DROP TABLE IF EXISTS `hci_invoicefee`;
CREATE TABLE `hci_invoicefee` (
  `invf_id` int(11) NOT NULL AUTO_INCREMENT,
  `invf_invoice` int(11) NOT NULL,
  `invf_fee` int(11) NOT NULL,
  `invf_feecat` int(11) DEFAULT NULL,
  `invf_feedescription` varchar(200) DEFAULT NULL,
  `invf_amount` decimal(10,2) NOT NULL,
  `invf_discount` decimal(10,2) DEFAULT NULL,
  `invf_addition` decimal(10,0) DEFAULT NULL,
  `invf_netamount` decimal(10,0) DEFAULT NULL,
  `invf_reftype` varchar(30) DEFAULT NULL,
  `invf_paidamount` decimal(10,2) DEFAULT NULL,
  `invf_balanceamount` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`invf_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of hci_invoicefee
-- ----------------------------
INSERT INTO `hci_invoicefee` VALUES ('1', '1', '1', '1', 'FEE001 - Admission Fee', '250000.00', '0.00', '0', '250000', 'hci_feemaster', '0.00', '250000.00');
INSERT INTO `hci_invoicefee` VALUES ('2', '2', '17', '2', 'FEE002 - Term Fee', '53000.00', '0.00', '0', '53000', 'hci_feemaster', '0.00', '53000.00');
INSERT INTO `hci_invoicefee` VALUES ('3', '3', '1', '1', 'FEE001 - Admission Fee', '250000.00', '0.00', '0', '250000', 'hci_feemaster', '0.00', '250000.00');
INSERT INTO `hci_invoicefee` VALUES ('4', '4', '17', '2', 'FEE002 - Term Fee', '53000.00', '0.00', '0', '53000', 'hci_feemaster', '0.00', '53000.00');
INSERT INTO `hci_invoicefee` VALUES ('5', '5', '1', '1', 'FEE001 - Admission Fee', '250000.00', '0.00', '0', '250000', 'hci_feemaster', '0.00', '250000.00');
INSERT INTO `hci_invoicefee` VALUES ('6', '6', '17', '2', 'FEE002 - Term Fee', '53000.00', '0.00', '0', '53000', 'hci_feemaster', '0.00', '53000.00');
INSERT INTO `hci_invoicefee` VALUES ('7', '7', '1', '1', 'FEE001 - Admission Fee', '250000.00', '0.00', '0', '250000', 'hci_feemaster', '0.00', '250000.00');
INSERT INTO `hci_invoicefee` VALUES ('8', '8', '17', '2', 'FEE002 - Term Fee', '53000.00', '0.00', '0', '53000', 'hci_feemaster', '0.00', '53000.00');
INSERT INTO `hci_invoicefee` VALUES ('9', '9', '1', '1', 'FEE001 - Admission Fee', '250000.00', '0.00', '0', '250000', 'hci_feemaster', '0.00', '250000.00');
INSERT INTO `hci_invoicefee` VALUES ('10', '10', '17', '2', 'FEE002 - Term Fee', '53000.00', '0.00', '0', '53000', 'hci_feemaster', '0.00', '53000.00');
INSERT INTO `hci_invoicefee` VALUES ('11', '11', '1', '1', 'FEE001 - Admission Fee', '250000.00', '0.00', '0', '250000', 'hci_feemaster', '0.00', '250000.00');
INSERT INTO `hci_invoicefee` VALUES ('12', '12', '17', '2', 'FEE002 - Term Fee', '53000.00', '0.00', '0', '53000', 'hci_feemaster', '0.00', '53000.00');

-- ----------------------------
-- Table structure for `hci_monthend`
-- ----------------------------
DROP TABLE IF EXISTS `hci_monthend`;
CREATE TABLE `hci_monthend` (
  `mend_id` int(11) NOT NULL AUTO_INCREMENT,
  `mend_sdate` date DEFAULT NULL,
  `mend_edate` date DEFAULT NULL,
  `mend_accyear` int(11) DEFAULT NULL,
  `mend_month` varchar(255) DEFAULT NULL,
  `mend_date` date DEFAULT NULL,
  PRIMARY KEY (`mend_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of hci_monthend
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_payment`
-- ----------------------------
DROP TABLE IF EXISTS `hci_payment`;
CREATE TABLE `hci_payment` (
  `pay_id` int(11) NOT NULL AUTO_INCREMENT,
  `pay_index` text,
  `pay_description` varchar(255) DEFAULT NULL,
  `pay_date` date DEFAULT NULL,
  `pay_custype` varchar(20) DEFAULT NULL,
  `pay_customer` int(11) DEFAULT NULL,
  `pay_amount` decimal(10,2) DEFAULT NULL,
  `pay_usedbalance` decimal(10,2) DEFAULT NULL,
  `pay_outstanding` decimal(10,2) DEFAULT NULL,
  `pay_cohbalance` decimal(10,2) DEFAULT NULL,
  `pay_status` varchar(5) DEFAULT NULL,
  `pay_type` varchar(5) DEFAULT NULL,
  `pay_refsid` int(11) DEFAULT NULL,
  `pay_gltransid` int(11) DEFAULT NULL,
  `pay_grlupdateid` int(11) DEFAULT NULL,
  `pay_grlupdateid2` int(11) DEFAULT NULL,
  `pay_createuser` int(11) DEFAULT NULL,
  `pay_createdate` timestamp NULL DEFAULT NULL,
  `pay_branch` int(11) DEFAULT NULL,
  `pay_remarks` text,
  PRIMARY KEY (`pay_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of hci_payment
-- ----------------------------
INSERT INTO `hci_payment` VALUES ('1', 'PAYMB1800019', 'ghfhg', '2018-04-06', 'TEMPSTU', '1', '50000.00', null, '0.00', '0.00', 'T', 'ADV', '8708', '8709', '26057', '26058', '1', '2018-04-06 06:17:31', '1', '');
INSERT INTO `hci_payment` VALUES ('2', 'PAYMB1800020', 'fgdfgdfgdfg', '2018-04-06', 'TEMPSTU', '2', '10000.00', null, '0.00', '0.00', 'T', 'ADV', '8709', '8710', '26059', '26060', '1', '2018-04-06 06:17:52', '1', '');
INSERT INTO `hci_payment` VALUES ('3', 'PAYMB1800021', 'ghhgfhg', '2018-04-06', 'TEMPSTU', '3', '20000.00', null, '0.00', '0.00', 'T', 'ADV', '8710', '8711', '26061', '26062', '1', '2018-04-06 06:18:17', '1', '');
INSERT INTO `hci_payment` VALUES ('4', 'PAYMB1800022', 'uiuiuyi', '2018-04-06', 'TEMPSTU', '5', '40000.00', null, '0.00', '0.00', 'T', 'ADV', '8711', '8712', '26063', '26064', '1', '2018-04-06 06:26:07', '1', '');
INSERT INTO `hci_payment` VALUES ('5', 'PAYMB1800023', 'jkhkhjkhjk', '2018-04-06', 'TEMPSTU', '6', '30000.00', null, '0.00', '0.00', 'T', 'ADV', '8712', '8713', '26065', '26066', '1', '2018-04-06 06:26:29', '1', '');
INSERT INTO `hci_payment` VALUES ('6', 'PAYMB1800024', 'hghfgh', '2018-04-09', 'STUDENT', '4', '273000.00', '280000.00', '303000.00', '30000.00', 'T', 'INV', '8713', '8714', '26067', '26068', '1', '2018-04-09 06:27:09', '1', '');
INSERT INTO `hci_payment` VALUES ('7', 'PAYMB1800025', 'gfhgfhfgh', '2018-04-09', 'TEMPSTU', '7', '50000.00', null, '0.00', '0.00', 'T', 'ADV', '8714', '8715', '26069', '26070', '1', '2018-04-09 10:28:42', '1', '');
INSERT INTO `hci_payment` VALUES ('8', 'PAYMB1800026', 'yuytutyu', '2018-04-09', 'TEMPSTU', '8', '50000.00', null, '0.00', '0.00', 'T', 'ADV', '8715', '8716', '26071', '26072', '1', '2018-04-09 10:55:28', '1', '');
INSERT INTO `hci_payment` VALUES ('9', 'PAYMB1800027', 'hjkjhj', '2018-04-09', 'STUDENT', '6', '200000.00', null, '303000.00', '50000.00', 'T', 'INV', '8716', '8717', '26073', '26074', '1', '2018-04-09 10:58:41', '1', '');
INSERT INTO `hci_payment` VALUES ('10', 'PAYMB1800028', 'jkhjk', '2018-04-09', 'STUDENT', '6', '10000.00', '60000.00', '103000.00', '50000.00', 'T', 'INV', '8717', '8718', '26075', '26076', '1', '2018-04-09 11:01:00', '1', '');
INSERT INTO `hci_payment` VALUES ('11', 'PAYMB1800029', 'kjhkhjkjh', '2018-04-09', 'STUDENT', '6', '100000.00', null, '43000.00', '0.00', 'T', 'INV', '8718', '8719', '26077', '26078', '1', '2018-04-09 11:03:31', '1', '');

-- ----------------------------
-- Table structure for `hci_paymentinvoice`
-- ----------------------------
DROP TABLE IF EXISTS `hci_paymentinvoice`;
CREATE TABLE `hci_paymentinvoice` (
  `payinv_id` int(11) NOT NULL AUTO_INCREMENT,
  `payinv_payment` int(11) NOT NULL,
  `payinv_invoice` int(11) NOT NULL,
  `payinv_amount` decimal(10,2) NOT NULL,
  `payinv_usedbalance` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`payinv_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of hci_paymentinvoice
-- ----------------------------
INSERT INTO `hci_paymentinvoice` VALUES ('1', '6', '7', '250000.00', '0.00');
INSERT INTO `hci_paymentinvoice` VALUES ('2', '6', '8', '23000.00', '30000.00');
INSERT INTO `hci_paymentinvoice` VALUES ('3', '9', '11', '200000.00', '0.00');
INSERT INTO `hci_paymentinvoice` VALUES ('4', '10', '11', '10000.00', '40000.00');
INSERT INTO `hci_paymentinvoice` VALUES ('5', '10', '12', '0.00', '10000.00');
INSERT INTO `hci_paymentinvoice` VALUES ('6', '11', '12', '43000.00', '0.00');

-- ----------------------------
-- Table structure for `hci_paymentmethod`
-- ----------------------------
DROP TABLE IF EXISTS `hci_paymentmethod`;
CREATE TABLE `hci_paymentmethod` (
  `paymeth_id` int(11) NOT NULL AUTO_INCREMENT,
  `paymeth_method` varchar(255) NOT NULL,
  `paymeth_amount` decimal(10,2) NOT NULL,
  `paymeth_ref` text,
  `paymeth_refdate` date DEFAULT NULL,
  `paymeth_payment` int(11) DEFAULT NULL,
  `paymeth_bank` int(11) DEFAULT NULL,
  `paymeth_bankaccount` text,
  PRIMARY KEY (`paymeth_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of hci_paymentmethod
-- ----------------------------
INSERT INTO `hci_paymentmethod` VALUES ('1', 'cash', '50000.00', null, null, '1', null, null);
INSERT INTO `hci_paymentmethod` VALUES ('2', 'cash', '10000.00', null, null, '2', null, null);
INSERT INTO `hci_paymentmethod` VALUES ('3', 'cash', '20000.00', null, null, '3', null, null);
INSERT INTO `hci_paymentmethod` VALUES ('4', 'cash', '40000.00', null, null, '4', null, null);
INSERT INTO `hci_paymentmethod` VALUES ('5', 'cash', '30000.00', null, null, '5', null, null);
INSERT INTO `hci_paymentmethod` VALUES ('6', 'cash', '273000.00', null, null, '6', null, null);
INSERT INTO `hci_paymentmethod` VALUES ('7', 'cash', '50000.00', null, null, '7', null, null);
INSERT INTO `hci_paymentmethod` VALUES ('8', 'cash', '50000.00', null, null, '8', null, null);
INSERT INTO `hci_paymentmethod` VALUES ('9', 'cash', '200000.00', null, null, '9', null, null);
INSERT INTO `hci_paymentmethod` VALUES ('10', 'cash', '10000.00', null, null, '10', null, null);
INSERT INTO `hci_paymentmethod` VALUES ('11', 'cash', '100000.00', null, null, '11', null, null);

-- ----------------------------
-- Table structure for `hci_paymentplan`
-- ----------------------------
DROP TABLE IF EXISTS `hci_paymentplan`;
CREATE TABLE `hci_paymentplan` (
  `plan_id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plan_fee` int(11) DEFAULT NULL,
  `plan_type` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plan_status` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plan_default` tinyint(5) DEFAULT NULL,
  PRIMARY KEY (`plan_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of hci_paymentplan
-- ----------------------------
INSERT INTO `hci_paymentplan` VALUES ('1', 'Total Admission', '1', 'TA', 'A', null);
INSERT INTO `hci_paymentplan` VALUES ('2', 'Total Term ', '2', 'TA', 'A', null);
INSERT INTO `hci_paymentplan` VALUES ('3', 'Two Terms Together Term', '2', 'TO', 'A', null);
INSERT INTO `hci_paymentplan` VALUES ('4', 'Term Wise Term', '2', 'T', 'A', null);
INSERT INTO `hci_paymentplan` VALUES ('5', 'Service Total', '3', 'TA', 'A', null);
INSERT INTO `hci_paymentplan` VALUES ('6', 'Service  Term wise', '3', 'T', 'A', null);
INSERT INTO `hci_paymentplan` VALUES ('7', 'Transport Term', '4', 'T', 'A', null);

-- ----------------------------
-- Table structure for `hci_popularmethod`
-- ----------------------------
DROP TABLE IF EXISTS `hci_popularmethod`;
CREATE TABLE `hci_popularmethod` (
  `pm_id` int(11) NOT NULL AUTO_INCREMENT,
  `pm_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`pm_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of hci_popularmethod
-- ----------------------------
INSERT INTO `hci_popularmethod` VALUES ('1', 'News Paper');
INSERT INTO `hci_popularmethod` VALUES ('2', 'Word of Mouth');
INSERT INTO `hci_popularmethod` VALUES ('3', 'Web ');

-- ----------------------------
-- Table structure for `hci_preadmission`
-- ----------------------------
DROP TABLE IF EXISTS `hci_preadmission`;
CREATE TABLE `hci_preadmission` (
  `pre_schl1` varchar(111) NOT NULL,
  `pre_schl2` varchar(100) NOT NULL,
  `enrlno1` varchar(255) NOT NULL,
  `enrlno2` varchar(255) NOT NULL,
  `yearfrom1` varchar(255) NOT NULL,
  `yearfrom2` varchar(255) NOT NULL,
  `yearupto1` varchar(255) NOT NULL,
  `yearupto2` varchar(255) NOT NULL,
  `reason1` text NOT NULL,
  `reason2` text NOT NULL,
  `es_preadmissionid` int(11) NOT NULL AUTO_INCREMENT,
  `es_enquiryid` int(11) NOT NULL,
  `pre_serialno` varchar(255) NOT NULL,
  `pre_number` varchar(60) NOT NULL,
  `pre_student_username` varchar(255) NOT NULL,
  `pre_student_lastname` varchar(255) NOT NULL,
  `pre_student_password` varchar(255) NOT NULL,
  `pre_dateofbirth` date NOT NULL,
  `pre_grade` int(11) NOT NULL,
  `pre_intake` int(2) NOT NULL,
  `pre_fathername` varchar(255) NOT NULL,
  `pre_fatheraddress1` varchar(255) NOT NULL,
  `pre_fatheraddress2` varchar(255) NOT NULL,
  `pre_fatheraddress3` varchar(255) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `pre_mothername` varchar(255) NOT NULL,
  `pre_fathersoccupation` varchar(255) NOT NULL,
  `pre_fathersoccupation2` varchar(225) NOT NULL,
  `pre_motheroccupation` varchar(255) NOT NULL,
  `pre_motheroccupation2` varchar(255) NOT NULL,
  `pre_contactname1` varchar(255) NOT NULL,
  `pre_contactno1` varchar(255) NOT NULL,
  `pre_contactno2` varchar(255) NOT NULL,
  `pre_contactname2` varchar(255) NOT NULL,
  `pre_motheraddress1` varchar(255) NOT NULL,
  `pre_motheraddress2` varchar(255) NOT NULL,
  `pre_motheraddress3` varchar(255) NOT NULL,
  `pre_personname` varchar(255) NOT NULL,
  `pre_personaddress1` varchar(255) NOT NULL,
  `pre_personaddress2` varchar(255) NOT NULL,
  `pre_personaddress3` varchar(255) NOT NULL,
  `pre_city1` varchar(255) NOT NULL,
  `pre_state1` varchar(255) NOT NULL,
  `pre_country1` varchar(255) NOT NULL,
  `aadharno` varchar(100) NOT NULL,
  `board` varchar(100) NOT NULL,
  `edugap` varchar(255) NOT NULL,
  `pre_phno1` varchar(255) NOT NULL,
  `pre_mobile1` varchar(255) NOT NULL,
  `pre_prev_acadamicname` varchar(255) NOT NULL,
  `pre_prev_class` varchar(255) NOT NULL,
  `pre_prev_university` varchar(255) NOT NULL,
  `pre_othername` varchar(255) NOT NULL,
  `pre_nationality` varchar(255) NOT NULL,
  `pre_religion` varchar(255) NOT NULL,
  `pre_foreigncitizen` varchar(255) NOT NULL,
  `pre_foreignnationality` varchar(255) NOT NULL,
  `pre_foreignpassportno` varchar(255) NOT NULL,
  `pre_placeDOI` date NOT NULL,
  `pre_foreignfirstlanguage` varchar(255) NOT NULL,
  `pre_foreignspokenhome` varchar(255) NOT NULL,
  `pre_personhomenumber` varchar(50) NOT NULL,
  `pre_prev_percentage` varchar(255) NOT NULL,
  `pre_persomobile` varchar(20) NOT NULL,
  `eq_personemail` varchar(255) NOT NULL,
  `pre_emergancyname` varchar(255) NOT NULL,
  `pre_emergancyaddress1` varchar(255) NOT NULL,
  `pre_emergancyaddress2` varchar(255) NOT NULL,
  `pre_emergancycity` varchar(255) NOT NULL,
  `pre_emergancyhome` varchar(255) NOT NULL,
  `pre_emergancymobile` varchar(255) NOT NULL,
  `pre_prev_address1` varchar(255) NOT NULL,
  `pre_prev_address2` varchar(255) NOT NULL,
  `pre_prev_city` int(11) NOT NULL,
  `pre_language_Instruction` int(11) NOT NULL,
  `pre_withdraw` int(2) NOT NULL,
  `pre_withdrawreason` varchar(255) NOT NULL,
  `pre_prev_activities` varchar(255) NOT NULL,
  `pre_englishexperience` int(2) NOT NULL,
  `pre_situation` varchar(255) NOT NULL,
  `pre_long` varchar(255) NOT NULL,
  `pre_specialeduprogramme` int(2) NOT NULL,
  `pre_specialeduprogrammereason` varchar(255) NOT NULL,
  `pre_testedstuden` int(2) NOT NULL,
  `pre_testedstudentreason` varchar(255) NOT NULL,
  `pre_medicaldisability` int(2) NOT NULL,
  `pre_fatheraffliliation` varchar(255) NOT NULL,
  `pre_businessaddress` varchar(255) NOT NULL,
  `pre_businesstel` varchar(20) NOT NULL,
  `pre_motheraffliation` varchar(255) NOT NULL,
  `pre_motherbusinessaddress` varchar(255) NOT NULL,
  `pre_motherbusinesstel` varchar(20) NOT NULL,
  `pre_expectedenrollment` varchar(255) NOT NULL,
  `pre_lengthstay` varchar(255) NOT NULL,
  `pre_prev_tcno` varchar(255) NOT NULL,
  `pre_current_acadamicname` varchar(255) NOT NULL,
  `pre_current_class1` varchar(255) NOT NULL,
  `pre_current_percentage1` varchar(255) NOT NULL,
  `pre_current_result1` varchar(255) NOT NULL,
  `pre_current_class2` varchar(255) NOT NULL,
  `pre_current_percentage2` varchar(255) NOT NULL,
  `pre_current_result2` varchar(255) NOT NULL,
  `pre_current_class3` varchar(255) NOT NULL,
  `pre_current_percentage3` varchar(255) NOT NULL,
  `pre_current_result3` varchar(255) NOT NULL,
  `pre_physical_details` varchar(255) NOT NULL,
  `pre_height` varchar(255) NOT NULL,
  `pre_weight` varchar(255) NOT NULL,
  `pre_alerge` varchar(255) NOT NULL,
  `pre_physical_status` longtext NOT NULL,
  `pre_special_care` varchar(255) NOT NULL,
  `pre_class` varchar(255) NOT NULL,
  `pre_sec` varchar(255) NOT NULL,
  `pre_name` varchar(255) NOT NULL,
  `pre_age` int(11) NOT NULL,
  `pre_address` varchar(255) NOT NULL,
  `pre_city` varchar(255) NOT NULL,
  `pre_state` varchar(255) NOT NULL,
  `pre_country` varchar(255) NOT NULL,
  `pre_phno` varchar(255) NOT NULL,
  `pre_mobile` varchar(255) NOT NULL,
  `pre_resno` varchar(255) NOT NULL,
  `pre_resno1` varchar(255) NOT NULL,
  `pre_image` varchar(255) NOT NULL,
  `test1` varchar(255) NOT NULL,
  `test2` varchar(255) NOT NULL,
  `test3` varchar(255) NOT NULL,
  `pre_pincode1` varchar(255) NOT NULL,
  `pre_pincode` varchar(255) NOT NULL,
  `pre_emailid` varchar(255) NOT NULL,
  `pre_emailid2` varchar(100) NOT NULL,
  `pre_fromdate` date NOT NULL,
  `pre_todate` date NOT NULL,
  `status` enum('pass','fail','resultawaiting','inactive') NOT NULL,
  `pre_status` enum('inactive','active') NOT NULL DEFAULT 'active',
  `es_user_theme` varchar(255) NOT NULL,
  `pre_hobbies` varchar(255) NOT NULL,
  `pre_blood_group` varchar(255) NOT NULL,
  `pre_gender` int(2) NOT NULL,
  `admission_status` varchar(255) NOT NULL,
  `caste_id` int(11) NOT NULL,
  `tr_place_id` int(11) NOT NULL,
  `ann_income` varchar(255) NOT NULL,
  `admission_date` date NOT NULL,
  `admission_date1` date NOT NULL,
  `document_deposited` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `fee_concession` varchar(255) NOT NULL,
  `pre_dateofbirth1` date NOT NULL,
  `pre_dateofbirth2` date NOT NULL,
  `pre_dateofbirth3` date NOT NULL,
  `es_home` varchar(255) NOT NULL,
  `es_econbackward` varchar(255) NOT NULL,
  `es_econbackward1` varchar(200) NOT NULL,
  `es_econbackward2` varchar(255) NOT NULL,
  `es_econbackward3` varchar(200) NOT NULL,
  `es_econbackward4` varchar(255) NOT NULL,
  `es_econbackward5` varchar(255) NOT NULL,
  `admission_id` varchar(100) NOT NULL,
  `school_id` int(11) NOT NULL,
  `pre_lastname` varchar(230) NOT NULL,
  `pre_placeofbirth` varchar(230) NOT NULL,
  `pre_contactno` varchar(230) NOT NULL,
  `pre_contactno3` varchar(230) NOT NULL,
  `pre_resno2` varchar(230) NOT NULL,
  `pre_family1` varchar(111) NOT NULL,
  `pre_family2` varchar(100) NOT NULL,
  `pre_family3` varchar(100) NOT NULL,
  `pre_family4` varchar(100) NOT NULL,
  `pre_family5` varchar(111) NOT NULL,
  `age1` varchar(111) NOT NULL,
  `age2` varchar(111) NOT NULL,
  `age3` varchar(111) NOT NULL,
  `age4` varchar(111) NOT NULL,
  `age5` varchar(111) NOT NULL,
  `age6` varchar(111) NOT NULL,
  `relation1` text NOT NULL,
  `relation2` text NOT NULL,
  `relation3` text NOT NULL,
  `relation4` text NOT NULL,
  `relation5` text NOT NULL,
  `relation6` text NOT NULL,
  `eduation1` text NOT NULL,
  `eduation2` text NOT NULL,
  `eduation3` text NOT NULL,
  `eduation4` text NOT NULL,
  `eduation5` text NOT NULL,
  `eduation6` text NOT NULL,
  `occupation1` text NOT NULL,
  `occupation2` text NOT NULL,
  `occupation3` text NOT NULL,
  `occupation4` text NOT NULL,
  `occupation5` text NOT NULL,
  `occupation6` text NOT NULL,
  `pre_family6` varchar(111) NOT NULL,
  `reason` varchar(230) NOT NULL,
  `pre_transportbybus` int(2) NOT NULL,
  `pre_payplan` int(2) DEFAULT NULL,
  `pre_feestructure` int(11) DEFAULT NULL,
  `pre_admissionno` text,
  PRIMARY KEY (`es_preadmissionid`),
  KEY `es_enquiryid` (`es_enquiryid`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of hci_preadmission
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_preadmission1`
-- ----------------------------
DROP TABLE IF EXISTS `hci_preadmission1`;
CREATE TABLE `hci_preadmission1` (
  `pre_schl1` varchar(111) NOT NULL,
  `pre_schl2` varchar(100) NOT NULL,
  `enrlno1` varchar(255) NOT NULL,
  `enrlno2` varchar(255) NOT NULL,
  `yearfrom1` varchar(255) NOT NULL,
  `yearfrom2` varchar(255) NOT NULL,
  `yearupto1` varchar(255) NOT NULL,
  `yearupto2` varchar(255) NOT NULL,
  `reason1` text NOT NULL,
  `reason2` text NOT NULL,
  `es_preadmissionid` int(11) NOT NULL AUTO_INCREMENT,
  `pre_serialno` varchar(255) NOT NULL,
  `pre_number` varchar(60) NOT NULL,
  `pre_student_username` varchar(255) NOT NULL,
  `pre_student_lastname` varchar(255) NOT NULL,
  `pre_student_password` varchar(255) NOT NULL,
  `pre_dateofbirth` date NOT NULL,
  `pre_grade` int(11) NOT NULL,
  `pre_intake` int(2) NOT NULL,
  `pre_fathername` varchar(255) NOT NULL,
  `pre_fatheraddress1` varchar(255) NOT NULL,
  `pre_fatheraddress2` varchar(255) NOT NULL,
  `pre_fatheraddress3` varchar(255) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `pre_mothername` varchar(255) NOT NULL,
  `pre_fathersoccupation` varchar(255) NOT NULL,
  `pre_fathersoccupation2` varchar(225) NOT NULL,
  `pre_motheroccupation` varchar(255) NOT NULL,
  `pre_motheroccupation2` varchar(255) NOT NULL,
  `pre_contactname1` varchar(255) NOT NULL,
  `pre_contactno1` varchar(255) NOT NULL,
  `pre_contactno2` varchar(255) NOT NULL,
  `pre_contactname2` varchar(255) NOT NULL,
  `pre_motheraddress1` varchar(255) NOT NULL,
  `pre_motheraddress2` varchar(255) NOT NULL,
  `pre_motheraddress3` varchar(255) NOT NULL,
  `pre_personname` varchar(255) NOT NULL,
  `pre_personaddress1` varchar(255) NOT NULL,
  `pre_personaddress2` varchar(255) NOT NULL,
  `pre_personaddress3` varchar(255) NOT NULL,
  `pre_city1` varchar(255) NOT NULL,
  `pre_state1` varchar(255) NOT NULL,
  `pre_country1` varchar(255) NOT NULL,
  `aadharno` varchar(100) NOT NULL,
  `board` varchar(100) NOT NULL,
  `edugap` varchar(255) NOT NULL,
  `pre_phno1` varchar(255) NOT NULL,
  `pre_mobile1` varchar(255) NOT NULL,
  `pre_prev_acadamicname` varchar(255) NOT NULL,
  `pre_prev_class` varchar(255) NOT NULL,
  `pre_prev_university` varchar(255) NOT NULL,
  `pre_othername` varchar(255) NOT NULL,
  `pre_nationality` varchar(255) NOT NULL,
  `pre_religion` varchar(255) NOT NULL,
  `pre_foreigncitizen` varchar(255) NOT NULL,
  `pre_foreignnationality` varchar(255) NOT NULL,
  `pre_foreignpassportno` varchar(255) NOT NULL,
  `pre_placeDOI` date NOT NULL,
  `pre_foreignfirstlanguage` varchar(255) NOT NULL,
  `pre_foreignspokenhome` varchar(255) NOT NULL,
  `pre_personhomenumber` varchar(50) NOT NULL,
  `pre_prev_percentage` varchar(255) NOT NULL,
  `pre_persomobile` int(11) NOT NULL,
  `eq_personemail` int(11) NOT NULL,
  `pre_emergancyname` int(11) NOT NULL,
  `pre_emergancyaddress1` varchar(255) NOT NULL,
  `pre_emergancyaddress2` varchar(255) NOT NULL,
  `pre_emergancycity` varchar(255) NOT NULL,
  `pre_emergancyhome` varchar(255) NOT NULL,
  `pre_emergancymobile` varchar(255) NOT NULL,
  `pre_prev_address1` varchar(255) NOT NULL,
  `pre_prev_address2` varchar(255) NOT NULL,
  `pre_prev_city` int(11) NOT NULL,
  `pre_language_Instruction` int(11) NOT NULL,
  `pre_withdraw` int(2) NOT NULL,
  `pre_withdrawreason` varchar(255) NOT NULL,
  `pre_prev_activities` varchar(255) NOT NULL,
  `pre_englishexperience` int(2) NOT NULL,
  `pre_situation` varchar(255) NOT NULL,
  `pre_long` varchar(255) NOT NULL,
  `pre_specialeduprogramme` int(2) NOT NULL,
  `pre_specialeduprogrammereason` varchar(255) NOT NULL,
  `pre_testedstuden` int(2) NOT NULL,
  `pre_testedstudentreason` varchar(255) NOT NULL,
  `pre_medicaldisability` int(2) NOT NULL,
  `pre_fatheraffliliation` varchar(255) NOT NULL,
  `pre_businessaddress` varchar(255) NOT NULL,
  `pre_businesstel` varchar(20) NOT NULL,
  `pre_motheraffliation` varchar(255) NOT NULL,
  `pre_motherbusinessaddress` varchar(255) NOT NULL,
  `pre_motherbusinesstel` varchar(20) NOT NULL,
  `pre_expectedenrollment` varchar(255) NOT NULL,
  `pre_lengthstay` varchar(255) NOT NULL,
  `pre_prev_tcno` varchar(255) NOT NULL,
  `pre_current_acadamicname` varchar(255) NOT NULL,
  `pre_current_class1` varchar(255) NOT NULL,
  `pre_current_percentage1` varchar(255) NOT NULL,
  `pre_current_result1` varchar(255) NOT NULL,
  `pre_current_class2` varchar(255) NOT NULL,
  `pre_current_percentage2` varchar(255) NOT NULL,
  `pre_current_result2` varchar(255) NOT NULL,
  `pre_current_class3` varchar(255) NOT NULL,
  `pre_current_percentage3` varchar(255) NOT NULL,
  `pre_current_result3` varchar(255) NOT NULL,
  `pre_physical_details` varchar(255) NOT NULL,
  `pre_height` varchar(255) NOT NULL,
  `pre_weight` varchar(255) NOT NULL,
  `pre_alerge` varchar(255) NOT NULL,
  `pre_physical_status` longtext NOT NULL,
  `pre_special_care` varchar(255) NOT NULL,
  `pre_class` varchar(255) NOT NULL,
  `pre_sec` varchar(255) NOT NULL,
  `pre_name` varchar(255) NOT NULL,
  `pre_age` int(11) NOT NULL,
  `pre_address` varchar(255) NOT NULL,
  `pre_city` varchar(255) NOT NULL,
  `pre_state` varchar(255) NOT NULL,
  `pre_country` varchar(255) NOT NULL,
  `pre_phno` varchar(255) NOT NULL,
  `pre_mobile` varchar(255) NOT NULL,
  `pre_resno` varchar(255) NOT NULL,
  `pre_resno1` varchar(255) NOT NULL,
  `pre_image` varchar(255) NOT NULL,
  `test1` varchar(255) NOT NULL,
  `test2` varchar(255) NOT NULL,
  `test3` varchar(255) NOT NULL,
  `pre_pincode1` varchar(255) NOT NULL,
  `pre_pincode` varchar(255) NOT NULL,
  `pre_emailid` varchar(255) NOT NULL,
  `pre_emailid2` varchar(100) NOT NULL,
  `pre_fromdate` date NOT NULL,
  `pre_todate` date NOT NULL,
  `status` enum('pass','fail','resultawaiting','inactive') NOT NULL,
  `pre_status` enum('inactive','active') NOT NULL DEFAULT 'active',
  `es_user_theme` varchar(255) NOT NULL,
  `pre_hobbies` varchar(255) NOT NULL,
  `pre_blood_group` varchar(255) NOT NULL,
  `pre_gender` int(2) NOT NULL,
  `admission_status` varchar(255) NOT NULL,
  `caste_id` int(11) NOT NULL,
  `tr_place_id` int(11) NOT NULL,
  `ann_income` varchar(255) NOT NULL,
  `admission_date` date NOT NULL,
  `admission_date1` date NOT NULL,
  `document_deposited` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `fee_concession` varchar(255) NOT NULL,
  `pre_dateofbirth1` date NOT NULL,
  `pre_dateofbirth2` date NOT NULL,
  `pre_dateofbirth3` date NOT NULL,
  `es_home` varchar(255) NOT NULL,
  `es_econbackward` varchar(255) NOT NULL,
  `es_econbackward1` varchar(200) NOT NULL,
  `es_econbackward2` varchar(255) NOT NULL,
  `es_econbackward3` varchar(200) NOT NULL,
  `es_econbackward4` varchar(255) NOT NULL,
  `es_econbackward5` varchar(255) NOT NULL,
  `admission_id` varchar(100) NOT NULL,
  `school_id` int(11) NOT NULL,
  `pre_lastname` varchar(230) NOT NULL,
  `pre_placeofbirth` varchar(230) NOT NULL,
  `pre_contactno` varchar(230) NOT NULL,
  `pre_contactno3` varchar(230) NOT NULL,
  `pre_resno2` varchar(230) NOT NULL,
  `pre_family1` varchar(111) NOT NULL,
  `pre_family2` varchar(100) NOT NULL,
  `pre_family3` varchar(100) NOT NULL,
  `pre_family4` varchar(100) NOT NULL,
  `pre_family5` varchar(111) NOT NULL,
  `age1` varchar(111) NOT NULL,
  `age2` varchar(111) NOT NULL,
  `age3` varchar(111) NOT NULL,
  `age4` varchar(111) NOT NULL,
  `age5` varchar(111) NOT NULL,
  `age6` varchar(111) NOT NULL,
  `relation1` text NOT NULL,
  `relation2` text NOT NULL,
  `relation3` text NOT NULL,
  `relation4` text NOT NULL,
  `relation5` text NOT NULL,
  `relation6` text NOT NULL,
  `eduation1` text NOT NULL,
  `eduation2` text NOT NULL,
  `eduation3` text NOT NULL,
  `eduation4` text NOT NULL,
  `eduation5` text NOT NULL,
  `eduation6` text NOT NULL,
  `occupation1` text NOT NULL,
  `occupation2` text NOT NULL,
  `occupation3` text NOT NULL,
  `occupation4` text NOT NULL,
  `occupation5` text NOT NULL,
  `occupation6` text NOT NULL,
  `pre_family6` varchar(111) NOT NULL,
  `reason` varchar(230) NOT NULL,
  `pre_transportbybus` int(2) NOT NULL,
  PRIMARY KEY (`es_preadmissionid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of hci_preadmission1
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_preadmissionold`
-- ----------------------------
DROP TABLE IF EXISTS `hci_preadmissionold`;
CREATE TABLE `hci_preadmissionold` (
  `pre_schl1` varchar(111) NOT NULL,
  `pre_schl2` varchar(100) NOT NULL,
  `enrlno1` varchar(255) NOT NULL,
  `enrlno2` varchar(255) NOT NULL,
  `yearfrom1` varchar(255) NOT NULL,
  `yearfrom2` varchar(255) NOT NULL,
  `yearupto1` varchar(255) NOT NULL,
  `yearupto2` varchar(255) NOT NULL,
  `reason1` text NOT NULL,
  `reason2` text NOT NULL,
  `es_preadmissionid` int(11) NOT NULL AUTO_INCREMENT,
  `pre_serialno` varchar(255) NOT NULL,
  `pre_number` varchar(60) NOT NULL,
  `pre_student_username` varchar(255) NOT NULL,
  `pre_student_password` varchar(255) NOT NULL,
  `pre_dateofbirth` date NOT NULL,
  `pre_fathername` varchar(255) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `pre_mothername` varchar(255) NOT NULL,
  `pre_fathersoccupation` varchar(255) NOT NULL,
  `pre_fathersoccupation2` varchar(225) NOT NULL,
  `pre_motheroccupation` varchar(255) NOT NULL,
  `pre_motheroccupation2` varchar(255) NOT NULL,
  `pre_contactname1` varchar(255) NOT NULL,
  `pre_contactno1` varchar(255) NOT NULL,
  `pre_contactno2` varchar(255) NOT NULL,
  `pre_contactname2` varchar(255) NOT NULL,
  `pre_address1` varchar(255) NOT NULL,
  `pre_city1` varchar(255) NOT NULL,
  `pre_state1` varchar(255) NOT NULL,
  `pre_country1` varchar(255) NOT NULL,
  `aadharno` varchar(100) NOT NULL,
  `board` varchar(100) NOT NULL,
  `edugap` varchar(255) NOT NULL,
  `pre_phno1` varchar(255) NOT NULL,
  `pre_mobile1` varchar(255) NOT NULL,
  `pre_prev_acadamicname` varchar(255) NOT NULL,
  `pre_prev_class` varchar(255) NOT NULL,
  `pre_prev_university` varchar(255) NOT NULL,
  `pre_prev_percentage` varchar(255) NOT NULL,
  `pre_prev_tcno` varchar(255) NOT NULL,
  `pre_current_acadamicname` varchar(255) NOT NULL,
  `pre_current_class1` varchar(255) NOT NULL,
  `pre_current_percentage1` varchar(255) NOT NULL,
  `pre_current_result1` varchar(255) NOT NULL,
  `pre_current_class2` varchar(255) NOT NULL,
  `pre_current_percentage2` varchar(255) NOT NULL,
  `pre_current_result2` varchar(255) NOT NULL,
  `pre_current_class3` varchar(255) NOT NULL,
  `pre_current_percentage3` varchar(255) NOT NULL,
  `pre_current_result3` varchar(255) NOT NULL,
  `pre_physical_details` varchar(255) NOT NULL,
  `pre_height` varchar(255) NOT NULL,
  `pre_weight` varchar(255) NOT NULL,
  `pre_alerge` varchar(255) NOT NULL,
  `pre_physical_status` varchar(255) NOT NULL,
  `pre_special_care` varchar(255) NOT NULL,
  `pre_class` varchar(255) NOT NULL,
  `pre_sec` varchar(255) NOT NULL,
  `pre_name` varchar(255) NOT NULL,
  `pre_age` int(11) NOT NULL,
  `pre_address` varchar(255) NOT NULL,
  `pre_city` varchar(255) NOT NULL,
  `pre_state` varchar(255) NOT NULL,
  `pre_country` varchar(255) NOT NULL,
  `pre_phno` varchar(255) NOT NULL,
  `pre_mobile` varchar(255) NOT NULL,
  `pre_resno` varchar(255) NOT NULL,
  `pre_resno1` varchar(255) NOT NULL,
  `pre_image` varchar(255) NOT NULL,
  `test1` varchar(255) NOT NULL,
  `test2` varchar(255) NOT NULL,
  `test3` varchar(255) NOT NULL,
  `pre_pincode1` varchar(255) NOT NULL,
  `pre_pincode` varchar(255) NOT NULL,
  `pre_emailid` varchar(255) NOT NULL,
  `pre_emailid2` varchar(100) NOT NULL,
  `pre_fromdate` date NOT NULL,
  `pre_todate` date NOT NULL,
  `status` enum('pass','fail','resultawaiting','inactive') NOT NULL,
  `pre_status` enum('inactive','active') NOT NULL DEFAULT 'active',
  `es_user_theme` varchar(255) NOT NULL,
  `pre_hobbies` varchar(255) NOT NULL,
  `pre_blood_group` varchar(255) NOT NULL,
  `pre_gender` enum('male','female') NOT NULL,
  `admission_status` varchar(255) NOT NULL,
  `caste_id` int(11) NOT NULL,
  `tr_place_id` int(11) NOT NULL,
  `ann_income` varchar(255) NOT NULL,
  `admission_date` date NOT NULL,
  `admission_date1` date NOT NULL,
  `document_deposited` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `fee_concession` varchar(255) NOT NULL,
  `pre_dateofbirth1` date NOT NULL,
  `pre_dateofbirth2` date NOT NULL,
  `pre_dateofbirth3` date NOT NULL,
  `es_home` varchar(255) NOT NULL,
  `es_econbackward` varchar(255) NOT NULL,
  `es_econbackward1` varchar(200) NOT NULL,
  `es_econbackward2` varchar(255) NOT NULL,
  `es_econbackward3` varchar(200) NOT NULL,
  `es_econbackward4` varchar(255) NOT NULL,
  `es_econbackward5` varchar(255) NOT NULL,
  `admission_id` varchar(100) NOT NULL,
  `school_id` int(11) NOT NULL,
  `pre_lastname` varchar(230) NOT NULL,
  `pre_placeofbirth` varchar(230) NOT NULL,
  `pre_contactno` varchar(230) NOT NULL,
  `pre_contactno3` varchar(230) NOT NULL,
  `pre_resno2` varchar(230) NOT NULL,
  `pre_family1` varchar(111) NOT NULL,
  `pre_family2` varchar(100) NOT NULL,
  `pre_family3` varchar(100) NOT NULL,
  `pre_family4` varchar(100) NOT NULL,
  `pre_family5` varchar(111) NOT NULL,
  `age1` varchar(111) NOT NULL,
  `age2` varchar(111) NOT NULL,
  `age3` varchar(111) NOT NULL,
  `age4` varchar(111) NOT NULL,
  `age5` varchar(111) NOT NULL,
  `age6` varchar(111) NOT NULL,
  `relation1` text NOT NULL,
  `relation2` text NOT NULL,
  `relation3` text NOT NULL,
  `relation4` text NOT NULL,
  `relation5` text NOT NULL,
  `relation6` text NOT NULL,
  `eduation1` text NOT NULL,
  `eduation2` text NOT NULL,
  `eduation3` text NOT NULL,
  `eduation4` text NOT NULL,
  `eduation5` text NOT NULL,
  `eduation6` text NOT NULL,
  `occupation1` text NOT NULL,
  `occupation2` text NOT NULL,
  `occupation3` text NOT NULL,
  `occupation4` text NOT NULL,
  `occupation5` text NOT NULL,
  `occupation6` text NOT NULL,
  `pre_family6` varchar(111) NOT NULL,
  `reason` varchar(230) NOT NULL,
  PRIMARY KEY (`es_preadmissionid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of hci_preadmissionold
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_preadmission_details`
-- ----------------------------
DROP TABLE IF EXISTS `hci_preadmission_details`;
CREATE TABLE `hci_preadmission_details` (
  `es_preadmission_detailsid` int(11) NOT NULL AUTO_INCREMENT,
  `es_preadmissionid` int(11) NOT NULL,
  `pre_class` varchar(255) NOT NULL,
  `pre_fromdate` date NOT NULL,
  `pre_todate` date NOT NULL,
  `status` enum('pass','fail','resultawaiting','inactive') NOT NULL,
  `admission_status` enum('newadmission','promoted') NOT NULL,
  `scat_id` int(11) NOT NULL,
  PRIMARY KEY (`es_preadmission_detailsid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of hci_preadmission_details
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_receipt`
-- ----------------------------
DROP TABLE IF EXISTS `hci_receipt`;
CREATE TABLE `hci_receipt` (
  `rec_id` int(11) NOT NULL AUTO_INCREMENT,
  `rec_index` text,
  `rec_payid` int(11) DEFAULT NULL,
  `rec_date` date DEFAULT NULL,
  `rec_amount` decimal(10,2) DEFAULT NULL,
  `rec_usedbalance` decimal(10,2) DEFAULT NULL,
  `rec_outstanding` decimal(10,2) DEFAULT NULL,
  `rec_cohvbalance` decimal(10,2) DEFAULT NULL,
  `rec_custype` varchar(20) DEFAULT NULL,
  `rec_customer` int(11) DEFAULT NULL,
  `rec_type` varchar(5) DEFAULT NULL,
  `rec_cusindex` text,
  `rec_cusname` varchar(255) DEFAULT NULL,
  `rec_createduser` int(11) DEFAULT NULL,
  `rec_createdusername` varchar(75) DEFAULT NULL,
  `rec_createddate` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `rec_description` varchar(255) DEFAULT NULL,
  `rec_branch` int(11) DEFAULT NULL,
  `rec_compname` text,
  `rec_compaddline1` text,
  `rec_compaddline2` text,
  `rec_compaddline3` text,
  `rec_remarks` text,
  PRIMARY KEY (`rec_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of hci_receipt
-- ----------------------------
INSERT INTO `hci_receipt` VALUES ('1', 'RECMB1800019', '1', '2018-04-06', '50000.00', null, '0.00', '0.00', 'TEMPSTU', '1', 'ADV', 'TEMPSTUMB0010', 'tyytytyty uyuyuyu', '1', 'admin', '2018-04-06 06:17:31', 'ghfhg', '1', 'Horizon College of Education -  - Malabe', 'xx', 'xxxxxxxxxxxx', 'xxxxxxxx', '');
INSERT INTO `hci_receipt` VALUES ('2', 'RECMB1800020', '2', '2018-04-06', '10000.00', null, '0.00', '0.00', 'TEMPSTU', '2', 'ADV', 'TEMPSTUMB0011', 'hghgfhgfh ghfghfgh', '1', 'admin', '2018-04-06 06:17:52', 'fgdfgdfgdfg', '1', 'Horizon College of Education -  - Malabe', 'xx', 'xxxxxxxxxxxx', 'xxxxxxxx', '');
INSERT INTO `hci_receipt` VALUES ('3', 'RECMB1800021', '3', '2018-04-06', '20000.00', null, '0.00', '0.00', 'TEMPSTU', '3', 'ADV', 'TEMPSTUMB0012', 'gfhfghgfh gdfgfg', '1', 'admin', '2018-04-06 06:18:17', 'ghhgfhg', '1', 'Horizon College of Education -  - Malabe', 'xx', 'xxxxxxxxxxxx', 'xxxxxxxx', '');
INSERT INTO `hci_receipt` VALUES ('4', 'RECMB1800022', '4', '2018-04-06', '40000.00', null, '0.00', '0.00', 'TEMPSTU', '5', 'ADV', 'TEMPSTUMB0014', 'fgfdgdfg fdgdgdfg', '1', 'admin', '2018-04-06 06:26:07', 'uiuiuyi', '1', 'Horizon College of Education -  - Malabe', 'xx', 'xxxxxxxxxxxx', 'xxxxxxxx', '');
INSERT INTO `hci_receipt` VALUES ('5', 'RECMB1800023', '5', '2018-04-06', '30000.00', null, '0.00', '0.00', 'TEMPSTU', '6', 'ADV', 'TEMPSTUMB0015', 'fgfg fgdfgf', '1', 'admin', '2018-04-06 06:26:29', 'jkhkhjkhjk', '1', 'Horizon College of Education -  - Malabe', 'xx', 'xxxxxxxxxxxx', 'xxxxxxxx', '');
INSERT INTO `hci_receipt` VALUES ('6', 'RECMB1800024', '6', '2018-04-09', '273000.00', '280000.00', '303000.00', '30000.00', 'STUDENT', '4', 'INV', 'MB/70/09/PG-4/4', 'fgdfgf fgfg', '1', 'admin', '2018-04-09 09:57:09', 'hghfgh', '1', 'Horizon College of Education -  - Malabe', 'xx', 'xxxxxxxxxxxx', 'xxxxxxxx', '');
INSERT INTO `hci_receipt` VALUES ('7', 'RECMB1800025', '7', '2018-04-09', '50000.00', null, '0.00', '0.00', 'TEMPSTU', '7', 'ADV', 'TEMPSTUMB0016', 'fdgdfgdfg gdfgdfffffffffffffffff', '1', 'admin', '2018-04-09 10:28:42', 'gfhgfhfgh', '1', 'Horizon College of Education -  - Malabe', 'xx', 'xxxxxxxxxxxx', 'xxxxxxxx', '');
INSERT INTO `hci_receipt` VALUES ('8', 'RECMB1800026', '8', '2018-04-09', '50000.00', null, '0.00', '0.00', 'TEMPSTU', '8', 'ADV', 'TEMPSTUMB0017', 'gfgdfgdfg ffgdfgdgdfg', '1', 'admin', '2018-04-09 10:55:28', 'yuytutyu', '1', 'Horizon College of Education -  - Malabe', 'xx', 'xxxxxxxxxxxx', 'xxxxxxxx', '');
INSERT INTO `hci_receipt` VALUES ('9', 'RECMB1800027', '9', '2018-04-09', '200000.00', null, '303000.00', '50000.00', 'STUDENT', '6', 'INV', 'MB/70/09/PG-6/6', 'ffgdfgdgdfg gfgdfgdfg', '1', 'admin', '2018-04-09 10:58:41', 'hjkjhj', '1', 'Horizon College of Education -  - Malabe', 'xx', 'xxxxxxxxxxxx', 'xxxxxxxx', '');
INSERT INTO `hci_receipt` VALUES ('10', 'RECMB1800028', '10', '2018-04-09', '10000.00', '60000.00', '103000.00', '50000.00', 'STUDENT', '6', 'INV', 'MB/70/09/PG-6/6', 'ffgdfgdgdfg gfgdfgdfg', '1', 'admin', '2018-04-09 14:31:00', 'jkhjk', '1', 'Horizon College of Education -  - Malabe', 'xx', 'xxxxxxxxxxxx', 'xxxxxxxx', '');
INSERT INTO `hci_receipt` VALUES ('11', 'RECMB1800029', '11', '2018-04-09', '100000.00', null, '43000.00', '0.00', 'STUDENT', '6', 'INV', 'MB/70/09/PG-6/6', 'ffgdfgdgdfg gfgdfgdfg', '1', 'admin', '2018-04-09 11:03:31', 'kjhkhjkjh', '1', 'Horizon College of Education -  - Malabe', 'xx', 'xxxxxxxxxxxx', 'xxxxxxxx', '');

-- ----------------------------
-- Table structure for `hci_scheme`
-- ----------------------------
DROP TABLE IF EXISTS `hci_scheme`;
CREATE TABLE `hci_scheme` (
  `schm_id` int(11) NOT NULL AUTO_INCREMENT,
  `schm_name` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `schm_status` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `schm_section` int(11) DEFAULT NULL,
  PRIMARY KEY (`schm_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of hci_scheme
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_schoolattendance`
-- ----------------------------
DROP TABLE IF EXISTS `hci_schoolattendance`;
CREATE TABLE `hci_schoolattendance` (
  `ssatn_id` int(11) NOT NULL AUTO_INCREMENT,
  `ssatn_ayear` int(11) DEFAULT NULL,
  `ssatn_grade` int(11) DEFAULT NULL,
  `ssatn_class` int(11) DEFAULT NULL,
  `ssatn_date` date DEFAULT NULL,
  `ssatn_student` int(11) DEFAULT NULL,
  `ssatn_isabsent` tinyint(5) DEFAULT NULL,
  `ssatn_remarks` text,
  PRIMARY KEY (`ssatn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of hci_schoolattendance
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_section`
-- ----------------------------
DROP TABLE IF EXISTS `hci_section`;
CREATE TABLE `hci_section` (
  `sec_id` int(11) NOT NULL AUTO_INCREMENT,
  `sec_name` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `sec_status` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`sec_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of hci_section
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_sibling`
-- ----------------------------
DROP TABLE IF EXISTS `hci_sibling`;
CREATE TABLE `hci_sibling` (
  `es_sibilingid` int(11) NOT NULL AUTO_INCREMENT,
  `es_family` int(11) DEFAULT NULL,
  `es_admissionid` int(11) NOT NULL,
  `es_seqnumber` int(11) NOT NULL,
  `es_reftable` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`es_sibilingid`),
  KEY `es_preadmissionid` (`es_admissionid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of hci_sibling
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_sibling1`
-- ----------------------------
DROP TABLE IF EXISTS `hci_sibling1`;
CREATE TABLE `hci_sibling1` (
  `es_sibilingid` int(11) NOT NULL AUTO_INCREMENT,
  `es_preadmissionid` int(11) NOT NULL,
  `es_sibiling` int(11) NOT NULL,
  PRIMARY KEY (`es_sibilingid`),
  KEY `es_preadmissionid` (`es_preadmissionid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of hci_sibling1
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_startdate`
-- ----------------------------
DROP TABLE IF EXISTS `hci_startdate`;
CREATE TABLE `hci_startdate` (
  `start_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hci_startdate
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_stfamily`
-- ----------------------------
DROP TABLE IF EXISTS `hci_stfamily`;
CREATE TABLE `hci_stfamily` (
  `sf_id` int(11) NOT NULL AUTO_INCREMENT,
  `sf_num` int(11) DEFAULT NULL,
  PRIMARY KEY (`sf_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=FIXED;

-- ----------------------------
-- Records of hci_stfamily
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_stuinvdiscount`
-- ----------------------------
DROP TABLE IF EXISTS `hci_stuinvdiscount`;
CREATE TABLE `hci_stuinvdiscount` (
  `id_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_invoice` int(11) NOT NULL,
  `id_feecat` int(11) DEFAULT NULL,
  `id_adjusment` int(11) NOT NULL,
  `id_adjstemp` int(11) NOT NULL,
  `id_amttype` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_description` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_amount` decimal(10,2) DEFAULT NULL,
  `id_calcamount` decimal(10,2) DEFAULT NULL,
  `id_type` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of hci_stuinvdiscount
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_stumonthinvdetail`
-- ----------------------------
DROP TABLE IF EXISTS `hci_stumonthinvdetail`;
CREATE TABLE `hci_stumonthinvdetail` (
  `smid_id` int(11) NOT NULL AUTO_INCREMENT,
  `smid_description` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `smid_glcode` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `smid_monthinv` int(11) NOT NULL,
  `smid_amount` decimal(10,2) NOT NULL,
  `smid_rdcrtype` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `smid_action` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `simd_grlupdateid` int(11) DEFAULT NULL,
  PRIMARY KEY (`smid_id`)
) ENGINE=MyISAM AUTO_INCREMENT=109 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of hci_stumonthinvdetail
-- ----------------------------
INSERT INTO `hci_stumonthinvdetail` VALUES ('1', 'Admission Fee Income', '450', '1', '50000.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('2', 'Differed  Revenue', '0030', '1', '200000.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('3', 'Accounts Receivables-School Students', '0018', '1', '250000.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('4', 'Admission Fee Income', '450', '2', '50000.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('5', 'Differed  Revenue', '0030', '2', '50000.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('6', 'Admission Fee Income', '450', '3', '50000.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('7', 'Differed  Revenue', '0030', '3', '50000.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('8', 'Admission Fee Income', '450', '4', '50000.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('9', 'Differed  Revenue', '0030', '4', '50000.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('10', 'Term Fee Income', '565', '5', '10600.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('11', 'Differed  Revenue', '0030', '5', '42400.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('12', 'Accounts Receivables-School Students', '0018', '5', '53000.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('13', 'Term Fee Income', '565', '6', '10600.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('14', 'Differed  Revenue', '0030', '6', '10600.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('15', 'Term Fee Income', '565', '7', '10600.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('16', 'Differed  Revenue', '0030', '7', '10600.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('17', 'Term Fee Income', '565', '8', '10600.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('18', 'Differed  Revenue', '0030', '8', '10600.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('19', 'Admission Fee Income', '450', '9', '50000.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('20', 'Differed  Revenue', '0030', '9', '200000.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('21', 'Accounts Receivables-School Students', '0018', '9', '250000.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('22', 'Admission Fee Income', '450', '10', '50000.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('23', 'Differed  Revenue', '0030', '10', '50000.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('24', 'Admission Fee Income', '450', '11', '50000.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('25', 'Differed  Revenue', '0030', '11', '50000.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('26', 'Admission Fee Income', '450', '12', '50000.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('27', 'Differed  Revenue', '0030', '12', '50000.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('28', 'Term Fee Income', '565', '13', '10600.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('29', 'Differed  Revenue', '0030', '13', '42400.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('30', 'Accounts Receivables-School Students', '0018', '13', '53000.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('31', 'Term Fee Income', '565', '14', '10600.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('32', 'Differed  Revenue', '0030', '14', '10600.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('33', 'Term Fee Income', '565', '15', '10600.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('34', 'Differed  Revenue', '0030', '15', '10600.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('35', 'Term Fee Income', '565', '16', '10600.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('36', 'Differed  Revenue', '0030', '16', '10600.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('37', 'Admission Fee Income', '450', '17', '50000.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('38', 'Differed  Revenue', '0030', '17', '200000.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('39', 'Accounts Receivables-School Students', '0018', '17', '250000.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('40', 'Admission Fee Income', '450', '18', '50000.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('41', 'Differed  Revenue', '0030', '18', '50000.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('42', 'Admission Fee Income', '450', '19', '50000.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('43', 'Differed  Revenue', '0030', '19', '50000.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('44', 'Admission Fee Income', '450', '20', '50000.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('45', 'Differed  Revenue', '0030', '20', '50000.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('46', 'Term Fee Income', '565', '21', '10600.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('47', 'Differed  Revenue', '0030', '21', '42400.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('48', 'Accounts Receivables-School Students', '0018', '21', '53000.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('49', 'Term Fee Income', '565', '22', '10600.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('50', 'Differed  Revenue', '0030', '22', '10600.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('51', 'Term Fee Income', '565', '23', '10600.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('52', 'Differed  Revenue', '0030', '23', '10600.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('53', 'Term Fee Income', '565', '24', '10600.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('54', 'Differed  Revenue', '0030', '24', '10600.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('55', 'Admission Fee Income', '450', '25', '50000.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('56', 'Differed  Revenue', '0030', '25', '200000.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('57', 'Accounts Receivables-School Students', '0018', '25', '250000.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('58', 'Admission Fee Income', '450', '26', '50000.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('59', 'Differed  Revenue', '0030', '26', '50000.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('60', 'Admission Fee Income', '450', '27', '50000.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('61', 'Differed  Revenue', '0030', '27', '50000.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('62', 'Admission Fee Income', '450', '28', '50000.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('63', 'Differed  Revenue', '0030', '28', '50000.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('64', 'Term Fee Income', '565', '29', '10600.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('65', 'Differed  Revenue', '0030', '29', '42400.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('66', 'Accounts Receivables-School Students', '0018', '29', '53000.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('67', 'Term Fee Income', '565', '30', '10600.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('68', 'Differed  Revenue', '0030', '30', '10600.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('69', 'Term Fee Income', '565', '31', '10600.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('70', 'Differed  Revenue', '0030', '31', '10600.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('71', 'Term Fee Income', '565', '32', '10600.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('72', 'Differed  Revenue', '0030', '32', '10600.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('73', 'Admission Fee Income', '450', '33', '50000.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('74', 'Differed  Revenue', '0030', '33', '200000.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('75', 'Accounts Receivables-School Students', '0018', '33', '250000.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('76', 'Admission Fee Income', '450', '34', '50000.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('77', 'Differed  Revenue', '0030', '34', '50000.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('78', 'Admission Fee Income', '450', '35', '50000.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('79', 'Differed  Revenue', '0030', '35', '50000.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('80', 'Admission Fee Income', '450', '36', '50000.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('81', 'Differed  Revenue', '0030', '36', '50000.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('82', 'Term Fee Income', '565', '37', '10600.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('83', 'Differed  Revenue', '0030', '37', '42400.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('84', 'Accounts Receivables-School Students', '0018', '37', '53000.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('85', 'Term Fee Income', '565', '38', '10600.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('86', 'Differed  Revenue', '0030', '38', '10600.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('87', 'Term Fee Income', '565', '39', '10600.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('88', 'Differed  Revenue', '0030', '39', '10600.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('89', 'Term Fee Income', '565', '40', '10600.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('90', 'Differed  Revenue', '0030', '40', '10600.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('91', 'Admission Fee Income', '450', '41', '50000.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('92', 'Differed  Revenue', '0030', '41', '200000.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('93', 'Accounts Receivables-School Students', '0018', '41', '250000.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('94', 'Admission Fee Income', '450', '42', '50000.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('95', 'Differed  Revenue', '0030', '42', '50000.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('96', 'Admission Fee Income', '450', '43', '50000.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('97', 'Differed  Revenue', '0030', '43', '50000.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('98', 'Admission Fee Income', '450', '44', '50000.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('99', 'Differed  Revenue', '0030', '44', '50000.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('100', 'Term Fee Income', '565', '45', '10600.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('101', 'Differed  Revenue', '0030', '45', '42400.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('102', 'Accounts Receivables-School Students', '0018', '45', '53000.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('103', 'Term Fee Income', '565', '46', '10600.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('104', 'Differed  Revenue', '0030', '46', '10600.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('105', 'Term Fee Income', '565', '47', '10600.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('106', 'Differed  Revenue', '0030', '47', '10600.00', 'DR', 'A', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('107', 'Term Fee Income', '565', '48', '10600.00', 'CR', 'D', null);
INSERT INTO `hci_stumonthinvdetail` VALUES ('108', 'Differed  Revenue', '0030', '48', '10600.00', 'DR', 'A', null);

-- ----------------------------
-- Table structure for `hci_stumonthlyinvoice`
-- ----------------------------
DROP TABLE IF EXISTS `hci_stumonthlyinvoice`;
CREATE TABLE `hci_stumonthlyinvoice` (
  `smi_id` int(11) NOT NULL AUTO_INCREMENT,
  `smi_invoice` int(11) NOT NULL,
  `smi_processdate` date NOT NULL,
  `smi_status` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `smi_customer` text COLLATE utf8_unicode_ci,
  `smi_subnumber` int(11) DEFAULT NULL,
  `smi_month` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `smi_refsid` int(11) DEFAULT NULL,
  `smi_gltransid` int(11) DEFAULT NULL,
  PRIMARY KEY (`smi_id`)
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of hci_stumonthlyinvoice
-- ----------------------------
INSERT INTO `hci_stumonthlyinvoice` VALUES ('1', '1', '0000-00-00', 'A', 'MB/70/09/PG-1/1 - uyuyuyu tyytytyty', '1', '2018-05-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('2', '1', '0000-00-00', 'A', 'MB/70/09/PG-1/1 - uyuyuyu tyytytyty', '2', '2018-06-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('3', '1', '0000-00-00', 'A', 'MB/70/09/PG-1/1 - uyuyuyu tyytytyty', '3', '2018-07-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('4', '1', '0000-00-00', 'A', 'MB/70/09/PG-1/1 - uyuyuyu tyytytyty', '4', '2018-08-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('5', '2', '0000-00-00', 'A', 'MB/70/09/PG-1/1 - uyuyuyu tyytytyty', '1', '2018-05-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('6', '2', '0000-00-00', 'A', 'MB/70/09/PG-1/1 - uyuyuyu tyytytyty', '2', '2018-06-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('7', '2', '0000-00-00', 'A', 'MB/70/09/PG-1/1 - uyuyuyu tyytytyty', '3', '2018-07-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('8', '2', '0000-00-00', 'A', 'MB/70/09/PG-1/1 - uyuyuyu tyytytyty', '4', '2018-08-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('9', '3', '0000-00-00', 'A', 'MB/70/09/PG-2/2 - ghfghfgh hghgfhgfh', '1', '2018-05-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('10', '3', '0000-00-00', 'A', 'MB/70/09/PG-2/2 - ghfghfgh hghgfhgfh', '2', '2018-06-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('11', '3', '0000-00-00', 'A', 'MB/70/09/PG-2/2 - ghfghfgh hghgfhgfh', '3', '2018-07-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('12', '3', '0000-00-00', 'A', 'MB/70/09/PG-2/2 - ghfghfgh hghgfhgfh', '4', '2018-08-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('13', '4', '0000-00-00', 'A', 'MB/70/09/PG-2/2 - ghfghfgh hghgfhgfh', '1', '2018-05-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('14', '4', '0000-00-00', 'A', 'MB/70/09/PG-2/2 - ghfghfgh hghgfhgfh', '2', '2018-06-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('15', '4', '0000-00-00', 'A', 'MB/70/09/PG-2/2 - ghfghfgh hghgfhgfh', '3', '2018-07-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('16', '4', '0000-00-00', 'A', 'MB/70/09/PG-2/2 - ghfghfgh hghgfhgfh', '4', '2018-08-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('17', '5', '0000-00-00', 'A', 'MB/70/09/PG-3/3 - fdgdgdfg fgfdgdfg', '1', '2018-05-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('18', '5', '0000-00-00', 'A', 'MB/70/09/PG-3/3 - fdgdgdfg fgfdgdfg', '2', '2018-06-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('19', '5', '0000-00-00', 'A', 'MB/70/09/PG-3/3 - fdgdgdfg fgfdgdfg', '3', '2018-07-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('20', '5', '0000-00-00', 'A', 'MB/70/09/PG-3/3 - fdgdgdfg fgfdgdfg', '4', '2018-08-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('21', '6', '0000-00-00', 'A', 'MB/70/09/PG-3/3 - fdgdgdfg fgfdgdfg', '1', '2018-05-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('22', '6', '0000-00-00', 'A', 'MB/70/09/PG-3/3 - fdgdgdfg fgfdgdfg', '2', '2018-06-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('23', '6', '0000-00-00', 'A', 'MB/70/09/PG-3/3 - fdgdgdfg fgfdgdfg', '3', '2018-07-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('24', '6', '0000-00-00', 'A', 'MB/70/09/PG-3/3 - fdgdgdfg fgfdgdfg', '4', '2018-08-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('25', '7', '0000-00-00', 'A', 'MB/70/09/PG-4/4 - fgdfgf fgfg', '1', '2018-05-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('26', '7', '0000-00-00', 'A', 'MB/70/09/PG-4/4 - fgdfgf fgfg', '2', '2018-06-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('27', '7', '0000-00-00', 'A', 'MB/70/09/PG-4/4 - fgdfgf fgfg', '3', '2018-07-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('28', '7', '0000-00-00', 'A', 'MB/70/09/PG-4/4 - fgdfgf fgfg', '4', '2018-08-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('29', '8', '0000-00-00', 'A', 'MB/70/09/PG-4/4 - fgdfgf fgfg', '1', '2018-05-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('30', '8', '0000-00-00', 'A', 'MB/70/09/PG-4/4 - fgdfgf fgfg', '2', '2018-06-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('31', '8', '0000-00-00', 'A', 'MB/70/09/PG-4/4 - fgdfgf fgfg', '3', '2018-07-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('32', '8', '0000-00-00', 'A', 'MB/70/09/PG-4/4 - fgdfgf fgfg', '4', '2018-08-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('33', '9', '0000-00-00', 'A', 'MB/70/09/PG-5/5 - gdfgdfffffffffffffffff fdgdfgdfg', '1', '2018-05-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('34', '9', '0000-00-00', 'A', 'MB/70/09/PG-5/5 - gdfgdfffffffffffffffff fdgdfgdfg', '2', '2018-06-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('35', '9', '0000-00-00', 'A', 'MB/70/09/PG-5/5 - gdfgdfffffffffffffffff fdgdfgdfg', '3', '2018-07-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('36', '9', '0000-00-00', 'A', 'MB/70/09/PG-5/5 - gdfgdfffffffffffffffff fdgdfgdfg', '4', '2018-08-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('37', '10', '0000-00-00', 'A', 'MB/70/09/PG-5/5 - gdfgdfffffffffffffffff fdgdfgdfg', '1', '2018-05-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('38', '10', '0000-00-00', 'A', 'MB/70/09/PG-5/5 - gdfgdfffffffffffffffff fdgdfgdfg', '2', '2018-06-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('39', '10', '0000-00-00', 'A', 'MB/70/09/PG-5/5 - gdfgdfffffffffffffffff fdgdfgdfg', '3', '2018-07-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('40', '10', '0000-00-00', 'A', 'MB/70/09/PG-5/5 - gdfgdfffffffffffffffff fdgdfgdfg', '4', '2018-08-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('41', '11', '0000-00-00', 'A', 'MB/70/09/PG-6/6 - ffgdfgdgdfg gfgdfgdfg', '1', '2018-05-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('42', '11', '0000-00-00', 'A', 'MB/70/09/PG-6/6 - ffgdfgdgdfg gfgdfgdfg', '2', '2018-06-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('43', '11', '0000-00-00', 'A', 'MB/70/09/PG-6/6 - ffgdfgdgdfg gfgdfgdfg', '3', '2018-07-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('44', '11', '0000-00-00', 'A', 'MB/70/09/PG-6/6 - ffgdfgdgdfg gfgdfgdfg', '4', '2018-08-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('45', '12', '0000-00-00', 'A', 'MB/70/09/PG-6/6 - ffgdfgdgdfg gfgdfgdfg', '1', '2018-05-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('46', '12', '0000-00-00', 'A', 'MB/70/09/PG-6/6 - ffgdfgdgdfg gfgdfgdfg', '2', '2018-06-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('47', '12', '0000-00-00', 'A', 'MB/70/09/PG-6/6 - ffgdfgdgdfg gfgdfgdfg', '3', '2018-07-1', null, null);
INSERT INTO `hci_stumonthlyinvoice` VALUES ('48', '12', '0000-00-00', 'A', 'MB/70/09/PG-6/6 - ffgdfgdgdfg gfgdfgdfg', '4', '2018-08-1', null, null);

-- ----------------------------
-- Table structure for `hci_subject`
-- ----------------------------
DROP TABLE IF EXISTS `hci_subject`;
CREATE TABLE `hci_subject` (
  `sub_id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sub_curriculum` int(11) DEFAULT NULL,
  `sub_type` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sub_mainorsel` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sub_status` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sub_section` int(11) DEFAULT NULL,
  PRIMARY KEY (`sub_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of hci_subject
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_subjectgroup`
-- ----------------------------
DROP TABLE IF EXISTS `hci_subjectgroup`;
CREATE TABLE `hci_subjectgroup` (
  `subgrp_id` int(11) NOT NULL AUTO_INCREMENT,
  `subgrp_name` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `subgrp_status` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `subgrp_type` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subgrp_schm` int(11) DEFAULT NULL,
  PRIMARY KEY (`subgrp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of hci_subjectgroup
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_subjectscheme`
-- ----------------------------
DROP TABLE IF EXISTS `hci_subjectscheme`;
CREATE TABLE `hci_subjectscheme` (
  `subschm_id` int(11) NOT NULL AUTO_INCREMENT,
  `subschm_subject` int(11) DEFAULT NULL,
  `subschm_scheme` int(11) DEFAULT NULL,
  `subschm_status` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subschm_subgroup` int(11) DEFAULT NULL,
  PRIMARY KEY (`subschm_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of hci_subjectscheme
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_subjsgroup`
-- ----------------------------
DROP TABLE IF EXISTS `hci_subjsgroup`;
CREATE TABLE `hci_subjsgroup` (
  `subjgrp_id` int(11) NOT NULL AUTO_INCREMENT,
  `subjgrp_group` int(11) DEFAULT NULL,
  `subjgrp_schm` int(11) DEFAULT NULL,
  `subjgrp_subject` int(11) DEFAULT NULL,
  `subjgrp_status` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`subjgrp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of hci_subjsgroup
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_subsubject`
-- ----------------------------
DROP TABLE IF EXISTS `hci_subsubject`;
CREATE TABLE `hci_subsubject` (
  `ssub_id` int(11) NOT NULL AUTO_INCREMENT,
  `ssub_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ssub_subject` int(11) DEFAULT NULL,
  `ssub_status` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ssub_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of hci_subsubject
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_term`
-- ----------------------------
DROP TABLE IF EXISTS `hci_term`;
CREATE TABLE `hci_term` (
  `term_id` int(11) NOT NULL AUTO_INCREMENT,
  `term_number` int(11) DEFAULT NULL,
  `term_sdate` date DEFAULT NULL,
  `term_edate` date DEFAULT NULL,
  `term_acyear` int(11) DEFAULT NULL,
  `term_periods` int(11) DEFAULT NULL,
  `term_isgeninvoice` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of hci_term
-- ----------------------------
INSERT INTO `hci_term` VALUES ('1', '1', '2016-09-01', '2016-12-31', '1', null, '0');
INSERT INTO `hci_term` VALUES ('2', '2', '2017-01-01', '2017-04-30', '1', null, '0');
INSERT INTO `hci_term` VALUES ('3', '3', '2017-05-01', '2017-08-31', '1', null, '0');
INSERT INTO `hci_term` VALUES ('4', '1', '2017-09-01', '2017-12-31', '2', null, '0');
INSERT INTO `hci_term` VALUES ('5', '2', '2018-01-01', '2018-04-30', '2', null, '0');
INSERT INTO `hci_term` VALUES ('6', '3', '2018-05-01', '2018-08-31', '2', null, '0');

-- ----------------------------
-- Table structure for `hci_termperiod`
-- ----------------------------
DROP TABLE IF EXISTS `hci_termperiod`;
CREATE TABLE `hci_termperiod` (
  `tprd_id` int(11) NOT NULL AUTO_INCREMENT,
  `tprd_name` varchar(75) DEFAULT NULL,
  `tprd_sdate` date DEFAULT NULL,
  `tprd_edate` date DEFAULT NULL,
  `tprd_status` varchar(5) DEFAULT NULL,
  `tprd_term` int(11) DEFAULT NULL,
  PRIMARY KEY (`tprd_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of hci_termperiod
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_transaction`
-- ----------------------------
DROP TABLE IF EXISTS `hci_transaction`;
CREATE TABLE `hci_transaction` (
  `tran_id` int(11) NOT NULL AUTO_INCREMENT,
  `tran_index` text,
  `tran_custype` varchar(20) DEFAULT NULL,
  `tran_customer` int(11) DEFAULT NULL,
  `tran_type` varchar(20) DEFAULT NULL,
  `tran_refid` int(11) DEFAULT NULL,
  `tran_description` varchar(200) DEFAULT NULL,
  `tran_date` date DEFAULT NULL,
  `tran_crordr` varchar(5) DEFAULT NULL,
  `tran_amount` decimal(10,2) DEFAULT NULL,
  `tran_balance` decimal(10,2) DEFAULT NULL,
  `tran_cashonhand` decimal(10,2) DEFAULT NULL,
  `tran_createdate` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `tran_createuser` int(11) DEFAULT NULL,
  `tran_createusername` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`tran_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of hci_transaction
-- ----------------------------
INSERT INTO `hci_transaction` VALUES ('1', 'TRANMB180000097', 'TEMPSTU', '1', 'PAYMENT', '1', 'ghfhg', '2018-04-06', 'CR', '50000.00', '-50000.00', '50000.00', '2018-04-06 06:17:31', '1', 'admin');
INSERT INTO `hci_transaction` VALUES ('2', 'TRANMB180000098', 'TEMPSTU', '2', 'PAYMENT', '2', 'fgdfgdfgdfg', '2018-04-06', 'CR', '10000.00', '-10000.00', '10000.00', '2018-04-06 06:17:52', '1', 'admin');
INSERT INTO `hci_transaction` VALUES ('3', 'TRANMB180000099', 'TEMPSTU', '3', 'PAYMENT', '3', 'ghhgfhg', '2018-04-06', 'CR', '20000.00', '-20000.00', '20000.00', '2018-04-06 06:18:17', '1', 'admin');
INSERT INTO `hci_transaction` VALUES ('4', 'TRANMB180000100', 'TEMPSTU', '5', 'PAYMENT', '4', 'uiuiuyi', '2018-04-06', 'CR', '40000.00', '-40000.00', '40000.00', '2018-04-06 06:26:07', '1', 'admin');
INSERT INTO `hci_transaction` VALUES ('5', 'TRANMB180000101', 'TEMPSTU', '6', 'PAYMENT', '5', 'jkhkhjkhjk', '2018-04-06', 'CR', '30000.00', '-30000.00', '30000.00', '2018-04-06 06:26:29', '1', 'admin');
INSERT INTO `hci_transaction` VALUES ('6', 'TRANMB180000102', 'STUDENT', '1', 'ADVANCEPAY', '1', 'ghfhg', '2018-04-06', 'CR', '50000.00', '-50000.00', '50000.00', '2018-04-06 06:17:31', '1', 'admin');
INSERT INTO `hci_transaction` VALUES ('7', 'TRANMB180000103', 'STUDENT', '1', 'INVOICE', '1', 'REGISTRATION - Admission Fee', '2018-04-06', 'DR', '250000.00', '200000.00', '50000.00', '2018-04-06 06:26:58', '1', 'admin');
INSERT INTO `hci_transaction` VALUES ('8', 'TRANMB180000104', 'STUDENT', '1', 'INVOICE', '2', 'REGISTRATION - Term Fee', '2018-04-06', 'DR', '53000.00', '253000.00', '50000.00', '2018-04-06 06:26:58', '1', 'admin');
INSERT INTO `hci_transaction` VALUES ('9', 'TRANMB180000105', 'STUDENT', '2', 'ADVANCEPAY', '2', 'fgdfgdfgdfg', '2018-04-06', 'CR', '10000.00', '-10000.00', '10000.00', '2018-04-06 06:17:52', '1', 'admin');
INSERT INTO `hci_transaction` VALUES ('10', 'TRANMB180000106', 'STUDENT', '2', 'INVOICE', '3', 'REGISTRATION - Admission Fee', '2018-04-06', 'DR', '250000.00', '240000.00', '0.00', '2018-04-06 12:30:39', '1', 'admin');
INSERT INTO `hci_transaction` VALUES ('11', 'TRANMB180000107', 'STUDENT', '2', 'INVOICE', '4', 'REGISTRATION - Term Fee', '2018-04-06', 'DR', '53000.00', '293000.00', '0.00', '2018-04-06 12:30:40', '1', 'admin');
INSERT INTO `hci_transaction` VALUES ('12', 'TRANMB180000108', 'STUDENT', '3', 'ADVANCEPAY', '4', 'uiuiuyi', '2018-04-06', 'CR', '40000.00', '-40000.00', '40000.00', '2018-04-06 06:26:07', '1', 'admin');
INSERT INTO `hci_transaction` VALUES ('13', 'TRANMB180000109', 'STUDENT', '3', 'INVOICE', '5', 'REGISTRATION - Admission Fee', '2018-04-09', 'DR', '250000.00', '210000.00', '0.00', '2018-04-09 05:28:52', '1', 'admin');
INSERT INTO `hci_transaction` VALUES ('14', 'TRANMB180000110', 'STUDENT', '3', 'INVOICE', '6', 'REGISTRATION - Term Fee', '2018-04-09', 'DR', '53000.00', '263000.00', '0.00', '2018-04-09 05:28:52', '1', 'admin');
INSERT INTO `hci_transaction` VALUES ('15', 'TRANMB180000111', 'STUDENT', '4', 'ADVANCEPAY', '5', 'jkhkhjkhjk', '2018-04-06', 'CR', '30000.00', '-30000.00', '30000.00', '2018-04-09 09:40:19', '1', 'admin');
INSERT INTO `hci_transaction` VALUES ('16', 'TRANMB180000112', 'STUDENT', '4', 'INVOICE', '7', 'REGISTRATION - Admission Fee', '2018-04-09', 'DR', '250000.00', '250000.00', '30000.00', '2018-04-09 06:01:56', '1', 'admin');
INSERT INTO `hci_transaction` VALUES ('17', 'TRANMB180000113', 'STUDENT', '4', 'INVOICE', '8', 'REGISTRATION - Term Fee', '2018-04-09', 'DR', '53000.00', '303000.00', '30000.00', '2018-04-09 06:01:56', '1', 'admin');
INSERT INTO `hci_transaction` VALUES ('18', 'TRANMB180000114', 'STUDENT', '4', 'PAYMENT', '6', 'hghfgh', '2018-04-09', 'CR', '303000.00', '30000.00', '0.00', '2018-04-09 06:27:09', '1', 'admin');
INSERT INTO `hci_transaction` VALUES ('19', 'TRANMB180000115', 'TEMPSTU', '7', 'PAYMENT', '7', 'gfhgfhfgh', '2018-04-09', 'CR', '50000.00', '-50000.00', '50000.00', '2018-04-09 10:28:42', '1', 'admin');
INSERT INTO `hci_transaction` VALUES ('20', 'TRANMB180000116', 'STUDENT', '5', 'ADVANCEPAY', '7', 'gfhgfhfgh', '2018-04-09', 'CR', '50000.00', '-50000.00', '50000.00', '2018-04-09 10:28:42', '1', 'admin');
INSERT INTO `hci_transaction` VALUES ('21', 'TRANMB180000117', 'STUDENT', '5', 'INVOICE', '9', 'REGISTRATION - Admission Fee', '2018-04-09', 'DR', '250000.00', '200000.00', '50000.00', '2018-04-09 10:33:02', '1', 'admin');
INSERT INTO `hci_transaction` VALUES ('22', 'TRANMB180000118', 'STUDENT', '5', 'INVOICE', '10', 'REGISTRATION - Term Fee', '2018-04-09', 'DR', '53000.00', '253000.00', '50000.00', '2018-04-09 10:33:02', '1', 'admin');
INSERT INTO `hci_transaction` VALUES ('23', 'TRANMB180000119', 'TEMPSTU', '8', 'PAYMENT', '8', 'yuytutyu', '2018-04-09', 'CR', '50000.00', '0.00', '50000.00', '2018-04-09 10:55:28', '1', 'admin');
INSERT INTO `hci_transaction` VALUES ('24', 'TRANMB180000120', 'STUDENT', '6', 'ADVANCEPAY', '8', 'yuytutyu', '2018-04-09', 'CR', '50000.00', '0.00', '50000.00', '2018-04-09 10:55:28', '1', 'admin');
INSERT INTO `hci_transaction` VALUES ('25', 'TRANMB180000121', 'STUDENT', '6', 'INVOICE', '11', 'REGISTRATION - Admission Fee', '2018-04-09', 'DR', '250000.00', '250000.00', '50000.00', '2018-04-09 10:57:12', '1', 'admin');
INSERT INTO `hci_transaction` VALUES ('26', 'TRANMB180000122', 'STUDENT', '6', 'INVOICE', '12', 'REGISTRATION - Term Fee', '2018-04-09', 'DR', '53000.00', '303000.00', '50000.00', '2018-04-09 10:57:12', '1', 'admin');
INSERT INTO `hci_transaction` VALUES ('27', 'TRANMB180000123', 'STUDENT', '6', 'PAYMENT', '9', 'hjkjhj', '2018-04-09', 'CR', '200000.00', '103000.00', '50000.00', '2018-04-09 10:58:41', '1', 'admin');
INSERT INTO `hci_transaction` VALUES ('28', 'TRANMB180000124', 'STUDENT', '6', 'PAYMENT', '10', 'jkhjk', '2018-04-09', 'CR', '60000.00', '43000.00', '0.00', '2018-04-09 11:01:00', '1', 'admin');
INSERT INTO `hci_transaction` VALUES ('29', 'TRANMB180000125', 'STUDENT', '6', 'PAYMENT', '11', 'kjhkhjkjh', '2018-04-09', 'CR', '100000.00', '0.00', '57000.00', '2018-04-09 11:03:31', '1', 'admin');

-- ----------------------------
-- Table structure for `hci_transmonthlypaydiv`
-- ----------------------------
DROP TABLE IF EXISTS `hci_transmonthlypaydiv`;
CREATE TABLE `hci_transmonthlypaydiv` (
  `tmpd_id` int(11) NOT NULL AUTO_INCREMENT,
  `tmpd_invoice` int(11) DEFAULT NULL,
  `tmpd_date` date DEFAULT NULL,
  `tmpd_month` int(11) DEFAULT NULL,
  `tmpd_amount` decimal(10,2) DEFAULT NULL,
  `tmpd_actualamount` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`tmpd_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=FIXED;

-- ----------------------------
-- Records of hci_transmonthlypaydiv
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_transpickpoint`
-- ----------------------------
DROP TABLE IF EXISTS `hci_transpickpoint`;
CREATE TABLE `hci_transpickpoint` (
  `tr_place_id` int(11) NOT NULL AUTO_INCREMENT,
  `tpp_index` text,
  `place_name` varchar(255) NOT NULL,
  `tpp_route` int(11) DEFAULT NULL,
  `tpp_stype` int(11) DEFAULT NULL,
  `created_on` date NOT NULL,
  PRIMARY KEY (`tr_place_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of hci_transpickpoint
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_transregistration`
-- ----------------------------
DROP TABLE IF EXISTS `hci_transregistration`;
CREATE TABLE `hci_transregistration` (
  `treg_id` int(11) NOT NULL AUTO_INCREMENT,
  `treg_index` text COLLATE utf8_unicode_ci,
  `treg_customer` int(11) DEFAULT NULL,
  `treg_custtype` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `treg_feestructure` int(11) DEFAULT NULL,
  `treg_route` int(11) DEFAULT NULL,
  `treg_pickpoint` int(11) DEFAULT NULL,
  `treg_status` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `treg_istempreg` tinyint(2) DEFAULT NULL,
  `treg_date` date DEFAULT NULL,
  `treg_famsequence` int(11) DEFAULT NULL,
  `treg_branch` int(11) DEFAULT NULL,
  PRIMARY KEY (`treg_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of hci_transregistration
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_trans_feestructure`
-- ----------------------------
DROP TABLE IF EXISTS `hci_trans_feestructure`;
CREATE TABLE `hci_trans_feestructure` (
  `fs_id` int(11) NOT NULL AUTO_INCREMENT,
  `fs_index` text COLLATE utf8_unicode_ci,
  `fs_name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fs_iscurrent` int(11) DEFAULT NULL,
  `fs_branch` int(11) DEFAULT NULL,
  PRIMARY KEY (`fs_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of hci_trans_feestructure
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_trans_fsfee`
-- ----------------------------
DROP TABLE IF EXISTS `hci_trans_fsfee`;
CREATE TABLE `hci_trans_fsfee` (
  `fsf_id` int(11) NOT NULL AUTO_INCREMENT,
  `fsf_fee` int(11) DEFAULT NULL,
  `fsf_distance` int(11) DEFAULT NULL,
  `fsf_isholiday` int(11) DEFAULT NULL,
  `fsf_amount` decimal(10,2) DEFAULT NULL,
  `fsf_feestructure` int(11) DEFAULT NULL,
  PRIMARY KEY (`fsf_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=FIXED;

-- ----------------------------
-- Records of hci_trans_fsfee
-- ----------------------------

-- ----------------------------
-- Table structure for `hci_trans_route`
-- ----------------------------
DROP TABLE IF EXISTS `hci_trans_route`;
CREATE TABLE `hci_trans_route` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `route_index` text,
  `route_title` varchar(255) NOT NULL,
  `route_Via` text NOT NULL,
  `amount` double NOT NULL,
  `status` varchar(5) NOT NULL,
  `created_on` datetime NOT NULL,
  `route_branch` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of hci_trans_route
-- ----------------------------

-- ----------------------------
-- Table structure for `hgc_academicyears`
-- ----------------------------
DROP TABLE IF EXISTS `hgc_academicyears`;
CREATE TABLE `hgc_academicyears` (
  `es_ac_year_id` int(11) NOT NULL AUTO_INCREMENT,
  `ac_startdate` date NOT NULL,
  `ac_enddate` date NOT NULL,
  `ac_iscurryear` tinyint(1) DEFAULT NULL,
  `ac_annualpromotion` int(11) DEFAULT NULL,
  `ac_isyearendrun` tinyint(5) NOT NULL DEFAULT '0',
  `ac_yearenddate` date DEFAULT NULL,
  PRIMARY KEY (`es_ac_year_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

-- ----------------------------
-- Records of hgc_academicyears
-- ----------------------------
INSERT INTO `hgc_academicyears` VALUES ('1', '2016-09-01', '2017-08-31', '0', null, '0', '0000-00-00');
INSERT INTO `hgc_academicyears` VALUES ('2', '2017-09-01', '2018-08-31', '1', null, '0', null);

-- ----------------------------
-- Table structure for `hgc_admins`
-- ----------------------------
DROP TABLE IF EXISTS `hgc_admins`;
CREATE TABLE `hgc_admins` (
  `es_adminsid` int(11) NOT NULL AUTO_INCREMENT,
  `admin_username` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_fname` varchar(255) NOT NULL,
  `user_type` int(11) NOT NULL,
  `user_theme` varchar(255) NOT NULL DEFAULT 'pink.css',
  `admin_lname` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_phoneno` varchar(255) NOT NULL,
  `admin_more` text NOT NULL,
  `admin_permissions` text NOT NULL,
  `admin_group` int(11) NOT NULL,
  `admin_branch` int(11) NOT NULL,
  PRIMARY KEY (`es_adminsid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of hgc_admins
-- ----------------------------

-- ----------------------------
-- Table structure for `hgc_authfunction`
-- ----------------------------
DROP TABLE IF EXISTS `hgc_authfunction`;
CREATE TABLE `hgc_authfunction` (
  `func_id` int(11) NOT NULL AUTO_INCREMENT,
  `func_name` varchar(150) NOT NULL,
  `func_module` varchar(200) NOT NULL,
  `func_submodule` varchar(200) NOT NULL,
  `func_url` text NOT NULL,
  `func_description` text,
  `func_developer` text,
  `func_status` varchar(5) NOT NULL,
  `func_type` varchar(50) DEFAULT NULL,
  `func_isedit` tinyint(5) DEFAULT NULL,
  `func_uitype` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`func_id`)
) ENGINE=InnoDB AUTO_INCREMENT=140 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hgc_authfunction
-- ----------------------------
INSERT INTO `hgc_authfunction` VALUES ('1', 'login', 'System Access', 'Login Module', 'login', 'Login View', 'Ruvini', 'A', 'Page Open', '0', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('2', 'authmanage :: register_function', 'System Access', 'Function Management', 'authmanage/register_function', 'Function Management View', 'Ruvini', 'A', 'Page Open', '0', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('3', 'authmanage :: load_function_data', 'System Access', 'Function Management', 'authmanage/load_function_data', 'Get function data to edit', 'Ruvini', 'A', 'Search - Detail View', '0', 'Input Event');
INSERT INTO `hgc_authfunction` VALUES ('4', 'authmanage :: load_submodule', 'System Access', 'Function Management', 'authmanage/load_submodule', 'Get Sub existing Sub Modules list under selected Module', 'Ruvini', 'A', 'Search - pre data', '0', 'Input Event');
INSERT INTO `hgc_authfunction` VALUES ('5', 'authmanage :: update_function_details', 'System Access', 'Function Management', 'authmanage/update_function_details', 'Update Function Details', 'Ruvini', 'A', 'Update', '0', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('6', 'dashboard', 'MIS', 'Dashboard', 'dashboard', 'Dashboard', 'Ruvini', 'A', 'Page Open', '0', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('7', 'login :: loginSubmit', 'System Access', 'Login Module', 'login/loginSubmit', 'Authenticate Login', 'Ruvini', 'A', 'Page Open', '0', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('8', 'login :: logout', 'System Access', 'Login Module', 'login/logout', 'Process Logout', 'Ruvini', 'A', 'Page Open', '0', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('9', 'authmanage :: accessrights_view', 'System Access', 'Access Rights Management', 'authmanage/accessrights_view', 'Access Rights Management', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('10', 'user :: usergroup_view', 'System Access', 'User Group', 'user/usergroup_view', 'User Group Management View', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('11', 'company', 'Configurations', 'Company', 'company', 'Company Management View', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('12', 'company :: load_terms', 'Configurations', 'Academic Year', 'company/load_terms', 'Get Terms list', 'Ruvini', 'A', 'Search - pre data', '1', 'Input Event');
INSERT INTO `hgc_authfunction` VALUES ('13', 'company :: update_comp_info', 'Configurations', 'Company', 'company/update_comp_info', 'Update Group Information', 'Ruvini', 'A', 'Update', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('14', 'company :: save_group', 'Configurations', 'Group', 'company/save_group', 'Create / Edit Company information', 'Ruvini', 'A', 'Insert/Update', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('15', 'company :: load_branches', 'Configurations', 'Branch', 'company/load_branches', 'Load Branches list for selected Company', 'Ruvini', 'A', 'Search - Main Lookup', '1', 'Input Event');
INSERT INTO `hgc_authfunction` VALUES ('16', 'company :: save_branch', 'Configurations', 'Branch', 'company/save_branch', 'Save /Update Branch information', 'Ruvini', 'A', 'Insert/Update', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('17', 'company :: save_fyear', 'Configurations', 'Financial Year', 'company/save_fyear', 'Save / Edit Financial Year', 'Ruvini', 'A', 'Insert/Update', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('18', 'user :: save_usergroup', 'System Access', 'User Group', 'user/save_usergroup', 'Save / Update User Group', 'Ruvini', 'A', 'Insert/Update', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('19', 'authmanage :: search_right', 'System Access', 'Access Rights Management', 'authmanage/search_right', 'Load User Group\'s Access Rights', 'Ruvini', 'A', 'Search - Detail View', '1', 'Search Button');
INSERT INTO `hgc_authfunction` VALUES ('20', 'authmanage :: set_rights', 'System Access', 'Access Rights Management', 'authmanage/set_rights', 'Save Access Rights', 'Ruvini', 'A', 'Update', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('21', 'hci_config :: department_view', 'Configurations', 'Department', 'hci_config/department_view', 'Access to department management sub module', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('22', 'company :: edit_branch_load', 'Configurations', 'Branch', 'company/edit_branch_load', 'Fetch branch details for edit', 'Ruvini', 'A', 'Search - Detail View', '1', 'Action Button');
INSERT INTO `hgc_authfunction` VALUES ('23', 'company :: save_ayear', 'Configurations', 'Academic Year', 'company/save_ayear', 'Save / Update Academic Year', 'Ruvini', 'A', 'Insert/Update', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('24', 'company :: load_termsperiods', 'Configurations', 'Academic Year', 'company/load_termsperiods', 'Fetch periods of selected term', 'Ruvini', 'A', 'Search - Detail View', '1', 'Input Event');
INSERT INTO `hgc_authfunction` VALUES ('25', 'company :: save_termperiod', 'Configurations', 'Academic Year', 'company/save_termperiod', 'Save / Update Term period', 'Ruvini', 'A', 'Search - Detail View', '1', 'Input Event');
INSERT INTO `hgc_authfunction` VALUES ('26', 'hci_config :: save_department', 'Configurations', 'Department', 'hci_config/save_department', 'Save / update department', 'Ruvini', 'A', 'Insert/Update', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('27', 'hci_config :: change_deptstatus', 'Configurations', 'Department', 'hci_config/change_deptstatus', 'Activate / Deactivate department', 'Ruvini', 'A', 'Activate/Deactivate', '1', 'Action Button');
INSERT INTO `hgc_authfunction` VALUES ('28', 'hci_config :: designation_view', 'Configurations', 'Designation', 'hci_config/designation_view', 'Access Designation Management module', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('29', 'hci_config :: save_designation', 'Configurations', 'Designation', 'hci_config/save_designation', 'Save / update designation', 'Ruvini', 'A', 'Insert/Update', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('30', 'hci_config :: change_desigstatus', 'Configurations', 'Designation', 'hci_config/change_desigstatus', 'Activate / Deactivate designation', 'Ruvini', 'A', 'Activate/Deactivate', '1', 'Action Button');
INSERT INTO `hgc_authfunction` VALUES ('31', 'hci_fee_structure :: fee_cat_view', 'Configurations', 'Fee Category', 'hci_fee_structure/fee_cat_view', 'Access Fee categories management', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('32', 'hci_fee_structure :: save_fee', 'Configurations', 'Fee Category', 'hci_fee_structure/save_fee', 'Save / Update Fee category', 'Ruvini', 'A', 'Insert/Update', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('33', 'hci_fee_structure', 'Configurations', 'Fee Template', 'hci_fee_structure', 'Access School Fee Structure Configuration', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('34', 'hci_fee_structure :: load_feestructure_data', 'Configurations', 'Fee Template', 'hci_fee_structure/load_feestructure_data', 'Load fee structure detail', 'Ruvini', 'A', 'Search - Detail View', '1', 'Input Event');
INSERT INTO `hgc_authfunction` VALUES ('35', 'hci_fee_structure :: save_fee_structure', 'Configurations', 'Fee Template', 'hci_fee_structure/save_fee_structure', 'Save/ Update Fee Structure', 'Ruvini', 'A', 'Insert/Update', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('36', 'hci_grade', 'Education Structure', 'Grade', 'hci_grade', 'Access to grade management', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('37', 'hci_grade :: save_grade', 'Education Structure', 'Grade', 'hci_grade/save_grade', 'Save / update grade', 'Ruvini', 'A', 'Insert/Update', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('38', 'hci_fee_structure :: load_feetemp_amounts', 'Configurations', 'Fee Template', 'hci_fee_structure/load_feetemp_amounts', 'Get fee structure amounts', 'Ruvini', 'A', 'Search - Detail View', '1', 'Input Event');
INSERT INTO `hgc_authfunction` VALUES ('39', 'hci_fee_structure :: payment_plan_view', 'Configurations', 'Payment Plan', 'hci_fee_structure/payment_plan_view', 'Access to payment plan configuration', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('40', 'hci_fee_structure :: save_paymentplan', 'Configurations', 'Payment Plan', 'hci_fee_structure/save_paymentplan', 'Save / update payment plan', 'Ruvini', 'A', 'Insert/Update', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('41', 'hci_accounts :: adjusment_view', 'Configurations', 'Adjusments', 'hci_accounts/adjusment_view', 'Access to adjustments configurations ', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('42', 'hci_accounts :: save_adjusment', 'Configurations', 'Adjusments', 'hci_accounts/save_adjusment', 'Save adjustment ', 'Ruvini', 'A', 'Insert', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('43', 'hci_fee_structure :: load_paymentplan', 'Configurations', 'Adjusments', 'hci_fee_structure/load_paymentplan', 'Load payment plans for selected fee category', 'Ruvini', 'A', 'Search - pre data', '1', 'Input Event');
INSERT INTO `hgc_authfunction` VALUES ('44', 'hci_fee_structure :: load_periods', 'Configurations', 'Adjusments', 'hci_fee_structure/load_periods', 'Load number of periods for selected payment plan', 'Ruvini', 'A', 'Search - pre data', '1', 'Input Event');
INSERT INTO `hgc_authfunction` VALUES ('45', 'hci_accounts :: config_adjusments', 'Configurations', 'Adjusments', 'hci_accounts/config_adjusments', 'Save/ update configuration of adjusment', 'Ruvini', 'A', 'Insert/Update', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('46', 'hci_admission :: inquary_view', 'Front Office', 'Inquiry Form', 'hci_admission/inquary_view', 'Access to Inquiry Form', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('47', 'hci_admission :: new_admission', 'Front Office', 'Inquiry Form', 'hci_admission/new_admission', 'Save / Update inquiries ', 'Ruvini', 'A', 'Insert/Update', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('48', 'hci_studentreg :: stureg_view', 'Student Management', 'Registration', 'hci_studentreg/stureg_view', 'Access to student registration form', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('49', 'hci_studentreg :: load_academic_data', 'Student Management', 'Registration', 'hci_studentreg/load_academic_data', 'Load academic data', 'Ruvini', 'A', 'Search - pre data', '1', 'Input Event');
INSERT INTO `hgc_authfunction` VALUES ('50', 'hci_studentreg :: load_sibling_data', 'Student Management', 'Registration', 'hci_studentreg/load_sibling_data', 'Load siblings and related discounts', 'Ruvini', 'A', 'Search - pre data', '1', 'Input Event');
INSERT INTO `hgc_authfunction` VALUES ('51', 'hci_studentreg :: new_studentreg', 'Student Management', 'Registration', 'hci_studentreg/new_studentreg', 'Save  New registration', 'Ruvini', 'A', 'Insert', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('52', 'hci_studentreg :: load_branch_student', 'Student Management', 'Registration', 'hci_studentreg/load_branch_student', 'Get branch sibling', 'Ruvini', 'A', 'Search - pre data', '1', 'Input Event');
INSERT INTO `hgc_authfunction` VALUES ('53', 'hci_student :: student_lookup', 'Student Management', 'Student List', 'hci_student/student_lookup', 'Load Student List', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('54', 'hci_student :: load_student', 'Student Management', 'Student List', 'hci_student/load_student', 'Load student list ', 'Ruvini', 'A', 'Search - Main Lookup', '1', 'Search Button');
INSERT INTO `hgc_authfunction` VALUES ('55', 'hci_student :: load_stuprofview', 'Student Management', 'Student List', 'hci_student/load_stuprofview', 'Load Student profile', 'Ruvini', 'A', 'Search - Detail View', '1', 'Action Button');
INSERT INTO `hgc_authfunction` VALUES ('56', 'hci_student :: load_stueditview', 'Student Management', 'Student List', 'hci_student/load_stueditview', 'Access to student edit view', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('57', 'hci_student :: annualgradepromotion_view', 'Student Management', 'Grade Promotion', 'hci_student/annualgradepromotion_view', 'Access to grade promotion view', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('58', 'hci_student :: load_stubygrade', 'Student Management', 'Grade Promotion', 'hci_student/load_stubygrade', 'Load Student by branch and grade', 'Ruvini', 'A', 'Search - pre data', '1', 'Search Button');
INSERT INTO `hgc_authfunction` VALUES ('59', 'hci_student :: promote_students', 'Student Management', 'Grade Promotion', 'hci_student/promote_students', 'Save / Update promotion', 'Ruvini', 'A', 'Update', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('60', 'hci_grade :: classmanage_view', 'Education Structure', 'Class', 'hci_grade/classmanage_view', 'Access Class manage view', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('61', 'hci_grade :: load_class_list', 'Education Structure', 'Class', 'hci_grade/load_class_list', 'Load classes list for selected academic year', 'Ruvini', 'A', 'Search - Main Lookup', '1', 'Input Event');
INSERT INTO `hgc_authfunction` VALUES ('62', 'hci_grade :: save_class', 'Education Structure', 'Class', 'hci_grade/save_class', 'Save / update class', 'Ruvini', 'A', 'Insert/Update', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('63', 'hci_student :: student_sufflingview', 'Student Management', 'Student Shuffling', 'hci_student/student_sufflingview', 'Access to student shuffling view', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('64', 'hci_student :: load_class_stulist', 'Student Management', 'Student Shuffling', 'hci_student/load_class_stulist', 'Load current student class list', 'Ruvini', 'A', 'Search - Detail View', '1', 'Search Button');
INSERT INTO `hgc_authfunction` VALUES ('65', 'hci_student :: load_shuffledata', 'Student Management', 'Student Shuffling', 'hci_student/load_shuffledata', 'Load student list for shuffling', 'Ruvini', 'A', 'Search - pre data', '1', 'Action Button');
INSERT INTO `hgc_authfunction` VALUES ('66', 'hci_student :: save_suffling', 'Student Management', 'Student Shuffling', 'hci_student/save_suffling', 'Save / update shuffling', 'Ruvini', 'A', 'Insert/Update', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('67', 'hci_accounts :: invoice_view', 'Accounts', 'Invoice Lookup', 'hci_accounts/invoice_view', 'Access to invoice Look up', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('68', 'hci_accounts :: load_invoicefees', 'Accounts', 'Invoice Lookup', 'hci_accounts/load_invoicefees', 'Load individual invoice', 'Ruvini', 'A', 'Search - Detail View', '1', 'Action Button');
INSERT INTO `hgc_authfunction` VALUES ('69', 'hci_accounts :: print_invoice', 'Accounts', 'Invoice Lookup', 'hci_accounts/print_invoice', 'Get PDF of invoice', 'Ruvini', 'A', 'Search - Detail View', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('70', 'hci_payment', 'Accounts', 'Payments', 'hci_payment', 'Access to payment management view', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('71', 'hci_payment :: load_payments', 'Accounts', 'Payments', 'hci_payment/load_payments', 'Load payments list', 'Ruvini', 'A', 'Search - Main Lookup', '1', 'Input Event');
INSERT INTO `hgc_authfunction` VALUES ('72', 'hci_payment :: load_customers', 'Accounts', 'Payments', 'hci_payment/load_customers', 'Load customer list according to the customer type', 'Ruvini', 'A', 'Search - pre data', '1', 'Input Event');
INSERT INTO `hgc_authfunction` VALUES ('73', 'hci_payment :: load_outstanding', 'Accounts', 'Payments', 'hci_payment/load_outstanding', 'Load customer outstanding invoices', 'Ruvini', 'A', 'Search - pre data', '1', 'Input Event');
INSERT INTO `hgc_authfunction` VALUES ('74', 'hci_payment :: save_payment', 'Accounts', 'Payments', 'hci_payment/save_payment', 'Create Payement', 'Ruvini', 'A', 'Insert', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('75', 'hci_payment :: load_paymentrecipt', 'Accounts', 'Payments', 'hci_payment/load_paymentrecipt', 'Load receipt modal view', 'Ruvini', 'A', 'Search - Detail View', '1', 'Action Button');
INSERT INTO `hgc_authfunction` VALUES ('76', 'hci_payment :: print_receipt', 'Accounts', 'Payments', 'hci_payment/print_receipt', 'Load receipt print view in pdf', 'Ruvini', 'A', 'Search - Detail View', '1', 'Action Button');
INSERT INTO `hgc_authfunction` VALUES ('77', 'hci_accounts :: creditnote_view', 'Accounts', 'Credit Note', 'hci_accounts/creditnote_view', 'Access to credit note view', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('78', 'hci_accounts :: load_cnotelist', 'Accounts', 'Credit Note', 'hci_accounts/load_cnotelist', 'Load credit notes', 'Ruvini', 'A', 'Search - Main Lookup', '1', 'Input Event');
INSERT INTO `hgc_authfunction` VALUES ('79', 'hci_accounts :: save_creditnote', 'Accounts', 'Credit Note', 'hci_accounts/save_creditnote', 'Insert Credit Note', 'Ruvini', 'A', 'Insert', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('80', 'hci_accounts :: load_cnote_receipt', 'Accounts', 'Credit Note', 'hci_accounts/load_cnote_receipt', 'Load Credit note detail view', 'Ruvini', 'A', 'Search - Detail View', '1', 'Action Button');
INSERT INTO `hgc_authfunction` VALUES ('81', 'hci_accounts :: print_cnote', 'Accounts', 'Credit Note', 'hci_accounts/print_cnote', 'Load credit note print view (PDF)', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('82', 'hci_accounts :: debitnote_view', 'Accounts', 'Debit Note', 'hci_accounts/debitnote_view', 'Access to debit note view', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('83', 'hci_accounts :: load_dnotelist', 'Accounts', 'Debit Note', 'hci_accounts/load_dnotelist', 'Load debit note list', 'Ruvini', 'A', 'Search - Main Lookup', '1', 'Input Event');
INSERT INTO `hgc_authfunction` VALUES ('84', 'hci_accounts :: save_debitnote', 'Accounts', 'Debit Note', 'hci_accounts/save_debitnote', 'Save debit note', 'Ruvini', 'A', 'Insert', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('85', 'hci_accounts :: load_dnote_receipt', 'Accounts', 'Debit Note', 'hci_accounts/load_dnote_receipt', 'Load debit note detail view', 'Ruvini', 'A', 'Search - Detail View', '1', 'Action Button');
INSERT INTO `hgc_authfunction` VALUES ('86', 'hci_accounts :: print_dnote', 'Accounts', 'Debit Note', 'hci_accounts/print_dnote', 'Load debit note print view', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('87', 'hci_accounts :: monthend_view', 'Accounts', 'Month End', 'hci_accounts/monthend_view', 'Access to month end process', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('88', 'hci_accounts :: run_monthend', 'Accounts', 'Month End', 'hci_accounts/run_monthend', 'Process Month end', 'Ruvini', 'A', 'Insert', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('89', 'hci_accounts :: chequemanagment_view', 'Accounts', 'Cheque Management', 'hci_accounts/chequemanagment_view', 'Load Cheque management view', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('90', 'hci_accounts :: load_chequelist', 'Accounts', 'Cheque Management', 'hci_accounts/load_chequelist', 'Load cheque list', 'Ruvini', 'A', 'Search - Main Lookup', '1', 'Input Event');
INSERT INTO `hgc_authfunction` VALUES ('91', 'hci_accounts :: bouns_cheque', 'Accounts', 'Cheque Management', 'hci_accounts/bouns_cheque', 'Bounce a Cheque', 'Ruvini', 'A', 'Update', '1', 'Action Button');
INSERT INTO `hgc_authfunction` VALUES ('92', 'hci_attendance :: studentclass_attendance', 'Attendance', 'School Attendance', 'hci_attendance/studentclass_attendance', 'Access to attendance view', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('93', 'hci_attendance :: load_classlist', 'Attendance', 'School Attendance', 'hci_attendance/load_classlist', 'Load existing classes list', 'Ruvini', 'A', 'Search - pre data', '1', 'Input Event');
INSERT INTO `hgc_authfunction` VALUES ('94', 'hci_attendance :: search_attendance', 'Attendance', 'School Attendance', 'hci_attendance/search_attendance', 'Load current attendance', 'Ruvini', 'A', 'Search - Detail View', '1', 'Search Button');
INSERT INTO `hgc_authfunction` VALUES ('95', 'hci_attendance :: save_attendance', 'Attendance', 'School Attendance', 'hci_attendance/save_attendance', 'Save / Update Attendance', 'Ruvini', 'A', 'Insert/Update', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('96', 'hci_staff', 'Staff', 'Add Staff', 'hci_staff', 'Load Staff add view', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('97', 'hci_staff :: staff_save', 'Staff', 'Add Staff', 'hci_staff/staff_save', 'Save/ update staff member details', 'Ruvini', 'A', 'Insert/Update', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('98', 'hci_edustructure :: educationalsection', 'Education Structure', 'Section', 'hci_edustructure/educationalsection', 'Access to educational structure view', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('99', 'hci_edustructure :: save_edusection', 'Education Structure', 'Section', 'hci_edustructure/save_edusection', 'Access to the Educational Section view', 'Ruvini', 'A', 'Insert/Update', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('100', 'hci_edustructure :: change_sectionstatus', 'Education Structure', 'Section', 'hci_edustructure/change_sectionstatus', 'Activate / Deactivate educational section', 'Ruvini', 'A', 'Activate/Deactivate', '1', 'Action Button');
INSERT INTO `hgc_authfunction` VALUES ('101', 'hci_edustructure :: educationalscheme', 'Education Structure', 'Scheme', 'hci_edustructure/educationalscheme', 'Access to Manage Scheme view', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('102', 'hci_edustructure :: save_eduscheme', 'Education Structure', 'Scheme', 'hci_edustructure/save_eduscheme', 'Save/ update Scheme', 'Ruvini', 'A', 'Insert/Update', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('103', 'hci_edustructure :: change_schemestatus', 'Education Structure', 'Scheme', 'hci_edustructure/change_schemestatus', 'Activate / Deactivate Scheme', 'Ruvini', 'A', 'Activate/Deactivate', '1', 'Action Button');
INSERT INTO `hgc_authfunction` VALUES ('104', 'hci_subject :: subjectgroup_view', 'Subject', 'Subject Group', 'hci_subject/subjectgroup_view', 'Access to subject group view', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('105', 'hci_subject :: save_subjectgroup', 'Subject', 'Subject Group', 'hci_subject/save_subjectgroup', 'Save / update subject group', 'Ruvini', 'A', 'Insert/Update', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('106', 'hci_subject :: change_subgrpstatus', 'Subject', 'Subject Group', 'hci_subject/change_subgrpstatus', 'Activate / Deactivate Subject group', 'Ruvini', 'A', 'Activate/Deactivate', '1', 'Action Button');
INSERT INTO `hgc_authfunction` VALUES ('107', 'hci_subject :: subject_view', 'Subject', 'Subject', 'hci_subject/subject_view', 'Access to subject view', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('108', 'hci_subject :: load_schemelist', 'Subject', 'Subject', 'hci_subject/load_schemelist', 'Load schemes for selected curriculum ', 'Ruvini', 'A', 'Search - pre data', '1', 'Input Event');
INSERT INTO `hgc_authfunction` VALUES ('109', 'hci_subject :: save_subject', 'Subject', 'Subject', 'hci_subject/save_subject', 'Save / update subject', 'Ruvini', 'A', 'Insert/Update', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('110', 'hci_subject :: edit_sub_load', 'Subject', 'Subject', 'hci_subject/edit_sub_load', 'Load subject info to edit', 'Ruvini', 'A', 'Search - pre data', '1', 'Action Button');
INSERT INTO `hgc_authfunction` VALUES ('111', 'hci_subject :: change_substatus', 'Subject', 'Subject', 'hci_subject/change_substatus', 'Activate / deactivate subject', 'Ruvini', 'A', 'Activate/Deactivate', '1', 'Action Button');
INSERT INTO `hgc_authfunction` VALUES ('112', 'hci_transport :: manage_routes_view', 'Transport', 'Route', 'hci_transport/manage_routes_view', 'Access to Route Management view', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('113', 'hci_transport :: save_route', 'Transport', 'Route', 'hci_transport/save_route', 'Save / Update Route', 'Ruvini', 'A', 'Insert/Update', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('114', 'hci_transport :: manage_pickingpoints', 'Transport', 'Picking Point', 'hci_transport/manage_pickingpoints', 'Access to the picking points management view', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('115', 'hci_transport :: save_pickpoint', 'Transport', 'Picking Point', 'hci_transport/save_pickpoint', 'Save / update picking points', 'Ruvini', 'A', 'Insert/Update', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('116', 'hci_transport :: transport_feestructure', 'Transport', 'Fee Structure', 'hci_transport/transport_feestructure', 'Access to transport fee Structure view', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('117', 'hci_transport :: save_feestructure', 'Transport', 'Fee Structure', 'hci_transport/save_feestructure', 'Save / update transport fee structure', 'Ruvini', 'A', 'Insert/Update', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('118', 'hci_transport :: load_transfeestructures', 'Transport', 'Fee Structure', 'hci_transport/load_transfeestructures', 'Load fee structure as a detailed view ', 'Ruvini', 'A', 'Search - Detail View', '1', 'Action Button');
INSERT INTO `hgc_authfunction` VALUES ('119', 'hci_transport :: transport_registration', 'Transport', 'Registration', 'hci_transport/transport_registration', 'Access to transport registration', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('120', 'hci_transport :: load_pickingpoints', 'Transport', 'Registration', 'hci_transport/load_pickingpoints', 'Load picking points for selected route', 'Ruvini', 'A', 'Search - pre data', '1', 'Input Event');
INSERT INTO `hgc_authfunction` VALUES ('121', 'hci_transport :: registration_trans', 'Transport', 'Registration', 'hci_transport/registration_trans', 'Save Transport registration', 'Ruvini', 'A', 'Insert', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('122', 'hci_childcare :: cc_fee_structure', 'Childcare Centre', 'Fee Structure', 'hci_childcare/cc_fee_structure', 'Access  to childcare fee structure', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');
INSERT INTO `hgc_authfunction` VALUES ('123', 'hci_childcare :: save_feestructure', 'Childcare Centre', 'Fee Structure', 'hci_childcare/save_feestructure', 'Save / update childcare fee structure', 'Ruvini', 'A', 'Insert/Update', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('124', 'hci_studentreg', '', '', 'hci_studentreg', null, 'Ruvini', 'A', null, '1', null);
INSERT INTO `hgc_authfunction` VALUES ('125', 'hci_fee_structure :: edit_pay_plan', 'Configurations', 'Fee Structure', 'hci_fee_structure/edit_pay_plan', null, 'Ruvini', 'A', null, '1', null);
INSERT INTO `hgc_authfunction` VALUES ('126', 'hci_grade :: change_status', 'Education Structure', 'Grade', 'hci_grade/change_status', null, 'Ruvini', 'A', null, '1', null);
INSERT INTO `hgc_authfunction` VALUES ('127', 'hci_fee_structure :: delete_feecat', 'Configurations', 'Fee Category', 'hci_fee_structure/delete_feecat', 'Delete  fee category', 'Ruvini', 'A', 'Delete', '1', 'Action Button');
INSERT INTO `hgc_authfunction` VALUES ('128', 'hci_fee_structure :: load_feetemplate_list', 'Configurations', 'Fee Template', 'hci_fee_structure/load_feetemplate_list', 'Load  fee templates list by branch and type', 'Ruvini', 'A', 'Search - pre data', '1', 'Input Event');
INSERT INTO `hgc_authfunction` VALUES ('129', 'hci_fee_structure :: load_feetemplate_data', 'Configurations', 'Fee Template', 'hci_fee_structure/load_feetemplate_data', 'Load fee template details', 'Ruvini', 'A', 'Search - Main Lookup', '1', 'Input Event');
INSERT INTO `hgc_authfunction` VALUES ('130', 'hci_fee_structure :: load_feetemplate_amounts', 'Configurations', 'Fee Template', 'hci_fee_structure/load_feetemplate_amounts', 'Load fee template  amounts', 'Ruvini', 'A', 'Search - Main Lookup', '1', 'Input Event');
INSERT INTO `hgc_authfunction` VALUES ('131', 'hci_studentreg :: display_prefmethod_view', 'Student Management', 'Registration', 'hci_studentreg/display_prefmethod_view', 'Display preferred payment method ', 'Ruvini', 'A', 'Search - pre data', '1', 'Action Button');
INSERT INTO `hgc_authfunction` VALUES ('132', 'hci_studentreg :: load_feestructures', 'Student Management', 'Registration', 'hci_studentreg/load_feestructures', 'load fee structures', 'Ruvini', 'A', 'Search - pre data', '1', 'Input Event');
INSERT INTO `hgc_authfunction` VALUES ('133', 'hci_admission :: get_student_info', 'Front Office', 'Inquiry Form', 'hci_admission/get_student_info', 'get student\'s information', 'Ruvini', 'A', 'Search - pre data', '1', 'Tab');
INSERT INTO `hgc_authfunction` VALUES ('134', 'hci_studentreg :: confirm_regi', 'Student Management', 'Registration', 'hci_studentreg/confirm_regi', 'load confirmation detail  to verify', 'Ruvini', 'A', 'Search - Detail View', '1', 'Search Button');
INSERT INTO `hgc_authfunction` VALUES ('135', 'hci_student :: set_studentdatasession', 'Student Management', 'Registration', 'hci_student/set_studentdatasession', null, 'Ruvini', 'A', null, '1', null);
INSERT INTO `hgc_authfunction` VALUES ('136', 'hci_student :: update_student_info', 'Student Management', 'Student Information Update', 'hci_student/update_student_info', 'Update student information', 'Ruvini', 'A', 'Update', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('137', 'hci_student :: load_student_discounts', 'Student Management', 'Registration', 'hci_student/load_student_discounts', null, 'Ruvini', 'A', null, '1', null);
INSERT INTO `hgc_authfunction` VALUES ('138', 'hci_student :: registerpendingstu', 'Student Management', 'Registration', 'hci_student/registerpendingstu', 'Register temporary registered students. ', 'Ruvini', 'A', 'Update', '1', 'Submit Button');
INSERT INTO `hgc_authfunction` VALUES ('139', 'user', 'System Access', 'User', 'user', 'Access to user management', 'Ruvini', 'A', 'Page Open', '1', 'Full Page');

-- ----------------------------
-- Table structure for `hgc_authright`
-- ----------------------------
DROP TABLE IF EXISTS `hgc_authright`;
CREATE TABLE `hgc_authright` (
  `rgt_id` int(11) NOT NULL AUTO_INCREMENT,
  `rgt_usergroup` int(11) DEFAULT NULL,
  `rgt_user` int(11) NOT NULL,
  `rgt_group` int(11) DEFAULT NULL,
  `rgt_branch` int(11) DEFAULT NULL,
  `rgt_type` varchar(20) DEFAULT NULL,
  `rgt_refid` int(11) DEFAULT NULL,
  `rgt_status` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`rgt_id`)
) ENGINE=InnoDB AUTO_INCREMENT=261 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hgc_authright
-- ----------------------------
INSERT INTO `hgc_authright` VALUES ('1', '2', '0', null, '1', 'function', '9', 'A');
INSERT INTO `hgc_authright` VALUES ('2', '2', '0', null, '1', 'function', '19', 'A');
INSERT INTO `hgc_authright` VALUES ('3', '2', '0', null, '1', 'function', '20', 'A');
INSERT INTO `hgc_authright` VALUES ('4', '2', '0', null, '1', 'function', '139', 'A');
INSERT INTO `hgc_authright` VALUES ('5', '2', '0', null, '1', 'function', '10', 'A');
INSERT INTO `hgc_authright` VALUES ('6', '2', '0', null, '1', 'function', '18', 'A');
INSERT INTO `hgc_authright` VALUES ('7', '2', '0', null, '1', 'function', '12', 'A');
INSERT INTO `hgc_authright` VALUES ('8', '2', '0', null, '1', 'function', '23', 'A');
INSERT INTO `hgc_authright` VALUES ('9', '2', '0', null, '1', 'function', '24', 'A');
INSERT INTO `hgc_authright` VALUES ('10', '2', '0', null, '1', 'function', '25', 'A');
INSERT INTO `hgc_authright` VALUES ('11', '2', '0', null, '1', 'function', '41', 'A');
INSERT INTO `hgc_authright` VALUES ('12', '2', '0', null, '1', 'function', '42', 'A');
INSERT INTO `hgc_authright` VALUES ('13', '2', '0', null, '1', 'function', '43', 'A');
INSERT INTO `hgc_authright` VALUES ('14', '2', '0', null, '1', 'function', '44', 'A');
INSERT INTO `hgc_authright` VALUES ('15', '2', '0', null, '1', 'function', '45', 'A');
INSERT INTO `hgc_authright` VALUES ('16', '2', '0', null, '1', 'function', '15', 'A');
INSERT INTO `hgc_authright` VALUES ('17', '2', '0', null, '1', 'function', '16', 'A');
INSERT INTO `hgc_authright` VALUES ('18', '2', '0', null, '1', 'function', '22', 'A');
INSERT INTO `hgc_authright` VALUES ('19', '2', '0', null, '1', 'function', '11', 'A');
INSERT INTO `hgc_authright` VALUES ('20', '2', '0', null, '1', 'function', '13', 'A');
INSERT INTO `hgc_authright` VALUES ('21', '2', '0', null, '1', 'function', '21', 'A');
INSERT INTO `hgc_authright` VALUES ('22', '2', '0', null, '1', 'function', '26', 'A');
INSERT INTO `hgc_authright` VALUES ('23', '2', '0', null, '1', 'function', '27', 'A');
INSERT INTO `hgc_authright` VALUES ('24', '2', '0', null, '1', 'function', '28', 'A');
INSERT INTO `hgc_authright` VALUES ('25', '2', '0', null, '1', 'function', '29', 'A');
INSERT INTO `hgc_authright` VALUES ('26', '2', '0', null, '1', 'function', '30', 'A');
INSERT INTO `hgc_authright` VALUES ('27', '2', '0', null, '1', 'function', '31', 'A');
INSERT INTO `hgc_authright` VALUES ('28', '2', '0', null, '1', 'function', '32', 'A');
INSERT INTO `hgc_authright` VALUES ('29', '2', '0', null, '1', 'function', '127', 'A');
INSERT INTO `hgc_authright` VALUES ('30', '2', '0', null, '1', 'function', '125', 'A');
INSERT INTO `hgc_authright` VALUES ('31', '2', '0', null, '1', 'function', '33', 'A');
INSERT INTO `hgc_authright` VALUES ('32', '2', '0', null, '1', 'function', '34', 'A');
INSERT INTO `hgc_authright` VALUES ('33', '2', '0', null, '1', 'function', '35', 'A');
INSERT INTO `hgc_authright` VALUES ('34', '2', '0', null, '1', 'function', '38', 'A');
INSERT INTO `hgc_authright` VALUES ('35', '2', '0', null, '1', 'function', '128', 'A');
INSERT INTO `hgc_authright` VALUES ('36', '2', '0', null, '1', 'function', '129', 'A');
INSERT INTO `hgc_authright` VALUES ('37', '2', '0', null, '1', 'function', '130', 'A');
INSERT INTO `hgc_authright` VALUES ('38', '2', '0', null, '1', 'function', '17', 'A');
INSERT INTO `hgc_authright` VALUES ('39', '2', '0', null, '1', 'function', '14', 'A');
INSERT INTO `hgc_authright` VALUES ('40', '2', '0', null, '1', 'function', '39', 'A');
INSERT INTO `hgc_authright` VALUES ('41', '2', '0', null, '1', 'function', '40', 'A');
INSERT INTO `hgc_authright` VALUES ('42', '2', '0', null, '1', 'function', '60', 'A');
INSERT INTO `hgc_authright` VALUES ('43', '2', '0', null, '1', 'function', '61', 'A');
INSERT INTO `hgc_authright` VALUES ('44', '2', '0', null, '1', 'function', '62', 'A');
INSERT INTO `hgc_authright` VALUES ('45', '2', '0', null, '1', 'function', '36', 'A');
INSERT INTO `hgc_authright` VALUES ('46', '2', '0', null, '1', 'function', '37', 'A');
INSERT INTO `hgc_authright` VALUES ('47', '2', '0', null, '1', 'function', '126', 'A');
INSERT INTO `hgc_authright` VALUES ('48', '2', '0', null, '1', 'function', '101', 'A');
INSERT INTO `hgc_authright` VALUES ('49', '2', '0', null, '1', 'function', '102', 'A');
INSERT INTO `hgc_authright` VALUES ('50', '2', '0', null, '1', 'function', '103', 'A');
INSERT INTO `hgc_authright` VALUES ('51', '2', '0', null, '1', 'function', '98', 'A');
INSERT INTO `hgc_authright` VALUES ('52', '2', '0', null, '1', 'function', '99', 'A');
INSERT INTO `hgc_authright` VALUES ('53', '2', '0', null, '1', 'function', '100', 'A');
INSERT INTO `hgc_authright` VALUES ('54', '2', '0', null, '1', 'function', '46', 'A');
INSERT INTO `hgc_authright` VALUES ('55', '2', '0', null, '1', 'function', '47', 'A');
INSERT INTO `hgc_authright` VALUES ('56', '2', '0', null, '1', 'function', '133', 'A');
INSERT INTO `hgc_authright` VALUES ('57', '2', '0', null, '1', 'function', '57', 'A');
INSERT INTO `hgc_authright` VALUES ('58', '2', '0', null, '1', 'function', '58', 'A');
INSERT INTO `hgc_authright` VALUES ('59', '2', '0', null, '1', 'function', '59', 'A');
INSERT INTO `hgc_authright` VALUES ('60', '2', '0', null, '1', 'function', '48', 'A');
INSERT INTO `hgc_authright` VALUES ('61', '2', '0', null, '1', 'function', '49', 'A');
INSERT INTO `hgc_authright` VALUES ('62', '2', '0', null, '1', 'function', '50', 'A');
INSERT INTO `hgc_authright` VALUES ('63', '2', '0', null, '1', 'function', '51', 'A');
INSERT INTO `hgc_authright` VALUES ('64', '2', '0', null, '1', 'function', '52', 'A');
INSERT INTO `hgc_authright` VALUES ('65', '2', '0', null, '1', 'function', '131', 'A');
INSERT INTO `hgc_authright` VALUES ('66', '2', '0', null, '1', 'function', '132', 'A');
INSERT INTO `hgc_authright` VALUES ('67', '2', '0', null, '1', 'function', '134', 'A');
INSERT INTO `hgc_authright` VALUES ('68', '2', '0', null, '1', 'function', '135', 'A');
INSERT INTO `hgc_authright` VALUES ('69', '2', '0', null, '1', 'function', '137', 'A');
INSERT INTO `hgc_authright` VALUES ('70', '2', '0', null, '1', 'function', '138', 'A');
INSERT INTO `hgc_authright` VALUES ('71', '2', '0', null, '1', 'function', '136', 'A');
INSERT INTO `hgc_authright` VALUES ('72', '2', '0', null, '1', 'function', '53', 'A');
INSERT INTO `hgc_authright` VALUES ('73', '2', '0', null, '1', 'function', '54', 'A');
INSERT INTO `hgc_authright` VALUES ('74', '2', '0', null, '1', 'function', '55', 'A');
INSERT INTO `hgc_authright` VALUES ('75', '2', '0', null, '1', 'function', '56', 'A');
INSERT INTO `hgc_authright` VALUES ('76', '2', '0', null, '1', 'function', '63', 'A');
INSERT INTO `hgc_authright` VALUES ('77', '2', '0', null, '1', 'function', '64', 'A');
INSERT INTO `hgc_authright` VALUES ('78', '2', '0', null, '1', 'function', '65', 'A');
INSERT INTO `hgc_authright` VALUES ('79', '2', '0', null, '1', 'function', '66', 'A');
INSERT INTO `hgc_authright` VALUES ('80', '2', '0', null, '1', 'function', '89', 'A');
INSERT INTO `hgc_authright` VALUES ('81', '2', '0', null, '1', 'function', '90', 'A');
INSERT INTO `hgc_authright` VALUES ('82', '2', '0', null, '1', 'function', '91', 'A');
INSERT INTO `hgc_authright` VALUES ('83', '2', '0', null, '1', 'function', '77', 'A');
INSERT INTO `hgc_authright` VALUES ('84', '2', '0', null, '1', 'function', '78', 'A');
INSERT INTO `hgc_authright` VALUES ('85', '2', '0', null, '1', 'function', '79', 'A');
INSERT INTO `hgc_authright` VALUES ('86', '2', '0', null, '1', 'function', '80', 'A');
INSERT INTO `hgc_authright` VALUES ('87', '2', '0', null, '1', 'function', '81', 'A');
INSERT INTO `hgc_authright` VALUES ('88', '2', '0', null, '1', 'function', '82', 'A');
INSERT INTO `hgc_authright` VALUES ('89', '2', '0', null, '1', 'function', '83', 'A');
INSERT INTO `hgc_authright` VALUES ('90', '2', '0', null, '1', 'function', '84', 'A');
INSERT INTO `hgc_authright` VALUES ('91', '2', '0', null, '1', 'function', '85', 'A');
INSERT INTO `hgc_authright` VALUES ('92', '2', '0', null, '1', 'function', '86', 'A');
INSERT INTO `hgc_authright` VALUES ('93', '2', '0', null, '1', 'function', '67', 'A');
INSERT INTO `hgc_authright` VALUES ('94', '2', '0', null, '1', 'function', '68', 'A');
INSERT INTO `hgc_authright` VALUES ('95', '2', '0', null, '1', 'function', '69', 'A');
INSERT INTO `hgc_authright` VALUES ('96', '2', '0', null, '1', 'function', '87', 'A');
INSERT INTO `hgc_authright` VALUES ('97', '2', '0', null, '1', 'function', '88', 'A');
INSERT INTO `hgc_authright` VALUES ('98', '2', '0', null, '1', 'function', '70', 'A');
INSERT INTO `hgc_authright` VALUES ('99', '2', '0', null, '1', 'function', '71', 'A');
INSERT INTO `hgc_authright` VALUES ('100', '2', '0', null, '1', 'function', '72', 'A');
INSERT INTO `hgc_authright` VALUES ('101', '2', '0', null, '1', 'function', '73', 'A');
INSERT INTO `hgc_authright` VALUES ('102', '2', '0', null, '1', 'function', '74', 'A');
INSERT INTO `hgc_authright` VALUES ('103', '2', '0', null, '1', 'function', '75', 'A');
INSERT INTO `hgc_authright` VALUES ('104', '2', '0', null, '1', 'function', '76', 'A');
INSERT INTO `hgc_authright` VALUES ('105', '2', '0', null, '1', 'function', '92', 'A');
INSERT INTO `hgc_authright` VALUES ('106', '2', '0', null, '1', 'function', '93', 'A');
INSERT INTO `hgc_authright` VALUES ('107', '2', '0', null, '1', 'function', '94', 'A');
INSERT INTO `hgc_authright` VALUES ('108', '2', '0', null, '1', 'function', '95', 'A');
INSERT INTO `hgc_authright` VALUES ('109', '2', '0', null, '1', 'function', '96', 'A');
INSERT INTO `hgc_authright` VALUES ('110', '2', '0', null, '1', 'function', '97', 'A');
INSERT INTO `hgc_authright` VALUES ('111', '2', '0', null, '1', 'function', '107', 'A');
INSERT INTO `hgc_authright` VALUES ('112', '2', '0', null, '1', 'function', '108', 'A');
INSERT INTO `hgc_authright` VALUES ('113', '2', '0', null, '1', 'function', '109', 'A');
INSERT INTO `hgc_authright` VALUES ('114', '2', '0', null, '1', 'function', '110', 'A');
INSERT INTO `hgc_authright` VALUES ('115', '2', '0', null, '1', 'function', '111', 'A');
INSERT INTO `hgc_authright` VALUES ('116', '2', '0', null, '1', 'function', '104', 'A');
INSERT INTO `hgc_authright` VALUES ('117', '2', '0', null, '1', 'function', '105', 'A');
INSERT INTO `hgc_authright` VALUES ('118', '2', '0', null, '1', 'function', '106', 'A');
INSERT INTO `hgc_authright` VALUES ('119', '2', '0', null, '1', 'function', '116', 'A');
INSERT INTO `hgc_authright` VALUES ('120', '2', '0', null, '1', 'function', '117', 'A');
INSERT INTO `hgc_authright` VALUES ('121', '2', '0', null, '1', 'function', '118', 'A');
INSERT INTO `hgc_authright` VALUES ('122', '2', '0', null, '1', 'function', '114', 'A');
INSERT INTO `hgc_authright` VALUES ('123', '2', '0', null, '1', 'function', '115', 'A');
INSERT INTO `hgc_authright` VALUES ('124', '2', '0', null, '1', 'function', '119', 'A');
INSERT INTO `hgc_authright` VALUES ('125', '2', '0', null, '1', 'function', '120', 'A');
INSERT INTO `hgc_authright` VALUES ('126', '2', '0', null, '1', 'function', '121', 'A');
INSERT INTO `hgc_authright` VALUES ('127', '2', '0', null, '1', 'function', '112', 'A');
INSERT INTO `hgc_authright` VALUES ('128', '2', '0', null, '1', 'function', '113', 'A');
INSERT INTO `hgc_authright` VALUES ('129', '2', '0', null, '1', 'function', '122', 'A');
INSERT INTO `hgc_authright` VALUES ('130', '2', '0', null, '1', 'function', '123', 'A');
INSERT INTO `hgc_authright` VALUES ('131', '6', '0', null, '2', 'function', '9', 'A');
INSERT INTO `hgc_authright` VALUES ('132', '6', '0', null, '2', 'function', '19', 'A');
INSERT INTO `hgc_authright` VALUES ('133', '6', '0', null, '2', 'function', '20', 'A');
INSERT INTO `hgc_authright` VALUES ('134', '6', '0', null, '2', 'function', '139', 'A');
INSERT INTO `hgc_authright` VALUES ('135', '6', '0', null, '2', 'function', '10', 'A');
INSERT INTO `hgc_authright` VALUES ('136', '6', '0', null, '2', 'function', '18', 'A');
INSERT INTO `hgc_authright` VALUES ('137', '6', '0', null, '2', 'function', '12', 'A');
INSERT INTO `hgc_authright` VALUES ('138', '6', '0', null, '2', 'function', '23', 'A');
INSERT INTO `hgc_authright` VALUES ('139', '6', '0', null, '2', 'function', '24', 'A');
INSERT INTO `hgc_authright` VALUES ('140', '6', '0', null, '2', 'function', '25', 'A');
INSERT INTO `hgc_authright` VALUES ('141', '6', '0', null, '2', 'function', '41', 'A');
INSERT INTO `hgc_authright` VALUES ('142', '6', '0', null, '2', 'function', '42', 'A');
INSERT INTO `hgc_authright` VALUES ('143', '6', '0', null, '2', 'function', '43', 'A');
INSERT INTO `hgc_authright` VALUES ('144', '6', '0', null, '2', 'function', '44', 'A');
INSERT INTO `hgc_authright` VALUES ('145', '6', '0', null, '2', 'function', '45', 'A');
INSERT INTO `hgc_authright` VALUES ('146', '6', '0', null, '2', 'function', '15', 'A');
INSERT INTO `hgc_authright` VALUES ('147', '6', '0', null, '2', 'function', '16', 'A');
INSERT INTO `hgc_authright` VALUES ('148', '6', '0', null, '2', 'function', '22', 'A');
INSERT INTO `hgc_authright` VALUES ('149', '6', '0', null, '2', 'function', '11', 'A');
INSERT INTO `hgc_authright` VALUES ('150', '6', '0', null, '2', 'function', '13', 'A');
INSERT INTO `hgc_authright` VALUES ('151', '6', '0', null, '2', 'function', '21', 'A');
INSERT INTO `hgc_authright` VALUES ('152', '6', '0', null, '2', 'function', '26', 'A');
INSERT INTO `hgc_authright` VALUES ('153', '6', '0', null, '2', 'function', '27', 'A');
INSERT INTO `hgc_authright` VALUES ('154', '6', '0', null, '2', 'function', '28', 'A');
INSERT INTO `hgc_authright` VALUES ('155', '6', '0', null, '2', 'function', '29', 'A');
INSERT INTO `hgc_authright` VALUES ('156', '6', '0', null, '2', 'function', '30', 'A');
INSERT INTO `hgc_authright` VALUES ('157', '6', '0', null, '2', 'function', '31', 'A');
INSERT INTO `hgc_authright` VALUES ('158', '6', '0', null, '2', 'function', '32', 'A');
INSERT INTO `hgc_authright` VALUES ('159', '6', '0', null, '2', 'function', '127', 'A');
INSERT INTO `hgc_authright` VALUES ('160', '6', '0', null, '2', 'function', '125', 'A');
INSERT INTO `hgc_authright` VALUES ('161', '6', '0', null, '2', 'function', '33', 'A');
INSERT INTO `hgc_authright` VALUES ('162', '6', '0', null, '2', 'function', '34', 'A');
INSERT INTO `hgc_authright` VALUES ('163', '6', '0', null, '2', 'function', '35', 'A');
INSERT INTO `hgc_authright` VALUES ('164', '6', '0', null, '2', 'function', '38', 'A');
INSERT INTO `hgc_authright` VALUES ('165', '6', '0', null, '2', 'function', '128', 'A');
INSERT INTO `hgc_authright` VALUES ('166', '6', '0', null, '2', 'function', '129', 'A');
INSERT INTO `hgc_authright` VALUES ('167', '6', '0', null, '2', 'function', '130', 'A');
INSERT INTO `hgc_authright` VALUES ('168', '6', '0', null, '2', 'function', '17', 'A');
INSERT INTO `hgc_authright` VALUES ('169', '6', '0', null, '2', 'function', '14', 'A');
INSERT INTO `hgc_authright` VALUES ('170', '6', '0', null, '2', 'function', '39', 'A');
INSERT INTO `hgc_authright` VALUES ('171', '6', '0', null, '2', 'function', '40', 'A');
INSERT INTO `hgc_authright` VALUES ('172', '6', '0', null, '2', 'function', '60', 'A');
INSERT INTO `hgc_authright` VALUES ('173', '6', '0', null, '2', 'function', '61', 'A');
INSERT INTO `hgc_authright` VALUES ('174', '6', '0', null, '2', 'function', '62', 'A');
INSERT INTO `hgc_authright` VALUES ('175', '6', '0', null, '2', 'function', '36', 'A');
INSERT INTO `hgc_authright` VALUES ('176', '6', '0', null, '2', 'function', '37', 'A');
INSERT INTO `hgc_authright` VALUES ('177', '6', '0', null, '2', 'function', '126', 'A');
INSERT INTO `hgc_authright` VALUES ('178', '6', '0', null, '2', 'function', '101', 'A');
INSERT INTO `hgc_authright` VALUES ('179', '6', '0', null, '2', 'function', '102', 'A');
INSERT INTO `hgc_authright` VALUES ('180', '6', '0', null, '2', 'function', '103', 'A');
INSERT INTO `hgc_authright` VALUES ('181', '6', '0', null, '2', 'function', '98', 'A');
INSERT INTO `hgc_authright` VALUES ('182', '6', '0', null, '2', 'function', '99', 'A');
INSERT INTO `hgc_authright` VALUES ('183', '6', '0', null, '2', 'function', '100', 'A');
INSERT INTO `hgc_authright` VALUES ('184', '6', '0', null, '2', 'function', '46', 'A');
INSERT INTO `hgc_authright` VALUES ('185', '6', '0', null, '2', 'function', '47', 'A');
INSERT INTO `hgc_authright` VALUES ('186', '6', '0', null, '2', 'function', '133', 'A');
INSERT INTO `hgc_authright` VALUES ('187', '6', '0', null, '2', 'function', '57', 'A');
INSERT INTO `hgc_authright` VALUES ('188', '6', '0', null, '2', 'function', '58', 'A');
INSERT INTO `hgc_authright` VALUES ('189', '6', '0', null, '2', 'function', '59', 'A');
INSERT INTO `hgc_authright` VALUES ('190', '6', '0', null, '2', 'function', '48', 'A');
INSERT INTO `hgc_authright` VALUES ('191', '6', '0', null, '2', 'function', '49', 'A');
INSERT INTO `hgc_authright` VALUES ('192', '6', '0', null, '2', 'function', '50', 'A');
INSERT INTO `hgc_authright` VALUES ('193', '6', '0', null, '2', 'function', '51', 'A');
INSERT INTO `hgc_authright` VALUES ('194', '6', '0', null, '2', 'function', '52', 'A');
INSERT INTO `hgc_authright` VALUES ('195', '6', '0', null, '2', 'function', '131', 'A');
INSERT INTO `hgc_authright` VALUES ('196', '6', '0', null, '2', 'function', '132', 'A');
INSERT INTO `hgc_authright` VALUES ('197', '6', '0', null, '2', 'function', '134', 'A');
INSERT INTO `hgc_authright` VALUES ('198', '6', '0', null, '2', 'function', '135', 'A');
INSERT INTO `hgc_authright` VALUES ('199', '6', '0', null, '2', 'function', '137', 'A');
INSERT INTO `hgc_authright` VALUES ('200', '6', '0', null, '2', 'function', '138', 'A');
INSERT INTO `hgc_authright` VALUES ('201', '6', '0', null, '2', 'function', '136', 'A');
INSERT INTO `hgc_authright` VALUES ('202', '6', '0', null, '2', 'function', '53', 'A');
INSERT INTO `hgc_authright` VALUES ('203', '6', '0', null, '2', 'function', '54', 'A');
INSERT INTO `hgc_authright` VALUES ('204', '6', '0', null, '2', 'function', '55', 'A');
INSERT INTO `hgc_authright` VALUES ('205', '6', '0', null, '2', 'function', '56', 'A');
INSERT INTO `hgc_authright` VALUES ('206', '6', '0', null, '2', 'function', '63', 'A');
INSERT INTO `hgc_authright` VALUES ('207', '6', '0', null, '2', 'function', '64', 'A');
INSERT INTO `hgc_authright` VALUES ('208', '6', '0', null, '2', 'function', '65', 'A');
INSERT INTO `hgc_authright` VALUES ('209', '6', '0', null, '2', 'function', '66', 'A');
INSERT INTO `hgc_authright` VALUES ('210', '6', '0', null, '2', 'function', '89', 'A');
INSERT INTO `hgc_authright` VALUES ('211', '6', '0', null, '2', 'function', '90', 'A');
INSERT INTO `hgc_authright` VALUES ('212', '6', '0', null, '2', 'function', '91', 'A');
INSERT INTO `hgc_authright` VALUES ('213', '6', '0', null, '2', 'function', '77', 'A');
INSERT INTO `hgc_authright` VALUES ('214', '6', '0', null, '2', 'function', '78', 'A');
INSERT INTO `hgc_authright` VALUES ('215', '6', '0', null, '2', 'function', '79', 'A');
INSERT INTO `hgc_authright` VALUES ('216', '6', '0', null, '2', 'function', '80', 'A');
INSERT INTO `hgc_authright` VALUES ('217', '6', '0', null, '2', 'function', '81', 'A');
INSERT INTO `hgc_authright` VALUES ('218', '6', '0', null, '2', 'function', '82', 'A');
INSERT INTO `hgc_authright` VALUES ('219', '6', '0', null, '2', 'function', '83', 'A');
INSERT INTO `hgc_authright` VALUES ('220', '6', '0', null, '2', 'function', '84', 'A');
INSERT INTO `hgc_authright` VALUES ('221', '6', '0', null, '2', 'function', '85', 'A');
INSERT INTO `hgc_authright` VALUES ('222', '6', '0', null, '2', 'function', '86', 'A');
INSERT INTO `hgc_authright` VALUES ('223', '6', '0', null, '2', 'function', '67', 'A');
INSERT INTO `hgc_authright` VALUES ('224', '6', '0', null, '2', 'function', '68', 'A');
INSERT INTO `hgc_authright` VALUES ('225', '6', '0', null, '2', 'function', '69', 'A');
INSERT INTO `hgc_authright` VALUES ('226', '6', '0', null, '2', 'function', '87', 'A');
INSERT INTO `hgc_authright` VALUES ('227', '6', '0', null, '2', 'function', '88', 'A');
INSERT INTO `hgc_authright` VALUES ('228', '6', '0', null, '2', 'function', '70', 'A');
INSERT INTO `hgc_authright` VALUES ('229', '6', '0', null, '2', 'function', '71', 'A');
INSERT INTO `hgc_authright` VALUES ('230', '6', '0', null, '2', 'function', '72', 'A');
INSERT INTO `hgc_authright` VALUES ('231', '6', '0', null, '2', 'function', '73', 'A');
INSERT INTO `hgc_authright` VALUES ('232', '6', '0', null, '2', 'function', '74', 'A');
INSERT INTO `hgc_authright` VALUES ('233', '6', '0', null, '2', 'function', '75', 'A');
INSERT INTO `hgc_authright` VALUES ('234', '6', '0', null, '2', 'function', '76', 'A');
INSERT INTO `hgc_authright` VALUES ('235', '6', '0', null, '2', 'function', '92', 'A');
INSERT INTO `hgc_authright` VALUES ('236', '6', '0', null, '2', 'function', '93', 'A');
INSERT INTO `hgc_authright` VALUES ('237', '6', '0', null, '2', 'function', '94', 'A');
INSERT INTO `hgc_authright` VALUES ('238', '6', '0', null, '2', 'function', '95', 'A');
INSERT INTO `hgc_authright` VALUES ('239', '6', '0', null, '2', 'function', '96', 'A');
INSERT INTO `hgc_authright` VALUES ('240', '6', '0', null, '2', 'function', '97', 'A');
INSERT INTO `hgc_authright` VALUES ('241', '6', '0', null, '2', 'function', '107', 'A');
INSERT INTO `hgc_authright` VALUES ('242', '6', '0', null, '2', 'function', '108', 'A');
INSERT INTO `hgc_authright` VALUES ('243', '6', '0', null, '2', 'function', '109', 'A');
INSERT INTO `hgc_authright` VALUES ('244', '6', '0', null, '2', 'function', '110', 'A');
INSERT INTO `hgc_authright` VALUES ('245', '6', '0', null, '2', 'function', '111', 'A');
INSERT INTO `hgc_authright` VALUES ('246', '6', '0', null, '2', 'function', '104', 'A');
INSERT INTO `hgc_authright` VALUES ('247', '6', '0', null, '2', 'function', '105', 'A');
INSERT INTO `hgc_authright` VALUES ('248', '6', '0', null, '2', 'function', '106', 'A');
INSERT INTO `hgc_authright` VALUES ('249', '6', '0', null, '2', 'function', '116', 'A');
INSERT INTO `hgc_authright` VALUES ('250', '6', '0', null, '2', 'function', '117', 'A');
INSERT INTO `hgc_authright` VALUES ('251', '6', '0', null, '2', 'function', '118', 'A');
INSERT INTO `hgc_authright` VALUES ('252', '6', '0', null, '2', 'function', '114', 'A');
INSERT INTO `hgc_authright` VALUES ('253', '6', '0', null, '2', 'function', '115', 'A');
INSERT INTO `hgc_authright` VALUES ('254', '6', '0', null, '2', 'function', '119', 'A');
INSERT INTO `hgc_authright` VALUES ('255', '6', '0', null, '2', 'function', '120', 'A');
INSERT INTO `hgc_authright` VALUES ('256', '6', '0', null, '2', 'function', '121', 'A');
INSERT INTO `hgc_authright` VALUES ('257', '6', '0', null, '2', 'function', '112', 'A');
INSERT INTO `hgc_authright` VALUES ('258', '6', '0', null, '2', 'function', '113', 'A');
INSERT INTO `hgc_authright` VALUES ('259', '6', '0', null, '2', 'function', '122', 'A');
INSERT INTO `hgc_authright` VALUES ('260', '6', '0', null, '2', 'function', '123', 'A');

-- ----------------------------
-- Table structure for `hgc_bank`
-- ----------------------------
DROP TABLE IF EXISTS `hgc_bank`;
CREATE TABLE `hgc_bank` (
  `bnk_id` int(11) NOT NULL AUTO_INCREMENT,
  `bnk_code` varchar(11) NOT NULL,
  `bnk_placecode` varchar(11) NOT NULL,
  `bnk_name` varchar(50) NOT NULL,
  `bnk_branch` varchar(50) NOT NULL,
  `bnk_addedby` varchar(45) DEFAULT 'System',
  `bnk_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`bnk_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4007 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hgc_bank
-- ----------------------------
INSERT INTO `hgc_bank` VALUES ('42', '7010', '001', 'Bank of Ceylon', 'City Office', 'System', null);
INSERT INTO `hgc_bank` VALUES ('321', '7010', '657', 'Bank of Ceylon', 'Agalawatta', 'System', null);
INSERT INTO `hgc_bank` VALUES ('322', '7010', '754', 'Bank of Ceylon', 'Ahungalla', 'System', null);
INSERT INTO `hgc_bank` VALUES ('323', '7010', '590', 'Bank of Ceylon', 'Akkaraipattu', 'System', null);
INSERT INTO `hgc_bank` VALUES ('324', '7010', '613', 'Bank of Ceylon', 'Akuressa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('325', '7010', '498', 'Bank of Ceylon', 'Alawwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('326', '7010', '680', 'Bank of Ceylon', 'Aluthgama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('327', '7010', '047', 'Bank of Ceylon', 'Ambalangoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('328', '7010', '537', 'Bank of Ceylon', 'Ambalantota', 'System', null);
INSERT INTO `hgc_bank` VALUES ('329', '7010', '021', 'Bank of Ceylon', 'Ampara', 'System', null);
INSERT INTO `hgc_bank` VALUES ('330', '7010', '548', 'Bank of Ceylon', 'Anamaduwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('331', '7010', '494', 'Bank of Ceylon', 'Andiambalama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('332', '7010', '022', 'Bank of Ceylon', 'Anuradhapura', 'System', null);
INSERT INTO `hgc_bank` VALUES ('333', '7010', '551', 'Bank of Ceylon', 'Anuradhapura Bazzar', 'System', null);
INSERT INTO `hgc_bank` VALUES ('334', '7010', '599', 'Bank of Ceylon', 'Aralaganwila', 'System', null);
INSERT INTO `hgc_bank` VALUES ('335', '7010', '566', 'Bank of Ceylon', 'Aranayake', 'System', null);
INSERT INTO `hgc_bank` VALUES ('336', '7010', '757', 'Bank of Ceylon', 'Athurugiriya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('337', '7010', '530', 'Bank of Ceylon', 'Avissawella', 'System', null);
INSERT INTO `hgc_bank` VALUES ('338', '7010', '401', 'Bank of Ceylon', 'Ayagama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('339', '7010', '540', 'Bank of Ceylon', 'Badalkumbura', 'System', null);
INSERT INTO `hgc_bank` VALUES ('340', '7010', '525', 'Bank of Ceylon', 'Baddegama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('341', '7010', '011', 'Bank of Ceylon', 'Badulla', 'System', null);
INSERT INTO `hgc_bank` VALUES ('342', '7010', '652', 'Bank of Ceylon', 'Bakamuna', 'System', null);
INSERT INTO `hgc_bank` VALUES ('343', '7010', '688', 'Bank of Ceylon', 'Balangoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('344', '7010', '320', 'Bank of Ceylon', 'Ballakatuwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('345', '7010', '037', 'Bank of Ceylon', 'Bambalapitiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('346', '7010', '665', 'Bank of Ceylon', 'Bandaragama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('347', '7010', '515', 'Bank of Ceylon', 'Bandarawela', 'System', null);
INSERT INTO `hgc_bank` VALUES ('348', '7010', '522', 'Bank of Ceylon', 'Batapola', 'System', null);
INSERT INTO `hgc_bank` VALUES ('349', '7010', '012', 'Bank of Ceylon', 'Batticaloa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('350', '7010', '102', 'Bank of Ceylon', 'Bentota', 'System', null);
INSERT INTO `hgc_bank` VALUES ('351', '7010', '058', 'Bank of Ceylon', 'Beruwela', 'System', null);
INSERT INTO `hgc_bank` VALUES ('352', '7010', '579', 'Bank of Ceylon', 'Bibile', 'System', null);
INSERT INTO `hgc_bank` VALUES ('353', '7010', '554', 'Bank of Ceylon', 'Bingiriya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('354', '7010', '732', 'Bank of Ceylon', 'Biyagama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('355', '7010', '038', 'Bank of Ceylon', 'Borella', 'System', null);
INSERT INTO `hgc_bank` VALUES ('356', '7010', '668', 'Bank of Ceylon', 'Borella 2nd Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('357', '7010', '673', 'Bank of Ceylon', 'Bulathsinhala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('358', '7010', '560', 'Bank of Ceylon', 'Buttala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('359', '7010', '573', 'Bank of Ceylon', 'Central Bus Stand', 'System', null);
INSERT INTO `hgc_bank` VALUES ('360', '7010', '015', 'Bank of Ceylon', 'Central Office', 'System', null);
INSERT INTO `hgc_bank` VALUES ('361', '7010', '672', 'Bank of Ceylon', 'Central Supermkt Bra', 'System', null);
INSERT INTO `hgc_bank` VALUES ('362', '7010', '501', 'Bank of Ceylon', 'Chavakachcheri', 'System', null);
INSERT INTO `hgc_bank` VALUES ('363', '7010', '630', 'Bank of Ceylon', 'Chenkalady', 'System', null);
INSERT INTO `hgc_bank` VALUES ('364', '7010', '020', 'Bank of Ceylon', 'Chilaw', 'System', null);
INSERT INTO `hgc_bank` VALUES ('365', '7010', '053', 'Bank of Ceylon', 'Chunnakam', 'System', null);
INSERT INTO `hgc_bank` VALUES ('367', '7010', '660', 'Bank of Ceylon', 'Corporate Barnch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('368', '7010', '576', 'Bank of Ceylon', 'Dambulla', 'System', null);
INSERT INTO `hgc_bank` VALUES ('369', '7010', '497', 'Bank of Ceylon', 'Dankotuwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('370', '7010', '686', 'Bank of Ceylon', 'Dehiattakandiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('371', '7010', '634', 'Bank of Ceylon', 'Dehiowita', 'System', null);
INSERT INTO `hgc_bank` VALUES ('372', '7010', '051', 'Bank of Ceylon', 'Dehiwela', 'System', null);
INSERT INTO `hgc_bank` VALUES ('373', '7010', '561', 'Bank of Ceylon', 'Dematagoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('374', '7010', '528', 'Bank of Ceylon', 'Deniyaya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('375', '7010', '642', 'Bank of Ceylon', 'Deraniyagala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('376', '7010', '504', 'Bank of Ceylon', 'Devinuwara', 'System', null);
INSERT INTO `hgc_bank` VALUES ('377', '7010', '563', 'Bank of Ceylon', 'Dharga Town', 'System', null);
INSERT INTO `hgc_bank` VALUES ('378', '7010', '592', 'Bank of Ceylon', 'Dickwella', 'System', null);
INSERT INTO `hgc_bank` VALUES ('379', '7010', '273', 'Bank of Ceylon', 'Digana', 'System', null);
INSERT INTO `hgc_bank` VALUES ('380', '7010', '433', 'Bank of Ceylon', 'Divulapitiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('381', '7010', '260', 'Bank of Ceylon', 'Diyatalawa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('382', '7010', '293', 'Bank of Ceylon', 'Dodangoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('383', '7010', '580', 'Bank of Ceylon', 'Dummalasuriya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('384', '7010', '057', 'Bank of Ceylon', 'Eheliyagoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('385', '7010', '619', 'Bank of Ceylon', 'Elpitiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('386', '7010', '535', 'Bank of Ceylon', 'Embilipitiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('387', '7010', '692', 'Bank of Ceylon', 'Eppawala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('388', '7010', '476', 'Bank of Ceylon', 'Ettampitiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('389', '7010', '060', 'Bank of Ceylon', 'Fifth City', 'System', null);
INSERT INTO `hgc_bank` VALUES ('390', '7010', '615', 'Bank of Ceylon', 'Galagedara', 'System', null);
INSERT INTO `hgc_bank` VALUES ('391', '7010', '101', 'Bank of Ceylon', 'Galaha', 'System', null);
INSERT INTO `hgc_bank` VALUES ('392', '7010', '122', 'Bank of Ceylon', 'Galenbindunuwewa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('393', '7010', '432', 'Bank of Ceylon', 'Galewela', 'System', null);
INSERT INTO `hgc_bank` VALUES ('394', '7010', '549', 'Bank of Ceylon', 'Galgamuwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('395', '7010', '653', 'Bank of Ceylon', 'Galkiriyagama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('396', '7010', '003', 'Bank of Ceylon', 'Galle', 'System', null);
INSERT INTO `hgc_bank` VALUES ('397', '7010', '089', 'Bank of Ceylon', 'Galle Bazzar', 'System', null);
INSERT INTO `hgc_bank` VALUES ('398', '7010', '514', 'Bank of Ceylon', 'Galnewa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('399', '7010', '045', 'Bank of Ceylon', 'Gampaha', 'System', null);
INSERT INTO `hgc_bank` VALUES ('400', '7010', '575', 'Bank of Ceylon', 'Gampola', 'System', null);
INSERT INTO `hgc_bank` VALUES ('401', '7010', '524', 'Bank of Ceylon', 'Geli Oya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('402', '7010', '646', 'Bank of Ceylon', 'Ginigathhena', 'System', null);
INSERT INTO `hgc_bank` VALUES ('403', '7010', '669', 'Bank of Ceylon', 'Girandurukotte', 'System', null);
INSERT INTO `hgc_bank` VALUES ('404', '7010', '553', 'Bank of Ceylon', 'Giriulla', 'System', null);
INSERT INTO `hgc_bank` VALUES ('405', '7010', '628', 'Bank of Ceylon', 'Grandpass', 'System', null);
INSERT INTO `hgc_bank` VALUES ('406', '7010', '691', 'Bank of Ceylon', 'Hakmana', 'System', null);
INSERT INTO `hgc_bank` VALUES ('407', '7010', '463', 'Bank of Ceylon', 'Haldummulla', 'System', null);
INSERT INTO `hgc_bank` VALUES ('408', '7010', '085', 'Bank of Ceylon', 'Hambantota', 'System', null);
INSERT INTO `hgc_bank` VALUES ('409', '7010', '741', 'Bank of Ceylon', 'Hanwella', 'System', null);
INSERT INTO `hgc_bank` VALUES ('410', '7010', '035', 'Bank of Ceylon', 'Haputale', 'System', null);
INSERT INTO `hgc_bank` VALUES ('411', '7010', '365', 'Bank of Ceylon', 'Hasalaka', 'System', null);
INSERT INTO `hgc_bank` VALUES ('412', '7010', '040', 'Bank of Ceylon', 'Hatton', 'System', null);
INSERT INTO `hgc_bank` VALUES ('413', '7010', '337', 'Bank of Ceylon', 'Helboda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('414', '7010', '570', 'Bank of Ceylon', 'Hettipola', 'System', null);
INSERT INTO `hgc_bank` VALUES ('415', '7010', '609', 'Bank of Ceylon', 'Hikkaduwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('416', '7010', '601', 'Bank of Ceylon', 'Hingurakgoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('417', '7010', '509', 'Bank of Ceylon', 'Hingurana', 'System', null);
INSERT INTO `hgc_bank` VALUES ('418', '7010', '569', 'Bank of Ceylon', 'Hiripitiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('419', '7010', '568', 'Bank of Ceylon', 'Homagama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('420', '7010', '054', 'Bank of Ceylon', 'Horana', 'System', null);
INSERT INTO `hgc_bank` VALUES ('421', '7010', '217', 'Bank of Ceylon', 'Horowpothana', 'System', null);
INSERT INTO `hgc_bank` VALUES ('422', '7010', '032', 'Bank of Ceylon', 'Hulftsdorp', 'System', null);
INSERT INTO `hgc_bank` VALUES ('423', '7010', '521', 'Bank of Ceylon', 'Hyde Park', 'System', null);
INSERT INTO `hgc_bank` VALUES ('424', '7010', '061', 'Bank of Ceylon', 'Idama - Moratuwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('425', '7010', '135', 'Bank of Ceylon', 'Imaduwa / Kodagoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('426', '7010', '604', 'Bank of Ceylon', 'Ingiriya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('427', '7010', '087', 'Bank of Ceylon', 'International Division', 'System', null);
INSERT INTO `hgc_bank` VALUES ('428', '7010', '236', 'Bank of Ceylon', 'Ipalogama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('429', '7010', '039', 'Bank of Ceylon', 'Ja-Ela', 'System', null);
INSERT INTO `hgc_bank` VALUES ('430', '7010', '005', 'Bank of Ceylon', 'Jaffna', 'System', null);
INSERT INTO `hgc_bank` VALUES ('431', '7010', '500', 'Bank of Ceylon', 'Jaffna Second Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('432', '7010', '600', 'Bank of Ceylon', 'Jayanthipura', 'System', null);
INSERT INTO `hgc_bank` VALUES ('433', '7010', '621', 'Bank of Ceylon', 'Kabithigollawa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('434', '7010', '059', 'Bank of Ceylon', 'Kadawatha', 'System', null);
INSERT INTO `hgc_bank` VALUES ('435', '7010', '502', 'Bank of Ceylon', 'Kaduruwela', 'System', null);
INSERT INTO `hgc_bank` VALUES ('436', '7010', '608', 'Bank of Ceylon', 'Kaduwela', 'System', null);
INSERT INTO `hgc_bank` VALUES ('437', '7010', '622', 'Bank of Ceylon', 'Kahatagasdigiliya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('438', '7010', '507', 'Bank of Ceylon', 'Kahawatte', 'System', null);
INSERT INTO `hgc_bank` VALUES ('439', '7010', '645', 'Bank of Ceylon', 'Kalawana', 'System', null);
INSERT INTO `hgc_bank` VALUES ('440', '7010', '510', 'Bank of Ceylon', 'Kalmunai', 'System', null);
INSERT INTO `hgc_bank` VALUES ('441', '7010', '589', 'Bank of Ceylon', 'Kalpitiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('442', '7010', '016', 'Bank of Ceylon', 'Kalutara', 'System', null);
INSERT INTO `hgc_bank` VALUES ('443', '7010', '611', 'Bank of Ceylon', 'Kaluwanchikudy', 'System', null);
INSERT INTO `hgc_bank` VALUES ('444', '7010', '529', 'Bank of Ceylon', 'Kamburupitiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('445', '7010', '633', 'Bank of Ceylon', 'Kandapola', 'System', null);
INSERT INTO `hgc_bank` VALUES ('446', '7010', '002', 'Bank of Ceylon', 'Kandy', 'System', null);
INSERT INTO `hgc_bank` VALUES ('447', '7010', '649', 'Bank of Ceylon', 'Kandy 2nd City', 'System', null);
INSERT INTO `hgc_bank` VALUES ('448', '7010', '605', 'Bank of Ceylon', 'Kankesanthural', 'System', null);
INSERT INTO `hgc_bank` VALUES ('449', '7010', '623', 'Bank of Ceylon', 'Kantalai Bazzar', 'System', null);
INSERT INTO `hgc_bank` VALUES ('450', '7010', '384', 'Bank of Ceylon', 'Karametiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('451', '7010', '616', 'Bank of Ceylon', 'Kataragama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('452', '7010', '648', 'Bank of Ceylon', 'Kattankudy', 'System', null);
INSERT INTO `hgc_bank` VALUES ('453', '7010', '030', 'Bank of Ceylon', 'Katubedda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('454', '7010', '666', 'Bank of Ceylon', 'Katugastota', 'System', null);
INSERT INTO `hgc_bank` VALUES ('455', '7010', '658', 'Bank of Ceylon', 'Katunayake-IPZ', 'System', null);
INSERT INTO `hgc_bank` VALUES ('456', '7010', '063', 'Bank of Ceylon', 'Kayts', 'System', null);
INSERT INTO `hgc_bank` VALUES ('457', '7010', '027', 'Bank of Ceylon', 'Kegalle', 'System', null);
INSERT INTO `hgc_bank` VALUES ('458', '7010', '536', 'Bank of Ceylon', 'Kegalle Bazzar', 'System', null);
INSERT INTO `hgc_bank` VALUES ('459', '7010', '676', 'Bank of Ceylon', 'Kekirawa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('460', '7010', '093', 'Bank of Ceylon', 'Kilinochchi', 'System', null);
INSERT INTO `hgc_bank` VALUES ('461', '7010', '735', 'Bank of Ceylon', 'Kinniya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('462', '7010', '543', 'Bank of Ceylon', 'Kiribathgoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('463', '7010', '571', 'Bank of Ceylon', 'Kirindiwela', 'System', null);
INSERT INTO `hgc_bank` VALUES ('464', '7010', '172', 'Bank of Ceylon', 'Kobeigane', 'System', null);
INSERT INTO `hgc_bank` VALUES ('465', '7010', '034', 'Bank of Ceylon', 'Kollupitiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('466', '7010', '670', 'Bank of Ceylon', 'Kollupitiya 2nd Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('467', '7010', '595', 'Bank of Ceylon', 'Kolonnawa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('468', '7010', '629', 'Bank of Ceylon', 'Koslanda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('469', '7010', '663', 'Bank of Ceylon', 'Kotahena', 'System', null);
INSERT INTO `hgc_bank` VALUES ('470', '7010', '745', 'Bank of Ceylon', 'Kotiyakumbura', 'System', null);
INSERT INTO `hgc_bank` VALUES ('471', '7010', '052', 'Bank of Ceylon', 'Kuliyapitiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('472', '7010', '009', 'Bank of Ceylon', 'Kurunegala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('473', '7010', '513', 'Bank of Ceylon', 'Kurunegala Bazzar', 'System', null);
INSERT INTO `hgc_bank` VALUES ('474', '7010', '325', 'Bank of Ceylon', 'Kuruwita', 'System', null);
INSERT INTO `hgc_bank` VALUES ('475', '7010', '636', 'Bank of Ceylon', 'Lake House Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('476', '7010', '612', 'Bank of Ceylon', 'Lake View Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('477', '7010', '577', 'Bank of Ceylon', 'Lunugala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('478', '7010', '647', 'Bank of Ceylon', 'Lunuwatte', 'System', null);
INSERT INTO `hgc_bank` VALUES ('479', '7010', '544', 'Bank of Ceylon', 'Madampe', 'System', null);
INSERT INTO `hgc_bank` VALUES ('480', '7010', '654', 'Bank of Ceylon', 'Madatugama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('481', '7010', '581', 'Bank of Ceylon', 'Madawala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('482', '7010', '565', 'Bank of Ceylon', 'Madurankuliya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('483', '7010', '055', 'Bank of Ceylon', 'Maharagama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('484', '7010', '542', 'Bank of Ceylon', 'Mahiyangana', 'System', null);
INSERT INTO `hgc_bank` VALUES ('485', '7010', '564', 'Bank of Ceylon', 'Maho', 'System', null);
INSERT INTO `hgc_bank` VALUES ('486', '7010', '026', 'Bank of Ceylon', 'Main Street', 'System', null);
INSERT INTO `hgc_bank` VALUES ('487', '7010', '281', 'Bank of Ceylon', 'Manipay', 'System', null);
INSERT INTO `hgc_bank` VALUES ('488', '7010', '574', 'Bank of Ceylon', 'Mankulam', 'System', null);
INSERT INTO `hgc_bank` VALUES ('489', '7010', '046', 'Bank of Ceylon', 'Mannar', 'System', null);
INSERT INTO `hgc_bank` VALUES ('490', '7010', '041', 'Bank of Ceylon', 'Maradana', 'System', null);
INSERT INTO `hgc_bank` VALUES ('491', '7010', '506', 'Bank of Ceylon', 'Maskeliya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('492', '7010', '068', 'Bank of Ceylon', 'Matale', 'System', null);
INSERT INTO `hgc_bank` VALUES ('493', '7010', '024', 'Bank of Ceylon', 'Matara', 'System', null);
INSERT INTO `hgc_bank` VALUES ('494', '7010', '614', 'Bank of Ceylon', 'Matara Bazzar', 'System', null);
INSERT INTO `hgc_bank` VALUES ('495', '7010', '556', 'Bank of Ceylon', 'Matugama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('496', '7010', '559', 'Bank of Ceylon', 'Mawanella', 'System', null);
INSERT INTO `hgc_bank` VALUES ('497', '7010', '257', 'Bank of Ceylon', 'Mawathagama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('498', '7010', '238', 'Bank of Ceylon', 'Medagama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('499', '7010', '162', 'Bank of Ceylon', 'Medawachchiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('500', '7010', '641', 'Bank of Ceylon', 'Medirigiriya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('501', '7010', '728', 'Bank of Ceylon', 'Meegallewa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('502', '7010', '555', 'Bank of Ceylon', 'Melsiripura', 'System', null);
INSERT INTO `hgc_bank` VALUES ('503', '7010', '617', 'Bank of Ceylon', 'Metropolitan Br-York St', 'System', null);
INSERT INTO `hgc_bank` VALUES ('504', '7010', '618', 'Bank of Ceylon', 'Metropolitan Br-York St', 'System', null);
INSERT INTO `hgc_bank` VALUES ('505', '7010', '518', 'Bank of Ceylon', 'Middeniya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('506', '7010', '335', 'Bank of Ceylon', 'Mihintale', 'System', null);
INSERT INTO `hgc_bank` VALUES ('507', '7010', '593', 'Bank of Ceylon', 'Milagiriya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('508', '7010', '545', 'Bank of Ceylon', 'Minuwangoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('509', '7010', '088', 'Bank of Ceylon', 'Mirigama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('510', '7010', '082', 'Bank of Ceylon', 'Monaragala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('511', '7010', '424', 'Bank of Ceylon', 'Mulhalkelle', 'System', null);
INSERT INTO `hgc_bank` VALUES ('512', '7010', '511', 'Bank of Ceylon', 'Mullaitivu', 'System', null);
INSERT INTO `hgc_bank` VALUES ('513', '7010', '118', 'Bank of Ceylon', 'Muthur', 'System', null);
INSERT INTO `hgc_bank` VALUES ('514', '7010', '534', 'Bank of Ceylon', 'Narammala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('515', '7010', '050', 'Bank of Ceylon', 'Nattandiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('516', '7010', '092', 'Bank of Ceylon', 'Naula', 'System', null);
INSERT INTO `hgc_bank` VALUES ('517', '7010', '598', 'Bank of Ceylon', 'Nawalapitiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('518', '7010', '018', 'Bank of Ceylon', 'Negombo', 'System', null);
INSERT INTO `hgc_bank` VALUES ('519', '7010', '572', 'Bank of Ceylon', 'Negombo Bazzar', 'System', null);
INSERT INTO `hgc_bank` VALUES ('520', '7010', '638', 'Bank of Ceylon', 'Nelliady', 'System', null);
INSERT INTO `hgc_bank` VALUES ('521', '7010', '667', 'Bank of Ceylon', 'Neluwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('522', '7010', '547', 'Bank of Ceylon', 'Nikaweratiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('523', '7010', '223', 'Bank of Ceylon', 'Nildandahinna', 'System', null);
INSERT INTO `hgc_bank` VALUES ('524', '7010', '591', 'Bank of Ceylon', 'Nintavur', 'System', null);
INSERT INTO `hgc_bank` VALUES ('525', '7010', '675', 'Bank of Ceylon', 'Nittambuwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('526', '7010', '597', 'Bank of Ceylon', 'Nivitigala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('527', '7010', '656', 'Bank of Ceylon', 'Nochiyagama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('528', '7010', '049', 'Bank of Ceylon', 'Nugegoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('529', '7010', '029', 'Bank of Ceylon', 'Nuwara Eliya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('530', '7010', '250', 'Bank of Ceylon', 'Opatha', 'System', null);
INSERT INTO `hgc_bank` VALUES ('531', '7010', '100', 'Bank of Ceylon', 'Oruwala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('532', '7010', '127', 'Bank of Ceylon', 'Padavi Parakramapura', 'System', null);
INSERT INTO `hgc_bank` VALUES ('533', '7010', '492', 'Bank of Ceylon', 'Padiyapelella', 'System', null);
INSERT INTO `hgc_bank` VALUES ('534', '7010', '348', 'Bank of Ceylon', 'Padiyatalawa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('535', '7010', '678', 'Bank of Ceylon', 'Padukka', 'System', null);
INSERT INTO `hgc_bank` VALUES ('536', '7010', '640', 'Bank of Ceylon', 'Pallepola', 'System', null);
INSERT INTO `hgc_bank` VALUES ('537', '7010', '342', 'Bank of Ceylon', 'Pambahinna', 'System', null);
INSERT INTO `hgc_bank` VALUES ('538', '7010', '007', 'Bank of Ceylon', 'Panadura', 'System', null);
INSERT INTO `hgc_bank` VALUES ('539', '7010', '607', 'Bank of Ceylon', 'Panadura Bazzar', 'System', null);
INSERT INTO `hgc_bank` VALUES ('540', '7010', '546', 'Bank of Ceylon', 'Pannala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('541', '7010', '644', 'Bank of Ceylon', 'Parliamentary Complex', 'System', null);
INSERT INTO `hgc_bank` VALUES ('542', '7010', '503', 'Bank of Ceylon', 'Passara', 'System', null);
INSERT INTO `hgc_bank` VALUES ('543', '7010', '690', 'Bank of Ceylon', 'Pelawatta', 'System', null);
INSERT INTO `hgc_bank` VALUES ('544', '7010', '042', 'Bank of Ceylon', 'Peliyagoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('545', '7010', '683', 'Bank of Ceylon', 'Pelmadulla', 'System', null);
INSERT INTO `hgc_bank` VALUES ('546', '7010', '152', 'Bank of Ceylon', 'Pemaduwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('547', '7010', '588', 'Bank of Ceylon', 'Peradeniya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('548', '7010', '681', 'Bank of Ceylon', 'Personal Br.New HQ Bld.', 'System', null);
INSERT INTO `hgc_bank` VALUES ('549', '7010', '004', 'Bank of Ceylon', 'Pettah', 'System', null);
INSERT INTO `hgc_bank` VALUES ('550', '7010', '587', 'Bank of Ceylon', 'Pilimatalawa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('551', '7010', '736', 'Bank of Ceylon', 'Piliyandala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('552', '7010', '610', 'Bank of Ceylon', 'Pitigala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('553', '7010', '028', 'Bank of Ceylon', 'Point Pedro', 'System', null);
INSERT INTO `hgc_bank` VALUES ('554', '7010', '083', 'Bank of Ceylon', 'Polannaruwa New Town', 'System', null);
INSERT INTO `hgc_bank` VALUES ('555', '7010', '526', 'Bank of Ceylon', 'Polgahawela', 'System', null);
INSERT INTO `hgc_bank` VALUES ('556', '7010', '664', 'Bank of Ceylon', 'Pothuhera', 'System', null);
INSERT INTO `hgc_bank` VALUES ('557', '7010', '318', 'Bank of Ceylon', 'Potuvil', 'System', null);
INSERT INTO `hgc_bank` VALUES ('558', '7010', '025', 'Bank of Ceylon', 'Prince Street', 'System', null);
INSERT INTO `hgc_bank` VALUES ('559', '7010', '425', 'Bank of Ceylon', 'Pundaluoya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('560', '7010', '048', 'Bank of Ceylon', 'Puttalam', 'System', null);
INSERT INTO `hgc_bank` VALUES ('561', '7010', '582', 'Bank of Ceylon', 'Rabukkana', 'System', null);
INSERT INTO `hgc_bank` VALUES ('562', '7010', '097', 'Bank of Ceylon', 'Rajanganaya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('563', '7010', '594', 'Bank of Ceylon', 'Rakwana', 'System', null);
INSERT INTO `hgc_bank` VALUES ('564', '7010', '689', 'Bank of Ceylon', 'Ratmalana', 'System', null);
INSERT INTO `hgc_bank` VALUES ('565', '7010', '031', 'Bank of Ceylon', 'Ratnapura', 'System', null);
INSERT INTO `hgc_bank` VALUES ('566', '7010', '684', 'Bank of Ceylon', 'Ratnapura Bazzar', 'System', null);
INSERT INTO `hgc_bank` VALUES ('567', '7010', '639', 'Bank of Ceylon', 'Rattota', 'System', null);
INSERT INTO `hgc_bank` VALUES ('568', '7010', '627', 'Bank of Ceylon', 'Regent Street', 'System', null);
INSERT INTO `hgc_bank` VALUES ('569', '7010', '532', 'Bank of Ceylon', 'Ridigama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('570', '7010', '167', 'Bank of Ceylon', 'Rikillagaskada', 'System', null);
INSERT INTO `hgc_bank` VALUES ('571', '7010', '693', 'Bank of Ceylon', 'Ruhunu Campus Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('572', '7010', '585', 'Bank of Ceylon', 'Ruwanwella', 'System', null);
INSERT INTO `hgc_bank` VALUES ('573', '7010', '440', 'Bank of Ceylon', 'Sammanthurai', 'System', null);
INSERT INTO `hgc_bank` VALUES ('574', '7010', '010', 'Bank of Ceylon', 'Savings Dept.', 'System', null);
INSERT INTO `hgc_bank` VALUES ('575', '7010', '421', 'Bank of Ceylon', 'Seeduwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('576', '7010', '183', 'Bank of Ceylon', 'Sewagama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('577', '7010', '416', 'Bank of Ceylon', 'Siyambalanduwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('578', '7010', '098', 'Bank of Ceylon', 'Srawasthipura', 'System', null);
INSERT INTO `hgc_bank` VALUES ('579', '7010', '650', 'Bank of Ceylon', 'Talatuoya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('580', '7010', '531', 'Bank of Ceylon', 'Talawakelle', 'System', null);
INSERT INTO `hgc_bank` VALUES ('581', '7010', '596', 'Bank of Ceylon', 'Talgaswela', 'System', null);
INSERT INTO `hgc_bank` VALUES ('582', '7010', '655', 'Bank of Ceylon', 'Tambuttegama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('583', '7010', '056', 'Bank of Ceylon', 'Tangalle', 'System', null);
INSERT INTO `hgc_bank` VALUES ('584', '7010', '747', 'Bank of Ceylon', 'Taprobane', 'System', null);
INSERT INTO `hgc_bank` VALUES ('585', '7010', '322', 'Bank of Ceylon', 'Thanamalwila', 'System', null);
INSERT INTO `hgc_bank` VALUES ('586', '7010', '512', 'Bank of Ceylon', 'Thimbirigasaya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('587', '7010', '157', 'Bank of Ceylon', 'Tirappane', 'System', null);
INSERT INTO `hgc_bank` VALUES ('588', '7010', '538', 'Bank of Ceylon', 'Tissamaharama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('589', '7010', '453', 'Bank of Ceylon', 'Torrington Square', 'System', null);
INSERT INTO `hgc_bank` VALUES ('590', '7010', '006', 'Bank of Ceylon', 'Trincomalee', 'System', null);
INSERT INTO `hgc_bank` VALUES ('591', '7010', '624', 'Bank of Ceylon', 'Trincomalee Bazzar', 'System', null);
INSERT INTO `hgc_bank` VALUES ('592', '7010', '606', 'Bank of Ceylon', 'Udu Dumbara', 'System', null);
INSERT INTO `hgc_bank` VALUES ('593', '7010', '043', 'Bank of Ceylon', 'Union Place', 'System', null);
INSERT INTO `hgc_bank` VALUES ('594', '7010', '298', 'Bank of Ceylon', 'Urubokka / Pasgoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('595', '7010', '343', 'Bank of Ceylon', 'Uva Paranagama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('596', '7010', '626', 'Bank of Ceylon', 'Valachchenai', 'System', null);
INSERT INTO `hgc_bank` VALUES ('597', '7010', '044', 'Bank of Ceylon', 'Vavuniya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('598', '7010', '682', 'Bank of Ceylon', 'Veyangoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('599', '7010', '584', 'Bank of Ceylon', 'Wadduwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('600', '7010', '232', 'Bank of Ceylon', 'Wahakotte', 'System', null);
INSERT INTO `hgc_bank` VALUES ('601', '7010', '558', 'Bank of Ceylon', 'Waikkal', 'System', null);
INSERT INTO `hgc_bank` VALUES ('602', '7010', '517', 'Bank of Ceylon', 'Walasmulla', 'System', null);
INSERT INTO `hgc_bank` VALUES ('603', '7010', '562', 'Bank of Ceylon', 'Warakapola', 'System', null);
INSERT INTO `hgc_bank` VALUES ('604', '7010', '379', 'Bank of Ceylon', 'Wariyapola', 'System', null);
INSERT INTO `hgc_bank` VALUES ('605', '7010', '505', 'Bank of Ceylon', 'Wattala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('606', '7010', '340', 'Bank of Ceylon', 'Wattegama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('607', '7010', '139', 'Bank of Ceylon', 'Weeraketiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('608', '7010', '550', 'Bank of Ceylon', 'Weligama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('609', '7010', '730', 'Bank of Ceylon', 'Welimada', 'System', null);
INSERT INTO `hgc_bank` VALUES ('610', '7010', '527', 'Bank of Ceylon', 'Welisara', 'System', null);
INSERT INTO `hgc_bank` VALUES ('611', '7010', '023', 'Bank of Ceylon', 'Wellawatte', 'System', null);
INSERT INTO `hgc_bank` VALUES ('612', '7010', '434', 'Bank of Ceylon', 'Wellawaya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('613', '7010', '104', 'Bank of Ceylon', 'Welpalla', 'System', null);
INSERT INTO `hgc_bank` VALUES ('614', '7010', '508', 'Bank of Ceylon', 'Wennappuwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('615', '7010', '179', 'Bank of Ceylon', 'Wilgamuwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('616', '7010', '578', 'Bank of Ceylon', 'Yakkalamulla', 'System', null);
INSERT INTO `hgc_bank` VALUES ('617', '7010', '144', 'Bank of Ceylon', 'Yatawatte', 'System', null);
INSERT INTO `hgc_bank` VALUES ('618', '7010', '477', 'Bank of Ceylon', 'Yatiyantota', 'System', null);
INSERT INTO `hgc_bank` VALUES ('619', '7010', '822', 'Bank of Ceylon', '2nd Corporate Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('620', '7010', '761', 'Bank of Ceylon', 'Jaffna Dist-Thirunel', 'System', null);
INSERT INTO `hgc_bank` VALUES ('621', '7010', '763', 'Bank of Ceylon', 'Malabe Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('622', '7010', '762', 'Bank of Ceylon', 'Narahenpita Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('623', '7010', '099', 'Bank of Ceylon', 'Primary Dealer Unit', 'System', null);
INSERT INTO `hgc_bank` VALUES ('624', '7010', '750', 'Bank of Ceylon', 'E.P.Z. Koggala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('625', '7010', '749', 'Bank of Ceylon', 'Karainagar', 'System', null);
INSERT INTO `hgc_bank` VALUES ('626', '7010', '746', 'Bank of Ceylon', 'Rajagiriya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('627', '7010', '743', 'Bank of Ceylon', 'Walapane', 'System', null);
INSERT INTO `hgc_bank` VALUES ('628', '7454', '009', 'DFCC Vardhana Bank', 'Anuradhapura', 'System', null);
INSERT INTO `hgc_bank` VALUES ('629', '7454', '007', 'DFCC Vardhana Bank', 'City Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('630', '7454', '001', 'DFCC Vardhana Bank', 'Colombo 3', 'System', null);
INSERT INTO `hgc_bank` VALUES ('631', '7454', '002', 'DFCC Vardhana Bank', 'Gangodawila Bra', 'System', null);
INSERT INTO `hgc_bank` VALUES ('632', '7454', '006', 'DFCC Vardhana Bank', 'Kandy Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('633', '7454', '005', 'DFCC Vardhana Bank', 'Kurunegala Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('634', '7454', '003', 'DFCC Vardhana Bank', 'Malabe Br', 'System', null);
INSERT INTO `hgc_bank` VALUES ('635', '7454', '004', 'DFCC Vardhana Bank', 'Matara Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('636', '7454', '008', 'DFCC Vardhana Bank', 'Ratnapura Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('637', '7083', '078', 'Hatton National Bank', 'Akkaraipattu', 'System', null);
INSERT INTO `hgc_bank` VALUES ('638', '7083', '001', 'Hatton National Bank', 'Aluthkade', 'System', null);
INSERT INTO `hgc_bank` VALUES ('639', '7083', '049', 'Hatton National Bank', 'Air Cargo Village', 'System', null);
INSERT INTO `hgc_bank` VALUES ('640', '7083', '081', 'Hatton National Bank', 'Ambalangoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('641', '7083', '045', 'Hatton National Bank', 'Ambalantota', 'System', null);
INSERT INTO `hgc_bank` VALUES ('642', '7083', '058', 'Hatton National Bank', 'Ampara', 'System', null);
INSERT INTO `hgc_bank` VALUES ('643', '7083', '010', 'Hatton National Bank', 'Anuradhapura', 'System', null);
INSERT INTO `hgc_bank` VALUES ('644', '7083', '011', 'Hatton National Bank', 'Badulla', 'System', null);
INSERT INTO `hgc_bank` VALUES ('645', '7083', '071', 'Hatton National Bank', 'Balangoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('646', '7083', '039', 'Hatton National Bank', 'Bambalapitiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('647', '7083', '012', 'Hatton National Bank', 'Bandarawela', 'System', null);
INSERT INTO `hgc_bank` VALUES ('648', '7083', '057', 'Hatton National Bank', 'Batticaloa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('649', '7083', '047', 'Hatton National Bank', 'Biyagama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('650', '7083', '060', 'Hatton National Bank', 'Bogawathalawa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('651', '7083', '055', 'Hatton National Bank', 'Borella', 'System', null);
INSERT INTO `hgc_bank` VALUES ('652', '7083', '075', 'Hatton National Bank', 'Buttala branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('653', '7083', '114', 'Hatton National Bank', 'Central Colombo Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('654', '7083', '116', 'Hatton National Bank', 'Colombo South Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('655', '7083', '115', 'Hatton National Bank', 'Colombo West Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('656', '7083', '040', 'Hatton National Bank', 'Chilaw', 'System', null);
INSERT INTO `hgc_bank` VALUES ('657', '7083', '117', 'Hatton National Bank', 'Chunakam Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('658', '7083', '076', 'Hatton National Bank', 'Cinnamon Garden', 'System', null);
INSERT INTO `hgc_bank` VALUES ('659', '7083', '002', 'Hatton National Bank', 'City Office', 'System', null);
INSERT INTO `hgc_bank` VALUES ('660', '7083', '048', 'Hatton National Bank', 'Dambulla', 'System', null);
INSERT INTO `hgc_bank` VALUES ('661', '7083', '070', 'Hatton National Bank', 'Dankotuwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('662', '7083', '003', 'Hatton National Bank', 'Darley Road', 'System', null);
INSERT INTO `hgc_bank` VALUES ('663', '7083', '103', 'Hatton National Bank', 'Dematagoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('664', '7083', '050', 'Hatton National Bank', 'Embilipitiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('665', '7083', '037', 'Hatton National Bank', 'Emirates Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('666', '7083', '013', 'Hatton National Bank', 'Galle', 'System', null);
INSERT INTO `hgc_bank` VALUES ('667', '7083', '051', 'Hatton National Bank', 'Gampaha', 'System', null);
INSERT INTO `hgc_bank` VALUES ('668', '7083', '014', 'Hatton National Bank', 'Gampola', 'System', null);
INSERT INTO `hgc_bank` VALUES ('669', '7083', '046', 'Hatton National Bank', 'Grandpass', 'System', null);
INSERT INTO `hgc_bank` VALUES ('670', '7083', '068', 'Hatton National Bank', 'Hambantota', 'System', null);
INSERT INTO `hgc_bank` VALUES ('671', '7083', '015', 'Hatton National Bank', 'Hatton', 'System', null);
INSERT INTO `hgc_bank` VALUES ('672', '7083', '004', 'Hatton National Bank', 'Head-Office', 'System', null);
INSERT INTO `hgc_bank` VALUES ('673', '7083', '077', 'Hatton National Bank', 'Homagama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('674', '7083', '052', 'Hatton National Bank', 'Horana', 'System', null);
INSERT INTO `hgc_bank` VALUES ('675', '7083', '063', 'Hatton National Bank', 'Hulftsdorp', 'System', null);
INSERT INTO `hgc_bank` VALUES ('676', '7083', '087', 'Hatton National Bank', 'Ja-ela Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('677', '7083', '016', 'Hatton National Bank', 'Jaffna', 'System', null);
INSERT INTO `hgc_bank` VALUES ('678', '7083', '084', 'Hatton National Bank', 'Kadawatte', 'System', null);
INSERT INTO `hgc_bank` VALUES ('679', '7083', '082', 'Hatton National Bank', 'Kaduwela', 'System', null);
INSERT INTO `hgc_bank` VALUES ('680', '7083', '017', 'Hatton National Bank', 'Kahawatte', 'System', null);
INSERT INTO `hgc_bank` VALUES ('681', '7083', '034', 'Hatton National Bank', 'Kalutara', 'System', null);
INSERT INTO `hgc_bank` VALUES ('682', '7083', '018', 'Hatton National Bank', 'Kandy', 'System', null);
INSERT INTO `hgc_bank` VALUES ('683', '7083', '100', 'Hatton National Bank', 'Katugastota Br', 'System', null);
INSERT INTO `hgc_bank` VALUES ('684', '7083', '041', 'Hatton National Bank', 'Kegalle', 'System', null);
INSERT INTO `hgc_bank` VALUES ('685', '7083', '056', 'Hatton National Bank', 'Kiribathgoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('686', '7083', '043', 'Hatton National Bank', 'Kirulapone', 'System', null);
INSERT INTO `hgc_bank` VALUES ('687', '7083', '062', 'Hatton National Bank', 'Kollupitiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('688', '7083', '074', 'Hatton National Bank', 'Kuliyapitiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('689', '7083', '019', 'Hatton National Bank', 'Kurunagala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('690', '7083', '005', 'Hatton National Bank', 'Main Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('691', '7083', '020', 'Hatton National Bank', 'Mannar', 'System', null);
INSERT INTO `hgc_bank` VALUES ('692', '7083', '079', 'Hatton National Bank', 'Maradagahamula', 'System', null);
INSERT INTO `hgc_bank` VALUES ('693', '7083', '080', 'Hatton National Bank', 'Marawila', 'System', null);
INSERT INTO `hgc_bank` VALUES ('694', '7083', '021', 'Hatton National Bank', 'Maskeliya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('695', '7083', '065', 'Hatton National Bank', 'Matale', 'System', null);
INSERT INTO `hgc_bank` VALUES ('696', '7083', '089', 'Hatton National Bank', 'Mawanella', 'System', null);
INSERT INTO `hgc_bank` VALUES ('697', '7083', '053', 'Hatton National Bank', 'Monaragala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('698', '7083', '022', 'Hatton National Bank', 'Moratuwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('699', '7083', '061', 'Hatton National Bank', 'Mount Lavinia', 'System', null);
INSERT INTO `hgc_bank` VALUES ('700', '7083', '023', 'Hatton National Bank', 'Nawalapitiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('701', '7083', '024', 'Hatton National Bank', 'Negombo', 'System', null);
INSERT INTO `hgc_bank` VALUES ('702', '7083', '025', 'Hatton National Bank', 'Nittambuwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('703', '7083', '026', 'Hatton National Bank', 'Nochchiyagama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('704', '7083', '027', 'Hatton National Bank', 'Nugegoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('705', '7083', '028', 'Hatton National Bank', 'Nuwara Eliya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('706', '7083', '069', 'Hatton National Bank', 'Panadura', 'System', null);
INSERT INTO `hgc_bank` VALUES ('707', '7083', '059', 'Hatton National Bank', 'Panchikawatte', 'System', null);
INSERT INTO `hgc_bank` VALUES ('708', '7083', '101', 'Hatton National Bank', 'Pelmadulla Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('709', '7083', '007', 'Hatton National Bank', 'Pettah', 'System', null);
INSERT INTO `hgc_bank` VALUES ('710', '7083', '038', 'Hatton National Bank', 'Piliyandala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('711', '7083', '066', 'Hatton National Bank', 'Pinnawala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('712', '7083', '044', 'Hatton National Bank', 'Polonnaruwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('713', '7083', '029', 'Hatton National Bank', 'Pussellewa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('714', '7083', '083', 'Hatton National Bank', 'Puttalam', 'System', null);
INSERT INTO `hgc_bank` VALUES ('715', '7083', '030', 'Hatton National Bank', 'Ratnapura', 'System', null);
INSERT INTO `hgc_bank` VALUES ('716', '7083', '072', 'Hatton National Bank', 'Sea Street Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('717', '7083', '008', 'Hatton National Bank', 'Suduwella', 'System', null);
INSERT INTO `hgc_bank` VALUES ('718', '7083', '067', 'Hatton National Bank', 'Suriyawewa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('719', '7083', '086', 'Hatton National Bank', 'Tangalle', 'System', null);
INSERT INTO `hgc_bank` VALUES ('720', '7083', '088', 'Hatton National Bank', 'Thambuthegama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('721', '7083', '031', 'Hatton National Bank', 'Trincomalee', 'System', null);
INSERT INTO `hgc_bank` VALUES ('722', '7083', '032', 'Hatton National Bank', 'Vavuniya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('723', '7083', '035', 'Hatton National Bank', 'Wattala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('724', '7083', '033', 'Hatton National Bank', 'Welimada', 'System', null);
INSERT INTO `hgc_bank` VALUES ('725', '7083', '009', 'Hatton National Bank', 'Wellawatte', 'System', null);
INSERT INTO `hgc_bank` VALUES ('726', '7083', '094', 'Hatton National Bank', 'Minuwangoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('727', '7083', '104', 'Hatton National Bank', 'Narahenpita Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('728', '7083', '111', 'Hatton National Bank', 'Awissawella', 'System', null);
INSERT INTO `hgc_bank` VALUES ('729', '7083', '112', 'Hatton National Bank', 'Boralesgamuwa Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('730', '7083', '107', 'Hatton National Bank', 'Elpitiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('731', '7083', '095', 'Hatton National Bank', 'Kantalai Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('732', '7083', '099', 'Hatton National Bank', 'Kirindiwela Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('733', '7083', '098', 'Hatton National Bank', 'Kottawa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('734', '7083', '102', 'Hatton National Bank', 'Ragama Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('735', '7083', '109', 'Hatton National Bank', 'Aluthgama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('736', '7083', '093', 'Hatton National Bank', 'Dehiwela', 'System', null);
INSERT INTO `hgc_bank` VALUES ('737', '7083', '096', 'Hatton National Bank', 'Kotahena Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('738', '7083', '108', 'Hatton National Bank', 'Maradana Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('739', '7083', '036', 'Hatton National Bank', 'Sri Jayawardenapura', 'System', null);
INSERT INTO `hgc_bank` VALUES ('740', '7083', '090', 'Hatton National Bank', 'Tissamaharama Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('741', '7083', '106', 'Hatton National Bank', 'Wellawatta Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('742', '7083', '085', 'Hatton National Bank', 'Talangama Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('743', '7083', '091', 'Hatton National Bank', 'Kalmunai Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('744', '7083', '105', 'Hatton National Bank', 'International Division', 'System', null);
INSERT INTO `hgc_bank` VALUES ('745', '7083', '092', 'Hatton National Bank', 'Thimbirigasyaya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('746', '7083', '110', 'Hatton National Bank', 'Wennappuwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('747', '7092', '008', 'HSBC', 'Pelawatte Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('748', '7092', '001', 'HSBC', '', 'System', null);
INSERT INTO `hgc_bank` VALUES ('749', '7092', '002', 'HSBC', 'Kandy', 'System', null);
INSERT INTO `hgc_bank` VALUES ('750', '7092', '006', 'HSBC', 'Echelon Square Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('751', '7092', '003', 'HSBC', 'Borella', 'System', null);
INSERT INTO `hgc_bank` VALUES ('752', '7092', '007', 'HSBC', 'Colombo 4 Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('753', '7092', '005', 'HSBC', 'Nugegoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('754', '7092', '004', 'HSBC', 'Wellawatte', 'System', null);
INSERT INTO `hgc_bank` VALUES ('755', '7092', '012', 'HSBC', 'Union Place', 'System', null);
INSERT INTO `hgc_bank` VALUES ('756', '7162', '001', 'Nation Trust Bank', 'Kiribathgoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('757', '7162', '011', 'Nation Trust Bank', 'Cinnamon Garden Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('758', '7162', '007', 'Nation Trust Bank', 'Negombo Br', 'System', null);
INSERT INTO `hgc_bank` VALUES ('759', '7162', '004', 'Nation Trust Bank', 'Kandy Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('760', '7162', '002', 'Nation Trust Bank', 'Kollupitya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('761', '7162', '018', 'Nation Trust Bank', 'Gampaha', 'System', null);
INSERT INTO `hgc_bank` VALUES ('762', '7162', '016', 'Nation Trust Bank', 'Kiribathgoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('763', '7162', '017', 'Nation Trust Bank', 'Panadura', 'System', null);
INSERT INTO `hgc_bank` VALUES ('764', '7162', '012', 'Nation Trust Bank', 'Kurunegala Br', 'System', null);
INSERT INTO `hgc_bank` VALUES ('765', '7162', '009', 'Nation Trust Bank', 'Mahabage Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('766', '7162', '014', 'Nation Trust Bank', 'Moratuwa Br.', 'System', null);
INSERT INTO `hgc_bank` VALUES ('767', '7162', '008', 'Nation Trust Bank', 'Pettah Br', 'System', null);
INSERT INTO `hgc_bank` VALUES ('768', '7162', '003', 'Nation Trust Bank', 'Sri Sangaraja Mawatha', 'System', null);
INSERT INTO `hgc_bank` VALUES ('769', '7162', '010', 'Nation Trust Bank', 'Battaramulla Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('770', '7162', '400', 'Nation Trust Bank', 'Card Center', 'System', null);
INSERT INTO `hgc_bank` VALUES ('771', '7162', '005', 'Nation Trust Bank', 'Wellawatte Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('772', '7162', '006', 'Nation Trust Bank', 'Corporate Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('773', '7162', '015', 'Nation Trust Bank', 'Borella Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('774', '7162', '013', 'Nation Trust Bank', 'Maharagama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('775', '7214', '004', 'NDB Bank Limited', 'Nugegoda Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('776', '7214', '006', 'NDB Bank Limited', 'Matara Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('777', '7214', '005', 'NDB Bank Limited', 'Rjagiriya Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('778', '7214', '001', 'NDB Bank Limited', 'Nawam Mawatha Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('779', '7214', '003', 'NDB Bank Limited', 'Jawatte Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('780', '7214', '900', 'NDB Bank Limited', 'Head Office (Corpora', 'System', null);
INSERT INTO `hgc_bank` VALUES ('781', '7214', '100', 'NDB Bank Limited', 'Head Office (Retail)', 'System', null);
INSERT INTO `hgc_bank` VALUES ('782', '7214', '002', 'NDB Bank Limited', 'Kandy Br', 'System', null);
INSERT INTO `hgc_bank` VALUES ('783', '7311', '003', 'Pan Asia Bank', 'Kollupitiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('784', '7311', '002', 'Pan Asia Bank', 'Panchikawatte', 'System', null);
INSERT INTO `hgc_bank` VALUES ('785', '7311', '005', 'Pan Asia Bank', 'Kandy', 'System', null);
INSERT INTO `hgc_bank` VALUES ('786', '7311', '009', 'Pan Asia Bank', 'Bambalapitiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('787', '7311', '013', 'Pan Asia Bank', 'Matara', 'System', null);
INSERT INTO `hgc_bank` VALUES ('788', '7311', '011', 'Pan Asia Bank', 'Gampaha', 'System', null);
INSERT INTO `hgc_bank` VALUES ('789', '7311', '010', 'Pan Asia Bank', 'Negombo', 'System', null);
INSERT INTO `hgc_bank` VALUES ('790', '7311', '006', 'Pan Asia Bank', 'Rajagiriya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('791', '7311', '001', 'Pan Asia Bank', 'Colombo', 'System', null);
INSERT INTO `hgc_bank` VALUES ('792', '7311', '015', 'Pan Asia Bank', 'Dehiwala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('793', '7311', '014', 'Pan Asia Bank', 'Katahena', 'System', null);
INSERT INTO `hgc_bank` VALUES ('794', '7311', '016', 'Pan Asia Bank', 'Wattala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('795', '7135', '228', 'Peoples Bank', 'Addalachchenai', 'System', null);
INSERT INTO `hgc_bank` VALUES ('796', '7135', '188', 'Peoples Bank', 'Ahangama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('797', '7135', '153', 'Peoples Bank', 'Akurana', 'System', null);
INSERT INTO `hgc_bank` VALUES ('798', '7135', '117', 'Peoples Bank', 'Akuressa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('799', '7135', '294', 'Peoples Bank', 'Alawatugoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('800', '7135', '149', 'Peoples Bank', 'Alawwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('801', '7135', '003', 'Peoples Bank', 'Kandy', 'System', null);
INSERT INTO `hgc_bank` VALUES ('802', '7135', '035', 'Peoples Bank', 'Ambalangoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('803', '7135', '072', 'Peoples Bank', 'Ambalantota', 'System', null);
INSERT INTO `hgc_bank` VALUES ('804', '7135', '015', 'Peoples Bank', 'Ampara', 'System', null);
INSERT INTO `hgc_bank` VALUES ('805', '7135', '267', 'Peoples Bank', 'Anamaduwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('806', '7135', '205', 'Peoples Bank', 'Angunakolapalessa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('807', '7135', '183', 'Peoples Bank', 'Ankumbura', 'System', null);
INSERT INTO `hgc_bank` VALUES ('808', '7135', '008', 'Peoples Bank', 'Anuradhapura', 'System', null);
INSERT INTO `hgc_bank` VALUES ('809', '7135', '220', 'Peoples Bank', 'Anuradhapura Nuwarawewa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('810', '7135', '253', 'Peoples Bank', 'Aralaganwila', 'System', null);
INSERT INTO `hgc_bank` VALUES ('811', '7135', '248', 'Peoples Bank', 'Aranayake', 'System', null);
INSERT INTO `hgc_bank` VALUES ('812', '7135', '029', 'Peoples Bank', 'Avissawella', 'System', null);
INSERT INTO `hgc_bank` VALUES ('813', '7135', '087', 'Peoples Bank', 'Baddegama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('814', '7135', '010', 'Peoples Bank', 'Badulla', 'System', null);
INSERT INTO `hgc_bank` VALUES ('815', '7135', '269', 'Peoples Bank', 'Badulla - Muthiyangana', 'System', null);
INSERT INTO `hgc_bank` VALUES ('816', '7135', '283', 'Peoples Bank', 'Badureliya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('817', '7135', '242', 'Peoples Bank', 'Bakamuna', 'System', null);
INSERT INTO `hgc_bank` VALUES ('818', '7135', '017', 'Peoples Bank', 'Balangoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('819', '7135', '154', 'Peoples Bank', 'Balapitiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('820', '7135', '310', 'Peoples Bank', 'Bambalapitiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('821', '7135', '121', 'Peoples Bank', 'Bandaragama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('822', '7135', '037', 'Peoples Bank', 'Bandarawela', 'System', null);
INSERT INTO `hgc_bank` VALUES ('823', '7135', '234', 'Peoples Bank', 'Batapola', 'System', null);
INSERT INTO `hgc_bank` VALUES ('824', '7135', '208', 'Peoples Bank', 'Battaramulla', 'System', null);
INSERT INTO `hgc_bank` VALUES ('825', '7135', '075', 'Peoples Bank', 'Batticaloa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('826', '7135', '113', 'Peoples Bank', 'Batticaloa Town', 'System', null);
INSERT INTO `hgc_bank` VALUES ('827', '7135', '244', 'Peoples Bank', 'Beliatta', 'System', null);
INSERT INTO `hgc_bank` VALUES ('828', '7135', '311', 'Peoples Bank', 'Beruwela', 'System', null);
INSERT INTO `hgc_bank` VALUES ('829', '7135', '011', 'Peoples Bank', 'Bibile', 'System', null);
INSERT INTO `hgc_bank` VALUES ('830', '7135', '172', 'Peoples Bank', 'Bingiriya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('831', '7135', '209', 'Peoples Bank', 'Boralanda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('832', '7135', '078', 'Peoples Bank', 'Borella', 'System', null);
INSERT INTO `hgc_bank` VALUES ('833', '7135', '320', 'Peoples Bank', 'Borella Cotta Road.', 'System', null);
INSERT INTO `hgc_bank` VALUES ('834', '7135', '252', 'Peoples Bank', 'Bulathkohupitiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('835', '7135', '161', 'Peoples Bank', 'Bulathsinhala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('836', '7135', '147', 'Peoples Bank', 'Buttala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('837', '7135', '110', 'Peoples Bank', 'Chavakachcheri', 'System', null);
INSERT INTO `hgc_bank` VALUES ('838', '7135', '227', 'Peoples Bank', 'Chenkalady', 'System', null);
INSERT INTO `hgc_bank` VALUES ('839', '7135', '024', 'Peoples Bank', 'Chilaw', 'System', null);
INSERT INTO `hgc_bank` VALUES ('840', '7135', '109', 'Peoples Bank', 'Chunnakam', 'System', null);
INSERT INTO `hgc_bank` VALUES ('841', '7135', '297', 'Peoples Bank', 'Dam Street', 'System', null);
INSERT INTO `hgc_bank` VALUES ('842', '7135', '138', 'Peoples Bank', 'Dambulla', 'System', null);
INSERT INTO `hgc_bank` VALUES ('843', '7135', '291', 'Peoples Bank', 'Dankotuwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('844', '7135', '206', 'Peoples Bank', 'Davulagala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('845', '7135', '330', 'Peoples Bank', 'Dehiattakandiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('846', '7135', '293', 'Peoples Bank', 'Dehiowita', 'System', null);
INSERT INTO `hgc_bank` VALUES ('847', '7135', '019', 'Peoples Bank', 'Dehiwala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('848', '7135', '337', 'Peoples Bank', 'Dehiwela(new)', 'System', null);
INSERT INTO `hgc_bank` VALUES ('849', '7135', '118', 'Peoples Bank', 'Delgoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('850', '7135', '257', 'Peoples Bank', 'Deltota', 'System', null);
INSERT INTO `hgc_bank` VALUES ('851', '7135', '071', 'Peoples Bank', 'Dematagoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('852', '7135', '132', 'Peoples Bank', 'Deniyaya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('853', '7135', '180', 'Peoples Bank', 'Deraniyagala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('854', '7135', '243', 'Peoples Bank', 'Devinuwara', 'System', null);
INSERT INTO `hgc_bank` VALUES ('855', '7135', '135', 'Peoples Bank', 'Dickwella', 'System', null);
INSERT INTO `hgc_bank` VALUES ('856', '7135', '151', 'Peoples Bank', 'Diyatalawa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('857', '7135', '001', 'Peoples Bank', 'Duke Street', 'System', null);
INSERT INTO `hgc_bank` VALUES ('858', '7135', '073', 'Peoples Bank', 'Elpitiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('859', '7135', '170', 'Peoples Bank', 'Eppawela', 'System', null);
INSERT INTO `hgc_bank` VALUES ('860', '7135', '114', 'Peoples Bank', 'Galagedera', 'System', null);
INSERT INTO `hgc_bank` VALUES ('861', '7135', '177', 'Peoples Bank', 'Galenbindunuwewa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('862', '7135', '115', 'Peoples Bank', 'Galewela', 'System', null);
INSERT INTO `hgc_bank` VALUES ('863', '7135', '184', 'Peoples Bank', 'Galgamuwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('864', '7135', '185', 'Peoples Bank', 'Galigamuwa/Kegalle', 'System', null);
INSERT INTO `hgc_bank` VALUES ('865', '7135', '301', 'Peoples Bank', 'Galkiriyagama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('866', '7135', '169', 'Peoples Bank', 'Galle Main Street', 'System', null);
INSERT INTO `hgc_bank` VALUES ('867', '7135', '013', 'Peoples Bank', 'Galle,Fort', 'System', null);
INSERT INTO `hgc_bank` VALUES ('868', '7135', '179', 'Peoples Bank', 'Galnewa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('869', '7135', '026', 'Peoples Bank', 'Gampaha', 'System', null);
INSERT INTO `hgc_bank` VALUES ('870', '7135', '018', 'Peoples Bank', 'Gampola', 'System', null);
INSERT INTO `hgc_bank` VALUES ('871', '7135', '307', 'Peoples Bank', 'Gandara', 'System', null);
INSERT INTO `hgc_bank` VALUES ('872', '7135', '332', 'Peoples Bank', 'Ganemulla', 'System', null);
INSERT INTO `hgc_bank` VALUES ('873', '7135', '097', 'Peoples Bank', 'Gangodawila', 'System', null);
INSERT INTO `hgc_bank` VALUES ('874', '7135', '302', 'Peoples Bank', 'Ginigathena', 'System', null);
INSERT INTO `hgc_bank` VALUES ('875', '7135', '298', 'Peoples Bank', 'Ginthupitiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('876', '7135', '268', 'Peoples Bank', 'Girandurukotte', 'System', null);
INSERT INTO `hgc_bank` VALUES ('877', '7135', '092', 'Peoples Bank', 'Giriulla', 'System', null);
INSERT INTO `hgc_bank` VALUES ('878', '7135', '245', 'Peoples Bank', 'Godakawela', 'System', null);
INSERT INTO `hgc_bank` VALUES ('879', '7135', '238', 'Peoples Bank', 'Gonagaladeniya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('880', '7135', '126', 'Peoples Bank', 'Grandpass', 'System', null);
INSERT INTO `hgc_bank` VALUES ('881', '7135', '203', 'Peoples Bank', 'Habarana', 'System', null);
INSERT INTO `hgc_bank` VALUES ('882', '7135', '130', 'Peoples Bank', 'Hakmana', 'System', null);
INSERT INTO `hgc_bank` VALUES ('883', '7135', '195', 'Peoples Bank', 'Haldummulla', 'System', null);
INSERT INTO `hgc_bank` VALUES ('884', '7135', '225', 'Peoples Bank', 'Haliela', 'System', null);
INSERT INTO `hgc_bank` VALUES ('885', '7135', '007', 'Peoples Bank', 'Hambantota', 'System', null);
INSERT INTO `hgc_bank` VALUES ('886', '7135', '022', 'Peoples Bank', 'Hanguranketha', 'System', null);
INSERT INTO `hgc_bank` VALUES ('887', '7135', '229', 'Peoples Bank', 'Hanwella', 'System', null);
INSERT INTO `hgc_bank` VALUES ('888', '7135', '140', 'Peoples Bank', 'Hasalaka', 'System', null);
INSERT INTO `hgc_bank` VALUES ('889', '7135', '204', 'Peoples Bank', 'Head Quarters Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('890', '7135', '221', 'Peoples Bank', 'Hemmathagama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('891', '7135', '144', 'Peoples Bank', 'Hettipola', 'System', null);
INSERT INTO `hgc_bank` VALUES ('892', '7135', '136', 'Peoples Bank', 'Hikkaduwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('893', '7135', '006', 'Peoples Bank', 'Hingurakgoka', 'System', null);
INSERT INTO `hgc_bank` VALUES ('894', '7135', '049', 'Peoples Bank', 'Homagama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('895', '7135', '041', 'Peoples Bank', 'Horana', 'System', null);
INSERT INTO `hgc_bank` VALUES ('896', '7135', '218', 'Peoples Bank', 'Horowpothana', 'System', null);
INSERT INTO `hgc_bank` VALUES ('897', '7135', '207', 'Peoples Bank', 'Ibbagamuwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('898', '7135', '247', 'Peoples Bank', 'Imaduwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('899', '7135', '300', 'Peoples Bank', 'Ingiriya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('900', '7135', '286', 'Peoples Bank', 'Int\'l Div-Lakeside Barnch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('901', '7135', '004', 'Peoples Bank', 'International Division', 'System', null);
INSERT INTO `hgc_bank` VALUES ('902', '7135', '239', 'Peoples Bank', 'Ja-Ela', 'System', null);
INSERT INTO `hgc_bank` VALUES ('903', '7135', '284', 'Peoples Bank', 'Jaffna Kannathiddy', 'System', null);
INSERT INTO `hgc_bank` VALUES ('904', '7135', '104', 'Peoples Bank', 'Jaffna Main Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('905', '7135', '162', 'Peoples Bank', 'Jaffna Model Market', 'System', null);
INSERT INTO `hgc_bank` VALUES ('906', '7135', '030', 'Peoples Bank', 'Jaffna Stanley Road', 'System', null);
INSERT INTO `hgc_bank` VALUES ('907', '7135', '273', 'Peoples Bank', 'Kadawata', 'System', null);
INSERT INTO `hgc_bank` VALUES ('908', '7135', '159', 'Peoples Bank', 'Kadugannawa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('909', '7135', '196', 'Peoples Bank', 'Kaduwela', 'System', null);
INSERT INTO `hgc_bank` VALUES ('910', '7135', '051', 'Peoples Bank', 'Kahatagasdigiliya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('911', '7135', '155', 'Peoples Bank', 'Kahawatte', 'System', null);
INSERT INTO `hgc_bank` VALUES ('912', '7135', '235', 'Peoples Bank', 'Kalawana', 'System', null);
INSERT INTO `hgc_bank` VALUES ('913', '7135', '023', 'Peoples Bank', 'Kalmunai', 'System', null);
INSERT INTO `hgc_bank` VALUES ('914', '7135', '125', 'Peoples Bank', 'Kalpitiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('915', '7135', '289', 'Peoples Bank', 'Kaltota', 'System', null);
INSERT INTO `hgc_bank` VALUES ('916', '7135', '039', 'Peoples Bank', 'Kalutara', 'System', null);
INSERT INTO `hgc_bank` VALUES ('917', '7135', '133', 'Peoples Bank', 'Kamburupitiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('918', '7135', '250', 'Peoples Bank', 'Kandeketiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('919', '7135', '031', 'Peoples Bank', 'Kankesanthurai', 'System', null);
INSERT INTO `hgc_bank` VALUES ('920', '7135', '223', 'Peoples Bank', 'Karativu', 'System', null);
INSERT INTO `hgc_bank` VALUES ('921', '7135', '168', 'Peoples Bank', 'Kataragama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('922', '7135', '065', 'Peoples Bank', 'Kattankudy', 'System', null);
INSERT INTO `hgc_bank` VALUES ('923', '7135', '313', 'Peoples Bank', 'Katubedda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('924', '7135', '089', 'Peoples Bank', 'Katugastota', 'System', null);
INSERT INTO `hgc_bank` VALUES ('925', '7135', '276', 'Peoples Bank', 'Katunayake', 'System', null);
INSERT INTO `hgc_bank` VALUES ('926', '7135', '105', 'Peoples Bank', 'Kayts', 'System', null);
INSERT INTO `hgc_bank` VALUES ('927', '7135', '150', 'Peoples Bank', 'Kebithigollawa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('928', '7135', '027', 'Peoples Bank', 'Kegalle', 'System', null);
INSERT INTO `hgc_bank` VALUES ('929', '7135', '299', 'Peoples Bank', 'Kegalle Bazzar', 'System', null);
INSERT INTO `hgc_bank` VALUES ('930', '7135', '259', 'Peoples Bank', 'Kehelwatte', 'System', null);
INSERT INTO `hgc_bank` VALUES ('931', '7135', '042', 'Peoples Bank', 'Kekirawa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('932', '7135', '055', 'Peoples Bank', 'Kelaniya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('933', '7135', '240', 'Peoples Bank', 'Keppetipola', 'System', null);
INSERT INTO `hgc_bank` VALUES ('934', '7135', '327', 'Peoples Bank', 'Kesbewa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('935', '7135', '048', 'Peoples Bank', 'Kilinochchi', 'System', null);
INSERT INTO `hgc_bank` VALUES ('936', '7135', '094', 'Peoples Bank', 'Kinniya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('937', '7135', '237', 'Peoples Bank', 'Kiribathgoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('938', '7135', '266', 'Peoples Bank', 'Kiriella', 'System', null);
INSERT INTO `hgc_bank` VALUES ('939', '7135', '202', 'Peoples Bank', 'Kirindiwela', 'System', null);
INSERT INTO `hgc_bank` VALUES ('940', '7135', '319', 'Peoples Bank', 'Kirulapana', 'System', null);
INSERT INTO `hgc_bank` VALUES ('941', '7135', '281', 'Peoples Bank', 'Kobeigane', 'System', null);
INSERT INTO `hgc_bank` VALUES ('942', '7135', '142', 'Peoples Bank', 'Kochichikade', 'System', null);
INSERT INTO `hgc_bank` VALUES ('943', '7135', '329', 'Peoples Bank', 'Koggala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('944', '7135', '210', 'Peoples Bank', 'Kollupitiya Co-op House', 'System', null);
INSERT INTO `hgc_bank` VALUES ('945', '7135', '194', 'Peoples Bank', 'Kolonnawa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('946', '7135', '260', 'Peoples Bank', 'Koslanda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('947', '7135', '308', 'Peoples Bank', 'Kotahena', 'System', null);
INSERT INTO `hgc_bank` VALUES ('948', '7135', '098', 'Peoples Bank', 'Kotikawatte', 'System', null);
INSERT INTO `hgc_bank` VALUES ('949', '7135', '328', 'Peoples Bank', 'Kottawa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('950', '7135', '288', 'Peoples Bank', 'Kudawella', 'System', null);
INSERT INTO `hgc_bank` VALUES ('951', '7135', '028', 'Peoples Bank', 'Kuliyapitiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('952', '7135', '012', 'Peoples Bank', 'Kurunegala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('953', '7135', '334', 'Peoples Bank', 'Kurunegala Ethugalpura', 'System', null);
INSERT INTO `hgc_bank` VALUES ('954', '7135', '226', 'Peoples Bank', 'Kurunegala-Maliyadeva Br.', 'System', null);
INSERT INTO `hgc_bank` VALUES ('955', '7135', '263', 'Peoples Bank', 'Kuruwita', 'System', null);
INSERT INTO `hgc_bank` VALUES ('956', '7135', '309', 'Peoples Bank', 'Liberty Plaza', 'System', null);
INSERT INTO `hgc_bank` VALUES ('957', '7135', '331', 'Peoples Bank', 'Lucky Plaza Branch.', 'System', null);
INSERT INTO `hgc_bank` VALUES ('958', '7135', '251', 'Peoples Bank', 'Lunugala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('959', '7135', '215', 'Peoples Bank', 'Madampe', 'System', null);
INSERT INTO `hgc_bank` VALUES ('960', '7135', '282', 'Peoples Bank', 'Maggona', 'System', null);
INSERT INTO `hgc_bank` VALUES ('961', '7135', '181', 'Peoples Bank', 'Maha-Oya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('962', '7135', '217', 'Peoples Bank', 'Mahara', 'System', null);
INSERT INTO `hgc_bank` VALUES ('963', '7135', '306', 'Peoples Bank', 'Maharagama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('964', '7135', '303', 'Peoples Bank', 'Mahawewa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('965', '7135', '058', 'Peoples Bank', 'Mahiyangana', 'System', null);
INSERT INTO `hgc_bank` VALUES ('966', '7135', '052', 'Peoples Bank', 'Maho', 'System', null);
INSERT INTO `hgc_bank` VALUES ('967', '7135', '050', 'Peoples Bank', 'Main Street Colombo', 'System', null);
INSERT INTO `hgc_bank` VALUES ('968', '7135', '137', 'Peoples Bank', 'Makandura', 'System', null);
INSERT INTO `hgc_bank` VALUES ('969', '7135', '191', 'Peoples Bank', 'Malwana', 'System', null);
INSERT INTO `hgc_bank` VALUES ('970', '7135', '312', 'Peoples Bank', 'Malwatte Road Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('971', '7135', '165', 'Peoples Bank', 'Mankulam', 'System', null);
INSERT INTO `hgc_bank` VALUES ('972', '7135', '044', 'Peoples Bank', 'Mannar', 'System', null);
INSERT INTO `hgc_bank` VALUES ('973', '7135', '236', 'Peoples Bank', 'Maradana', 'System', null);
INSERT INTO `hgc_bank` VALUES ('974', '7135', '176', 'Peoples Bank', 'Maradana Railway HQ. Bldg', 'System', null);
INSERT INTO `hgc_bank` VALUES ('975', '7135', '100', 'Peoples Bank', 'Marandagahamulla', 'System', null);
INSERT INTO `hgc_bank` VALUES ('976', '7135', '322', 'Peoples Bank', 'Marawila', 'System', null);
INSERT INTO `hgc_bank` VALUES ('977', '7135', '178', 'Peoples Bank', 'Maskeliya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('978', '7135', '002', 'Peoples Bank', 'Matale', 'System', null);
INSERT INTO `hgc_bank` VALUES ('979', '7135', '152', 'Peoples Bank', 'Matara Dharmapala Mawatha', 'System', null);
INSERT INTO `hgc_bank` VALUES ('980', '7135', '032', 'Peoples Bank', 'Matara Uyanwatte', 'System', null);
INSERT INTO `hgc_bank` VALUES ('981', '7135', '070', 'Peoples Bank', 'Matugama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('982', '7135', '069', 'Peoples Bank', 'Mawanella', 'System', null);
INSERT INTO `hgc_bank` VALUES ('983', '7135', '199', 'Peoples Bank', 'Mawathagama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('984', '7135', '258', 'Peoples Bank', 'Medagama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('985', '7135', '096', 'Peoples Bank', 'Medawachchiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('986', '7135', '231', 'Peoples Bank', 'Medirigiriya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('987', '7135', '246', 'Peoples Bank', 'Meegalewa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('988', '7135', '157', 'Peoples Bank', 'Menikhinna', 'System', null);
INSERT INTO `hgc_bank` VALUES ('989', '7135', '265', 'Peoples Bank', 'Middeniya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('990', '7135', '021', 'Peoples Bank', 'Minuwangoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('991', '7135', '198', 'Peoples Bank', 'Mirigama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('992', '7135', '068', 'Peoples Bank', 'Monaragala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('993', '7135', '290', 'Peoples Bank', 'Moratumulla', 'System', null);
INSERT INTO `hgc_bank` VALUES ('994', '7135', '091', 'Peoples Bank', 'Moratuwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('995', '7135', '060', 'Peoples Bank', 'Morawaka', 'System', null);
INSERT INTO `hgc_bank` VALUES ('996', '7135', '336', 'Peoples Bank', 'Mount Lavinia', 'System', null);
INSERT INTO `hgc_bank` VALUES ('997', '7135', '046', 'Peoples Bank', 'Mudalige Mawatha', 'System', null);
INSERT INTO `hgc_bank` VALUES ('998', '7135', '020', 'Peoples Bank', 'Mullativu', 'System', null);
INSERT INTO `hgc_bank` VALUES ('999', '7135', '166', 'Peoples Bank', 'Murunkan', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1000', '7135', '095', 'Peoples Bank', 'Muttur', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1001', '7135', '214', 'Peoples Bank', 'Mutuwal', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1002', '7135', '119', 'Peoples Bank', 'Narahenpita', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1003', '7135', '082', 'Peoples Bank', 'Narammala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1004', '7135', '083', 'Peoples Bank', 'Nattandiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1005', '7135', '146', 'Peoples Bank', 'Naula', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1006', '7135', '053', 'Peoples Bank', 'Nawalapitiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1007', '7135', '249', 'Peoples Bank', 'Neeboda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1008', '7135', '034', 'Peoples Bank', 'Negombo', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1009', '7135', '124', 'Peoples Bank', 'Nikeweratiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1010', '7135', '127', 'Peoples Bank', 'Nildandhahinna', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1011', '7135', '296', 'Peoples Bank', 'Nintavur', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1012', '7135', '278', 'Peoples Bank', 'Nittambuwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1013', '7135', '192', 'Peoples Bank', 'Nivithigala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1014', '7135', '171', 'Peoples Bank', 'Nochchiyagama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1015', '7135', '174', 'Peoples Bank', 'Nugegoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1016', '7135', '335', 'Peoples Bank', 'Nugegoda City Branch', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1017', '7135', '134', 'Peoples Bank', 'Nuwara - Eliya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1018', '7135', '275', 'Peoples Bank', 'Olcott Mawatha', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1019', '7135', '043', 'Peoples Bank', 'Padaviya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1020', '7135', '241', 'Peoples Bank', 'Pallepola', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1021', '7135', '318', 'Peoples Bank', 'Pamunugama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1022', '7135', '148', 'Peoples Bank', 'Panadura', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1023', '7135', '321', 'Peoples Bank', 'Panadura Town', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1024', '7135', '211', 'Peoples Bank', 'Panwila', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1025', '7135', '111', 'Peoples Bank', 'Paranthan', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1026', '7135', '025', 'Peoples Bank', 'Park Street', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1027', '7135', '116', 'Peoples Bank', 'Passara', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1028', '7135', '999', 'Peoples Bank', 'Pay Order\'s Division', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1029', '7135', '261', 'Peoples Bank', 'Pelawatte', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1030', '7135', '160', 'Peoples Bank', 'Pelmadulla', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1031', '7135', '057', 'Peoples Bank', 'Peradeniya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1032', '7135', '139', 'Peoples Bank', 'Pettah', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1033', '7135', '256', 'Peoples Bank', 'Pilimathalawa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1034', '7135', '103', 'Peoples Bank', 'Piliyandala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1035', '7135', '279', 'Peoples Bank', 'Pitakotte', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1036', '7135', '285', 'Peoples Bank', 'Point Pedro', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1037', '7135', '059', 'Peoples Bank', 'Polgahawela', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1038', '7135', '005', 'Peoples Bank', 'Polonnaruwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1039', '7135', '232', 'Peoples Bank', 'Polonnaruwa Town', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1040', '7135', '280', 'Peoples Bank', 'Potuhera', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1041', '7135', '164', 'Peoples Bank', 'Potuvil', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1042', '7135', '093', 'Peoples Bank', 'Pugoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1043', '7135', '173', 'Peoples Bank', 'Pundaluoya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1044', '7135', '274', 'Peoples Bank', 'Pussellawa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1045', '7135', '009', 'Peoples Bank', 'Puttalam', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1046', '7135', '033', 'Peoples Bank', 'Queen Street', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1047', '7135', '036', 'Peoples Bank', 'Ragala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1048', '7135', '316', 'Peoples Bank', 'Ragama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1049', '7135', '129', 'Peoples Bank', 'Rakwana', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1050', '7135', '101', 'Peoples Bank', 'Rambukkana', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1051', '7135', '080', 'Peoples Bank', 'Ratmalana', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1052', '7135', '088', 'Peoples Bank', 'Ratnapura', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1053', '7135', '317', 'Peoples Bank', 'Ratnapura Town', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1054', '7135', '128', 'Peoples Bank', 'Rattota', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1055', '7135', '193', 'Peoples Bank', 'Ridigama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1056', '7135', '081', 'Peoples Bank', 'Ruwanwela', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1057', '7135', '064', 'Peoples Bank', 'Sammanthurai', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1058', '7135', '277', 'Peoples Bank', 'Sea Street', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1059', '7135', '324', 'Peoples Bank', 'Seeduwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1060', '7135', '158', 'Peoples Bank', 'Senkadagala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1061', '7135', '233', 'Peoples Bank', 'Serunuwara', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1062', '7135', '056', 'Peoples Bank', 'Sri Sangaraja Mawatha.', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1063', '7135', '143', 'Peoples Bank', 'Suduwella', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1064', '7135', '264', 'Peoples Bank', 'Suriyawewa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1065', '7135', '315', 'Peoples Bank', 'Talawa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1066', '7135', '038', 'Peoples Bank', 'Talawakelle', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1067', '7135', '272', 'Peoples Bank', 'Talgaswela', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1068', '7135', '219', 'Peoples Bank', 'Tambuttegama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1069', '7135', '067', 'Peoples Bank', 'Tangalle', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1070', '7135', '112', 'Peoples Bank', 'Teldeniya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1071', '7135', '230', 'Peoples Bank', 'Thanamalwila', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1072', '7135', '086', 'Peoples Bank', 'Thimbirigasyaya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1073', '7135', '224', 'Peoples Bank', 'Thirukkovil', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1074', '7135', '270', 'Peoples Bank', 'Thulhiriya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1075', '7135', '061', 'Peoples Bank', 'Tissamaharama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1076', '7135', '167', 'Peoples Bank', 'Town Hall, Colombo.', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1077', '7135', '066', 'Peoples Bank', 'Trincomalee', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1078', '7135', '255', 'Peoples Bank', 'Trincomalee Town', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1079', '7135', '292', 'Peoples Bank', 'Udapussellawa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1080', '7135', '295', 'Peoples Bank', 'Udawalawe', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1081', '7135', '131', 'Peoples Bank', 'Udugama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1082', '7135', '189', 'Peoples Bank', 'Uhana', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1083', '7135', '201', 'Peoples Bank', 'Ukuwela', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1084', '7135', '014', 'Peoples Bank', 'Union Place', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1085', '7135', '197', 'Peoples Bank', 'Uragasmanhandiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1086', '7135', '271', 'Peoples Bank', 'Urubokka', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1087', '7135', '156', 'Peoples Bank', 'Uva Paranagama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1088', '7135', '102', 'Peoples Bank', 'Valaichenai', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1089', '7135', '040', 'Peoples Bank', 'Vavuniya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1090', '7135', '141', 'Peoples Bank', 'Velvettiturai', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1091', '7135', '079', 'Peoples Bank', 'Veyangoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1092', '7135', '262', 'Peoples Bank', 'Wadduwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1093', '7135', '325', 'Peoples Bank', 'Waduramba', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1094', '7135', '304', 'Peoples Bank', 'Walasgala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1095', '7135', '120', 'Peoples Bank', 'Walasmulla', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1096', '7135', '054', 'Peoples Bank', 'Warakapola', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1097', '7135', '163', 'Peoples Bank', 'Wariyapola', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1098', '7135', '222', 'Peoples Bank', 'Wattala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1099', '7135', '074', 'Peoples Bank', 'Wattegama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1100', '7135', '077', 'Peoples Bank', 'Weligama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1101', '7135', '254', 'Peoples Bank', 'Welikanda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1102', '7135', '016', 'Peoples Bank', 'Welimada', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1103', '7135', '145', 'Peoples Bank', 'Wellawatte', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1104', '7135', '062', 'Peoples Bank', 'Wellawaya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1105', '7135', '076', 'Peoples Bank', 'Wennappuwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1106', '7135', '122', 'Peoples Bank', 'Wilgamuwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1107', '7135', '333', 'Peoples Bank', 'Yakkala', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1108', '7135', '047', 'Peoples Bank', 'Yatiyantota', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1109', '7135', '200', 'Peoples Bank', 'Majestic City', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1110', '7135', '107', 'Peoples Bank', 'Jaffna Region-Atchuvely', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1111', '7135', '108', 'Peoples Bank', 'Jaffna Region-Chankanai', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1112', '7038', '106', 'Peoples Bank', 'Jaffna Region-Nelliady', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1113', '7038', '005', 'Standard Charterd Bank', 'Kirullapone', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1114', '7038', '008', 'Standard Charterd Bank', 'Kollupitiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1115', '7038', '006', 'Standard Charterd Bank', 'Moratuwa', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1116', '7038', '010', 'Standard Charterd Bank', 'Pettah', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1117', '7038', '007', 'Standard Charterd Bank', 'ajagiriya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1118', '7038', '011', 'Standard Charterd Bank', 'Lipton Circus', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1119', '7038', '003', 'Standard Charterd Bank', 'Wellawatte', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1120', '7278', '001', 'Standard Charterd Bank', 'Main Office', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1121', '7278', '034', 'Sampath Bank', 'Thimbirigasyaya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1122', '7278', '028', 'Sampath Bank', 'Tissamaharama', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1123', '7278', '021', 'Sampath Bank', 'Anuradhapura', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1124', '7278', '022', 'Sampath Bank', 'Avissawella', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1125', '7278', '011', 'Sampath Bank', 'Bambalapitiya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1126', '7278', '004', 'Sampath Bank', 'Borella', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1127', '7278', '996', 'Sampath Bank', 'Card Centre', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1128', '7278', '993', 'Sampath Bank', 'Central Clearing Dept.', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1129', '7278', '012', 'Sampath Bank', 'Chatham Street', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1130', '7056', '111', 'Commercial Bank', 'Hingurakgoda', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1131', '7010', '111', 'Bank Of Ceylon', 'Kaduruwela', 'System', null);
INSERT INTO `hgc_bank` VALUES ('1132', '7454', '111', 'DFCC Vardhana Bank', 'Kaduruwela', 'System', null);
INSERT INTO `hgc_bank` VALUES ('3999', '7083', '222', 'Hatton National Bank', 'Medirigiriya', 'System', null);
INSERT INTO `hgc_bank` VALUES ('4000', '7278\r\n', '057', 'Sampath Bank', 'Horana', 'System', '2014-05-09 23:40:51');
INSERT INTO `hgc_bank` VALUES ('4001', '7287', '023', 'Seylan Bank', 'Horana', 'System', '2014-05-09 23:42:54');
INSERT INTO `hgc_bank` VALUES ('4002', '7162', '036', 'Nation Trust Bank', 'Horana', 'System', '2014-05-09 23:45:52');
INSERT INTO `hgc_bank` VALUES ('4003', '7287', '066', 'Seylan Bank', 'Mathugama', 'System', '2014-05-09 23:48:23');
INSERT INTO `hgc_bank` VALUES ('4004', '7287', '075', 'Seylan Bank', 'Avissawella', 'System', '2014-05-09 23:49:20');
INSERT INTO `hgc_bank` VALUES ('4005', '7135', '003', 'Peoples Bank', 'Kandy', 'System', '2014-08-28 15:49:21');
INSERT INTO `hgc_bank` VALUES ('4006', '7287', '002', 'Seylan Bank', 'Kandy', 'System', '2014-08-28 15:50:45');

-- ----------------------------
-- Table structure for `hgc_branch`
-- ----------------------------
DROP TABLE IF EXISTS `hgc_branch`;
CREATE TABLE `hgc_branch` (
  `br_id` int(11) NOT NULL AUTO_INCREMENT,
  `br_group` int(11) DEFAULT NULL,
  `br_name` varchar(255) NOT NULL,
  `br_code` varchar(50) NOT NULL,
  `br_addl1` text NOT NULL,
  `br_addl2` text,
  `br_city` varchar(50) DEFAULT NULL,
  `br_country` varchar(75) DEFAULT NULL,
  `br_telephone` text,
  `br_fax` text,
  `br_prefix` varchar(5) DEFAULT NULL,
  `br_color` text,
  PRIMARY KEY (`br_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hgc_branch
-- ----------------------------
INSERT INTO `hgc_branch` VALUES ('1', '1', 'Malabe', 'MB', 'xx', 'xxxxxxxxxxxx', 'xxxxxxxx', 'xxxxxxxxxxx', '0546565651', '0656565555', null, null);
INSERT INTO `hgc_branch` VALUES ('2', '1', 'Nugegoda', 'NG', 'yyy', 'yyyyyyyyyyyyyy', 'yyyyyyyy', 'yyyyyyy', '0989898888', '0656565655', null, null);

-- ----------------------------
-- Table structure for `hgc_company`
-- ----------------------------
DROP TABLE IF EXISTS `hgc_company`;
CREATE TABLE `hgc_company` (
  `comp_id` int(11) NOT NULL AUTO_INCREMENT,
  `comp_name` varchar(255) NOT NULL,
  `comp_brno` varchar(50) NOT NULL,
  `comp_code` varchar(50) NOT NULL,
  `comp_addl1` text NOT NULL,
  `comp_addl2` text,
  `comp_city` varchar(50) DEFAULT NULL,
  `comp_country` varchar(75) DEFAULT NULL,
  `comp_telephone` text,
  `comp_fax` text,
  PRIMARY KEY (`comp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hgc_company
-- ----------------------------
INSERT INTO `hgc_company` VALUES ('1', 'Evolve School', '123', 'HCSM', '82', 'Evolve Technologies (Pvt) Ltd', 'Colombo 02', 'Sri Lanka', '0116565655', '0116555957');

-- ----------------------------
-- Table structure for `hgc_designation`
-- ----------------------------
DROP TABLE IF EXISTS `hgc_designation`;
CREATE TABLE `hgc_designation` (
  `des_id` int(11) NOT NULL AUTO_INCREMENT,
  `des_description` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `des_status` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`des_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of hgc_designation
-- ----------------------------

-- ----------------------------
-- Table structure for `hgc_group`
-- ----------------------------
DROP TABLE IF EXISTS `hgc_group`;
CREATE TABLE `hgc_group` (
  `grp_id` int(11) NOT NULL AUTO_INCREMENT,
  `grp_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`grp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hgc_group
-- ----------------------------
INSERT INTO `hgc_group` VALUES ('1', 'Horizon College');
INSERT INTO `hgc_group` VALUES ('2', 'KIA');
INSERT INTO `hgc_group` VALUES ('3', 'ChildCare');

-- ----------------------------
-- Table structure for `hgc_logger`
-- ----------------------------
DROP TABLE IF EXISTS `hgc_logger`;
CREATE TABLE `hgc_logger` (
  `log_id` int(11) DEFAULT NULL,
  `log_code` text,
  `log_action` varchar(100) DEFAULT NULL,
  `log_status` varchar(20) DEFAULT NULL,
  `log_description` varchar(255) DEFAULT NULL,
  `log_timestamp` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `log_user` int(11) DEFAULT NULL,
  `log_userbranch` int(11) DEFAULT NULL,
  `log_usergroup` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hgc_logger
-- ----------------------------

-- ----------------------------
-- Table structure for `hgc_religion`
-- ----------------------------
DROP TABLE IF EXISTS `hgc_religion`;
CREATE TABLE `hgc_religion` (
  `rel_id` int(11) NOT NULL AUTO_INCREMENT,
  `rel_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`rel_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of hgc_religion
-- ----------------------------
INSERT INTO `hgc_religion` VALUES ('1', 'Buddhist');
INSERT INTO `hgc_religion` VALUES ('2', 'Hindu');

-- ----------------------------
-- Table structure for `hgc_rightextrabranch`
-- ----------------------------
DROP TABLE IF EXISTS `hgc_rightextrabranch`;
CREATE TABLE `hgc_rightextrabranch` (
  `urb_id` int(11) NOT NULL AUTO_INCREMENT,
  `urb_right` int(11) NOT NULL,
  `urb_branch` int(11) NOT NULL,
  `urb_status` varchar(5) NOT NULL,
  PRIMARY KEY (`urb_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of hgc_rightextrabranch
-- ----------------------------

-- ----------------------------
-- Table structure for `hgc_sequence`
-- ----------------------------
DROP TABLE IF EXISTS `hgc_sequence`;
CREATE TABLE `hgc_sequence` (
  `HGC_SEQ_Name` varchar(12) NOT NULL,
  `HGC_SEQ_NextValue` bigint(11) NOT NULL,
  `HGC_SEQ_Reserved` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`HGC_SEQ_Name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of hgc_sequence
-- ----------------------------
INSERT INTO `hgc_sequence` VALUES ('EMP', '2', null);
INSERT INTO `hgc_sequence` VALUES ('FEE', '4', null);
INSERT INTO `hgc_sequence` VALUES ('FS//', '1', null);
INSERT INTO `hgc_sequence` VALUES ('INVMB18', '77', null);
INSERT INTO `hgc_sequence` VALUES ('PAYMB18', '29', null);
INSERT INTO `hgc_sequence` VALUES ('RECMB18', '29', null);
INSERT INTO `hgc_sequence` VALUES ('TEMPSTUMB', '18', null);
INSERT INTO `hgc_sequence` VALUES ('TRANMB18', '125', null);

-- ----------------------------
-- Table structure for `hgc_staff`
-- ----------------------------
DROP TABLE IF EXISTS `hgc_staff`;
CREATE TABLE `hgc_staff` (
  `stf_id` int(11) NOT NULL AUTO_INCREMENT,
  `stf_index` text,
  `stf_firstname` varchar(255) DEFAULT NULL,
  `stf_lastname` varchar(255) DEFAULT NULL,
  `stf_gender` varchar(5) DEFAULT NULL,
  `stf_dob` date DEFAULT NULL,
  `stf_primarysubject` varchar(255) DEFAULT NULL,
  `stf_fatherhusname` varchar(255) DEFAULT NULL,
  `stf_maritalstat` varchar(255) DEFAULT NULL,
  `stf_designation` varchar(255) DEFAULT NULL,
  `stf_email` varchar(255) DEFAULT NULL,
  `stf_mobileno` varchar(255) DEFAULT NULL,
  `stf_contactnum` varchar(255) DEFAULT NULL,
  `stf_altnum` varchar(255) DEFAULT NULL,
  `stf_altnum2` varchar(255) DEFAULT NULL,
  `stf_adress1` varchar(255) DEFAULT NULL,
  `stf_adress2` varchar(255) DEFAULT NULL,
  `stf_city` varchar(255) DEFAULT NULL,
  `stf_postcode` varchar(255) DEFAULT NULL,
  `stf_country` varchar(255) DEFAULT NULL,
  `stf_praddress1` varchar(255) DEFAULT NULL,
  `stf_pradress2` varchar(255) DEFAULT NULL,
  `stf_prcity` varchar(255) DEFAULT NULL,
  `stf_prpostcode` varchar(255) DEFAULT NULL,
  `stf_prphoneno` varchar(255) DEFAULT NULL,
  `stf_prresino` varchar(255) DEFAULT NULL,
  `stf_prcountry` varchar(255) DEFAULT NULL,
  `stf_prmobileno` varchar(255) DEFAULT NULL,
  `stf_status` varchar(5) NOT NULL,
  `stf_dateofjoining` varchar(255) DEFAULT NULL,
  `stf_ image` text,
  `stf_terminationdate` varchar(255) DEFAULT NULL,
  `stf_group` int(11) DEFAULT NULL,
  `stf_department` int(11) DEFAULT NULL,
  `stf_branch` int(11) DEFAULT NULL,
  `stf_nationality` varchar(255) DEFAULT NULL,
  `stf_section` int(11) DEFAULT NULL,
  PRIMARY KEY (`stf_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of hgc_staff
-- ----------------------------
INSERT INTO `hgc_staff` VALUES ('1', 'EMP0001', 'Nirosha', 'Perera', null, null, null, null, '1', '1', 'hjfhsdfjshdfjhdsf@jkjkj.com', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'A', null, null, null, '2', '1', '1', 'Sin', null);
INSERT INTO `hgc_staff` VALUES ('2', 'EMP0002', 'Kosala', 'Imbulgoda', null, null, null, null, '1', '4', 'kfjdkfjdkf@jkjk.com', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'A', null, null, null, '2', '2', '1', 'sdsd', null);

-- ----------------------------
-- Table structure for `hgc_staff1`
-- ----------------------------
DROP TABLE IF EXISTS `hgc_staff1`;
CREATE TABLE `hgc_staff1` (
  `stf_id` int(11) NOT NULL AUTO_INCREMENT,
  `stf_postaplied` varchar(255) NOT NULL,
  `stf_firstname` varchar(255) NOT NULL,
  `stf_lastname` varchar(255) NOT NULL,
  `stf_gender` varchar(255) NOT NULL,
  `stf_fthname` varchar(255) NOT NULL,
  `stf_dob` varchar(255) NOT NULL,
  `stf_primarysubject` varchar(255) NOT NULL,
  `stf_fatherhusname` varchar(255) NOT NULL,
  `stf_noofdughters` int(11) NOT NULL,
  `stf_noofsons` varchar(255) NOT NULL,
  `stf_faminfo` varchar(255) NOT NULL,
  `stf_hobbies` varchar(255) NOT NULL,
  `stf_marital` varchar(255) NOT NULL,
  `stf_experience` varchar(255) NOT NULL,
  `stf_attach1` varchar(255) NOT NULL,
  `stf_attach2` varchar(255) NOT NULL,
  `stf_attach3` varchar(255) NOT NULL,
  `stf_attach4` varchar(255) NOT NULL,
  `stf_category` varchar(255) NOT NULL,
  `stf_email` varchar(255) NOT NULL,
  `stf_mobilenocomunication` varchar(255) NOT NULL,
  `stf_examp1` varchar(255) NOT NULL,
  `stf_examp2` varchar(255) NOT NULL,
  `stf_examp3` varchar(255) NOT NULL,
  `stf_borduniversity1` varchar(255) NOT NULL,
  `stf_borduniversity2` varchar(255) NOT NULL,
  `stf_borduniversity3` varchar(255) NOT NULL,
  `stf_year1` varchar(255) NOT NULL,
  `stf_year2` varchar(255) NOT NULL,
  `stf_year3` varchar(255) NOT NULL,
  `stf_insititute1` varchar(255) NOT NULL,
  `stf_insititute2` varchar(255) NOT NULL,
  `stf_insititute3` varchar(255) NOT NULL,
  `stf_position1` varchar(255) NOT NULL,
  `stf_position2` varchar(255) NOT NULL,
  `stf_position3` varchar(255) NOT NULL,
  `stf_period1` varchar(255) NOT NULL,
  `stf_period2` varchar(255) NOT NULL,
  `stf_period3` varchar(255) NOT NULL,
  `stf_pradress` varchar(255) NOT NULL,
  `stf_prcity` varchar(255) NOT NULL,
  `stf_prpincode` varchar(255) NOT NULL,
  `stf_prphonecode` varchar(255) NOT NULL,
  `stf_prstate` varchar(255) NOT NULL,
  `stf_prresino` varchar(255) NOT NULL,
  `stf_prcountry` varchar(255) NOT NULL,
  `stf_prmobno` varchar(255) NOT NULL,
  `stf_peadress` varchar(255) NOT NULL,
  `stf_pecity` varchar(255) NOT NULL,
  `stf_pepincode` varchar(255) NOT NULL,
  `stf_pephoneno` varchar(255) NOT NULL,
  `stf_pestate` varchar(255) NOT NULL,
  `stf_peresino` varchar(255) NOT NULL,
  `stf_pecountry` varchar(255) NOT NULL,
  `stf_pemobileno` varchar(255) NOT NULL,
  `stf_refposname1` varchar(255) NOT NULL,
  `stf_refposname2` varchar(255) NOT NULL,
  `stf_refposname3` varchar(255) NOT NULL,
  `stf_refdesignation1` varchar(255) NOT NULL,
  `stf_refdesignation2` varchar(255) NOT NULL,
  `stf_refdesignation3` varchar(255) NOT NULL,
  `stf_refinsititute1` varchar(255) NOT NULL,
  `stf_refinsititute2` varchar(255) NOT NULL,
  `stf_refinsititute3` varchar(255) NOT NULL,
  `stf_refemail1` varchar(255) NOT NULL,
  `stf_refemail2` varchar(255) NOT NULL,
  `stf_refemail3` varchar(255) NOT NULL,
  `stf_writentest` varchar(255) NOT NULL,
  `stf_technicalinterview` varchar(255) NOT NULL,
  `stf_finalinterview` varchar(255) NOT NULL,
  `stf_status` enum('selected','notselected','onhold','added','dismisied') NOT NULL,
  `stf_perviouspackage` varchar(255) NOT NULL,
  `stf_basic` varchar(255) NOT NULL,
  `stf_dateofjoining` varchar(255) NOT NULL,
  `stf_post` varchar(255) NOT NULL,
  `stf_department` varchar(255) NOT NULL,
  `stf_remarks` varchar(255) NOT NULL,
  `intdate` date NOT NULL,
  `image` varchar(255) NOT NULL,
  `selstatus` enum('issued','notissued','accepted','notaccepted') NOT NULL,
  `tcstatus` enum('notissued','issued','resigned') NOT NULL,
  `st_username` varchar(255) NOT NULL,
  `st_password` varchar(255) NOT NULL,
  `st_theme` varchar(255) NOT NULL,
  `st_class` varchar(255) NOT NULL,
  `st_subject` varchar(255) NOT NULL,
  `st_qualification` varchar(255) NOT NULL,
  `st_marks1` varchar(255) NOT NULL,
  `st_marks2` varchar(255) NOT NULL,
  `st_marks3` varchar(255) NOT NULL,
  `st_permissions` varchar(255) NOT NULL,
  `st_bloodgroup` varchar(255) NOT NULL,
  `teach_nonteach` enum('teaching','nonteaching') NOT NULL DEFAULT 'teaching',
  `st_emailsend` varchar(255) NOT NULL,
  `terminationdate` varchar(255) NOT NULL,
  `hrdsid` varchar(255) NOT NULL,
  `st_mail` varchar(222) NOT NULL,
  `mamber1` varchar(255) NOT NULL,
  `mamber2` varchar(255) NOT NULL,
  `mamber3` varchar(255) NOT NULL,
  `mamber4` varchar(255) NOT NULL,
  `mamber5` varchar(255) NOT NULL,
  `age1` int(11) NOT NULL,
  `age2` int(11) NOT NULL,
  `age3` int(11) NOT NULL,
  `age4` int(11) NOT NULL,
  `age5` int(11) NOT NULL,
  `relation1` varchar(255) NOT NULL,
  `relation2` varchar(255) NOT NULL,
  `relation3` varchar(255) NOT NULL,
  `relation4` varchar(255) NOT NULL,
  `relation5` varchar(255) NOT NULL,
  `education1` varchar(255) NOT NULL,
  `education2` varchar(255) NOT NULL,
  `education3` varchar(255) NOT NULL,
  `education4` varchar(255) NOT NULL,
  `education5` varchar(255) NOT NULL,
  `occupation1` varchar(255) NOT NULL,
  `occupation2` varchar(255) NOT NULL,
  `occupation3` varchar(255) NOT NULL,
  `occupation4` varchar(255) NOT NULL,
  `occupation5` varchar(255) NOT NULL,
  PRIMARY KEY (`stf_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of hgc_staff1
-- ----------------------------

-- ----------------------------
-- Table structure for `hgc_user`
-- ----------------------------
DROP TABLE IF EXISTS `hgc_user`;
CREATE TABLE `hgc_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(75) DEFAULT NULL,
  `user_password` varchar(75) DEFAULT NULL,
  `user_ugroup` int(11) DEFAULT NULL,
  `user_employee` int(11) DEFAULT NULL,
  `user_group` int(11) DEFAULT NULL,
  `user_branch` int(11) DEFAULT NULL,
  `user_status` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hgc_user
-- ----------------------------
INSERT INTO `hgc_user` VALUES ('1', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '1', null, null, null, 'A');
INSERT INTO `hgc_user` VALUES ('2', 'acc', 'e10adc3949ba59abbe56e057f20f883e', '2', '1', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('3', 'reg', 'e10adc3949ba59abbe56e057f20f883e', '3', '2', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('4', 'MB/16/09/PG-1/1', '202cb962ac59075b964b07152d234b70', '4', '1', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('5', 'accng', 'e10adc3949ba59abbe56e057f20f883e', '2', null, '1', '2', 'A');
INSERT INTO `hgc_user` VALUES ('6', 'regng', 'e10adc3949ba59abbe56e057f20f883e', '6', null, '1', '2', 'A');
INSERT INTO `hgc_user` VALUES ('7', 'MB/16/09/PG-1/1', '202cb962ac59075b964b07152d234b70', '4', '1', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('8', 'MB/16/09/PG-2/2', '202cb962ac59075b964b07152d234b70', '4', '2', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('9', 'MB/16/09/PG-1/1', '202cb962ac59075b964b07152d234b70', '4', '1', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('10', 'MB/16/09/PG-2/2', '202cb962ac59075b964b07152d234b70', '4', '2', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('11', 'MB/70/09/PG-1/3', '202cb962ac59075b964b07152d234b70', '4', '3', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('12', 'MB/70/09/PG-2/4', '202cb962ac59075b964b07152d234b70', '4', '4', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('13', 'MB/70/09/PG-3/5', '202cb962ac59075b964b07152d234b70', '4', '5', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('14', 'MB/70/09/PG-4/6', '202cb962ac59075b964b07152d234b70', '4', '6', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('15', 'MB/70/09/PG-5/7', '202cb962ac59075b964b07152d234b70', '4', '7', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('16', 'MB/70/09/PG-6/8', '202cb962ac59075b964b07152d234b70', '4', '8', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('17', 'MB/70/09/PG-7/9', '202cb962ac59075b964b07152d234b70', '4', '9', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('18', 'MB/70/09/PG-8/10', '202cb962ac59075b964b07152d234b70', '4', '10', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('19', 'MB/70/09/PG-9/11', '202cb962ac59075b964b07152d234b70', '4', '11', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('20', 'MB/70/09/PG-10/12', '202cb962ac59075b964b07152d234b70', '4', '12', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('21', 'MB/70/09/PG-11/13', '202cb962ac59075b964b07152d234b70', '4', '13', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('22', 'MB/70/09/PG-12/14', '202cb962ac59075b964b07152d234b70', '4', '14', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('23', 'MB/70/09/PG-13/15', '202cb962ac59075b964b07152d234b70', '4', '15', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('24', 'MB/70/09/PG-14/16', '202cb962ac59075b964b07152d234b70', '4', '16', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('25', 'MB/70/09/PG-15/17', '202cb962ac59075b964b07152d234b70', '4', '17', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('26', 'MB/70/09/PG-16/18', '202cb962ac59075b964b07152d234b70', '4', '18', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('27', 'MB/70/09/PG-17/19', '202cb962ac59075b964b07152d234b70', '4', '19', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('28', 'MB/70/09/PG-18/20', '202cb962ac59075b964b07152d234b70', '4', '20', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('29', 'MB/70/09/PG-19/21', '202cb962ac59075b964b07152d234b70', '4', '21', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('30', 'MB/70/09/PG-20/22', '202cb962ac59075b964b07152d234b70', '4', '22', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('31', 'MB/70/09/PG-21/23', '202cb962ac59075b964b07152d234b70', '4', '23', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('32', 'MB/70/09/PG-22/24', '202cb962ac59075b964b07152d234b70', '4', '24', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('33', 'MB/17/09/Y3-1/25', '202cb962ac59075b964b07152d234b70', '4', '25', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('34', 'MB/70/09/PG-23/26', '202cb962ac59075b964b07152d234b70', '4', '26', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('37', 'MB/70/09/PG-24/27', '202cb962ac59075b964b07152d234b70', '4', '29', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('38', 'MB/70/09/PG-25/28', '202cb962ac59075b964b07152d234b70', '4', '30', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('39', 'MB/70/09/PG-1/1', '202cb962ac59075b964b07152d234b70', '4', '1', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('42', 'MB/70/09/PG-2/2', '202cb962ac59075b964b07152d234b70', '4', '4', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('43', 'MB/70/09/PG-3/3', '202cb962ac59075b964b07152d234b70', '4', '5', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('44', 'MB/70/09/PG-4/4', '202cb962ac59075b964b07152d234b70', '4', '6', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('45', 'MB/70/09/PG-1/1', '202cb962ac59075b964b07152d234b70', '4', '1', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('46', 'MB/70/09/PG-2/2', '202cb962ac59075b964b07152d234b70', '4', '2', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('47', 'MB/70/09/PG-3/3', '202cb962ac59075b964b07152d234b70', '4', '3', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('48', 'MB/70/09/PG-4/4', '202cb962ac59075b964b07152d234b70', '4', '4', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('49', 'MB/70/09/PG-5/5', '202cb962ac59075b964b07152d234b70', '4', '5', '1', '1', 'A');
INSERT INTO `hgc_user` VALUES ('50', 'MB/70/09/PG-6/6', '202cb962ac59075b964b07152d234b70', '4', '6', '1', '1', 'A');

-- ----------------------------
-- Table structure for `hgc_usergroup`
-- ----------------------------
DROP TABLE IF EXISTS `hgc_usergroup`;
CREATE TABLE `hgc_usergroup` (
  `ug_id` int(11) NOT NULL AUTO_INCREMENT,
  `ug_name` varchar(255) NOT NULL,
  `ug_description` text,
  `ug_level` int(11) NOT NULL,
  `ug_company` int(11) DEFAULT NULL,
  `ug_branch` int(11) DEFAULT NULL,
  `ug_status` varchar(5) NOT NULL,
  PRIMARY KEY (`ug_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hgc_usergroup
-- ----------------------------
INSERT INTO `hgc_usergroup` VALUES ('1', 'Admin', null, '1', '1', null, 'A');
INSERT INTO `hgc_usergroup` VALUES ('2', 'Accountant', null, '4', '1', null, 'A');
INSERT INTO `hgc_usergroup` VALUES ('3', 'Registrar', null, '4', '1', null, 'A');
INSERT INTO `hgc_usergroup` VALUES ('4', 'Student', null, '5', null, null, 'A');
INSERT INTO `hgc_usergroup` VALUES ('5', 'Parent', null, '6', null, null, 'A');
INSERT INTO `hgc_usergroup` VALUES ('6', 'regng', null, '4', '1', null, 'A');

-- ----------------------------
-- Table structure for `him_administrator`
-- ----------------------------
DROP TABLE IF EXISTS `him_administrator`;
CREATE TABLE `him_administrator` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `type` enum('super','admin') NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `status` enum('active','inactive','deleted') NOT NULL,
  `created_on` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of him_administrator
-- ----------------------------

-- ----------------------------
-- Table structure for `him_admission`
-- ----------------------------
DROP TABLE IF EXISTS `him_admission`;
CREATE TABLE `him_admission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` enum('Active','Inactive','Deleted') NOT NULL,
  `created_on` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of him_admission
-- ----------------------------

-- ----------------------------
-- Table structure for `him_applyteacher`
-- ----------------------------
DROP TABLE IF EXISTS `him_applyteacher`;
CREATE TABLE `him_applyteacher` (
  `id` int(22) NOT NULL AUTO_INCREMENT,
  `aname` varchar(222) NOT NULL,
  `fname` varchar(222) NOT NULL,
  `mname` varchar(222) NOT NULL,
  `resident` text NOT NULL,
  `postapplied` varchar(222) NOT NULL,
  `classes` varchar(222) NOT NULL,
  `teachingsub` text NOT NULL,
  `experience` text NOT NULL,
  `nameinstitue` varchar(222) NOT NULL,
  `hobbies` varchar(222) NOT NULL,
  `tellus` text NOT NULL,
  `salery` varchar(222) NOT NULL,
  `landlineno` varchar(222) NOT NULL,
  `mobileno` varchar(222) NOT NULL,
  `email` varchar(222) NOT NULL,
  `corrosponding` text NOT NULL,
  `photo` varchar(222) NOT NULL,
  `status` enum('Active','Inactive','Deleted') NOT NULL,
  `created_on` date NOT NULL,
  `downloadapp` varchar(222) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of him_applyteacher
-- ----------------------------

-- ----------------------------
-- Table structure for `him_cms`
-- ----------------------------
DROP TABLE IF EXISTS `him_cms`;
CREATE TABLE `him_cms` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `document1` varchar(255) NOT NULL,
  `document2` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `image` varchar(255) NOT NULL,
  `header_image` varchar(255) NOT NULL,
  `videocode` text NOT NULL,
  `back_ground_image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of him_cms
-- ----------------------------

-- ----------------------------
-- Table structure for `him_country`
-- ----------------------------
DROP TABLE IF EXISTS `him_country`;
CREATE TABLE `him_country` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_id` int(11) NOT NULL DEFAULT '1',
  `country_name` varchar(64) DEFAULT NULL,
  `country_3_code` char(3) DEFAULT NULL,
  `country_2_code` char(2) DEFAULT NULL,
  `country_flag` varchar(255) NOT NULL,
  PRIMARY KEY (`country_id`),
  KEY `idx_country_name` (`country_name`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of him_country
-- ----------------------------

-- ----------------------------
-- Table structure for `him_datasheet`
-- ----------------------------
DROP TABLE IF EXISTS `him_datasheet`;
CREATE TABLE `him_datasheet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `document` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of him_datasheet
-- ----------------------------

-- ----------------------------
-- Table structure for `him_gallery`
-- ----------------------------
DROP TABLE IF EXISTS `him_gallery`;
CREATE TABLE `him_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
  `created_on` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of him_gallery
-- ----------------------------

-- ----------------------------
-- Table structure for `him_settings`
-- ----------------------------
DROP TABLE IF EXISTS `him_settings`;
CREATE TABLE `him_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `constant_name` varchar(255) NOT NULL,
  `field_value` varchar(255) NOT NULL,
  `field_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of him_settings
-- ----------------------------

-- ----------------------------
-- Table structure for `him_toppers`
-- ----------------------------
DROP TABLE IF EXISTS `him_toppers`;
CREATE TABLE `him_toppers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_on` date NOT NULL,
  `details` text NOT NULL,
  `select_front` enum('selected','notselected') NOT NULL DEFAULT 'notselected',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of him_toppers
-- ----------------------------

-- ----------------------------
-- Table structure for `kia_admission`
-- ----------------------------
DROP TABLE IF EXISTS `kia_admission`;
CREATE TABLE `kia_admission` (
  `pre_admissionid` int(11) NOT NULL AUTO_INCREMENT,
  `grp_id` int(11) NOT NULL,
  `br_id` int(11) NOT NULL,
  `es_enquiryid` int(11) NOT NULL,
  `pre_gradeid` int(5) NOT NULL,
  `student_id` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `pre_schoolcatergory` int(5) NOT NULL,
  `pre_familyname` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pre_othername` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pre_sex` int(5) NOT NULL,
  `pre_dob` date NOT NULL,
  `pre_religon` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `pre_nationality` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `pre_mothername` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pre_motheraddress1` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pre_motheraddress2` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pre_mothercity` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `pre_motherhomeno` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `pre_mothermobile` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `pre_motheremail` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `pre_fathername` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pre_fatheraddress1` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pre_fatheraddress2` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pre_fathercity` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `pre_fatherhomeno` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `pre_fathermobile` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `pre_fatheremail` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `pre_personname` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pre_personaddress1` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pre_personaddress2` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pre_personcity` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `pre_personhomeno` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `pre_personmobile` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `pre_personemail` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `pre_emergencyname` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pre_emergencyaddress1` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pre_emergencyaddress2` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pre_emergencycity` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `pre_emergencyhome` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `pre_emergencymobile` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `pre_fatheraffliliation` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `pre_fatherposition` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `pre_fbusinessaddress` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pre_fbusinesstel` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `pre_motheraffliation` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `pre_motherposition` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `pre_motherbusinessaddress` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pre_motherbusinesstel` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `pre_expecteddate` date NOT NULL,
  `pre_status` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `pre_admissiondate` date NOT NULL,
  `pre_emonthofenrolment` int(12) NOT NULL,
  `inv_type` int(5) NOT NULL,
  PRIMARY KEY (`pre_admissionid`),
  KEY `grp_id` (`grp_id`) USING BTREE,
  KEY `br_id` (`br_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kia_admission
-- ----------------------------

-- ----------------------------
-- Table structure for `kia_enquiry`
-- ----------------------------
DROP TABLE IF EXISTS `kia_enquiry`;
CREATE TABLE `kia_enquiry` (
  `es_enquiryid` int(11) NOT NULL AUTO_INCREMENT,
  `eq_fname` varchar(500) NOT NULL,
  `eq_lname` varchar(500) NOT NULL,
  `eq_address` varchar(500) NOT NULL,
  `eq_sex` int(5) NOT NULL,
  `eq_dob` date NOT NULL,
  `category_id` int(5) NOT NULL,
  `eq_egrade` int(5) NOT NULL,
  `eq_emonthofenrolment` varchar(250) NOT NULL,
  `eq_religon` varchar(250) NOT NULL,
  `eq_fathername` varchar(500) NOT NULL,
  `eq_fatheraddress1` varchar(500) NOT NULL,
  `eq_fatheraddress2` varchar(500) NOT NULL,
  `eq_fathercity` varchar(500) NOT NULL,
  `eq_fatheroccupation` varchar(250) NOT NULL,
  `eq_fatherhome` varchar(25) NOT NULL,
  `eq_fathermobile` varchar(25) NOT NULL,
  `eq_fathermail` varchar(25) NOT NULL,
  `eq_mothername` varchar(500) NOT NULL,
  `eq_motheraddress1` varchar(500) NOT NULL,
  `eq_motheraddress2` varchar(500) NOT NULL,
  `eq_mothercity` varchar(250) NOT NULL,
  `eq_motheroccupation` varchar(250) NOT NULL,
  `eq_motherhome` varchar(25) NOT NULL,
  `eq_mothermobile` varchar(25) NOT NULL,
  `eq_aboutkia` varchar(500) NOT NULL,
  `eq_livingperent` int(5) NOT NULL,
  `eq_perentsepate` int(5) NOT NULL,
  `eq_personname` varchar(500) NOT NULL,
  `eq_personaddress1` varchar(500) NOT NULL,
  `eq_personaddress2` varchar(500) NOT NULL,
  `eq_personcity` varchar(250) NOT NULL,
  `eq_personoccupation` varchar(250) NOT NULL,
  `eq_personhome` varchar(25) NOT NULL,
  `eq_personmobile` varchar(25) NOT NULL,
  `eq_learningdisability` int(5) NOT NULL,
  `eq_learningdisabilitydetails` varchar(500) NOT NULL,
  `eq_physicaldisability` int(5) NOT NULL,
  `eq_physicaldisabilitydetails` varchar(500) NOT NULL,
  `eq_allergic` int(5) NOT NULL,
  `eq_allergicdetails` varchar(500) NOT NULL,
  `eq_doctorcare` int(5) NOT NULL,
  `eq_doctorcaredetails` varchar(500) NOT NULL,
  `eq_createdon` date NOT NULL,
  `grp_id` int(11) NOT NULL,
  `br_id` int(11) NOT NULL,
  PRIMARY KEY (`es_enquiryid`),
  KEY `grp_id` (`grp_id`) USING BTREE,
  KEY `br_id` (`br_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of kia_enquiry
-- ----------------------------

-- ----------------------------
-- Table structure for `kia_feestructure`
-- ----------------------------
DROP TABLE IF EXISTS `kia_feestructure`;
CREATE TABLE `kia_feestructure` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grp_id` int(11) NOT NULL,
  `br_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `admission_fee` decimal(10,2) NOT NULL,
  `refundable_fee` decimal(10,2) NOT NULL,
  `monthly_fee` decimal(10,2) NOT NULL,
  `halfmonth_fee` decimal(10,2) NOT NULL,
  `other_fee` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `br_id` (`br_id`) USING BTREE,
  KEY `category_id` (`category_id`) USING BTREE,
  KEY `grp_id` (`grp_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=FIXED;

-- ----------------------------
-- Records of kia_feestructure
-- ----------------------------

-- ----------------------------
-- Table structure for `kia_grade`
-- ----------------------------
DROP TABLE IF EXISTS `kia_grade`;
CREATE TABLE `kia_grade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grade_code` varchar(50) NOT NULL,
  `grade_name` varchar(250) NOT NULL,
  `status` varchar(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of kia_grade
-- ----------------------------

-- ----------------------------
-- Table structure for `kia_grade_seq`
-- ----------------------------
DROP TABLE IF EXISTS `kia_grade_seq`;
CREATE TABLE `kia_grade_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grade_id` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `number` int(11) NOT NULL,
  `branch` int(11) NOT NULL,
  `branch_seq` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kia_grade_seq
-- ----------------------------

-- ----------------------------
-- Table structure for `kia_invdetails`
-- ----------------------------
DROP TABLE IF EXISTS `kia_invdetails`;
CREATE TABLE `kia_invdetails` (
  `inv_id` int(11) NOT NULL AUTO_INCREMENT,
  `fee_id` int(11) NOT NULL,
  `br_id` int(11) NOT NULL,
  `admission_id` int(11) NOT NULL,
  `stu_id` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `catogory_id` int(11) NOT NULL,
  `admission_fee` decimal(15,2) NOT NULL,
  `monthly_fee` decimal(15,2) NOT NULL,
  `nbt` decimal(10,2) NOT NULL,
  `refun_fee` decimal(15,2) NOT NULL,
  `stk_id` int(11) NOT NULL,
  `other_charges` decimal(20,2) NOT NULL,
  `other_reason` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `discount` decimal(10,2) NOT NULL,
  `dis_reason` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `tax_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `tot_sum` decimal(10,2) NOT NULL,
  `pay_method` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `pay_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `card_no` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `crate_date` date NOT NULL,
  `month` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `bankname` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `cheque_no` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `inv_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`inv_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kia_invdetails
-- ----------------------------

-- ----------------------------
-- Table structure for `kia_invoice`
-- ----------------------------
DROP TABLE IF EXISTS `kia_invoice`;
CREATE TABLE `kia_invoice` (
  `in_id` int(11) NOT NULL AUTO_INCREMENT,
  `fee_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `br_id` int(11) NOT NULL,
  `stu_id` int(11) NOT NULL,
  PRIMARY KEY (`in_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of kia_invoice
-- ----------------------------

-- ----------------------------
-- Table structure for `kia_month`
-- ----------------------------
DROP TABLE IF EXISTS `kia_month`;
CREATE TABLE `kia_month` (
  `mnth_id` int(11) NOT NULL AUTO_INCREMENT,
  `month_seq` text COLLATE utf8_unicode_ci NOT NULL,
  `mnth_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`mnth_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kia_month
-- ----------------------------

-- ----------------------------
-- Table structure for `kia_paymentdetails`
-- ----------------------------
DROP TABLE IF EXISTS `kia_paymentdetails`;
CREATE TABLE `kia_paymentdetails` (
  `pay_id` int(11) NOT NULL AUTO_INCREMENT,
  `inv_id` int(11) NOT NULL,
  `admission` decimal(15,2) NOT NULL,
  `outs` decimal(10,2) NOT NULL,
  `monthly` decimal(15,2) NOT NULL,
  `refund` decimal(15,2) NOT NULL,
  `nbt` decimal(15,2) NOT NULL,
  `tot_amount` decimal(15,2) NOT NULL,
  `uniform` decimal(15,2) NOT NULL,
  `allocation` decimal(15,2) NOT NULL,
  `pay_method` int(11) NOT NULL,
  `bankname` varchar(250) NOT NULL,
  `cheque_no` int(11) NOT NULL,
  `pay_type` int(10) NOT NULL,
  `card_no` varchar(25) NOT NULL,
  `crate_date` date NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`pay_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of kia_paymentdetails
-- ----------------------------

-- ----------------------------
-- Table structure for `kia_schcatagory`
-- ----------------------------
DROP TABLE IF EXISTS `kia_schcatagory`;
CREATE TABLE `kia_schcatagory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grp_id` int(11) NOT NULL,
  `br_id` int(11) NOT NULL,
  `catagory_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `grp_id` (`grp_id`) USING BTREE,
  KEY `br_id` (`br_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kia_schcatagory
-- ----------------------------

-- ----------------------------
-- Table structure for `kia_stock`
-- ----------------------------
DROP TABLE IF EXISTS `kia_stock`;
CREATE TABLE `kia_stock` (
  `stk_id` int(11) NOT NULL AUTO_INCREMENT,
  `grp_id` int(11) NOT NULL,
  `br_id` int(11) NOT NULL,
  `item_name` varchar(250) NOT NULL,
  `item_price` decimal(10,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`stk_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of kia_stock
-- ----------------------------

-- ----------------------------
-- Table structure for `kia_stumonthinvdetail`
-- ----------------------------
DROP TABLE IF EXISTS `kia_stumonthinvdetail`;
CREATE TABLE `kia_stumonthinvdetail` (
  `smid_id` int(11) NOT NULL AUTO_INCREMENT,
  `smid_description` varchar(50) NOT NULL,
  `smid_glcode` varchar(11) NOT NULL,
  `smid_monthinv` int(20) NOT NULL,
  `smid_amount` decimal(10,2) NOT NULL,
  `smid_rdcrtype` varchar(20) NOT NULL,
  `smid_action` varchar(10) NOT NULL,
  PRIMARY KEY (`smid_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of kia_stumonthinvdetail
-- ----------------------------

-- ----------------------------
-- Table structure for `kia_stumonthlyinvoice`
-- ----------------------------
DROP TABLE IF EXISTS `kia_stumonthlyinvoice`;
CREATE TABLE `kia_stumonthlyinvoice` (
  `smi_id` int(11) NOT NULL AUTO_INCREMENT,
  `smi_invoice` int(11) NOT NULL,
  `smi_processdate` date NOT NULL,
  `smi_student` int(11) NOT NULL,
  `smi_subnumber` int(11) NOT NULL,
  `smi_status` varchar(20) NOT NULL,
  PRIMARY KEY (`smi_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of kia_stumonthlyinvoice
-- ----------------------------

-- ----------------------------
-- Table structure for `kia_tax`
-- ----------------------------
DROP TABLE IF EXISTS `kia_tax`;
CREATE TABLE `kia_tax` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=FIXED;

-- ----------------------------
-- Records of kia_tax
-- ----------------------------

-- ----------------------------
-- Table structure for `mlc_college`
-- ----------------------------
DROP TABLE IF EXISTS `mlc_college`;
CREATE TABLE `mlc_college` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `header_image` varchar(255) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL,
  `created_on` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of mlc_college
-- ----------------------------

-- ----------------------------
-- Table structure for `mlc_document`
-- ----------------------------
DROP TABLE IF EXISTS `mlc_document`;
CREATE TABLE `mlc_document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `document` varchar(255) NOT NULL,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
  `created_on` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of mlc_document
-- ----------------------------

-- ----------------------------
-- Table structure for `mlc_download`
-- ----------------------------
DROP TABLE IF EXISTS `mlc_download`;
CREATE TABLE `mlc_download` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `document` varchar(255) NOT NULL,
  `created_on` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of mlc_download
-- ----------------------------

-- ----------------------------
-- Table structure for `mlc_feedback`
-- ----------------------------
DROP TABLE IF EXISTS `mlc_feedback`;
CREATE TABLE `mlc_feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first` varchar(255) NOT NULL,
  `last` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `created_on` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of mlc_feedback
-- ----------------------------

-- ----------------------------
-- Table structure for `mlc_homenews`
-- ----------------------------
DROP TABLE IF EXISTS `mlc_homenews`;
CREATE TABLE `mlc_homenews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
  `created_on` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of mlc_homenews
-- ----------------------------

-- ----------------------------
-- Table structure for `mlc_news`
-- ----------------------------
DROP TABLE IF EXISTS `mlc_news`;
CREATE TABLE `mlc_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
  `created_on` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of mlc_news
-- ----------------------------

-- ----------------------------
-- Table structure for `mlc_newsletters`
-- ----------------------------
DROP TABLE IF EXISTS `mlc_newsletters`;
CREATE TABLE `mlc_newsletters` (
  `news_id` int(11) NOT NULL AUTO_INCREMENT,
  `news_title` varchar(255) NOT NULL,
  `news_desc` longtext NOT NULL,
  `news_doc` varchar(255) NOT NULL,
  `news_status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created_on` date NOT NULL,
  PRIMARY KEY (`news_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of mlc_newsletters
-- ----------------------------

-- ----------------------------
-- Table structure for `mlc_school_policies`
-- ----------------------------
DROP TABLE IF EXISTS `mlc_school_policies`;
CREATE TABLE `mlc_school_policies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
  `created_on` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of mlc_school_policies
-- ----------------------------

-- ----------------------------
-- Table structure for `mlc_subscribers`
-- ----------------------------
DROP TABLE IF EXISTS `mlc_subscribers`;
CREATE TABLE `mlc_subscribers` (
  `subid` int(11) NOT NULL AUTO_INCREMENT,
  `sub_email` varchar(255) NOT NULL,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
  `created_on` datetime DEFAULT NULL,
  PRIMARY KEY (`subid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of mlc_subscribers
-- ----------------------------

-- ----------------------------
-- Table structure for `mlc_yourcomment`
-- ----------------------------
DROP TABLE IF EXISTS `mlc_yourcomment`;
CREATE TABLE `mlc_yourcomment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `postal_code` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `fax` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `comments` varchar(255) NOT NULL,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
  `created_on` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of mlc_yourcomment
-- ----------------------------

-- ----------------------------
-- Table structure for `st_details`
-- ----------------------------
DROP TABLE IF EXISTS `st_details`;
CREATE TABLE `st_details` (
  `st_id` varchar(255) DEFAULT NULL,
  `ref` varchar(255) DEFAULT NULL,
  `st_grade` int(11) DEFAULT NULL,
  `grade` varchar(255) DEFAULT NULL,
  `family_name` varchar(255) DEFAULT NULL,
  `other_names` varchar(255) DEFAULT NULL,
  `gender` char(1) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `local_nationality` varchar(255) DEFAULT NULL,
  `st_religion` int(11) DEFAULT NULL,
  `religion` varchar(255) DEFAULT NULL,
  `citizenship` varchar(255) DEFAULT NULL,
  `foreign_nationality` varchar(255) DEFAULT NULL,
  `passport` varchar(255) DEFAULT NULL,
  `issue` varchar(255) DEFAULT NULL,
  `language` varchar(255) DEFAULT NULL,
  `lang_home` varchar(255) DEFAULT NULL,
  `mom_name` varchar(255) DEFAULT NULL,
  `mom_addy` varchar(255) DEFAULT NULL,
  `mom_home` varchar(255) DEFAULT NULL,
  `mom_mobile` varchar(255) DEFAULT NULL,
  `mom_email` varchar(255) DEFAULT NULL,
  `dad_name` varchar(255) DEFAULT NULL,
  `dad_addy` varchar(255) DEFAULT NULL,
  `dad_home` varchar(255) DEFAULT NULL,
  `dad_mobile` varchar(255) DEFAULT NULL,
  `dad_email` varchar(255) DEFAULT NULL,
  `guar_name` varchar(255) DEFAULT NULL,
  `guar_addy` varchar(255) DEFAULT NULL,
  `guar_home` varchar(255) DEFAULT NULL,
  `guar_mobile` varchar(255) DEFAULT NULL,
  `guar_email` varchar(255) DEFAULT NULL,
  `emg_name` varchar(255) DEFAULT NULL,
  `emg_addy` varchar(255) DEFAULT NULL,
  `emg_home` varchar(255) DEFAULT NULL,
  `emg_mobile` varchar(255) DEFAULT NULL,
  `school_age` varchar(255) DEFAULT NULL,
  `1sch_name` varchar(255) DEFAULT NULL,
  `1sch_lang` varchar(255) DEFAULT NULL,
  `1sch_addy` varchar(255) DEFAULT NULL,
  `ffrom` date DEFAULT NULL,
  `fto` date DEFAULT NULL,
  `1st_gc` varchar(255) DEFAULT NULL,
  `2sch_name` varchar(255) DEFAULT NULL,
  `2sch_lang` varchar(255) DEFAULT NULL,
  `2sch_addy` varchar(255) DEFAULT NULL,
  `sfrom` date DEFAULT NULL,
  `sto` date DEFAULT NULL,
  `2nd_gc` varchar(255) DEFAULT NULL,
  `gc` varchar(255) DEFAULT NULL,
  `comp_date` date DEFAULT NULL,
  `mgt_wd` varchar(255) DEFAULT NULL,
  `mgt_exp` varchar(255) DEFAULT NULL,
  `int_act` varchar(255) DEFAULT NULL,
  `eng_xp` varchar(255) DEFAULT NULL,
  `eng_sit` varchar(255) DEFAULT NULL,
  `eng_len` varchar(255) DEFAULT NULL,
  `enr_sped` varchar(255) DEFAULT NULL,
  `sped_xp` varchar(255) DEFAULT NULL,
  `med_test` varchar(255) DEFAULT NULL,
  `med_xp` varchar(255) DEFAULT NULL,
  `med_dis` varchar(255) DEFAULT NULL,
  `dis_xp` varchar(255) DEFAULT NULL,
  `mom_aff` varchar(255) DEFAULT NULL,
  `mom_pos` varchar(255) DEFAULT NULL,
  `mom_biz_addy` varchar(255) DEFAULT NULL,
  `mom_biz_tel` varchar(255) DEFAULT NULL,
  `dad_aff` varchar(255) DEFAULT NULL,
  `dad_pos` varchar(255) DEFAULT NULL,
  `dad_biz_addy` varchar(255) DEFAULT NULL,
  `dad_biz_tel` varchar(255) DEFAULT NULL,
  `st_prefgrade` int(11) DEFAULT NULL,
  `pref_grade` varchar(255) DEFAULT NULL,
  `curriculum` varchar(255) DEFAULT NULL,
  `ex_date` date DEFAULT NULL,
  `len_stay` int(11) DEFAULT '0',
  `route` varchar(255) DEFAULT NULL,
  `pickup` varchar(255) DEFAULT NULL,
  `dropoff` varchar(255) DEFAULT NULL,
  `blood` varchar(255) DEFAULT NULL,
  `class` varchar(255) DEFAULT NULL,
  `st_curgrade` int(11) DEFAULT NULL,
  `cur_grade` varchar(255) DEFAULT 'in',
  `st_status` varchar(5) DEFAULT NULL,
  `stat` varchar(255) DEFAULT 'in',
  `town` varchar(255) DEFAULT NULL,
  `wd_date` date DEFAULT NULL,
  `wd_reason` varchar(255) DEFAULT NULL,
  `exclude_run` int(11) DEFAULT '0',
  `bill_template` varchar(255) DEFAULT NULL,
  `admin_template` varchar(255) DEFAULT NULL,
  `bill_audit` varchar(255) DEFAULT NULL,
  `admin_audit` varchar(255) DEFAULT NULL,
  `bill_audit_edit` varchar(255) DEFAULT NULL,
  `admin_audit_edit` varchar(255) DEFAULT NULL,
  `mail_to` varchar(1) DEFAULT 'F',
  `intake` varchar(255) DEFAULT NULL,
  `st_intake` int(11) DEFAULT NULL,
  `first_child` varchar(50) DEFAULT '',
  `st_accyear` int(11) DEFAULT NULL,
  `entered_date` date DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exclude_transport` int(255) DEFAULT '0',
  `st_enqid` int(11) DEFAULT NULL,
  `st_gender` int(11) DEFAULT NULL,
  `mom_add2` varchar(255) DEFAULT NULL,
  `mom_addcity` varchar(255) DEFAULT NULL,
  `dad_add2` varchar(255) DEFAULT NULL,
  `dad_addcity` varchar(255) DEFAULT NULL,
  `guar_add2` varchar(255) DEFAULT NULL,
  `guar_addcity` varchar(255) DEFAULT NULL,
  `emg_add2` varchar(255) DEFAULT NULL,
  `emg_addcity` varchar(255) DEFAULT NULL,
  `1sch_add2` varchar(255) DEFAULT NULL,
  `1sch_addcity` varchar(255) DEFAULT NULL,
  `st_prev_wd` int(11) DEFAULT NULL,
  `st_prevreason` varchar(255) DEFAULT NULL,
  `st_prev_acts` text,
  `st_englishexperience` text,
  `st_admintemplate` int(11) DEFAULT NULL,
  `st_termtemplate` int(11) DEFAULT NULL,
  `st_feestructure` int(11) DEFAULT NULL,
  `st_addpaymethod` tinyint(4) DEFAULT NULL,
  `st_infComPerson` int(11) DEFAULT NULL,
  `st_lastpromoted` int(11) DEFAULT '0',
  `st_includetrans` int(11) DEFAULT NULL,
  `st_transregisno` int(11) DEFAULT NULL,
  `st_term` int(11) NOT NULL DEFAULT '0',
  `st_motherstaff` int(11) DEFAULT NULL,
  `st_fatherstaff` int(11) DEFAULT NULL,
  `mom_alt` varchar(255) DEFAULT NULL,
  `mom_fornum` text,
  `dad_alt` varchar(255) DEFAULT NULL,
  `dad_fornum` text,
  `st_currclass` int(11) DEFAULT NULL,
  `st_branch` int(11) DEFAULT NULL,
  `st_tempreid` int(11) DEFAULT NULL,
  `st_leftyear` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of st_details
-- ----------------------------
INSERT INTO `st_details` VALUES ('MB/70/09/PG-1/1', null, '1', null, 'uyuyuyu', 'tyytytyty', 'M', '0000-00-00', '', '0', null, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '0000-00-00', null, null, null, null, null, null, null, null, null, null, '', '', null, '', '', null, '', null, '', null, '', '', '', '', '', '', '', '', '', null, null, '', '0000-00-00', '0', null, null, null, null, null, '0', 'in', 'R', 'in', null, null, null, '0', null, null, null, null, null, null, 'F', null, '3', '', '0', '2018-04-06', '1', '0', null, null, '', '', '', '', '', '', '', '', '', '', null, null, null, null, '1', '2', '1', null, '1', '0', null, null, '5', '0', '0', '', '', '', '', null, '1', '1', null);
INSERT INTO `st_details` VALUES ('MB/70/09/PG-2/2', null, '1', null, 'ghfghfgh', 'hghgfhgfh', null, '0000-00-00', '', '0', null, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '0000-00-00', null, null, null, null, null, null, null, null, null, null, '', '', null, '', '', null, '', null, '', null, '', '', '', '', '', '', '', '', '', null, null, '', '0000-00-00', '0', null, null, null, null, null, '0', 'in', 'R', 'in', null, null, null, '0', null, null, null, null, null, null, 'F', null, '3', '', '0', '2018-04-06', '2', '0', null, null, '', '', '', '', '', '', '', '', '', '', null, null, null, null, '1', '2', '1', null, '1', '0', null, null, '5', '0', '0', '', '', '', '', null, '1', '2', null);
INSERT INTO `st_details` VALUES ('MB/70/09/PG-3/3', null, '1', null, 'fdgdgdfg', 'fgfdgdfg', null, '0000-00-00', '', '0', null, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '0000-00-00', null, null, null, null, null, null, null, null, null, null, '', '', null, '', '', null, '', null, '', null, '', '', '', '', '', '', '', '', '', null, null, '', '0000-00-00', '0', null, null, null, null, null, '0', 'in', 'R', 'in', null, null, null, '0', null, null, null, null, null, null, 'F', null, '3', '', '0', '2018-04-09', '3', '0', null, null, '', '', '', '', '', '', '', '', '', '', null, null, null, null, '1', '2', '1', null, '1', '0', null, null, '5', '0', '0', '', '', '', '', null, '1', '5', null);
INSERT INTO `st_details` VALUES ('MB/70/09/PG-4/4', null, '1', null, 'fgdfgf', 'fgfg', null, '0000-00-00', '', '0', null, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '0000-00-00', null, null, null, null, null, null, null, null, null, null, '', '', null, '', '', null, '', null, '', null, '', '', '', '', '', '', '', '', '', null, null, '', '0000-00-00', '0', null, null, null, null, null, '0', 'in', 'R', 'in', null, null, null, '0', null, null, null, null, null, null, 'F', null, '3', '', '0', '2018-04-09', '4', '0', null, null, '', '', '', '', '', '', '', '', '', '', null, null, null, null, '1', '2', '1', null, '1', '0', null, null, '5', '0', '0', '', '', '', '', null, '1', '6', null);
INSERT INTO `st_details` VALUES ('MB/70/09/PG-5/5', null, '1', null, 'gdfgdfffffffffffffffff', 'fdgdfgdfg', null, '0000-00-00', '', '0', null, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '0000-00-00', null, null, null, null, null, null, null, null, null, null, '', '', null, '', '', null, '', null, '', null, '', '', '', '', '', '', '', '', '', null, null, '', '0000-00-00', '0', null, null, null, null, null, '0', 'in', 'R', 'in', null, null, null, '0', null, null, null, null, null, null, 'F', null, '3', '', '0', '2018-04-09', '5', '0', null, null, '', '', '', '', '', '', '', '', '', '', null, null, null, null, '1', '2', '1', null, '1', '0', null, null, '5', '0', '0', '', '', '', '', null, '1', '7', null);
INSERT INTO `st_details` VALUES ('MB/70/09/PG-6/6', null, '1', null, 'ffgdfgdgdfg', 'gfgdfgdfg', null, '0000-00-00', '', '0', null, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '0000-00-00', null, null, null, null, null, null, null, null, null, null, '', '', null, '', '', null, '', null, '', null, '', '', '', '', '', '', '', '', '', null, null, '', '0000-00-00', '0', null, null, null, null, null, '0', 'in', 'R', 'in', null, null, null, '0', null, null, null, null, null, null, 'F', null, '3', '', '0', '2018-04-09', '6', '0', null, null, '', '', '', '', '', '', '', '', '', '', null, null, null, null, '1', '2', '1', null, '1', '0', null, null, '5', '0', '0', '', '', '', '', null, '1', '8', null);

-- ----------------------------
-- Table structure for `st_details_copy`
-- ----------------------------
DROP TABLE IF EXISTS `st_details_copy`;
CREATE TABLE `st_details_copy` (
  `st_id` varchar(255) DEFAULT NULL,
  `ref` varchar(255) DEFAULT NULL,
  `st_grade` int(11) DEFAULT NULL,
  `grade` varchar(255) DEFAULT NULL,
  `family_name` varchar(255) DEFAULT NULL,
  `other_names` varchar(255) DEFAULT NULL,
  `gender` char(1) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `local_nationality` varchar(255) DEFAULT NULL,
  `st_religion` int(11) DEFAULT NULL,
  `religion` varchar(255) DEFAULT NULL,
  `citizenship` varchar(255) DEFAULT NULL,
  `foreign_nationality` varchar(255) DEFAULT NULL,
  `passport` varchar(255) DEFAULT NULL,
  `issue` varchar(255) DEFAULT NULL,
  `language` varchar(255) DEFAULT NULL,
  `lang_home` varchar(255) DEFAULT NULL,
  `mom_name` varchar(255) DEFAULT NULL,
  `mom_addy` varchar(255) DEFAULT NULL,
  `mom_home` varchar(255) DEFAULT NULL,
  `mom_mobile` varchar(255) DEFAULT NULL,
  `mom_email` varchar(255) DEFAULT NULL,
  `dad_name` varchar(255) DEFAULT NULL,
  `dad_addy` varchar(255) DEFAULT NULL,
  `dad_home` varchar(255) DEFAULT NULL,
  `dad_mobile` varchar(255) DEFAULT NULL,
  `dad_email` varchar(255) DEFAULT NULL,
  `guar_name` varchar(255) DEFAULT NULL,
  `guar_addy` varchar(255) DEFAULT NULL,
  `guar_home` varchar(255) DEFAULT NULL,
  `guar_mobile` varchar(255) DEFAULT NULL,
  `guar_email` varchar(255) DEFAULT NULL,
  `emg_name` varchar(255) DEFAULT NULL,
  `emg_addy` varchar(255) DEFAULT NULL,
  `emg_home` varchar(255) DEFAULT NULL,
  `emg_mobile` varchar(255) DEFAULT NULL,
  `school_age` varchar(255) DEFAULT NULL,
  `1sch_name` varchar(255) DEFAULT NULL,
  `1sch_lang` varchar(255) DEFAULT NULL,
  `1sch_addy` varchar(255) DEFAULT NULL,
  `ffrom` date DEFAULT NULL,
  `fto` date DEFAULT NULL,
  `1st_gc` varchar(255) DEFAULT NULL,
  `2sch_name` varchar(255) DEFAULT NULL,
  `2sch_lang` varchar(255) DEFAULT NULL,
  `2sch_addy` varchar(255) DEFAULT NULL,
  `sfrom` date DEFAULT NULL,
  `sto` date DEFAULT NULL,
  `2nd_gc` varchar(255) DEFAULT NULL,
  `gc` varchar(255) DEFAULT NULL,
  `comp_date` date DEFAULT NULL,
  `mgt_wd` varchar(255) DEFAULT NULL,
  `mgt_exp` varchar(255) DEFAULT NULL,
  `int_act` varchar(255) DEFAULT NULL,
  `eng_xp` varchar(255) DEFAULT NULL,
  `eng_sit` varchar(255) DEFAULT NULL,
  `eng_len` varchar(255) DEFAULT NULL,
  `enr_sped` varchar(255) DEFAULT NULL,
  `sped_xp` varchar(255) DEFAULT NULL,
  `med_test` varchar(255) DEFAULT NULL,
  `med_xp` varchar(255) DEFAULT NULL,
  `med_dis` varchar(255) DEFAULT NULL,
  `dis_xp` varchar(255) DEFAULT NULL,
  `mom_aff` varchar(255) DEFAULT NULL,
  `mom_pos` varchar(255) DEFAULT NULL,
  `mom_biz_addy` varchar(255) DEFAULT NULL,
  `mom_biz_tel` varchar(255) DEFAULT NULL,
  `dad_aff` varchar(255) DEFAULT NULL,
  `dad_pos` varchar(255) DEFAULT NULL,
  `dad_biz_addy` varchar(255) DEFAULT NULL,
  `dad_biz_tel` varchar(255) DEFAULT NULL,
  `st_prefgrade` int(11) DEFAULT NULL,
  `pref_grade` varchar(255) DEFAULT NULL,
  `curriculum` varchar(255) DEFAULT NULL,
  `ex_date` date DEFAULT NULL,
  `len_stay` int(11) DEFAULT '0',
  `route` varchar(255) DEFAULT NULL,
  `pickup` varchar(255) DEFAULT NULL,
  `dropoff` varchar(255) DEFAULT NULL,
  `blood` varchar(255) DEFAULT NULL,
  `class` varchar(255) DEFAULT NULL,
  `st_curgrade` int(11) DEFAULT NULL,
  `cur_grade` varchar(255) DEFAULT 'in',
  `st_status` varchar(5) DEFAULT NULL,
  `stat` varchar(255) DEFAULT 'in',
  `town` varchar(255) DEFAULT NULL,
  `wd_date` date DEFAULT NULL,
  `wd_reason` varchar(255) DEFAULT NULL,
  `exclude_run` int(11) DEFAULT '0',
  `bill_template` varchar(255) DEFAULT NULL,
  `admin_template` varchar(255) DEFAULT NULL,
  `bill_audit` varchar(255) DEFAULT NULL,
  `admin_audit` varchar(255) DEFAULT NULL,
  `bill_audit_edit` varchar(255) DEFAULT NULL,
  `admin_audit_edit` varchar(255) DEFAULT NULL,
  `mail_to` varchar(1) DEFAULT 'F',
  `intake` varchar(255) DEFAULT NULL,
  `st_intake` int(11) DEFAULT NULL,
  `first_child` varchar(50) DEFAULT '',
  `st_accyear` int(11) DEFAULT NULL,
  `entered_date` date DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exclude_transport` int(255) DEFAULT '0',
  `st_enqid` int(11) DEFAULT NULL,
  `st_gender` int(11) DEFAULT NULL,
  `mom_add2` varchar(255) DEFAULT NULL,
  `mom_addcity` varchar(255) DEFAULT NULL,
  `dad_add2` varchar(255) DEFAULT NULL,
  `dad_addcity` varchar(255) DEFAULT NULL,
  `guar_add2` varchar(255) DEFAULT NULL,
  `guar_addcity` varchar(255) DEFAULT NULL,
  `emg_add2` varchar(255) DEFAULT NULL,
  `emg_addcity` varchar(255) DEFAULT NULL,
  `1sch_add2` varchar(255) DEFAULT NULL,
  `1sch_addcity` varchar(255) DEFAULT NULL,
  `st_prev_wd` int(11) DEFAULT NULL,
  `st_prevreason` varchar(255) DEFAULT NULL,
  `st_prev_acts` text,
  `st_englishexperience` text,
  `st_feestructure` int(11) DEFAULT NULL,
  `st_addpaymethod` tinyint(4) DEFAULT NULL,
  `st_infComPerson` int(11) DEFAULT NULL,
  `st_lastpromoted` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of st_details_copy
-- ----------------------------

-- ----------------------------
-- Table structure for `st_details_temp`
-- ----------------------------
DROP TABLE IF EXISTS `st_details_temp`;
CREATE TABLE `st_details_temp` (
  `st_id` varchar(255) DEFAULT NULL,
  `ref` varchar(255) DEFAULT NULL,
  `st_grade` int(11) DEFAULT NULL,
  `grade` varchar(255) DEFAULT NULL,
  `family_name` varchar(255) DEFAULT NULL,
  `other_names` varchar(255) DEFAULT NULL,
  `gender` char(1) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `local_nationality` varchar(255) DEFAULT NULL,
  `st_religion` int(11) DEFAULT NULL,
  `religion` varchar(255) DEFAULT NULL,
  `citizenship` varchar(255) DEFAULT NULL,
  `foreign_nationality` varchar(255) DEFAULT NULL,
  `passport` varchar(255) DEFAULT NULL,
  `issue` varchar(255) DEFAULT NULL,
  `language` varchar(255) DEFAULT NULL,
  `lang_home` varchar(255) DEFAULT NULL,
  `mom_name` varchar(255) DEFAULT NULL,
  `mom_addy` varchar(255) DEFAULT NULL,
  `mom_home` varchar(255) DEFAULT NULL,
  `mom_mobile` varchar(255) DEFAULT NULL,
  `mom_email` varchar(255) DEFAULT NULL,
  `dad_name` varchar(255) DEFAULT NULL,
  `dad_addy` varchar(255) DEFAULT NULL,
  `dad_home` varchar(255) DEFAULT NULL,
  `dad_mobile` varchar(255) DEFAULT NULL,
  `dad_email` varchar(255) DEFAULT NULL,
  `guar_name` varchar(255) DEFAULT NULL,
  `guar_addy` varchar(255) DEFAULT NULL,
  `guar_home` varchar(255) DEFAULT NULL,
  `guar_mobile` varchar(255) DEFAULT NULL,
  `guar_email` varchar(255) DEFAULT NULL,
  `emg_name` varchar(255) DEFAULT NULL,
  `emg_addy` varchar(255) DEFAULT NULL,
  `emg_home` varchar(255) DEFAULT NULL,
  `emg_mobile` varchar(255) DEFAULT NULL,
  `school_age` varchar(255) DEFAULT NULL,
  `1sch_name` varchar(255) DEFAULT NULL,
  `1sch_lang` varchar(255) DEFAULT NULL,
  `1sch_addy` varchar(255) DEFAULT NULL,
  `ffrom` date DEFAULT NULL,
  `fto` date DEFAULT NULL,
  `1st_gc` varchar(255) DEFAULT NULL,
  `2sch_name` varchar(255) DEFAULT NULL,
  `2sch_lang` varchar(255) DEFAULT NULL,
  `2sch_addy` varchar(255) DEFAULT NULL,
  `sfrom` date DEFAULT NULL,
  `sto` date DEFAULT NULL,
  `2nd_gc` varchar(255) DEFAULT NULL,
  `gc` varchar(255) DEFAULT NULL,
  `comp_date` date DEFAULT NULL,
  `mgt_wd` varchar(255) DEFAULT NULL,
  `mgt_exp` varchar(255) DEFAULT NULL,
  `int_act` varchar(255) DEFAULT NULL,
  `eng_xp` varchar(255) DEFAULT NULL,
  `eng_sit` varchar(255) DEFAULT NULL,
  `eng_len` varchar(255) DEFAULT NULL,
  `enr_sped` varchar(255) DEFAULT NULL,
  `sped_xp` varchar(255) DEFAULT NULL,
  `med_test` varchar(255) DEFAULT NULL,
  `med_xp` varchar(255) DEFAULT NULL,
  `med_dis` varchar(255) DEFAULT NULL,
  `dis_xp` varchar(255) DEFAULT NULL,
  `mom_aff` varchar(255) DEFAULT NULL,
  `mom_pos` varchar(255) DEFAULT NULL,
  `mom_biz_addy` varchar(255) DEFAULT NULL,
  `mom_biz_tel` varchar(255) DEFAULT NULL,
  `dad_aff` varchar(255) DEFAULT NULL,
  `dad_pos` varchar(255) DEFAULT NULL,
  `dad_biz_addy` varchar(255) DEFAULT NULL,
  `dad_biz_tel` varchar(255) DEFAULT NULL,
  `st_prefgrade` int(11) DEFAULT NULL,
  `pref_grade` varchar(255) DEFAULT NULL,
  `curriculum` varchar(255) DEFAULT NULL,
  `ex_date` date DEFAULT NULL,
  `len_stay` int(11) DEFAULT '0',
  `route` varchar(255) DEFAULT NULL,
  `pickup` varchar(255) DEFAULT NULL,
  `dropoff` varchar(255) DEFAULT NULL,
  `blood` varchar(255) DEFAULT NULL,
  `class` varchar(255) DEFAULT NULL,
  `st_curgrade` int(11) DEFAULT NULL,
  `cur_grade` varchar(255) DEFAULT 'in',
  `st_status` varchar(5) DEFAULT NULL,
  `stat` varchar(255) DEFAULT 'in',
  `town` varchar(255) DEFAULT NULL,
  `wd_date` date DEFAULT NULL,
  `wd_reason` varchar(255) DEFAULT NULL,
  `exclude_run` int(11) DEFAULT '0',
  `bill_template` varchar(255) DEFAULT NULL,
  `admin_template` varchar(255) DEFAULT NULL,
  `bill_audit` varchar(255) DEFAULT NULL,
  `admin_audit` varchar(255) DEFAULT NULL,
  `bill_audit_edit` varchar(255) DEFAULT NULL,
  `admin_audit_edit` varchar(255) DEFAULT NULL,
  `mail_to` varchar(1) DEFAULT 'F',
  `intake` varchar(255) DEFAULT NULL,
  `st_intake` int(11) DEFAULT NULL,
  `first_child` varchar(50) DEFAULT '',
  `st_accyear` int(11) DEFAULT NULL,
  `entered_date` date DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exclude_transport` int(255) DEFAULT '0',
  `st_enqid` int(11) DEFAULT NULL,
  `st_gender` int(11) DEFAULT NULL,
  `mom_add2` varchar(255) DEFAULT NULL,
  `mom_addcity` varchar(255) DEFAULT NULL,
  `dad_add2` varchar(255) DEFAULT NULL,
  `dad_addcity` varchar(255) DEFAULT NULL,
  `guar_add2` varchar(255) DEFAULT NULL,
  `guar_addcity` varchar(255) DEFAULT NULL,
  `emg_add2` varchar(255) DEFAULT NULL,
  `emg_addcity` varchar(255) DEFAULT NULL,
  `1sch_add2` varchar(255) DEFAULT NULL,
  `1sch_addcity` varchar(255) DEFAULT NULL,
  `st_prev_wd` int(11) DEFAULT NULL,
  `st_prevreason` varchar(255) DEFAULT NULL,
  `st_prev_acts` text,
  `st_englishexperience` text,
  `st_admintemplate` int(11) DEFAULT NULL,
  `st_termtemplate` int(11) DEFAULT NULL,
  `st_feestructure` int(11) DEFAULT NULL,
  `st_addpaymethod` tinyint(4) DEFAULT NULL,
  `st_infComPerson` int(11) DEFAULT NULL,
  `st_lastpromoted` int(11) DEFAULT NULL,
  `st_term` int(11) DEFAULT NULL,
  `st_branch` int(11) DEFAULT NULL,
  `st_motherstaff` int(11) DEFAULT NULL,
  `mom_alt` text,
  `mom_fornum` text,
  `st_fatherstaff` int(11) DEFAULT NULL,
  `dad_alt` text,
  `dad_fornum` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of st_details_temp
-- ----------------------------
INSERT INTO `st_details_temp` VALUES ('TEMPSTUMB0010', null, '1', null, 'tyytytyty', 'uyuyuyu', 'M', '0000-00-00', '', '0', null, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '0000-00-00', null, null, null, null, null, null, null, null, null, null, '', '', null, null, null, null, '', null, '', null, '', '', '', '', '', '', '', '', '', null, null, '', '0000-00-00', '0', null, null, null, null, null, '0', 'in', 'R', 'in', null, null, null, '0', null, null, null, null, null, null, 'F', null, '3', '', '2', '2018-04-06', '1', '0', '0', null, '', '', '', '', '', '', '', '', '', '', null, null, null, null, '1', '2', null, null, '1', null, '5', '1', '0', '', '', '0', '', '');
INSERT INTO `st_details_temp` VALUES ('TEMPSTUMB0011', null, '1', null, 'hghgfhgfh', 'ghfghfgh', null, '0000-00-00', '', '0', null, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '0000-00-00', null, null, null, null, null, null, null, null, null, null, '', '', null, null, null, null, '', null, '', null, '', '', '', '', '', '', '', '', '', null, null, '', '0000-00-00', '0', null, null, null, null, null, '0', 'in', 'R', 'in', null, null, null, '0', null, null, null, null, null, null, 'F', null, '3', '', '2', '2018-04-06', '2', '0', '0', null, '', '', '', '', '', '', '', '', '', '', null, null, null, null, '1', '2', null, null, '1', null, '5', '1', '0', '', '', '0', '', '');
INSERT INTO `st_details_temp` VALUES ('TEMPSTUMB0012', null, '5', null, 'gfhfghgfh', 'gdfgfg', 'M', '0000-00-00', '', '0', null, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '0000-00-00', null, null, null, null, null, null, null, null, null, null, '', '', null, null, null, null, '', null, '', null, '', '', '', '', '', '', '', '', '', null, null, '', '0000-00-00', '0', null, null, null, null, null, '0', 'in', 'P', 'in', null, null, null, '0', null, null, null, null, null, null, 'F', null, '3', '', '2', '2018-04-06', '3', '0', '0', null, '', '', '', '', '', '', '', '', '', '', null, null, null, null, '1', '2', null, null, '1', null, '5', '1', '0', '', '', '0', '', '');
INSERT INTO `st_details_temp` VALUES ('TEMPSTUMB0013', null, '7', null, 'fgdfgfd', 'gfgdfgdfg', null, '0000-00-00', '', '0', null, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '0000-00-00', null, null, null, null, null, null, null, null, null, null, '', '', null, null, null, null, '', null, '', null, '', '', '', '', '', '', '', '', '', null, null, '', '0000-00-00', '0', null, null, null, null, null, '0', 'in', 'P', 'in', null, null, null, '0', null, null, null, null, null, null, 'F', null, '3', '', '2', '2018-04-06', '4', '0', '0', null, '', '', '', '', '', '', '', '', '', '', null, null, null, null, '1', '2', null, null, '1', null, '5', '1', '0', '', '', '0', '', '');
INSERT INTO `st_details_temp` VALUES ('TEMPSTUMB0014', null, '5', null, 'fgfdgdfg', 'fdgdgdfg', null, '0000-00-00', '', '0', null, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '0000-00-00', null, null, null, null, null, null, null, null, null, null, '', '', null, null, null, null, '', null, '', null, '', '', '', '', '', '', '', '', '', null, null, '', '0000-00-00', '0', null, null, null, null, null, '0', 'in', 'R', 'in', null, null, null, '0', null, null, null, null, null, null, 'F', null, '3', '', '2', '2018-04-06', '5', '0', '0', null, '', '', '', '', '', '', '', '', '', '', null, null, null, null, '1', '2', null, null, '1', null, '5', '1', '0', '', '', '0', '', '');
INSERT INTO `st_details_temp` VALUES ('TEMPSTUMB0015', null, '8', null, 'fgfg', 'fgdfgf', null, '0000-00-00', '', '0', null, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '0000-00-00', null, null, null, null, null, null, null, null, null, null, '', '', null, null, null, null, '', null, '', null, '', '', '', '', '', '', '', '', '', null, null, '', '0000-00-00', '0', null, null, null, null, null, '0', 'in', 'R', 'in', null, null, null, '0', null, null, null, null, null, null, 'F', null, '3', '', '2', '2018-04-06', '6', '0', '0', null, '', '', '', '', '', '', '', '', '', '', null, null, null, null, '1', '2', null, null, '1', null, '5', '1', '0', '', '', '0', '', '');
INSERT INTO `st_details_temp` VALUES ('TEMPSTUMB0016', null, '5', null, 'fdgdfgdfg', 'gdfgdfffffffffffffffff', null, '0000-00-00', '', '0', null, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '0000-00-00', null, null, null, null, null, null, null, null, null, null, '', '', null, null, null, null, '', null, '', null, '', '', '', '', '', '', '', '', '', null, null, '', '0000-00-00', '0', null, null, null, null, null, '0', 'in', 'R', 'in', null, null, null, '0', null, null, null, null, null, null, 'F', null, '3', '', '2', '2018-04-06', '7', '0', '0', null, '', '', '', '', '', '', '', '', '', '', null, null, null, null, '1', '2', null, null, '1', null, '5', '1', '0', '', '', '0', '', '');
INSERT INTO `st_details_temp` VALUES ('TEMPSTUMB0017', null, '5', null, 'gfgdfgdfg', 'ffgdfgdgdfg', null, '0000-00-00', '', '0', null, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '0000-00-00', null, null, null, null, null, null, null, null, null, null, '', '', null, null, null, null, '', null, '', null, '', '', '', '', '', '', '', '', '', null, null, '', '0000-00-00', '0', null, null, null, null, null, '0', 'in', 'R', 'in', null, null, null, '0', null, null, null, null, null, null, 'F', null, '3', '', '2', '2018-04-06', '8', '0', '0', null, '', '', '', '', '', '', '', '', '', '', null, null, null, null, '1', '2', null, null, '1', null, '5', '1', '0', '', '', '0', '', '');
INSERT INTO `st_details_temp` VALUES ('TEMPSTUMB0018', null, '5', null, 'hgfhgfh', 'hfghfgh', null, '0000-00-00', '', '0', null, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '0000-00-00', null, null, null, null, null, null, null, null, null, null, '', '', null, null, null, null, '', null, '', null, '', '', '', '', '', '', '', '', '', null, null, '', '0000-00-00', '0', null, null, null, null, null, '0', 'in', 'P', 'in', null, null, null, '0', null, null, null, null, null, null, 'F', null, '3', '', '2', '2018-04-06', '9', '0', '0', null, '', '', '', '', '', '', '', '', '', '', null, null, null, null, '1', '2', null, null, '1', null, '5', '1', '0', '', '', '0', '', '');

-- ----------------------------
-- Table structure for `subjects_cat`
-- ----------------------------
DROP TABLE IF EXISTS `subjects_cat`;
CREATE TABLE `subjects_cat` (
  `scat_id` int(11) NOT NULL AUTO_INCREMENT,
  `classid` int(11) NOT NULL,
  `scat_name` varchar(255) NOT NULL,
  `subject_id_array` varchar(255) NOT NULL,
  `created_on` date NOT NULL,
  PRIMARY KEY (`scat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of subjects_cat
-- ----------------------------

-- ----------------------------
-- Table structure for `time_period`
-- ----------------------------
DROP TABLE IF EXISTS `time_period`;
CREATE TABLE `time_period` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `period_id` int(11) NOT NULL,
  `from_p` varchar(255) NOT NULL,
  `to_p` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of time_period
-- ----------------------------

-- ----------------------------
-- Function structure for `get_next_value`
-- ----------------------------
DROP FUNCTION IF EXISTS `get_next_value`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `get_next_value`(prefix varchar(12)) RETURNS int(11)
    DETERMINISTIC
    SQL SECURITY INVOKER
begin  
  declare current_val integer;
  
  INSERT INTO HGC_SEQUENCE (HGC_SEQ_Name,HGC_SEQ_NextValue) VALUES (prefix,1) ON DUPLICATE KEY UPDATE HGC_SEQ_NextValue = HGC_SEQ_NextValue + 1;
  SELECT HGC_SEQ_NextValue into current_val FROM HGC_SEQUENCE WHERE HGC_SEQ_Name = prefix;  
  
  return current_val;
end
;;
DELIMITER ;
